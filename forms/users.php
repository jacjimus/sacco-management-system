<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>...::: IMAB MIS :::...</title>
	<link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../css/isacco.css">
	
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../js/isacco.js"></script>
</head>

<body>
<div id="toolbar_users" style="padding:5px;height:auto"> 
    <div style="margin-bottom:5px"> 
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New User</a>  
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit User</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="resetUserPassword()">Reset Password</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="resetUserLoginAttempts()">Reset Login Attempts</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="accountsUser()">User Accounts</a> 		
    </div> 
    <div> 
        <input name="searchuser" id="su" class="easyui-combobox" style="width:200px"
		data-options="url:'../data/get_data.php?action=5',valueField:'userid',textField:'username'">
       
         <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="searchUser()">Search</a>
    </div> 
</div>  
<table id="dgUsers" class="easyui-datagrid" style="width:1000px;height:400px" 
data-options="url:'../data/get_data.php?action=4',method:'get',toolbar:'#toolbar_users',singleSelect:true,fitColumns:true,striped:true,autoRowHeight:true,remoteSort:false,sortName:'userid',pagination:true" >
    <thead> 
        <tr> 
			<th data-options="field:'userid'">User ID</th>
			<th data-options="field:'username'">User Name</th>
			<th data-options="field:'branchid',hidden:true">Profile ID</th>
			<th data-options="field:'branchname'">Branch</th>
			<th data-options="field:'fullname'">Full Name</th>
			<th data-options="field:'idtype',hidden:true">idtype</th>
			<th data-options="field:'idtypedesc'">ID Type</th>						
			<th data-options="field:'idno'">ID No</th>
			<th data-options="field:'loginattempts'">Login Attempts</th>
			<th data-options="field:'profileid',hidden:'true'">Profile ID</th>
			<th data-options="field:'profilename'">User Profile</th>
			<th data-options="field:'cashieraccount'">Cashier Account</th>				
			<th data-options="field:'active',hidden:true">Active</th>		
			<th data-options="field:'activestatus'">Is Active</th>
			<th data-options="field:'createdon'">Created On</th>			
        </tr>  
    </thead>  
</table> 

<div id="dlgUsers" class="easyui-dialog" style="width:400px;height:350px;padding:10px 20px" closed="true" buttons="#dlg-buttons-users">   
    <form id="fmUsers" method="post">
		<table>
			<tr>
				<td>User Name:</td>
				<td><input name="username" class="easyui-validatebox" required="true"></td>
			</tr>		
			<tr>
				<td>Branch :</td>
				<td>
					<input name="branchid" id="branchid" class="easyui-combobox" style="width:200px"
					data-options="url:'../data/get_data.php?action=7',valueField:'branchid',textField:'branchname'">
				</td>
			</tr>	
			<tr>
				<td>Full Name:</td>
				<td><input name="fullname" class="easyui-validatebox" required="true"></td>
			</tr>
			<tr>
				<td>ID Type :</td>
				<td>
					<input name="idtype" id="idtype" class="easyui-combobox" style="width:200px"
					data-options="url:'../data/get_data.php?action=8',valueField:'valuemember',textField:'displaymember'">					 			
				</td>
			</tr>			
			<tr>
				<td>National ID:</td> 
				<td><input name="idno"></td>
			</tr>
			<tr>
				<td>User Profile :</td>
				<td>
					<input name="profileid" id="profileid" class="easyui-combobox" style="width:200px"
					data-options="url:'../data/get_data.php?action=3',valueField:'profileid',textField:'profilename'">					 			
				</td>
			</tr>	
			<tr>
				<td>Is Active :</td>
				<td> 
					<select id="active" class="easyui-combobox" name="active" style="width:200px;">  
						<option value="0">false</option>  
						<option value="1">true</option> 
					</select>
				</td> 
			</tr>	
		</table>
    </form>  
</div>  
<div id="dlg-buttons-users">  
    <a href="#" id="btnuser" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#btnuser').linkbutton('disable');saveUser();">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#fmUsers').form('clear');$('#dlgUsers').dialog('close');">Cancel</a>  
</div>  

<div id="dlgUserAccounts" class="easyui-dialog" style="width:550px;height:300px;padding:10px 20px" closed="true" buttons="#dlg-buttons-useraccounts">   
    <form id="fmUserAccounts" method="post">
		<table>
			<tr>
			<td align="right">User Cashier Account :</td>
			<td>
				<input name="cashieraccount" id="cashieraccount" class="easyui-combobox" style="width:300px"
				data-options="  
				valueField: 'accountid',  
				textField: 'accountname',  
				url: '../data/get_data.php?action=51&categoryid=3'"  /> 
			</td>
			</tr>
		</table>
    </form>  
</div>  
<div id="dlg-buttons-useraccounts">  
    <a href="#"id="btnuseraccounts" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#btnuseraccounts').linkbutton('disable');saveUserAccounts();">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#fmUserAccounts').form('clear');$('#dlgUserAccounts').dialog('close');">Cancel</a>  
</div>  

<div id="dlgUserPassord" class="easyui-dialog" style="width:450px;height:300px;padding:10px 20px" closed="true" buttons="#dlg-buttons-userresetpassword">   
    <form id="fmUserPassord" method="post">
		<table>
			<tr>
				<td>Password :</td>
				<td><input name="password" class="easyui-validatebox" required="true" type="password"></td>
			</tr>
		</table>
    </form> 
	<div id="pswd_reset_info">
		<h4>Password must meet the following requirements:</h4>
		<ul>
			<li>Length should be six or more characters</li>
			<li>Should contains at least one digit (0-9)</li>
			<li>At least one lowercase letter</li>
			<li>At least one uppercase character</li>
		</ul>
	</div>		
</div>  
<div id="dlg-buttons-userresetpassword">  
    <a href="#" id="btnresetpassword" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#btnresetpassword').linkbutton('disable');saveUserPasswordReset();">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#fmUserPassord').form('clear');$('#dlgUserPassord').dialog('close');">Cancel</a>  
</div>  
</body>
</html>
