<?php

define('DB_SERVER', '127.0.0.1');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '0kd6UP84vu');
define('DB_DATABASE', 'saccodb');

class DB_Class 
{
	function __construct() 
	{
	}

	public function Myconn() {	
		try {
		 $connection = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_DATABASE,
			DB_USERNAME,
			DB_PASSWORD,
			  array(PDO::ATTR_PERSISTENT => true));
			 
		} catch (Exception $e) {
			echo json_encode(array('retcode'=>'001','retmsg'=>'Server Connection error','results'=>$e->getMessage()));
			die();
		}

		return $connection;
	}
}
?>