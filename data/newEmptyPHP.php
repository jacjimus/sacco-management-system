<?php
session_start();
date_default_timezone_set('Africa/Nairobi');
include('../conf/config.inc');

#Global Variable
$action=$_REQUEST['action'];

#Constants
$retcode='999';
$retmsg='General Failure';
$results='';

class GetData {
	private $dbconnect;
	
   //Database connect
	public function __construct()
	{
           
		$db = new DB_Class;
		$this->dbconnect=$db->Myconn();
	}
	
	function formatMoney($number, $fractional=false) { 
		if ($fractional) { 
			$number = sprintf('%.2f', $number); 
		} 
		while (true) { 
			$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
			if ($replaced != $number) { 
				$number = $replaced; 
			} else { 
				break; 
			} 
		} 
		return $number; 
	} 	
	//login function
	public function User_Login($username,$pwd) {
		
	$sql="SELECT tb_users.userid,tb_users.username, tb_users.branchid,tb_branches.branchname,tb_userprofiles.profileid,tb_userprofiles.profilename,tb_userprofiles.active AS profilestatus, tb_users.active, 
	(SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
						INNER JOIN tb_ledgeraccounts
						ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
						WHERE userid = tb_users.userid
						AND gltype = 8) AS cashieraccountid,
	(SELECT tb_ledgeraccounts.description FROM tb_ledgerlinks
						INNER JOIN tb_ledgeraccounts
						ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
						WHERE userid = tb_users.userid
						AND gltype = 8) AS cashieraccount
	FROM tb_users 
	INNER JOIN tb_userprofiles
	ON tb_users.profileid=tb_userprofiles.profileid
	INNER JOIN tb_branches
	ON tb_branches.branchid=tb_users.branchid WHERE  
	(username = '$username') 
	and (password = '$pwd')  LIMIT 1";
	
		if($login=$this->dbconnect->query($sql))
		{
			$session = array();
			if ($login->rowCount()==1){
			
				if (!isset($_SESSION['created'])) {
					$_SESSION['created'] = time();
	
				} else if (time() - $_SESSION['created'] > 900) {// session started more than 15 minutes ago
					session_regenerate_id(true);    // change session ID for the current session an invalidate old session ID
					$_SESSION['created'] = time();  // update creation time
					
				}
				
			
				foreach($login=$this->dbconnect->query($sql) as $row) {
                                    if($row['profilestatus'] <> 1)
                                    {
                                        $retcode='001';
                                        $retmsg='<strong>Failed:</strong> Your profile account is not Activated. Contact care center for assistance';
                                        $results='';
                                        echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>''));
                                        return;
                                    }
                                    if($row['active'] == 3)
                                    {
                                        $retcode='001';
                                        $retmsg='<strong>Failed:</strong> Your account is not Activated. Contact care center for assistance';
                                        $results='';
                                        echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>''));
                                        return;
                                    }
					$_SESSION['username'] = $row['username'];
					$_SESSION['userid'] = $row['userid'];
//                                        /$_SESSION['idno'] = $row['idno'];
					$_SESSION['branchid'] = $row['branchid'];
					$_SESSION['branchname'] = $row['branchname'];
					$_SESSION['profileid'] = $row['profileid'];
					$_SESSION['profilename'] = $row['profilename'];
					$_SESSION['cashieraccountid'] = $row['cashieraccountid'];
					$_SESSION['cashieraccount'] = $row['cashieraccount'];					
					$_SESSION['sesidletime']=900;
				}
				
				$session['username'] = $_SESSION['username'];
				$session['userid'] = $_SESSION['userid'];
				$session['branchid'] = $row['branchid'];
				$session['branchname'] = $row['branchname'];
				$session['profileid'] = $row['profileid'];
				$session['profilename'] = $row['profilename'];
				$session['cashieraccountid'] = $row['cashieraccountid'];
				$session['cashieraccount'] = $row['cashieraccount'];
				$session['mainurl'] = './forms/main.html';
				
				$retcode='000';
				$retmsg='Login Successful';
				$results='./forms/main.html';
			}else {
				$retcode='001';
				$retmsg='Failed: Please enter correct login details!';
				$results='';
			}
		}else{
			$retcode='001';
			$retmsg='Login Failed';
			$results='';
		}		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$session));
	}
	
	public function LoadMenus() {
	$sql="SELECT tb_menus.menuid,tb_menus.icons AS menusicons,tb_menus.menuname,tb_menuitems.icon AS menuitemsicons,
	    tb_menuitems.itemid,tb_menuitems.itemname,tb_menuitems.filename  
		FROM tb_userpermissions 
		INNER JOIN tb_menuitems
		ON tb_menuitems.itemid=tb_userpermissions.itemid
		INNER JOIN tb_menus
		ON tb_menus.menuid=tb_menuitems.menuid
		WHERE profileid=".$_SESSION['profileid']."
		ORDER BY tb_menus.orderby,tb_menuitems.orderby";
		
    if($res=$this->dbconnect->query($sql)){
		$items = array();
		
		foreach($res as $row) {
			$node=array();
			$node['menuid']=$row['menuid'];
			$node['menusicons']=$row['menusicons'];
			$node['menuname']=$row['menuname'];
			$node['menuitemsicons']=$row['menuitemsicons'];
			$node['itemid']=$row['itemid'];
			$node['itemname']=$row['itemname'];
			$node['filename']=$row['filename'];		
			
			array_push($items, $node);
		}
		
		$retcode='000';	
		$retmsg='Loading menus';
		$results=$items;	
	}else{
		$retcode='002';	
		$retmsg='User Not allowed to login';
		$results='';		
	}

	echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//User Profiles
	public function UserProfile() {
		$sql="SELECT profileid,profilename,active,CASE active WHEN 1 THEN 'YES' ELSE 'NO' END AS activestate FROM tb_userprofiles";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['profileid']=$row['profileid'];
				$node['profilename']=$row['profilename'];
				$node['active']=$row['active'];
				$node['activestate']=$row['activestate'];
				
				array_push($items, $node);
			}
		
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No user profile found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Users
	public function getUsers($userid,$issearch,$page,$rows) {
		if ($issearch==1) {
			$sql="SELECT userid,CONCAT(userid,' - ',username) AS username FROM tb_users";
			
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['userid']=$row['userid'];
					$node['username']=$row['username'];
					
					array_push($items, $node);
				}
			
				echo json_encode($items);
			}else{
				$retcode='003';
				$retmsg='No user found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}			
		} else {
			$offset = ($page-1)*$rows;
						 
			if ($userid==0){
				$sql_count="SELECT COUNT(*) FROM tb_users
						INNER JOIN tb_branches
						ON tb_branches.branchid=tb_users.branchid
						INNER JOIN tb_userprofiles
						ON tb_userprofiles.profileid=tb_users.profileid";
			
				$sql = "SELECT tb_users.userid,tb_users.username,tb_users.branchid,tb_branches.branchname,tb_users.fullname,tb_users.idno,idtype,
				(SELECT description FROM tb_miscellaneouslists WHERE parentid=10 AND valuemember=idtype) as idtypedesc,
						tb_users.profileid,tb_userprofiles.profilename,tb_users.active,CASE tb_users.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestatus,tb_users.createdon,
						(SELECT tb_ledgeraccounts.description FROM tb_ledgerlinks
						INNER JOIN tb_ledgeraccounts
						ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
						WHERE userid = tb_users.userid
						AND gltype = 8) AS cashieraccount
						FROM tb_users
						INNER JOIN tb_branches
						ON tb_branches.branchid=tb_users.branchid
						INNER JOIN tb_userprofiles
						ON tb_userprofiles.profileid=tb_users.profileid limit $offset,$rows"; 
			} else {
				$sql_count="SELECT COUNT(*) FROM tb_users
						INNER JOIN tb_branches
						ON tb_branches.branchid=tb_users.branchid
						INNER JOIN tb_userprofiles
						ON tb_userprofiles.profileid=tb_users.profileid
				        WHERE tb_users.userid=$userid";
				
				$sql = "SELECT tb_users.userid,tb_users.username,tb_users.branchid,tb_branches.branchname,tb_users.fullname,tb_users.idno,idtype,
						(SELECT description FROM tb_miscellaneouslists WHERE parentid=10 AND valuemember=idtype)	as idtypedesc,
						tb_users.profileid,tb_userprofiles.profilename,tb_users.active,CASE tb_users.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestatus,tb_users.createdon,
						(SELECT tb_ledgeraccounts.description FROM tb_ledgerlinks
						INNER JOIN tb_ledgeraccounts
						ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
						WHERE userid = tb_users.userid
						AND gltype = 8) AS cashieraccount
						FROM tb_users
						INNER JOIN tb_branches
						ON tb_branches.branchid=tb_users.branchid
						INNER JOIN tb_userprofiles
						ON tb_userprofiles.profileid=tb_users.profileid
				        WHERE tb_users.userid=$userid limit $offset,$rows";
			}
			
			$rs=$this->dbconnect->query($sql_count);
			$row=$rs->fetch();;
			$result["total"] = $row[0];
			
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['userid']=$row['userid'];
					$node['username']=$row['username'];
					$node['branchid']=$row['branchid'];
					$node['branchname']=$row['branchname'];
					$node['fullname']=$row['fullname'];
					$node['idno']=$row['idno'];
					$node['idtype']=$row['idtype'];
					$node['idtypedesc']=$row['idtypedesc'];
					$node['profileid']=$row['profileid'];
					$node['profilename']=$row['profilename'];
					$node['active']=$row['active'];
					$node['activestatus']=$row['activestatus'];
					$node['createdon']=$row['createdon'];
					$node['cashieraccount']=$row['cashieraccount'];
										
					array_push($items, $node);
				}
				
				$result["rows"] = $items;
				
				echo json_encode($result);
			}else{
				$retcode='003';
				$retmsg='No user profile found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}			
		}				
	}	

	//Add User
	public function addUser($username, $password, $branchid,$fullname,$idtype,$idno,$profileid,$active) {
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_usercreation = $this->dbconnect->prepare("INSERT INTO tb_users(username,password,branchid,fullname,idtype,idno,profileid,active,createdon) VALUES(?,MD5(?),?,?,?,?,?,?,NOW())");
			$res_usercreation->execute(array($username,$password, $branchid,$fullname,$idtype,$idno,$profileid,$active));
			
			$this->dbconnect->commit();

			$retcode='000';
			$retmsg='User Created Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}
	}

	//Reset User Login Attempts
	public function ResetUserLoginAttempts($userid) {
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_useredit = $this->dbconnect->prepare("UPDATE tb_users SET loginattempts=0 WHERE userid = ?");
			$res_useredit->execute(array($userid));
			
			$this->dbconnect->commit();

			$retcode='000';
			$retmsg='User Login Attempts Reset Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}
	}
	
	//ResetUserPassword
	public function ResetUserPassword($userid,$password) {
		//Check if user exist
		$sql_checkuser="SELECT * FROM tb_users WHERE userid=".$userid;
		if($res=$this->dbconnect->query($sql_checkuser)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'User does not exist. Contact system support','results'=>''));
				return;
			}
		}else{
			echo json_encode(array('retcode'=>'003','retmsg'=>'User does not exist. Contact system support','results'=>''));
			return;
		}					
		//matches a string of six or more characters
		if(strlen($password) < 6)
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password too short!','results'=>''));
			return;			
		}
		//that contains at least one digit (0-9)
		if(!preg_match("#[0-9]+#",$password))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one number!','results'=>''));
			return;			
		}
		//that contains at least one letter
		if(!preg_match("#[a-zA-Z]+#",$password))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one letter!','results'=>''));
			return;			
		}
		//that contains at least one letter in uppercase
		if(!preg_match("#[A-Z]+#",$password))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one uppercase letter!','results'=>''));
			return;			
		}	
		//that contains at least one letter in lowercase
		if(!preg_match("#[a-z]+#",$password))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one lowercase letter!','results'=>''));
			return;			
		}
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_useredit = $this->dbconnect->prepare("UPDATE tb_users SET password=MD5(?) WHERE userid = ?");
			$res_useredit->execute(array($password,$userid));
			
			$this->dbconnect->commit();

			$retcode='000';
			$retmsg='User Password Reset Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}
	}
	
        
      
	//Edit User
	public function editUser($userid,$username,$branchid,$fullname,$idtype,$idno,$profileid,$active) {
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_useredit = $this->dbconnect->prepare("UPDATE tb_users SET username=?,branchid=?,fullname=?,idtype=?,idno=?,profileid=?,active=? WHERE userid = ?");
			$res_useredit->execute(array($username,$branchid,$fullname,$idtype,$idno,$profileid,$active,$userid));
			
			$this->dbconnect->commit();

			$retcode='000';
			$retmsg='User Edited Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}
	}
	//Assing loan officer to suctomer
	public function assingLoanofficer($customerid,$loanofficerid) {
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_useredit = $this->dbconnect->prepare("UPDATE tb_customer SET loanofficerid=?,status=? WHERE customerid = ?");
			$res_useredit->execute(array($loanofficerid,1,$customerid));
			$sql = "SELECT tb_users.userid FROM tb_users JOIN tb_person ON tb_users.userid = tb_person.loginid JOIN tb_customerperson ON tb_customerperson.personid = tb_person.personid WHERE tb_customerperson.customerid = $customerid";
                        $result=$this->dbconnect->query($sql);
                        foreach($result As $row)
                        $userid = $row['userid'];
                        $res_usredit = $this->dbconnect->prepare("UPDATE tb_users SET active=? WHERE userid = ?");
			$res_usredit->execute(array(1,$userid));
			
			$this->dbconnect->commit();

			$retcode='000';
			$retmsg='customer updated successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}
	}

	//Add User Profile
	public function addUserProfile($profilename,$active,$permissions) {
	
		$sql="INSERT INTO tb_userprofiles(profilename,active) VALUES('$profilename',$active)";

		if($res=$this->dbconnect->query($sql)){

			$profileid = $this->dbconnect->lastInsertId();
			
			if (!empty($permissions)) {
				foreach($permissions as $pms) {
					$sql_pms="INSERT INTO tb_userpermissions(profileid,itemid) SELECT $profileid,".$pms['itemid'].";";
					
					$result_pms = $this->dbconnect->query($sql_pms);
				}
			}
			
			$retcode='000';
			$retmsg='User Profile Created Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}else{
			$retcode='003';
			$retmsg='No Failed to ';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}

	//Edit User Profile
	public function editUserProfile($profileid,$profilename,$active,$permissions) {	
		$sql="UPDATE tb_userprofiles SET profilename='$profilename',active=$active WHERE profileid = $profileid";
		
		if($res=$this->dbconnect->query($sql)){
			$sql_rights="DELETE FROM tb_userpermissions WHERE profileid=$profileid;";
			if($result_rights = $this->dbconnect->query($sql_rights)){
				$array_permissions = json_decode($permissions, true);
				if (!empty($array_permissions)) {
					foreach($array_permissions as $pms) {
						$sql_pms="INSERT INTO tb_userpermissions(profileid,itemid) SELECT $profileid,".$pms['itemid'].";";					
						$result_pms = $this->dbconnect->query($sql_pms);
					}
				}
				
				$retcode='000';
				$retmsg='User Profile Edited Successfully';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}else{
				$retcode='003';
				$retmsg='No Failed to update user profile rights';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}					
		}else{
			$retcode='003';
			$retmsg='No Failed to update user profile';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Add Branch
	public function addBranch($branchname,$code,$active,$addressid,$phoneno) {	
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$date = date('Y-m-d');
								
			$res_account = $this->dbconnect->prepare("INSERT INTO tb_branches(branchname,code,active,addressid,phoneno,creationdate) VALUES(?,?,?,?,?,?)"); 			
			$res_account->execute( array($branchname,$code,$active,$addressid,$phoneno,$date));
						
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Branch Created Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}

	//Edit Branch
	public function editBranch($branchid,$branchname,$code,$active,$addressid,$phoneno) {	
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_account = $this->dbconnect->prepare("UPDATE tb_branches SET branchname=?,code=?,active=?,addressid=?,phoneno=? WHERE branchid=?"); 			
			$res_account->execute( array($branchname,$code,$active,$addressid,$phoneno,$branchid));
						
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Branch Edited Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}
	
	//Branches
	public function getBranches($filter) {
		if($filter == 0){
			$sql="SELECT branchid,branchname,code,phoneno,creationdate,active,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestatus,addressid FROM tb_branches";
			
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['branchid']=$row['branchid'];
					$node['branchname']=$row['branchname'];
					$node['code']=$row['code'];
					$node['phoneno']=$row['phoneno'];
					$node['creationdate']=$row['creationdate'];
					$node['active']=$row['active'];
					$node['activestatus']=$row['activestatus'];		
					$node['addressid']=$row['addressid'];
					
					array_push($items, $node);
				}
			
				echo json_encode($items);
			}else{
				$retcode='003';
				$retmsg='No branch found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}
		}else{
			$sql="SELECT 0 AS branchid,'All' AS branchname
			UNION ALL
			SELECT branchid,branchname FROM tb_branches ORDER BY branchid";
			
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['branchid']=$row['branchid'];
					$node['branchname']=$row['branchname'];
										
					array_push($items, $node);
				}
			
				echo json_encode($items);
			}else{
				$retcode='003';
				$retmsg='No branch found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}	

		}		
	}

	//Misc
	public function getLoanStatus() {	
		$sql="SELECT 0 AS valuemember ,'All' AS displaymember
		UNION ALL
		SELECT valuemember ,description as displaymember FROM tb_miscellaneouslists WHERE ParentID=8  AND active=1 ORDER BY valuemember";

		$items = array();
		
		if($res=$this->dbconnect->query($sql)){					
			foreach($res as $row) {
				$node=array();
				$node['valuemember']=$row['valuemember'];
				$node['displaymember']=$row['displaymember'];
				
				array_push($items, $node);
			}					
		}

		echo json_encode($items);			
	}	
		
	//Misc
	public function getCustomersforSelect() {	
		$result = array();
		
		
			
			$sql="SELECT customerid as valuemember , CONCAT(number,' - ' , NAME ) AS displaymember FROM tb_customer";
			
		
		if($res=$this->dbconnect->query($sql)){
		
				$items = array();
				foreach($res as $row) {
					$node=array();
					$node['customerid']=$row['customerid'];
					$node['customer']=$row['customer'];
					
					array_push($items, $node);
				}
				
				echo json_encode($items);
			}				
			
	}
	public function getMisc($ParentID,$page,$rows) {	
		$result = array();
		
		if ($ParentID == 0){
			$offset = ($page-1)*$rows;
			
			$sql_count="SELECT COUNT(tb_miscellaneouslists.id) As count
			FROM tb_miscellaneouslists 
			INNER JOIN tb_definations
			ON tb_miscellaneouslists.ParentID=tb_definations.ID";
			
			$rs=$this->dbconnect->query($sql_count);
			foreach($rs As $row)
			$result["total"] = $row['count'];
			
			
			$sql="SELECT tb_miscellaneouslists.id,parentid,tb_definations.description AS category,valuemember,tb_miscellaneouslists.description,
			code,defaultItem,CASE DefaultItem WHEN 1 THEN 'Yes' ELSE 'No' END AS defaultitemdesc,active,
			CASE active WHEN 1 THEN 'Yes' ELSE 'No' END AS activestate
			FROM tb_miscellaneouslists 
			INNER JOIN tb_definations
			ON tb_miscellaneouslists.ParentID=tb_definations.ID ORDER BY ParentID,ValueMember limit $offset,$rows";
			
		} else {
			$sql="SELECT valuemember ,description as displaymember FROM tb_miscellaneouslists WHERE ParentID=$ParentID  AND active=1";
		}
		if($res=$this->dbconnect->query($sql)){
		
			if ($ParentID ==0){				
				$items = array();				
				
				foreach($res as $row) {
					$node=array();
					$node['id']=$row['id'];
					$node['parentid']=$row['parentid'];					
					$node['category']=$row['category'];
					$node['valuemember']=$row['valuemember'];
					$node['description']=$row['description'];
					$node['code']=$row['code'];
					$node['defaultItem']=$row['defaultItem'];
					$node['defaultitemdesc']=$row['defaultitemdesc'];
					$node['active']=$row['active'];
					$node['activestate']=$row['activestate'];
					
					array_push($items, $node);
				}
				$result["rows"] = $items;
				
				echo json_encode($result);
			} else {
				$items = array();
				foreach($res as $row) {
					$node=array();
					$node['valuemember']=$row['valuemember'];
					$node['displaymember']=$row['displaymember'];
					
					array_push($items, $node);
				}
				
				echo json_encode($items);
			}				
		}else{
			$retcode='003';
			$retmsg='No branch found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get User Profile Menus
	public function getUserProfileMenus($activity,$profileid){		
		if($activity==1){//Edit profile			 		
			$sql = "SELECT * FROM tb_menus ORDER BY orderby";
			if($res=$this->dbconnect->query($sql)){
				$results = array();
				foreach($res as $row) {
					$node=array();
					$node['id']=$row['menuid'];
					$node['text']=$row['menuname'];
					$node['state']='closed';
						//Getting children
						$sql_children = "SELECT tb_menuitems.itemid,tb_menuitems.itemname, 
													CASE COALESCE(tb_userpermissions.itemid,0) WHEN 0 THEN 'false' ELSE 'true' END AS checked
													FROM tb_menuitems
													INNER JOIN tb_menus
													ON tb_menus.menuid=tb_menuitems.menuid
													LEFT JOIN tb_userpermissions
													ON tb_userpermissions.itemid=tb_menuitems.itemid
													AND tb_userpermissions.profileid=$profileid 
													WHERE tb_menuitems.menuid=".$row['menuid']." ORDER BY tb_menuitems.orderby";
													
						if($res_children=$this->dbconnect->query($sql_children)){
							if ($res_children->rowCount() > 0) {
								$children_data=array();
								foreach($res_children as $rows) {
									$children=array();
									$children['id']=$rows['itemid'];
									$children['text']=$rows['itemname'];
									if($rows['checked'] =='true'){
									$children['checked']=$rows['checked'];
									}
									array_push($children_data,$children);
								}
								
								$node['children']=$children_data;
							}
						}
						
					array_push($results,$node);
				}
				echo  json_encode($results, true);
			} else{
				$retcode='003';
				$retmsg='No branch found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}	
		}else{//new 		
			$sql = "SELECT * FROM tb_menus ORDER BY orderby";
			if($res=$this->dbconnect->query($sql)){			
				$results = array();
				foreach($res as $row) {
					$node=array();
					$node['id']=$row['menuid'];
					$node['text']=$row['menuname'];
					$node['state']='closed';
						//Getting children
						$sql_children = "SELECT * FROM tb_menuitems WHERE menuid=".$row['menuid']." ORDER BY orderby";
						if($res_children=$this->dbconnect->query($sql_children)){
							if ($res_children->rowCount() > 0) {
								$children_data=array();
								foreach($res_children as $rows) {
									$children=array();
									$children['id']=$rows['itemid'];
									$children['text']=$rows['itemname'];
									array_push($children_data,$children);
								}
								
								$node['children']=$children_data;
							}
						}
					array_push($results,$node);
				}
				echo  json_encode($results, true);
			} else{
				$retcode='003';
				$retmsg='No branch found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}			
		}	
	}

	//Definations
	public function getDefinations() {
		$sql="SELECT id,description FROM tb_definations ";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['id']=$row['id'];
				$node['description']=$row['description'];	
				
				array_push($items, $node);
			}
		
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No defination found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Category
	public function addMisc($parentid,$description,$code,$defaultitem,$active) {
		$sql = "INSERT INTO tb_miscellaneouslists(ParentID,ValueMember,Description,Code,DefaultItem,Active) SELECT $parentid,(SELECT COALESCE(MAX(ValueMember),0)+1 FROM tb_miscellaneouslists WHERE ParentID = $parentid),'$description','$code',$defaultitem,$active";

		if($res=$this->dbconnect->query($sql)){
			
			$retcode='000';
			$retmsg='Miscellaneous list record Created Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}else{
			$retcode='003';
			$retmsg='Error occured record cannot be created';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}

	//Edit Category
	public function editMisc($id,$parentid,$description,$code,$defaultitem,$active) {	
		$sql = "UPDATE tb_miscellaneouslists SET ParentID=$parentid,Description='$description',Code='$code',DefaultItem=$defaultitem,Active=$active  WHERE ID=$id";

		if($res=$this->dbconnect->query($sql)){
			
			$retcode='000';
			$retmsg='Miscellaneous list record updated Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}else{
			$retcode='003';
			$retmsg='Error occured record cannot be updated';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}
	
	//Get Loan Officers
	public function getLoanofficers($page,$rows,$filter) {
		if($filter == 0){
			$result = array();
			
			$offset = ($page-1)*$rows;
			
			$sql_count="SELECT COUNT(*) FROM tb_loanofficers
				INNER JOIN tb_branches
				ON tb_loanofficers.branchid = tb_branches.branchid";
			
			$rs=$this->dbconnect->query($sql_count);
			$row=$rs->fetch();;
			$result["total"] = $row[0];
				
			$sql="SELECT loanofficerid,description,tb_loanofficers.branchid,tb_branches.branchname,tb_loanofficers.active,
				CASE tb_loanofficers.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate,tb_loanofficers.creationdate FROM tb_loanofficers
				INNER JOIN tb_branches
				ON tb_loanofficers.branchid = tb_branches.branchid limit $offset,$rows";
			
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['loanofficerid']=$row['loanofficerid'];
					$node['description']=$row['description'];				
					$node['branchid']=$row['branchid'];				
					$node['branchname']=$row['branchname'];				
					$node['active']=$row['active'];				
					$node['activestate']=$row['activestate'];
					$node['creationdate']=$row['creationdate'];
					
					array_push($items, $node);
				}
			
				$result["rows"] = $items;
				
				echo json_encode($items);
			}else{
				$retcode='003';
				$retmsg='No defination found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}	
		}else{
			$sql="SELECT 0 AS loanofficerid,'All' AS description
			UNION ALL
			SELECT loanofficerid,description
			FROM tb_loanofficers
				INNER JOIN tb_branches
				ON tb_loanofficers.branchid = tb_branches.branchid ORDER BY loanofficerid";
				
			if($res=$this->dbconnect->query($sql)){
				
				$items = array();
				
				foreach($res as $row) {
					$node=array();
					$node['loanofficerid']=$row['loanofficerid'];
					$node['description']=$row['description'];				
										
					array_push($items, $node);
				}
			
				$result["rows"] = $items;
				
				echo json_encode($items);
			}else{
				$retcode='003';
				$retmsg='No defination found';
				$results='';
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}				
		}
	}

	//Add Loan Officer
	public function addLoanofficers($description,$branchid,$active) {
		$sql = "INSERT INTO tb_loanofficers(description,branchid,active,creationdate) VALUES('$description',$branchid,$active,NOW())";

		if($res=$this->dbconnect->query($sql)){
			
			$retcode='000';
			$retmsg='Loan Officer Created Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}else{
			$retcode='003';
			$retmsg='Error occured record cannot be created';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}

	//Edit Loan Officer
	public function editLoanofficers($loanofficerid,$description,$branchid,$active) {	
		$sql = "UPDATE tb_loanofficers SET description='$description',branchid=$branchid,active=$active,editdate=NOW()  WHERE loanofficerid=$loanofficerid";

		if($res=$this->dbconnect->query($sql)){
			
			$retcode='000';
			$retmsg='Loan Officer updated Successfully';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}else{
			$retcode='003';
			$retmsg='Error occured record cannot be updated';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}

	//Get Customers
	public function getCustomers($page,$rows,$status,$idno,$mobileno) {
		$result = array();
		$cond ="";
		$offset = ($page-1)*$rows;
		if($idno=="" && $mobileno =="" && $status == "")
                    {
                     $cond .= " tb_customer.status = 5 ";
                    }
		else {
                    if($status  <> "")
                    {
                     $cond .= " tb_customer.status = $status ";
                    }
                    
		if($idno <> "" ){
                    if($cond  <> "")
                   $cond .= " AND ";
                     $cond .= "tb_person.idno = '".$idno."' ";
                }
                if($mobileno <> ""){
                    if($cond <> "")
                    $cond .= " AND ";
                    $cond .= "  (RIGHT(tb_person.mobileno,9) = RIGHT('".$mobileno."',9)) ";
                }
                
                }
		$sql_count="SELECT COUNT(*)
		FROM tb_customer
		INNER JOIN tb_branches
		ON tb_branches.branchid  = tb_customer.branchid	
		LEFT JOIN tb_customeraddress
		ON tb_customeraddress.customerid = tb_customer.customerid			
		LEFT JOIN tb_address
		ON tb_customeraddress.addressid = tb_address.addressid
		LEFT JOIN tb_customerperson
		ON tb_customerperson.customerid = tb_customer.customerid
		LEFT JOIN tb_person
		ON tb_person.personid = tb_customerperson.personid 
                WHERE $cond" ;
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
			
		$sql="SELECT tb_customer.customerid,tb_customer.number,tb_customer.name,tb_customer.branchid,tb_branches.branchname,tb_customer.joindate,
		tb_customer.customertype,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 7 AND valueMember = tb_customer.customertype) AS customertypedesc,
		tb_customer.loanofficerid,tb_address.address, tb_address.postcode,tb_customeraddress.addresstype,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 9 AND valueMember = tb_customeraddress.addresstype) AS addresstypedesc,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 13 AND valueMember = tb_customer.status) AS statusdesc,
		tb_address.town,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 17 AND valueMember = tb_address.town) AS towndesc
		,tb_address.countryid,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 3 AND valueMember = tb_address.countryid) AS country,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 14 AND valueMember = tb_address.county) AS countydesc,
		tb_address.county,tb_person.personid,tb_person.title,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 5 AND valueMember = tb_person.title) AS titledesc,
		tb_person.surname,tb_person.firstname,
		tb_person.lastname,tb_person.gender,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 2 AND valueMember = tb_person.gender) AS genderdesc,
		tb_person.maritalstatus,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 2 AND valueMember = tb_person.maritalstatus) AS maritalstatusdesc,
		tb_person.dateofbirth,tb_person.idtype,
		(SELECT description FROM tb_miscellaneouslists WHERE parentid = 10 AND valueMember = tb_person.idtype) AS idtypedesc,
		tb_person.idno,tb_person.phoneno,tb_person.mobileno,tb_person.email,tb_address.addressid,
                (SELECT description FROM tb_miscellaneouslists WHERE parentid = 26 AND valueMember = tb_customerbankaccounts.bankid) AS bankdesc , tb_customerbankaccounts.bankid as bank, accountno ,accountname
		
		FROM tb_customer
		INNER JOIN tb_branches
		ON tb_branches.branchid  = tb_customer.branchid	
		LEFT JOIN tb_customeraddress
		ON tb_customeraddress.customerid = tb_customer.customerid			
		LEFT JOIN tb_address
		ON tb_customeraddress.addressid = tb_address.addressid
		LEFT JOIN tb_customerperson
		ON tb_customerperson.customerid = tb_customer.customerid
		LEFT JOIN tb_person
		ON tb_person.personid = tb_customerperson.personid 
                LEFT JOIN tb_customerbankaccounts
		ON tb_customerbankaccounts.customerid = tb_customer.customerid
                WHERE  $cond  limit $offset,$rows";			
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['customerid']=$row['customerid'];
				$node['number']=$row['number'];
				$node['name']=$row['name'];				
				$node['branchid']=$row['branchid'];
				$node['branchname']=$row['branchname'];				
				$node['joindate']=$row['joindate'];				
				$node['customertype']=$row['customertype'];
				$node['customertypedesc']=$row['customertypedesc'];				
				$node['statusdesc']=$row['statusdesc'];				
				$node['loanofficerid']=$row['loanofficerid'];
				$node['address']=$row['address'];				
				$node['postcode']=$row['postcode'];				
				$node['addresstype']=$row['addresstype'];
				$node['addresstypedesc']=$row['addresstypedesc'];
				$node['town']=$row['town'];
				$node['towndesc']=$row['towndesc'];
				$node['countryid']=$row['countryid'];
				$node['country']=$row['country'];
				$node['county']=$row['county'];							
				$node['countydes']=$row['countydesc'];
				$node['personid']=$row['personid'];
				$node['title']=$row['title'];
				$node['titledesc']=$row['titledesc'];
				$node['surname']=$row['surname'];
				$node['firstname']=$row['firstname'];
				$node['lastname']=$row['lastname'];
				$node['gender']=$row['gender'];
				$node['genderdesc']=$row['genderdesc'];
				$node['maritalstatus']=$row['maritalstatus'];
				$node['maritalstatusdesc']=$row['maritalstatusdesc'];
				$node['dateofbirth']=$row['dateofbirth'];
				$node['idtype']=$row['idtype'];
				$node['idtypedesc']=$row['idtypedesc'];
				$node['idno']=$row['idno'];				
				$node['phoneno']=$row['phoneno'];
				$node['mobileno']=$row['mobileno'];
				$node['email']=$row['email'];
				$node['bank']=$row['bank'];				
				$node['bankaccountname']=$row['accountname'];				
				$node['bankaccountno']=$row['accountno'];				
				$node['addressid']=$row['addressid'];				

				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($result);
		}else{
			$retcode='003';
			$retmsg='No Customer found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Customers
	public function addCustomers($branchid,$joindate,$customertype,$loanofficerid,$title,$surname,$firstname,$lastname,$gender,$maritalstatus,$dateofbirth,$email,$idtype,$idno,$phoneno,$mobileno,$address,$postcode,$town,$county,$province,$countryid,$addresstype,$bankid, $branchid,$accountname,$accountno) {
		try{
			//Check for duplicates id no
			$sql_checkduplicateidnos = "SELECT * FROM tb_person WHERE idno = '".$idno."'";			
			if($res=$this->dbconnect->query($sql_checkduplicateidnos)){
				if($res->rowCount()>0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Duplicate National ID Numbers not allowed : '.$idno,'results'=>''));
					return;
				}
			}
			
			//Check for duplicates mobile numbers
			$sql_checkduplicatemobilenos = "SELECT * FROM tb_person WHERE mobileno = '".$mobileno."'";			
			if($res=$this->dbconnect->query($sql_checkduplicatemobilenos)){
				if($res->rowCount()>0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Duplicate Mobile Numbers not allowed : '.$mobileno,'results'=>''));
					return;
				}
			}
			
			//Get Loan Officer Branch
			$sql_getloanofficerbranch = "SELECT * FROM tb_loanofficers WHERE loanofficerid = '".$loanofficerid."'";			
			if($res=$this->dbconnect->query($sql_getloanofficerbranch)){
				if($res->rowCount()>0){
					foreach($res as $row) {
						$branchid = $row['branchid'];
					}
				}
			}else{
				echo json_encode(array('retcode'=>'003','retmsg'=>'Loan officer not found','results'=>''));
				return;
			}				
			
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$name = strtoupper($surname).', '.strtoupper($firstname).' '.strtoupper($lastname);
			$date = date('Y-m-d');
			
			$res_customer = $this->dbconnect->prepare("INSERT INTO tb_customer (name,branchid,joindate,customertype,loanofficerid,creationdate) VALUES(?,?,?,?,?,?)"); 			
			$res_customer->execute( array($name,$branchid,$joindate,$customertype,$loanofficerid,$date));
			
			$customerid = $this->dbconnect->lastInsertId();
			$customerno = 'RS'.substr('000000'.$customerid, -6);
			
			$res_customer = $this->dbconnect->prepare("UPDATE tb_customer SET number=? WHERE customerid=?"); 			
			$res_customer->execute( array($customerno,$customerid ));
			
			$res_person = $this->dbconnect->prepare("INSERT INTO tb_person(title,surname,firstname,lastname,gender,maritalstatus,dateofbirth,email,idtype,idno,phoneno,mobileno,creationdate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");			
			$res_person->execute(array($title,strtoupper($surname),strtoupper($firstname),strtoupper($lastname),$gender,$maritalstatus,$dateofbirth,$email,$idtype,$idno,$phoneno,$mobileno,$date));
			
			$personid = $this->dbconnect->lastInsertId();
			
			$res_customerperson = $this->dbconnect->prepare("INSERT INTO tb_customerperson(customerid,personid) VALUES(?,?)");
			$res_customerperson->execute(array($customerid,$personid));
			
			$res_address = $this->dbconnect->prepare("INSERT INTO tb_address(address,postcode,town,county,province,countryid,creationdate) VALUES(?,?,?,?,?,?)");
			$res_address->execute(array($address,$postcode,$town,$county,$province,$countryid,$date));
			$addressid = $this->dbconnect->lastInsertId();
			
			$res_customeraddress = $this->dbconnect->prepare("INSERT INTO tb_customeraddress(customerid,addressid,addresstype,creationdate) VALUES(?,?,?,?)");
			$res_customeraddress->execute(array($customerid,$addressid,$addresstype,$date));
                        
                        // Insert customer Bank Details
			$res_customerbankaccount= $this->dbconnect->prepare("INSERT INTO tb_customerbankaccounts (bankid, branchid,accountname,accountno, customerid) VALUES (?,?,?,?,?)");
			$res_customerbankaccount->execute(array($bankid, $branchid,$accountname,$accountno, $customerid));
				
			
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Customer Created Successfully';
			$results=$customerid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	}
        

	//Edit Customers
	public function editCustomers($customerid,$personid,$addressid,$joindate,$customertype,$loanofficerid,$title,$surname,$firstname,$lastname,$gender,$maritalstatus,$dateofbirth,$email,$idtype,$idno,$phoneno,$mobileno,$address, $postcode, $town,$county,$countryid,$addresstype,$bankid, $branchid,$accountname,$accountno) {	
		try{
			//Check for duplicates id no
			$sql_checkduplicateidnos = "SELECT * FROM tb_person WHERE idno = '".$idno."' AND personid <> ".$personid;			
			if($res=$this->dbconnect->query($sql_checkduplicateidnos)){
				if($res->rowCount()>0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Duplicate National ID Numbers not allowed : '.$idno,'results'=>''));
					return;
				}
			}
			
			//Check for duplicates mobile numbers
			$sql_checkduplicatemobilenos = "SELECT * FROM tb_person WHERE mobileno = '".$mobileno."' AND personid <> ".$personid;			
			if($res=$this->dbconnect->query($sql_checkduplicatemobilenos)){
				if($res->rowCount()>0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Duplicate Mobile Numbers not allowed : '.$mobileno,'results'=>''));
					return;
				}
			}
			
			
			
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$name = strtoupper($surname).', '.strtoupper($firstname).' '.strtoupper($lastname);
			
			$date = date('Y-m-d');
			
			
			$res_customer = $this->dbconnect->prepare("UPDATE tb_customer SET name=?,joindate=?,customertype=?,loanofficerid=?,editdate=? WHERE customerid=?"); 			
			$res_customer->execute( array($name,$joindate,$customertype,$loanofficerid,$date,$customerid));
			
			$res_person = $this->dbconnect->prepare("UPDATE tb_person SET title=?,surname=?,firstname=?,lastname=?,gender=?,maritalstatus=?,dateofbirth=?,email=?,idtype=?,idno=?,phoneno=?,mobileno=?,editdate=? WHERE personid=?");			
			$res_person->execute(array($title,strtoupper($surname),strtoupper($firstname),strtoupper($lastname), $gender,$maritalstatus,$dateofbirth,$email,$idtype,$idno,$phoneno,$mobileno,$date,$personid));
			
			// Insert Customer address for the first time ELSE Update it!
                        
			$sql_checkadd = "SELECT customeraddressid FROM tb_customeraddress WHERE customerid = '".$customerid."'";			
			if($res=$this->dbconnect->query($sql_checkadd)){
				if($res->rowCount()>0){
					$res_address = $this->dbconnect->prepare("UPDATE tb_address SET address=?,postcode=?,town=?,county=?,countryid=?,editdate=? WHERE addressid=?");
                                        $res_address->execute(array($address,$postcode,$town,$county,$countryid,$date,$addressid));

                                        $res_customeraddress = $this->dbconnect->prepare("UPDATE tb_customeraddress SET addresstype=? WHERE customerid=? AND addressid=?");
                                        $res_customeraddress->execute(array($addresstype,$customerid,$addressid));
				}
                                else
                                {
                                    
                                        $res_address = $this->dbconnect->prepare("INSERT INTO tb_address (address,postcode,town,county,countryid,editdate) VALUES(?,?,?,?,?,NOW())");
                                        $res_address->execute(array($address, $postcode, $town, $county,$countryid));

                                        $addressid = $this->dbconnect->lastInsertId();
			
                                        $res_customeraddress = $this->dbconnect->prepare("INSERT INTO tb_customeraddress(customerid,addressid,addresstype,creationdate) VALUES(?,?,?,NOW())");
                                        $res_customeraddress->execute(array($customerid,$addressid,$addresstype));
                                }
			}
                        
                        //Get Bank Account
			$sql_getbank_account = "SELECT * FROM tb_customerbankaccounts WHERE customerid = '".$customerid."'";			
			if($acc=$this->dbconnect->query($sql_getbank_account)){
				if($acc->rowCount() < 1){
				// Insert customer Bank Details
			$res_customerbankaccount= $this->dbconnect->prepare("INSERT INTO tb_customerbankaccounts (bankid, branch,accountname,accountno, customerid) VALUES (?,?,?,?,?)");
			$res_customerbankaccount->execute(array($bankid, $branchid,$accountname,$accountno, $customerid));
				
				}
			
                        else {
                        // Update customer Bank Details
			$res_customerbankaccount= $this->dbconnect->prepare("UPDATE tb_customerbankaccounts SET bankid=? , branch=?,accountname=?,accountno=? WHERE customerid=? ");
			$res_customerbankaccount->execute(array($bankid, $branchid,$accountname,$accountno, $customerid));
                        }
                        }
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Customer Edited Successfully';
			$results=$customerid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}				
	}
        
        
        // Insert SMS Notification
        public function sendSms($msisdn , $body , $customerid)
        {
            try{
		$insert = $this->dbconnect->prepare("INSERT INTO tb_outgoingsms(msisdn,body,status,customerid,sendtime) VALUES(?,?,'New',?,now()");
		$insert->execute(array($msisdn,$body,$customerid));
                        return true;
			} catch (Exception $e) {
		  return false;
                  
                        }
        }

	//Get Currency
	public function getCurrency($page,$rows) {
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*) FROM tb_currency";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();;
		$result["total"] = $row[0];
			
		$sql="SELECT currencyid,code,description,symbol,rounding,homecurrency,active,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate,creationdate FROM tb_currency limit $offset,$rows";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['currencyid']=$row['currencyid'];
				$node['code']=$row['code'];				
				$node['description']=$row['description'];				
				$node['symbol']=$row['symbol'];
				$node['homecurrency']=$row['homecurrency'];				
				$node['rounding']=$row['rounding'];
				$node['active']=$row['active'];				
				$node['activestate']=$row['activestate'];
				$node['creationdate']=$row['creationdate'];
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No Currency found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Currency Combo
	public function getCurrencyCombo() {
		$result = array();
			
		$sql="SELECT currencyid,description FROM tb_currency WHERE active=1";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['currencyid']=$row['currencyid'];				
				$node['description']=$row['description'];				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No Currency found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Currency Combo
	public function getProductType($producttype) {
		$result = array();
		if($producttype ==''){
			$sql="SELECT 0 AS producttypeid,'ALL' AS description UNION ALL SELECT producttypeid,description FROM tb_producttype ORDER BY producttypeid";
		}else{
			$sql="SELECT producttypeid,description FROM tb_producttype WHERE producttype ='$producttype'";
		}
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['producttypeid']=$row['producttypeid'];				
				$node['description']=$row['description'];				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No Currency found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Add Currency
	public function addCurrency($code,$description,$symbol,$rounding,$homecurrency,$active) {
		$date = date('Y-m-d');
		
		try{
			$res_currency = $this->dbconnect->prepare("INSERT INTO tb_currency(code,description,symbol,rounding,homecurrency,active,creationdate) VALUES(?,?,?,?,?,?,?)");
			$res_currency->execute(array($code,$description,$symbol,$rounding,$homecurrency,$active,$date));

			echo json_encode(array('retcode'=>'000','retmsg'=>'Currency Created Successfully','results'=>''));			
		} catch (Exception $e) {
		  echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}			
	}

	//Edit Currency
	public function editCurrency($currencyid,$code,$description,$symbol,$rounding,$homecurrency,$active) {	
		try{
			$res_currency = $this->dbconnect->prepare("UPDATE tb_currency SET code=?,description=?,symbol=?,rounding=?,homecurrency=?,active=? WHERE currencyid=?");
			$res_currency->execute(array($code,$description,$symbol,$rounding,$homecurrency,$active,$currencyid));

			echo json_encode(array('retcode'=>'000','retmsg'=>'Currency Updated Successfully','results'=>''));			
		} catch (Exception $e) {
		  echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}			
	}

	//Get Savings Product
	public function getSavingsProduct($page,$rows,$all) {
		$result = array();
		if ($all==1){
		$sql="SELECT 
			productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active, 
			creationdate,activestate,termdeposit,minbalance,maxbalance,minage,maxage,minterm,maxterm,
			productcontrol,productcontroldesc,savingsint,savingsintdesc,savingstax,savingstaxdesc,contributions
			FROM v_getsavingsproducts";
		}else {
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*)	FROM v_getsavingsproducts";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();;
		$result["total"] = $row[0];
			
		$sql="SELECT 
			productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active, 
			creationdate,activestate,termdeposit,minbalance,maxbalance,minage,maxage,minterm,maxterm,
			productcontrol,productcontroldesc,savingsint,savingsintdesc,savingstax,savingstaxdesc,contributions
			FROM v_getsavingsproducts
			 limit $offset,$rows";
		}
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['productid']=$row['productid'];
				$node['code']=$row['code'];				
				$node['description']=$row['description'];				
				$node['producttypeid']=$row['producttypeid'];
				$node['producttype']=$row['producttype'];				
				$node['currencyid']=$row['currencyid'];
				$node['currency']=$row['currency'];					
				$node['currencycode']=$row['currencycode'];	
				$node['maxaccounts']=$row['maxaccounts'];
				$node['active']=$row['active'];				
				$node['activestate']=$row['activestate'];
				$node['creationdate']=$row['creationdate'];				
				$node['termdeposit']=$row['termdeposit'];	
				$node['minbalance']=$row['minbalance'];	
				$node['maxbalance']=$row['maxbalance'];
				$node['minage']=$row['minage'];	
				$node['maxage']=$row['maxage'];	
				$node['minterm']=$row['minterm'];	
				$node['maxterm']=$row['maxterm'];	
				
				$node['productcontrol']=$row['productcontrol'];	
				$node['productcontroldesc']=$row['productcontroldesc'];
				$node['savingsint']=$row['savingsint'];	
				$node['savingsintdesc']=$row['savingsintdesc'];	
				$node['savingstax']=$row['savingstax'];	
				$node['savingstaxdesc']=$row['savingstaxdesc'];
				$node['contributions']=json_decode($row['contributions'], true);				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No Product found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Savings Product
	public function addSavingsProduct($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$termdeposit,$minbalance,$maxbalance,$minage,$maxage,$minterm,$maxterm,$productcontrol,$savingsint,$savingstax,$contributions) {
		$date = date('Y-m-d');
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_savings = $this->dbconnect->prepare("INSERT INTO tb_products(code,description,producttypeid,currencyid,maxaccounts,active,creationdate) VALUES(?,?,?,?,?,?,?)");
			$res_savings->execute(array($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$date));
			
			$productid = $this->dbconnect->lastInsertId();
			
			$res_savings = $this->dbconnect->prepare("INSERT INTO tb_productshare(productid,termdeposit,minbalance,maxbalance,minage,maxage,minterm,maxterm,contributions) VALUES(?,?,?,?,?,?,?,?,?)");
			$res_savings->execute(array($productid,$termdeposit,$minbalance,$maxbalance,$minage,$maxage,$minterm,$maxterm,$contributions));
			
			$res_savings = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(productid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_savings->execute(array($productid,1,$productcontrol,$productid,4,$savingsint,$productid,6,$savingstax));
						
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Savings Product Created Successfully','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}			
	}

	//Edit Savings Product
	public function editSavingsProduct($productid,$code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$termdeposit,$minbalance,$maxbalance,$minage,$maxage,$minterm,$maxterm,$productcontrol,$savingsint,$savingstax,$contributions) {	
		$date = date('Y-m-d');
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_savings = $this->dbconnect->prepare("UPDATE tb_products SET code=?,description=?,producttypeid=?,currencyid=?,maxaccounts=?,active=?,editdate=? WHERE productid=?");
			$res_savings->execute(array($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$date,$productid));
			
			$res_savings = $this->dbconnect->prepare("UPDATE tb_productshare SET termdeposit=?,minbalance=?,maxbalance=?,minage=?,maxage=?,minterm=?,maxterm=?,contributions=? WHERE productid=?");
			$res_savings->execute(array($termdeposit,$minbalance,$maxbalance,$minage,$maxage,$minterm,$maxterm,$contributions,$productid));

			$res_savings = $this->dbconnect->prepare("DELETE FROM tb_ledgerlinks WHERE productid=?");
			$res_savings->execute(array($productid));
			
			$res_savings = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(productid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_savings->execute(array($productid,1,$productcontrol,$productid,4,$savingsint,$productid,6,$savingstax));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Savings Product Update Successfully','results'=>$maxbalance));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be updated','results'=>''));
		}			
	}

	//Get Fee
	public function getFee() {
		$result = array();
			
		$sql="SELECT feeid,description,shortname,feetype,feetypedesc,feeoption,feeoptiondesc,createdon,amount,feeincome,feeincomedesc,feeaccrual,feeaccrualdesc,active,isactive  FROM v_fee";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['feeid']=$row['feeid'];				
				$node['description']=$row['description'];				
				$node['shortname']=$row['shortname'];				
				$node['feetype']=$row['feetype'];
				$node['feetypedesc']=$row['feetypedesc'];
				$node['feeoption']=$row['feeoption'];
				$node['feeoptiondesc']=$row['feeoptiondesc'];				
				$node['createdon']=$row['createdon'];
				$node['amount']=$row['amount'];				
				$node['feeincome']=$row['feeincome'];
				$node['feeincomedesc']=$row['feeincomedesc'];
				$node['feeaccrual']=$row['feeaccrual']; 
				$node['feeaccrualdesc']=$row['feeaccrualdesc'];
				$node['active']=$row['active'];
				$node['isactive']=$row['isactive'];
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No fee found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Search Fee
	public function searchFee() {
		$result = array();
			
		$sql="SELECT feeid,description FROM v_fee WHERE active = 1";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['feeid']=$row['feeid'];				
				$node['description']=$row['description'];				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No fee found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}	
	
	//Add Fee
	public function addFee($description,$shortname,$feetype,$feeoption,$amount,$active,$feeincome,$feeaccrual) {
		$date = date('Y-m-d');
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_fee = $this->dbconnect->prepare("INSERT INTO tb_fee(description,shortname,feetype,feeoption,createdon,amount,active)VALUES(?,?,?,?,?,?,?)");
			$res_fee->execute(array($description,$shortname,$feetype,$feeoption,$date,$amount,$active));
			
			$feeid = $this->dbconnect->lastInsertId();
			
			$res_fee = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(feeid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_fee->execute(array($feeid,2,$feeincome,$feeid,3,$feeaccrual));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Fee Created Successfully','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}			
	}

	//edit Fee
	public function editFee($feeid,$description,$shortname,$feetype,$feeoption,$amount,$active,$feeincome,$feeaccrual) {
		$date = date('Y-m-d');
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_fee = $this->dbconnect->prepare("UPDATE tb_fee SET description=?,shortname=?,feetype=?,feeoption=?,amount=?,active=? WHERE feeid=?");
			$res_fee->execute(array($description,$shortname,$feetype,$feeoption,$amount,$active,$feeid));
			
			$res_fee = $this->dbconnect->prepare("DELETE FROM tb_ledgerlinks WHERE feeid=?");
			$res_fee->execute(array($feeid));
			
			$res_fee = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(feeid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_fee->execute(array($feeid,2,$feeincome,$feeid,3,$feeaccrual));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Fee Updated Successfully','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be edited','results'=>''));
		}			
	}
	
	//Get Ledger Categories
	public function getLedgerCategory() {
		$result = array();
			
		$sql="SELECT categoryid,description FROM tb_ledgercategory";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['categoryid']=$row['categoryid'];				
				$node['description']=$row['description'];				
								
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Ledger Categories
	public function getLedgerSubCategory($categoryid) {
		$result = array();
			
		$sql="SELECT formatid,description FROM tb_ledgerformat WHERE parentid IS NULL AND categoryid=$categoryid ORDER BY formatid";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['formatid']=$row['formatid'];				
				$node['description']=$row['description'];				
								
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}	
	
	//Get Ledger Categories Parent
	public function getLedgerSubCategoryParent($parentid) {
		$result = array();
			
		$sql="SELECT formatid,description FROM tb_ledgerformat WHERE parentid IS NOT NULL AND parentid=$parentid ORDER BY formatid";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['formatid']=$row['formatid'];				
				$node['description']=$row['description'];				
								
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}	

	//Get Ledger Accounts
    public function getLedgerAccounts($accountid,$page,$rows) {	
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*) FROM v_ledgeraccounts WHERE (accountid=$accountid OR $accountid=0)";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
		
		
		$sql="SELECT branchid,branchname,accountid,number,description,dramount,cramount,balancedate,categoryid,
		categoryname,financialcategoryid,financialcategory,subfinancialcategoryid,subfinancialcategory,active,activestate FROM v_ledgeraccounts 
		WHERE (accountid=$accountid OR $accountid=0) limit $offset,$rows";
			
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();				
			
			foreach($res as $row) {
				$node=array();
				$node['branchid']=$row['branchid'];				
				$node['branchname']=$row['branchname'];				
				$node['accountid']=$row['accountid'];				
				$node['number']=$row['number'];
				$node['description']=$row['description'];				
				$node['dramount']=$row['dramount'];				
				$node['cramount']=$row['cramount'];				
				$node['balancedate']=$row['balancedate'];
				$node['categoryid']=$row['categoryid'];				
				$node['categoryname']=$row['categoryname'];
				$node['financialcategoryid']=$row['financialcategoryid'];				
				$node['financialcategory']=$row['financialcategory'];
				$node['subfinancialcategoryid']=$row['subfinancialcategoryid'];				
				$node['subfinancialcategory']=$row['subfinancialcategory'];
				$node['active']=$row['active'];
				$node['activestate']=$row['activestate'];
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No branch found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Get Ledger Account Search
	public function getLedgerAccountSearch($active,$istransactionposting,$categoryid) {
		$result = array();
		if ($istransactionposting == 1){//Restrict posting to system accounts
			$sql="SELECT accountid,CONCAT(number,' - ',branchid,' : ',description) AS accountname 
			FROM v_ledgeraccounts 
			WHERE (active = $active OR $active=0)
			AND (categoryid = $categoryid OR $categoryid=0)
			AND accountid NOT IN (SELECT DISTINCT accountid FROM tb_ledgerlinks WHERE (productid IS NOT NULL OR feeid IS NOT NULL))";		
		}else{
			$sql="SELECT accountid,CONCAT(number,' - ',branchid,' : ',description) AS accountname FROM v_ledgeraccounts WHERE (active = $active OR $active=0) AND (categoryid = $categoryid OR $categoryid=0)";
		}

		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['accountid']=$row['accountid'];				
				$node['accountname']=$row['accountname'];				
								
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Ledger Account
	public function addLedgerAccount($description,$categoryid,$formatid,$subformatid,$branchid,$active,$createdby) {
		$date = date('Y-m-d');
					
		$sql = "INSERT INTO tb_ledgeraccounts(number,description,categoryid,formatid,subformatid,branchid,dramount,cramount,balancedate,active,createdon,createdby)
		SELECT CONCAT(ledgeraccountcode,(SELECT LPAD(COUNT(accountid)+1,3,'0') FROM tb_ledgeraccounts WHERE formatid=$formatid AND branchid=$branchid)),'$description',$categoryid,$formatid,$subformatid,$branchid,0,0,'$date',$active,'$date',$createdby FROM tb_ledgerformat WHERE formatid=$formatid;";
		
		if($res=$this->dbconnect->query($sql)){
			echo json_encode(array('retcode'=>'000','retmsg'=>'Ledger Account Created Successfully','results'=>''));
		} else {
		echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}		
	}	


	//Add Ledger Account
	public function editLedgerAccount($accountid,$description,$categoryid,$formatid,$subformatid,$branchid,$active) {
		$date = date('Y-m-d');
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_LedgerAccount = $this->dbconnect->prepare("UPDATE tb_ledgeraccounts SET description=?,categoryid=?,formatid=?,subformatid=?,branchid=?,active=? WHERE accountid=?");
			$res_LedgerAccount->execute(array($description,$categoryid,$formatid,$subformatid,$branchid,$active,$accountid));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Ledger Account Updated Successfully','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>''));
		}			
	}	

	public function getLedgerAccountByType($categoryid) {
		$result = array();
		if ($categoryid == 0){
			$sql="SELECT accountid,CONCAT(number,' - ',branchid,' : ',description) AS accountname FROM v_ledgeraccounts WHERE active = 1";
		} else {
			$sql="SELECT accountid,CONCAT(number,' - ',branchid,' : ',description) AS accountname FROM v_ledgeraccounts WHERE categoryid=$categoryid AND active = 1";
		}
				
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['accountid']=$row['accountid'];				
				$node['accountname']=$row['accountname'];				
								
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Customer Accounts
    public function getCustomerAccount($page,$rows,$idno) {	
		$result = array();
		
		$offset = ($page-1)*$rows;
		$cond = "";
                if($idno != "")
                {
                    $cond .= " idno='$idno' ";
                }
               
		$sql_count="SELECT COUNT(*) FROM v_customeraccountdetails 
		WHERE 
		$cond
		AND active=1";
                //var_dump($sql_count);die;
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
		
		
		$sql="SELECT accountid,accountnumber,branchname,customername,customerid,productid,productname,balance,
		creationdate,producttypeid,producttype,idno,idtype,idtypedesc,active,activestate 
		FROM v_customeraccountdetails 
		WHERE 
		$cond 
		AND active=1 ORDER BY productid,accountid
		limit $offset,$rows";
			
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();				
			
			foreach($res as $row) {
				$node=array();			
				$node['branchname']=$row['branchname'];			
				$node['accountid']=$row['accountid'];
				$node['accountnumber']=$row['accountnumber'];				
				$node['customername']=$row['customername'];
				$node['customerid']=$row['customerid'];				
				$node['productid']=$row['productid'];				
				$node['productname']=$row['productname'];				
				$node['balance']=$row['balance'];
				$node['creationdate']=$row['creationdate'];				
				$node['producttypeid']=$row['producttypeid'];
				$node['producttype']=$row['producttype'];
				$node['idno']=$row['idno'];				
				$node['idtype']=$row['idtype'];
				$node['idtypedesc']=$row['idtypedesc'];				
				$node['active']=$row['active'];
				$node['activestate']=$row['activestate'];
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No customer Accounts found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Get Customer Accounts
    public function getCustomerAccountsByType($customerid,$idno,$accountno,$producttypeid) {	
		$result = array();

		$sql="SELECT accountid,accountnumber,branchname,customername,customerid,productid,productname,balance,
		creationdate,producttypeid,producttype,idno,idtype,idtypedesc,active,activestate 
		FROM v_customeraccountdetails 
		WHERE (idno=$idno OR $idno=0) 
		AND (customerid=$customerid OR $customerid=0)
		AND (accountnumber=$accountno OR $accountno=0)
		AND (producttypeid = $producttypeid OR $producttypeid=0) ";
			
		if($res=$this->dbconnect->query($sql)){

		foreach($res as $row) {
				$node=array();			
				$node['branchname']=$row['branchname'];			
				$node['accountid']=$row['accountid'];
				$node['accountnumber']=$row['accountnumber'];				
				$node['customername']=$row['customername'];
				$node['customerid']=$row['customerid'];				
				$node['productid']=$row['productid'];				
				$node['productname']=$row['productname'];				
				$node['balance']=$row['balance'];
				$node['creationdate']=$row['creationdate'];				
				$node['producttypeid']=$row['producttypeid'];
				$node['producttype']=$row['producttype'];
				$node['idno']=$row['idno'];				
				$node['idtype']=$row['idtype'];
				$node['idtypedesc']=$row['idtypedesc'];				
				$node['active']=$row['active'];
				$node['activestate']=$row['activestate'];
				$node['accountname']=$row['accountnumber'].' - '.$row['productname'];
				
				array_push($result, $node);
			}
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No customer Accounts found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Get Customer Search
    public function getCustomerSearch($page,$rows,$customerid,$idno) {	
		$result = array();
		 
		$offset = ($page-1)*$rows;
                $cond = "";
                if($_SESSION['profileid'] ==3)
                {
                   $cond .=  " idno=".$_SESSION['idno'];
                }
                else {
                if($idno != "")
                    $cond .= " (idno=$idno ) ";
                else
                    $cond .=  " 1=1 ";
		if($customerid != "")
                    $cond .= " AND (mobileno=$customerid) ";
                }
		$sql_count="SELECT COUNT(*) As num FROM v_customerdetails
		WHERE" . $cond;
				
		$rs=$this->dbconnect->query($sql_count);
		foreach($rs as $row) {
			$result["total"] = $row['num'];
		}
		
		$sql="SELECT *
		FROM v_customerdetails 
		WHERE ".
		$cond. " limit $offset,$rows";
        	
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();				

			foreach($res as $row) {
				$node=array();			
				$node['branchname']=$row['branchname'];				
				$node['branchid']=$row['branchid'];				
				$node['name']=$row['name'];
				$node['customerid']=$row['customerid'];				
				$node['mobileno']=$row['mobileno'];				
				$node['customertype']=$row['customertype'];				
				$node['customertypedesc']=$row['customertypedesc'];				
				$node['loanofficerid']=$row['loanofficerid'];
				$node['loanofficername']=$row['loanofficername'];				
				$node['idno']=$row['idno'];				
				$node['idtype']=$row['idtype'];
				$node['idtypedesc']=$row['idtypedesc'];				
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No customer Accounts found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
        //Get Customer Loan Applications
    public function getCustomerLoanApplications($page,$rows,$mobileno,$idno) {	
		$result = array();
		 
		$offset = ($page-1)*$rows;
                $cond = "loanstatus = 1";
               
                if($idno != "")
                    $cond .= "AND (idno=$idno ) ";
                
		if($mobileno != "")
                    $cond .= " AND (mobileno=$mobileno) ";
                
		$sql_count="SELECT COUNT(loanid) As num FROM v_newloans WHERE " . $cond;
		$rs=$this->dbconnect->query($sql_count);
		foreach($rs as $row) {
			$result["total"] = $row['num'];
		}
		
		$sql="SELECT * FROM v_newloans 	WHERE $cond limit $offset,$rows";
        	
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();				

			foreach($res as $row) {
				$node=array();			
				$node['loanid']=$row['loanid'];				
				$node['accountid']=$row['accountid'];				
				$node['accountnumber']=$row['accountnumber'];				
				$node['amount']=$row['amount'];
				$node['applicationdate']=$row['applicationdate'];				
				$node['applicationnote']=$row['applicationnote'];				
				$node['expecteddisbursementdate']=$row['expecteddisbursementdate'];				
				$node['loanstatus']=$row['loanstatus'];				
				$node['bank']=$row['bank'];
				$node['bankbranch']=$row['bankbranch'];
				$node['accountname']=$row['accountname'];
				$node['bankaccountnumber']=$row['bankaccountnumber'];
				$node['biztown']=$row['biztown'];
				$node['bizbuilding']=$row['bizbuilding'];
				$node['shopno']=$row['shopno'];
				$node['landmark']=$row['landmark'];
				$node['knowus']=$row['knowus'];
				$node['guarantor_upload']=$row['guarantor_upload'];
				$node['employer_upload']=$row['employer_upload'];
				$node['bus_assessment_upload']=$row['bus_assessment_upload'];
				$node['customerid']=$row['customerid'];				
				$node['customertypedesc']=$row['customertypedesc'];				
				$node['name']=$row['name'];				
				$node['drAmount']=$row['drAmount'];
				$node['balancedate']=$row['balancedate'];				
				$node['crAmount']=$row['crAmount'];				
				$node['crAmount']=$row['crAmount'];				
				$node['idno']=$row['idno'];				
				$node['mobileno']=$row['mobileno'];				
				$node['loanofficer']=$row['loanofficer'];				
				$node['loanofficerid']=$row['loanofficerid'];				
				$node['loanproduct']=$row['loanproduct'];				
				$node['address']=$row['address'];				
				$node['town']=$row['town'];				
				$node['postcode']=$row['postcode'];				
				$node['area']=$row['area'];				
				$node['street']=$row['street'];				
				$node['houseno']=$row['houseno'];				
				$node['county']=$row['county'];				
				$node['country']=$row['country'];				
				$node['physicallocation']=$row['physicallocation'];
                                // Employer details
                                $node['empname']=$row['empname'];
                                $node['phone']=$row['phone'];
                                $node['email']=$row['email'];
                                $node['profile_image']=$row['profile_image'];
                                $node['empaddress']=$row['empaddress'];
                                $node['building']=$row['building'];
                                $node['employmentterms']=$row['employmentterms'];
                                $node['grosssalary']=$row['grosssalary'];
                                $node['netsalary']=$row['netsalary'];
                                $node['designation']=$row['designation'];
                                $node['department']=$row['department'];
                                $node['dateofemployment']=$row['dateofemployment'];
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No customer Accounts found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
        

	//Add Savings Account
	public function addSavingsAccount($customerid,$productid,$openingbalance) {	
	//Validation
	$noofaccounts = 0;
	$maxaccounts = 0;
	
	//Get no of accounts customer have in this product
	$sql_noofaccounts ="SELECT COUNT(*) AS noofaccounts FROM tb_products INNER JOIN tb_accounts ON tb_products.productid = tb_accounts.productid 
	WHERE tb_accounts.productid = $productid AND tb_accounts.customerid = $customerid AND tb_products.active =1";
	if($res=$this->dbconnect->query($sql_noofaccounts)){
		foreach($res as $row) {
			$noofaccounts = $row['noofaccounts'];
		}
	}
	//Get Product settings
	$sql_productsettings="SELECT maxaccounts FROM tb_products WHERE productid = $productid AND active =1";		
	if($res=$this->dbconnect->query($sql_productsettings)){
		foreach($res as $row) {		
			$maxaccounts = $row['maxaccounts'];							
		}
	}
	
	if ($noofaccounts < $maxaccounts) {
	try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$date = date('Y-m-d');
								
			$res_account = $this->dbconnect->prepare("INSERT INTO tb_accounts(customerid,productid,drAmount,crAmount,accountstatus,balancedate,active,creationdate) VALUES(?,?,?,?,?,?,?,?)"); 			
			$res_account->execute( array($customerid,$productid,0,0,1,$date,1,$date));
						
			$accountid = $this->dbconnect->lastInsertId();
			$accountnumber = substr("0000000000".$accountid, -8);
			
			$res_account = $this->dbconnect->prepare("UPDATE tb_accounts SET accountnumber=? WHERE accountid=?"); 			
			$res_account->execute( array($accountnumber,$accountid));
			
			
			
			$this->dbconnect->commit();
                        if ($openingbalance != 0)
			{
			 $this->saveAmount($accountid,'',$openingbalance,'Deposit',$_SESSION['userid'],'Opening Balance',date('Y-m-d'),$_SESSION['cashieraccountid']);
			}
		  	$retcode='000';
			$retmsg='Account Created Successfully';
			$results=$accountid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
	} else {
		  	$retcode='003';
			$retmsg='Customer have reached maximun number of accounts allowed in this product';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}	
	
	}
        
        //Post transactions
	public function saveAmount($accountid,$feeid,$amount,$trxtype,$userid,$trxdescription,$valuedate,$usercontraaccount){		
	try{
				
			$glcontrol = 0;
			$sql_checkglcontrol="";
			
			 							
				$sql_checkglcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
										INNER JOIN tb_ledgeraccounts
										ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
										WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $accountid)
										AND  gltype = 1";				
			
			if($res=$this->dbconnect->query($sql_checkglcontrol)){
				
					foreach($res as $row) {		
						$glcontrol = $row['accountid'];							
				}
			} 		
			
				$iscredit = 1;
				$transactiontype = 1;
				$debitaccid = $usercontraaccount;
				$creditaccid = $glcontrol;				
			
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$serverdate = date('Y-m-d');

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
			$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
			
			$receiptno = $this->dbconnect->lastInsertId();	
			
			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array($receiptno,$valuedate,$accountid,$feeid,$iscredit,$amount,$transactiontype,$debitaccid,$creditaccid));
			
			

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
			
			$ledgerid = $this->dbconnect->lastInsertId();

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
			$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$amount));			

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
			$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$amount));			
			
				if($iscredit == 0){
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
					$res_posttrx->execute( array($amount,$accountid));
				} else {
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
					$res_posttrx->execute( array($amount,$accountid));		
				}
			
			$this->dbconnect->commit();
		  
		  	return true;
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}	

	//Get interest due
	/*
	public function loaninterestdue($accountid,$date){
		//Get Loan details
		$productid = 0;
		$loanid = 0;
		$rate = 0;
		$disbursedon =0;
		$interestcalculationmethod = 0;
		$interestdue = 0;
		$interestbalance = 0;
		$prevrepaymentdate = 0;
		$interestdays = 0;
		$loanbalance = 0; 
		$interestpaid = 0; 
		
		$sql_loandetails = $this->dbconnect->prepare("SELECT productid,loanid,rate,disbursedon,fn_accountbalanceatdate(accountid,?) AS pbalance FROM tb_loans WHERE accountid = ?");			
		$sql_loandetails->execute(array($date,$accountid));
		$results = $sql_loandetails->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)) {
			$productid = $results[0]['productid'];
			$loanid =  $results[0]['loanid'];
			$rate =  $results[0]['rate'];
			$disbursedon = $results[0]['disbursedon'];
			$loanbalance = $results[0]['pbalance']; 
		}

		$sql_interestmethod = $this->dbconnect->prepare("SELECT COALESCE(IF(INSTR(settings,'interestcalculationmethod'), SUBSTRING(settings,INSTR(settings,'interestcalculationmethod')+29,1),0),0) AS interestcalculationmethod FROM tb_productloan WHERE productid = ?;");			
		$sql_interestmethod->execute(array($productid));
		$results = $sql_interestmethod->fetchAll(PDO::FETCH_ASSOC);		

		if (is_array($results)){
			$interestcalculationmethod = $results[0]['interestcalculationmethod'];
		}

		if(($interestcalculationmethod == 1) || ($interestcalculationmethod == 3)){
			$sql_interestdue = $this->dbconnect->prepare("SELECT
		   ((SELECT COALESCE(SUM(interest),0) FROM tb_loanschedule WHERE loanid = _loanid AND repaymentdate <= ?)-
		   (SELECT COALESCE(SUM(amount),0) FROM tb_transactions WHERE transactiontype = 8 AND valuedate <= ? AND accountid = ?)) AS interestdue;");			
			$sql_interestdue->execute(array($date,$date,$accountid));			
			$results = $sql_interestdue->fetchAll(PDO::FETCH_ASSOC);
			
			if (is_array($results)){
				$interestdue = $results[0]['interestdue'];
			}
		}elseif($interestcalculationmethod == 2) { 
			$sql_interestdue = $this->dbconnect->prepare("SELECT (fn_accountbalanceatdate(accountid,valuedate)+amount) as amount,valuedate 
			FROM tb_transactions 	
			WHERE accountid = ?
			AND valuedate <= ?
			AND transactiontype = 6 ORDER BY valuedate;");			
			$sql_interestdue->execute(array($accountid,$date));			
			$results = $sql_interestdue->fetchAll(PDO::FETCH_ASSOC);
			
			$prevrepaymentdate = $disbursedon;
			
			if (is_array($results)){
				foreach($results as $row) {
					$date1 = new DateTime(date('Y-m-d', strtotime($prevrepaymentdate)));
					$date2 = new DateTime(date('Y-m-d', strtotime($row['valuedate'])));
					$interestdays = $date1->diff($date2);
					$interestdays = if($interestdays <= 0,0,$interestdays);

					$interestbalance =+ (($row['amount']*$rate) / ( 365 * 100 ))*$interestdays ;
					$prevrepaymentdate = $date2;
				}
				
				$date1 = new DateTime(date('Y-m-d', strtotime($prevrepaymentdate)));
				$date2 = new DateTime(date('Y-m-d', strtotime($date)));
				$interestdays = $date1->diff($date2);
				$interestdays = if($interestdays <= 0,0,$interestdays);

				$interestbalance =+ (($loanbalance*$rate) / ( 365 * 100 ))*$interestdays ;				
			}

			$sql_interestpaid = $this->dbconnect->prepare("SELECT COALESCE(SUM(amount),0) AS interestpaid FROM tb_transactions WHERE transactiontype = 8 AND valuedate <= ? AND accountid = ?;");			
			$sql_interestpaid->execute(array($date,$accountid));			
			$results = $sql_interestpaid->fetchAll(PDO::FETCH_ASSOC);
			
			if (is_array($results)){
				$interestpaid = $results[0]['interestpaid'];;
			}
			
			$interestdue = $interestbalance - $interestpaid;
		}
		
		return $interestdue;
	}
	*/
	//Get Loan balance details
	public function loanbalancedetails($accountid,$valuedate,$print){
		$sql_loandetails = $this->dbconnect->prepare("SELECT fn_getinterestdue(?,?) AS interestdue,fn_getprincipaldue(?,?) AS principaldue,fn_getprincipalbalance(?,?) AS principalbalance,fn_getinterestbalance(?,?) AS interestbalance,
			COALESCE((fn_getinterestbalance(?,?)+fn_getprincipalbalance(?,?)),0) AS payoffamount,0 AS arrearsamount,0 AS arrearsdays");
			
		$sql_loandetails->execute(array($accountid,$valuedate,$accountid,$valuedate,$accountid,$valuedate,$accountid,$valuedate,$accountid,$valuedate,$accountid,$valuedate));
		$results = $sql_loandetails->fetchAll(PDO::FETCH_ASSOC);
		
		$loandetails = array();
		if (is_array($results)) {
			foreach($results as $row) {
				if($print === 1){
					$item = array();
					$item['interestdue']= $this->formatMoney($row['interestdue'], true);
					$item['principaldue']= $this->formatMoney($row['principaldue'], true);
					$item['principalbalance']= $this->formatMoney($row['principalbalance'], true);
					$item['interestbalance']= $this->formatMoney($row['interestbalance'], true);
					$item['payoffamount']= $this->formatMoney($row['payoffamount'], true);
					$item['arrearsamount']= $this->formatMoney($row['arrearsamount'], true);
					$item['arrearsdays']= $this->formatMoney($row['arrearsdays'], true);
												
					array_push($loandetails, $item);
				}else {
					$item = array();
					$item['interestdue']= $row['interestdue'];
					$item['principaldue']= $row['principaldue'];
					$item['principalbalance']= $row['principalbalance'];
					$item['interestbalance']= $row['interestbalance'];
					$item['payoffamount']= $row['payoffamount'];
					$item['arrearsamount']=$row['arrearsamount'];
					$item['arrearsdays']= $row['arrearsdays'];
												
					array_push($loandetails, $item);			
				}			
			}			
		}
		if($print === 1){
			echo json_encode($loandetails);
		}else {
			return $loandetails;
		}
	}

	//Post Loan Repayment - When making changed to loan posting also check transfer posting
	public function postLoanRepayment($accountid,$amount,$userid,$trxdescription,$valuedate,$usercontraaccount) {		
	try{	
			//Check if period is active
			$sql_checkperiod="SELECT * FROM tb_period WHERE '".$valuedate."' BETWEEN startdate AND enddate";
			if($res=$this->dbconnect->query($sql_checkperiod)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
				return;			
			}

			//Check for backdating of loan repayment
			$sql_checkbackdating="SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0),receiptno FROM tb_transactions WHERE  accountid = $accountid AND valuedate > '".$valuedate."' GROUP BY receiptno HAVING  COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0)<>0";
			if($res=$this->dbconnect->query($sql_checkbackdating)){
				if($res->rowCount()>0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot backdate loan repayment transaction. Reverse previous payments first.','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot backdate loan repayment transaction. Reverse previous payments first.','results'=>''));
				return;			
			}			
			
			/* Check if account exists   */
			$sql_checkaccount="SELECT tb_loans.* FROM tb_accounts INNER JOIN tb_loans ON tb_loans.accountid = tb_accounts.accountid WHERE tb_loans.accountid = $accountid AND tb_accounts.active =1";
			
			if($res=$this->dbconnect->query($sql_checkaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
					return;
				}else{
					foreach($res as $res_rows){
						if( $res_rows['loanstatus']<>3){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Loan. Check if loan disbursed and active','results'=>''));
							return;	
						}						
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
				return;			
			}
			
		    // Check if user exists   
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}

		    /* Check user contra account */
			$sql_checkusercontraaccount="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
					INNER JOIN tb_ledgeraccounts
					ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
					WHERE userid = $userid
					AND tb_ledgerlinks.accountid = $usercontraaccount
					AND gltype = 8 ";
			
			if($res=$this->dbconnect->query($sql_checkusercontraaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
				return;			
			}
			
			//Get loan balance details 
			$sql_updateschedule = '';
			$interestdue = 0;
			$principaldue = 0;
			$principalbalance = 0;
			$payoffamount = 0;
			$arrearsamount = 0;
			$arrearsdays = 0;
			
			$interestamount = 0;
			$principalamount = 0;
			
			$excessamount = 0;
			$excessamount_account = 0;
			$excessamount_glcontrol = 0;
			
			$closeloan = 0;
			
			$loanbalancedetails = $this->loanbalancedetails($accountid,$valuedate,0);

			if (is_array($loanbalancedetails)) {
				if(!empty($loanbalancedetails)){
					foreach($loanbalancedetails as $row){
						$interestdue = $row['interestdue'];
						$principaldue = $row['principaldue'];
						$principalbalance = $row['principalbalance'];
						$payoffamount = $row['payoffamount'];
						$arrearsamount = $row['arrearsamount'];
						$arrearsdays = $row['arrearsdays'];
					}
				}else{
					echo json_encode(array('retcode'=>'003','retmsg'=>'Loan balance details not found','results'=>''));
					return;					
				}
			}else{
				echo json_encode(array('retcode'=>'003','retmsg'=>'Loan balance details not found','results'=>''));
				return;					
			}

			//Distribute Amount Paid
			if($amount > 0){
				//Check if there is an overpayment
				if($amount >= $payoffamount){
					$closeloan = 1;
				}
				
				if($amount > $payoffamount){
					$excessamount = $amount - $payoffamount;
					$amount = $amount - $excessamount;
				}
				
				//Default interest calc method to 0
				$interestcalculationmethod = 0;
				$accrueinterest = 0;
				
				//Check Loan Interest Calculation Method
				$sql_product_settings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
					settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
					FROM v_getloanproducts WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = ?)");
				if($sql_product_settings->execute(array($accountid)))
				{					
					$productsettings = $sql_product_settings->fetchAll(PDO::FETCH_ASSOC);
					
					if(!empty($productsettings)){		
						$product_settings  = json_decode($productsettings[0]["settings"], true);		
						
						$interestcalculationmethod = $product_settings['interestcalculationmethod'];
						$accrueinterest = $product_settings['accrueinterest'];						
					}
				}else{
					echo json_encode(array('retcode'=>'003','retmsg'=>'Cannot get product setting. Contact system support','results'=>''));
					return;		
				}
			
				//NB: Interest calc method 2 don't use schedule all other use schedule to get interest to be paid
				if($interestcalculationmethod == 2)
				{					
					//Deal with interest first
					if($amount <= $interestdue){
						$interestamount = $amount;
						$amount = 0;
					}else{
						$interestamount = $interestdue;
						$amount = $amount - $interestdue;
					}
					
					//then deal with principal				
					$principalamount = $amount;							
				}else{
					//Deal with interest first					
					$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
					if($sql_loan_schedule->execute(array($accountid)))
					{							
						$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
						foreach($loanschedule as $row){
							$val_principal = $row['principal'];
							$val_interest = $row['interest'];
							$val_principalpaid = $row['principalpaid'];
							$val_interestpaid = $row['interestpaid'];							
							$val_loanscheduleid = $row['loanscheduleid'];
							$val_loanid = $row['loanid'];
							
							$principalpaid = 0;
							$interestpaid = 0;
							
							//Check for partial payments
							//Interest
							if($val_interestpaid > 0){
								$val_interest = $val_interest - $val_interestpaid;
							}
							//Principal
							if($val_principalpaid > 0)
							{
								$val_principal = $val_principal - $val_principalpaid;
							}
							
							
							if($amount > 0)
							{
								if($amount <= $val_interest)
								{
									$principalpaid = 0;
									$interestpaid = $amount;
									
									$interestamount = $interestamount + $interestpaid;
									$principalamount = $principalamount + $principalpaid;
									$amount = $amount - $interestpaid;									
								}else{
									$interestpaid = $val_interest;
									$amount = $amount - $interestpaid;
									
									if($amount <= $val_principal){
										$principalpaid = $amount;
										$amount = $amount - $principalpaid;										
									}else{
										$principalpaid = $val_principal;
										$amount = $amount - $principalpaid;
									}									
									
									$principalamount = $principalamount + $principalpaid;
									$interestamount = $interestamount + $interestpaid;									
								}
								
								$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
							}							
						}
						//Testing
						//echo $sql_updateschedule.' - Interest Amount : '.$interestamount.' - Principal Amount : '.$principalamount.' - Excess Amount : '.$excessamount;
						//return;
					}else{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Unable to get loan schedule ','results'=>''));
						return;		
					}															
				}									
			}else{
				echo json_encode(array('retcode'=>'003','retmsg'=>'Amount paid should be greater than 0 (zero)','results'=>''));
				return;					
			}			
			/*
			if(strlen($sql_updateschedule) > 0){
				$res_update_schedule = $this->dbconnect->prepare($sql_updateschedule); 			
				$res_update_schedule->execute();	
				echo 'true';	
				return;
			}
			echo 'false';
			return;
			*/
			/* Get GL control of the product */	
			$glcontrol = 0;
			$interestincome = 0;
			$interestreceivable = 0;
			
			$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
					INNER JOIN tb_ledgeraccounts
					ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
					WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $accountid)";	

			if($res=$this->dbconnect->query($sql_gllinks)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;
				}else{
					foreach($res as $row) {	
						if($row['gltype'] == 1){
							$glcontrol = $row['accountid'];	
						}
						if($row['gltype'] == 9){
							$interestincome = $row['accountid'];	
						}
						if($row['gltype'] == 10){
							$interestreceivable = $row['accountid'];	
						}
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
				return;			
			}

			if(($glcontrol <= 0) || ($interestincome <= 0) || ($interestreceivable <= 0)){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL settings for the loan product','results'=>''));
				return;	
			}

			if($excessamount > 0){
				/* Check for deposit account exists   */
				$sql_excess_checkaccount="SELECT * FROM tb_accounts WHERE customerid IN (SELECT customerid FROM tb_accounts WHERE accountid = $accountid) AND productid = 2"; 
				
				if($res=$this->dbconnect->query($sql_excess_checkaccount)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
						return;
					}else{
						foreach($res as $row){
							$excessamount_account = $row['accountid'];
						}
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
					return;			
				}	

				$sql_excess_checkglcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
										INNER JOIN tb_ledgeraccounts
										ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
										WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $excessamount_account)
										AND  gltype = 1";				
										
				if($res=$this->dbconnect->query($sql_excess_checkglcontrol)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
						return;
					}else{
						foreach($res as $row) {		
							$excessamount_glcontrol = $row['accountid'];							
						}					
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;			
				}				
			}	
			
			$serverdate = date('Y-m-d');
			$ledgerid = 0;

			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
			$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
			
			$receiptno = $this->dbconnect->lastInsertId();	
			//Post Principal 
			if($principalamount > 0){
				$debitaccid = $usercontraaccount;
				$creditaccid = $glcontrol;
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,223,0,1,$principalamount,6,$debitaccid,$creditaccid));
				
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
				$res_posttrx->execute( array($principalamount,$accountid));

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
				
				$ledgerid = $this->dbconnect->lastInsertId();

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$principalamount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$principalamount));	
			}			
						
			if($interestamount > 0){
				//Post Interest Charged against loan account (but it should not affect loan balance) ---> DR - Interest receivable CR - Interest Income
				if ($accrueinterest != "1"){//Check if interest charged should be posted to the system when it is due
					$debitaccid = $interestreceivable;
					$creditaccid = $interestincome;	
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount,7,$debitaccid,$creditaccid));
					
					if($ledgerid == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
						
						$ledgerid = $this->dbconnect->lastInsertId();				
					}
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));				
				}
				
				//Post Interest paid against loan account (but it should not affect loan balance) ---> DR - Source of payment e.g. bank\cash\mpesa CR - Interest receivable
				$debitaccid = $usercontraaccount;
				$creditaccid = $interestreceivable;	
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,1,$interestamount,8,$debitaccid,$creditaccid));

				if($ledgerid == 0){
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
					
					$ledgerid = $this->dbconnect->lastInsertId();				
				}
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));		
			}	
			//Post excess payment to savings account. For now it will be posted in to deposit account
			//This should be in the settings page
			if($excessamount > 0){
				$debitaccid = $usercontraaccount;
				$creditaccid = $excessamount_glcontrol;	

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$excessamount_account,0,1,$excessamount,1,$debitaccid,$creditaccid));
				
				if($ledgerid == 0){
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
					
					$ledgerid = $this->dbconnect->lastInsertId();				
				}
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$excessamount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$excessamount));							
			}
			if($closeloan == 1){
				//close loan
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus=5 WHERE accountid = ?"); 			
				$res_posttrx->execute( array($accountid));	
				
				//close loan account
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET accountstatus=7,active=0 WHERE accountid = ?"); 			
				$res_posttrx->execute( array($accountid));				
			}
						
			$this->dbconnect->commit();		  
		  	$retcode='000';
			$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
			$results=$accountid;
			
			if(strlen($sql_updateschedule) > 0){
				$res_update_schedule = $this->dbconnect->prepare($sql_updateschedule); 			
				$res_update_schedule->execute();					
			}
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}				
	}	
	
	//Post transactions
	public function postTransaction($accountid,$feeid,$amount,$trxtype,$userid,$trxdescription,$valuedate,$usercontraaccount){		
	try{
			//Check if period is active
			$sql_checkperiod="SELECT * FROM tb_period WHERE '".$valuedate."' BETWEEN startdate AND enddate";
			if($res=$this->dbconnect->query($sql_checkperiod)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
				return;			
			}	
			
			//Validations first before transaction posting
			$trxtype=trim($trxtype);
			$transactiontype = array("Deposit", "Withdraw", "FeePayment");
			
			if(in_array($trxtype,$transactiontype) == false){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Transaction type','results'=>$trxtype));
				return;
			}
			/* Check if account exists   */
			$sql_checkaccount="SELECT * FROM tb_accounts WHERE accountid = $accountid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
				return;			
			}
			
		    /* Check if user exists   */
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}

		    /* Check user contra account */
			$sql_checkusercontraaccount="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
					INNER JOIN tb_ledgeraccounts
					ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
					WHERE userid = $userid
					AND tb_ledgerlinks.accountid = $usercontraaccount
					AND gltype = 8 ";
			
			if($res=$this->dbconnect->query($sql_checkusercontraaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
				return;			
			}			
			/* Check if transaction amount */
			if($amount <= 0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid transaction amount. Amount cannot be less or equal to zero','results'=>''));
				return;	
			}
			/* Get GL control of the product */	
			$glcontrol = 0;
			$sql_checkglcontrol="";
			
			if($trxtype == 'FeePayment'){
				
				$sql_checkfee="SELECT * FROM tb_fee WHERE feeid = $feeid AND active =1";
				
				if($res=$this->dbconnect->query($sql_checkfee)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid fee. Check if fee is active','results'=>''));
						return;
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid fee. Check if fee is active','results'=>''));
					return;			
				}

				$sql_checkglcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
										INNER JOIN tb_ledgeraccounts
										ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
										WHERE feeid = $feeid
										AND  gltype = 2";	
										
			} else {							
				$sql_checkglcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
										INNER JOIN tb_ledgeraccounts
										ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
										WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $accountid)
										AND  gltype = 1";				
			}

			if($res=$this->dbconnect->query($sql_checkglcontrol)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;
				}else{
					foreach($res as $row) {		
						$glcontrol = $row['accountid'];							
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
				return;			
			}		
				
			if($trxtype == 'Deposit'){
				$iscredit = 1;
				$transactiontype = 1;
				$debitaccid = $usercontraaccount;
				$creditaccid = $glcontrol;				
			} else if($trxtype == 'Withdraw'){
				$iscredit = 0;
				$transactiontype = 2;
				$debitaccid = $glcontrol;
				$creditaccid = $usercontraaccount;
                                // Check if the amount exceeds the balance
                                $prod_id=$this->dbconnect->query($sql_checkaccount);
                                foreach($prod_id As $p)
                                    $id = $p['productid'];
                                    $balance = $p['crAmount'] - $p['drAmount'] ;
                                $sql_min_balance = "SELECT minbalance FROM tb_productshare WHERE productid = $id ";
                                $find = $this->dbconnect->query($sql_min_balance);
                                foreach($find As $m)
                                    $amt = $m['minbalance'];
                               // var_dump($balance);die;
                                if(($amt + $balance) < $amount)
                                {
                                    echo json_encode(array('retcode'=>'003','retmsg'=>'Withdrawal Amount Exceeds account minimum limit','results'=>''));
						return;
                                }
			} else if($trxtype == 'FeePayment'){
				
				$sql_checkfee="SELECT * FROM tb_fee WHERE feeid = $feeid AND active =1";
				
				if($res=$this->dbconnect->query($sql_checkfee)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid fee amount.','results'=>''));
						return;
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid fee. Check if fee is active','results'=>''));
					return;			
				}		
				
				$iscredit = 1;
				$transactiontype = 3;
				$debitaccid = $usercontraaccount;
				$creditaccid = $glcontrol;				
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Incorrect Transaction Type','results'=>''));
				return;
			}
	
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$serverdate = date('Y-m-d');

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
			$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
			
			$receiptno = $this->dbconnect->lastInsertId();	
			
			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array($receiptno,$valuedate,$accountid,$feeid,$iscredit,$amount,$transactiontype,$debitaccid,$creditaccid));
			
			if($trxtype != 'FeePayment'){
				if($iscredit == 0){
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
					$res_posttrx->execute( array($amount,$accountid));
				} else {
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
					$res_posttrx->execute( array($amount,$accountid));		
				}
			}

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
			
			$ledgerid = $this->dbconnect->lastInsertId();

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
			$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$amount));			

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
			$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$amount));			
			
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
			$results=$accountid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}	

	//Post transfer
	public function postTransfer($craccountid,$draccountid,$amount,$transfertype,$userid,$trxdescription,$valuedate,$usercontraaccount){		
	try{
			//Check if period is active
			$sql_checkperiod="SELECT * FROM tb_period WHERE '".$valuedate."' BETWEEN startdate AND enddate";
			if($res=$this->dbconnect->query($sql_checkperiod)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
				return;			
			}	
			
			//Validations first before transaction posting
			/*
				9  - Share Transfer
				10 - Deposit Transfer
				11 - Loan Repayment Transfer
				12 - Other Transfer
			*/
			$transfertype=trim($transfertype);
			$transfertype_array = array("9", "10", "11", "12");
			
			if(in_array($transfertype,$transfertype_array) == false){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Transfer type','results'=>$transfertype));
				return;
			}
			/* Check if dr account exists   */
			$sql_checkaccount="SELECT * FROM tb_accounts WHERE accountid = $draccountid AND active =1 ";
			
			if($res=$this->dbconnect->query($sql_checkaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
				return;			
			}
			
			/* Check if cr account exists */
			/* For Loan Repayment check if loan is active and has balance */
			if($transfertype == "11")
			{
				$sql_checkaccount="SELECT * FROM tb_accounts WHERE accountid = $craccountid AND active =1 AND COALESCE(fn_accountbalanceatdate(accountid,'".$valuedate."'),0) <> 0";
			}else{
				$sql_checkaccount="SELECT * FROM tb_accounts WHERE accountid = $craccountid AND active =1";
			}			
						
			if($res=$this->dbconnect->query($sql_checkaccount)){
				if($res->rowCount()<=0){
					if($transfertype == "11")
					{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
						return;
					}else{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Check if account exist, is active and loan account has balance','results'=>''));
						return;
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
				return;			
			}
			
			/* Check if credit account is a loan account or when transaction type is not 11  */
			$sql_checkaccount="SELECT * FROM tb_accounts INNER JOIN tb_products ON tb_accounts.productid = tb_products.productid WHERE producttypeid = 4 AND tb_accounts.accountid = $craccountid AND tb_accounts.active =1 ";

			if($res=$this->dbconnect->query($sql_checkaccount)){
				if(($res->rowCount()>0) && ($transfertype != "11")){
					echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot post to a loan account. Transaction type must be loan Repayment','results'=>''));
					return;				
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
				return;			
			}
		    /* Check if user exists   */
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}

		    /* Check user contra account */
			$sql_checkusercontraaccount="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
					INNER JOIN tb_ledgeraccounts
					ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
					WHERE userid = $userid
					AND tb_ledgerlinks.accountid = $usercontraaccount
					AND gltype = 8 ";
			
			if($res=$this->dbconnect->query($sql_checkusercontraaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User Cashier Account. Assign the right cashier account to the user','results'=>''));
				return;			
			}
			
			/* Check if transaction amount */
			if($amount <= 0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid transaction amount. Amount cannot be less or equal to zero','results'=>''));
				return;	
			}
			
			/* Get GL control of the product for cr account is defined */	
			$glcontrol_cr = 0;
										
			$sql_check_cr_glcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
									INNER JOIN tb_ledgeraccounts
									ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
									WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $craccountid)
									AND  gltype = 1";							

			if($res=$this->dbconnect->query($sql_check_cr_glcontrol)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;
				}else{
					foreach($res as $row) {		
						$glcontrol_cr = $row['accountid'];							
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
				return;			
			}	

			/* Get GL control of the product for dr account is defined */	
			$glcontrol_dr = 0;
										
			$sql_check_dr_glcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
									INNER JOIN tb_ledgeraccounts
									ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
									WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $draccountid)
									AND  gltype = 1";							

			if($res=$this->dbconnect->query($sql_check_dr_glcontrol)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;
				}else{
					foreach($res as $row) {		
						$glcontrol_dr = $row['accountid'];							
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
				return;			
			}
			
			//Check shares can only be transferred to shares product 
			$dr_productid = 0;
			$dr_account_balance = 0;
			$cr_productid = 0;
			
			$sql_check_dr_product="SELECT productid,COALESCE(fn_accountbalanceatdate(accountid,'".$valuedate."'),0) AS balance FROM tb_accounts WHERE accountid = $draccountid";							

			if($res=$this->dbconnect->query($sql_check_dr_product)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid debit account product','results'=>''));
					return;
				}else{
					foreach($res as $row) {		
						$dr_productid = $row['productid'];	
						$dr_account_balance = $row['balance'];					
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid debit account product','results'=>''));
				return;			
			}
			
			$sql_check_cr_product="SELECT productid FROM tb_accounts WHERE accountid = ".$craccountid;							

			if($res=$this->dbconnect->query($sql_check_cr_product)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid debit account product','results'=>''));
					return;
				}else{
					foreach($res as $row) {		
						$cr_productid = $row['productid'];							
					}					
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid debit account product','results'=>''));
				return;			
			}
			//To do
			//Share product hardcoded - This to be transferred to settings table
			if(($dr_productid == 1) && ($cr_productid != 1)){
				echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot transfer shares to any other product','results'=>''));
				return;		
			}
			
			//Check Debit account balance - in future include fee amount check
			if($amount > $dr_account_balance){
				echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot transfer amount more than current balance','results'=>''));
				return;		
			}
			
			//Check if customer has a loan or has guaranteed someone
			//To do
			//Check with sacco conditions for Share and deposit transfer
			// For loan transfer the following parameter will be removed in if condition ->  || ($dr_productid == 2)
			if($dr_productid == 1){
				$sql_check_loan_balance="SELECT productid, COALESCE( fn_accountbalanceatdate(accountid,'".$valuedate."'),0) AS balance FROM tb_accounts WHERE productid IN (SELECT productid FROM tb_products WHERE producttypeid=4)
				AND customerid IN (SELECT customerid FROM tb_accounts WHERE accountid = $draccountid)";							

				if($res=$this->dbconnect->query($sql_check_loan_balance)){
					if($res->rowCount()<=0){

					}else{
						foreach($res as $row) {		
							if($row['balance'] > 0){
								echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot transfer shares because client has loan balance','results'=>''));
								return;	
							}							
						}					
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Unable to validate if client has a loan','results'=>''));
					return;			
				}			
			
			
				$sql_check_guaranteed="SELECT COALESCE(COUNT(*),0) nooofloans FROM tb_guarantors WHERE customerid IN (SELECT customerid FROM tb_accounts WHERE accountid = $draccountid) AND guarantorstatus = 1";							

				if($res=$this->dbconnect->query($sql_check_guaranteed)){
					if($res->rowCount()<=0){

					}else{
						foreach($res as $row) {		
							if($row['nooofloans'] > 0){
								echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot transfer shares because client has guaranteed loan(s)','results'=>''));
								return;	
							}							
						}					
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Unable to validate if client has a guaranteed loan(s)','results'=>''));
					return;			
				}		
			}			

			//Post loan repayment
			if($transfertype == "11")
			{	
				//DR
				$iscredit_dr = 0;		
				$debitaccid_dr = $glcontrol_dr;
				$creditaccid_dr = $usercontraaccount;
				
				//Check for backdating of loan repayment
				$sql_checkbackdating="SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0),receiptno FROM tb_transactions WHERE  accountid = $craccountid AND valuedate > '".$valuedate."' GROUP BY receiptno HAVING  COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0)<>0";
				if($res=$this->dbconnect->query($sql_checkbackdating)){
					if($res->rowCount()>0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot backdate loan repayment transaction. Reverse previous payments first.','results'=>''));
						return;
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'You cannot backdate loan repayment transaction. Reverse previous payments first.','results'=>''));
					return;			
				}			
				
				/* Check if account exists   */
				$sql_checkaccount="SELECT tb_loans.* FROM tb_accounts INNER JOIN tb_loans ON tb_loans.accountid = tb_accounts.accountid WHERE tb_loans.accountid = $craccountid AND tb_accounts.active =1";
				
				if($res=$this->dbconnect->query($sql_checkaccount)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
						return;
					}else{
						foreach($res as $res_rows){
							if( $res_rows['loanstatus']<>3){
								echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Loan. Check if loan disbursed and active','results'=>''));
								return;	
							}						
						}
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
					return;			
				}

				//Get loan balance details 
				$interestdue = 0;
				$principaldue = 0;
				$principalbalance = 0;
				$payoffamount = 0;
				$arrearsamount = 0;
				$arrearsdays = 0;
				
				$interestamount = 0;
				$principalamount = 0;
				
				$excessamount = 0;
				$excessamount_account = 0;
				$excessamount_glcontrol = 0;
				
				$closeloan = 0;
				
				$loanbalancedetails = $this->loanbalancedetails($craccountid,$valuedate,0);

				if (is_array($loanbalancedetails)) {
					if(!empty($loanbalancedetails)){
						foreach($loanbalancedetails as $row){
							$interestdue = $row['interestdue'];
							$principaldue = $row['principaldue'];
							$principalbalance = $row['principalbalance'];
							$payoffamount = $row['payoffamount'];
							$arrearsamount = $row['arrearsamount'];
							$arrearsdays = $row['arrearsdays'];
						}
					}else{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Loan balance details not found','results'=>''));
						return;					
					}
				}else{
					echo json_encode(array('retcode'=>'003','retmsg'=>'Loan balance details not found','results'=>''));
					return;					
				}
				
				//Distribute Amount Paid
				if($amount > 0){
					//Check if there is an overpayment
					if($amount >= $payoffamount){
						$closeloan = 1;
					}
					
					if($amount > $payoffamount){
						$excessamount = $amount - $payoffamount;
						$amount = $amount - $excessamount;
					}
					
					//Deal with interest first
					if($amount <= $interestdue){
						$interestamount = $amount;
						$amount = 0;
					}else{
						$interestamount = $interestdue;
						$amount = $amount - $interestdue;
					}
					
					//then deal with principal				
					$principalamount = $amount;						
				}else{
					echo json_encode(array('retcode'=>'003','retmsg'=>'Amount paid should be greater than 0 (zero)','results'=>''));
					return;					
				}		

				/* Get GL control of the product */	
				$glcontrol = 0;
				$interestincome = 0;
				$interestreceivable = 0;
				
				$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
						INNER JOIN tb_ledgeraccounts
						ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
						WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $craccountid)";	

				if($res=$this->dbconnect->query($sql_gllinks)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
						return;
					}else{
						foreach($res as $row) {	
							if($row['gltype'] == 1){
								$glcontrol = $row['accountid'];	
							}
							if($row['gltype'] == 9){
								$interestincome = $row['accountid'];	
							}
							if($row['gltype'] == 10){
								$interestreceivable = $row['accountid'];	
							}
						}					
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
					return;			
				}

				if(($glcontrol <= 0) || ($interestincome <= 0) || ($interestreceivable <= 0)){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL settings for the loan product','results'=>''));
					return;	
				}

				if($excessamount > 0){
					/* Check for deposit account exists   */
					$sql_excess_checkaccount="SELECT * FROM tb_accounts WHERE customerid IN (SELECT customerid FROM tb_accounts WHERE accountid = $craccountid) AND productid = 2"; 
					
					if($res=$this->dbconnect->query($sql_excess_checkaccount)){
						if($res->rowCount()<=0){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
							return;
						}else{
							foreach($res as $row){
								$excessamount_account = $row['accountid'];
							}
						}
					} else {
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account. Check if account exist and is active','results'=>''));
						return;			
					}	

					$sql_excess_checkglcontrol="SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
											INNER JOIN tb_ledgeraccounts
											ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
											WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = $excessamount_account)
											AND  gltype = 1";				
											
					if($res=$this->dbconnect->query($sql_excess_checkglcontrol)){
						if($res->rowCount()<=0){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
							return;
						}else{
							foreach($res as $row) {		
								$excessamount_glcontrol = $row['accountid'];							
							}					
						}
					} else {
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid GL Control','results'=>''));
						return;			
					}				
				}	

				$serverdate = date('Y-m-d');
				$ledgerid = 0;

				$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbconnect->beginTransaction();

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
				$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
				
				$receiptno = $this->dbconnect->lastInsertId();	
				
				//Post Debit Transaction to the source account 
				$source_amount_dr = $principalamount+$interestamount+$excessamount;
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$draccountid,0,$iscredit_dr,$source_amount_dr,$transfertype,$debitaccid_dr,$creditaccid_dr));
				
				//Update Source Debit Account
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
				$res_posttrx->execute( array($source_amount_dr,$draccountid));
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
				
				$ledgerid = $this->dbconnect->lastInsertId();
					
				//Ledger transaction for debit transaction for source account
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid_dr,0,$source_amount_dr));			
				
				//Ledger transaction for credit transaction for source account
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid_dr,1,$source_amount_dr));	
				
				//Post Principal 
				if($principalamount > 0){
					$debitaccid = $usercontraaccount;
					$creditaccid = $glcontrol;
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$craccountid,0,1,$principalamount,6,$debitaccid,$creditaccid));
					
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
					$res_posttrx->execute( array($principalamount,$craccountid));
					
					if($ledgerid == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
						
						$ledgerid = $this->dbconnect->lastInsertId();				
					}
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$principalamount));			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$principalamount));	
				}			
							
				if($interestamount > 0){
					//Post Interest Charged against loan account (but it should not affect loan balance) ---> DR - Interest receivable CR - Interest Income
					$debitaccid = $interestreceivable;
					$creditaccid = $interestincome;	
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$craccountid,0,0,$interestamount,7,$debitaccid,$creditaccid));
					
					if($ledgerid == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
						
						$ledgerid = $this->dbconnect->lastInsertId();				
					}
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));				

					//Post Interest paid against loan account (but it should not affect loan balance) ---> DR - Source of payment e.g. bank\cash\mpesa CR - Interest receivable
					$debitaccid = $usercontraaccount;
					$creditaccid = $interestreceivable;	
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$craccountid,0,1,$interestamount,8,$debitaccid,$creditaccid));

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));		
				}	
				//Post excess payment to savings account. For now it will be posted in to deposit account
				//This should be in the settings page
				if($excessamount > 0){
					$debitaccid = $usercontraaccount;
					$creditaccid = $excessamount_glcontrol;	

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$excessamount_account,0,1,$excessamount,1,$debitaccid,$creditaccid));
					
					if($ledgerid == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
						
						$ledgerid = $this->dbconnect->lastInsertId();				
					}
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$excessamount));			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$excessamount));							
				}
				if($closeloan == 1){
					//close loan
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus=5 WHERE accountid = ?"); 			
					$res_posttrx->execute( array($craccountid));	
					
					//close loan account
					$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET accountstatus=7,active=0 WHERE accountid = ?"); 			
					$res_posttrx->execute( array($craccountid));				
				}
				
				$this->dbconnect->commit();		  
				$retcode='000';
				$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
				$results=$craccountid;
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
			}else{
				//Set DR and CR transaction types
				//CR
				$iscredit_cr = 1;
				$debitaccid_cr = $usercontraaccount;
				$creditaccid_cr = $glcontrol_cr;				

				//DR
				$iscredit_dr = 0;		
				$debitaccid_dr = $glcontrol_dr;
				$creditaccid_dr = $usercontraaccount;
				
				$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbconnect->beginTransaction();
				
				$serverdate = date('Y-m-d');

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
				$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
				
				$receiptno = $this->dbconnect->lastInsertId();	
				//Post Debit
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$draccountid,0,$iscredit_dr,$amount,$transfertype,$debitaccid_dr,$creditaccid_dr));
				
				//Update Debit Account
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
				$res_posttrx->execute( array($amount,$draccountid));
				
				//Post Credit
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$craccountid,0,$iscredit_cr,$amount,$transfertype,$debitaccid_cr,$creditaccid_cr));
										
				//Update Credit Account
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
				$res_posttrx->execute( array($amount,$craccountid));		

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
				
				$ledgerid = $this->dbconnect->lastInsertId();
				
				//Ledger transaction for debit transaction
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid_dr,0,$amount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid_dr,1,$amount));			
				
				//Ledger Transaction for credit transaction
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid_cr,0,$amount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid_cr,1,$amount));		
				
				$this->dbconnect->commit();
			  
				$retcode='000';
				$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
				$results=$craccountid.' - '.$draccountid;
				
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}
	
	//Maintain User Cashier Account
	public function userAccounts($cashieraccount,$userid) {			
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_useraccounts = $this->dbconnect->prepare("DELETE FROM tb_ledgerlinks WHERE userid=?");
			$res_useraccounts->execute(array($userid));
			
			if ((strlen($cashieraccount) > 0) && (is_numeric($cashieraccount))){
				$res_useraccounts = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(userid,gltype,accountid) SELECT ?,?,?");
				$res_useraccounts->execute(array($userid,8,$cashieraccount));
			}
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'User Accounts Updated Successfully. User must log out of the system and login again','results'=>$cashieraccount));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be updated','results'=>''));
		}			
	}

	//Find Transaction Search
    public function searchTransaction($page,$rows,$receiptno) {	
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*) FROM v_customertransactions WHERE (v_customertransactions.receiptno = $receiptno OR $receiptno = 0)";

		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
				
		$sql="SELECT * FROM v_customertransactions WHERE (v_customertransactions.receiptno = $receiptno OR $receiptno = 0)	limit $offset,$rows";

		if($res=$this->dbconnect->query($sql)){
			
			$items = array();

			foreach($res as $row) {
				$node=array();			
				$node['transactionid']=$row['transactionid'];				
				$node['receiptno']=$row['receiptno'];				
				$node['amount']=$row['amount'];
				$node['valuedate']=$row['valuedate'];				
				$node['serverdate']=$row['serverdate'];				
				$node['trxtype']=$row['trxtype'];				
				$node['name']=$row['name'];
				$node['productname']=$row['productname'];				
				$node['fee']=$row['fee'];				
				$node['trxdescription']=$row['trxdescription'];
				$node['debitaccount']=$row['debitaccount'];
				$node['creditaccount']=$row['creditaccount'];	
				$node['reversed']=$row['reversed'];					
				$node['ledgerid']=$row['ledgerid'];	
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No transaction found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Post Reversal transactions
	public function reverseTransaction($receiptno,$userid) {		
	try{
			//Validations first before transaction posting
			$transactions = array();
			/* Check if receipt no exists   */
			$sql_checkreceiptno="SELECT * FROM tb_receipts WHERE receiptno = $receiptno";
			$prev_userid=0;
			
			if($res=$this->dbconnect->query($sql_checkreceiptno)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Receipt No does not exists','results'=>''));
					return;
				} else {
					foreach($res as $row) {		
						$prev_userid = $row['createdby'];							
					}				
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Receipt no not found','results'=>''));
				return;			
			}
			
			/* Check if receipt is already reversed */
			$sql_checkreceiptno="SELECT * FROM tb_receipts WHERE receiptno = $receiptno AND isreversed = 1";
			$prev_userid=0;
			
			if($res=$this->dbconnect->query($sql_checkreceiptno)){
				if($res->rowCount() > 0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Receipt has been already been reversed','results'=>''));
					return;
				} 
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Receipt no not found','results'=>''));
				return;			
			}			
			
		    /* Check if user exists   */
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				} else {
					if ($prev_userid == $userid){
						echo json_encode(array('retcode'=>'003','retmsg'=>'User who posted the transaction cannot initiate reversal','results'=>''));
						return;					
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}

		    /* Get Transaction */
			$sql_gettransaction="SELECT valuedate,accountid,feeid,iscredit AS trxtype,amount,transactiontype,
			debitaccid AS debitacc,creditaccid AS creditacc FROM tb_transactions WHERE receiptno = $receiptno";
			
			if($res=$this->dbconnect->query($sql_gettransaction)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction not found','results'=>''));
					return;
				} else {
					foreach($res as $row) {	
						$node=array();		
						$node['valuedate']=$row['valuedate'];						
						$node['accountid']=$row['accountid'];										
						$node['feeid']=$row['feeid'];	
						$node['trxtype']=$row['trxtype'];						
						$node['amount']=$row['amount'];										
						$node['transactiontype']=$row['transactiontype'];														
						$node['debitacc']=$row['debitacc'];
						$node['creditacc']=$row['creditacc'];				
						
						array_push($transactions, $node);					
					}				
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction not found','results'=>''));
				return;			
			}			

			//Check if period is active
			$sql_checkperiod="SELECT * FROM tb_period WHERE (SELECT DISTINCT valuedate FROM tb_transactions WHERE receiptno = $receiptno) BETWEEN startdate AND enddate";
			if($res=$this->dbconnect->query($sql_checkperiod)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
				return;			
			}
			
		    /* Get Ledger Transaction */
			$sql_gettransaction="SELECT ledgerid FROM tb_ledgers WHERE receiptno = $receiptno";
			
			if($res=$this->dbconnect->query($sql_gettransaction)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Ledger transaction not found','results'=>''));
					return;
				} else {
					foreach($res as $row) {		
						$ledgerid = $row['ledgerid'];							
					}				
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Ledger transaction not found','results'=>''));
				return;			
			}
			
			//Check if it is a loan disbursement			
			foreach($transactions as $row)
			{
				if ($row['transactiontype'] == 5){
					//Check if loan has been paid
					$sql_check_loandisbursement="SELECT * FROM tb_transactions
						INNER JOIN tb_receipts
						ON tb_transactions.receiptno = tb_receipts.receiptno
						WHERE accountid = ".$row['accountid']." 
						AND transactiontype <>5 
						AND tb_receipts.isreversed = 0";
					if($res=$this->dbconnect->query($sql_check_loandisbursement)){
						if($res->rowCount()>0){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Please reverse all loan repayments before cancelling the loan','results'=>''));
							return;
						}
					} else {
						echo json_encode(array('retcode'=>'003','retmsg'=>'Please reverse all loan repayments before cancelling the loan','results'=>''));
						return;			
					}				
				}
			}
				
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			foreach($transactions as $row)
			{
				$valuedate = $row['valuedate'];
				$accountid = $row['accountid'];
				$feeid = $row['feeid'];
				$amount = $row['amount'];
				$transactiontype = $row['transactiontype'];
				$temp_debitaccid = $row['debitacc'];
				$temp_creditaccid = $row['creditacc'];
				$debitaccid = $row['debitacc'];
				$creditaccid = $row['creditacc'];
						
				if($row['trxtype'] == 0)
				{
					$iscredit = 1;
					$debitaccid = $temp_creditaccid;
					$creditaccid = $temp_debitaccid;
				}
				else
				{
					$iscredit = 0;	
					$debitaccid = $temp_creditaccid;
					$creditaccid = $temp_debitaccid;					
				}
				
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_receipts SET isreversed= ?,reversedby= ?, reversedon= NOW() WHERE receiptno =?"); 			
				$res_posttrx->execute( array(1,$userid,$receiptno));		
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$accountid,$feeid,$iscredit,$amount,$transactiontype,$debitaccid,$creditaccid));
				
				if($feeid == 0){
					if($iscredit == 0){
						$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+?,active=1 WHERE accountid=?"); 			
						$res_posttrx->execute( array($amount,$accountid));
					} else {
						$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+?,active=1 WHERE accountid=?"); 			
						$res_posttrx->execute( array($amount,$accountid));		
					}
				}
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$amount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$amount));
				
				if($transactiontype == 5){
					$open_loan_status = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus= 4 WHERE accountid =?"); 			
					$open_loan_status->execute( array($accountid));	

					$open_loanschedule = $this->dbconnect->prepare("UPDATE tb_loanschedule INNER JOIN tb_loans ON tb_loanschedule.loanid = tb_loans.loanid SET principalpaid=0,interestpaid=0 WHERE tb_loans.accountid = ?"); 			
					$open_loanschedule->execute( array($accountid));					
				}else{
					if($transactiontype == 8){ //Update schedule interest paid
						$interestamount = $amount;
						$interestpaid = 0;
						$loanscheduleid = 0;
						
						while ($interestamount > 0){
							$check_getinterestpaid = $this->dbconnect->prepare("SELECT tb_loanschedule.loanscheduleid,tb_loanschedule.interestpaid FROM tb_loans INNER JOIN tb_loanschedule ON tb_loanschedule.loanid = tb_loans.loanid 
								WHERE tb_loans.accountid = ? AND interestpaid<>0 ORDER BY repaymentdate DESC LIMIT 1 "); 			
							if($check_getinterestpaid->execute( array($accountid))){
								if($check_getinterestpaid->rowCount() > 0){
									$loanscheduleitem = $check_getinterestpaid->fetchAll(PDO::FETCH_ASSOC);
									unset($check_getinterestpaid);
									$interestpaid = $loanscheduleitem[0]["interestpaid"];
									$loanscheduleid = $loanscheduleitem[0]["loanscheduleid"];
									
									if($interestpaid == 0){//Avoid updating -ves in the schedule
										$interestamount = 0;
									}else{
										if($interestamount > $interestpaid){
											$update_schedule_interest = $this->dbconnect->prepare("UPDATE tb_loanschedule SET interestpaid= interestpaid-?  WHERE loanscheduleid =?"); 			
											$update_schedule_interest->execute( array($interestpaid,$loanscheduleid));
											unset($update_schedule_interest);
											
											$interestamount=$interestamount-$interestpaid;
										}else if($interestamount <= $interestpaid){																			
											$update_schedule_interest = $this->dbconnect->prepare("UPDATE tb_loanschedule SET interestpaid= interestpaid-?  WHERE loanscheduleid =?"); 			
											$update_schedule_interest->execute( array($interestamount,$loanscheduleid));
											unset($update_schedule_interest);
											$interestamount = 0;
										}
									}
								}else{
									$interestamount = 0;
								}
							}else{
								$interestamount = 0;
							}												
						}					
					}
					
					if($transactiontype == 6){ //Update schedule principal paid
						$principalamount = $amount;
						$principalpaid = 0;
						$loanscheduleid = 0;
						
						while ($principalamount > 0){
							$check_getprincipalpaid = $this->dbconnect->prepare("SELECT tb_loanschedule.loanscheduleid,tb_loanschedule.principalpaid FROM tb_loans INNER JOIN tb_loanschedule ON tb_loanschedule.loanid = tb_loans.loanid 
								WHERE tb_loans.accountid = ? AND principalpaid<>0 ORDER BY repaymentdate DESC LIMIT 1 "); 			
							if($check_getprincipalpaid->execute( array($accountid))){
								if($check_getprincipalpaid->rowCount() > 0){
									$loanscheduleitem = $check_getprincipalpaid->fetchAll(PDO::FETCH_ASSOC);
									unset($check_getprincipalpaid);
									$principalpaid = $loanscheduleitem[0]["principalpaid"];
									$loanscheduleid = $loanscheduleitem[0]["loanscheduleid"];
									
									if($principalpaid == 0){//Avoid updating -ves in the schedule
										$principalamount = 0;
									}else{
										if($principalamount > $principalpaid){
											$update_schedule_principal = $this->dbconnect->prepare("UPDATE tb_loanschedule SET principalpaid= principalpaid-?  WHERE loanscheduleid =?"); 			
											$update_schedule_principal->execute( array($principalpaid,$loanscheduleid));
											unset($update_schedule_principal);
											$principalamount=$principalamount-$principalpaid;
										}else if($principalamount <= $principalpaid){																			
											$update_schedule_principal = $this->dbconnect->prepare("UPDATE tb_loanschedule SET principalpaid= principalpaid-?  WHERE loanscheduleid =?"); 			
											$update_schedule_principal->execute( array($principalamount,$loanscheduleid));
											unset($update_schedule_principal);
											
											$principalamount = 0;
										}
									}
								}else{
									$principalamount = 0;
								}
							}else{
								$principalamount = 0;
							}												
						}							
					}
					
					$check_closedloan = $this->dbconnect->prepare("SELECT * FROM tb_loans WHERE accountid = ? AND loanstatus <> 3 AND COALESCE((fn_getinterestbalance(accountid,NOW())+fn_getprincipalbalance(accountid,NOW())),0) >0"); 			
					if($check_closedloan->execute( array($accountid))){
						if($check_closedloan->rowCount() > 0){
							$open_closedloan = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus= 3 WHERE accountid =?"); 			
							$open_closedloan->execute( array($accountid));
						}
					}	
				}								
			}
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Transaction Reversed Successfully';
			$results=$accountid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}	

	//Post Ledger Transactions
	public function postLedgerTransaction($valuedate,$narration,$ledgertrx,$userid) {		
	try{			
		    /* Check if user exists   */
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}
			
			/* Validate Accounts */
			foreach($ledgertrx as $ldgtrx) {
				$sql_validateaccounts="SELECT * FROM tb_ledgeraccounts WHERE tb_ledgeraccounts.accountid = ".$ldgtrx['accountid'];
				
				if($res=$this->dbconnect->query($sql_validateaccounts)){
					if($res->rowCount()<=0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account','results'=>''));
						return;
					}
				} else {
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Account','results'=>''));
					return;			
				}			
			}

			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,transactionnote) VALUES (?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$narration));
			
			$ledgerid = $this->dbconnect->lastInsertId();
			
			foreach($ledgertrx as $ldgtrx) {
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$ldgtrx['accountid'],$ldgtrx['trxtypeid'],$ldgtrx['amount']));			
			}
			
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='Transaction Posted Successfully';
			$results=$ledgerid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}

	//Account Statement
    public function accountstatement($page,$rows,$customerid,$idno,$accountno,$fromdate,$todate){ 	
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT * FROM v_customertransactions WHERE (customerid = $customerid OR $customerid = 0 ) 
		AND (accountid = $accountno OR $accountno = 0 )
		AND (idno = $idno OR $idno = 0 )
		AND valuedate BETWEEN '$fromdate' AND '$todate' ORDER BY productid,receiptno,valuedate,transactionid";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
				
		$sql="SELECT * FROM v_customertransactions WHERE (customerid = $customerid OR $customerid = 0 ) 
		AND (accountid = $accountno OR $accountno = 0)
		AND (idno = $idno OR $idno = 0)
		AND valuedate BETWEEN '$fromdate' AND '$todate'	 ORDER BY productid,receiptno,valuedate,transactionid limit $offset,$rows";

		$balance = 0;
		$accountid  = 0;
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();

			foreach($res as $row) {
				if(($row['accountid'] != $accountid) && ($row['feeid'] == 0))
				{
					$balance = 0;
				}
				if(($row['accountid'] != $accountid) && ($row['feeid'] != 0) && ($row['transactiontypeid'] == 5))
				{
					$balance = 0;
				}
				
				$accountid  = $row['accountid'];
				
				$node=array();	
				if ($row['producttypeid'] != 4) {
					if (($row['iscredit'] == 1) && ($row['feeid'] == 0))
					{
						$balance = 	$balance + $row['credit'];	
					}
					if (($row['iscredit'] == 0) && ($row['feeid'] == 0))
					{
						$balance = 	$balance - $row['debit'];	
					}
				}else{
					if (($row['iscredit'] == 1) && ($row['feeid'] == 0))
					{
						$balance = 	$balance - $row['credit'];	
					}
					if (($row['iscredit'] == 0) && ($row['feeid'] == 0))
					{
						$balance = 	$balance + $row['debit'];	
					}				
					if (($row['iscredit'] == 0) && ($row['feeid'] != 0)  && ($row['transactiontypeid'] == 5)){
						$balance = 	$balance + $row['debit'];	
					}
				}
				
				$node['transactionid']=$row['transactionid'];				
				$node['receiptno']=$row['receiptno'];
				$node['accountid']=$row['accountid'];				
				$node['amount']=$row['amount'];
				$node['valuedate']=$row['valuedate'];				
				$node['serverdate']=$row['serverdate'];				
				$node['trxtype']=$row['trxtype'];				
				$node['name']=$row['name'];
				$node['iscredit']=$row['iscredit'];
				$node['productid']=$row['productid'];	
				$node['productname']=$row['productname'];				
				$node['fee']=$row['fee'];				
				$node['trxdescription']=$row['trxdescription'];
				$node['debitaccount']=$row['debitaccount'];
				$node['creditaccount']=$row['creditaccount'];	
				$node['reversed']=$row['reversed'];					
				$node['ledgerid']=$row['ledgerid'];	
				$node['idno']=$row['idno'];	
				$node['transactiontype']=$row['transactiontype'];	
				$node['debit']=$row['debit'];	
				$node['credit']=$row['credit'];	
				$node['balance'] = $balance;	
				
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No transaction found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Loan Product
	public function getLoanProduct($page,$rows,$applylimt,$productid) {
		$result = array();
		if ($applylimt==0){
		$sql="SELECT 
			productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
			settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
			FROM v_getloanproducts WHERE (productid = $productid OR $productid = 0)";
		}else {
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*)	FROM v_getloanproducts WHERE (productid = $productid OR $productid = 0)";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
			
		$sql="SELECT 
			productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
			settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
			FROM v_getloanproducts WHERE (productid = $productid OR $productid = 0)
			 limit $offset,$rows";
		}

		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['productid']=$row['productid'];
				$node['code']=$row['code'];				
				$node['description']=$row['description'];				
				$node['producttypeid']=$row['producttypeid'];
				$node['producttype']=$row['producttype'];				
				$node['currencyid']=$row['currencyid'];
				$node['currency']=$row['currency'];					
				$node['currencycode']=$row['currencycode'];	
				$node['maxaccounts']=$row['maxaccounts'];
				$node['active']=$row['active'];				
				$node['activestate']=$row['activestate'];
				$node['creationdate']=$row['creationdate'];				
				$node['effectivedate']=$row['effectivedate'];				
				$node['productcontrol']=$row['productcontrol'];	
				$node['productcontroldesc']=$row['productcontroldesc'];
				$node['interestincome']=$row['interestincome'];	
				$node['interestincomedesc']=$row['interestincomedesc'];	
				$node['interestreceivable']=$row['interestreceivable'];	
				$node['interestreceivabledesc']=$row['interestreceivabledesc'];				
				
				$settings =json_decode($row['settings'], true);	

				$node['interestrate'] = $settings['interestrate'];
				$node['effectivedate'] = $settings['effectivedate'];
				$node['minloanterm'] = $settings['minloanterm'];
				$node['maxloanterm'] = $settings['maxloanterm'];
				$node['minloanamount'] = $settings['minloanamount'];
				$node['maxloanamount'] = $settings['maxloanamount'];
				$node['interestcalculationmethod'] = $settings['interestcalculationmethod'];
				$node['minimumsavings'] = $settings['minimumsavings'];
				$node['graceperiod'] = $settings['graceperiod'];
				$node['graceperiodon'] = $settings['graceperiodon'];
				$node['daycount'] = $settings['daycount'];
				$node['termperiod'] = $settings['termperiod'];
				$node['flatinterestamount'] = $settings['flatinterestamount'];
				$node['ignoreholidays'] = $settings['ignoreholidays'];
				$node['savingsproduct'] = $settings['savingsproduct'];
				
				$node['useguaranteedshares'] = $settings['useguaranteedshares'];
				$node['percentage'] = $settings['percentage'];
				$node['noofguarantor'] = $settings['noofguarantor'];
				$node['guaranteeproducts'] = $settings['guaranteeproducts'];
				
				$node['fee'] = $settings['fee'];
				
				$node['futureinterest']=$settings['futureinterest'];	
				$node['accrueinterest']=$settings['accrueinterest'];					

				array_push($items, $node);
			}

			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No Product found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Loan Product
	public function addLoanProduct($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$minloanterm,$maxloanterm,$minloanamount,$maxloanamount,$interestrate,$interestcalculationmethod,$minimumsavings,$savingsproduct,$graceperiod,$graceperiodon,$daycount,$ignoreholidays,$fee,$productcontrol,$interestincome,$interestreceivable,$noofguarantor,$useguaranteedshares,$percentage,$guaranteeproducts,$termperiod,$flatinterestamount) {
		$date = date('Y-m-d');
		if(strlen($noofguarantor) == 0)
		{
			$noofguarantor = 0;
		}
		$settings = '{ "interestrate": "'.$interestrate.'", "effectivedate": "'.$date.'", "minloanterm": "'.$minloanterm.'", "maxloanterm": "'.$maxloanterm.'", "minloanamount": "'.$minloanamount.'", "maxloanamount": "'.$maxloanamount.'", "interestcalculationmethod": "'.$interestcalculationmethod.'", "minimumsavings": "'.$minimumsavings.'", "savingsproduct":"'.$savingsproduct.'", "graceperiod": "'.$graceperiod.'", "percentage": "'.$percentage.'", "guaranteeproducts": '.$guaranteeproducts.', "noofguarantor": "'.$noofguarantor.'", "useguaranteedshares": "'.$useguaranteedshares.'", "graceperiodon": "'.$graceperiodon.'", "daycount": "'.$daycount.'", "termperiod": "'.$termperiod.'", "flatinterestamount": "'.$flatinterestamount.'", "ignoreholidays": "'.$ignoreholidays.'", "fee": '.$fee.', "futureinterest": "'.$futureinterest.'", "accrueinterest": "'.$accrueinterest.'" }';

		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_loan = $this->dbconnect->prepare("INSERT INTO tb_products(code,description,producttypeid,currencyid,maxaccounts,active,creationdate) VALUES(?,?,?,?,?,?,?)");
			$res_loan->execute(array($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$date));
			
			$productid = $this->dbconnect->lastInsertId();
			
			$res_loan = $this->dbconnect->prepare("INSERT INTO tb_productloan(productid,settings,effectivedate) VALUES(?,?,?)");
			$res_loan->execute(array($productid,$settings,$date));
			
			$res_loan = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(productid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_loan->execute(array($productid,1,$productcontrol,$productid,9,$interestincome,$productid,10,$interestreceivable));		
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Loan Product Created Successfully','results'=>$productid));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>$e->getMessage()));
		}
	}

	//Edit Loan Product
	public function editLoanProduct($productid,$code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$minloanterm,$maxloanterm,$minloanamount,$maxloanamount,$interestrate,$interestcalculationmethod,$minimumsavings,$savingsproduct,$graceperiod,$graceperiodon,$daycount,$ignoreholidays,$fee,$productcontrol,$interestincome,$interestreceivable,$noofguarantor,$useguaranteedshares,$percentage,$guaranteeproducts,$termperiod,$flatinterestamount,$futureinterest,$accrueinterest) {	
		$date = date('Y-m-d');
		$settings = '{ "interestrate": "'.$interestrate.'", "effectivedate": "'.$date.'", "minloanterm": "'.$minloanterm.'", "maxloanterm": "'.$maxloanterm.'", "minloanamount": "'.$minloanamount.'", "maxloanamount": "'.$maxloanamount.'", "interestcalculationmethod": "'.$interestcalculationmethod.'", "minimumsavings": "'.$minimumsavings.'", "savingsproduct":"'.$savingsproduct.'", "graceperiod": "'.$graceperiod.'", "percentage": "'.$percentage.'", "guaranteeproducts": '.$guaranteeproducts.', "noofguarantor": "'.$noofguarantor.'", "useguaranteedshares": "'.$useguaranteedshares.'", "graceperiodon": "'.$graceperiodon.'", "daycount": "'.$daycount.'", "termperiod": "'.$termperiod.'", "flatinterestamount": "'.$flatinterestamount.'", "ignoreholidays": "'.$ignoreholidays.'", "fee": '.$fee.', "futureinterest": "'.$futureinterest.'", "accrueinterest": "'.$accrueinterest.'" }';
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			$res_loan = $this->dbconnect->prepare("UPDATE tb_products SET code=?,description=?,producttypeid=?,currencyid=?,maxaccounts=?,active=?,editdate=? WHERE productid=?");
			$res_loan->execute(array($code,$description,$producttypeid,$currencyid,$maxaccounts,$active,$date,$productid));
			
			$res_loan = $this->dbconnect->prepare("UPDATE tb_productloan SET settings=?,effectivedate=? WHERE productid=?");
			$res_loan->execute(array($settings,$date,$productid));

			$res_loan = $this->dbconnect->prepare("DELETE FROM tb_ledgerlinks WHERE productid=?");
			$res_loan->execute(array($productid));
			
			$res_loan = $this->dbconnect->prepare("INSERT INTO tb_ledgerlinks(productid,gltype,accountid) SELECT ?,?,? UNION ALL SELECT ?,?,? UNION ALL SELECT ?,?,?");
			$res_loan->execute(array($productid,1,$productcontrol,$productid,9,$interestincome,$productid,10,$interestreceivable));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Loan Product Update Successfully','results'=>$productid));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be updated','results'=>''));
		}			
	}

	//View Ledger Transactions
    public function viewledgertransactions($page,$rows,$ledgerid,$receiptno,$accountno,$fromdate,$todate){ 	
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT count(ledgerid) As ledger FROM v_getledgertransactions WHERE (ledgerid = $ledgerid OR $ledgerid = 0 ) 
		AND (accountid = $accountno OR $accountno = 0 )
		AND (receiptno = $receiptno OR $receiptno = 0 )
		AND valuedate BETWEEN '$fromdate' AND '$todate' ORDER BY ledgerid";
		
		$rs=$this->dbconnect->query($sql_count);
		foreach($rs As $row)
		$result["total"] = $row['ledger'];
				
		$sql="SELECT * FROM v_getledgertransactions WHERE (ledgerid = $ledgerid OR $ledgerid = 0 ) 
		AND (accountid = $accountno OR $accountno = 0)
		AND (receiptno = $receiptno OR $receiptno = 0)
		AND valuedate BETWEEN '$fromdate' AND '$todate'	 ORDER BY ledgerid limit $offset,$rows";

		if($res=$this->dbconnect->query($sql)){
			
			$items = array();

			foreach($res as $row) {			
				$node['ledgerid']=$row['ledgerid'];
				$node['accountid']=$row['accountid'];				
				$node['valuedate']=$row['valuedate'];				
				$node['accountname']=$row['accountname'];			
				$node['debit']=$this->formatMoney($row['debit'], true);				
				$node['credit']=$this->formatMoney($row['credit'], true);				
				$node['transactionnote']=$row['transactionnote'];
				$node['receiptno']=$row['receiptno'];
				$node['username']=$row['username'];	
								
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No transaction found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Calculate Loan Schedule
	function getschedule($productid,$loanamount,$loanterm,$print){
            $disbursementdate = date('Y-m-d');
		$sql_product_settings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
			settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
			FROM v_getloanproducts WHERE productid = ?");
		$sql_product_settings->execute(array($productid));		
		$productsettings = $sql_product_settings->fetchAll(PDO::FETCH_ASSOC);
		
		if(!empty($productsettings)){		
			$product_settings  = json_decode($productsettings[0]["settings"], true);		
			
			$interestcalculationmethod = $product_settings['interestcalculationmethod'];			
			$interestrate = $product_settings['interestrate'];
			$graceperiod = $product_settings['graceperiod'];
			$graceperiodon = $product_settings['graceperiodon'];
			$daycount = $product_settings['daycount'];
			$ignoreholidays = $product_settings['ignoreholidays'];
			$termperiod = $product_settings['termperiod']; 
			$flatinterestamount = $product_settings['flatinterestamount'];
			$roudto = 2; 
			
			if($interestcalculationmethod == 1){//Declining Balance With Equal Installments
				//EMI = i*P / [1- (1+i)^-n] //Equal Monthly Installment				
				$p = (float)$loanamount;			
				$l = (float)(1/12);
				$n = (float)$loanterm;
				$r = (float)($interestrate/100);
				$i = (float)($l * $r);
				$emi = round((($i*$p)/(1-(pow((1+$i),($n*-1))))),$roudto);
								
				$s_principalbalance = $loanamount;
				$s_interest = 0;
				$s_principal = 0;
				$s_period = 1;
				$schedule = array();
				$total_principal = 0;
				$total_interest = 0;
				$total_emi = 0;				
				$s_repaymentdate = $disbursementdate;
										
				while ($s_period <= $loanterm){
					$node = array();
					$dateincrement = "+1 months";
					
					if($termperiod == 1)
					{
						$dateincrement = "+1 months";
					} else if($termperiod == 2)
					{
						$dateincrement = "+1 week";
					}
					
					$s_interest = round((float)($s_principalbalance * $i),$roudto);
					$s_principal = round((float)($emi - $s_interest),$roudto);
					
					if(($s_period == $loanterm) && ($s_principalbalance < $s_principal)){
						$s_principal = (float)$s_principalbalance;
						$s_principalbalance = 0;
						$emi = ($s_principal + $s_interest);
					} elseif(($s_period == $loanterm) && ($s_principalbalance > $s_principal)){
						$s_principal = (float)$s_principalbalance;
						$s_principalbalance = 0;
						$emi = ($s_principal + $s_interest);
					}else{
						$s_principalbalance = round((float)($s_principalbalance - $s_principal),$roudto);
					}
					
					$date1 = new DateTime($s_repaymentdate);
					$date2 = new DateTime(date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate))));
					
					//ignore weekend. 
					//to do => ignore holidays
					if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 6)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 7)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else{
						$s_repaymentdate = date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate)));
					}
														
					$interval = $date1->diff($date2);	
											
					$node['period'] = $s_period;
					$node['repaymentdate'] = $s_repaymentdate;
					$node['interval'] = $interval->days;					
					$node['balance'] = $s_principalbalance;
					$node['principal'] = $s_principal;
					$node['interest'] = $s_interest;
					$node['totalrepayment'] = $emi;
					
					array_push($schedule, $node);
					
					$s_period++;								
				}
				
				if ($print == 1) {	
					echo "<table class=\"payslip\"><tr class=\"payslip\"><td class=\"payslip\"><strong>Period</strong></td><td class=\"payslip\"><strong>Repayment Date</strong></td><td class=\"payslip\"><strong>No of Days</strong></td><td class=\"payslip\"><strong>Balance</strong></td><td class=\"payslip\"><strong>Principal</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Repayment</strong></td></tr>";
					$total_principal = 0;
					$total_interest = 0;
					
					foreach($schedule as $row ) {
						$total_principal += $row['principal'];
						$total_interest += $row['interest'];
						
						echo "<tr class=\"payslip\"><td class=\"payslip\">".$row['period']."</td><td class=\"payslip\">".$row['repaymentdate']."</td><td class=\"payslip\">".$row['interval']."</td><td class=\"payslip\">".$this->formatMoney($row['balance'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['principal'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['interest'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['totalrepayment'], true)."</td></tr>";													
					}				
					$total_emi = ($total_principal+$total_interest);
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong> Totals </strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_principal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_interest, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_emi, true)."</strong></td></tr>";
					echo '</table>';	
				} else {
					return $schedule;				
				}
				
			}elseif($interestcalculationmethod == 2){//Declining Balance With Recalculate Interest
				//Daily Interest = Balance amount * rate of interest / ( 365 * 100 )				
				$s_principalbalance = (float)$loanamount;
				$s_loanterm = (float)$loanterm;
				$s_interestrate = (float)($interestrate);
				$s_repaymentdate = $disbursementdate;
				$s_interest = 0;
				$s_principal = round((float)($s_principalbalance/$s_loanterm),$roudto);
				$s_period = 1;				
				$total_principal = 0;
				$total_interest = 0;
				$total_emi = 0;	
				$emi = 0;		

				$schedule = array();
				
				while ($s_period <= $loanterm){
					$node = array();
					$dateincrement = "+1 months";
					
					if($termperiod == 1)
					{
						$dateincrement = "+1 months";
					} else if($termperiod == 2)
					{
						$dateincrement = "+1 week";
					}
					
					$date1 = new DateTime($s_repaymentdate);
					$date2 = new DateTime(date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate))));
					
					//ignore weekend. 
					//to do => ignore holidays
					if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 6)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 7)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else{
						$s_repaymentdate = date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate)));
					}	
					
					$interval = $date1->diff($date2);
					
					$s_interest = round((float)($interval->days * (($s_principalbalance * $s_interestrate)/( 365 * 100 ))),$roudto);
									
					if(($s_period == $loanterm) && ($s_principalbalance < $s_principal)){
						$s_principal = (float)$s_principalbalance;
						$s_principalbalance = 0;
					} elseif(($s_period == $loanterm) && ($s_principalbalance > $s_principal)){
						$s_principal = (float)$s_principalbalance;
						$s_principalbalance = 0;
					}else{
						$s_principalbalance = round((float)($s_principalbalance - $s_principal),$roudto);
					}

					$emi = ($s_principal + $s_interest);
					
					$node['period'] = $s_period;
					$node['repaymentdate'] = $s_repaymentdate;
					$node['interval'] = $interval->days;
					$node['balance'] = $s_principalbalance;
					$node['principal'] = $s_principal;
					$node['interest'] = $s_interest;
					$node['totalrepayment'] = $emi;
					
					array_push($schedule, $node);
					
					$s_period++;								
				}
				
				if ($print == 1) {				
					echo "<table class=\"payslip\"><tr class=\"payslip\"><td class=\"payslip\"><strong>Period</strong></td><td class=\"payslip\"><strong>Repayment Date</strong></td><td class=\"payslip\"><strong>No of Days</strong></td><td class=\"payslip\"><strong>Balance</strong></td><td class=\"payslip\"><strong>Principal</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Repayment</strong></td></tr>";
					$total_principal = 0;
					$total_interest = 0;
					
					foreach( $schedule as $row ) {
						$total_principal += $row['principal'];
						$total_interest += $row['interest'];
						
						echo "<tr class=\"payslip\"><td class=\"payslip\">".$row['period']."</td><td class=\"payslip\">".$row['repaymentdate']."</td><td class=\"payslip\">".$row['interval']."</td><td class=\"payslip\">".$this->formatMoney($row['balance'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['principal'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['interest'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['totalrepayment'], true)."</td></tr>";													
					}
					
					$total_emi = ($total_principal+$total_interest);
					//echo "<tr class=\"payslip\"><td colspan=\"7\"></td></tr>";				
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong> Totals </strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_principal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_interest, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_emi, true)."</strong></td></tr>";
					echo '</table>';
					
				} else {
					return $schedule;				
				}			
			}elseif($interestcalculationmethod == 3){//Flat Interest Rate
				//Interest = P*r/100*n
				$s_principalbalance = (float)$loanamount;
				$s_loanterm = (float)$loanterm;
				$s_interestrate = (float)($interestrate);
				$s_repaymentdate = $disbursementdate;
				$s_period = 1;				
				$total_principal = $s_principalbalance;
				$total_interest = round((float)($s_principalbalance*($s_interestrate/100)*$s_loanterm),$roudto);
				$total_emi = $total_principal + $total_interest;
				$total_loan = $total_principal + $total_interest;				
				$emi = round(($total_emi/$s_loanterm),$roudto);
				$s_interest = round((float)($total_interest/$s_loanterm),$roudto);
				$s_principal = round((float)($total_principal/$s_loanterm),$roudto);

				$schedule = array();				
				while ($s_period <= $loanterm){
					$node = array();
					$dateincrement = "+1 months";
					
					if($termperiod == 1)
					{
						$dateincrement = "+1 months";
					} else if($termperiod == 2)
					{
						$dateincrement = "+1 week";
					}
					
					$date1 = new DateTime($s_repaymentdate);
					$date2 = new DateTime(date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate))));
					
					//ignore weekend. 
					//to do => ignore holidays
					if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 6)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 7)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else{
						$s_repaymentdate = date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate)));
					}
					
					$interval = $date1->diff($date2);
					
					$total_loan = round((float)($total_loan - ($s_principal+$s_interest)),$roudto);
					
					$node['period'] = $s_period;
					$node['repaymentdate'] = $s_repaymentdate;
					$node['interval'] = $interval->days;
					$node['balance'] = $total_loan;
					$node['principal'] = $s_principal;
					$node['interest'] = $s_interest;
					$node['totalrepayment'] = $emi;
					
					array_push($schedule, $node);
					
					$s_period++;								
				}
				
				if ($print == 1) {				
					echo "<table class=\"payslip\"><tr class=\"payslip\"><td class=\"payslip\"><strong>Period</strong></td><td class=\"payslip\"><strong>Repayment Date</strong></td><td class=\"payslip\"><strong>No of Days</strong></td><td class=\"payslip\"><strong>Balance</strong></td><td class=\"payslip\"><strong>Principal</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Repayment</strong></td></tr>";
					$total_principal = 0;
					$total_interest = 0;
					
					foreach( $schedule as $row ) {
						$total_principal += $row['principal'];
						$total_interest += $row['interest'];
						
						echo "<tr class=\"payslip\"><td class=\"payslip\">".$row['period']."</td><td class=\"payslip\">".$row['repaymentdate']."</td><td class=\"payslip\">".$row['interval']."</td><td class=\"payslip\">".$this->formatMoney($row['balance'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['principal'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['interest'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['totalrepayment'], true)."</td></tr>";													
					}
					
					$total_emi = ($total_principal+$total_interest);
					//echo "<tr class=\"payslip\"><td colspan=\"7\"></td></tr>";				
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong> Totals </strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_principal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_interest, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_emi, true)."</strong></td></tr>";
					echo '</table>';
					
				} else {
					return $schedule;				
				}				
			}elseif($interestcalculationmethod == 4){//Flat Interest Amount
				//Interest = P*r/100*n
				$s_principalbalance = (float)$loanamount;
				$s_loanterm = (float)$loanterm;
				$s_interestrate = (float)($interestrate);
				$s_repaymentdate = $disbursementdate;
				$s_period = 1;				
				$total_principal = $s_principalbalance;
				$total_interest = round(($s_loanterm * $flatinterestamount) ,$roudto);
				$total_emi = $total_principal + $total_interest;
				$total_loan = $total_principal + $total_interest;				
				$emi = round(($total_emi/$s_loanterm),$roudto);
				$s_interest = round((float)($total_interest/$s_loanterm),$roudto);
				$s_principal = round((float)($total_principal/$s_loanterm),$roudto);

				$schedule = array();				
				while ($s_period <= $loanterm){
					$node = array();
					$dateincrement = "+1 months";
					
					if($termperiod == 1)
					{
						$dateincrement = "+1 months";
					} else if($termperiod == 2)
					{
						$dateincrement = "+1 week";
					}
					
					$date1 = new DateTime($s_repaymentdate);
					$date2 = new DateTime(date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate))));
					
					//ignore weekend. 
					//to do => ignore holidays
					if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 6)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+2 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else if(($ignoreholidays == 1) && (date('N', strtotime($dateincrement, strtotime($s_repaymentdate))) == 7)){
						$date2 = new DateTime(date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate)))));
						$s_repaymentdate = date('Y-m-d', strtotime("+1 days", strtotime($dateincrement, strtotime($s_repaymentdate))));
					}else{
						$s_repaymentdate = date('Y-m-d', strtotime($dateincrement, strtotime($s_repaymentdate)));
					}

					$interval = $date1->diff($date2);

					$total_loan = round((float)($total_loan - ($s_principal+$s_interest)),$roudto);
					
					$node['period'] = $s_period;
					$node['repaymentdate'] = $s_repaymentdate;
					$node['interval'] = $interval->days;
					$node['balance'] = $total_loan;
					$node['principal'] = $s_principal;
					$node['interest'] = $s_interest;
					$node['totalrepayment'] = $emi;
					
					array_push($schedule, $node);
					
					$s_period++;								
				}
				
				if ($print == 1) {				
					echo "<table class=\"payslip\"><tr class=\"payslip\"><td class=\"payslip\"><strong>Period</strong></td><td class=\"payslip\"><strong>Repayment Date</strong></td><td class=\"payslip\"><strong>No of Days</strong></td><td class=\"payslip\"><strong>Balance</strong></td><td class=\"payslip\"><strong>Principal</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Repayment</strong></td></tr>";
					$total_principal = 0;
					$total_interest = 0;
					
					foreach( $schedule as $row ) {
						$total_principal += $row['principal'];
						$total_interest += $row['interest'];
						
						echo "<tr class=\"payslip\"><td class=\"payslip\">".$row['period']."</td><td class=\"payslip\">".$row['repaymentdate']."</td><td class=\"payslip\">".$row['interval']."</td><td class=\"payslip\">".$this->formatMoney($row['balance'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['principal'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['interest'], true)."</td><td class=\"payslip\">".$this->formatMoney($row['totalrepayment'], true)."</td></tr>";													
					}
					
					$total_emi = ($total_principal+$total_interest);
					//echo "<tr class=\"payslip\"><td colspan=\"7\"></td></tr>";				
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong> Totals </strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_principal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_interest, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_emi, true)."</strong></td></tr>";
					echo '</table>';
					
				} else {
					return $schedule;				
				}				
			}
			
			//echo json_encode(array('retcode'=>'000','retmsg'=>'','results'=>$schedule));
		}else{
			echo json_encode(array('retcode'=>'004','retmsg'=>'Product not found','results'=>''));
		}
	}

	//Loan Application	
	public function loanapplication($disbursementdate,$customerid,$loanterm,$productid,$graceperiod,$amount,$loanreason,$fundsource,$applicationnote,$applicationby,$guarantors) {
		
		$date = date('Y-m-d h:i:s');
		
		/* Check if user exists   */
		$sql_checkuser="SELECT * FROM tb_users WHERE userid = $applicationby AND active =1";
		
		if($res=$this->dbconnect->query($sql_checkuser)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
				return;
			}
		} else {
			echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
			return;			
		}
		
		//Check if product is available and active
		$sql_productsettings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
			settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
			FROM v_getloanproducts WHERE productid = ? AND active = ?");
		$sql_productsettings->execute(array($productid,1));		
		$productsettings = $sql_productsettings->fetchAll(PDO::FETCH_ASSOC); 
		
		if(!empty($productsettings)){		
			$product_settings  = json_decode($productsettings[0]["settings"], true);		
			
			$chk_interestcalculationmethod = $product_settings['interestcalculationmethod'];			
			$chk_interestrate = $product_settings['interestrate'];
			$chk_graceperiod = $product_settings['graceperiod'];
			$chk_graceperiodon = $product_settings['graceperiodon'];
			$chk_daycount = $product_settings['daycount'];
			$chk_ignoreholidays = $product_settings['ignoreholidays'];
			$chk_minloanamount = $product_settings['minloanamount']; 
			$chk_maxloanamount = $product_settings['maxloanamount'];
			$chk_minloanterm = $product_settings['minloanterm']; 
			$chk_maxloanterm = $product_settings['maxloanterm'];		
		} else {
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>'Loan product do not exists'));
			return;
		}
		
		//check if loan amount is valid
		if(($amount > $chk_maxloanamount) || ($amount < $chk_minloanamount)){
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>'Loan amount is not within the limit'));
			return;
		}
		
		//check if loan term is valid
		if(($loanterm > $chk_maxloanterm) || ($loanterm < $chk_minloanterm)){
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>'Loan term is not within the limit'));
			return;
		}		
		
			

	    //Check number of existing loans
		$sql_customerloans = $this->dbconnect->prepare("SELECT COUNT(*) AS noofloans FROM tb_loans WHERE customerid = ? AND loanstatus = 3 AND productid = ?");
		$sql_customerloans->execute(array($customerid,$productid));		
		$customerloans = $sql_customerloans->fetchAll(PDO::FETCH_ASSOC);
		
		if(!empty($customerloans)){	
			if($customerloans[0]["noofloans"] >= $productsettings[0]["maxaccounts"]){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Maximum no of loans reached','results'=>''));
				return;
			}				
		}
		
		//Check if product is available and active
		$sql_customerdetails = $this->dbconnect->prepare("SELECT * FROM v_customerdetails WHERE customerid = ?");
		$sql_customerdetails->execute(array($customerid));		
		$customerdetails = $sql_customerdetails->fetchAll(PDO::FETCH_ASSOC);
		
		if(empty($customerdetails)){	
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>'Customer id not found'));
			return;			
		}		
		//Get Loan Schedule
		$schedule = $this->getschedule($productid,$amount,$loanterm,$disbursementdate,0);		
		$rate = $chk_interestrate;
		$repaymentamount = $schedule[0]['totalrepayment'];
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
                        // Create a loan account
                        $res_account = $this->dbconnect->prepare("INSERT INTO tb_accounts(accountnumber, customerid,productid,drAmount,crAmount,accountstatus,balancedate,active,creationdate) "
                                  . "VALUES(?,?,?,?,?,?,?,?,?)");
			$res_account->execute(array('LN-'.$customerid.'-'.$productid,$customerid,$productid,$amount,'',1,NOW(),1, NOW()));
			$accountid = $this->dbconnect->lastInsertId();
			
			$res_loan = $this->dbconnect->prepare("INSERT INTO tb_loans(customerid,accountid,term,graceperiod,productid,loanstatus,amount,rate,repaymentamount,loanreason,sourceoffund,expecteddisbursementdate,applicationdate,applicationnote,applicationby) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$res_loan->execute(array($customerid,$accountid,$loanterm,$graceperiod,$productid,1,$amount,$rate,$repaymentamount,$loanreason,$fundsource,$disbursementdate,$date,$applicationnote,$applicationby));
			
			$loanid = $this->dbconnect->lastInsertId();
			
			foreach($schedule as $row ){													
				$res_loan = $this->dbconnect->prepare("INSERT INTO tb_loanschedule(loanid,period,repaymentdate,periodlenght,balance,principal,interest,repayment) VALUES(?,?,?,?,?,?,?,?)");
				$res_loan->execute(array($loanid,$row['period'],$row['repaymentdate'],$row['interval'],$row['balance'],$row['principal'],$row['interest'],$row['totalrepayment']));
			}
			
                        // Create Loan account
                         //$this->addSavingsAccount($customerid,$productid,$openingbalance);
			//Process Guarantors
			$guarantors_array = json_decode($guarantors, true);
			if (!empty($guarantors_array)) {
                                    	
					$check_loan_guarantors = $this->dbconnect->prepare("DELETE FROM tb_guarantors WHERE loanid=?");
					$check_loan_guarantors->execute(array($loanid));
					
					foreach($guarantors_array as $guarantor) {		
						$res_loan_guarantors = $this->dbconnect->prepare("INSERT INTO tb_guarantors(loanid,customerid,guarantor_idno,guarantor_name,guarantor_occupation,guarantor_mobile) VALUES(?,?,?,?,?,?)");
						$res_loan_guarantors->execute(array($loanid,$guarantor['customerid'],$guarantor['guarantor_idno'],$guarantor['guarantor_name'],$guarantor['guarantor_occupation'],$guarantor['guarantor_mobile']));										
					}
				}
				
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Loan Application Created Successfully','results'=>$loanid));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>$e->getMessage()));
		}
	}
        
        //Get applied Loans for processing
    public function customerloans($page,$rows,$fromdate,$todate){ 	
		$result = array();
		$userid = $_SESSION['userid'];
                $user = "SELECT tb_customer.customerid FROM tb_customer "
                        . "INNER JOIN tb_customerperson ON tb_customerperson.customerid = tb_customer.customerid "
                        . "INNER JOIN tb_person ON tb_customerperson.personid = tb_person.personid "
                        . "WHERE tb_person.loginid = $userid";
                 $rs=$this->dbconnect->query($user);
		foreach($rs As $row)  
                    $customerid = $row['customerid'];
		    $offset = ($page-1)*$rows;
		
		$sql_count="SELECT count(*) As total FROM tb_loans 
		INNER JOIN tb_customer
		ON tb_customer.customerid = tb_loans.customerid
		INNER JOIN tb_products
		ON tb_products.productid = tb_loans.productid
                WHERE loanstatus = 1
		AND tb_loans.customerid = $customerid AND applicationdate BETWEEN '$fromdate' AND '$todate' ";
		
		
		$rs=$this->dbconnect->query($sql_count);
		foreach($rs as $r) {
		$result["total"] = $r['total'];
                }	
		$sql="SELECT loanid,tb_loans.customerid,tb_customer.name,tb_loans.productid,tb_products.description AS productdesc,loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = loanstatus) AS loanstatusdesc,
		amount,term,rate,graceperiod,repaymentamount,expecteddisbursementdate,
		loanreason,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 24 AND valueMember = loanreason) AS loanreasondesc,
		sourceoffund,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 23 AND valueMember = sourceoffund) AS sourceoffunddesc,
		applicationdate,applicationnote 
		FROM tb_loans 
		INNER JOIN tb_customer
		ON tb_customer.customerid = tb_loans.customerid
		INNER JOIN tb_products
		ON tb_products.productid = tb_loans.productid
		WHERE  tb_loans.customerid = $customerid  AND applicationdate BETWEEN '$customerid' AND '$todate' 
		ORDER BY applicationdate DESC limit $offset,$rows";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();

			foreach($res as $row) {			
				$node['loanid']=$row['loanid'];
				$node['customerid']=$row['customerid'];				
				$node['productid']=$row['productid'];
				$node['name']=$row['name'];
				$node['productdesc']=$row['productdesc'];				
				$node['loanstatus']=$row['loanstatus'];						
				$node['loanstatusdesc']=$row['loanstatusdesc'];
				$node['amount']=$row['amount'];
				$node['term']=$row['term'];	
				$node['rate']=$row['rate'];	
				$node['graceperiod']=$row['graceperiod'];	
				$node['repaymentamount']=$row['repaymentamount'];	
				$node['expecteddisbursementdate']=$row['expecteddisbursementdate'];	
				$node['loanreason']=$row['loanreason'];	
				$node['loanreasondesc']=$row['loanreasondesc'];
				$node['sourceoffund']=$row['sourceoffund'];	
				$node['sourceoffunddesc']=$row['sourceoffunddesc'];	
				$node['applicationdate']=$row['applicationdate'];	
				$node['applicationnote']=$row['applicationnote'];				
								
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No transaction found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get applied Loans for processing
    public function loanapplicationprocessing($page,$rows,$customerid,$loanid,$fromdate,$todate,$loanstatus,$branchid){ 	
		$result = array();
                $cond = '';
		if($customerid != null)
                {
                   $cond .= " AND (tb_person.idno = $customerid) ";
                }
                if($loanid != null)
                {
                   $cond .= " AND (loanid = $loanid ) ";
                }
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT count(loanid) As num FROM tb_loans 
		INNER JOIN tb_customer
		ON tb_customer.customerid = tb_loans.customerid
		INNER JOIN tb_products
		ON tb_products.productid = tb_loans.productid
                INNER JOIN tb_customerperson
                ON tb_customer.customerid = tb_customerperson.customerid
                INNER JOIN tb_person
                ON tb_customerperson.personid = tb_person.personid
		WHERE loanstatus = $loanstatus
		$cond	
		AND applicationdate BETWEEN '$fromdate' AND '$todate' 
		";
		
		
		$rs=$this->dbconnect->query($sql_count);
		foreach($rs As $row)
		$result["total"] = $row['num'];
				
		$sql="SELECT loanid,tb_loans.customerid,tb_person.idno,tb_customer.name,tb_loans.productid,tb_products.description AS productdesc,loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = loanstatus) AS loanstatusdesc,
		amount,term,rate,graceperiod,repaymentamount,expecteddisbursementdate,
		loanreason,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 24 AND valueMember = loanreason) AS loanreasondesc,
		sourceoffund,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 23 AND valueMember = sourceoffund) AS sourceoffunddesc,
		applicationdate,applicationnote 
		FROM tb_loans 
		INNER JOIN tb_customer
		ON tb_customer.customerid = tb_loans.customerid
		INNER JOIN tb_products
		ON tb_products.productid = tb_loans.productid
                INNER JOIN tb_customerperson
                ON tb_customer.customerid = tb_customerperson.customerid
                INNER JOIN tb_person
                ON tb_customerperson.personid = tb_person.personid
		WHERE loanstatus = $loanstatus
		$cond
		AND applicationdate BETWEEN '$fromdate' AND '$todate' 
		ORDER BY loanid limit $offset,$rows";
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();

			foreach($res as $row) {			
				$node['loanid']=$row['loanid'];
				$node['customerid']=$row['idno'];				
				$node['productid']=$row['productid'];
				$node['name']=$row['name'];
				$node['productdesc']=$row['productdesc'];				
				$node['loanstatus']=$row['loanstatus'];						
				$node['loanstatusdesc']=$row['loanstatusdesc'];
				$node['amount']=$row['amount'];
				$node['term']=$row['term'];	
				$node['rate']=$row['rate'];	
				$node['graceperiod']=$row['graceperiod'];	
				$node['repaymentamount']=$row['repaymentamount'];	
				$node['expecteddisbursementdate']=$row['expecteddisbursementdate'];	
				$node['loanreason']=$row['loanreason'];	
				$node['loanreasondesc']=$row['loanreasondesc'];
				$node['sourceoffund']=$row['sourceoffund'];	
				$node['sourceoffunddesc']=$row['sourceoffunddesc'];	
				$node['applicationdate']=$row['applicationdate'];	
				$node['applicationnote']=$row['applicationnote'];				
								
				array_push($items, $node);
			}
			$result["rows"] = $items;
			
			echo json_encode($result);				
		}else{
			$retcode='003';
			$retmsg='No transaction found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Approve Loans	
	public function approveloans($approvedby,$approvalnote,$loan,$approve) {
		$date = date('Y-m-d');
		
		/* Check if user exists   */
		$sql_checkuser="SELECT * FROM tb_users WHERE userid = $approvedby AND active =1";
		
		if($res=$this->dbconnect->query($sql_checkuser)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
				return;
			}
		} else {
			echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
			return;			
		}
		
		
		if ($approve == 1)
		{
			$loanstatus = 2;
			$loanstatusdesc = 'Approved';
		}else
		{
			$loanstatus = 4;
			$loanstatusdesc = 'Rejected';
		}
		
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();
			
			
				$res_approveloan = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus = ?,approvedon = ?,approvedamount = amount,approvedby = ?,approvalnote = ? WHERE loanid = ?");
				$res_approveloan->execute(array($loanstatus,$date,$approvedby,$approvalnote,$loan));
				
				
						
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Loan '.$loanstatusdesc.' Successfully','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured','results'=>$e->getMessage()));
		}
	}

	//Disburse Loans	
	public function disburseloans($disbursedby,$disbursenote,$loans,$disbursementoption,$disbursementaccount,$disbursementdate) {
		$date = date('Y-m-d');
		$loanids = '';
		$count = 0;
		$loans = json_decode($loans, true);
			
		//Check if period is active
		$sql_checkperiod="SELECT * FROM tb_period WHERE '".$disbursementdate."' BETWEEN startdate AND enddate";
		if($res=$this->dbconnect->query($sql_checkperiod)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
				return;
			}
		} else {
			echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed','results'=>''));
			return;			
		}	
		
		/* Check if user exists   */
		$sql_checkuser="SELECT * FROM tb_users WHERE userid = $disbursedby AND active =1";
		
		if($res=$this->dbconnect->query($sql_checkuser)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
				return;
			}
		} else {
			echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
			return;			
		}
			
		foreach($loans as $loan){
			//Get the loan details				
			$sql_loandetails = $this->dbconnect->prepare("SELECT customerid,productid,amount FROM tb_loans WHERE loanid = ?");
			$sql_loandetails->execute(array($loan['loanid']));		
			$loandetails = $sql_loandetails->fetchAll(PDO::FETCH_ASSOC);
			
			if (!empty($loandetails)){
				foreach($loandetails as $row){
					$customerid = $row['customerid'];
					$productid = $row['productid'];
					$amount = $row['amount'];
				}
			}else{
				$retcode = '003';
				$retmsg  = 'Loan details not found for loan ID ('.$loan['loanid'].')';
				$results = '';	
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));				
				return;
			}
			
			$sql_controlaccount = $this->dbconnect->prepare("SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
				INNER JOIN tb_ledgeraccounts
				ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
				WHERE tb_ledgerlinks.productid = ?
				AND  tb_ledgerlinks.gltype = 1");
			$sql_controlaccount->execute(array($productid));		
			$controlaccount = $sql_controlaccount->fetchAll(PDO::FETCH_ASSOC);

			if (!empty($controlaccount)){
				foreach($controlaccount as $row){
					$debitaccid = $row['accountid'];
				}
			}else{
				$retcode = '003';
				$retmsg  = 'Loan product control account not found for Product ID ('.$productid.')';
				$results = '';	
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));	
				return;
			}
			
			$sql_contraaccount = $this->dbconnect->prepare("SELECT accountid FROM tb_ledgeraccounts WHERE accountid = ?");
			$sql_contraaccount->execute(array($disbursementaccount));		
			$contraaccount = $sql_contraaccount->fetchAll(PDO::FETCH_ASSOC);

			if (!empty($contraaccount)){
				foreach($contraaccount as $row){
					$creditaccid = $row['accountid'];
				}
			}else{
				$retcode = '003';
				$retmsg  = 'Disbursement contra account not valid ('.$disbursementaccount.')';
				$results = '';	
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));	
				return;
			}					
		
			//Check if product is available and active
			$sql_productsettings = $this->dbconnect->prepare("SELECT maxaccounts,settings FROM v_getloanproducts WHERE productid = ? AND active = ?");
			$sql_productsettings->execute(array($productid,1));		
			$productsettings = $sql_productsettings->fetchAll(PDO::FETCH_ASSOC);
			
			if(!empty($productsettings)){		
				$product_settings  = json_decode($productsettings[0]["settings"], true);		
				$chk_fee = $product_settings['fee'];		
			} else {
				$retcode = '003';
				$retmsg  = 'Product does not exists';
				$results = '';	
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
				return;
			}

			//Check number of existing loans
			$sql_customerloans = $this->dbconnect->prepare("SELECT COUNT(*) AS noofloans FROM tb_loans WHERE customerid = ? AND loanstatus = 3 AND productid = ?");
			$sql_customerloans->execute(array($customerid,$productid));		
			$customerloans = $sql_customerloans->fetchAll(PDO::FETCH_ASSOC);
			
			if(!empty($customerloans)){	
				if($customerloans[0]["noofloans"] >= $productsettings[0]["maxaccounts"]){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Maximum no of loans reached','results'=>''));
					return;
				}				
			}
			
			$trxdescription = 'Loan Disbursement';
			$valuedate = $disbursementdate;
			$userid = $disbursedby;				
			$feeid = 0;
			$feeamount = 0;
			$iscredit = 0;				
			$transactiontype = 5;
						
			$fees = array();
			
			//Get Disbursement Fee
			if(is_array($chk_fee)){
				if (!empty($chk_fee)){
					foreach($chk_fee as $row){
						$sql_getfee = $this->dbconnect->prepare("SELECT feeid,feetype,amount FROM tb_fee WHERE feeid = ? AND active = ?");
						$sql_getfee->execute(array($row['feeid'],1));		
						$getfee = $sql_getfee->fetchAll(PDO::FETCH_ASSOC);
						
						if (!empty($getfee)){
							foreach($getfee as $row_fee){
								if($row_fee['feetype'] == 1){
									$feeamount += $row_fee['amount'];
									$fees[] = array('feeid'=>$row_fee['feeid'],'amount'=>$row_fee['amount']);
								}elseif($row_fee['feetype'] == 2){
									$feeamount += $row_fee['amount'];
									$fees[] = array('feeid'=>$row_fee['feeid'],'amount'=>$row_fee['amount']);
								}elseif($row_fee['feetype'] == 3){
									$feeamount += $amount*(float)($row_fee['amount']/100);
									$fees[] = array('feeid'=>$row_fee['feeid'],'amount'=>$amount*(float)($row_fee['amount']/100));
								}
							}
						}
					}					
				}
			}
			
			//Get fee GLS for posting the transaction			
			$disbursementfee = array();
			foreach($fees as $fee_row){
				$sql_disbursement_fee = $this->dbconnect->prepare("SELECT tb_ledgeraccounts.accountid FROM tb_ledgerlinks
										INNER JOIN tb_ledgeraccounts
										ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
										WHERE tb_ledgerlinks.feeid = ?
										AND  tb_ledgerlinks.gltype = 2");
				$sql_disbursement_fee->execute(array($fee_row['feeid']));		
				$disbursement_fee = $sql_disbursement_fee->fetchAll(PDO::FETCH_ASSOC);
				
				if (!empty($disbursement_fee)){
					foreach($disbursement_fee as $feeaccount){
						$disbursementfee[] = array('feeid'=>$fee_row['feeid'],'amount'=>$fee_row['amount'],'accountid'=>$feeaccount['accountid']);
					}
				}				
			}
										
			//Less fee from disbursement amount
			$amount = $amount - $feeamount;

			try{
				$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbconnect->beginTransaction();
							
				//Open loan account
				$res_account = $this->dbconnect->prepare("INSERT INTO tb_accounts(customerid,productid,drAmount,crAmount,accountstatus,balancedate,active,creationdate) VALUES(?,?,?,?,?,?,?,?)"); 			
				$res_account->execute( array($customerid,$productid,0,0,1,$date,1,$date));
							
				$accountid = $this->dbconnect->lastInsertId();
				$accountnumber = substr("0000000000".$accountid, -8);
				
				$res_account = $this->dbconnect->prepare("UPDATE tb_accounts SET accountnumber=? WHERE accountid=?"); 			
				$res_account->execute( array($accountnumber,$accountid));
				
				//Post transaction to customer account				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
				$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
				
				$receiptno = $this->dbconnect->lastInsertId();	
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array($receiptno,$valuedate,$accountid,$feeid,$iscredit,$amount,$transactiontype,$debitaccid,$creditaccid));
				
				$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
				$res_posttrx->execute( array($amount,$accountid));				
				
				//Post Ledger transactions
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
				$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
				
				$ledgerid = $this->dbconnect->lastInsertId();

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$amount));			

				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$amount));	
				
				//Post Disbursement Fee
				if (!empty($disbursementfee)){
					foreach($disbursementfee as $rowfee){
						$fee_creditaccid = $rowfee['accountid'];
						$fee_amount = $rowfee['amount'];
						$fee_feeid = $rowfee['feeid'];
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array($receiptno,$valuedate,$accountid,$fee_feeid,$iscredit,$fee_amount,$transactiontype,$debitaccid,$fee_creditaccid));

						$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET drAmount=drAmount+? WHERE accountid=?"); 			
						$res_posttrx->execute( array($fee_amount,$accountid));
				
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$fee_amount));			

						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,$valuedate,$fee_creditaccid,1,$fee_amount));							
					}
				}	
				
				//Update Loans Table
				$res_disburseloan = $this->dbconnect->prepare("UPDATE tb_loans SET accountid=?,loanstatus = ?,disbursedon = ?,disbursedamount = ?,disbursedby = ?,disbursenote = ?,disbursementoption = ? WHERE loanid = ?");
				$res_disburseloan->execute(array($accountid,3,$valuedate,$amount,$disbursedby,$disbursenote,$disbursementoption,$loan['loanid']));
				
				$results = '';				
				$this->dbconnect->commit();									
			} catch (Exception $e){
				$this->dbconnect->rollBack();
				
				$retcode = '003';
				$retmsg  = 'Error occured while disbursing loan ('.$loan['loanid'].') Error - '.$e->getMessage();
				$results = $e->getMessage();					
			}
			
			if ($results <> ''){
				echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
				return;
			}
			
			if ($count == 0){
				$loanids = $loan['loanid'];
			}else{
				$loanids = $loanids.','.$loan['loanid'];
			}
			$count++;
		}
		
		$retcode = '000';
		$retmsg  = 'Loan Disbursed Successfully Loan ID(s) ('.$loanids.')';
		$results = '';		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));	
	}

	//Get Period Years
	public function getPeriodYear($page,$rows) {
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*) FROM tb_periodyear";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();;
		$result["total"] = $row[0];
			
		$sql="SELECT periodyearid,code,eoydone,createdon FROM tb_periodyear limit $offset,$rows";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['periodyearid']=$row['periodyearid'];
				$node['code']=$row['code'];				
				$node['eoydone']=$row['eoydone'];				
				$node['createdon']=$row['createdon'];				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($result);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Period Year
	public function addPeriodYear($code,$eoydone,$createdby){
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_periodyear = $this->dbconnect->prepare("INSERT INTO tb_periodyear(code,eoydone,createdby) VALUES(?,?,?)");
			$res_periodyear->execute(array($code,$eoydone,$createdby));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Year Added','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured','results'=>$e->getMessage()));
		}		
	}

	//Edit Period Year
	public function editPeriodYear($periodyearid,$code,$createdby) {	
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_periodyear = $this->dbconnect->prepare("UPDATE tb_periodyear SET code = ? WHERE periodyearid = ?");
			$res_periodyear->execute(array($code,$periodyearid));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Year Added','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured','results'=>$e->getMessage()));
		}	
	}

	//Get Periods
	public function getPeriods($page,$rows) {
		$result = array();
		
		$offset = ($page-1)*$rows;
		
		$sql_count="SELECT COUNT(*) FROM tb_period";
		
		$rs=$this->dbconnect->query($sql_count);
		$row=$rs->fetch();
		$result["total"] = $row[0];
			
		$sql="SELECT periodid,periodyearid,monthno,quarter,code,description,startdate,enddate,closed,createdon,createdby FROM  tb_period limit $offset,$rows";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['periodid']=$row['periodid'];
				$node['periodyearid']=$row['periodyearid'];
				$node['monthno']=$row['monthno'];	
				$node['quarter']=$row['quarter'];
				$node['code']=$row['code'];
				$node['description']=$row['description'];				
				$node['startdate']=$row['startdate'];
				$node['enddate']=$row['enddate'];
				$node['closed']=$row['closed'];				
				$node['createdon']=$row['createdon'];
				$node['createdby']=$row['createdby'];					
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($result);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Search Periods
	public function getPeriodSearch($periodyearid) {
		$sql="SELECT periodid,periodyearid,monthno,quarter,code,description,startdate,enddate,closed,createdon,createdby FROM  tb_period WHERE periodyearid = ".$periodyearid;
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['periodid']=$row['periodid'];
				$node['periodyearid']=$row['periodyearid'];
				$node['monthno']=$row['monthno'];	
				$node['quarter']=$row['quarter'];
				$node['code']=$row['code'];
				$node['description']=$row['description'];				
				$node['startdate']=$row['startdate'];
				$node['enddate']=$row['enddate'];
				$node['closed']=$row['closed'];				
				$node['createdon']=$row['createdon'];
				$node['createdby']=$row['createdby'];					
				
				array_push($items, $node);
			}
				
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Get Period Year Search
	public function getPeriodYearSearch() {
		$result = array();

		$sql="SELECT periodyearid,code FROM tb_periodyear";
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['periodyearid']=$row['periodyearid'];
				$node['code']=$row['code'];				
				
				array_push($items, $node);
			}
		
			$result["rows"] = $items;
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}
	
	//Add Period Year
	public function addPeriods($periodyearid,$monthno,$quarter,$code,$description,$startdate,$enddate,$closed,$createdby) {
		try{
			$date = date('Y-m-d');
		
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_period = $this->dbconnect->prepare("INSERT INTO tb_period(periodyearid,monthno,quarter,code,description,startdate,enddate,closed,createdon,createdby) VALUES(?,?,?,?,?,?,?,?,?,?)");
			$res_period->execute(array($periodyearid,$monthno,$quarter,$code,$description,$startdate,$enddate,$closed,$date,$createdby));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Year Period Added','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured','results'=>$e->getMessage()));
		}			
	}

	//Edit Period Year
	public function editPeriods($periodid,$periodyearid,$monthno,$quarter,$code,$description,$startdate,$enddate,$createdby) {	
		try{
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_period = $this->dbconnect->prepare("UPDATE tb_period SET periodyearid = ?,monthno = ?,quarter = ?,code = ?,description = ?,startdate = ?,enddate = ? WHERE periodid = ?");
			$res_period->execute(array($periodyearid,$monthno,$quarter,$code,$description,$startdate,$enddate,$periodid));
			
			$this->dbconnect->commit();
			
			echo json_encode(array('retcode'=>'000','retmsg'=>'Year Period Updated','results'=>''));			
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured','results'=>$e->getMessage()));
		}			
	}

	//End Of Year
	public function EOY($periodyearid,$userid) {		
	try{
		    /* Check if EOY is done */ 
			$yearcode = 0;
			$sql_periodyear="SELECT * FROM tb_periodyear WHERE periodyearid = ".$periodyearid." AND eoydone=0";
			
			if($res=$this->dbconnect->query($sql_periodyear)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'End of year is already done','results'=>''));
					return;
				}else{
					foreach($res as $row) {
						$yearcode = $row['code'];
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'A problem occurred when starting end of year process','results'=>''));
				return;			
			}
			
		    /* Check if user exists   */
			$sql_checkuser="SELECT * FROM tb_users WHERE userid = $userid AND active =1";
			
			if($res=$this->dbconnect->query($sql_checkuser)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid User. Check if user exist and is active','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid user. Check if user exist and is active','results'=>''));
				return;			
			}
						
			/* Validate if period year has periods defined */
			$yearstart= '';
			$yearend   = '';
			$sql_periods="SELECT  MIN(startdate) AS yearstart,MAX(enddate) AS yearend FROM tb_period WHERE periodyearid = ".$periodyearid;
			
			if($res=$this->dbconnect->query($sql_periods)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'No Periods defined for this period year','results'=>''));
					return;
				}else{
					foreach($res as $row) {
						$yearstart= $row['yearstart'];
						$yearend   = $row['yearend'];						
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Period Year','results'=>''));
				return;			
			}			
			/* Check if suspense account has balance */
			
			/* Check if the year has open periods */
			$sql_openperiods="SELECT  * FROM tb_period WHERE closed =0 AND periodyearid = ".$periodyearid;
			
			if($res=$this->dbconnect->query($sql_openperiods)){
				if($res->rowCount() > 0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'The year has open periods','results'=>''));
					return;
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Period Year','results'=>''));
				return;			
			}	
			
		    /* Get Retained earnings account*/
			$retainedearningsaccount =0 ;
			$sql_retainedearningsaccount ="SELECT accountid FROM tb_ledgeraccounts WHERE subformatid =41 AND formatid= 40 AND categoryid=4";
			
			if($res=$this->dbconnect->query($sql_retainedearningsaccount)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'Retained Earnings account not defined','results'=>''));
					return;
				}else{
					foreach($res as $row) {
						$retainedearningsaccount = $row['accountid'];
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured. Contact your system administrator','results'=>''));
				return;			
			}
			
		    /* Get EOY Transactions*/
			$ledgertrx = array();
			$sql_eoytransactions ="SELECT accountid,balance,CASE WHEN debit > credit THEN 1 ELSE 0 END AS trxtype,valuedate FROM (
					SELECT tb_ledgertransactions.accountid,SUM(CASE iscredit WHEN 0 THEN amount ELSE 0 END) AS debit,
					SUM(CASE iscredit WHEN 1 THEN amount ELSE 0 END) AS credit,
					ABS(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END)) AS balance,
					'".$yearend."' AS valuedate
					FROM tb_ledgeraccounts
					INNER JOIN tb_ledgercategory
					ON tb_ledgeraccounts.categoryid = tb_ledgercategory.categoryid
					INNER JOIN tb_ledgertransactions
					ON tb_ledgertransactions.accountid = tb_ledgeraccounts.accountid
					WHERE tb_ledgeraccounts.categoryid IN (1,2)
					AND tb_ledgertransactions.valuedate BETWEEN '".$yearstart."' AND '".$yearend."'
					GROUP BY tb_ledgertransactions.accountid
					HAVING balance<>0
					)EOYtransactions
					";
			
			if($res=$this->dbconnect->query($sql_eoytransactions)){
				if($res->rowCount()<=0){
					echo json_encode(array('retcode'=>'003','retmsg'=>'There are no transactions for this year','results'=>''));
					return;
				}else{		
					foreach($res as $row) {
						$node=array();
						$node['accountid']=$row['accountid'];
						$node['amount']=$row['balance'];				
						$node['trxtypeid']=$row['trxtype'];				
						$node['valuedate']=$row['valuedate'];				
						
						array_push($ledgertrx, $node);
					}
				}
			} else {
				echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured. Contact your system administrator','results'=>''));
				return;			
			}
			$trxtypeid = 0;
			
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,transactionnote) VALUES (?,?,?,?,?,?);"); 			
			$res_posttrx->execute( array(1,1,1,$yearend,$userid,'EOY Transactions'));
			
			$ledgerid = $this->dbconnect->lastInsertId();
			
			foreach($ledgertrx as $ldgtrx) {
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$ldgtrx['valuedate'],$ldgtrx['accountid'],$ldgtrx['trxtypeid'],$ldgtrx['amount']));	
				
				if($ldgtrx['trxtypeid'] == 1){
					$trxtypeid = 0;
				}else{
					$trxtypeid = 1;
				}
				
				$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
				$res_posttrx->execute( array($ledgerid,$ldgtrx['valuedate'],$retainedearningsaccount,$trxtypeid,$ldgtrx['amount']));				
			}

			$res_updateyear = $this->dbconnect->prepare("UPDATE tb_periodyear SET eoydone = 1 WHERE periodyearid = ?;"); 			
			$res_updateyear->execute( array($periodyearid));
			
			$this->dbconnect->commit();
		  
		  	$retcode='000';
			$retmsg='End Of Year Process Successfully';
			$results=$ledgerid;
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}		
		
	}

	//End Of Month
	public function processfinancialperiod($periodid,$status) {		
		try{								
			$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbconnect->beginTransaction();

			$res_posttrx = $this->dbconnect->prepare("UPDATE tb_period SET closed=? WHERE periodid = ?"); 			
			$res_posttrx->execute( array($status,$periodid));

			$this->dbconnect->commit();
		  
			$retcode='000';
			if($status == 1){
				$retmsg='Financial Period Closed Successfully';
			}else{
				$retmsg='Financial Period Opened Successfully';
			}
			$results = '';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
		} catch (Exception $e) {
		  $this->dbconnect->rollBack();
		  $retcode = '003';
		  $retmsg = $e->getMessage();
		  $results = '';
		  echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}				
	}

	//Search Fee
	public function searchguarantor($customerid) {
		$valuedate = date('Y-m-d');
		$result = array();
			
		$sql="SELECT customerid,idno,customername,productid,accountid,productname,balance FROM v_customeraccountdetails WHERE active=1 AND balance<>0 AND producttypeid IN (1,2) AND idno = ".$customerid."
		UNION ALL
		SELECT customerid, idno, customername,productid,accountid,productname,COALESCE((fn_getinterestbalance(accountid,'".$valuedate."')+fn_getprincipalbalance(accountid,'".$valuedate."')),0) AS balance FROM v_customeraccountdetails WHERE active=1 AND balance<>0 AND producttypeid IN (4) AND idno = ".$customerid;
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			$products = array();
			
			foreach($res as $row) {
				$node=array();
				$result["customerid"]=$row['customerid'];
				$result["customername"]=$row['customername'];
				
				$node['accountid']=$row['accountid'];	
				$node['productname']=$row['productname'];							
				$node['balance']=$this->formatMoney($row['balance'], true);	
				$node['accountdetails']=$row['productname'].' - '.$this->formatMoney($row['balance'], true);

				array_push($items, $node);
			}
		
			$result["products"] = $items;
			
			echo json_encode($result);
		}else{
			$retcode='003';
			$retmsg='No fee found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Add Loan Guarantors
	public function addLoanGuarantors($guarantors,$validate,$productid,$disbursedamount){			
		try{
			$guarantors_array = json_decode($guarantors, true);

			if (!empty($guarantors_array)){	
				//Get number of guarantor
				$noofguarantors = count($guarantors_array);
				$guarantee_percentage = 0;
				$total_guarantee = 0;
				$s_percentage = 0;
				
				foreach($guarantors_array as $validate_guarantor) {	
					if($validate == 1){
						if($disbursedamount == 0){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Disbursement Amount cannot be zero(0)','results'=>''));
							return;						
						}
						
						if($productid == 0){
							echo json_encode(array('retcode'=>'003','retmsg'=>'Product id not specified','results'=>''));
							return;												
						}
							
						$res_loan_guarantors_check = $this->dbconnect->prepare("SELECT tb_products.productid,tb_productloan.settings,? AS disbursedamount FROM tb_products
							INNER JOIN tb_productloan
							ON tb_productloan.productid = tb_products.productid 
							WHERE tb_products.productid = ?");
						$res_loan_guarantors_check->execute(array($disbursedamount,$productid));
						
					}else{
						$res_loan_guarantors_check = $this->dbconnect->prepare("SELECT tb_loans.productid,tb_productloan.settings,disbursedamount FROM tb_loans
							INNER JOIN tb_products
							ON tb_products.productid = tb_loans.productid
							INNER JOIN tb_productloan
							ON tb_productloan.productid = tb_products.productid 
							WHERE tb_loans.loanid=?");
						$res_loan_guarantors_check->execute(array($validate_guarantor['loanid']));	
					}
					
					$results = $res_loan_guarantors_check->fetchAll(PDO::FETCH_ASSOC);
			
					if (is_array($results)){
						if(count($results) != 0){
							$json_settings = html_entity_decode($results[0]['settings']);
							$disbursedamount = $results[0]['disbursedamount'];
							$settings = json_decode($json_settings, true);
							$s_percentage = $settings['percentage'];
							$s_noofguarantor = $settings['noofguarantor'];
							$s_useguaranteedshares = $settings['useguaranteedshares'];
													
							$s_guaranteeproducts = $settings['guaranteeproducts'];
							$guaranteeproducts = 0;
							if (is_array($s_guaranteeproducts)){
								if (!empty($s_guaranteeproducts)) {
									foreach($s_guaranteeproducts as $row) {
										$guaranteeproducts = $guaranteeproducts.','.$row['productid'];
									}
								}
							}

							if(strlen(trim($s_noofguarantor)) == 0)
							{
								$s_noofguarantor = 0;
							}
							//Check if number of guarantors is met
							if($s_noofguarantor != 0){
								if($noofguarantors != $s_noofguarantor){
									echo json_encode(array('retcode'=>'003','retmsg'=>'No of guarontors required is '.$s_noofguarantor.'','results'=>''));
									return;
								}
							}
							
							
							
							
							//$total_guarantee = $total_guarantee+$validate_guarantor['guaranteedamount'];							
						}else {
							echo json_encode(array('retcode'=>'003','retmsg'=>'Loan Product specified does not exist','results'=>''));
							return;								
						}
					}else{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Loan Product specified does not exist','results'=>''));
						return;						
					}			
				}//End of for loop	

							
				if($validate == 1){
					if($disbursedamount == 0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Disbursement Amount cannot be zero(0)','results'=>''));
						return;						
					}
					
					if($productid == 0){
						echo json_encode(array('retcode'=>'003','retmsg'=>'Product id not specified','results'=>''));
						return;												
					}
				
					$res_loan_guarantors_check = $this->dbconnect->prepare("SELECT tb_productloan.productid,tb_productloan.settings,? AS disbursedamount FROM tb_products
						INNER JOIN tb_productloan
						ON tb_productloan.productid = tb_products.productid 
						WHERE tb_products.productid = ?");
						
					if($res_loan_guarantors_check->execute(array($disbursedamount,$productid)))
					{
						$results = $res_loan_guarantors_check->fetchAll(PDO::FETCH_ASSOC);

						if (is_array($results)){
							if(count($results) != 0){
								$json_settings = html_entity_decode($results[0]['settings']);
								$disbursedamount = $results[0]['disbursedamount'];
								$settings = json_decode($json_settings, true);
								$s_noofguarantor = $settings['noofguarantor'];
								
								if(strlen($s_noofguarantor) == 0)
								{
									$s_noofguarantor = 0;
								}
								
								if(empty($guarantors_array))
								{
									$noofguarantors = 0;
								}else{
									$noofguarantors = count($guarantors_array);
								}

								//Check if number of guarantors is met
								if($s_noofguarantor != 0){
									if($noofguarantors != $s_noofguarantor){
										echo json_encode(array('retcode'=>'003','retmsg'=>'No of guarontors required is '.$s_noofguarantor.'','results'=>''));
										return;
									}
								}							
							}
						}
					}else{
						echo json_encode(array('retcode'=>'003','retmsg'=>'Unable to get product settings. Please contact system support','results'=>''));	
						return;							
					}				
				}else{				
					echo json_encode(array('retcode'=>'003','retmsg'=>'No guarantor specified','results'=>''));	
					return;
				}
			
				
				//echo json_encode(array('retcode'=>'000','retmsg'=>'Loan guarantors added successfully','results'=>''));			
			}
                  
		} catch (Exception $e) {
			$this->dbconnect->rollBack();
			echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured record cannot be created','results'=>$e->getMessage()));
		}
	}

	public function getLoanGuarantors($loanid) {			
		$sql="SELECT loanid AS guarantor_loan_id,customerid AS guarantor_customerid,(SELECT NAME FROM v_customerdetails WHERE v_customerdetails.customerid = guarantor_customerid) AS guarantor_name,guarantorstatus AS guarantor_status,guaranteedamount AS guarantor_amount FROM tb_guarantors WHERE loanid=".$loanid;
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			
			foreach($res as $row) {
				$node=array();
				$node['guarantor_loan_id']=$row['guarantor_loan_id'];
				$node['guarantor_customerid']=$row['guarantor_customerid'];				
				$node['guarantor_name']=$row['guarantor_name'];				
				$node['guarantor_amount']=$row['guarantor_amount'];				
				
				array_push($items, $node);
			}
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No record found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	//Transfer Search
	public function transfersearch($customerid,$source,$valuedate) {
		//Check if valuedate is set
		if ((strlen($valuedate) == 0) || ($valuedate == '1970-01-01'))
		{
			$valuedate = date('Y-m-d');
		}

		$result = array();

		if($source == 'from'){		
			$sql="SELECT customerid,customername,productid,accountid,productname,balance FROM v_customeraccountdetails WHERE balance <> 0 AND active=1 AND producttypeid IN (1,2)  AND customerid = ".$customerid;
		}else if($source == 'to'){				
			$sql="SELECT customerid,customername,productid,accountid,productname,balance FROM v_customeraccountdetails WHERE active=1 AND producttypeid IN (1,2) AND customerid = ".$customerid."
					UNION ALL
					SELECT customerid,customername,productid,accountid,productname,COALESCE((fn_getinterestdue(accountid,'".$valuedate."')+fn_getprincipalbalance(accountid,'".$valuedate."')),0) AS balance FROM v_customeraccountdetails WHERE active=1 AND producttypeid IN (4) AND customerid = ".$customerid." AND balance <>0";
		}else{
			echo json_encode($result);
			return;
		}
		
		if($res=$this->dbconnect->query($sql)){
			
			$items = array();
			$products = array();
			
			foreach($res as $row) {
				$node=array();
				
				$node['accountid']=$row['accountid'];	
				$node['accountdetails']=$row['productname'].' - '.$this->formatMoney($row['balance'], true);

				array_push($items, $node);
			}
			
			echo json_encode($items);
		}else{
			$retcode='003';
			$retmsg='No fee found';
			$results='';
			
			echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
		}	
	}

	public function passwordreset($userid,$oldpassword,$newpassword,$confirmpassword){		
		//Check if user exist
		$sql_checkuser="SELECT * FROM tb_users WHERE userid=".$userid;
		if($res=$this->dbconnect->query($sql_checkuser)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'User does not exist. Contact system support','results'=>''));
				return;
			}
		}else{
			echo json_encode(array('retcode'=>'003','retmsg'=>'User does not exist. Contact system support','results'=>''));
			return;
		}			
		//Check old password
		$sql_checkoldpassword="SELECT * FROM tb_users WHERE userid=".$userid." AND password=MD5('".$oldpassword."')";
		if($res=$this->dbconnect->query($sql_checkoldpassword)){
			if($res->rowCount()<=0){
				echo json_encode(array('retcode'=>'003','retmsg'=>'Old password is invalid. Please try again','results'=>''));
				return;
			}
		}else{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Old password is invalid. Please try again','results'=>''));
			return;
		} 		
		//matches a string of six or more characters
		if(strlen($newpassword) < 6)
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password too short!','results'=>''));
			return;			
		}
		//that contains at least one digit (0-9)
		if(!preg_match("#[0-9]+#",$newpassword))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one number!','results'=>''));
			return;			
		}
		//that contains at least one letter
		if(!preg_match("#[a-zA-Z]+#",$newpassword))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one letter!','results'=>''));
			return;			
		}
		//that contains at least one letter in uppercase
		if(!preg_match("#[A-Z]+#",$newpassword))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one uppercase letter!','results'=>''));
			return;			
		}	
		//that contains at least one letter in lowercase
		if(!preg_match("#[a-z]+#",$newpassword))
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Password must contain at least one lowercase letter!','results'=>''));
			return;			
		}		
		//password confirmation
		if($newpassword != $confirmpassword)
		{
			echo json_encode(array('retcode'=>'003','retmsg'=>'New Password and confirm password do not match!','results'=>''));
			return;			
		}
					
		$sql_passwordreset = $this->dbconnect->prepare("UPDATE tb_users SET password = MD5(?) WHERE userid = ?");		
		if($sql_passwordreset->execute(array($newpassword,$userid))){
			$session['mainurl'] = '../index.html';
			echo json_encode(array('retcode'=>'000','retmsg'=>'Password Reset Successful','results'=>$session));
			return;			
		}else{
			echo json_encode(array('retcode'=>'003','retmsg'=>'Could not reset your password. Please contact system support','results'=>''));
			return;
		}
	}
	
	//Loan Accounts
	public function getloanaccountsforreport($customerid,$idno,$iscustomerid) {	
		if ($iscustomerid == 1){
			$sql="SELECT loanid,CONCAT(accountid,' (',disbursedamount,'-',term,'-',(SELECT description FROM tb_products 
			WHERE tb_products.productid = tb_loans.productid),')') AS loaniddesc FROM tb_loans WHERE customerid =  $customerid";			
		} else {
			$sql="SELECT loanid,CONCAT(accountid,' (',disbursedamount,'-',term,'-',(SELECT description FROM tb_products WHERE tb_products.productid = tb_loans.productid),')') AS loaniddesc 
			FROM tb_loans 
			INNER JOIN tb_customerperson
			ON tb_loans.customerid = tb_customerperson.customerid
			INNER JOIN tb_person
			ON tb_person.personid = tb_customerperson.personid
			WHERE tb_person.idno =  '".$idno."'";
		}
		
		if($res=$this->dbconnect->query($sql)){		
			$items = array();
			foreach($res as $row) {
				$node=array();
				$node['loanid']=$row['loanid'];
				$node['loaniddesc']=$row['loaniddesc'];
				
				array_push($items, $node);
			}
			
			echo json_encode($items);
		}				
	}

	//M-Pesa Repayments Report
	public function getMpesaRepayments($fromdate,$todate,$paymentstatus,$paymentprocessing){	
		$str_paymentprocessing = '';
	
		if($paymentprocessing == 1){
			$str_paymentprocessing = 'UPLOAD';
		}else if($paymentprocessing == 2){
			$str_paymentprocessing = 'HTTP';
		}
		
		$rpt_mpesarepayments = $this->dbconnect->prepare("SELECT mpesa_msisdn,mpesa_code,mpesa_trx_date,mpesa_sender,mpesa_amt,receiptno,
			CASE processed 
			WHEN 0 THEN 'Not Processed' 
			WHEN 1 THEN 'Pocessed Successfully' 
			WHEN 2 THEN 'Customer Not Found' 
			WHEN 3 THEN 'General Failure' 
			ELSE 'General Failure' END AS processed,
			CASE routemethod_name WHEN 'HTTP' THEN 'IPN' WHEN 'UPLOAD' THEN 'File Upload' END AS paymentprocess,
			filename
			FROM tb_mpesarepayments 
			WHERE (processed = ?)
			AND (routemethod_name = ? OR  ? = '')
			AND DATE(mpesa_trx_date) BETWEEN ? AND ?					
			ORDER BY mpesa_trx_date	");
			
		$rpt_mpesarepayments->execute(array($paymentstatus,$str_paymentprocessing,$str_paymentprocessing,$fromdate,$todate));
		$results = $rpt_mpesarepayments->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
									 'mpesa_msisdn'=>$row['mpesa_msisdn']
									 , 'mpesa_code'=>$row['mpesa_code']
									 , 'mpesa_trx_date'=>$row['mpesa_trx_date']
									 , 'mpesa_sender'=>$row['mpesa_sender']
									 , 'mpesa_amt'=>$row['mpesa_amt']
									, 'receiptno'=>$row['receiptno']
									, 'processed'=>$row['processed']	
									, 'paymentprocess'=>$row['paymentprocess']	
									, 'filename'=>$row['filename']										
									);
			}
			
			echo json_encode($arr);
		}			
	}

	//Get Report
	public function SMS_Report($fromdate,$todate,$status,$phoneno) {	
		$sql="SELECT phoneno,message,datein,dateout,statusid,
		CASE 
		WHEN statusid = 0 AND sent = 0 THEN 'Not Processed' 
		WHEN statusid = 1 AND sent = 0 THEN 'Processing' 
		WHEN statusid = 2 AND sent = 1 THEN 'Message Sent Successfully'
		WHEN statusid = 3 AND sent = 1 THEN 'SMS Cancelled' 		
		ELSE 'Failed to process. Check on bulk sms portal' 
		END AS statusdesc 
		,batchid,
		CASE sent WHEN 1 THEN 'Processed' ELSE 'Not Processed' END AS smsstatus 
		FROM tb_quesms 
		WHERE (sent=".$status." OR ".$status."=9) 
		AND (DATE_FORMAT(datein,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."') 
		AND (phoneno='".$phoneno."' OR '".$phoneno."'='')";

		$results = $this->dbconnect->query($sql);

		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
			
			if (is_array($items) ) {
				$count=0;
				echo "<table id=\"report_table\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>No</strong></td><td  class=\"payslip\"><strong>Phone No</strong></td><td class=\"payslip\"><strong>SMS</strong></td><td class=\"payslip\"><strong>Date In
				</strong></td><td class=\"payslip\"><strong>Date Out</strong></td><td class=\"payslip\"><strong>Status</strong></td><td class=\"payslip\"><strong>Batch ID</strong></td>				</tr>";
				foreach( $items as $row ) {
					$count++;
					echo "<tr class=\"payslip\">";
					echo "<td class=\"payslip\">".$count."</td>";
					echo "<td class=\"payslip\">".$row['phoneno']."</td>";
					echo "<td class=\"payslip\">".$row['message']."</td>";
					echo "<td class=\"payslip\">".$row['datein']."</td>";
					echo "<td class=\"payslip\">".$row['dateout']."</td>";
					echo "<td class=\"payslip\">".$row['statusdesc']."</td>";
					echo "<td class=\"payslip\">".$row['batchid']."</td>";
					echo "</tr>";
				}
				echo "</table>";
			}
		}else{
			echo 'No data found';
		}
	}	
	
	//Get SMS
	public function getSMS($fromdate,$todate){	
		$get_sms = $this->dbconnect->prepare("SELECT queid,phoneno,datein,message FROM tb_quesms WHERE sent = 0 AND statusid = 0
			AND DATE(datein) BETWEEN ? AND ?					
			ORDER BY datein	");
			
		$get_sms->execute(array($fromdate,$todate));
		$results = $get_sms->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
							 'queid'=>$row['queid']
							 , 'phoneno'=>$row['phoneno']
							 , 'datein'=>$row['datein']
							 , 'message'=>$row['message']																			
							);
			}
			
			echo json_encode($arr);
		}			
	}
	
	//Update SMS
	public function UpdateSMSStatus($smsstatus,$smss,$issent) {
		if($issent == 1){
			$sql="UPDATE tb_quesms SET statusid = ".$smsstatus.",sent = 1,dateout=NOW() WHERE queid IN (".$smss.")";
		}else{
			$sql="UPDATE tb_quesms SET statusid = ".$smsstatus." WHERE queid IN (".$smss.")";				
		}
		
		$update=$this->dbconnect->prepare($sql);
		if ($update->execute()) {		
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		} else{
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';			
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}

	//Update Repayment
	public function UpdateMpesaRepayment($mpesacode,$mpesa_msisdn,$mpesa_sender) {
		$update=$this->dbconnect->prepare("UPDATE tb_mpesarepayments SET mpesa_sender=?,mpesa_msisdn=? WHERE mpesa_code=?");				
		if ($update->execute(array($mpesa_sender,$mpesa_msisdn,$mpesacode))) {		
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		} else{
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';			
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//Post Loan Repayment - When making changed to loan posting also check transfer posting
	public function repostmpesarepayment($mpesarepayments) {
		//loanrepaymenttop 
		$sql_update="UPDATE tb_mpesarepayments SET processed = 0 WHERE mpesa_code IN (".$mpesarepayments.")";
		$res=$this->dbconnect->query($sql_update);

		$sql_update_one="UPDATE tb_mpesarepayments LEFT JOIN v_customerdetails  
						ON SUBSTR(mpesa_msisdn,4,LENGTH(mpesa_msisdn)) = RIGHT(TRIM(v_customerdetails.mobileno),9) 
						SET processed=2 WHERE v_customerdetails.mobileno IS NULL AND receiptno IS NULL AND mpesa_code IS NOT NULL";
		$res=$this->dbconnect->query($sql_update_one);
				
		//trxreference
		$sql="SELECT rowid,mpesa_code AS trxdescription,mpesa_amt AS amount,v_customerdetails.customerid,mpesa_trx_date AS valuedate,1 AS userid,14 AS usercontraaccount,
			mpesa_sender,mpesa_msisdn
			FROM tb_mpesarepayments
			INNER JOIN v_customerdetails  
			ON SUBSTR(mpesa_msisdn,4,LENGTH(mpesa_msisdn)) = RIGHT(TRIM(v_customerdetails.mobileno),9) 
			WHERE processed = 0
			ORDER BY mpesa_trx_date";
		
		$items = array();
		
		if($res=$this->dbconnect->query($sql)){						
			foreach($res as $row) {
				$node=array();	
				$node['rowid']=$row['rowid'];
				$node['customerid']=$row['customerid'];
				$node['amount']=$row['amount'];			
				$node['userid']=$row['userid'];	
				$node['trxdescription']=$row['trxdescription'];
				$node['valuedate']=$row['valuedate'];					
				$node['usercontraaccount']=$row['usercontraaccount'];
				$node['mpesa_sender']=$row['mpesa_sender'];
				$node['mpesa_msisdn']=$row['mpesa_msisdn'];						
				array_push($items, $node);
			}
		}
		
		//Validate repayment dates before allocation
		foreach($items as $item_validate){			
			//Check if period is active
			$sql_checkperiod="SELECT * FROM tb_period WHERE '".$item_validate['valuedate']."' BETWEEN startdate AND enddate";			
			if($res=$this->dbconnect->query($sql_checkperiod)){
				if($res->rowCount()<=0){					
					$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=3 WHERE receiptno = ?"); 			
					$res_customerdoesnotexist->execute(array($item_validate['trxdescription']));	
					unset($res_customerdoesnotexist);
					
					//Remove item
					unset($items[$item_validate]);					
					//echo json_encode(array('retcode'=>'003','retmsg'=>'Transaction to a closed period not allowed : '.$item_validate['trxdescription'],'results'=>''));
					//return;
				}
			} 	
		}
		
		//Building CEMES migration logic
		//To do:-
		//1. Get fees dynamically
		//
		foreach($items as $item){ 
			$savingsaccount = 0;
			$amount    = $item['amount'];
			$feepaid   = 150;
			$feeamount = 0;
			$feepaid_la   = 350;
			$feeamount_la = 0;			
			$excessamount = 0;
			$registrationfee = 150;
			$loanapplicationfee = 350;
			$registrationfeeid = 1;
			$loanapplicationfeeid = 2;
			$usercontraaccount = 14;
			$sql_updateschedule = '';
			
			$sql="SELECT accountid,COALESCE((SELECT SUM(amount) FROM tb_transactions WHERE tb_transactions.accountid = tb_accounts.`accountid` AND feeid = 1),0) AS feepaid FROM tb_accounts WHERE customerid = ".$item['customerid']." AND productid = 1";
			if($res_savingsaccount=$this->dbconnect->query($sql)){	
				foreach($res_savingsaccount as $row) {
					$savingsaccount = $row['accountid'];
					$feepaid = $row['feepaid'];
				}
			}
			
			//Post fee 1 amount 150
			if($feepaid < $registrationfee)
			{
				$feeamount = $registrationfee - $feepaid;
				if($amount <= $feeamount){
					$feeamount = $amount;
					$amount = 0;					
				}else{
					$amount = $amount - $feeamount;
				}
			}
			
			//Allocate this particular repayment to all loans untill there is an excess
			$postrepayments = array();
			
			//Allocate loan application fee amount to be posted
			$loanapplicationfees = array();

			if($amount > 0)
			{				
				//Get payoffamount of the oldest active loan for the customer based on disbursement date
				$sql_activeloans = "SELECT accountid,loanid,COALESCE((fn_getinterestbalance(accountid,NOW())+fn_getprincipalbalance(accountid,NOW())),0) AS payoffamount,COALESCE(fn_getprincipalbalance(accountid,NOW()),0) AS principalamount,COALESCE(fn_getinterestbalance(accountid,NOW()),0) AS interestamount FROM tb_loans WHERE customerid = ".$item['customerid']." AND loanstatus = 3 ORDER BY disbursedon ASC";

				$loans_arr = array();
				if($res_activeloans=$this->dbconnect->query($sql_activeloans)){						
					foreach($res_activeloans as $activeloan) {
						$node_activeloan = array();
						$node_activeloan['accountid'] = $activeloan['accountid'];
						$node_activeloan['loanid'] = $activeloan['loanid'];
						$node_activeloan['payoffamount'] = $activeloan['payoffamount'];
						$node_activeloan['principalamount'] = $activeloan['principalamount'];
						$node_activeloan['interestamount'] = $activeloan['interestamount'];
						array_push($loans_arr, $node_activeloan);
					}
				}

				//Loop through active loans assgining amount to be paid
				$noofloans = 0;
				$count = 0;
				
				if(!empty($loans_arr)){
					$noofloans = count($loans_arr);
				}

				foreach($loans_arr as $loan)
				{
					if($amount > 0) {
						//Check for this particular loan if there is fee paid
						$sql_account = "SELECT accountid,COALESCE((SELECT SUM(amount) FROM tb_transactions WHERE tb_transactions.accountid = tb_accounts.`accountid` AND feeid = 2),0) AS feepaid FROM tb_accounts WHERE accountid = ".$loan['accountid'];
						if($res_account_la=$this->dbconnect->query($sql_account)){	
							foreach($res_account_la as $row_la) {
								$feepaid_la = $row_la['feepaid'];
							}
						}
						
						//Post fee 2 amount 350
						if($feepaid_la < $loanapplicationfee)
						{
							$feeamount_la = $loanapplicationfee - $feepaid_la;
							if($amount <= $feeamount_la){
								//fucking hell - forgot to have line below in place resulting to inncorrect over payment bcoz of loan application fee
								$feeamount_la = $amount;
								$amount = 0;					
							}else{
								$amount = $amount - $feeamount_la;
							}
							
							if($feeamount_la > 0){
								$node_laf = array();
								$node_laf['accountid'] = $loan['accountid'];
								$node_laf['feeamount'] = $feeamount_la;
								$node_laf['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
								array_push($loanapplicationfees, $node_laf);							
							}
						}				
					}
					if(($amount > 0) && ($loan['payoffamount'] > 0))
					{
						if($amount < $loan['payoffamount'])
						{
							$node_repayment = array();
							$node_repayment['accountid'] = $loan['accountid'];
							$node_repayment['loanid'] = $loan['loanid'];
							$node_repayment['amount'] = $loan['payoffamount'];
							$node_repayment['userid'] = $item['userid'];
							$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
							$node_repayment['valuedate'] = $item['valuedate'];
							$node_repayment['usercontraaccount'] = $item['usercontraaccount'];							
							
							$principalamount = 0;
							$interestamount = 0;	
											
							$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
							if($sql_loan_schedule->execute(array($loan['accountid'])))
							{														
								$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
								unset($sql_loan_schedule);	
								foreach($loanschedule as $row){
									$val_principal = $row['principal'];
									$val_interest = $row['interest'];
									$val_principalpaid = $row['principalpaid'];
									$val_interestpaid = $row['interestpaid'];							
									$val_loanscheduleid = $row['loanscheduleid'];
									$val_loanid = $row['loanid'];
									
									$principalpaid = 0;
									$interestpaid = 0;
									
									//Check for partial payments
									//Interest
									if($val_interestpaid > 0){
										$val_interest = $val_interest - $val_interestpaid;
									}
									//Principal
									if($val_principalpaid > 0)
									{
										$val_principal = $val_principal - $val_principalpaid;
									}
																		
									if($amount > 0)
									{
										if($amount <= $val_interest)
										{
											$principalpaid = 0;
											$interestpaid = $amount;
											
											$interestamount = $interestamount + $interestpaid;
											$principalamount = $principalamount + $principalpaid;
											$amount = $amount - $interestpaid;									
										}else{
											$interestpaid = $val_interest;
											$amount = $amount - $interestpaid;
											
											if($amount <= $val_principal){
												$principalpaid = $amount;
												$amount = $amount - $principalpaid;										
											}else{
												$principalpaid = $val_principal;
												$amount = $amount - $principalpaid;
											}									
											
											$principalamount = $principalamount + $principalpaid;
											$interestamount = $interestamount + $interestpaid;									
										}
										
										$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
									}							
								}
							}
							
							$node_repayment['principalamount'] = $principalamount; 
							$node_repayment['interestamount'] = $interestamount;
							$node_repayment['closeloan'] = 0;
							$node_repayment['updateschedule'] = $sql_updateschedule;

							array_push($postrepayments, $node_repayment);							
							$amount = 0;
						}else if($amount == $loan['payoffamount'])
						{
							$node_repayment = array();
							$node_repayment['accountid'] = $loan['accountid'];
							$node_repayment['loanid'] = $loan['loanid'];
							$node_repayment['amount'] = $loan['payoffamount'];
							$node_repayment['userid'] = $item['userid'];
							$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
							$node_repayment['valuedate'] = $item['valuedate'];
							$node_repayment['usercontraaccount'] = $item['usercontraaccount'];							

							$principalamount = 0;
							$interestamount = 0;	
											
							$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
							if($sql_loan_schedule->execute(array($loan['accountid'])))
							{																
								$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
								unset($sql_loan_schedule);
								foreach($loanschedule as $row){
									$val_principal = $row['principal'];
									$val_interest = $row['interest'];
									$val_principalpaid = $row['principalpaid'];
									$val_interestpaid = $row['interestpaid'];							
									$val_loanscheduleid = $row['loanscheduleid'];
									$val_loanid = $row['loanid'];
									
									$principalpaid = 0;
									$interestpaid = 0;
									
									//Check for partial payments
									//Interest
									if($val_interestpaid > 0){
										$val_interest = $val_interest - $val_interestpaid;
									}
									//Principal
									if($val_principalpaid > 0)
									{
										$val_principal = $val_principal - $val_principalpaid;
									}
																		
									if($amount > 0)
									{
										if($amount <= $val_interest)
										{
											$principalpaid = 0;
											$interestpaid = $amount;
											
											$interestamount = $interestamount + $interestpaid;
											$principalamount = $principalamount + $principalpaid;
											$amount = $amount - $interestpaid;									
										}else{
											$interestpaid = $val_interest;
											$amount = $amount - $interestpaid;
											
											if($amount <= $val_principal){
												$principalpaid = $amount;
												$amount = $amount - $principalpaid;										
											}else{
												$principalpaid = $val_principal;
												$amount = $amount - $principalpaid;
											}									
											
											$principalamount = $principalamount + $principalpaid;
											$interestamount = $interestamount + $interestpaid;									
										}
										
										$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
									}							
								}
							}
							
							$node_repayment['principalamount'] = $principalamount; 
							$node_repayment['interestamount'] = $interestamount;
							$node_repayment['closeloan'] = 1;
							$node_repayment['updateschedule'] = $sql_updateschedule;

							array_push($postrepayments, $node_repayment);							
														
							$amount = 0;
						}else{
							$amount_allocate = $loan['payoffamount'];
							$amount = $amount - $loan['payoffamount'];
							
							$node_repayment = array();
							$node_repayment['accountid'] = $loan['accountid'];
							$node_repayment['loanid'] = $loan['loanid'];
							$node_repayment['amount'] = $loan['payoffamount'];
							$node_repayment['userid'] = $item['userid'];
							$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
							$node_repayment['valuedate'] = $item['valuedate'];
							$node_repayment['usercontraaccount'] = $item['usercontraaccount'];

							$principalamount = 0;
							$interestamount = 0;	
											
							$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
							if($sql_loan_schedule->execute(array($loan['accountid'])))
							{																
								$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
								unset($sql_loan_schedule);
								foreach($loanschedule as $row){
									$val_principal = $row['principal'];
									$val_interest = $row['interest'];
									$val_principalpaid = $row['principalpaid'];
									$val_interestpaid = $row['interestpaid'];							
									$val_loanscheduleid = $row['loanscheduleid'];
									$val_loanid = $row['loanid'];
									
									$principalpaid = 0;
									$interestpaid = 0;
									
									//Check for partial payments
									//Interest
									if($val_interestpaid > 0){
										$val_interest = $val_interest - $val_interestpaid;
									}
									//Principal
									if($val_principalpaid > 0)
									{
										$val_principal = $val_principal - $val_principalpaid;
									}
																		
									if($amount_allocate > 0)
									{
										if($amount_allocate <= $val_interest)
										{
											$principalpaid = 0;
											$interestpaid = $amount_allocate;
											
											$interestamount = $interestamount + $interestpaid;
											$principalamount = $principalamount + $principalpaid;
											$amount_allocate = $amount_allocate - $interestpaid;									
										}else{
											$interestpaid = $val_interest;
											$amount_allocate = $amount_allocate - $interestpaid;
											
											if($amount_allocate <= $val_principal){
												$principalpaid = $amount_allocate;
												$amount_allocate = $amount_allocate - $principalpaid;										
											}else{
												$principalpaid = $val_principal;
												$amount_allocate = $amount_allocate - $principalpaid;
											}									
											
											$principalamount = $principalamount + $principalpaid;
											$interestamount = $interestamount + $interestpaid;									
										}
										
										$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
									}							
								}
							}
							
							$node_repayment['principalamount'] = $principalamount; 
							$node_repayment['interestamount'] = $interestamount;
							$node_repayment['closeloan'] = 1;
							$node_repayment['updateschedule'] = $sql_updateschedule;

							array_push($postrepayments, $node_repayment);
						}
					}
					
					$count = $count+1;
				}
								
				//Check for excess and post to savings after distributing to loans
				if(($amount > 0) && ($count == $noofloans)){
					$excessamount = $amount;
					$amount = 0;
				}
			}
			/*
			print_r($excessamount);
			echo '<br>';
			print_r($feeamount);
			echo '<br>';
			print_r($postrepayments);
			echo '<br>';
			print_r($loanapplicationfees);			
			return;
			*/
			//Post transactions 
			try
			{			
				$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbconnect->beginTransaction();
				
				$serverdate = date('Y-m-d');
				$receiptno = 0;
				$ledgerid = 0;
				
				//post registration fee if greater than 0 
				if($feeamount > 0){
					$iscredit = 1;
					$transactiontype = 3;
					$debitaccid = $usercontraaccount;
					$creditaccid = 1;
					$userid	= 1;			

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
					$res_posttrx->execute( array('M-Pesa Transaction - '.$item['trxdescription'],date("Y-m-d",strtotime($item['valuedate'])),$userid,0));
					unset($res_posttrx);
					
					$receiptno = $this->dbconnect->lastInsertId();	
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,date("Y-m-d",strtotime($item['valuedate'])),$savingsaccount,$registrationfeeid,$iscredit,$feeamount,$transactiontype,$debitaccid,$creditaccid));
					unset($res_posttrx);
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array(1,1,0,date("Y-m-d",strtotime($item['valuedate'])),$userid,$receiptno,'M-Pesa Transaction - '.$item['trxdescription']));
					unset($res_posttrx);
					
					$ledgerid = $this->dbconnect->lastInsertId();

					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$debitaccid,0,$feeamount));			
					unset($res_posttrx);
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$creditaccid,1,$feeamount));	
					unset($res_posttrx);				
				}
							
				//Post Loan Application Fee
				if(!empty($loanapplicationfees)){
					foreach($loanapplicationfees as $laf){					
						$iscredit = 1;
						$transactiontype = 3;
						$debitaccid = $usercontraaccount;
						$creditaccid = 3;
						$userid	= 1;	
						
						if($receiptno == 0){
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
							$res_posttrx->execute( array($laf['trxdescription'],date("Y-m-d",strtotime($item['valuedate'])),$userid,0));
							unset($res_posttrx);
							
							$receiptno = $this->dbconnect->lastInsertId();	
						}
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array($receiptno,date("Y-m-d",strtotime($item['valuedate'])),$laf['accountid'],$loanapplicationfeeid,$iscredit,$laf['feeamount'],$transactiontype,$debitaccid,$creditaccid));
						unset($res_posttrx);
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,date("Y-m-d",strtotime($item['valuedate'])),$userid,$receiptno,$laf['trxdescription']));
						unset($res_posttrx);
						
						$ledgerid = $this->dbconnect->lastInsertId();

						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$debitaccid,0,$laf['feeamount']));			
						unset($res_posttrx);
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$creditaccid,1,$laf['feeamount']));	
						unset($res_posttrx);
					}					
				}
				//loop through repayments and post. NB: In this case there is no overpayments
				if(!empty($postrepayments)){
					foreach($postrepayments as $postrepayment){
						$accountid   = $postrepayment['accountid'];
						$loanid   = $postrepayment['loanid'];
						$amount   = $postrepayment['amount'];
						$userid   = $postrepayment['userid'];
						$trxdescription   = $postrepayment['trxdescription'];
						$valuedate   = date("Y-m-d",strtotime($postrepayment['valuedate']));
						$usercontraaccount   = $postrepayment['usercontraaccount'];	
						$principalamount   = $postrepayment['principalamount'];	
						$interestamount   = $postrepayment['interestamount'];	
						$closeloan   = $postrepayment['closeloan'];
						
						//Set receipt
						if($receiptno == 0){
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
							$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
							unset($res_posttrx);
							
							$receiptno = $this->dbconnect->lastInsertId();	
						}
						
						//Set ledger
						if($ledgerid == 0){
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
							unset($res_posttrx);
							$ledgerid = $this->dbconnect->lastInsertId();
						}
						
						//Post Principal 
						if($principalamount > 0){
							$debitaccid = $usercontraaccount;
							$creditaccid = 9;
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,1,$principalamount,6,$debitaccid,$creditaccid));
							unset($res_posttrx);
							$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
							$res_posttrx->execute( array($principalamount,$accountid));
							unset($res_posttrx);
						
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$principalamount));			
							unset($res_posttrx);
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$principalamount));	
							unset($res_posttrx);
						}			
									
						if($interestamount > 0){
							//Post Interest Charged against loan account (but it should not affect loan balance) ---> DR - Interest receivable CR - Interest Income
							$interestincome = 11;
							$interestreceivable = 13;
							
							$debitaccid = $interestreceivable;
							$creditaccid = $interestincome;	
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount,7,$debitaccid,$creditaccid));
							unset($res_posttrx);
							if($ledgerid == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
								$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
								unset($res_posttrx);
								$ledgerid = $this->dbconnect->lastInsertId();				
							}
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
							unset($res_posttrx);
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));				
							unset($res_posttrx);
							//Post Interest paid against loan account (but it should not affect loan balance) ---> DR - Source of payment e.g. bank\cash\mpesa CR - Interest receivable
							$debitaccid = $usercontraaccount;
							$creditaccid = $interestreceivable;	
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,1,$interestamount,8,$debitaccid,$creditaccid));
							unset($res_posttrx);
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
							unset($res_posttrx);
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));		
							unset($res_posttrx);
						}	

						if($closeloan == 1){
							//close loan
							$res_posttrx = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus=5 WHERE accountid = ?"); 			
							$res_posttrx->execute( array($accountid));	
							unset($res_posttrx);
							//close loan account
							$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET accountstatus=7,active=0 WHERE accountid = ?"); 			
							$res_posttrx->execute( array($accountid));	
							unset($res_posttrx);						
						}
						
						$sql_updateschedule = $postrepayment['updateschedule'];
						if(strlen($sql_updateschedule) > 0){
							$res_update_schedule = $this->dbconnect->prepare($sql_updateschedule); 			
							$res_update_schedule->execute();
							unset($res_update_schedule);				
						}							
					}
				}
				//Post excess payment to savings account. For now it will be posted in to deposit account
				//This should be in the settings page
				if($excessamount > 0){
					$debitaccid = $usercontraaccount;
					$creditaccid = 5;	

					$trxdescription = 'M-Pesa Transaction - '.$item['trxdescription'];				
					$valuedate = date("Y-m-d",strtotime($item['valuedate']));
					$userid = 1;
					
					//Set receipt
					if($receiptno == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
						$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));
						unset($res_posttrx);
						
						$receiptno = $this->dbconnect->lastInsertId();	
					}
					
					//Set ledger
					if($ledgerid == 0){
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
						unset($res_posttrx);
						$ledgerid = $this->dbconnect->lastInsertId();
					}
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
					$res_posttrx->execute( array($receiptno,$valuedate,$savingsaccount,0,1,$excessamount,1,$debitaccid,$creditaccid));
					unset($res_posttrx);
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$excessamount));			
					unset($res_posttrx);
					
					$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
					$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$excessamount));	
					unset($res_posttrx);				
				}			
				
				$res_update_loan = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=1,receiptno=? WHERE rowid = ?"); 			
				$res_update_loan->execute( array($receiptno,$item['rowid']));  
				unset($res_update_loan);	
				
				$message = 'Dear '.$item['mpesa_sender'].' your payment of ksh '.$item['amount'].' has been received. Thanks for choosing CEMES Ltd';
				$res_txt = $this->dbconnect->prepare("INSERT INTO tb_quesms(phoneno,message,sent,statusid,source) VALUES(?,?,0,0,1)"); 			
				$res_txt->execute(array($item['mpesa_msisdn'],$message));	
				unset($res_txt);	
				
				$this->dbconnect->commit();	
				
				$retcode='000';
				$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
				$results='';
				
				//echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			} catch (Exception $e) {
			  $this->dbconnect->rollBack();
			  $retcode = '003';
			  $retmsg = $e->getMessage();
			  $results = '';
			  
			$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=3 WHERE receiptno = ?"); 			
			$res_customerdoesnotexist->execute(array($item['trxdescription']));	
			unset($res_customerdoesnotexist);
			  //echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
			}			
		}		
		
		echo 'M-Pesa Repayment Posting completed. Check repayment status in the report';
	}	
	
	//Get Campaign
	public function Get_Campaign() {	
		$sql="SELECT A.campaignid,A.description,A.templateid,C.description AS templatename,C.content,A.ruleid,B.description AS rulename,
			A.scheduleid,D.description AS schedulename,A.active,CASE A.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate
			FROM tb_campaigns AS A
			INNER JOIN tb_rules AS B ON B.ruleid=A.ruleid
			INNER JOIN tb_templates AS C ON C.templateid=A.templateid
			INNER JOIN tb_schedules AS D ON D.scheduleid=A.scheduleid";
			
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Get Rules
	public function Get_Rules() {	
		$sql="SELECT ruleid,description FROM tb_rules WHERE active=1";
			
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Get Schedules
	public function Get_Schedules($detailed) {	
		if($detailed===1){
			$sql="SELECT scheduleid,description,minutes,hours,days,months,weekdays,active,lastrun,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate FROM tb_schedules WHERE active=1";
		}else{
			$sql="SELECT scheduleid,description FROM tb_schedules WHERE active=1 AND scheduleid<>1";
		}
		
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Save Schedule
	public function Save_Schedule($description,$minutes,$hours,$days,$months,$weekdays,$active) {
		if(empty($minutes) || $minutes==='' || !$minutes){
			$minutes=0;
		}
		
		if(empty($hours) || $hours==='' || !$hours){
			$hours=0;
		}
		
		if(empty($days) || $days==='' || !$days){
			$days=0;
		}	

		if(empty($months) || $months==='' || !$months){
			$months=0;
		}

		if(empty($weekdays) || $weekdays==='' || !$weekdays){
			$weekdays=0;
		}
				
		$insert=$this->dbconnect->prepare("INSERT INTO tb_schedules(description,minutes,hours,days,months,weekdays,active) Values('".$description."',".$minutes.",".$hours.",".$days.",".$months.",".$weekdays.",".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}	
	//Update Schedule
	public function Update_Schedule($scheduleid,$description,$minutes,$hours,$days,$months,$weekdays,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_schedules SET minutes=".$minutes.",description='".$description."',hours=".$hours.",days=".$days.",months=".$months.",weekdays=".$weekdays.",active=".$active." WHERE scheduleid=".$scheduleid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//Get Template
	public function Get_Template($isactive) {
		if($isactive===1) {
			$sql="SELECT templateid,code,description,content,active,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate FROM tb_templates WHERE active=1";
		}else{
			$sql="SELECT templateid,code,description,content,active,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate FROM tb_templates";
		}
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}
	
	//Save Template
	public function Save_Template($code,$description,$content,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_templates(code,description,content,active) Values('".$code."','".$description."','".$content."',".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	//Update Template
	public function Update_Template($templateid,$code,$description,$content,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_templates SET code='".$code."',description='".$description."',content='".$content."',active=".$active." WHERE templateid=".$templateid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}		
//Preview Campaign Messages
	public function Preview_Campaignmessages($campaignid) {	
	
		$sql="SELECT tb_rules.ruleid,tb_rules.ruletype AS local,tb_templates.content
		FROM tb_rules 
		INNER JOIN tb_campaigns
		ON tb_campaigns.ruleid = tb_rules.ruleid
		INNER JOIN tb_templates
		ON tb_campaigns.templateid = tb_templates.templateid
		WHERE tb_campaigns.campaignid=".$campaignid;

		$results = $this->dbconnect->query($sql);

		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				$local = $row['local'];
				$ruleid = $row['ruleid']; 
				$content = $row['content']; 
			}

			if($local==='1'){
				$sql="CALL sp_preview(".$ruleid.",'".$content."')";
				$result = $this->dbconnect->query($sql);
				
				foreach($result as $rows) {
					array_push($items, $rows);
				}				
			}else if($local==='0'){
				/*
				$conn = odbc_connect('isms','sa','sasa'); 

				if ($conn) 
				{ 
					$sql="EXEC sp_preview ".$ruleid.",'".$content."'";	
					$result=odbc_exec($conn, $sql);
					
					while($myRow = odbc_fetch_array( $result )){
						array_push($items, $myRow);
					}
				}
				*/
				$sql="CALL sp_preview(".$ruleid.",'".$content."')";
				$result = $this->dbconnect->query($sql);
				
				foreach($result as $rows) {
					array_push($items, $rows);
				}				
				
			}
		}
		
		echo json_encode($items);
	}

	//Save Campaign
	public function Save_Campaign($description,$templateid,$ruleid,$scheduleid,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_campaigns(description,templateid,ruleid,scheduleid,active) Values('".$description."',".$templateid.",".$ruleid.",".$scheduleid.",".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}	
	//Update Campaign
	public function Update_Campaign($campaignid,$description,$templateid,$ruleid,$scheduleid,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_campaigns SET description='".$description."',templateid=".$templateid.",ruleid=".$ruleid.",scheduleid=".$scheduleid.",active=".$active." WHERE campaignid=".$campaignid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}

	//Get Contents
	public function Get_Contacts() {	
		$sql="SELECT contactid,phoneno,name,email,tb_contacts.contacttypeid,tb_contacttype.description AS contacttype,
			tb_rules.ruleid,tb_rules.description AS rulename,
			tb_contacts.active,CASE tb_contacts.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate 
			FROM tb_contacts
			INNER JOIN tb_contacttype
			ON tb_contacttype.contacttypeid = tb_contacts.contacttypeid
			AND tb_contacttype.active=1
			INNER JOIN tb_rules
			ON tb_rules.ruleid = tb_contacts.ruleid
			AND tb_rules.active=1
			WHERE tb_contacts.active=1";
			
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Save Contact
	public function Save_Contacts($phoneno,$name,$email,$ruleid,$contacttypeid,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_contacts(phoneno,name,email,ruleid,contacttypeid,active) Values(".$phoneno.",'".$name."','".$email."',".$ruleid.",".$contacttypeid.",".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}	
	//Update Contacts
	public function Update_Contacts($contactid,$phoneno,$name,$email,$ruleid,$contacttypeid,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_contacts SET phoneno=?,name=?,email=?,contacttypeid=?,active=?,ruleid=? WHERE contactid=?");		
		if ($update->execute(array($phoneno,$name,$email,$contacttypeid,$active,$ruleid,$contactid))) {		
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		} else{
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';			
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//Get Business Rules
	public function Get_Businessrules($active,$ruletype) {	
		$sql="SELECT ruleid,tb_rules.description AS rulename,ruletype,CASE ruletype WHEN 1 THEN 'Contact Group' ELSE 'CBS Rule' END AS ruletypedesc,
		tb_rules.active,CASE tb_rules.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate FROM  tb_rules
		WHERE ((tb_rules.active = $active) OR ($active = 9))  AND ((tb_rules.ruletype = $ruletype) OR ($ruletype = 9))";
			
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}		
	
	//Update Campaign
	public function Update_Businessrules($ruleid,$rulename,$ruletype,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_rules SET description='".$rulename."',ruletype=".$ruletype.",active=".$active." WHERE ruleid=".$ruleid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//Save Campaign
	public function Save_Businessrules($rulename,$ruletype,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_rules(description,ruletype,active) Values('".$rulename."',".$ruletype.",".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	
	//Get Content Type
	public function Get_Contacttype() {	
		$sql="SELECT contacttypeid,description FROM tb_contacttype WHERE active=1";
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Get Direct Message
	public function Get_Directmessages() {	
		$sql="SELECT messageid,tb_directmessages.description,tb_templates.description as templatename,tb_directmessages.templateid,tb_templates.content,tb_directmessages.ruleid,tb_rules.description AS rulename,
			tb_directmessages.active,CASE tb_directmessages.active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate 
			FROM tb_directmessages 
			INNER JOIN tb_rules
			ON tb_rules.ruleid =tb_directmessages.ruleid
			INNER JOIN tb_templates
			ON tb_templates.templateid =tb_directmessages.templateid";
			
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}

	//Preview Direct Messages
	public function Preview_Directmessages($messageid) {	
	
		$sql="SELECT tb_rules.ruleid,tb_rules.ruletype,tb_templates.content
		FROM tb_rules 
		INNER JOIN tb_directmessages
		ON tb_directmessages.ruleid = tb_rules.ruleid
		INNER JOIN tb_templates
		ON tb_directmessages.templateid = tb_templates.templateid
		WHERE tb_directmessages.messageid=".$messageid;

		$results = $this->dbconnect->query($sql);

		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				$ruletype = $row['ruletype'];
				$ruleid = $row['ruleid']; 
				$content = $row['content']; 
			}

			if($ruletype==='1'){//Contact Group preview
				$sql="CALL sp_preview(".$ruleid.",'".$content."')";
				$result = $this->dbconnect->query($sql);
				
				foreach($result as $rows) {
					array_push($items, $rows);
				}				
			}else if($ruletype==='0'){//CBS Business Rule
				$conn = odbc_connect('isms','sa','sasa'); 

				if ($conn) 
				{ 
					$sql="CALL sp_preview(".$ruleid.",'".$content."')";	
					$result=odbc_exec($conn, $sql);
					
					while($myRow = odbc_fetch_array( $result )){
						array_push($items, $myRow);
					}
				}
			}
		}
		
		echo json_encode($items);
	}

	//Save Direct Message
	public function Save_Directmessage($description,$templateid,$ruleid,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_directmessages(description,templateid,ruleid,active) Values('".$description."',".$templateid.",".$ruleid.",".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}

	//Update Direct Message
	public function Update_Directmessage($messageid,$description,$templateid,$ruleid,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_directmessages SET description='".$description."',templateid=".$templateid.",ruleid=".$ruleid.",active=".$active." WHERE messageid=".$messageid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}

	//Send SMS
	public function Send_Directmessages($messageid) {		
		$sql="SELECT tb_rules.ruleid,tb_rules.ruletype,tb_templates.content
		FROM tb_rules 
		INNER JOIN tb_directmessages
		ON tb_directmessages.ruleid = tb_rules.ruleid
		INNER JOIN tb_templates
		ON tb_directmessages.templateid = tb_templates.templateid
		WHERE tb_directmessages.messageid=".$messageid;

		$retcode='';
		$retmsg='';
		$results='';
		$items = array();
		
		$results = $this->dbconnect->query($sql);
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				$ruletype = $row['ruletype'];
				$ruleid = $row['ruleid']; 
				$content = $row['content']; 
			}

			$sql="CALL sp_preview(".$ruleid.",'".$content."')";	

			$insert=$this->dbconnect->prepare($sql);
			$insert->execute();
			$items = array();
			
			if ($insert->rowCount() > 0) {		
				foreach($insert as $row) {
					array_push($items, $row);
				}
				
				$batchid=mt_rand();
				
				foreach($items as $item) {
					$sql="INSERT INTO tb_quesms(phoneno,message,batchid) VALUES(".$item['phoneno'].",'".$item['message']."',".$batchid.");";
					$insert=$this->dbconnect->prepare($sql);
					$insert->execute();
				}			
			}
			
			if (!$insert) {		
				$retcode='001';
				$retmsg=$insert->errorInfo();
				$results='';
			} else{
				$retcode='000';
				$retmsg='Insert Successful';
				$results='';
			}					
		}else{
			$retcode='001';
			$retmsg='Unable to send sms';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));	
	}

	//Get Tag
	public function Get_Tags() {
		$sql="SELECT tagid,code,description,active,CASE active WHEN 1 THEN 'YES' ELSE 'No' END AS activestate FROM tb_tags";
		//$results = mysqli_query($linkID,$sql) or die(mysqli_error($linkID));
		$results = $this->dbconnect->query($sql);
		
		$items = array();
		
		if ($results->rowCount() > 0) {		
			foreach($results as $row) {
				array_push($items, $row);
			}
		}
		
		echo json_encode($items);
	}
	//Save Tag
	public function Save_Tags($code,$description,$active) {
		$insert=$this->dbconnect->prepare("INSERT INTO tb_tags(code,description,active) Values('".$code."','".$description."',".$active.")");
		$insert->execute();
		
		if (!$insert) {		
			$retcode='001';
			$retmsg=$insert->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Insert Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}
	//Update Tag
	public function Update_Tags($tagid,$code,$description,$active) {
		$update=$this->dbconnect->prepare("UPDATE tb_tags SET code='".$code."',description='".$description."',active=".$active." WHERE tagid=".$tagid);
		$update->execute();
		
		if (!$update) {		
			$retcode='001';
			$retmsg=$update->errorInfo();
			$results='';
		} else{
			$retcode='000';
			$retmsg='Update Successful';
			$results='';
		}
		
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
	}	
//END OF CLASS	
}

$getdata=new GetData();
	
switch ($action)
{	
	case 1://User Login
		$getdata->User_Login($_POST['user'],$_POST['pwd']);
		
	break;
	case 2://Menus
		$getdata->LoadMenus();
	
	break;
	case 3://User Profiles		
		$getdata->UserProfile();		
	break;
	case 4://Users		
		$getdata->getUsers(0,0,intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;	
	case 5://Users Search		
		$getdata->getUsers(0,1,0,0);		
	break;	
	case 6://Users Add			
		$getdata->addUser($_POST['username'],$_POST['password'], $_POST['branchid'],$_POST['fullname'],$_POST['idtype'],$_POST['idno'],$_POST['profileid'],$_POST['active']);		
	break;	
	case 7://Get Branches	
		$filter = 0;
		if (isset($_REQUEST['filter'])) {
			$filter = intval($_REQUEST['filter']);
		}	
		$getdata->getBranches($filter);		
	break;
	case 8://Get Misc		
		$getdata->getMisc(10,0,0);		
	break;
	case 9://Edit User		
		$getdata->editUser($_REQUEST['userid'],$_POST['username'],$_POST['branchid'],$_POST['fullname'],$_POST['idtype'],$_POST['idno'],$_POST['profileid'],$_POST['active']);		
	break;
	case 10://Add User Profile		
		$getdata->addUserProfile($_POST['profilename'],$_POST['active'],$_POST['permissions']);	
	break;
        
            
	break;case 10://Add User Profile		
		$getdata->addUserProfile($_POST['profilename'],$_POST['active'],$_POST['permissions']);	
	break;
	case 11://Edit User Profile	
		$getdata->editUserProfile($_POST['profileid'],$_POST['profilename'],$_POST['active'],$_POST['permissions']);		
	break;	
	case 12://Get User Profile Menus		
		$getdata->getUserProfileMenus($_REQUEST['activity'],$_REQUEST['profileid']);		
	break;	
	case 13://Get Misc
		$getdata->getMisc(0,intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;
	case 14://Get Definations		
		$getdata->getDefinations();		
	break;
	case 15://Add Misc		
		$getdata->addMisc($_REQUEST['parentid'],$_REQUEST['description'],$_REQUEST['code'],$_REQUEST['defaultitem'],$_REQUEST['active']);		
	break;
	case 16://Edit Misc
		$getdata->editMisc($_REQUEST['id'],$_REQUEST['parentid'],$_REQUEST['description'],$_REQUEST['code'],$_REQUEST['defaultitem'],$_REQUEST['active']);		
	break;
	case 17://Get Titles		
		$getdata->getMisc(5,0,0);		
	break;
	case 18://Get Customer Type		
		$getdata->getMisc(7,0,0);		
	break;
	case 19://Get Gender		
		$getdata->getMisc(2,0,0);		
	break;
	case 20://Get Address Type		
		$getdata->getMisc(9,0,0);		
	break;
	case 21://Get Country		
		$getdata->getMisc(3,0,0);		
	break;
	case 22://Get County		
		$getdata->getMisc(14,0,0);		
	break;	
	case 23://Get Loanofficers		
		$getdata->getLoanofficers(intval($_REQUEST['page']),intval($_REQUEST['rows']),0);		
	break;		
	case 24://Add Loanofficers		
		$getdata->addLoanofficers($_REQUEST['description'],$_REQUEST['branchid'],$_REQUEST['active']);		
	break;
	case 25://Edit Loanofficers
		$getdata->editLoanofficers($_REQUEST['loanofficerid'],$_REQUEST['description'],$_REQUEST['branchid'],$_REQUEST['active']);		
	break;
	case 26://Get Loanofficers filter
		$filter = 0;
		if (isset($_REQUEST['filter'])) {
			$filter = intval($_REQUEST['filter']);
		}

		$getdata->getLoanofficers(1,1000,$filter);		
	break;
	case 27://Get Customers	
		$mobileno = '';
		$status = '';
                $idno= '';
		
		if (isset($_REQUEST['status'])) {
			$status = intval($_REQUEST['status']);
		}
		if (isset($_REQUEST['idno'])) {
			$idno = $_REQUEST['idno'];
		}
		if (isset($_REQUEST['mobileno'])) {
			$mobileno = $_REQUEST['mobileno'];
		}		
		$getdata->getCustomers(intval($_REQUEST['page']),intval($_REQUEST['rows']),$status,$idno,$mobileno);		
	break;		
	case 28://Add Customers		
		$getdata->addCustomers($_REQUEST['branchid'],date("Y-m-d",strtotime($_REQUEST['joindate'])),$_REQUEST['customertype'],$_REQUEST['loanofficerid']
		,$_REQUEST['title'],$_REQUEST['surname'],$_REQUEST['firstname'],$_REQUEST['lastname'],$_REQUEST['gender'],$_REQUEST['maritalstatus']
		,date("Y-m-d",strtotime($_REQUEST['dateofbirth'])),$_REQUEST['email'],$_REQUEST['idtype'],$_REQUEST['idno'],$_REQUEST['phoneno']
		,$_REQUEST['mobileno'],$_REQUEST['address'], $_REQUEST['postcode'], $_REQUEST['town'],$_REQUEST['county'],$_REQUEST['province'],$_REQUEST['countryid']
		,$_REQUEST['addresstype'] , $_REQUEST['bank'], $_REQUEST['bankbranch'] , $_REQUEST['bankaccountname'] ,$_REQUEST['bankaccountno']);
	break;
	case 29://Edit Customers
		$getdata->editCustomers($_REQUEST['customerid'],$_REQUEST['personid'],$_REQUEST['addressid'],date("Y-m-d",strtotime($_REQUEST['joindate'])),$_REQUEST['customertype'],$_REQUEST['loanofficerid']
		,$_REQUEST['title'],$_REQUEST['surname'],$_REQUEST['firstname'],$_REQUEST['lastname'],$_REQUEST['gender'],$_REQUEST['maritalstatus']
		,date("Y-m-d",strtotime($_REQUEST['dateofbirth'])),$_REQUEST['email'],$_REQUEST['idtype'],$_REQUEST['idno'],$_REQUEST['phoneno']
		,$_REQUEST['mobileno'],$_REQUEST['address'],$_REQUEST['postcode'],$_REQUEST['town'],$_REQUEST['county'],$_REQUEST['countryid']
		,$_REQUEST['addresstype'] , $_REQUEST['bank'], $_REQUEST['bankbranch'] , $_REQUEST['bankaccountname'] ,$_REQUEST['bankaccountno']);	
	break;
	case 30://Get Marital Status		
		$getdata->getMisc(12,0,0);		
	break;
	case 31://Get Currency		
		$getdata->getCurrency(intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;
	case 32://ADD Currency		
		$getdata->addCurrency($_REQUEST['code'],$_REQUEST['description'],$_REQUEST['symbol'],$_REQUEST['rounding'],$_REQUEST['homecurrency'],$_REQUEST['active']);		
	break;
	case 33://EDIT Currency		
		$getdata->editCurrency($_REQUEST['currencyid'],$_REQUEST['code'],$_REQUEST['description'],$_REQUEST['symbol'],$_REQUEST['rounding'],$_REQUEST['homecurrency'],$_REQUEST['active']);			
	break;
	case 34://Get SavingsProduct		
		$getdata->getSavingsProduct(intval($_REQUEST['page']),intval($_REQUEST['rows']),0);		
	break;
	case 35://ADD SavingsProduct		
		$getdata->addSavingsProduct($_REQUEST['code'],$_REQUEST['description'],$_REQUEST['producttypeid'],$_REQUEST['currencyid'],$_REQUEST['maxaccounts'],$_REQUEST['active'],$_REQUEST['termdeposit'],$_REQUEST['minbalance'],$_REQUEST['maxbalance'],$_REQUEST['minage'],$_REQUEST['maxage'],$_REQUEST['minterm'],$_REQUEST['maxterm'],$_REQUEST['productcontrol'],$_REQUEST['savingsint'],$_REQUEST['savingstax'],$_REQUEST['contributions']);	
	break;
	case 36://EDIT SavingsProduct		
		$getdata->editSavingsProduct($_REQUEST['productid'],$_REQUEST['code'],$_REQUEST['description'],$_REQUEST['producttypeid'],$_REQUEST['currencyid'],$_REQUEST['maxaccounts'],$_REQUEST['active'],$_REQUEST['termdeposit'],$_REQUEST['minbalance'],$_REQUEST['maxbalance'],$_REQUEST['minage'],$_REQUEST['maxage'],$_REQUEST['minterm'],$_REQUEST['maxterm'],$_REQUEST['productcontrol'],$_REQUEST['savingsint'],$_REQUEST['savingstax'],$_REQUEST['contributions']);			
	break;
	case 37://Get Currency	Combo	
		$getdata->getCurrencyCombo();		
	break;
	case 38://Get Product Type		
		$getdata->getProductType('savings');		
	break;
	case 39://Get Fee		
		$getdata->getFee();		
	break;
	case 40://Add Fee		
		$getdata->addFee($_REQUEST['description'],$_REQUEST['shortname'],$_REQUEST['feetype'],$_REQUEST['feeoption'],$_REQUEST['amount'],$_REQUEST['active'],$_REQUEST['feeincome'],$_REQUEST['feeaccrual']);		
	break;
	case 41://Edit Fee		
		$getdata->editFee($_REQUEST['feeid'],$_REQUEST['description'],$_REQUEST['shortname'],$_REQUEST['feetype'],$_REQUEST['feeoption'],$_REQUEST['amount'],$_REQUEST['active'],$_REQUEST['feeincome'],$_REQUEST['feeaccrual']);		
	break;
	case 42://Get Fee Type		
		$getdata->getMisc(6,0,0);		
	break;
	case 43://Get Fee Option
		$getdata->getMisc(15,0,0);		
	break;
	case 44://Get Ledger Categories		
		$getdata->getLedgerCategory();		
	break;
	case 45://Get Ledger Sub Category		
		$getdata->getLedgerSubCategory($_REQUEST['categoryid']);		
	break;
	case 46://Get Ledger Sub Category parent	
		$getdata->getLedgerSubCategoryParent($_REQUEST['parentid']);		
	break;
	case 47://Get Ledger Accounts		
		$getdata->getLedgerAccounts($_REQUEST['accountid'],intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;
	case 48://Get Ledger Accounts Search		
		$getdata->getLedgerAccountSearch(0,0,0);		
	break;	
	case 49://Add Ledger Accounts		
		$getdata->addLedgerAccount($_REQUEST['description'],$_REQUEST['categoryid'],$_REQUEST['financialcategoryid'],$_REQUEST['subfinancialcategoryid'],$_REQUEST['branchid'],$_REQUEST['active'],$_REQUEST['userid']);		
	break; 
	case 50://Edit Ledger Accounts		
		$getdata->editLedgerAccount($_REQUEST['accountid'],$_REQUEST['description'],$_REQUEST['categoryid'],$_REQUEST['financialcategoryid'],$_REQUEST['subfinancialcategoryid'],$_REQUEST['branchid'],$_REQUEST['active']);		
	break;	
	case 51://Get Ledger Accounts By Type		
		$getdata->getLedgerAccountByType($_REQUEST['categoryid']);		
	break;
	case 52://Get Customer Account		
		$getdata->getCustomerAccount(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['idno']);		
	break;	
	case 53://Get Customer search
           
		$getdata->getCustomerSearch(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['customerid'],$_REQUEST['idno']);		
	break;
	case 54://Get SavingsProduct		
		$getdata->getSavingsProduct(0,0,1);		
	break;
	case 55://Save Savings Account		
		$getdata->addSavingsAccount($_REQUEST['customerid'],$_REQUEST['productid'],$_REQUEST['openingbalance']);		
	break;	
	case 56://Add Branch	
		$getdata->addBranch($_REQUEST['branchname'],$_REQUEST['code'],$_REQUEST['active'],$_REQUEST['addressid'],$_REQUEST['phoneno']);		
	break;
	case 57://Edit Branch		
		$getdata->editBranch($_REQUEST['branchid'],$_REQUEST['branchname'],$_REQUEST['code'],$_REQUEST['active'],$_REQUEST['addressid'],$_REQUEST['phoneno']);		
	break;
	case 58://Get Town		
		$getdata->getMisc(17,0,0);		
	break;	
	case 59://Post Deposit		
		$getdata->postTransaction($_REQUEST['accountid'],$_REQUEST['feeid'],$_REQUEST['amount'],$_REQUEST['trxtype'],$_REQUEST['userid'],$_REQUEST['trxdescription'],date("Y-m-d",strtotime($_REQUEST['valuedate'])),$_REQUEST['usercontraaccount']);		
	break;	
	case 60://User Accounts		
		$getdata->userAccounts($_REQUEST['cashieraccount'],$_REQUEST['userid']);		
	break;
	case 61://Search fee		
		$getdata->searchFee();		
	break;
	case 62://Search transaction
		$getdata->searchTransaction(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['receiptno']);		
	break;	
	case 63://Post Reverse Transaction	
		$getdata->reverseTransaction($_REQUEST['receiptno'],$_REQUEST['userid']);		
	break;
	case 64://Get Active Ledger Accounts	
		$getdata->getLedgerAccountSearch(1,1,0);		
	break;
	case 65://Post Ledger Accounts	
		$getdata->postLedgerTransaction(date("Y-m-d",strtotime($_REQUEST['valuedate'])),$_REQUEST['narration'],json_decode($_REQUEST['ledgertrx'], true),$_REQUEST['userid']);		
	break;
	case 66://Empty and can be reused
		$getdata->getLedgerAccountSearch(1,0,0);		
	break;	
	case 67://Account Statement
		$date = date('Y-m-d');
		
		if (isset($_REQUEST['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_REQUEST['fromdate']));
		}else {
			$fromdate = $date;
		}
		
		if (isset($_REQUEST['todate'])) {	
			$todate = date("Y-m-d",strtotime($_REQUEST['todate']));
		}else {
			$todate = $date;
		}
		
		$getdata->accountstatement(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['customerid'],$_REQUEST['idno'],$_REQUEST['accountno'],$fromdate,$todate);		
	break;
	case 68://Get Loan Products
		$getdata->getLoanProduct(intval($_REQUEST['page']),intval($_REQUEST['rows']),1,0);		
	break;	
	case 69://Get Product Type		
		$getdata->getProductType('loan');		
	break;
	case 70://Add Loan Product	
		$futureinterest = 0;
		$accrueinterest = 0;
				
		if(isset($_REQUEST['valfutureinterest'])){
			$futureinterest = $_REQUEST['valfutureinterest'];
		}
		
		if(isset($_REQUEST['valaccrueinterest'])){
			$accrueinterest = $_REQUEST['valaccrueinterest'];
		}
		
		$getdata->addLoanProduct($_POST['code'],$_POST['description'],$_POST['producttypeid'],$_POST['currencyid'],$_POST['maxaccounts'],$_POST['active'],$_POST['minloanterm'],$_POST['maxloanterm'],$_POST['minloanamount'],$_POST['maxloanamount'],$_POST['interestrate'],$_POST['interestcalculationmethod'],$_POST['minimumsavings'],$_POST['savingsproduct'],$_POST['graceperiod'],$_POST['graceperiodon'],$_POST['daycount'],$_POST['ignoreholidays'],$_REQUEST['fee'],$_POST['productcontrol'],$_POST['interestincome'],$_POST['interestreceivable'],$_POST['noofguarantor'],$_POST['useguaranteedshares'],$_POST['percentage'],$_REQUEST['guaranteeproducts'],$_REQUEST['termperiod'],$_REQUEST['flatinterestamount'],$futureinterest,$accrueinterest);					
	break;	
	case 71://Edit Loan Product
		$futureinterest = 0;
		$accrueinterest = 0;
				
		if(isset($_REQUEST['valfutureinterest'])){
			$futureinterest = $_REQUEST['valfutureinterest'];
		}
		
		if(isset($_REQUEST['valaccrueinterest'])){
			$accrueinterest = $_REQUEST['valaccrueinterest'];
		}		
	
		$getdata->editLoanProduct($_REQUEST['productid'],$_POST['code'],$_POST['description'],$_POST['producttypeid'],$_POST['currencyid'],$_POST['maxaccounts'],$_POST['active'],$_POST['minloanterm'],$_POST['maxloanterm'],$_POST['minloanamount'],$_POST['maxloanamount'],$_POST['interestrate'],$_POST['interestcalculationmethod'],$_POST['minimumsavings'],$_POST['savingsproduct'],$_POST['graceperiod'],$_POST['graceperiodon'],$_POST['daycount'],$_POST['ignoreholidays'],$_REQUEST['fee'],$_POST['productcontrol'],$_POST['interestincome'],$_POST['interestreceivable'],$_POST['noofguarantor'],$_POST['useguaranteedshares'],$_POST['percentage'],$_REQUEST['guaranteeproducts'],$_REQUEST['termperiod'],$_REQUEST['flatinterestamount'],$futureinterest,$accrueinterest);
	break;		
	case 72://Grace Period Options		
		$getdata->getMisc(19,0,0);		
	break;	
	case 73://Interest Calculation Method		
		$getdata->getMisc(20,0,0);		
	break;	
	case 74://Day Count		
		$getdata->getMisc(21,0,0);		
	break;	
	case 75://Disbursement Options	
		$getdata->getMisc(22,0,0);		
	break;
	case 76://View Ledger Transactions
		$date = date('Y-m-d');
		
		if (isset($_REQUEST['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_REQUEST['fromdate']));
		}else {
			$fromdate = $date;
		}
		
		if (isset($_REQUEST['todate'])) {	
			$todate = date("Y-m-d",strtotime($_REQUEST['todate']));
		}else {
			$todate = $date;
		}
		
		$getdata->viewledgertransactions(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['ledgerid'],$_REQUEST['receiptno'],$_REQUEST['accountno'],$fromdate,$todate);		
	break;	
	case 77://Get Loan Product
		$getdata->getLoanProduct(0,0,0,$_REQUEST['productid']);		
	break;	
	case 78://Get Loan Product For COmbobox
		$getdata->getLoanProduct(0,0,0,0);		
	break;
	case 79://Get Loan Product For COmbobox 
		$getdata->getschedule($_REQUEST['productid'],$_REQUEST['loanamount'],$_REQUEST['loanterm'],$_REQUEST['print']);		
	break;
	case 80://Loan source of funds	
		$getdata->getMisc(23,0,0);		
	break;	
	case 81://Loan Reason
		$getdata->getMisc(24,0,0);		
	break;	
	case 82://Loan Application
		$getdata->loanapplication(date('Y-m-d h:i:s' , strtotime("+1 days")),$_REQUEST['customerid'],$_REQUEST['loanterm'],$_REQUEST['productid'],$_REQUEST['graceperiod'],$_REQUEST['amount'],$_REQUEST['loanreason'],$_REQUEST['fundsource'],$_REQUEST['applicationnote'],$_REQUEST['applicationby'],$_REQUEST['guarantors']);
	break;
	case 83://Loans
		$date = date('Y-m-d');
		
		if ($_REQUEST['fromdate'] != "") {
			$fromdate = date("Y-m-d",strtotime($_REQUEST['fromdate']));
		}else {
			$fromdate = $date;
		}
		
		if ($_REQUEST['todate'] != "") {	
			$todate = date("Y-m-d",strtotime($_REQUEST['todate']));
		}else {
			$todate = $date;
		}	
		$getdata->loanapplicationprocessing(intval($_REQUEST['page']),intval($_REQUEST['rows']),$_REQUEST['customerid'],$_REQUEST['loanid'],$fromdate,$todate,$_REQUEST['loanstatus'],$_REQUEST['branchid']);
	break;	
	case 84://Approve Loans
		$getdata->approveloans($_REQUEST['approvedby'],$_REQUEST['approvalnote'],$_REQUEST['loan'],$_REQUEST['approve']);
	break;
	case 85://Disburse Loans
		$date = date('Y-m-d');
		
		if (isset($_REQUEST['disbursementdate'])) {	
			$disbursementdate = date("Y-m-d",strtotime($_REQUEST['disbursementdate']));
		}else {
			$disbursementdate = $date;
		}
		$getdata->disburseloans($_REQUEST['disbursedby'],$_REQUEST['disbursenote'],$_REQUEST['loans'],$_REQUEST['disbursementoption'],$_REQUEST['disbursementaccount'],$disbursementdate);
	break;
	case 86://Loan Disbursement Option
		$getdata->getMisc(22,0,0);		
	break;	
	case 87://Get Loan Disbursement Ledger Accounts	
		$getdata->getLedgerAccountSearch(1,1,3);		
	break;	
	case 88://Get Customer Account		
		$getdata->getCustomerAccountsByType($_REQUEST['customerid'],0,0,3);		
	break;
	case 89://Get Loan Balance Details		
		$getdata->loanbalancedetails($_REQUEST['accountid'],date("Y-m-d",strtotime($_REQUEST['valuedate'])),1);		
	break;		
	case 90://Post loan repayment		
		$getdata->postLoanRepayment($_REQUEST['accountid'],$_REQUEST['amount'],$_REQUEST['userid'],$_REQUEST['trxdescription'],date("Y-m-d",strtotime($_REQUEST['valuedate'])),$_REQUEST['usercontraaccount']);		
	break;		
	case 91://Get Product Type		
		$getdata->getProductType('');		
	break;	
	case 92://Get Period Year		
		$getdata->getPeriodYear(intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;	
	case 93://Add Period Year		
		$getdata->addPeriodYear($_REQUEST['code'],0,$_REQUEST['userid']);		
	break;
	case 94://Edit Period Year		
		$getdata->editPeriodYear($_REQUEST['periodyearid'],$_REQUEST['code'],$_REQUEST['userid']);		
	break;
	case 95://Get Period		
		$getdata->getPeriods(intval($_REQUEST['page']),intval($_REQUEST['rows']));		
	break;	
	case 96://Add Period		
		$getdata->addPeriods($_REQUEST['periodyearid'],$_REQUEST['monthno'],$_REQUEST['quarter'],$_REQUEST['code'],$_REQUEST['description'],date("Y-m-d",strtotime($_REQUEST['startdate'])),date("Y-m-d",strtotime($_REQUEST['enddate'])),1,$_REQUEST['createdby']);		
	break;
	case 97://Edit Period		
		$getdata->editPeriods($_REQUEST['periodid'],$_REQUEST['periodyearid'],$_REQUEST['monthno'],$_REQUEST['quarter'],$_REQUEST['code'],$_REQUEST['description'],date("Y-m-d",strtotime($_REQUEST['startdate'])),date("Y-m-d",strtotime($_REQUEST['enddate'])),$_REQUEST['createdby']);		
	break;
	case 98://Get Period		
		$getdata->getPeriodYearSearch();		
	break;		
	case 99://Process EOY		
		$getdata->EOY($_REQUEST['periodyearid'],$_REQUEST['userid']);		
	break;
	case 100://Get Periods		
		$getdata->getPeriodSearch($_REQUEST['periodyearid']);		
	break;	
	case 101://Process EOM		
		$getdata->processfinancialperiod($_REQUEST['periodid'],$_REQUEST['status']);		
	break;
	case 102://Search Guarantor		
		$getdata->searchguarantor($_REQUEST['customerid']);		
	break;
	case 103://Save Guarantor
		$getdata->addLoanGuarantors($_REQUEST['guarantors'],$_REQUEST['validate'],$_REQUEST['productid'],$_REQUEST['disbursedamount']);		
	break;		
	case 104://Get Loan Guarantors		
		$getdata->getLoanGuarantors($_REQUEST['loanid']);		
	break;		
	case 105://Transfer Accounts search	
		$getdata->transfersearch($_REQUEST['customerid'],$_REQUEST['source'],date("Y-m-d",strtotime($_REQUEST['valuedate'])));		
	break;
	case 106://Post Transfer		
		$getdata->postTransfer($_REQUEST['craccountid'],$_REQUEST['draccountid'],$_REQUEST['amount'],$_REQUEST['transfertype'],$_REQUEST['userid'],$_REQUEST['trxdescription'],date("Y-m-d",strtotime($_REQUEST['valuedate'])),$_REQUEST['usercontraaccount']);		
	break;	
	case 107://Loan Term Type
		$getdata->getMisc(25,0,0);		
	break;
	case 108://Password Reset 
		$getdata->passwordreset($_REQUEST['userid'],$_REQUEST['oldpassword'],$_REQUEST['newpassword'],$_REQUEST['confirmpassword']);		
	break;	
	case 109://Get loan Accounts
		$getdata->getloanaccountsforreport($_REQUEST['customerid'],$_REQUEST['idno'],$_REQUEST['iscustomerid']);		
	break;
	case 110://Get mpesa repayments
		$date = date('Y-m-d');
		$fromdate = $date;
		$todate = $date;	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}
		
		$getdata->getMpesaRepayments($fromdate,$todate,$_GET['paymentstatus'],$_GET['paymentprocessing']);	 		
	break;	
	case 111://Repost M-Pesa Repayments
		$getdata->repostmpesarepayment($_REQUEST['mpesarepayments']);		
	break;	
	case 112://Get Campaign
		$getdata->Get_Campaign();
		break;	
	case 113://Get Rules
		$getdata->Get_Rules();
		break;
	case 114://Get Schedules
		$getdata->Get_Schedules(0);
		break;
	case 115://Get Active Templates
		$getdata->Get_Template(1);
		break;
	case 116://Preview Campaign Messages
		$getdata->Preview_Campaignmessages($_REQUEST['campaignid']);
		break;
	case 117://Save Campaign 
		$getdata->Save_Campaign($_POST['description'],$_POST['templateid'],$_POST['ruleid'],$_POST['scheduleid'],$_POST['active']);
		break;
	case 118://Update Campaign
		$getdata->Update_Campaign($_REQUEST['campaignid'],$_POST['description'],$_POST['templateid'],$_POST['ruleid'],$_POST['scheduleid'],$_POST['active']);
		break;	
	case 119://Get Contacts
		$getdata->Get_Contacts();
		break;
	case 120://Get Contact Group Rule
		$getdata->Get_Businessrules(1,1); 
		break;	
	case 121://Get Contact Type
		$getdata->Get_Contacttype();
		break;
	case 122://Get Direct Messages
		$getdata->Get_Directmessages();
		break;	
	case 123://Preview Direct Message
		$getdata->Preview_Directmessages($_REQUEST['messageid']);
		break;
	case 124://Save Direct Messages
		$getdata->Save_Directmessage($_POST['description'],$_POST['templateid'],$_POST['ruleid'],$_POST['active']);
		break;
	case 125://Edit Direct Messages
		$getdata->Update_Directmessage($_REQUEST['messageid'],$_POST['description'],$_POST['templateid'],$_POST['ruleid'],$_POST['active']);
		break;	
	case 126://Send Direct Message
		$getdata->Send_Directmessages($_REQUEST['messageid']);
		break;	
	case 127://Get Template
		$getdata->Get_Template(0);
		break;	
	case 128://Save Template
		$getdata->Save_Template($_POST['code'],$_POST['description'],$_POST['content'],$_POST['active']);
		break;	
	case 129://Edit Template
		$getdata->Update_Template($_REQUEST['templateid'],$_POST['code'],$_POST['description'],$_POST['content'],$_POST['active']);
		break;	
	case 130://Get Business  Rules
		$getdata->Get_Businessrules(9,9); 
		break;
	case 131://Save Business  Rules
		$getdata->Save_Businessrules($_POST['rulename'],$_POST['ruletype'],$_POST['active']);
		break;
	case 132://Save Business  Rules
		$getdata->Update_Businessrules($_REQUEST['ruleid'],$_POST['rulename'],$_POST['ruletype'],$_POST['active']);
		break;
	case 133://Get Tag
		$getdata->Get_Tags();
		break;
	case 134://Save Tag
		$getdata->Save_Tags($_POST['code'],$_POST['description'],$_POST['active']);
		break;
	case 135://Update Tag
		$getdata->Update_Tags($_REQUEST['tagid'],$_POST['code'],$_POST['description'],$_POST['active']);
		break;	
	case 136://Get Schedules
		$getdata->Get_Schedules(1);
		break;
	case 137://Save Schedules
		$getdata->Save_Schedule($_POST['description'],$_POST['minutes'],$_POST['hours'],$_POST['days'],$_POST['months'],$_POST['weekdays'],$_POST['active']);
		break;
	case 138://Update Schedules
		$getdata->Update_Schedule($_REQUEST['scheduleid'],$_POST['description'],$_POST['minutes'],$_POST['hours'],$_POST['days'],$_POST['months'],$_POST['weekdays'],$_POST['active']);
		break;	
	case 139://Save Contacts 
		$getdata->Save_Contacts($_POST['phoneno'],$_POST['name'],$_POST['email'],$_POST['ruleid'],$_POST['contacttypeid'],$_POST['active']);
		break;	
	case 140://Edit Contacts
		$getdata->Update_Contacts($_REQUEST['contactid'],$_POST['phoneno'],$_POST['name'],$_POST['email'],$_POST['ruleid'],$_POST['contacttypeid'],$_POST['active']);
		break;
	case 141://Get SMS
		$date = date('Y-m-d');
		$fromdate = $date;
		$todate = $date;	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}
		
		$getdata->getSMS($fromdate,$todate);	 		
	break;		
	case 142://SMS Report
		$date = date("Y-m-d", time());
		$fromdate = isset($_REQUEST['fromdate']) ? date("Y-m-d",strtotime($_REQUEST['fromdate'])): $date;
		$todate = isset($_REQUEST['todate']) ? date("Y-m-d",strtotime($_REQUEST['todate'])): $date; 
		$status =  isset($_REQUEST['status']) ? $_REQUEST['status']:9; 
		$phoneno = isset($_REQUEST['phoneno']) ? $_REQUEST['phoneno']:'';
		
		$getdata->SMS_Report($fromdate,$todate,$status,$phoneno);
		break;	
	case 143://Update SMS Status
		$getdata->UpdateSMSStatus($_REQUEST['smsstatus'],$_REQUEST['smss'],$_REQUEST['issent']); 
		break;
	case 144://Update M-Pesa Repayments
		$getdata->UpdateMpesaRepayment($_REQUEST['mpesacode'],$_REQUEST['mpesa_msisdn'],$_REQUEST['mpesa_sender']);
		break;
	case 145://Loan Status
		$getdata->getLoanStatus();		
	break;	
	case 146://Reset User password
		$getdata->ResetUserPassword($_REQUEST['userid'],$_REQUEST['password']);
		break;
	case 147://Reset User password
		$getdata->ResetUserLoginAttempts($_REQUEST['userid']);
		break;		
        case 148://Get Banks	
            $getdata->getMisc(26,0,0);	
              break;  
          
        case 149://Get Customer status	
            $getdata->getMisc(13,0,0);	
              break;
        case 150://Loans
            $date = date('Y-m-d');

            if (isset($_REQUEST['fromdate'])) {
                    $fromdate = date("Y-m-d",strtotime($_REQUEST['fromdate']));
            }else {
                    $fromdate =date("Y-m-d" , strtotime("-1 week"));
            }

            if (isset($_REQUEST['todate'])) {	
                    $todate = date("Y-m-d",strtotime($_REQUEST['todate']));
            }else {
                    $todate = $date;
            }	
            $getdata->customerloans(intval($_REQUEST['page']),intval($_REQUEST['rows']),$fromdate,$todate);
    break;
    
     case 151://Get Loan status	
            $getdata->getMisc(8,0,0);	
              break;  
     //Assign custome loan officer
    case 152:
	     $getdata->assingLoanofficer($_REQUEST['customerid'],$_REQUEST['loanofficerid']); 
		break;
    //display all customers in a select
    case 153:
	     $getdata->getCustomersforSelect(); 
		break;
            
     case 154://Get Applied loans search
         $mobileno = '';  
         $idno = '';  
         if(isset($_REQUEST['idno']))
               $idno = $_REQUEST['idno'];
           if(isset($_REQUEST['mobileno']))
               $mobileno = $_REQUEST['mobileno'];
		$getdata->getCustomerLoanApplications(intval($_REQUEST['page']),intval($_REQUEST['rows']),$mobileno,$idno);		
	break;
	default: //when nothing is passed  getProductType
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));			
}

?>