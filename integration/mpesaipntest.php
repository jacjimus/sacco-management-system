<?php
	//Initialize config
	include('../conf/config.inc');
	
	//Class to perform various functionality
	class ProcessIntegration {
		private $dbconnect;
		
	   //Database connect
		public function __construct()
		{
			$db = new DB_Class;
			$this->dbconnect=$db->Myconn();
		}	
		
		//Process Repayments
		public function processpayments($id,$orig,$dest,$tstamp,$text,$customer_id,$user,$pass,$routemethod_id,$routemethod_name,$mpesa_code,$mpesa_acc,$mpesa_trx_date,$mpesa_trx_time,$mpesa_amt,$mpesa_sender) {
			$date = date('Y-m-d');

			//Check if M-Pesa Reference Number Exist
			$sql_mpesarepayments="SELECT * FROM tb_mpesarepayments_test WHERE mpesa_code ='".$mpesa_code."'";
			if($res=$this->dbconnect->query($sql_mpesarepayments)){
				if($res->rowCount()>0){
					echo 'FAIL|Duplicate transactions';
					return;
				}
			}
			
			try{				
				$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbconnect->beginTransaction();
				
				$res_approveloan = $this->dbconnect->prepare("INSERT INTO tb_mpesarepayments_test(id,orig,dest,tstamp,text,customer_id,user,pass,routemethod_id,routemethod_name,mpesa_code,mpesa_acc,mpesa_trx_date,mpesa_trx_time,mpesa_amt,mpesa_sender) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				$res_approveloan->execute(array($id,$orig,$dest,$tstamp,$text,$customer_id,$user,$pass,$routemethod_id,$routemethod_name,$mpesa_code,$mpesa_acc,$mpesa_trx_date,$mpesa_trx_time,$mpesa_amt,$mpesa_sender));
						
				$this->dbconnect->commit();
				
				echo 'OK|Thank you for your payment';			
			} catch (Exception $e) {
				$this->dbconnect->rollBack();				
				echo 'FAIL|Repayment failed to process';
				//echo $e->getMessage();
			}
		}		
	}
	
	$processintegration = new ProcessIntegration();	
	$processintegration->processpayments($_REQUEST['id'],$_REQUEST['orig'],$_REQUEST['dest'],$_REQUEST['tstamp'],$_REQUEST['text'],$_REQUEST['customer_id'],$_REQUEST['user'],$_REQUEST['pass'],$_REQUEST['routemethod_id'],$_REQUEST['routemethod_name'],$_REQUEST['mpesa_code'],$_REQUEST['mpesa_acc'],$_REQUEST['mpesa_trx_date'],$_REQUEST['mpesa_trx_time'],$_REQUEST['mpesa_amt'],$_REQUEST['mpesa_sender']);
?>