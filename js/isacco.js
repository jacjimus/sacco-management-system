﻿	function LoadMenus(){
		var menu_btn='mb';
		var menu_buttons='';
		var menu_items='';
		var div;	
		var userdiv;
		var userinfo='User : '+sessvars.username+' | Branch : '+sessvars.branchname+' | User Profile : '+sessvars.profilename+' | User Account : '+sessvars.cashieraccount+' | <a href="../">Sign Out</a>'; 
						
		userdiv = $('<div><h1>'+userinfo+'</h1></div>').appendTo('body');
		userdiv.attr('align', 'center');
		
		$.ajax({
		type: "GET",
		url: '../data/get_data.php',
		datatype: "json",
		data: {
			action: 2,
			
		},
		success: function(data){
			var response = eval('('+data+')');
			var menus=[];
			var menuicons= [];
			var i=0;
			
			$.each(response.results, function(index, element) {
				//Create main menu item
				if($.inArray(element.menuid, menus)==-1){
					menus.push(element.menuid);
					menuicons.push(element.menusicons);
					
					div = $('<a>'+element.menuname+'</a>').appendTo('body');
					div.attr('id', 'mb'+element.menuid);
					div.attr('class', 'easyui-menubutton');
					div.attr('href', 'javascript:void(0)');					
					
					divMenu= $('<div />').appendTo('body');
					divMenu.attr('id', 'mm'+element.menuid);					
				}	
					divMenuItem= $('<div>'+element.itemname+'</div>').appendTo('#mm'+element.menuid);
					divMenuItem.attr('iconCls', element.menuitemsicons);
					divMenuItem.attr('onclick', ""+element.filename+"()");										
			});
			
			$.each(menus, function(index, item) {
				$('#mb'+item).menubutton({
					iconCls: menuicons[i],
					menu: '#mm'+item
				});
				i++;
			});
		},
		async: false
		});			
	}
	
	$("#sub").click(function() {
		$("#frmlogin :input")
	});
	
	/* User profile maintenance  */
	var url;
	var useraction;
	var userprofileid = 0;
	
	function userprofile(){
		$('<div id="winuserprofile"></div>').appendTo('body');
		
		$('#winuserprofile').window({ width:400,height:450,modal:false,padding:10,title:'Maintain User Profile'});
		$('#winuserprofile').window('refresh', 'userprofile.php');
		$('#winuserprofile').window('open');
	}
	
	function newUserprofile(){	
		$('#menuitems').tree({  
			url:'../data/get_data.php?action=12&activity=0&profileid=0' 
		}); 		
		
		$('#dlgUserProfile').dialog('open').dialog('setTitle','New User Profile');
		$('#active').combobox('setValue','1');//selected value
		useraction = 10;
	}
	
	function searchUserprofile(){
		try{
		var val = $('#sup').combobox('getValue');
		$('#dgUserProfile').datagrid('reload',{profileid:val});
		}
		catch(e){
		$.messager.alert("Warning", e);
		}
	}

	function editUserprofile(){
		var row = $('#dgUserProfile').datagrid('getSelected');
		if (row){
			$('#menuitems').tree({  
				url:'../data/get_data.php?action=12&activity=1&profileid='+row.profileid  
			}); 

			$('#dlgUserProfile').dialog('open').dialog('setTitle','Edit User Profile Details');
			$('#fmUserProfile').form('load',row);
			useraction = 11;
			userprofileid = row.profileid;
		}
	}
	
	function saveUserprofile(){ 
		var permissions = new Array();		
		
		var nodes = $('#menuitems').tree('getChecked');			
	    for(var i=0; i<nodes.length; i++){
			var pnode = $('#menuitems').tree('getParent', nodes[i].target);

			if(pnode !== null){
				var childitems = {};
				childitems["itemid"] = nodes[i].id;
				childitems["itemname"] = nodes[i].text;
				
				permissions.push(childitems);
				//permissions.push("{"+"\"itemid"+"\""+":\""+nodes[i].id+"\""+","+"\"itemname"+"\":\""+nodes[i].text+"\""+"}");  
			}				
	    }		
		//if (permissions.length != 0) {
		//	permissions='['+permissions+']';
		//}

		var val_active = $('#profileactive').combobox('getValue');
		var val_profilename  = $('#profilename').val();

		$.ajax({
		type: "POST",
		cache: false,
		url: '../data/get_data.php',
		datatype: "json",
		data: {
			action: useraction,
			profileid: userprofileid,
			profilename: val_profilename,
			active: val_active,
			permissions: JSON.stringify(permissions)	
		},
		success: function(result){
			var response = eval('('+result+')');  
			if (response.retcode === '000'){
				$.messager.alert("Warning", response.retmsg);
				$('#dlgUserProfile').dialog('close');   // close the dialog  
				$('#dgUserProfile').datagrid('reload'); // reload the user data  
				$('#su').combobox('reload'); //reload the data
				$('#fmUserProfile').form('clear'); //clear form				
			} else {  
				$.messager.alert("Warning", response.retmsg); 
			} 				
		},
		error: function(result){
			$.messager.alert("Warning", 'Error Occured when processing user profile');
		}		
		}); 
		/*
		$('#fmUserProfile').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'User Profile Details Saved successfully');
					$('#dlgUserProfile').dialog('close');   // close the dialog  
					$('#dgUserProfile').datagrid('reload'); // reload the user data  
					$('#su').combobox('reload'); //reload the data
					$('#fmUserProfile').form('clear'); //clear form
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});
	*/
	}	
	/* End of User profile management*/	
	/* Start Import M-Pesa Repayments */
	function uploadrepayment(){
		$('<div id="winuploadrepayment"></div>').appendTo('body');
		
	    $('#winuploadrepayment').window({ width:900,height:500,modal:false,title:'Import M-Pesa Repayments'});
		$('#winuploadrepayment').window('refresh', 'uploadrepayment.html');
		$('#winuploadrepayment').window('open');
	}
	
	function uploadmpesarepaymentsfile(){	
		$('#uploadnotification').text('Processing file upload. please wait...');
				
		$('#uploadmpesafile').form('submit',{   
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){
					$('#btnuploadfile').linkbutton('enable');	
					$('#uploadnotification').text(result.retmsg);	
					$('#mpesarepaymentfile').val('');					
				} else {					
					$('#btnuploadfile').linkbutton('enable');
					$('#uploadnotification').text('System failed to process the file. Error : '+ result.retmsg);	
					$('#mpesarepaymentfile').val('');					
				}  
			}  
		});			
	}
	
	/* End Import M-Pesa Repayments */
	/* Start processfinancialperiod*/
	function processfinancialperiod(){
		$('<div id="winprocessfinancialperiod"></div>').appendTo('body');
		
	    $('#winprocessfinancialperiod').window({ width:450,height:200,modal:false,title:'Process Financial Period'});
		$('#winprocessfinancialperiod').window('refresh', 'processfinancialperiod.html');
		$('#winprocessfinancialperiod').window('open');
	}
	
	function processperiod(status){
		var val_periodid = $('#eomperiodmonthid').combobox('getValue');
		
		if(val_periodid.length == 0){
			$.messager.alert("Warning", 'Please select month to process');
			return;
		}
		
		$.ajax({
		type: "POST",
		cache: false,
		url: '../data/get_data.php',
		datatype: "json",
		data: {
			action: 101,
			periodid: val_periodid,
			status: status
		},
		success: function(result){
			var response = eval('('+result+')');  
			if (response.retcode === '000'){
				$.messager.alert("Warning", response.retmsg);
			} else {  
				$.messager.alert("Warning", response.retmsg); 
			} 				
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when processing EOM');
		}		
		}); 
	}
	/* End EOM*/
	/* Start EOY*/
	function eoy(){
		$('<div id="wineoy"></div>').appendTo('body');
		
	    $('#wineoy').window({ width:450,height:200,modal:false,title:'End Of Year Process'});
		$('#wineoy').window('refresh', 'eoy.html');
		$('#wineoy').window('open');
	}
	
	function posteoy(){
		var val_periodyearid = $('#eoyperiodyearid').combobox('getValue');
		
		if(val_periodyearid.length == 0){
			$.messager.$.messager.alert("Warning",  "Warning", 'Please select year to process');
			return;
		}
		
		$.ajax({
		type: "POST",
		cache: false,
		url: '../data/get_data.php',
		datatype: "json",
		data: {
			action: 99,
			periodyearid: val_periodyearid,
			userid: sessvars.userid
		},
		success: function(result){
			var response = eval('('+result+')');  
			if (response.retcode === '000'){
				$.messager.alert("Warning",  response.retmsg);
			} else {  
				$.messager.alert("Warning",  response.retmsg); 
			} 				
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when processing EOY');
		}		
		}); 
	}
	
	/* END EOY*/
	/* Start Period Year*/
	function periodyear(){
		$('<div id="winperiodyear"></div>').appendTo('body');
		
	    $('#winperiodyear').window({ width:550,height:400,modal:false,title:'Maintain Period Year'});
		$('#winperiodyear').window('refresh', 'periodyear.html');
		$('#winperiodyear').window('open');
	}	
		
	function newPeriodyear(){
		$('#dlgperiodyear').dialog('open').dialog('setTitle','New Period Year');
		$('#fmperiodyear').form('clear');
		$('#eoydone').combobox('setValue','0');
		
		var userid=sessvars.userid;
		url = '../data/get_data.php?action=93&userid='+userid;
	}

	function editPeriodyear(){
		var row = $('#dgperiodyear').datagrid('getSelected');
		if (row){
			$('#dlgperiodyear').dialog('open').dialog('setTitle','Edit Period Year');
			$('#fmperiodyear').form('load',row);
			
			var userid=sessvars.userid;
			url = '../data/get_data.php?action=94&periodyearid='+row.periodyearid+'&userid='+userid;
		}
	}
	
	function savePeriodyear(){  
		$('#fmperiodyear').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgperiodyear').dialog('close');      // close the dialog  
					$('#dgperiodyear').datagrid('reload');    // reload the user data  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.msg  
					});  
				}  
			}  
		});  
	}	
	/* End of period Year*/
	/* Start Period*/
	function period(){
		$('<div id="winperiod"></div>').appendTo('body');
		
	    $('#winperiod').window({ width:900,height:450,modal:false,title:'Maintain Period'});
		$('#winperiod').window('refresh', 'period.html');
		$('#winperiod').window('open');
	}
	
	function newPeriod(){
		$('#dlgperiod').dialog('open').dialog('setTitle','New Period');
		$('#fmperiod').form('clear');
		$('#closed').combobox('setValue','0');
		url = '../data/get_data.php?action=96';
	}
	
	function searchPeriod(){
		try{
			var val = $('#ss').combobox('getValue');
			$('#dgperiod').datagrid('reload',{studentid:val});
		}catch(e){
			$.messager.alert("Warning",  e);
		}
	}

	function editPeriod(){
		var row = $('#dgperiod').datagrid('getSelected');
		if (row){
			$('#dlgperiod').dialog('open').dialog('setTitle','Edit Periods');
			$('#fmperiod').form('load',row);
			url = '../data/get_data.php?action=97&periodid='+row.periodid;
		}
	}
	
	function savePeriod(){  
		var userid=sessvars.userid;
		var code=$('#monthno').combobox('getText');
		//$periodyearid,$monthno,$quarter,$code,$description,$startdate,$enddate,$closed,$createdby
		//periodyearid,monthno,quarter,description,startdate,enddate,closed
		url = url+'&createdby='+userid+'&code='+code;
		
		$('#fmperiod').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgperiod').dialog('close');      // close the dialog  
					$('#dgperiod').datagrid('reload');    // reload the user data  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.msg  
					});  
				}  
			}  
		});  
	}	
	/* End of period*/	
	/* Start Users*/
	function users(){
		$('<div id="winusers"></div>').appendTo('body');
		
	    $('#winusers').window({ width:1050,height:450,modal:false,title:'Maintain Users'});
		$('#winusers').window('refresh', 'users.php');
		$('#winusers').window('open');
	}
	
	function newUser(){
		var userid;
		
		$('#dlgUsers').dialog('open').dialog('setTitle','New User');
		$('#idtype').combobox('setValue','1');
		$('#profileid').combobox('setValue','1');
		$('#active').combobox('setValue','1');
		$('#branchid').combobox('setValue','1');
		url = '../data/get_data.php?action=6';
	}
	
	function searchUser(){
		try{
		var val = $('#su').combobox('getValue');
		$('#dgUsers').datagrid('reload',{userid:val});
		}
		catch(e){
		$.messager.alert("Warning", e);
		}
	}

	function editUser(){
		var row = $('#dgUsers').datagrid('getSelected');
		if (row){
			$('#dlgUsers').dialog('open').dialog('setTitle','Edit User Details');
			$('#fmUsers').form('load',row);
			url = '../data/get_data.php?action=9&userid='+row.userid;
		}
	}

	function accountsUser(){
		var row = $('#dgUsers').datagrid('getSelected');
		if (row){	
			$('#dlgUserAccounts').dialog('open').dialog('setTitle','Assign User Accounts');
			url = '../data/get_data.php?action=60&userid='+row.userid;
		}
	}

	function saveUserAccounts(){  
		$('#fmUserAccounts').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning",  result.retmsg);
					$('#dlgUserAccounts').dialog('close');   // close the dialog  
					$('#dgUsers').datagrid('reload'); // reload the user data  
					$('#fmUserAccounts').form('clear'); //clear form
					$('#btnuseraccounts').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
					$('#btnuseraccounts').linkbutton('enable');
				}  
			}  
		});  
	}
	
	function resetUserPassword(){
		var row = $('#dgUsers').datagrid('getSelected');
		
		if (row){
			$('#dlgUserPassord').dialog('open').dialog('setTitle','Reset User Password');
			url = '../data/get_data.php?action=146&userid='+row.userid;
		}
	}

	function saveUserPasswordReset(){  
		$('#fmUserPassord').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", result.retmsg);
					$('#dlgUserPassord').dialog('close');   // close the dialog   
					$('#fmUserPassord').form('clear'); //clear form
					$('#btnresetpassword').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
					$('#btnresetpassword').linkbutton('enable');
				}  
			}  
		});  
	}	
	
	function resetUserLoginAttempts(){
		//debugger;
		//sessvars.username;
		
		var row = $('#dgUsers').datagrid('getSelected');
		if (row){		
			$.get( '../data/get_data.php',{action:147,userid: row.userid}, function( result ) {
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'User Details Saved successfully');
					$('#dgUsers').datagrid('reload'); 
				}else{
					$.messager.alert("Warning", result.retmsg);					
				}
			});
		}		
	}

	function saveUser(){  
		$('#fmUsers').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'User Details Saved successfully');
					$('#dlgUsers').dialog('close');   // close the dialog  
					$('#dgUsers').datagrid('reload'); // reload the user data  
					$('#su').combobox('reload'); //reload the data
					$('#fmUsers').form('clear'); //clear form
					$('#btnuser').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					}); 
					$('#btnuser').linkbutton('enable');					
				}  
			}  
		});  
	}	
	/* End of Users*/
	/* Start Misc */
	function miscellaneouslists(){
		$('<div id="winmiscellaneouslists"></div>').appendTo('body');
		
	    $('#winmiscellaneouslists').window({ width:580,height:400,modal:false,title:'Maintain Miscellaneous Lists'});
		$('#winmiscellaneouslists').window('refresh', 'miscellaneouslists.html');
		$('#winmiscellaneouslists').window('open');
	}	
		
	function newItem(){
		$('#dlgMisc').dialog('open').dialog('setTitle','New Category');
		$('#fmMisc').form('clear');
		$('#parentid').combobox('setValue','1');
		$('#defaultitem').combobox('setValue','0');
		$('#active').combobox('setValue','1');
		
		url = '../data/get_data.php?action=15';
	}
	
	function editItem(){
		var row = $('#dgMisc').datagrid('getSelected');
		if (row){
			$('#dlgMisc').dialog('open').dialog('setTitle','Edit Category');
			$('#fmMisc').form('load',row);
			url = '../data/get_data.php?action=16&id='+row.id;
		}
	}
	
	function saveCategory(){  
		$('#fmMisc').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgMisc').dialog('close');      // close the dialog  
					$('#dgMisc').datagrid('reload');    // reload the user data  
					$('#btnsavecategory').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	} 
	
	function removeItem(){
		var row = $('#dgMisc').datagrid('getSelected');
		if (row){
			$.messager.confirm('Confirm','Are you sure you want to remove this category?',function(r){
				if (r){
					$.post('../data/remove_data.php?action=2',{id:row.ID},function(result){
						if (result.retcode === '000'){
							$('#dgMisc').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg: result.retmsg
							});
						}
					},'json');
				}
			});
		}
	}	
	/* End of Misc */
	/* Start Company Information */
	function companyinformation(){
		$('<div id="wincompanyinformation"></div>').appendTo('body');
	
	    $('#wincompanyinformation').window({ width:580,height:400,modal:false,title:'Company Information'});
		$('#wincompanyinformation').window('refresh', 'companyinformation.html');
		$('#wincompanyinformation').window('open');
	}
	
	function newbranch(){
		$('#dlgbranch').dialog('open').dialog('setTitle','New Branch');
		$('#fmbranch').form('clear');
		$('#ci_active').combobox('setValue','1');			
		url = '../data/get_data.php?action=56';
	}
	
	function editbranch(){
		var row = $('#dgbranch').datagrid('getSelected');
		if (row){
			$('#dlgbranch').dialog('open').dialog('setTitle','Edit Branch');
			$('#fmbranch').form('load',row);
			url = '../data/get_data.php?action=57&branchid='+row.branchid;
		}
	}
	
	function savebranch(){  
		$('#fmbranch').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgbranch').dialog('close');      // close the dialog  
					$('#dgbranch').datagrid('reload');    // reload the user data  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Company Information */
	/* Start Ledger Accounts */
	function ledgeraccounts(){
		$('<div id="winledgeraccounts"></div>').appendTo('body');
		
	    $('#winledgeraccounts').window({ width:1200,height:430,modal:false,title:'Maintain Ledger Accounts'});
		$('#winledgeraccounts').window('refresh', 'ledgeraccounts.php');
		$('#winledgeraccounts').window('open');
	}	
	
	function newLedgerAccount(){
		$('#dlgLedgerAccount').dialog('open').dialog('setTitle','New Ledger Account');
		$('#fmLedgerAccount').form('clear');
		$('#active').combobox('setValue','1');					
		url = '../data/get_data.php?action=49';
	}
	
	function searchLedgerAccouts(){
		try{
		var val = $('#ss').combobox('getValue');
		$('#dg').datagrid('reload',{accountid:val});
		}
		catch(e){
		$.messager.alert("Warning", e);
		}
	}

	function editLedgerAccount(){
		var row = $('#dgLedgerAccount').datagrid('getSelected');
		
		if (row){
			$('#financialcategoryid').combobox('clear');
		    $('#financialcategoryid').combobox('reload', '../data/get_data.php?action=45&categoryid='+row.categoryid);
			
			$('#subfinancialcategoryid').combobox('clear');
			$('#subfinancialcategoryid').combobox('reload', '../data/get_data.php?action=46&parentid='+row.financialcategoryid); 
			
			$('#dlgLedgerAccount').dialog('open').dialog('setTitle','Edit Ledger Account');						
			$('#fmLedgerAccount').form('load',row);
			
			url = '../data/get_data.php?action=50&accountid='+row.accountid;
		}
	}
	
	function saveLedgerAccount(){ 
		url=url+'&userid='+sessvars.userid;
		
		$('#fmLedgerAccount').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgLedgerAccount').dialog('close');      // close the dialog  
					$('#dgLedgerAccount').datagrid('reload');    // reload the user data  
					$('#btnsaveledgeraccount').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});		
	}	
	/* End Ledger Accounts */
	/* Start Ledger transactions */
	function ledgertransaction(){
		$('<div id="winledgertransaction"></div>').appendTo('body');
	
	    $('#winledgertransaction').window({ width:750,height:500,modal:false,title:'Ledger Transactions'});
		$('#winledgertransaction').window('refresh', 'ledgertransactions.php');
		$('#winledgertransaction').window('open');
	}

	function addLedgerTransaction(){
		var valaccountid = $('#ldg_accountid').combobox('getValue');
		var valaccountname = $('#ldg_accountid').combobox('getText');
		var valtrxtyeid = $('#ldg_trxtype').combobox('getValue');
		var valtrxtype = $('#ldg_trxtype').combobox('getText');				
		var valamount= $('#ldg_amount').val();
		
		if (valaccountid.length == 0) {
			$.messager.alert("Warning", "Account is required!");
			return;
		} 
		
		if (valamount.length == 0) {
			$.messager.alert("Warning", "Transaction amount is required!");
		} else {

	   $('#dgtrx').datagrid('appendRow',{
			   accountid:valaccountid,
			   accountname:valaccountname,
			   trxtypeid:valtrxtyeid,
			   trxtype:valtrxtype,
			   amount:valamount
		});
		}		   
	}

	function removeLedgerTransaction(){
		var rows = $('#dgtrx').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			$('#dgtrx').datagrid('deleteRow',0);
		}		   
	}
	
	function postLedgerTransaction(){ 
		$('#dgtrx').datagrid('selectAll');
		var userid=sessvars.userid;
		var trxvaluedate=$('#ldg_valuedate').datebox('getValue');
		var valnarration= $('#ldg_description').val();
		var intdr = 0;
		var intcr = 0;
		
		var trx = new Array();
		var rows = $('#dgtrx').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			if (rows[i].trxtypeid==0){			
				intdr = parseFloat(intdr)+parseFloat(rows[i].amount);
			}else{
				intcr = parseFloat(intcr)+parseFloat(rows[i].amount);
			}
			trx.push("{"+"\"accountid"+"\""+":\""+rows[i].accountid+"\""+","+"\"trxtypeid"+"\":\""+rows[i].trxtypeid+"\""+","+"\"amount"+"\":\""+rows[i].amount+"\""+"}");
		}

		if(intdr != intcr){
			$.messager.alert("Warning", "Debit amount is not equal to Credit Amount!");
			$('#btnpostledgertransaction').linkbutton('enable');
			return;
		}

		if (validatedate(trxvaluedate)== false )
		{
			$('#btnpostledgertransaction').linkbutton('enable');
			return;
		}
		
		if (trx.length != 0){
			trx='['+trx+']';
			
			$.ajax({
			type: "POST",
			url: '../data/get_data.php',
			data: {
				action: 65,
				ledgertrx: trx,
				userid: userid,
				valuedate: trxvaluedate,
				narration: valnarration
			},
			datatype: "json",
			success: function(result){			
				var result = eval('('+result+')');  
				if(result.retcode === '000'){  
					$.messager.alert("Warning", 'Transaction posted successfully for Ledger Transaction ID ('+ result.results +')'); 
					
					for(var i=0;i<rows.length;i++){
						$('#dgtrx').datagrid('deleteRow',0);
					}
					
					$('#amount').val(0);
					$('#description').val('');
					$('#btnpostledgertransaction').linkbutton('enable');
				}else{
					$('#btnpostledgertransaction').linkbutton('enable');
					$.messager.alert("Warning", 'Error occured : '+result.retmsg);					
				}  
			},
			error: function(result){
				$.messager.alert("Warning", 'Error Occured when posting transactions');
				$('#btnpostledgertransaction').linkbutton('enable');
			}
			});
		}else {
			$.messager.alert("Warning", 'There is no transaction to post');
			$('#btnpostledgertransaction').linkbutton('enable');
		}
			
	}	
	/* End Ledger transactions*/

	/* Start Customer Details */
	function customerdetails(){	
	    $('<div id="wincustomerdetails"></div>').appendTo('body');
		
	    $('#wincustomerdetails').window({width:1000,height:450,modal:false,title:'Customer Details'});
		$('#wincustomerdetails').window('refresh', 'customerdetails.html');		
		$('#wincustomerdetails').window('open');
	}

	function newCustomer(){
            
		$('#dlgCustomer').dialog('open').dialog('setTitle','New Customer');
		$('#fmCustomer').form('clear');
		$('#idtype').combobox('setValue','1');		
		var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
		$('#joindate').datebox('setValue', m+'/'+d+'/'+y);
		$('#customertype').combobox('setValue','1');
		$('#addresstype').combobox('setValue','1');
		$('#county').combobox('setValue','1');
		$('#town').combobox('setValue','1');
		$('#countryid').combobox('setValue','1');		
		$('#loanofficerid').combobox('setValue','1');
		$('#maritalstatus').combobox('setValue','2');
		
		url = '../data/get_data.php?action=28&branchid='+sessvars.branchid;
	}

	function editCustomer(){
		var row = $('#dgCustomer').datagrid('getSelected');
		if (row){
			$('#dlgCustomer').dialog('open').dialog('setTitle','Edit Customer');
			$('#fmCustomer').form('load',row);
			url = '../data/get_data.php?action=29&customerid='+row.customerid+'&personid='+row.personid+'&addressid='+row.addressid;
		}
                else
                    $.messager.alert("Warning", 'Error', "Select Customer to edit" , 'warning');
				
	}

	function searchCustomer(){
		try{
			var val_customerid = $('#custcustomerid').val();
			var val_idno = $('#custidno').val();
			var val_mobileno = $('#custmobileno').val();
			
			if (val_customerid == "C00" && (val_idno.length == 0) && (val_mobileno.length == 0))
			{
                            $.messager.alert("Warning",  "Enter valid customer ID or National ID or Mobile No to search", 'warning');
                            return;
			}
			
			$('#dgCustomer').datagrid('reload',{customerid:val_customerid,idno:val_idno,mobileno:val_mobileno});
			
		}catch(e){
			$.messager.alert("Warning",  e , 'warning');
		}
	}
	
	function saveCustomer(){ 	
		$('#fmCustomer').form('submit',{  
			url: url,  
			onSubmit: function(){  				
				if(!$(this).form('validate'))
				{
					$.messager.alert("Warning",  "Please check in all tabs and fill all mandatory fields ", 'warning');
					$('#btnsavecustomer').linkbutton('enable');
                                        return $(this).form('validate');  
                                }
				
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					 $.messager.alert("Warning",  'success', "Customer details saved!");
					$('#dlgCustomer').dialog('close');      // close the dialog  
					$('#dgCustomer').datagrid('reload');    // reload the user data  
					$('#btnsavecustomer').linkbutton('enable');					
				} else {  
					$('#btnsavecustomer').linkbutton('enable');
					
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Customer Details*/	

	/* Start Loan Officers Details */
	function loanofficers(){	
	    $('<div id="winloanofficers"></div>').appendTo('body');
		
	    $('#winloanofficers').window({width:580,height:400,modal:false,title:'Loan Officer Details'});
		$('#winloanofficers').window('refresh', 'loanofficers.html');		
		$('#winloanofficers').window('open');
	}

	function newLoanofficers(){
		$('#dlgLoanofficers').dialog('open').dialog('setTitle','New Loan Officers');
		$('#fmLoanofficers').form('clear');
		$('#active').combobox('setValue','1');		
		$('#branchid').combobox('setValue','1');	
		
		url = '../data/get_data.php?action=24';
	}

	function editLoanofficers(){
		var row = $('#dgLoanofficers').datagrid('getSelected');
		if (row){
			$('#dlgLoanofficers').dialog('open').dialog('setTitle','Edit Loan Officers');
			$('#fmLoanofficers').form('load',row);
			url = '../data/get_data.php?action=25&loanofficerid='+row.loanofficerid;
		}
	}

	function saveLoanofficers(){  
		$('#fmLoanofficers').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgLoanofficers').dialog('close');      // close the dialog  
					$('#dgLoanofficers').datagrid('reload');    // reload the user data 
					$('#btnsaveLoanofficers').linkbutton('enable');					
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Loan Officers Details*/
	/* Start Currency */
	function currency(){
		$('<div id="wincurrency"></div>').appendTo('body');
		
	    $('#wincurrency').window({ width:650,height:430,modal:false,title:'Maintain Currency'});
		$('#wincurrency').window('refresh', 'currency.html');
		$('#wincurrency').window('open');
	}
	
	function newCurrency(){
		$('#dlgCurrency').dialog('open').dialog('setTitle','New Currency');
		$('#fmCurrency').form('clear');
		$('#active').combobox('setValue','1');	
		$('#homecurrency').combobox('setValue','1');			
		url = '../data/get_data.php?action=32';
	}

	function editCurrency(){
		var row = $('#dgCurrency').datagrid('getSelected');
		if (row){
			$('#dlgCurrency').dialog('open').dialog('setTitle','Edit Currency');
			$('#fmCurrency').form('load',row);
			url = '../data/get_data.php?action=33&currencyid='+row.currencyid;
		}
	}

	function saveCurrency(){  
		$('#fmCurrency').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgCurrency').dialog('close');      // close the dialog  
					$('#dgCurrency').datagrid('reload');    // reload the user data  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Currency  */
	/* Start Savings Product */
	function savingsproduct(){
		$('<div id="winsavingsproduct"></div>').appendTo('body');
		
	    $('#winsavingsproduct').window({ width:1300,height:430,modal:false,title:'Maintain Savings Product'});
		$('#winsavingsproduct').window('refresh', 'savingsproduct.html');
		$('#winsavingsproduct').window('open');
	}
	
	function newSavingsProduct(){
		$('#dlgSavingsProduct').dialog('open').dialog('setTitle','New Savings Product');
		$('#fmSavingsProduct').form('clear');
		$('#active').combobox('setValue','1');	
		$('#termdeposit').combobox('setValue','0');    	
		$('#currencyid').combobox('setValue','1');
		$('#producttypeid').combobox('setValue','1');
				
		url = '../data/get_data.php?action=35';
	}

	function editSavingsProduct(){
		var row = $('#dgSavingsProduct').datagrid('getSelected');
		if (row){
			$('#dlgSavingsProduct').dialog('open').dialog('setTitle','Edit Savings Product');
			$('#fmSavingsProduct').form('load',row);
			
			$('#contributions').datagrid({
				data: row.contributions
			});
			
			$('#isboard').combobox('setValue','1');
			
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#effectivedate').datebox('setValue', m+'/'+d+'/'+y);
			
			url = '../data/get_data.php?action=36&productid='+row.productid;
		}
	}

	function addcontribution(){ 
		var valcontributionamount = $('#contributionamount').val();
		var valisboard = $('#isboard').combobox('getValue');
		var valeffectivedate = $('#effectivedate').datebox('getValue');
		
		if (isNaN(valcontributionamount) || (valcontributionamount.length == 0)){
			$.messager.alert("Warning", 'Select Amount cannot be zero(0)');
			return;
		}
		
		$('#contributions').datagrid('appendRow',{
			   id:1,
			   effectivedate:valeffectivedate,
			   amount:valcontributionamount,
			   isboard:valisboard
		});
    }		   

	function removecontribution(){		
		var rows = $('#contributions').datagrid('getSelections');
		
		for(var i=0;i<rows.length;i++){				
			var rowIndex = $("#contributions").datagrid("getRowIndex", rows[i]);
			$('#contributions').datagrid('deleteRow',rowIndex);
		}
    }
	
	function saveSavingsProduct(){  
		//Getting contributions
		$('#contributions').datagrid('selectAll');
		var ids = new Array();
		var rows = $('#contributions').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			ids.push("{"+"\"id"+"\""+":\""+rows[i].id+"\""+","+"\"effectivedate"+"\":\""+rows[i].effectivedate+"\""+","+"\"amount"+"\":\""+rows[i].amount+"\""+","+"\"isboard"+"\":\""+rows[i].isboard+"\""+"}");
		}
		
		ids='['+ids+']';
		
		url = url+'&contributions='+ids;	
	
		$('#fmSavingsProduct').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgSavingsProduct').dialog('close');      // close the dialog  
					$('#dgSavingsProduct').datagrid('reload');    // reload the user data  
					$('#btnsavesavingsproduct').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Savings Product  */	
	/* Start fee*/
	function fee(){
		$('<div id="winfee"></div>').appendTo('body');
		
	    $('#winfee').window({ width:1250,height:430,modal:false,title:'Maintain Fee'});
		$('#winfee').window('refresh', 'fee.html');
		$('#winfee').window('open');
	}
	
	function newFee(){		
		$('#dlgFee').dialog('open').dialog('setTitle','New Fee');	
		$('#fmFee').form('clear');
		$('#feeoption').combobox('setValue','1');//selected value		
		$('#active').combobox('setValue','1');//selected value
		$('#feetype').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=40';
	}
	
	function editFee(){
		var row = $('#dgFee').datagrid('getSelected');
		if (row){
			$('#dlgFee').dialog('open').dialog('setTitle','Edit Fee');
			$('#fmFee').form('load',row);
			url = '../data/get_data.php?action=41&feeid='+row.feeid;
		}
	}
	
	function saveFee(){
		$('#fmFee').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgFee').dialog('close');      // close the dialog  
					$('#dgFee').datagrid('reload');    // reload the user data  
					$('#btnsavefee').linkbutton('enable');
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});	 
	} 
	
	/* End fee*/
	/* Start Savings Account*/
	function savingsaccounts(){
		$('<div id="winsavingsaccounts"></div>').appendTo('body');
		
	    $('#winsavingsaccounts').window({ width:1000,height:500,modal:false,title:'Maintain Members Vehicle Accounts'});
		$('#winsavingsaccounts').window('refresh', 'savingsaccounts.html');
		$('#winsavingsaccounts').window('open');
	}
	
	function getCustomer(){
		try{
			var val_customerid = $('#svg_customerid').val();
			var val_idno = $('#svg_idno').val();
			/*
			if ((val_customerid == 0 || val_customerid.length == 0) && (val_idno == 0 || val_idno.length == 0))
			{
				$.messager.alert("Warning", 'Enter valid search values');
				return;
			}
			*/
                      $('#dgSavingsCustomer').datagrid('reload',{customerid:val_customerid,idno:val_idno});
			$('#dgCustomerAccounts').datagrid('reload',{customerid:val_customerid,idno:val_idno,accountno:0});
			
		}catch(e){
			$.messager.alert("Warning",  e, 'warning');
		}
                
                
	}

	function newSavingsAccounts(){
		var row = $('#dgSavingsCustomer').datagrid('getSelected');
		if (row){
			$('#fmAccounts')[0].reset();
                        $('#savingscustomerdetails').empty();
			$('#savingscustomerdetails').append(row.name);
			$('#dlgSavingsAccounts').dialog('open').dialog('setTitle','Open New Vehicle Account');
			url = '../data/get_data.php?action=55&customerid='+row.customerid;
		}
                else
                    $.messager.alert('Error Message',"Please select customer to create accounts", 'warning');
	}
	function viewAccountTransactions(){
		var row = $('#dgCustomerAccounts').datagrid('getSelected');
		if (row){
                    
			var val_account = row.accountid;
			//$.messager.alert("Warning", row.accountid)
			$('#dlgTransactions').dialog('open').dialog('setTitle','Shares deposit Transaction Logs');
			$('#tbtxsAccounts').datagrid('reload',{accountid:val_account});
		}
                else
                    $.messager.alert('Error Message',"Please select account to view transactions", 'warning');
	}
	function viewSaccoTransactions(){
		var row = $('#dgCustomerAccounts').datagrid('getSelected');
		if (row){
                    
			var val_account = row.accountid;
			//$.messager.alert("Warning", row.accountid)
			$('#dlgSaccoContributionsTransactions').dialog('open').dialog('setTitle','Sacco Contributions Transaction Logs');
			$('#tbtxsSaccoAccounts').datagrid('reload',{accountid:val_account});
		}
                else
                    $.messager.alert('Error Message',"Please select account to view transactions", 'warning');
	}
	
	function viewSavingsAccounts(){
		var row = $('#dgSavingsCustomer').datagrid('getSelected');
		if (row){	
			var val_customerid = row.customerid;
			var val_idno = 0;
			var val_accountno = "";
			
			$('#dgCustomerAccounts').datagrid('reload',{customerid:val_customerid,idno:val_idno,accountno:val_accountno, customerno : 0});
		}
                else
                    $.messager.alert('Error Message',"Please select customer to view accounts", 'warning');
	}
	
	function saveSavingsAccounts(){
		$('#fmAccounts').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$('#dlgSavingsAccounts').dialog('close');      // close the dialog 
					$('#btnsavesavingsaccounts').linkbutton('enable');
                                        $.messager.show({  
						title: 'Success',  
						msg: result.retmsg  
					});  
				} else { 
                                    $('#btnsavesavingsaccounts').linkbutton('enable');
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});	
	}
	/* End  Savings Account*/
	/* Start Customer Transactions  */
	function searchCustomerAccounts(){	
			
			var val_accountno = $('#ctrx_accountno').val();
			var val_customerno = $('#ctrx_customerno').val();
			
			$('#dgCustAccounts').datagrid('reload',{accountno:val_accountno , customerno:val_customerno});
	}
	
	function customertransactions(){
		$('<div id="wincustomertransactions"></div>').appendTo('body');
		
	    $('#wincustomertransactions').window({ width:1050,height:450,modal:false,title:'Customer Transactions'});
		$('#wincustomertransactions').window('refresh', 'customertransactions.html');
		$('#wincustomertransactions').window('open');
	}
	
	function depositTransaction(){
		if(sessvars.cashieraccountid <= 0){
			$.messager.alert("Warning",'User Account Missing. You must be a cashier!' , 'warning');
			return;
		}
		
		var row = $('#dgCustAccounts').datagrid('getSelected');
		if (row){		
			if (row.producttypeid == 4){
				$.messager.alert("Warning", 'You cannot deposit to a loan account', 'warning');
				return;
			}

			$('#dlgposttransaction').dialog('open').dialog('setTitle','Post Customer Deposit Transaction');						
			$('#fmposttransaction').form('clear');
			$('#ctrx_customeraccountdetails').empty();
			$('#ctrx_customeraccountdetails').append(row.customerid+' - '+row.customername);
			$('#ctrx_accountid').append(row.accountid);
			
			$('#ctrx_accountnumber').empty();
			$('#ctrx_accountnumber').append(row.accountnumber +' - '+ row.productname);
			 
			
			$('#ctrx_trxtype').empty();
			$('#ctrx_trxtype').append('Deposit');
			
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#ctrx_valuedate').datebox('setValue', m+'/'+d+'/'+y);
			
			var rows = $('table.posttrx tr');
			rows.filter('.fee').hide();
			rows.filter('.loan').hide();
			
			url = '../data/get_data.php?action=40';
		}
                else
                    $.messager.alert("Warning", 'Select a vehicle savings account', 'warning');
	}

	function withdrawTransaction(){
		if(sessvars.cashieraccountid <= 0){
			$.messager.alert("Warning",'User Account Missing. You must be a cashier!' , 'warning');
			return;
		}
		
		var row = $('#dgCustAccounts').datagrid('getSelected');
		if (row){
			if (row.producttypeid == 1){
				$.messager.alert("Warning", 'You can only withdraw from a savings and deposit account type');
				return;
			}		
			$('#dlgposttransaction').dialog('open').dialog('setTitle','Post Customer Withdraw Transaction');						
			$('#fmposttransaction').form('clear');
			$('#ctrx_customeraccountdetails').empty();
			$('#ctrx_customeraccountdetails').append(row.customerid+' - '+row.customername);
			
			$('#ctrx_accountnumber').empty();
			$('#ctrx_accountnumber').append(row.accountnumber+' - '+row.productname);
			
			$('#ctrx_accountid').empty();
			$('#ctrx_accountid').append(row.accountid);

			$('#ctrx_trxtype').empty();
			$('#ctrx_trxtype').append('Withdraw');

			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#ctrx_valuedate').datebox('setValue', m+'/'+d+'/'+y);
			
			var rows = $('table.posttrx tr');
			rows.filter('.fee').hide();
			rows.filter('.loan').hide();
		}
	}	

	function feePayment(){		
		if(sessvars.cashieraccountid <= 0){
			$.messager.alert("Warning", 'User Account Missing. You must be a cashier!' , 'warning');
			return;
		}
		
		var row = $('#dgCustAccounts').datagrid('getSelected');
		if (row){	
			if (row.producttypeid == 4){
				$.messager.alert("Warning", 'You cannot attached fee to a loan account. Select a savings account');
				return;
			}		
			$('#dlgposttransaction').dialog('open').dialog('setTitle','Post Customer Fee Payment Transaction');						
			$('#fmposttransaction').form('clear');
			$('#ctrx_customeraccountdetails').empty();
			$('#ctrx_customeraccountdetails').append(row.customerid+' - '+row.customername);
			
			$('#ctrx_accountnumber').empty();
			$('#ctrx_accountnumber').append(row.accountnumber+' - '+row.productname);
			
			$('#ctrx_accountid').empty();
			$('#ctrx_accountid').append(row.accountid);

			$('#ctrx_trxtype').empty();
			$('#ctrx_trxtype').append('FeePayment');
			
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#ctrx_valuedate').datebox('setValue', m+'/'+d+'/'+y);
			
			$('#ctrx_feeid').combobox('setValue','1');
			
			var rows = $('table.posttrx tr');
			rows.filter('.fee').show();
			rows.filter('.loan').hide();
		}
                else
                    $.messager.alert("Warning", 'Please select an account to pay fee for', 'warning');
	}
	
	function loanRepayment(){		
		if(sessvars.cashieraccountid <= 0){
			$.messager.alert("Warning",  'User Account Missing. You must be a cashier!', 'warning');
			$('#btnposttransaction').linkbutton('enable');
			return;
		}
		
		var row = $('#dgCustAccounts').datagrid('getSelected');
		if (row){
			if (row.producttypeid != 4){
				$.messager.alert("Warning",  'Incorrect Product Type. Please select a loan account', 'warning');
				$('#btnposttransaction').linkbutton('enable');
				return;
			}
		
			$('#dlgposttransaction').dialog('open').dialog('setTitle','Post Customer Loan Repayment Transaction');						
			$('#fmposttransaction').form('clear');
			$('#ctrx_customeraccountdetails').empty();
			$('#ctrx_customeraccountdetails').append(row.customerid+' - '+row.customername);
			
			$('#ctrx_accountnumber').empty();
			$('#ctrx_accountnumber').append(row.accountnumber+' - '+row.productname);
			
			$('#ctrx_accountid').empty();
			$('#ctrx_accountid').append(row.accountid);

			$('#ctrx_trxtype').empty();
			$('#ctrx_trxtype').append('LoanRepayment');			
		
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#ctrx_valuedate').datebox('setValue', m+'/'+d+'/'+y);
			
			var rows = $('table.posttrx tr');
			rows.filter('.fee').hide();
			rows.filter('.loan').show();

			$('#ctrx_loanbalance').empty();
			$.get( '../data/get_data.php',{action:89,accountid: row.accountid ,valuedate: m+'/'+d+'/'+y }, function( data ) {
				var loandetails = eval('('+data+')');  						

				var output = '';
				output = '<table class=\'payslip\'>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Interest Due</td><td class=\'payslip\'>'+loandetails[0].interestdue+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Principal Due</td><td class=\'payslip\'>'+loandetails[0].principaldue+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Principal Balance</td><td class=\'payslip\'>'+loandetails[0].principalbalance+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Interest Balance</td><td class=\'payslip\'>'+loandetails[0].interestbalance+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Arrears Days</td><td class=\'payslip\'>'+loandetails[0].arrearsdays+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Arrears Amount</td><td class=\'payslip\'>'+loandetails[0].arrearsamount+'</td></tr>';
				output += '<tr class=\'payslip\'><td class=\'payslip\'>Payoff Amount</td><td class=\'payslip\'>'+loandetails[0].payoffamount+'</td></tr>';				
				output += '</table>';

				document.getElementById('ctrx_loanbalance').innerHTML = output;
			});

			$('#ctrx_valuedate').datebox({
				onSelect: function(date){
					var row = $('#dgCustAccounts').datagrid('getSelected');
					if (row){
						$('#ctrx_loanbalance').empty();
						var yy = date.getFullYear();
						var mm = date.getMonth()+1;
						var dd = date.getDate();
						$.get( '../data/get_data.php',{action:89,accountid: row.accountid ,valuedate: mm+'/'+dd+'/'+yy }, function( data ) {
							var loandetails = eval('('+data+')');  						

							var output = '';
							output = '<table class=\'payslip\'>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Interest Due</td><td class=\'payslip\'>'+loandetails[0].interestdue+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Principal Due</td><td class=\'payslip\'>'+loandetails[0].principaldue+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Principal Balance</td><td class=\'payslip\'>'+loandetails[0].principalbalance+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Interest Balance</td><td class=\'payslip\'>'+loandetails[0].interestbalance+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Arrears Days</td><td class=\'payslip\'>'+loandetails[0].arrearsdays+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Arrears Amount</td><td class=\'payslip\'>'+loandetails[0].arrearsamount+'</td></tr>';
							output += '<tr class=\'payslip\'><td class=\'payslip\'>Payoff Amount</td><td class=\'payslip\'>'+loandetails[0].payoffamount+'</td></tr>';				
							output += '</table>';

							document.getElementById('ctrx_loanbalance').innerHTML = output;
						});
					}
				}
			});
		}
	}
	
	function posttransaction(){
		var val_accountid = $('#ctrx_accountid').html();
		var val_feeid = $('#ctrx_feeid').combobox('getValue');
		var val_amount = $('#ctrx_trxamount').val();
		var val_trxtype = $('#ctrx_trxtype').html();
		var val_receiptno = $('#ctrx_receiptno').val();
		var val_trxdescription = $('#ctrx_trxdescription').val();
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
			
		var val_post_valuedate = $('#ctrx_valuedate').datebox('getValue');

		if(val_post_valuedate.length == 0){
			//When date is not picked from datebox
			//val_post_valuedate = m+'/'+d+'/'+y;
			$.messager.alert("Warning",  'Transaction Date not valid. Contact system administrator about this issue', 'warning');
			$('#btnposttransaction').linkbutton('enable');
			return;
		}
		
		if ((val_amount <= 0) || (isNaN(val_amount))){
			$.messager.alert("Warning",  'Invalid Transaction Amount!', 'warning');
			$('#btnposttransaction').linkbutton('enable');
			return;
		}
		if ((isNaN(val_feeid)) && (val_trxtype =='FeePayment')){
			$.messager.alert("Warning",  'Invalid Fee Selected!', 'warning');
			$('#btnposttransaction').linkbutton('enable');
			return;
		}
		

		if (isNaN(val_accountid)){
			$.messager.alert("Warning",  'Invalid Account!', 'warning');
			$('#btnposttransaction').linkbutton('enable');
			return;
		}

		if (validatedate(val_post_valuedate)== false ){
			$('#btnposttransaction').linkbutton('enable');
			return;
		}
		
		if(val_trxtype =='LoanRepayment'){
			$.ajax({
			type: "POST",
			cache: false,
			url: '../data/get_data.php',
			datatype: "json",
			data: {
				action: 90,
				accountid: val_accountid,
				amount: val_amount,
				receiptno: val_receiptno,
				userid: sessvars.userid,
				trxdescription: val_trxdescription,
				valuedate: val_post_valuedate,
				usercontraaccount: sessvars.cashieraccountid
			},
			success: function(result){
				var response = eval('('+result+')');  

				if (response.retcode === '000'){
					$.messager.alert("Warning",  response.retmsg, 'warning');
					$('#fmposttransaction').form('clear');
					$('#dlgposttransaction').dialog('close');
					$('#btnposttransaction').linkbutton('enable');
					searchCustomerAccounts();
				} else {  
					$.messager.alert("Warning", response.retmsg); 
				} 				
			},
			error: function(result){
				$.messager.alert("Warning",  'Error Occured when posting transactions', 'warning');
				$('#btnposttransaction').linkbutton('enable');
			}		
			}); 		
		}else{
			$.ajax({
			type: "POST",
			cache: false,
			url: '../data/get_data.php',
			datatype: "json",
			data: {
				action: 59,
				accountid: val_accountid,
				feeid: val_feeid,
				amount: val_amount,
				trxtype: val_trxtype,
                                receiptno: val_receiptno,
				userid: sessvars.userid,
				trxdescription: val_trxdescription,
				valuedate: val_post_valuedate,
				usercontraaccount: sessvars.cashieraccountid
			},
			success: function(result){
				var response = eval('('+result+')');  

				if (response.retcode === '000'){
					$.messager.alert("Warning",  response.retmsg, 'warning');
					$('#fmposttransaction').form('clear');
					$('#dlgposttransaction').dialog('close');
					$('#btnposttransaction').linkbutton('enable');
					searchCustomerAccounts();
				} else {  
					$.messager.alert("Warning", response.retmsg); 
					$('#btnposttransaction').linkbutton('enable');
				} 				
			},
			error: function(result){
				$.messager.alert("Warning",  'Error Occured when posting transactions', 'warning');
			}		
			}); 
		}
	} 

	function validatedate(valuedate)  {
		var dateformat  = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
		 // Match the date format through regular expression 
		 if(valuedate.match(dateformat))  {
			 //Test which seperator is used '/' or '-' 
			 var opera1  = valuedate.split('/');
			 var opera2  = valuedate.split('-');
			 lopera1  = opera1.length;
			 lopera2  = opera2.length;
			 // Extract the string into month, date and year 
			 if (lopera1 > 1)  {
				var pdate  = valuedate.split('/');
			} else if (lopera2 > 1)  {
				var pdate  = valuedate.split('-');
			}
			var mm  = parseInt(pdate[0]);
			 var dd  = parseInt(pdate[1]);
			 var yy  = parseInt(pdate[2]);
			 // Create list of days of a month [assume there is no leap year by default] 
			 var ListofDays  = [31,28,31,30,31,30,31,31,30,31,30,31];
			 if (mm == 1  || mm > 2)  {
				if (dd > ListofDays[mm - 1])  {
					$.messager.alert("Warning", 'Invalid date format!' , 'warning');
					 return false;
				}
			}
			if (mm == 2)  {
				var lyear  = false;
				 if ( ( ! (yy  % 4)  && yy  % 100)  ||  ! (yy  % 400))  {
					lyear  = true;
				}
				if ((lyear == false)  && (dd >= 29))  {
					$.messager.alert("Warning", 'Invalid date format!', 'warning');
					 return false;
				}
				if ((lyear == true)  && (dd > 29))  {
					$.messager.alert("Warning", 'Invalid date format!', 'warning');
					 return false;
				}
			}
		} else  {
			$.messager.alert("Warning", "Invalid date format!", 'warning');
			 return false;
		}
		return true;
	}
	
	/*  End Customer Transactions  */	
	
	/* Start Reverse Transactions  */
	function searchTransaction(){	
		var receiptno = $('#reversalreceiptno').val();
		$('#dgreversetransaction').datagrid('reload',{receiptno:receiptno});
	}
	
	function reversetransactions(){
		$('<div id="winreversetransactions"></div>').appendTo('body');
		
	    $('#winreversetransactions').window({ width:1200,height:450,modal:false,title:'Reverse Transaction'});
		$('#winreversetransactions').window('refresh', 'reversetransactions.html');
		$('#winreversetransactions').window('open');
	}
	
	function reverseTransaction(){
		var row = $('#dgreversetransaction').datagrid('getSelected');
		if (row){
               $.messager.confirm('Confirm','Are you sure you want to reverse this transaction?',function(r){
   
			$.ajax({
			type: "POST",
			cache: false,
			url: '../data/get_data.php',
			datatype: "json",
			data: {
				action: 63,
				receiptno: row.receiptno,
				userid: sessvars.userid
			},
			success: function(result){
				var response = eval('('+result+')');  

				if (response.retcode === '000'){
					$.messager.alert('Success',response.retms, 'info')
					$('#dgreversetransaction').datagrid('reload',{receiptno:0});
					$('#btnreversetransaction').linkbutton('enable');
				} else {  
					$.messager.alert('Error!',response.retms , 'info')
					$('#btnreversetransaction').linkbutton('enable');
				} 				
			},
			error: function(result){
				 $.messager.alert('Warning','Error Occured when posting transactions', 'warning')
				$('#btnreversetransaction').linkbutton('enable');
			}		
			}); 		
		})
            
            }
	}
	/*  End Reverse Transactions  */
	/* Start Account Statement */
	function accountstatement(){
		$('<div id="winaccountstatement"></div>').appendTo('body');
		
	    $('#winaccountstatement').window({ width:1050,height:450,modal:false,title:'Account Statement'});
		$('#winaccountstatement').window('refresh', 'accountstatement.html');
		$('#winaccountstatement').window('open');
	}

	function searchAccountstatement(){	
		var val_customerid = $('#as_customerid').val();
		var val_idno = $('#as_idno').val();
		var val_accountno = $('#as_accountno').val();		
		var val_fromdate =$('#as_fromdate').datebox('getValue');
		var val_todate =$('#as_todate').datebox('getValue');
		
		if (val_fromdate.length == 0) {
			$.messager.alert("Warning", 'Error','From date is required', 'warning')
                        return;
		}
		
		if (val_todate.length == 0) {
			$.messager.alert("Warning", 'Error','To date is required', 'warning')
			return;
		}	
		
		$('#dgaccountstatement').datagrid('reload',{customerid:val_customerid,idno:val_idno,accountno:val_accountno,fromdate:val_fromdate,todate:val_todate});
	}
	/* End Account Statement */
	/* Start Loan Product */
	function loanproduct(){
		$('<div id="winloanproduct"></div>').appendTo('body');
		
	    $('#winloanproduct').window({ width:900,height:420,modal:false,title:'Loan Product'});
		$('#winloanproduct').window('refresh', 'loanproduct.html');
		$('#winloanproduct').window('open');
	}
	
	function newLoanProduct(){
		$('#dlgLoanProduct').dialog('open').dialog('setTitle','New Loan Product');
		$('#fmLoanProduct').form('clear');
		$('#active').combobox('setValue','1');	   	
		$('#currencyid').combobox('setValue','1');
		$('#producttypeid').combobox('setValue','4');
		
		$('#termperiod').combobox('setValue','1');
		$('#interestcalculationmethod').combobox('setValue','1');
		$('#graceperiodon').combobox('setValue','1');
		$('#daycount').combobox('setValue','1');
		$('#ignoreholidays').combobox('setValue','0');
		$('#useguaranteedshares').combobox('setValue','0');
				
		$('#maxaccounts').numberbox({
			min:1,
			value:1
		});

		$('#minloanterm').numberbox({
			min:1,
			value:1
		});		

		$('#maxloanterm').numberbox({
			min:1,
			value:99999
		});		
		
		$('#minloanamount').numberbox({
			min:1,
			value:1
		});	

		$('#maxloanamount').numberbox({
			min:1,
			value:9999999
		});	

		$('#interestrate').numberbox({
			min:0,
			value:0
		});	

		$('#minimumsavings').numberbox({
			min:0,
			value:0
		});	

		$('#graceperiod').numberbox({
			min:0,
			value:0
		});	
		
		url = '../data/get_data.php?action=70';
	}

	function addonerow(){
		var valfeeid = $('#selectloanfee').combobox('getValue');
		var valfeename = $('#selectloanfee').combobox('getText');

		if (isNaN(valfeeid) || (valfeeid.length == 0)){
			 $.messager.alert('Error','Select fee to add!', 'warning')
			return;
		}
		
		$('#fee').datagrid('appendRow',{
			   feeid:valfeeid,
			   feename:valfeename
		});
    }		   

	function removeselectedrow(){		
		$('#fee').datagrid('deleteRow',0);
    }			
	
	function editLoanProduct(){
		var row = $('#dgLoanProduct').datagrid('getSelected');
		if (row){
			$('#dlgLoanProduct').dialog('open').dialog('setTitle','Edit Loan Product');
			$('#fmLoanProduct').form('load',row);
			
			$('#fee').datagrid({
				data: row.fee
			});
			
			$('#guaranteeproducts').datagrid({
				data: row.guaranteeproducts
			});

			if(row.futureinterest == "1"){
				$('#futureinterest').prop('checked', true);
			}
			
			if(row.accrueinterest == "1"){
				$('#accrueinterest').prop('checked', true);
			}
			
			if(row.allowloantopup == "1"){
				$('#allowloantopup').prop('checked', true);
			}
			
			url = '../data/get_data.php?action=71&productid='+row.productid;
		}
	}

	function saveLoanProduct(){
		//Getting fees
		$('#fee').datagrid('selectAll');
		var ids = new Array();
		var rows = $('#fee').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			ids.push("{"+"\"feeid"+"\""+":\""+rows[i].feeid+"\""+","+"\"feename"+"\":\""+rows[i].feename+"\""+"}");
		}
		
		ids='['+ids+']';
		
		url = url+'&fee='+ids;
		
		//Getting Guarantee products
		$('#guaranteeproducts').datagrid('selectAll');
		var guaranteeproducts = new Array();
		var rows_guarantor = $('#guaranteeproducts').datagrid('getSelections');
		for(var i=0;i<rows_guarantor.length;i++){
			guaranteeproducts.push("{"+"\"productid"+"\""+":\""+rows_guarantor[i].productid+"\""+","+"\"productname"+"\""+":\""+rows_guarantor[i].productname+"\""+"}");
		}
		
		guaranteeproducts='['+guaranteeproducts+']';
		
		url = url+'&guaranteeproducts='+guaranteeproducts;		

		//More settings
		var val_futureinterest = 0;
		var val_accrueinterest = 0;
		
		if(document.getElementById('futureinterest').checked){
			val_futureinterest = 1;
		}
		
		if(document.getElementById('accrueinterest').checked){
			val_accrueinterest = 1;
		}
		
		url = url+'&valfutureinterest='+val_futureinterest;
		url = url+'&valaccrueinterest='+val_accrueinterest;

		//More settings
		var val_allowloantopup = 0;
		var val_percentageoftopuploanpaid = 0;
		
		if(document.getElementById('allowloantopup').checked){
			val_allowloantopup = 1;
			val_percentageoftopuploanpaid = $('#percentageoftopuploanpaid').val();;
		}else{
			val_percentageoftopuploanpaid = 0;
		}
				
		url = url+'&valallowloantopup='+val_allowloantopup;
		url = url+'&valpercentageoftopuploanpaid='+val_percentageoftopuploanpaid;
		
		$('#fmLoanProduct').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate'); 			
			},  
			success: function(response){  
				var response = eval('('+response+')');  
				if (response.retcode === '000'){  
					for(var i=0;i<rows.length;i++){
						$('#fee').datagrid('deleteRow',0);
					}
					$('#dlgLoanProduct').dialog('close');      // close the dialog  
					$('#dgLoanProduct').datagrid('reload');    // reload the user data 
					$('#btnsaveloanproduct').linkbutton('enable');
					 $.messager.alert('Success',response.retmsg, 'info')
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: response.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Loan Product */
	/* Start Loan Application */
	function loanapplication(){
		$('<div id="winloanapplication"></div>').appendTo('body');
		
	    $('#winloanapplication').window({ width:800,height:450,modal:false,title:'Loan Application'});
		$('#winloanapplication').window('refresh', 'loanapplication.html');
		$('#winloanapplication').window('open');
	}

	function getCustomerLoanApp(){
		try{
			var val_customerid = $('#loanapplication_customerid').val();
			var val_idno = $('#loanapplication_idno').val();

			$('#dgloanapplication').datagrid('reload',{customerid:val_customerid,idno:val_idno});
			
		}catch(e){
			 $.messager.alert('Error',e , 'warning')
		}
	}

	function getschedule(){
		var val_productid = $('#productid').combobox('getValue');
		var val_loanamount = $('#loanamount').val();
		var val_loanterm = $('#loanterm').val();
                var val_disbursementdate = $('#disbursementdate').datebox('getValue');
				
		if(!($('#fmloanapplication').form('validate'))){
			$.messager.alert("Warning",  'Please specify value for all mandatory fields', 'warning');
			return;
		}
		
		if(val_loanamount <= 0){
			$.messager.alert("Warning",  'Please specify correct loan amount!', 'warning');
			return;
		}
		
		if(val_productid.length <= 0){
			$.messager.alert("Warning",  'Please select loan product!', 'warning');
			return;		
		}
		
		$.get( "../data/get_data.php",{action:79,productid:val_productid,loanamount:val_loanamount,loanterm:val_loanterm,disbursementdate:val_disbursementdate,print:1}, function( data ) {
			document.getElementById("installmentschedule").innerHTML = data;
		});	
	}
	
	function newLoanApplication(isnewloan){
		var row = $('#dgloanapplication').datagrid('getSelected');
		if (row){
			if (isnewloan == 0){
				//Unhide trloantotopup
				var trloantotopup_0_style = document.getElementById("trloantotopup").style;
				trloantotopup_0_style.display = '';
								
				//Unhide trtopupamount
				var trtopupamount_0_style = document.getElementById("trtopupamount").style;
				trtopupamount_0_style.display = '';
				
				//Unhide trcurrentloanbalance
				var trcurrentloanbalance_0_style = document.getElementById("trcurrentloanbalance").style;
				trcurrentloanbalance_0_style.display = '';
				
				//Hide loanproduct
				var loanproduct_0_style = document.getElementById("loanproduct").style;
				loanproduct_0_style.display = 'none';	
				
				$('#loantotopup').combobox({
					url:'../data/get_data.php?action=109&customerid='+row.customerid+'&idno=0&iscustomerid=1&loanstatus=3',
					valueField:'loanid',
					textField:'loaniddesc'
				});	

				$('#currentloanbalance').numberbox('disable');
				$('#loanamount').numberbox('disable');
			}
			
			if (isnewloan == 1){
				//Hide trloantotopup
				var trloantotopup_1_style = document.getElementById("trloantotopup").style;
				trloantotopup_1_style.display = 'none';

				//Hide trtopupamount
				var trtopupamount_1_style = document.getElementById("trtopupamount").style;
				trtopupamount_1_style.display = 'none';
				
				//Unhide trcurrentloanbalance
				var trcurrentloanbalance_1_style = document.getElementById("trcurrentloanbalance").style;
				trcurrentloanbalance_1_style.display = 'none';
				
				//UnHide loanproduct
				var loanproduct_1_style = document.getElementById("loanproduct").style;
				loanproduct_1_style.display = '';

				$('#loanamount').numberbox('enable');
				$('#loantotopup').combobox('reload',[]);
			}
			
			$('#loanapplicationcustomerdetails').empty();		
			$('#loanapplicationcustomersavings').empty();		
			$('#loanapplicationcustomerdetails').append(row.name+' - '+row.idtypedesc+'('+row.idno+')'); 
			$('#loanapplicationcustomersavings').append(row.balance); 
			
			$('#loanapplicationcustomerid').empty();		
			$('#loanapplicationcustomerid').append(row.customerid);				
	
			$('#dlgloanapplication').dialog('open').dialog('setTitle','New Loan Application');
			$('#fmloanapplication').form('clear');
			$('#installmentschedule').empty();
			$('#loanproductdetails').empty();
			
			$('#lappguarantorstatus').combobox('setValue','1');
			$('#loanreason').combobox('setValue','1');
			$('#sourceoffunds').combobox('setValue','1');
			
			$('#loanamount').numberbox({
				min:0,
				value:0
			});		

			$('#topupamount').numberbox({
				min:0,
				value:0
			});
			
			$('#currentloanbalance').numberbox({
				min:0,
				value:0
			});
			
			$('#loanterm').numberbox({
				min:1,
				value:1
			});	
			
			$('#graceperiod').numberbox({
				min:0,
				value:0
			});	

			$('#loanappgualantors').datagrid('reload');
			$('#lappguarantorid').empty();
			$('#lappguarantorname').empty();
			$('#lappguarantordetails').empty();
			$('#lappguaranteedamount').numberbox('setValue', 0);
			$('#lappguarantorstatus').combobox('setValue','0');
		}
		
		url = '../data/get_data.php?action=70';
	}
	
	function validatetopupamount(){
		var val_topupamount = $('#topupamount').val();
		var val_currentloanbalance = $('#currentloanbalance').val();
		var val_loanamount = 0;
		
		if(val_topupamount <=0){
			$.messager.alert("Warning", 'Please specify correct topup Amount' , 'warning');
			$('#loanamount').val(0);
			$('#topupamount').val(0);
			return;
		}

		if(val_currentloanbalance <=0){
			$.messager.alert("Warning", 'You cannot topup a loan with zero balance', 'warning');
			$('#loanamount').val(0);
			$('#topupamount').val(0);
			return;
		}
		
		val_loanamount = (parseFloat(val_topupamount)+parseFloat(val_currentloanbalance));
		$('#loanamount').numberbox('setValue', val_loanamount);
	}
        
        function validateAmount(){
		var val_amount = $('#loanamount').val();
		var var_savings = $('#loanapplicationcustomersavings').html();
               
               if(parseInt(val_amount) > parseInt(var_savings)){
                    if($.messager.confirm( 'Confirmation Message', 'Please add guarantors for the extra amount of ' + (val_amount - var_savings), 'warning'))
                    {
                        return;
                    }
		
		}
		
	}
	
	function addloanappguarantor(){
		var valguarantorid = document.getElementById("lappguarantorid").innerHTML;
		var valguarantorname = document.getElementById("lappguarantorname").innerHTML;
		var valguaranteedamount = $('#lappguaranteedamount').val();
		var valguarantorstatus = $('#lappguarantorstatus').combobox('getValue');
		var valguarantorloanid = document.getElementById("lappguarantorloanid").innerHTML;;

		if (isNaN(valguarantorid) || (valguarantorid.length == 0)){
			 $.messager.alert('Error','Select guarantor to add!')
			return;
		}
		
		if (isNaN(valguaranteedamount) || (valguaranteedamount.length == 0)){
			 $.messager.alert('Error','elect Amount cannot be zero(0)')
			return;
		}
		
		$('#loanappgualantors').datagrid('appendRow',{
			   guarantor_customerid:valguarantorid,
			   guarantor_name:valguarantorname,
			   guarantor_amount:valguaranteedamount,
			   guarantor_status:valguarantorstatus,
			   guarantor_loan_id:valguarantorloanid
		});
    }		   

	function removeloanappguarantor(){		
		var rows = $('#loanappgualantors').datagrid('getSelections');
		
		for(var i=0;i<rows.length;i++){				
			var rowIndex = $("#loanappgualantors").datagrid("getRowIndex", rows[i]);
			$('#loanappgualantors').datagrid('deleteRow',rowIndex);
		}
    }

	function searchloanappguarantor(){	
		var customerid = $('#lappg_customerid').val();
		var guarantor = [];

		$.get( '../data/get_data.php',{action:102,customerid: customerid}, function( data ) {
			var guarantor = JSON.parse(data);
			
			if (products == ''){
				document.getElementById('lappguarantordetails').innerHTML = 'Customer not found';
				document.getElementById('lappguarantorid').innerHTML = '';
				document.getElementById('lappguarantorname').innerHTML = '';
			}
			else
			{
				document.getElementById('lappguarantoridno').innerHTML = customerid;
				document.getElementById('lappguarantorid').innerHTML = guarantor.customerid;
				document.getElementById('lappguarantorname').innerHTML = guarantor.customername;				

				var output = '';
				var products = guarantor.products;
				
				output = '<table class=\'payslip\'>';
                                output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>Product Name</strong></td><td class=\'payslip\' align=\'right\'><strong>Product type</strong></td><td class=\'payslip\'>Amount</td></tr>';
				for(var i in products)
				{
					output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>'+products[i].productname+' :</strong></td><td class=\'payslip\' align=\'right\'><strong>'+products[i].producttype+' :</strong></td><td class=\'payslip\'>'+products[i].balance+'</td></tr>';
				}
                                
				output += '</table>';

				document.getElementById('lappguarantordetails').innerHTML = output;
			}
		}); 
		
		if(guarantor.length == 0){
			document.getElementById('lappguarantordetails').innerHTML = 'Customer not found';
			document.getElementById('lappguarantorid').innerHTML = '';
			document.getElementById('lappguarantorname').innerHTML = '';
		}		
    }
	
	function saveloanapplication(){
		var val_productid = $('#productid').combobox('getValue');
		var val_topuploanid = $('#loantotopup').combobox('getValue');
		var val_topupamount = $('#topupamount').val();		
		var istopup = 0;
		var val_amount = $('#loanamount').val();
		var val_loanreason =  $('#loanreason').combobox('getValue'); 
		var val_fundsource =  $('#sourceoffunds').combobox('getValue');
		var val_applicationnote =  $('#loanapplicationnotes').val();
		var val_customerid  = document.getElementById("loanapplicationcustomerid").innerHTML;
		var val_graceperiod  = $('#graceperiod').val();
		var val_loanterm  =  $('#loanterm').val();
		var userid = sessvars.userid;
		var val_disbursementdate = $('#disbursementdate').datebox('getValue');; 
                
               var var_savings = $('#loanapplicationcustomersavings').html();
                
		
		if(!($('#fmloanapplication').form('validate'))){
			$('#btnsaveloanapplication').linkbutton('enable');
			return;
		}
		
		if(val_topuploanid.length > 0){ 
			istopup = 1;		
		}
				
		if((val_topupamount <= 0) && (istopup == 1)){
			$.messager.alert("Warning",  'Please specify correct topup amount!', 'warning');
			$('#btnsaveloanapplication').linkbutton('enable');
			return;
		}

		if(val_amount <= 0){
			$.messager.alert("Warning", 'warning', 'Please specify correct amount!', 'warning');
			$('#btnsaveloanapplication').linkbutton('enable');
			return;
		}
		
		
		if(val_customerid.length <= 0){
			$.messager.alert("Warning",  'Customer ID not selected!', 'warning');
			$('#btnsaveloanapplication').linkbutton('enable');
			return;		
		}
		
		//Validate Guarantors
		$('#loanappgualantors').datagrid('selectAll');
		var guarantors = new Array();
		var rows = $('#loanappgualantors').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			guarantors.push("{"+"\"loanid"+"\""+":\""+rows[i].guarantor_loan_id+"\""+","+"\"customerid"+"\":\""+rows[i].guarantor_customerid+"\","+"\"guarantorstatus"+"\":\""+rows[i].guarantor_status+"\","+"\"guaranteedamount"+"\":\""+rows[i].guarantor_amount+"\""+"}");
		}
		guarantors ='['+guarantors+']';
		
		$guarantorerror = 0;
		$guarantorerrormsg = '';
              $.ajax({
		type: "POST",
		url: '../data/get_data.php',
		data: {
			action: 103,
			guarantors: guarantors,
			validate: 1, 
			productid: val_productid,
			disbursedamount: val_amount,
			istopuploan: istopup
		},
		datatype: "json",
		async: false,
		success: function(result){			
			var result = eval('('+result+')');	
			if (result.retcode === '000'){  
				$guarantorerror = 0;
				$guarantorerrormsg = result.retmsg;	
				
			} else {  
				$guarantorerrormsg = result.retmsg;  				
				$guarantorerror = 1;					
			}  
		},
		error: function(result){
			$guarantorerrormsg = 'Error Occured when adding loan guarantors';	
			$guarantorerror = 1;			
		}
		});	
		
		if($guarantorerror == 1){
			$.messager.alert("Warning", $guarantorerrormsg, 'warning');
			
			$('#btnsaveloanapplication').linkbutton('enable');
			return;
		}
		
		$.ajax({
			type: "POST",
			url: '../data/get_data.php',
			data: {
				action: 82,
				disbursementdate: val_disbursementdate,
				customerid: val_customerid,
				productid: val_productid,
				amount: val_amount,
				loanterm: val_loanterm,
				loanreason: val_loanreason,
				fundsource: val_fundsource,
				graceperiod: val_graceperiod,
				applicationnote: val_applicationnote,
				applicationby: userid,
				guarantors: guarantors,
				istopup: istopup,
				topuploanid: val_topuploanid,
				topupamount: val_topupamount
			},
			datatype: "json",
			success: function(result){			
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning",  'Loan application created successfully for Loan ID ('+ result.results +')' , 'warning');
					$('#btnsaveloanapplication').linkbutton('enable');
					$('#dlgloanapplication').dialog('close');			
				}else{  
					$.messager.alert("Warning", 'Error Occured : '+result.retmsg);
					$('#btnsaveloanapplication').linkbutton('enable');			
				}  
			},
			error: function(result){
				$.messager.alert("Warning",  'Error Occured when creating loan application', 'warning');
				$('#btnsaveloanapplication').linkbutton('enable');	
			}
		});	
	}	
	/* End Loan Application */
	/* Start Loan Approval */
	function loanapproval(){
		$('<div id="winloanapproval"></div>').appendTo('body');
		
	    $('#winloanapproval').window({ width:1200,height:450,modal:false,title:'Loan Approval'});
		$('#winloanapproval').window('refresh', 'loanapproval.html');
		$('#winloanapproval').window('open');
		
				var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			
			$('#approve_fromdate').datebox('setValue', m+'/'+d+'/'+y);
	}

	function searchforappliedloans(){
		try{
			var val_customerid = $('#approve_customerid').val();
			var val_loanid = $('#approve_loanid').val();
			var val_fromdate = $('#approve_fromdate').datebox('getValue');
			var val_todate = $('#approve_todate').datebox('getValue');

			$('#dgapproveloan').datagrid('reload',{customerid:val_customerid,loanid:val_loanid,fromdate:val_fromdate,todate:val_todate,branchid:sessvars.branchid,loanstatus:1});
			
		}catch(e){
			$.messager.alert("Warning", e);
		}
	}
	
	function approveloans(){		
		var rows = $('#dgapproveloan').datagrid('getChecked');
		if (rows.length <= 0){
			$.messager.alert("Warning",  'No loan selected', 'warning');
			return;
		}
		
		var loans = new Array();
		for(var i=0;i<rows.length;i++){
			loans.push("{"+"\"loanid"+"\""+":\""+rows[i].loanid+"\""+"}");
		}
		
		loans='['+loans+']';
		
		var val_approvalnote = '';
		
		$.ajax({
		type: "POST",
		url: '../data/get_data.php',
		data: {
			action: 84,
			loans: loans,
			approvalnote: val_approvalnote,
			approvedby: sessvars.userid,
			approve: 1
		},
		datatype: "json",
		success: function(result){			
		var result = eval('('+result+')');  
		if (result.retcode === '000'){  
			$.messager.alert("Warning", "Success", 'Loan application approved successfully for Loan ID ('+ result.results +')', 'success');
			searchforappliedloans();			
		} else {  
				$.messager.show({  
					title: 'Error',  
					msg: result.retmsg  
				});  
			}  
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when approving loan application', 'warning');
		}
		});	
	}	
	
	function rejectloans(source){
		var rows;
		var val_approvalnote = '';
		
		if(source == 1){
			rows = $('#dgapproveloan').datagrid('getChecked');			
			val_approvalnote = 'Rejected at loan approval';
		}
		
		if(source == 2){
			rows = $('#dgdisburseloan').datagrid('getChecked');			
			val_approvalnote = 'Rejected at loan disbursement';
		}
		
		
		if (rows.length <= 0){
			$.messager.alert("Warning",  'No loan selected', 'warning');
			return;
		}
		
		var loans = new Array();
		for(var i=0;i<rows.length;i++){
			loans.push("{"+"\"loanid"+"\""+":\""+rows[i].loanid+"\""+"}");
		}

		loans='['+loans+']';
		
		$.ajax({
		type: "POST",
		url: '../data/get_data.php',
		data: {
			action: 84,
			loans: loans,
			approvalnote: val_approvalnote,
			approvedby: sessvars.userid,
			approve: 0
		},
		datatype: "json",
		success: function(result){			
		var result = eval('('+result+')');  
		if (result.retcode === '000'){  
			$.messager.alert("Warning",  'Loan application rejected successfully for Loan ID ('+ result.results +')', 'warning');
			searchforappliedloans();			
		} else {  
				$.messager.show({  
					title: 'Error',  
					msg: result.retmsg  
				});  
			}  
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when approving loan application', 'warning');
		}
		});	
	}		
	/* End Loan Approval */
	/* Start Maintain Loan Guarantor */
	function guarantors(){
		$('<div id="winguarantors"></div>').appendTo('body');
		
	    $('#winguarantors').window({ width:1200,height:450,modal:false,title:'Maintain Loan Guarantors'});
		$('#winguarantors').window('refresh', 'guarantors.html');
		$('#winguarantors').window('open');
	}

	function searchforloans(){
		try{
			var val_customerid = $('#guarantor_customerid').val();
			var val_loanid = $('#guarantor_loanid').val();
			var val_fromdate = $('#guarantor_fromdate').datebox('getValue');
			var val_todate = $('#guarantor_todate').datebox('getValue');

			$('#dgguarantorloan').datagrid('reload',{customerid:val_customerid,loanid:val_loanid,fromdate:val_fromdate,todate:val_todate});
			
		}catch(e){
			$.messager.alert("Warning", e);
		}
	}

	function addguarantor(){
		var valguarantorid = document.getElementById("guarantorid").innerHTML;
		var valguarantorname = document.getElementById("guarantorname").innerHTML;
		var valguaranteedamount = $('#guaranteedamount').val();
		var valguarantorstatus = $('#guarantorstatus').combobox('getValue');
		var valguarantorloanid = document.getElementById("guarantorloanid").innerHTML;;

		if (isNaN(valguarantorid) || (valguarantorid.length == 0)){
			$.messager.alert("Warning", 'Select guarantor to add!');
			return;
		}
		
		if (isNaN(valguaranteedamount) || (valguaranteedamount.length == 0)){
			$.messager.alert("Warning",  'Select Amount cannot be zero(0)', 'warning');
			return;
		}
		
		$('#gualantors').datagrid('appendRow',{
			   guarantor_customerid:valguarantorid,
			   guarantor_name:valguarantorname,
			   guarantor_amount:valguaranteedamount,
			   guarantor_status:valguarantorstatus,
			   guarantor_loan_id:valguarantorloanid
		});
    }		   

	function removeguarantor(){		
		var rows = $('#gualantors').datagrid('getSelections');
		
		for(var i=0;i<rows.length;i++){				
			var rowIndex = $("#gualantors").datagrid("getRowIndex", rows[i]);
			$('#gualantors').datagrid('deleteRow',rowIndex);
		}
    }			

	function addguaranteeproduct(){
		var valproductid = $('#selectguaranteeproduct').combobox('getValue');
		var valproductname = $('#selectguaranteeproduct').combobox('getText');

		if (isNaN(valproductid) || (valproductid.length == 0)){
			$.messager.alert("Warning", 'Select Product to add!');
			return;
		}
		
		$('#guaranteeproducts').datagrid('appendRow',{
			   productid:valproductid,
			   productname:valproductname
		});
    }		   

	function removeguaranteeproduct(){		
		var rows = $('#guaranteeproducts').datagrid('getSelections');
		
		for(var i=0;i<rows.length;i++){				
			var rowIndex = $("#guaranteeproducts").datagrid("getRowIndex", rows[i]);
			$('#guaranteeproducts').datagrid('deleteRow',rowIndex);
		}		
    }
	
	function searchguarantor(){	
		var customerid = $('#g_customerid').val();
		var guarantor = [];

		$.get( '../data/get_data.php',{action:102,customerid: customerid}, function( data ) {
			var guarantor = JSON.parse(data);
			
			if (guarantor.name == ''){
				document.getElementById('guarantordetails').innerHTML = 'Customer not available';
				document.getElementById('guarantorid').innerHTML = '';
				document.getElementById('guarantorname').innerHTML = '';
			}
			else
			{
				document.getElementById('guarantorid').innerHTML = customerid;
				document.getElementById('guarantorname').innerHTML = guarantor.customername;				

				var output = '';
				var products = guarantor.products;
				
				output = '<table class=\'payslip\'>';
				for(var i in products)
				{
					output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>'+products[i].productname+' :</strong></td><td class=\'payslip\'>'+products[i].balance+'</td></tr>';
				}
				output += '</table>';

				document.getElementById('guarantordetails').innerHTML = output;
			}
		}); 
		
		if(guarantor.length == 0){
			document.getElementById('guarantordetails').innerHTML = 'Customer not available';
			document.getElementById('guarantorid').innerHTML = '';
			document.getElementById('guarantorname').innerHTML = '';
		}		
    }
	
	function maintainguarantors(){
		var row = $('#dgguarantorloan').datagrid('getSelected');
		if (row){				
			$('#dlgmaintainguarantor').dialog('open').dialog('setTitle','Maintain Guarantors');	
			document.getElementById('guarantorloanid').innerHTML = row.loanid; 
		}
				
		$('#guarantorstatus').combobox('setValue','1');//selected value
		document.getElementById('guarantordetails').innerHTML = '';
		document.getElementById('guarantorid').innerHTML = '';
		document.getElementById('guarantorname').innerHTML = '';
		$('#g_customerid').numberbox('clear');
		$('#guaranteedamount').numberbox('clear');
		
		$('#gualantors').datagrid('reload',{loanid:row.loanid});
		
		
		url = '../data/get_data.php?action=70';
	}
	
	function saveloanguarantors(){
		$('#gualantors').datagrid('selectAll');
		var guarantors = new Array();
		var rows = $('#gualantors').datagrid('getSelections');
		for(var i=0;i<rows.length;i++){
			guarantors.push("{"+"\"loanid"+"\""+":\""+rows[i].guarantor_loan_id+"\""+","+"\"customerid"+"\":\""+rows[i].guarantor_customerid+"\","+"\"guarantorstatus"+"\":\""+rows[i].guarantor_status+"\","+"\"guaranteedamount"+"\":\""+rows[i].guarantor_amount+"\""+"}");
		}
		
		guarantors ='['+guarantors+']';

		if (rows.length <= 0){
			$.messager.alert("Warning", 'No guarantor added for this loan');
			return;
		}
		
		$.ajax({
		type: "POST",
		url: '../data/get_data.php',
		data: {
			action: 103,
			guarantors: guarantors,
			validate: 0, 
			productid: 0,
			disbursedamount :0
		},
		datatype: "json",
		success: function(result){			
			var result = eval('('+result+')');  
			if (result.retcode === '000'){  
				$.messager.alert("Warning", 'Loan guarantors added successfully');
				$('#dlgmaintainguarantor').dialog('close');		
				$('#btnsaveloanguarantors').linkbutton('enable');
			} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when adding loan guarantors', 'warning');
		}
		});	
	}	
	/* End Maintain Loan Guarantor */	
	/* Start Loan Disbursement */
	function loandisbursement(){
		$('<div id="winloandisbursement"></div>').appendTo('body');
		
	    $('#winloandisbursement').window({ width:1200,height:450,modal:false,title:'Loan Disbursement'});
		$('#winloandisbursement').window('refresh', 'loandisbursement.html');
		$('#winloandisbursement').window('open');
	}

	function searchforapprovedloans(){
		try{
			var val_customerid = $('#disburse_customerid').val();
			var val_loanid = $('#disburse_loanid').val();
			var val_fromdate = $('#disburse_fromdate').datebox('getValue');
			var val_todate = $('#disburse_todate').datebox('getValue');

			$('#dgdisburseloan').datagrid('reload',{customerid:val_customerid,loanid:val_loanid,fromdate:val_fromdate,todate:val_todate,branchid:0,loanstatus:2});
			
		}catch(e){
			$.messager.alert("Warning", e);
		}
	}
	
	function disburseloans(isbatch){
		var rows = $('#dgdisburseloan').datagrid('getChecked');
		
		if (rows.length <= 0){
			$.messager.alert("Warning",  'No loan selected for disbursement', 'warning');
			return;
		}		
		if(isbatch == 1){
			$('#dlgdisburseloan').dialog('open').dialog('setTitle','Batch Loan Disbursement');
			$('#fmdisburseloan').form('clear');
			$('#disbursenote').empty();	
			$('#disbursecustomerid').empty();				
			$('#disbursecustomername').empty();	
			$('#errorsnotification').empty();		
		}else {
			if (rows.length > 1 ){
				$.messager.alert("Warning", 'Use Batch Laon disbursement option');
				return;
			}
			
			$('#dlgdisburseloan').dialog('open').dialog('setTitle','Batch Loan Disbursement');
			$('#fmdisburseloan').form('clear');
			$('#disbursenote').empty();		
			$('#errorsnotification').empty();

		    $('#disbursecustomername').empty();		
			$('#disbursecustomername').append('Customer ID :');

		    $('#disbursecustomerid').empty();		
			$('#disbursecustomerid').append(rows[0].customerid);			
		}
	}
	
	function saveLoanDisbursement(){
		if(!($('#fmdisburseloan').form('validate'))){
			return;
		}	
		
		var rows = $('#dgdisburseloan').datagrid('getChecked');
		if (rows.length <= 0){
			$.messager.alert("Warning",  'No loan selected for disbursement', 'warning');
			return;
		}
		
		var loans = new Array();
		for(var i=0;i<rows.length;i++){
			loans.push("{"+"\"loanid"+"\""+":\""+rows[i].loanid+"\""+"}");
		}
		
		loans='['+loans+']';
		
		var val_disbursenote = $('#disbursenote').val();
		var val_disbursementaccount = $('#disbursementaccount').combobox('getValue'); 
		var val_disbursementoption  = $('#disbursementoption').combobox('getValue'); 
		var val_disbursementdate = $('#ds_disbursementdate').datebox('getValue');
		$.ajax({
		type: "POST",
		url: '../data/get_data.php',
		data: {
			action: 85,
			loans: loans,
			disbursenote: val_disbursenote,
			disbursedby: sessvars.userid,
			disbursementaccount: val_disbursementaccount,
			disbursementoption: val_disbursementoption,
			disbursementdate: val_disbursementdate
		}, 
		datatype: "json",
		success: function(result){			
			var response = eval('('+result+')');  
			if (response.retcode === '000'){  
				$.messager.alert("Warning", response.retmsg);
				$('#fmdisburseloan').form('clear');
				$('#dlgdisburseloan').dialog('close');
				searchforapprovedloans();
			} else {
				$.messager.alert("Warning", response.retmsg);					
			}  
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when disbursing loan', 'warning');
		}
		});	
	}
	
	/* End Loan Disbursement */	
	/* Start View Ledger Transaction */
	function viewledgertransactions(){
		$('<div id="winviewledgertransactions"></div>').appendTo('body');
		
	    $('#winviewledgertransactions').window({ width:1050,height:450,modal:false,title:'View Ledger Transactions'});
		$('#winviewledgertransactions').window('refresh', 'viewledgertransactions.html');
		$('#winviewledgertransactions').window('open');
	}

	function searchviewledgertransactions(){	
		var val_receiptno = $('#ldgview_receiptno').val();
		var val_ledgerid = $('#ldgview_ledgerid').val();
		var val_accountno = $('#ldgview_accountno').val();
		
		var val_fromdate =$('#ldgview_fromdate').datebox('getValue');
		var val_todate =$('#ldgview_todate').datebox('getValue');
		
		if (val_fromdate.length == 0) {
			$.messager.alert("Warning",  'From date is required', 'warning');
			return;
		}
		
		if (val_todate.length == 0) {
			$.messager.alert("Warning",  'To date is required', 'warning');
			return;
		}	
		
		$('#dgviewledgertransactions').datagrid('reload',{ledgerid:val_ledgerid,receiptno:val_receiptno,accountno:val_accountno,fromdate:val_fromdate,todate:val_todate});
	}
	/* End  View Ledger Transaction */	
	/* Start Share Transfer*/
	function internaltransfer(){
		$('<div id="wininternaltransfer"></div>').appendTo('body');
		
	    $('#wininternaltransfer').window({ width:550,height:550,modal:false,title:'Internal Transfer'});
		$('#wininternaltransfer').window('refresh', 'internaltransfer.html');
		$('#wininternaltransfer').window('open');
	}	
	
	function searchtransfercustomer(source){
		if(source == 'from'){
			var customerid = $('#fromcustomerid').val();
			var from_valuedate = $('#transfer_valuedate').datebox('getValue');
			var customer = [];

			$.get( '../data/get_data.php',{action:102,customerid: customerid}, function( data ) {
				var customer = JSON.parse(data);

				if (customer.customername == ''){
					document.getElementById('fromcustomerdetails').innerHTML = 'Customer not available';
				}
				else
				{
					var output = '';
					var products = customer.products;
					
					output = '<table class=\'payslip\'>';
					output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>Customer Name :</strong></td><td class=\'payslip\'>'+customer.customername+'</td></tr>';
					for(var i in products)
					{	
						if(products[i].balance != 0){
							output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>'+products[i].productname+' :</strong></td><td class=\'payslip\'>'+products[i].balance+'</td></tr>';
						}
					}
					output += '</table>';

					document.getElementById('fromcustomerdetails').innerHTML = output;
					
					$('#fromcustomeraccounts').combobox({
						url:'../data/get_data.php?action=105&customerid='+customerid+'&source='+source+'&valuedate='+from_valuedate,
						valueField:'accountid',
						textField:'accountdetails'
					});					
				}
			}); 
			
			if(customer.length == 0){
				document.getElementById('fromcustomerdetails').innerHTML = 'Customer not available';
			}
		}
		
		if(source == 'to'){
			var customerid = $('#tocustomerid').val();
			var to_valuedate = $('#transfer_valuedate').datebox('getValue');
			var customer = [];

			$.get( '../data/get_data.php',{action:102,customerid: customerid}, function( data ) {
				var customer = JSON.parse(data);

				if (customer.customername == ''){
					document.getElementById('tocustomerdetails').innerHTML = 'Customer not available';
				}
				else
				{
					var output = '';
					var products = customer.products;
					
					output = '<table class=\'payslip\'>';
					output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>Customer Name :</strong></td><td class=\'payslip\'>'+customer.customername+'</td></tr>';
					for(var i in products)
					{	
						output += '<tr class=\'payslip\'><td class=\'payslip\' align=\'right\'><strong>'+products[i].productname+' :</strong></td><td class=\'payslip\'>'+products[i].balance+'</td></tr>';
					}
					output += '</table>';

					document.getElementById('tocustomerdetails').innerHTML = output;
					
					$('#tocustomeraccounts').combobox({
						url:'../data/get_data.php?action=105&customerid='+customerid+'&source='+source+'&valuedate='+to_valuedate,
						valueField:'accountid',
						textField:'accountdetails'
					});					
				}
			}); 
			
			if(customer.length == 0){
				document.getElementById('tocustomerdetails').innerHTML = 'Customer not available';
			}
		}		
    }

	//Post Transfer
	function posttransfer(){
		var val_craccountid = $('#tocustomeraccounts').combobox('getValue');
		var val_draccountid = $('#fromcustomeraccounts').combobox('getValue');			
		var val_transfertype = $('#transfertype').combobox('getValue');
		var val_amount = $('#transferamount').val();
		var val_trxdescription = $('#transferdescription').val();			
		var val_post_valuedate = $('#transfer_valuedate').datebox('getValue');
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
						
		if(val_post_valuedate.length == 0){
			//When date is not picked from datebox
			//val_post_valuedate = m+'/'+d+'/'+y;			
			$.messager.alert("Warning",  'Transaction Date not valid. Contact system administrator about this issue', 'warning');
			$('#btnposttransfer').linkbutton('enable');
			return;
		}
		
		if ((val_amount <= 0) || (isNaN(val_amount))){
			$.messager.alert("Warning",  'Invalid Transaction Amount!', 'warning');
			return;
		}

		if (isNaN(val_craccountid)){
			$('#btnposttransfer').linkbutton('enable');
			$.messager.alert("Warning",  'Invalid Account!', 'warning');
			return;
		}
		
		if (isNaN(val_draccountid)){
			$.messager.alert("Warning",  'Invalid Account!', 'warning');
			$('#btnposttransfer').linkbutton('enable');
			return;
		}
		
		if (validatedate(val_post_valuedate)== false ){
			$('#btnposttransfer').linkbutton('enable');
			return;
		}
		
		$.ajax({
		type: "POST",
		cache: false,
		url: '../data/get_data.php',
		datatype: "json",
		data: {
			action: 106,
			craccountid: val_craccountid,
			draccountid: val_draccountid,
			amount: val_amount,
			transfertype: val_transfertype,
			userid: sessvars.userid,
			trxdescription: val_trxdescription,
			valuedate: val_post_valuedate,
			usercontraaccount: sessvars.cashieraccountid
		},
		success: function(result){
			var response = eval('('+result+')');  

			if (response.retcode === '000'){
				$.messager.alert("Warning",  response.retmsg, 'warning');
				
				//clear controls
				$('#tocustomeraccounts').combobox('clear');
				$('#fromcustomeraccounts').combobox('clear');
				
				$('#tocustomeraccounts').combobox('reload','../data/get_data.php?action=105&customerid=0&source=empty');
				$('#fromcustomeraccounts').combobox('reload','../data/get_data.php?action=105&customerid=0&source=empty');
													
				$('#fromcustomerid').numberbox('setValue', 0);
				$('#tocustomerid').numberbox('setValue', 0);
				
				$('#transferamount').numberbox('setValue', 0);
				$('#transferdescription').val('');
				document.getElementById('tocustomerdetails').innerHTML = '';
				document.getElementById('fromcustomerdetails').innerHTML = '';	
				$('#btnposttransfer').linkbutton('enable');
			} else {  
				$.messager.alert("Warning", response.retmsg); 
				$('#btnposttransfer').linkbutton('enable');
			} 				
		},
		error: function(result){
			$.messager.alert("Warning",  'Error Occured when posting transactions' , 'warning');
		}		
		}); 
	}	
	/* End Share Transfer*/	
	/* Start Reports     */
	function rbalancesheet(){
		$('<div id="winrbalancesheet"></div>').appendTo('body');
		
	    $('#winrbalancesheet').window({ width:500,height:450,modal:false,title:'Balance Sheet Report'});
		$('#winrbalancesheet').window('refresh', '../reports/rbalancesheet.html');
		$('#winrbalancesheet').window('open');
	}

	function preview_balancesheet(){
		var valuedate=$('#rbs_valuedate').datebox('getValue');
		var val_balancesheetsummary = 0;
		
		if (document.getElementById('balancesheetsummary').checked) {
           val_balancesheetsummary = 1;
        }
		
		if (valuedate.length == 0) {
			$.messager.alert("Warning", 'Report date is required');
			return;
		}
		document.getElementById('loading').style.display = 'block';
		
		$.get( "../data/get_reports.php",{action:1,valuedate:valuedate,summary:val_balancesheetsummary}, function( data ) {
			document.getElementById("report_balancesheet").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function repostmpesarepayments(){
		var rows = $('#dgmpesarepaymentrepost').datagrid('getChecked');
		if (rows.length <= 0){
			$.messager.alert("Warning",  'No m-Pesa repayment selected', 'warning');
			return;
		}
		
		var str_mpesarepayments = '';		
		for(var i=0;i<rows.length;i++){
			if(i === 0){
				str_mpesarepayments = str_mpesarepayments+"'"+rows[i].mpesa_code+"'";
			}else{
				str_mpesarepayments = str_mpesarepayments+",'"+rows[i].mpesa_code+"'";
			}
		}
				
		$.get( "../data/get_data.php",{action:111,mpesarepayments:str_mpesarepayments}, function( data ) {
			$.messager.alert("Warning", data);
			$('#dgmpesarepaymentrepost').datagrid('reload');
		});		
	}
	
	function getrepostmpesarepayments(){
		var val_fromdate=$('#pr_fromdate').datebox('getValue');
		var val_todate=$('#pr_todate').datebox('getValue');
		var val_paymentprocessing = $('#pr_paymentprocessing').combobox('getValue'); 
		var val_paymentstatus = $('#pr_paymentstatus').combobox('getValue');	

		$('#dgmpesarepaymentrepost').datagrid('reload',{fromdate:val_fromdate,todate:val_todate,paymentprocessing: val_paymentprocessing,paymentstatus: val_paymentstatus});
	}
	
	function rmpesarepayments(){
		$('<div id="winrmpesarepayments"></div>').appendTo('body');
		
	    $('#winrmpesarepayments').window({ width:1500,height:500,modal:false,title:'M-Pesa Repayments Report'});
		$('#winrmpesarepayments').window('refresh', '../reports/rmpesarepayments.html');
		$('#winrmpesarepayments').window('open');
	}
	
	function preview_mpesarepayments(){
		var val_fromdate=$('#rmr_fromdate').datebox('getValue');
		var val_todate=$('#rmr_todate').datebox('getValue');
		var val_paymentprocessing = $('#rmr_paymentprocessing').combobox('getValue'); 
		var val_paymentstatus = $('#rmr_paymentstatus').combobox('getValue');	

		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:13,fromdate:val_fromdate,todate:val_todate,paymentprocessing: val_paymentprocessing,paymentstatus: val_paymentstatus}, function( data ) {
			document.getElementById("report_mpesarepayments").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});		
	}
	
	function raccountstatement(){
		$('<div id="winraccountstatement"></div>').appendTo('body');
		
	    $('#winraccountstatement').window({ width:1000,height:500,modal:false,title:'Account Statement'});
		$('#winraccountstatement').window('refresh', '../reports/raccountstatement.html');
		$('#winraccountstatement').window('open');
	}
	
	function preview_accountstatement(){
		var val_customerid = $('#ras_customerid').val();
		var val_idno = $('#ras_idno').val();
		var val_accountno = $('#ras_accountno').val();
		var val_ignorereversal = 0;
		
		if (document.getElementById('ignorereversal').checked) {
           val_ignorereversal = 1;
        } 
		
		var val_fromdate =$('#ras_fromdate').datebox('getValue');
		var val_todate =$('#ras_todate').datebox('getValue');
		
		if (val_fromdate.length == 0) {
			$.messager.alert("Warning",  'From date is required', 'warning');
			return;
		}
		
		if (val_todate.length == 0) {
			$.messager.alert("Warning",  'To date is required', 'warning');
			return;
		}	
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:2,customerid:val_customerid,idno:val_idno,accountno:val_accountno,fromdate:val_fromdate,todate:val_todate,ignorereversal: val_ignorereversal}, function( data ) {
			document.getElementById("report_accountstatement").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function rledgeraccountstatement(){
		$('<div id="winrledgeraccountstatement"></div>').appendTo('body');
		
	    $('#winrledgeraccountstatement').window({ width:800,height:500,modal:false,title:'Ledger Account Statement'});
		$('#winrledgeraccountstatement').window('refresh', '../reports/rledgeraccountstatement.html');
		$('#winrledgeraccountstatement').window('open');
	}
	
	function preview_ledgeraccountstatement(){
		var val_accountno =  $('#ldgstm_accountid').combobox('getValue');		
		var val_fromdate =$('#ldgstm_fromdate').datebox('getValue');
		var val_todate =$('#ldgstm_todate').datebox('getValue');
		
		if (val_fromdate.length == 0) {
			$.messager.alert("Warning",  'From date is required', 'warning');
			return;
		}
		
		if (val_todate.length == 0) {
			$.messager.alert("Warning",  'To date is required', 'warning');
			return;
		}	
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:3,accountno:val_accountno,fromdate:val_fromdate,todate:val_todate}, function( data ) {
			document.getElementById("report_ledgeraccountstatement").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}	

	function rincomestatement(){
		$('<div id="winrincomestatement"></div>').appendTo('body');
		
	    $('#winrincomestatement').window({ width:500,height:450,modal:false,title:'Income Statement Report'});
		$('#winrincomestatement').window('refresh', '../reports/rincomestatement.html');
		$('#winrincomestatement').window('open');
	}
	
	function preview_incomestatement(){
		var valuedate=$('#valuedate').datebox('getValue');
		var val_incomestatementsummary = 0;
		
		if (document.getElementById('incomestatementsummary').checked) {
           val_incomestatementsummary = 1;
        }		
		if (valuedate.length == 0) {
			$.messager.alert("Warning",  'Report date is required', 'warning');
			return;
		}
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:4,valuedate:valuedate,summary:val_incomestatementsummary}, function( data ) {
			document.getElementById("report_incomestatement").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function rtrialbalance(){
		$('<div id="winrtrialbalance"></div>').appendTo('body');
		
	    $('#winrtrialbalance').window({ width:500,height:450,modal:false,title:'Trial Balance Report'});
		$('#winrtrialbalance').window('refresh', '../reports/rtrialbalance.html');
		$('#winrtrialbalance').window('open');
	}
	
	function preview_trialbalance(){
		var valuedate=$('#rtrial_valuedate').datebox('getValue');
		
		if (valuedate.length == 0) {
			$.messager.alert("Warning", 'Report date is required');
			return;
		}
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:5,valuedate:valuedate}, function( data ) {
			document.getElementById("report_trialbalance").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function rcustomerregister(){
		$('<div id="winrcustomerregister"></div>').appendTo('body');
		
	    $('#winrcustomerregister').window({ width:1100,height:450,modal:false,title:'Customer Register Report'});
		$('#winrcustomerregister').window('refresh', '../reports/rcustomerregister.html');
		$('#winrcustomerregister').window('open');
	}
	
	function preview_customerregister(){
		var joindate=$('#joindate').datebox('getValue');
		
		if (joindate.length == 0) {
			$.messager.alert("Warning",  'Join date is required', 'warning');
			return;
		}
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:6,joindate:joindate}, function( data ) {
			document.getElementById("report_customerregister").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function raccountbalance(){
		$('<div id="winraccountbalance"></div>').appendTo('body');
			
	    $('#winraccountbalance').window({ width:650,height:450,modal:false,title:'Account Balance Report'});
		$('#winraccountbalance').window('refresh', '../reports/raccountbalance.html');
		$('#winraccountbalance').window('open');			
	}

	function preview_accountbalance(){
		var val_valuedate=$('#rab_valuedate').datebox('getValue');
		var val_producttypeid =  $('#rab_producttypeid').combobox('getValue');
		
		if (val_valuedate.length == 0) {
			$.messager.alert("Warning",  'Report date is required' , 'warning');
			return;
		}

		document.getElementById('loading').style.display = 'block';
		
		$.get( "../data/get_reports.php",{action:7,valuedate:val_valuedate,producttype:val_producttypeid}, function( data ) {
			document.getElementById("report_accountbalance").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});		
	}

	function rloanportfolio(){
		$('<div id="winrloanportfolio"></div>').appendTo('body');
		
	    $('#winrloanportfolio').window({ width:1000,height:500,modal:false,title:'Loan Portfolio'});
		$('#winrloanportfolio').window('refresh', '../reports/rloanportfolio.html');
		$('#winrloanportfolio').window('open');			
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		
		$('#rlp_valuedate').datebox('setValue', m+'/'+d+'/'+y);	
	}
	
	function preview_loanportfolio(){
		var val_valuedate=$('#rlp_valuedate').datebox('getValue');
		var val_loanofficerid = $('#rlp_loanofficerid').combobox('getValue');
		var val_branchid  = $('#rlp_branchid').combobox('getValue');
		var val_arrearsonly = 0;
		var val_rlpsummary = 0;
		
		if (document.getElementById('rlpsummary').checked) {
           val_rlpsummary = 1;
        } 
		
		if (document.getElementById('arrearsonly').checked) {
           val_arrearsonly = 1;
        }		
		
		if (val_valuedate.length == 0) {
			$.messager.alert("Warning",  'Report date is required', 'warning');
			return;
		}
		
		if (val_loanofficerid.length == 0) {
			$.messager.alert("Warning",  'Loan officer is required', 'warning');
			return;
		}
		
		if (val_branchid.length == 0) {
			$.messager.alert("Warning",  'Branch is required', 'warning');
			return;
		}
		
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:8,valuedate:val_valuedate,loanofficerid:val_loanofficerid,branchid:val_branchid,arrearsonly:val_arrearsonly,rlpsummary:val_rlpsummary}, function( data ) {
			document.getElementById("report_loanportfolio").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}
	
	function rparanalysis(){
		$('<div id="winrparanalysis"></div>').appendTo('body');
		
	    $('#winrparanalysis').window({ width:800,height:500,modal:false,title:'PAR Analysis Report'});
		$('#winrparanalysis').window('refresh', '../reports/rparanalysis.html');
		$('#winrparanalysis').window('open');			
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		
		$('#rlpar_valuedate').datebox('setValue', m+'/'+d+'/'+y);	
	}

	function preview_rparanalysis(){
		var val_valuedate=$('#rlpar_valuedate').datebox('getValue');
		var val_loanofficerid = $('#rlpar_loanofficerid').combobox('getValue');
		var val_branchid  = $('#rlpar_branchid').combobox('getValue');  
		var val_groupby = $('#rlpar_groupby').combobox('getValue');
		
		if (val_valuedate.length == 0) {
			$.messager.alert("Warning",  'Report date is required', 'warning');
			return;
		}
		
		if (val_loanofficerid.length == 0) {
			$.messager.alert("Warning",  'Loan officer is required', 'warning');
			return;
		}
		
		if (val_branchid.length == 0) {
			$.messager.alert("Warning",  'Branch is required', 'warning');
			return;
		}
		
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:9,valuedate:val_valuedate,loanofficerid:val_loanofficerid,branchid:val_branchid,groupby:val_groupby}, function( data ) {
			document.getElementById("report_rparanalysis").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}
	
	function rexpectedrepayments(){
		$('<div id="winrexpectedrepayments"></div>').appendTo('body');
		
	    $('#winrexpectedrepayments').window({ width:1100,height:500,modal:false,title:'Expected Repayments Report'});
		$('#winrexpectedrepayments').window('refresh', '../reports/rexpectedrepayments.html');
		$('#winrexpectedrepayments').window('open');			
		
		var date = new Date();
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		
		$('#rer_valuedate').datebox('setValue', m+'/'+d+'/'+y);	
	}

	function preview_rexpectedrepayments(){
		var val_valuedate=$('#rer_valuedate').datebox('getValue');
		var val_loanofficerid = $('#rer_loanofficerid').combobox('getValue');
		var val_branchid  = $('#rer_branchid').combobox('getValue'); 
		var val_exprepsummary = 0;
		
		if (document.getElementById('exprepsummary').checked) {
           val_exprepsummary = 1;
        } 
		
		if (val_valuedate.length == 0) {
			$.messager.alert("Warning",  'Report date is required', 'warning');
			return;
		}
		
		if (val_loanofficerid.length == 0) {
			$.messager.alert("Warning",  'Loan officer is required', 'warning');
			return;
		}
		
		if (val_branchid.length == 0) {
			$.messager.alert("Warning",  'Branch is required', 'warning');
			return;
		}		
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:10,valuedate:val_valuedate,loanofficerid:val_loanofficerid,branchid:val_branchid,exprepsummary:val_exprepsummary}, function( data ) {
			document.getElementById("report_rexpectedrepayments").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}
	
	function rloanrepaymentschedule(){
		$('<div id="winrloanrepaymentschedule"></div>').appendTo('body');
		
	    $('#winrloanrepaymentschedule').window({ width:600,height:500,modal:false,title:'Loan Repayment Schedule'});
		$('#winrloanrepaymentschedule').window('refresh', '../reports/rloanrepaymentschedule.html');
		$('#winrloanrepaymentschedule').window('open');			
	}

	function preview_rloanrepaymentschedule(){
		var val_rps_loanid  = $('#rps_loanid').combobox('getValue');
		
		if (val_rps_loanid.length == 0) {
			$.messager.alert("Warning",  'Select a loan!', 'warning');
			return;
		}
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:11,loanid:val_rps_loanid}, function( data ) {
			document.getElementById("report_rloanrepaymentschedule").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}
	
	function rloandisbursement(){
		$('<div id="winrloandisbursement"></div>').appendTo('body');
		
	    $('#winrloandisbursement').window({ width:1100,height:500,modal:false,title:'Loan Disbursement Report'});
		$('#winrloandisbursement').window('refresh', '../reports/rloandisbursement.html');
		$('#winrloandisbursement').window('open');			
	}

	function preview_rloandisbursement(){
		var val_fromdate=$('#rld_fromdate').datebox('getValue');
		var val_todate=$('#rld_todate').datebox('getValue');
		var val_loanofficerid = $('#rld_loanofficerid').combobox('getValue');
		var val_branchid  = $('#rld_branchid').combobox('getValue');
		var val_status  = $('#rld_status').combobox('getValue'); 		
		
		if (val_fromdate.length == 0) {
			$.messager.alert("Warning",  'From date is required', 'warning');
			return;
		}
		
		if (val_todate.length == 0) {
			$.messager.alert("Warning",  'To date is required', 'warning');
			return;
		}
		
		if (val_loanofficerid.length == 0) {
			$.messager.alert("Warning",  'Loan officer is required', 'warning');
			return;
		}
		
		if (val_branchid.length == 0) {
			$.messager.alert("Warning",  'Branch is required', 'warning');
			return;
		}
		
		if (val_status.length == 0) {
			$.messager.alert("Warning",  'Loan Status is required', 'warning');
			return;
		}		
		
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:12,fromdate:val_fromdate,todate:val_todate,loanofficerid:val_loanofficerid,branchid:val_branchid,loanstatus:val_status}, function( data ) {
			document.getElementById("report_rloandisbursement").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}
	
	function printSelection(node){
		var content = node.innerHTML;
		var pwin = window.open("","print_content","width=100,height=100");

		pwin.document.open();
		pwin.document.write('<html><body onload="window.print()">'+content+'</body></html>');
		pwin.document.close();

		setTimeout(function(){pwin.close();},1000);
	}

	function exporttoexcel(node,reportname){
		var content=node.innerHTML;
		if(content.length > 0){
			/*
			//Special Characters killing a.click()
			var a = document.createElement('a');
			var data_type = 'data:application/vnd.ms-excel';
			var table_html = encodeURIComponent(node.outerHTML);// node.outerHTML.replace(/ /g, '%20');
			a.href = data_type + ', ' + table_html;
			a.target      = '_blank';
			a.download = reportname + '.xls';	
			document.body.appendChild(a);
			a.click();
			*/
			window.open('data:application/vnd.ms-excel;base64,' + base64_encode(node.outerHTML));			
		}	
	}
	
	function base64_encode(data) {
	  //  discuss at: http://phpjs.org/functions/base64_encode/
	  // original by: Tyler Akins (http://rumkin.com)
	  // improved by: Bayron Guevara
	  // improved by: Thunder.m
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Rafał Kukawski (http://kukawski.pl)
	  // bugfixed by: Pellentesque Malesuada
	  //   example 1: base64_encode('Kevin van Zonneveld');
	  //   returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
	  //   example 2: base64_encode('a');
	  //   returns 2: 'YQ=='

	  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
	  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
		ac = 0,
		enc = '',
		tmp_arr = [];

	  if (!data) {
		return data;
	  }

	  do { // pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);

		bits = o1 << 16 | o2 << 8 | o3;

		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	  } while (i < data.length);

	  enc = tmp_arr.join('');

	  var r = data.length % 3;

	  return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
	}
	
	function passwordreset(){ 		
		$('#fmPasswordReset').form('submit',{  
			url:'../data/get_data.php?action=108&userid='+sessvars.userid,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", result.retmsg); 
					$('#fmPasswordReset').form('clear');
					$('#btnresetpassword').linkbutton('enable');
					
					$("#oldpassword").val('');
					$("#newpassword").val('');
					$("#confirmpassword").val('');

					var session = result.results;
					var mainurl = session.mainurl;
					
					window.location.href = mainurl;
				} else {  
					$.messager.alert("Warning", result.retmsg); 
					$('#btnresetpassword').linkbutton('enable');
				}  
			}  
		});  	
	}

	function getloanaccounts_nationalid(){
		var val_rps_idno  = $('#rps_idno').val();
		
		if (val_rps_idno.length == 0) {
			//$.messager.alert("Warning", 'Specify correct national id!');
			return;
		}else{ //$customerid,$idno,$iscustomerid	
			$('#rps_loanid').combobox({
				url:'../data/get_data.php?action=109&customerid=0&idno='+val_rps_idno+'&iscustomerid=0',
				valueField:'loanid',
				textField:'loaniddesc'
			});
		}		
	}
	
	function getloanaccounts_customerid(){
		var val_rps_customerid  = $('#rps_customerid').val();
		
		if ((val_rps_customerid.length == 0) || (val_rps_customerid == 0)){
			//$.messager.alert("Warning", 'Specify correct customer id!');
			return;
		}else{
			$('#rps_loanid').combobox({
				url:'../data/get_data.php?action=109&customerid='+val_rps_customerid+'&idno=0&iscustomerid=1&loanstatus=0',
				valueField:'loanid',
				textField:'loaniddesc'
			});
		}			
	}
	
	function pwdreset(){
		$('<div id="winpwdreset"></div>').appendTo('body');
		
	    $('#winpwdreset').window({ width:500,height:300,modal:false,title:'Reset Password'});
		$('#winpwdreset').window('refresh', 'pwdreset.html');
		$('#winpwdreset').window('open');
	}

	function sendsms(){
		var rows = $('#dgmprocesssms').datagrid('getChecked');
		if (rows.length <= 0){
			$.messager.alert("Warning", 'No SMS Selected');
			return;
		}
		
		var str_smstosend = '';		
		for(var i=0;i<rows.length;i++){
			if(i === 0){
				str_smstosend = str_smstosend+rows[i].queid;
			}else{
				str_smstosend = str_smstosend+","+rows[i].queid;
			}
		}		

		//Update SMS are being processed
		$.get( '../data/get_data.php',{action:143,smsstatus: 1 ,smss: str_smstosend, issent:0}, function( data ) {
			//document.getElementById("report_balancesheet").innerHTML = data;
		});	
				

		/*
		for(var i=0;i<rows.length;i++){
			//$urlsms = 'https://www.mspace.co.ke:9081/sms/websms.jsp?uname=cemesltd&passwd=cemesltd1&dest='+rows[i].phoneno+'&source=cemesltd&msg='+rows[i].message;	
			//$.get($urlsms , function( data ) {
				if(i == 0){
					str_smstosend = str_smstosend+rows[i].queid;
				}else{
					str_smstosend = str_smstosend+","+rows[i].queid;
				}
			//});			 			
		}*/

		str_smstosend = '';
		var str_failedsms = '';	
		
		var x = 0; //Used to track failed SMS
		var y = 0; //Used to track sent SMS
		
		for(var i=0;i<rows.length;i++){
			//CEMES
			//$urlsms = 'https://www.mspace.co.ke:9081/sms/websms.jsp?uname=cemesltd&passwd=cemesltd1&dest='+rows[i].phoneno+'&source=cemesltd&msg='+rows[i].message;	
			//Mspace
			//$urlsms = 'https://www.mspace.co.ke:9081/sms/websms.jsp?uname=andrew&passwd=andr3w1&dest=254721557528&source=MSpace&msg=Testing%20+Web+Service+SMS';	
			$urlsms = 'https://www.mspace.co.ke:9081/sms/websms.jsp?uname=andrew&passwd=andr3w1&dest='+rows[i].phoneno+'&source=MSpace&msg='+rows[i].message;	
			//Sun Group Micro Africa
			//$urlsms = 'https://www.mspace.co.ke:9081/sms/websms.jsp?uname=tested&passwd=tested&dest='+rows[i].phoneno+'&source=sma&msg='+rows[i].message;

			$.ajax({
			type: "GET",
			url: $urlsms,		
			success: function(result){
				if(y == 0){
					str_smstosend = str_smstosend+rows[i].queid;
				}else{
					str_smstosend = str_smstosend+","+rows[i].queid;
				}
				y=y+1;
			},
			error: function(result){
				//Don't update status
				if(x == 0){
					str_failedsms = str_failedsms+rows[i].queid;
				}else{
					str_failedsms = str_failedsms+","+rows[i].queid;
				}	
				x=x+1;
			}		
			}); 		
		}
		
		//Update SMS are being processed
		if(str_smstosend.length > 0){
			$.get( '../data/get_data.php',{action:143,smsstatus: 2 ,smss: str_smstosend, issent:1}, function( data ) {
				//document.getElementById("report_balancesheet").innerHTML = data;
			});
		}	
		
		if(str_failedsms.length > 0){
			$.messager.alert("Warning", 'SMS Processed Successfully but the following SMS(s) failed : '+str_failedsms);
		}else{
			$.messager.alert("Warning", 'SMS Processed Successfully');
		}
		getSMS();
	}

	function cancelsms(){
		var rows = $('#dgmprocesssms').datagrid('getChecked');
		if (rows.length <= 0){
			$.messager.alert("Warning", 'No SMS Selected');
			return;
		}
		
		var str_smstocancel = '';		
		for(var i=0;i<rows.length;i++){
			if(i === 0){
				str_smstocancel = str_smstocancel+rows[i].queid;
			}else{
				str_smstocancel = str_smstocancel+","+rows[i].queid;
			}
		}		

		//Update SMS are being processed
		if(str_smstocancel.length > 0){
			$.get( '../data/get_data.php',{action:143,smsstatus: 3 ,smss: str_smstocancel, issent:1}, function( data ) {
				//document.getElementById("report_balancesheet").innerHTML = data;
			});
		}			
		$.messager.alert("Warning", 'SMS Processed Successfully');
		getSMS();
	}
	
	function getSMS(){
		var val_fromdate=$('#sms_fromdate').datebox('getValue');
		var val_todate=$('#sms_todate').datebox('getValue');

		$('#dgmprocesssms').datagrid('reload',{fromdate:val_fromdate,todate:val_todate});
	}
	
	/* End Reports     */
	$(document).ready(function() {
		LoadMenus();		
	});
	
	/* Start SMS functions */
	/* Start Campaigns */
	function smscampaigns(){
		$('<div id="winsmscampaigns"></div>').appendTo('body');
		
	    $('#winsmscampaigns').window({ width:700,height:500,modal:false,title:'Manage Campaigns'});
		$('#winsmscampaigns').window('refresh', 'smscampaigns.html');
		$('#winsmscampaigns').window('open');
	}
	
	function newCampaign(){
		$('#dlgcampaigns').dialog('open').dialog('setTitle','Add Campaign');
		$('#fmcampaigns').form('clear');
		$('#ruleid').combobox('setValue','1');//selected value
		$('#scheduleid').combobox('setValue','2');//selected value
		$('#templateid').combobox('setValue','1');//selected value
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=117';
	}

	function editCampaign(){
		var row = $('#dgcampaigns').datagrid('getSelected');
		if (row){
			$('#dlgcampaigns').dialog('open').dialog('setTitle','Edit Campaign Details');
			$('#fmcampaigns').form('load',row);
			url = '../data/get_data.php?action=118&campaignid='+row.campaignid;
		}
	}
		
	function saveCampaign(){  
		$('#fmcampaigns').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Campaign details saved successfully');
					$('#dlgcampaigns').dialog('close');   // close the dialog  
					$('#dgcampaigns').datagrid('reload'); // reload the datagrid  
				} else { 
					$.messager.alert("Warning", result.retmsg);
				}  
			}  
		});  
	}	

	function previewMessageCampaigns(){
		var row = $('#dgcampaigns').datagrid('getSelected');
		if (row){
			if (row.active==="1"){
				$('#previewcampaigns').dialog('open').dialog('setTitle','Preview Campaign Messages');
				$('#dgpreview').datagrid('reload',{campaignid:row.campaignid});
			} else{
				$.messager.show({  
					title: 'Error',  
					msg: 'Campaign is not active' 
				});
			}
		}
	}		
	/* End Campaigns */
	/* Start Direct Message */
	function smsdirectmessage(){
		$('<div id="winsmsdirectmessage"></div>').appendTo('body');
		
	    $('#winsmsdirectmessage').window({ width:700,height:500,modal:false,title:'Direct Message'});
		$('#winsmsdirectmessage').window('refresh', 'smsdirectmessage.html');
		$('#winsmsdirectmessage').window('open');
	}
	
	function newMessage(){
		$('#dlgdirectmessage').dialog('open').dialog('setTitle','Add Message');
		$('#fmdirectmessage').form('clear');
		$('#templateid').combobox('setValue','1');//selected value
		$('#ruleid').combobox('setValue','1');//selected value
		$('#active').combobox('setValue','1');//selected value 
		url = '../data/get_data.php?action=124';
	}

	function editMessage(){
		var row = $('#dgdirectmessage').datagrid('getSelected');
		if (row){
			$('#dlgdirectmessage').dialog('open').dialog('setTitle','Edit Message Details');
			$('#fmdirectmessage').form('load',row);
			url = '../data/get_data.php?action=125&messageid='+row.messageid;
		}
	}
		
	function saveMessage(){  
		$('#fmdirectmessage').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){ 
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Message details saved successfully');
					$('#dlgdirectmessage').dialog('close');   // close the dialog  
					$('#dgdirectmessage').datagrid('reload'); // reload the datagrid  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}

	function sendMessage(){
		var row = $('#dgdirectmessage').datagrid('getSelected');
		if (row){		
			if (row.active==="1"){
				$.ajax({
				type: "POST",
				cache: false,
				url: '../data/get_data.php',
				datatype: "json",
				data: {action: 126,messageid: row.messageid},
				success: function(result){
					var result = eval('('+result+')');  
					if (result.retcode=='000'){  
						$.messager.alert("Warning", 'SMS sent successfully'); 
					} else {  
						$.messager.show({  
							title: 'Error',  
							msg: result.retmsg  
						});  
					}  				
				},
				error: function(result){
				}		
				});
			} else{
				$.messager.show({  
					title: 'Error',  
					msg: 'Campaign is not active' 
				});
			}
		}
	}
	
	function previewMessageDirectMessage(){
		var row = $('#dgdirectmessage').datagrid('getSelected');
		if (row){
			if (row.active==="1"){
				$('#previewdirectmessage').dialog('open').dialog('setTitle','Preview Direct Message');
				$('#dgpreviewdirectmessage').datagrid('reload',{messageid:row.messageid});
			} else{
				$.messager.show({  
					title: 'Error',  
					msg: 'Campaign is not active' 
				});
			}
		}
	}		
	/* End Direct Message */
	/* Start SMS Templates */	
	function smstemplates(){
		$('<div id="winsmstemplates"></div>').appendTo('body');
		
	    $('#winsmstemplates').window({ width:800,height:500,modal:false,title:'SMS Templates'});
		$('#winsmstemplates').window('refresh', 'smstemplates.html');
		$('#winsmstemplates').window('open');
	}
	
	function newTemplate(){
		$('#dlgtemplates').dialog('open').dialog('setTitle','Add Template');
		$('#fmtemplates').form('clear');
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=128';
	}

	function editTemplate(){
		var row = $('#dgtemplates').datagrid('getSelected');
		if (row){
			$('#dlgtemplates').dialog('open').dialog('setTitle','Edit Tag Details');
			$('#fmtemplates').form('load',row);
			url = '../data/get_data.php?action=129&templateid='+row.templateid;
		}
	}
		
	function saveTemplate(){  
		$('#fmtemplates').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){ 
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Template details saved successfully');
					$('#dlgtemplates').dialog('close');   // close the dialog  
					$('#dgtemplates').datagrid('reload'); // reload the datagrid  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}		
	/* End SMS Templates */
	/* Start Rules */
	function smsrules(){
		$('<div id="winsmsrules"></div>').appendTo('body');
		
	    $('#winsmsrules').window({ width:750,height:400,modal:false,title:'Business Rules'});
		$('#winsmsrules').window('refresh', 'smsrules.html');
		$('#winsmsrules').window('open');
	}
	
	function newRule(){
		$('#dlgrules').dialog('open').dialog('setTitle','Add Rule');
		$('#fmrules').form('clear');
		$('#ruletype').combobox('setValue','1');//selected value
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=131';
	}

	function editRule(){
		var row = $('#dgrules').datagrid('getSelected');
		if (row){
			if (row.ruletype > 0){
				$('#dlgrules').dialog('open').dialog('setTitle','Edit Rule Details');
				$('#fmrules').form('load',row);
				url = '../data/get_data.php?action=132&ruleid='+row.ruleid;
			} else {
				$.messager.alert("Warning", 'A Rule linked to a remote database cannot be edited. Contact Admin');
			}
		}
	}
		
	function saveRule(){  
		$('#fmrules').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){	
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Rule Details saved successfully');
					$('#dlgrules').dialog('close');   // close the dialog  
					$('#dgrules').datagrid('reload'); // reload the datagrid  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	/* End Rules */
	/*  Start Contacts */
	function smscontacts(){
		$('<div id="winsmscontacts"></div>').appendTo('body');
		
	    $('#winsmscontacts').window({ width:800,height:450,modal:false,title:'Manage Contacts'});
		$('#winsmscontacts').window('refresh', 'smscontacts.html');
		$('#winsmscontacts').window('open');
	}
	
	function newContact(){
		$('#dlgcontacts').dialog('open').dialog('setTitle','Add Contact');
		$('#fmcontacts').form('clear');
		$('#contacttypeid').combobox('setValue','1');//selected value
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=139';
	}

	function editContact(){
		var row = $('#dgcontacts').datagrid('getSelected');
		if (row){
			$('#dlgcontacts').dialog('open').dialog('setTitle','Edit Contact Details');
			$('#fmcontacts').form('load',row);
			url = '../data/get_data.php?action=140&contactid='+row.contactid;
		}
	}
		
	function saveContact(){  
		$('#fmcontacts').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){ 
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Contact details saved successfully');
					$('#dlgcontacts').dialog('close');   // close the dialog  
					$('#dgcontacts').datagrid('reload'); // reload the datagrid  
				} else {  
					$.Contactr.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}
	
	function getData(){
		var rows = [];
		for(var i=1; i<=800; i++){
			var amount = Math.floor(Math.random()*1000);
			var price = Math.floor(Math.random()*1000);
			rows.push({
				inv: 'Inv No '+i,
				date: $.fn.datebox.defaults.formatter(new Date()),
				name: 'Name '+i,
				amount: amount,
				price: price,
				cost: amount*price,
				note: 'Note '+i
			});
		}
		return rows;
	}
	
	function pagerFilter(data){
		if (typeof data.length == 'number' && typeof data.splice == 'function'){    // is array
			data = {
				total: data.length,
				rows: data
			}
		}
		var dg = $(this);
		var opts = dg.datagrid('options');
		var pager = dg.datagrid('getPager');
		pager.pagination({
			onSelectPage:function(pageNum, pageSize){
				opts.pageNumber = pageNum;
				opts.pageSize = pageSize;
				pager.pagination('refresh',{
					pageNumber:pageNum,
					pageSize:pageSize
				});
				dg.datagrid('loadData',data);
			}
		});
		if (!data.originalRows){
			data.originalRows = (data.rows);
		}
		var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
		var end = start + parseInt(opts.pageSize);
		data.rows = (data.originalRows.slice(start, end));
		return data;
	}
	
	$(function(){
		$('#dgcontacts').datagrid({loadFilter:pagerFilter}).datagrid('loadData', getData());
	});		
	/* End Contracts */
	/*  Start Tags */
	function smstags(){
		$('<div id="winsmstags"></div>').appendTo('body');
		
	    $('#winsmstags').window({ width:600,height:450,modal:false,title:'Tags'});
		$('#winsmstags').window('refresh', 'smstags.html');
		$('#winsmstags').window('open');
	}
	
	function newTag(){
		$('#dlgtags').dialog('open').dialog('setTitle','Add Tag');
		$('#fmtags').form('clear');
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=134';
	}

	function editTag(){
		var row = $('#dgtags').datagrid('getSelected');
		if (row){
			$('#dlgtags').dialog('open').dialog('setTitle','Edit Tag Details');
			$('#fmtags').form('load',row);
			url = '../get_data/data.php?action=135&tagid='+row.tagid;
		}
	}
		
	function saveTag(){  
		$('#fmtags').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){ 
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Tag Details saved successfully');
					$('#dlgtags').dialog('close');   // close the dialog  
					$('#dgtags').datagrid('reload'); // reload the datagrid  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}		
	/* End Tags */
	/* Start Schedule */
	function smsschedules(){
		$('<div id="winsmsschedules"></div>').appendTo('body');
		
	    $('#winsmsschedules').window({ width:700,height:450,modal:false,title:'Schedules'});
		$('#winsmsschedules').window('refresh', 'smsschedules.html');
		$('#winsmsschedules').window('open');
	}

	function newSchedule(){
		$('#dlgschedules').dialog('open').dialog('setTitle','Add Schedule');
		$('#fmschedules').form('clear');
		$('#active').combobox('setValue','1');//selected value
		url = '../data/get_data.php?action=137';
	}

	function editSchedule(){
		var row = $('#dgschedules').datagrid('getSelected');
		if (row){
			$('#dlgschedules').dialog('open').dialog('setTitle','Edit Schedule Details');
			$('#fmschedules').form('load',row);
			url = '../data/get_data.php?action=138&scheduleid='+row.scheduleid;
		}
	}
		
	function saveSchedule(){  
		$('#fmschedules').form('submit',{  
			url: url,  
			onSubmit: function(){  
				return $(this).form('validate');  
			},  
			success: function(result){
				var result = eval('('+result+')');  
				if (result.retcode=='000'){  
					$.messager.alert("Warning", 'Schedule details saved successfully');
					$('#dlgschedules').dialog('close');   // close the dialog  
					$('#dgschedules').datagrid('reload'); // reload the datagrid  
				} else {  
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}		
	/* End Schedule */
	/* Start SMS Report */
	function smsreport(){
		$('<div id="winsmsreport"></div>').appendTo('body');
		
	    $('#winsmsreport').window({ width:1200,height:500,modal:false,title:'SMS Report'});
		$('#winsmsreport').window('refresh', 'smsreport.html');
		$('#winsmsreport').window('open');
	}	
	
	function getReport(){
		var valstatus = $('#rsms_status').combobox('getValue');
		var valfromdate =$("#rsms_fromdate").datebox('getValue');
		var valtodate =$("#rsms_todate").datebox('getValue');
		var valphoneno =$("#rsms_phoneno").val();
		
		$.post("../data/get_data.php?action=142",{fromdate:valfromdate,todate:valtodate,status:valstatus,phoneno:valphoneno},function(result){
			document.getElementById("report_smsreport").innerHTML =result;
		},'html');
	}	
	/* End SMS Report */
	function processsms(){
		$('<div id="winprocesssms"></div>').appendTo('body');
		
	    $('#winprocesssms').window({ width:700,height:450,modal:false,title:'Process SMS'});
		$('#winprocesssms').window('refresh', 'processsms.html');
		$('#winprocesssms').window('open');
	}		
	/* End SMS functions */
	
	function editmpesarepayment(){
		var row = $('#dgmpesarepaymentrepost').datagrid('getChecked');
		
		if (row.length <= 0){
			$.messager.alert("Warning", 'No loan selected');
			return;
		}
		
		if (row.length > 1){
			$.messager.alert("Warning", 'Selected only one repayment to edit');
			return;
		}
		
		row = $('#dgmpesarepaymentrepost').datagrid('getSelected');
		
		if (row){
			$('#dlgmpesarepayment').dialog('open').dialog('setTitle','Edit M-Pesa Repayment');
			$('#fmmpesarepayment').form('load',row);
			url = '../data/get_data.php?action=144&mpesacode='+row.mpesa_code;
		}
	}
	
	function savempesarepayment(){ 	
		$('#fmmpesarepayment').form('submit',{  
			url: url,  
			onSubmit: function(){  				
				if(!$(this).form('validate'))
				{
					$.messager.alert("Warning", "Not all mandatory fields are filled. Please check in all tabs");
				}
				$('#btnsaverepayments').linkbutton('enable');
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", "Loan Repayment Edited Successfully");
					$('#dlgmpesarepayment').dialog('close');      // close the dialog  
					$('#dgmpesarepaymentrepost').datagrid('reload');    // reload the user data  
					$('#btnsaverepayments').linkbutton('enable');					
				} else {  
					$('#btnsaverepayments').linkbutton('enable');
					
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}

	function changeloanofficer(){
		$('<div id="winchangeloanofficer"></div>').appendTo('body');
		
	    $('#winchangeloanofficer').window({ width:550,height:180,modal:false,title:'Change Loan Officer'});
		$('#winchangeloanofficer').window('refresh', 'changeloanofficer.html');
		$('#winchangeloanofficer').window('open');
	}
	
	function change_loanofficer(){ 	
		$('#fmchangeloanofficer').form('submit',{  
			url: '../data/get_data.php?action=149',  
			onSubmit: function(){  				
				if(!$(this).form('validate'))
				{
					$.messager.alert("Warning", "Specify values for all mandatory fields");
				}
				$('#btnchangeloanofficer').linkbutton('enable');
				return $(this).form('validate');  
			},  
			success: function(result){  
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", "Loan officer changed Successfully");
					$('#btnchangeloanofficer').linkbutton('enable');					
				} else {  
					$('#btnchangeloanofficer').linkbutton('enable');
					
					$.messager.show({  
						title: 'Error',  
						msg: result.retmsg  
					});  
				}  
			}  
		});  
	}	
	
	
	/* Start USSD*/ 
	function ussdsubscription(){	
	    $('<div id="winussdsubscription"></div>').appendTo('body');
		
	    $('#winussdsubscription').window({width:1350,height:450,modal:false,title:'USSD Subscription'});
		$('#winussdsubscription').window('refresh', 'ussdsubscription.html');		
		$('#winussdsubscription').window('open');
	}
		
	function ussdsubscriptionreport(){
		$('<div id="winussdsubscriptionreport"></div>').appendTo('body');
		
	    $('#winussdsubscriptionreport').window({ width:1100,height:450,modal:false,title:'USSD Subscription Report'});
		$('#winussdsubscriptionreport').window('refresh', '../reports/ussdsubscriptionreport.html');
		$('#winussdsubscriptionreport').window('open');
	}	
	
	function searchUSSDCustomer(){
		try{
			var val_customerid = $('#ussd_custcustomerid').val();
			var val_idno = $('#ussd_custidno').val();
			var val_mobileno = $('#ussd_custmobileno').val();
			
			if ((val_customerid == 0 || val_customerid.length == 0) && (val_idno.length == 0) && (val_mobileno.length == 0))
			{
				$.messager.alert("Warning", 'Enter valid customer ID or National ID or Mobile No to search');
				return;
			}
			
			$('#dgUSSDSubscriptions').datagrid('reload',{customerid:val_customerid,idno:val_idno,mobileno:val_mobileno});
			
		}catch(e){
			$.messager.alert("Warning", e);
		}
	}
	
	function addussdsubscription(){ 
		var row = $('#dgUSSDSubscriptions').datagrid('getSelected');
		if (row){		
			$.get( '../data/get_data.php',{action:151,customerid: row.customerid,mobileno: row.mobileno,customernames : row.name,userid : sessvars.userid}, function( result ) {
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'USSD Subscription successfully');
					$('#dgUSSDSubscriptions').datagrid('reload'); 
				}else{
					$.messager.alert("Warning", result.retmsg);					
				}
			});
		}
	}
	
	function resetussdsubscription(){
		var row = $('#dgUSSDSubscriptions').datagrid('getSelected');
		if (row){		
			$.get( '../data/get_data.php',{action:152,customerid: row.customerid,mobileno: row.mobileno}, function( result ) {
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'USSD Subscription Reset successfully');
					$('#dgUSSDSubscriptions').datagrid('reload'); 
				}else{
					$.messager.alert("Warning", result.retmsg);					
				}
			});
		}
	}
	
	function resetussdpinattempts(){
		var row = $('#dgUSSDSubscriptions').datagrid('getSelected');
		if (row){		
			$.get( '../data/get_data.php',{action:150,customerid: row.customerid,mobileno: row.mobileno}, function( result ) {
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'Pin Attempts Reset successfully');
					$('#dgUSSDSubscriptions').datagrid('reload'); 
				}else{
					$.messager.alert("Warning", result.retmsg);					
				}
			});
		}
	}
	
	function removeussdsubscription(){
		var row = $('#dgUSSDSubscriptions').datagrid('getSelected');
		if (row){		
			$.get( '../data/get_data.php',{action:153,customerid: row.customerid,mobileno: row.mobileno}, function( result ) {
				var result = eval('('+result+')');  
				if (result.retcode === '000'){  
					$.messager.alert("Warning", 'USSD Subscription Reset successfully');
					$('#dgUSSDSubscriptions').datagrid('reload'); 
				}else{
					$.messager.alert("Warning", result.retmsg);					
				}
			});
		}
	}
	
	
	function preview_ussdsubscriptionreport(){
		var val_mobileno = $('#ussdr_mobileno').val();
		var val_ussdstate = $('#ussd_status').combobox('getValue');
		
		if (val_mobileno.length == 0) {
			val_mobileno = 0;
		}
				
		document.getElementById('loading').style.display = 'block';
		$.get( "../data/get_reports.php",{action:14,mobileno:val_mobileno,ussdstatus:val_ussdstate}, function( data ) {
			document.getElementById("report_ussdsubscriptionreport").innerHTML = data;
			document.getElementById('loading').style.display = 'none';
		});
	}

	function ussdsimulator(){	
	    $('<div id="winussdsimulator"></div>').appendTo('body');
		
	    $('#winussdsimulator').window({width:500,height:700,modal:false,title:'USSD Simulator'});
		$('#winussdsimulator').window('refresh', 'ussdsimulator.html');		
		$('#winussdsimulator').window('open');
	}
	
	function processussd(){
		var val_ussddata = $('#ussddata').val();
		var div_usssdcontent = document.getElementById('usssdcontent');
		var val_responsedata = '';
		
		switch(val_ussddata){
			case '*235#':
			val_responsedata = 'Please enter pin';
			break;
			case '1234':
			val_responsedata = '1 Loan Application <br> 2 Eligibility Check <br> 3 Balance Enquiry <br> 4 Change Password';
			break;
			case '1':
			val_responsedata = 'Enter loan amount';
			break;
			case '2':
			val_responsedata = 'You are eligible for Ksh. 2000';
			break;
			case '3':
			val_responsedata = 'Your Emergency Loan Balance is Ksh. 0';
			break;	
			case '4':
			val_responsedata = 'Enter new password';
			break;				
			default:
		
		}
		
		div_usssdcontent.innerHTML = '<br>' + div_usssdcontent.innerHTML;
		div_usssdcontent.innerHTML =  val_ussddata + div_usssdcontent.innerHTML;
		div_usssdcontent.innerHTML = '<br>' + div_usssdcontent.innerHTML;
		div_usssdcontent.innerHTML = '    -----------------------    '+ div_usssdcontent.innerHTML;
		div_usssdcontent.innerHTML = '<br>' + div_usssdcontent.innerHTML;
		div_usssdcontent.innerHTML = val_responsedata + div_usssdcontent.innerHTML;
		
		$('#ussddata').val('');
	}
	/* End USSD*/
	
	/* ***************** Added Js ********************************** */
$(document).ready(function () {

	$(function(){
		$('#useguarantors').click(function(){
		 var ckbox = $('#useguarantors');
        if (ckbox.is(':checked')) {
            $('#tabloanapplication').tabs('enableTab', 'Guarantors');
        } else {
            $('#tabloanapplication').tabs('disableTab', 'Guarantors');
        }
		});
	});
});