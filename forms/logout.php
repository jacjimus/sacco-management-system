<?php
session_start();

// Delete certain session
session_unset();
// Delete all session variables

session_destroy();

// Jump to login page
header('Location: ../index.php');
?>
