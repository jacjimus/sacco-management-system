<?php
session_start();

include('../conf/config.inc');

#Global Variable
$action=$_GET['action'];

#Constants
$retcode='999';
$retmsg='General Failure';
$results='';

class GetReports {
	private $dbconnect;
	private $report_logo = "../images/logo.png"; 

   //Database connect
	public function __construct()
	{
		$db = new DB_Class;
		$this->dbconnect=$db->Myconn();
	}
	
	function formatMoney($number, $fractional=false) { 
		if ($fractional) { 
			$number = sprintf('%.2f', $number); 
		} 
		while (true) { 
			$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
			if ($replaced != $number) { 
				$number = $replaced; 
			} else { 
				break; 
			} 
		} 
		return $number; 
	} 	
	
	//Balance Sheet
	public function reportBalanceSheet($valuedate,$summary){
		$fromdate = date("Y",strtotime($valuedate)).'-01-01';
		if($summary == 1){
		$rpt_balancesheet = $this->dbconnect->prepare("SELECT categoryid,categoryname,ledgeraccountcode AS number,parentacc AS accountname,SUM(balance) AS balance FROM (
			 SELECT tb_ledgerformat.ledgeraccountcode,tb_ledgerformat.description AS parentacc,tb_ledgercategory.categoryid,tb_ledgeraccounts.formatid,tb_ledgeraccounts.number,tb_ledgeraccounts.description AS accountname,
			  tb_ledgercategory.description AS categoryname,fn_ledgerbalance(tb_ledgeraccounts.accountid,?) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (3,4,5) 
			  
			  UNION ALL		  
					  
			  SELECT 999 AS ledgeraccountcode,'Current Year Profit and Loss' AS parentacc,4 AS categoryid,999999 AS formatid,'999999' AS number,'Current Year Profit and Loss' AS accountname,'Liability' AS categoryname,COALESCE(SUM(CASE WHEN tb_ledgeraccounts.categoryid=1 THEN fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?) ELSE fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?)*-1 END),0) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (1,2) 
			  )balancesheet
			  GROUP BY categoryid,categoryname,ledgeraccountcode,parentacc
			  ORDER BY categoryid,ledgeraccountcode");
		}else{
			$rpt_balancesheet = $this->dbconnect->prepare("SELECT tb_ledgercategory.categoryid,tb_ledgeraccounts.formatid,tb_ledgeraccounts.number,tb_ledgeraccounts.description AS accountname,
			  tb_ledgercategory.description AS categoryname,fn_ledgerbalance(tb_ledgeraccounts.accountid,?) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (3,4,5) 
			  
			  UNION ALL		  
					  
			  SELECT 4 AS categoryid,999999 AS formatid,'999999' AS number,'Current Year Profit and Loss' AS accountname,'Liability' AS categoryname,COALESCE(SUM(CASE WHEN tb_ledgeraccounts.categoryid=1 THEN fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?) ELSE fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?)*-1 END),0) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (1,2) 
			  ORDER BY categoryid,formatid");			
		}
		$rpt_balancesheet->execute(array($valuedate,$fromdate,$valuedate,$fromdate,$valuedate));
		$results = $rpt_balancesheet->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)) {		
			$arr_keys = array_keys($results);
			if (is_array($results[$arr_keys[0]]) ) {
				$arr = array();
				foreach( $results as $row ) {
					$arr[$row['categoryname']][] = array(
								 'number'=>$row['number']
								 , 'accountname'=>$row['accountname']
								 , 'balance'=>$row['balance']
								);
				}
			}
	if(!empty($arr)){
		echo "<table id=\"report_table\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>BALANCE SHEET</strong></td></tr>";		
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>AS AT ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";		
		echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>A/C Number</strong></td><td class=\"payslip\"> <strong>A/C Name</strong></td><td class=\"payslip\"><strong>Balance</strong></td></tr>";
		
		$arr_d = array('Asset', 'Liability');
		foreach($arr_d as $TYPE){
			$dblTotal=0;
			echo "<tr><td class=\"payslip\" colspan=\"3\"><strong>".$TYPE."</strong></td></tr>";
			foreach($arr[$TYPE] as $row => $val) {
				$dblTotal=$dblTotal+$val['balance'];

				echo "<tr class=\"payslip\">";
				echo "<td align=\"left\"  class=\"payslip\">".$val['number']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$val['accountname']."</td>";		
				echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['balance'], true)."</td>";
				echo "</tr>";
			}
			echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($dblTotal, true)."</strong></td></tr>";
		}
		echo "</table>";
	}else{
		echo "No data was found!";
	}
		}else{
			echo "No data was found!";
		}		
	}
	
	//Account Statement
    public function reportAccountStatement($customerid,$idno,$accountno,$fromdate,$todate,$ignorereversal){ 
	//Get Accounts
	$sql_accounts = "SELECT v_customerdetails.customerid,v_customerdetails.customernumber,v_customerdetails.idno,tb_accounts.accountid,tb_accounts.accountnumber,tb_products.productid,tb_products.description AS productname,
	fn_accountbalanceatdate(accountid,'".date('Y-m-d', strtotime("-1 days", strtotime($fromdate)))."') AS openingbalance,
	v_customerdetails.name AS membername
	FROM tb_accounts INNER JOIN tb_products ON tb_accounts.productid = tb_products.productid	
	INNER JOIN v_customerdetails ON v_customerdetails.customerid = tb_accounts.customerid
	WHERE (tb_accounts.customerid = ".$customerid." OR ".$customerid." = 0 ) AND (accountnumber = ".$accountno." OR ".$accountno." = 0 ) AND (idno = '".$idno."' OR '".$idno."' = 0 )";

	$accounts_arr = array();
	
	if($rs=$this->dbconnect->query($sql_accounts)){
		foreach($rs as $row) {
			$node=array();
			$node['accountid']=$row['accountid'];
			$node['accountnumber']=$row['accountnumber'];
			$node['productid']=$row['productid'];
			$node['productname']=$row['productname'];
			$node['openingbalance']=$row['openingbalance'];	
			$node['membername']=$row['membername'];	
			$node['customerid']=$row['customerid'];
			$node['customernumber']=$row['customernumber'];
			$node['idno']=$row['idno'];			
			array_push($accounts_arr, $node);
		}				
	}else{
		echo "No data was found!";
		return;
	}

	if(!empty($accounts_arr))
	{
		echo "<table id=\"report_table\" cellspacing=\"3\" class=\"payslip\">";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> <img src=\"".$this->report_logo."\" alt=\"MSAFARA SACCO\"> </strong></td></tr>";//width=\"42\" height=\"42\";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> ACCOUNT STATEMENT </strong></td></tr>";			
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>From ".date("d-m-Y",strtotime($fromdate))." To ".date("d-m-Y",strtotime($todate))."</strong></td></tr>";
		echo "<tr class=\"payslip\">";
		echo "<td align=\"left\"  class=\"payslip\"><strong>Member Name :</strong></td><td align=\"left\"  class=\"payslip\" colspan=\"6\"><strong>".$accounts_arr[0]['membername']."</strong></td>";
		echo "</tr>";
		echo "<tr class=\"payslip\">";
		echo "<td align=\"left\"  class=\"payslip\"><strong>Customer Number :</strong></td><td align=\"left\"  class=\"payslip\" colspan=\"6\"><strong>".$accounts_arr[0]['customernumber']."</strong></td>";
		echo "</tr>";
		echo "<tr class=\"payslip\">";
		echo "<td align=\"left\"  class=\"payslip\"><strong>National ID :</strong></td><td align=\"left\"  class=\"payslip\" colspan=\"6\"><strong>".$accounts_arr[0]['idno']."</strong></td>";
		echo "</tr>";
		
		foreach( $accounts_arr as $account ) 
		{
			$total_debit = 0;
			$total_credit = 0;
			
			//Start looping here
			echo "<tr class=\"payslip\">";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"7\"><strong>".$account['accountnumber']." - ".$account['productname']."</strong></td>";
			echo "</tr>";
			
			echo "<tr class=\"payslip\">";
			echo "<td class=\"payslip\"><strong>Receipt No</strong></td>";
			echo "<td class=\"payslip\"> <strong>Value Date</strong></td>";
			echo "<td class=\"payslip\"><strong>Narration</strong></td>";
			echo "<td class=\"payslip\"><strong>Fee</strong></td>";
			echo "<td class=\"payslip\"><strong>Debit</strong></td>";
			echo "<td class=\"payslip\"><strong>Credit</strong></td>";
			echo "<td class=\"payslip\"><strong>Balance</strong></td>";
			echo "</tr>";		
			
			$balance = $account['openingbalance'];
						
			echo "<tr class=\"payslip\">";
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>".date("d-m-Y",strtotime($fromdate))."</strong></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>Opening Balance</strong></td>";
			echo "<td align=\"left\"  class=\"payslip\"></td>";				
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>".$this->formatMoney($balance, true)."</strong></td>";
			echo "</tr>";	
				
			if ($ignorereversal == 1){
				$rpt_accountstatement = $this->dbconnect->prepare("SELECT 
				  tb_transactions.transactionid   AS transactionid,
				  tb_receipts.receiptno           AS receiptno,
				  CONCAT(tb_receipts.trxdescription,' (',(SELECT tb_miscellaneouslists.description FROM tb_miscellaneouslists WHERE ((tb_miscellaneouslists.parentid = 18) AND (tb_miscellaneouslists.valueMember = tb_transactions.transactiontype))),')') AS trxdescription,
				  tb_transactions.valuedate       AS valuedate,
				  tb_transactions.feeid           AS feeid,
				  tb_fee.description              AS fee,
				  tb_transactions.iscredit        AS iscredit,
				  (CASE tb_transactions.iscredit WHEN 1 THEN 'Credit' ELSE 'Debit' END) AS trxtype,
				  (CASE tb_transactions.iscredit WHEN 0 THEN tb_transactions.amount ELSE 0 END) AS debit,
				  (CASE tb_transactions.iscredit WHEN 1 THEN tb_transactions.amount ELSE 0 END) AS credit,
				  tb_products.producttypeid       AS producttypeid,
				  tb_transactions.transactiontype AS transactiontypeid
				FROM tb_transactions
				INNER JOIN tb_receipts
				ON tb_receipts.receiptno = tb_transactions.receiptno
				INNER JOIN tb_accounts
				ON tb_accounts.accountid = tb_transactions.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				LEFT JOIN tb_fee
				ON tb_fee.feeid = tb_transactions.feeid
				WHERE (tb_transactions.accountid = ? OR ? = 0 )
				AND tb_transactions.valuedate >= ? 
				AND tb_transactions.valuedate <= ?
				AND tb_receipts.isreversed = 0 ORDER BY tb_products.productid,tb_transactions.receiptno,tb_transactions.valuedate,tb_transactions.transactionid");
			}else{
				$rpt_accountstatement = $this->dbconnect->prepare("SELECT 
				  tb_transactions.transactionid   AS transactionid,
				  tb_receipts.receiptno           AS receiptno,
				  CONCAT(tb_receipts.trxdescription,' (',(SELECT tb_miscellaneouslists.description FROM tb_miscellaneouslists WHERE ((tb_miscellaneouslists.parentid = 18) AND (tb_miscellaneouslists.valueMember = tb_transactions.transactiontype))),')') AS trxdescription,
				  tb_transactions.valuedate       AS valuedate,
				  tb_transactions.feeid           AS feeid,
				  tb_fee.description              AS fee,
				  tb_transactions.iscredit        AS iscredit,
				  (CASE tb_transactions.iscredit WHEN 1 THEN 'Credit' ELSE 'Debit' END) AS trxtype,
				  (CASE tb_transactions.iscredit WHEN 0 THEN tb_transactions.amount ELSE 0 END) AS debit,
				  (CASE tb_transactions.iscredit WHEN 1 THEN tb_transactions.amount ELSE 0 END) AS credit,
				  tb_products.producttypeid       AS producttypeid,
				  tb_transactions.transactiontype AS transactiontypeid
				FROM tb_transactions
				INNER JOIN tb_receipts
				ON tb_receipts.receiptno = tb_transactions.receiptno
				INNER JOIN tb_accounts
				ON tb_accounts.accountid = tb_transactions.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				LEFT JOIN tb_fee
				ON tb_fee.feeid = tb_transactions.feeid
				WHERE (tb_transactions.accountid = ? OR ? = 0 )
				AND tb_transactions.valuedate >= ? 
				AND tb_transactions.valuedate <= ?
				ORDER BY tb_products.productid,tb_transactions.receiptno,tb_transactions.valuedate,tb_transactions.transactionid");		
			}
					
			$rpt_accountstatement->execute(array($account['accountid'],$account['accountid'],$fromdate,$todate));
			$transactions = $rpt_accountstatement->fetchAll(PDO::FETCH_ASSOC);
			unset($rpt_accountstatement);
			
			if (is_array($transactions) && !empty($transactions))
			{
				foreach( $transactions as $transaction ) 
				{
					$total_debit = $total_debit + $transaction['debit'];
					$total_credit = $total_credit + $transaction['credit'];	
					
					if ($transaction['producttypeid'] != 4) { 
						if (($transaction['iscredit'] == 1) && ($transaction['feeid'] == 0))
						{
							$balance = 	$balance + $transaction['credit'];	
						}
						if (($transaction['iscredit'] == 0) && ($transaction['feeid'] == 0))
						{
							$balance = 	$balance - $transaction['debit'];	
						}
					}else{
						if (($transaction['iscredit'] == 1) && ($transaction['feeid'] == 0))
						{
							$balance = 	$balance - $transaction['credit'];	
						}
						if (($transaction['iscredit'] == 0) && ($transaction['feeid'] == 0))
						{
							$balance = 	$balance + $transaction['debit'];	
						}				
						if (($transaction['iscredit'] == 0) && ($transaction['feeid'] != 0)  && ($transaction['transactiontypeid'] == 5)){
							$balance = 	$balance + $transaction['debit'];	
						}
					}
					
					echo "<tr class=\"payslip\">";
					echo "<td align=\"left\"  class=\"payslip\">".$transaction['receiptno']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($transaction['valuedate']))."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$transaction['trxdescription']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$transaction['fee']."</td>";				
					echo "<td align=\"left\"  class=\"payslip\">".$transaction['debit']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$transaction['credit']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($balance, true)."</td>";
					echo "</tr>";
				}				
			}
			
			echo "<tr class=\"payslip\">";
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>".date("d-m-Y",strtotime($todate))."</strong></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>Closing Balance</strong></td>";
			echo "<td align=\"left\"  class=\"payslip\"></td>";				
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"></td>";
			echo "<td align=\"left\"  class=\"payslip\"><strong>".$this->formatMoney($balance, true)."</strong></td>";
			echo "</tr>";

			echo "<tr class=\"payslip\">";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"7\"></td>";
			echo "</tr>";
			
			echo "<tr class=\"payslip\">";
			echo "<td align=\"center\"  class=\"payslip\" colspan=\"2\"><strong></strong></td>";
			echo "<td align=\"center\"  class=\"payslip\" colspan=\"5\"><strong>Account Summary</strong></td>";
			echo "</tr>";
			
			echo "<tr class=\"payslip\">";
			echo "<td align=\"right\"  class=\"payslip\" colspan=\"3\"><strong>Total Debit<strong></td>";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"4\"><strong>".$this->formatMoney($total_debit, true)."<strong></td>";
			echo "</tr>";	
			
			echo "<tr class=\"payslip\">";
			echo "<td align=\"right\"  class=\"payslip\" colspan=\"3\"><strong>Total Credit<strong></td>";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"4\"><strong>".$this->formatMoney($total_credit, true)."<strong></td>";
			echo "</tr>";

			echo "<tr class=\"payslip\">";
			echo "<td align=\"right\"  class=\"payslip\" colspan=\"3\"><strong>Account Balance<strong></td>";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"4\"><strong>".$this->formatMoney($balance, true)."<strong></td>";
			echo "</tr>";			
		}	
		echo "</table>";		
	}else{
		echo "No data was found!";
	}
	}

	//Ledger Account Statement
    public function reportLedgerAccountStatement($accountno,$fromdate,$todate){ 
		$rpt_openingbalance= $this->dbconnect->prepare("SELECT (SELECT description FROM tb_ledgeraccounts WHERE accountid = ? ) AS aname,'Opening Balance' AS opendesc,COALESCE(SUM(CASE WHEN categoryid IN (2,3) THEN debit-credit ELSE credit-debit END),0) AS amount FROM v_getledgertransactions WHERE accountid = ? AND valuedate <= DATE_ADD(?,INTERVAL -1 DAY)");				
		$rpt_openingbalance->execute(array($accountno,$accountno,$fromdate));		
		$openingbalance = $rpt_openingbalance->fetchAll(PDO::FETCH_ASSOC);	
		
		$opb  = array();
		foreach( $openingbalance as $row ) {
			$opb[] = array(
						'opendesc'=>$row['opendesc'],				
						'amount'=>$row['amount'],
						'aname'=>$row['aname']
					);
		}

		$rpt_accountstatement = $this->dbconnect->prepare("SELECT * FROM v_getledgertransactions WHERE (accountid = ? OR ? = 0 )
		AND valuedate BETWEEN ? AND ? ORDER BY valuedate,ledgerid,ledgertrxid");
		
		$rpt_accountstatement->execute(array($accountno,$accountno,$fromdate,$todate));
		$results = $rpt_accountstatement->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)){
		//Get transaction per product
		$arr = array();
		foreach( $results as $row ) {
			$arr[] = array(
						'ledgerid'=>$row['ledgerid'],				
						'accountid'=>$row['accountid'],
						'valuedate'=>$row['valuedate'],						
						'accountname'=>$row['accountname'],
						'debit'=>$row['debit'],	
						'credit'=>$row['credit'],						
						'transactionnote'=>$row['transactionnote'],				
						'receiptno'=>$row['receiptno'],				
						'username'=>$row['username'],
						'iscredit'=>$row['iscredit'],
						'categoryid'=>$row['categoryid']
					);
		}
			
		echo "<table id=\"report_table\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> LEDGER ACCOUNT STATEMENT </strong></td></tr>";			
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>From ".date("d-m-Y",strtotime($fromdate))." To ".date("d-m-Y",strtotime($todate))."</strong></td></tr>";
			
		echo "<tr class=\"payslip\">";
		echo "<td align=\"left\"  class=\"payslip\"><strong>Account :</strong></td><td align=\"left\"  class=\"payslip\" colspan=\"6\"><strong>".$opb[0]["aname"]."</strong></td>";
		echo "</tr>";							
		echo "<tr class=\"payslip\">";
		echo "<td class=\"payslip\"><strong>Ledger ID</strong></td>";
		echo "<td class=\"payslip\"> <strong>Value Date</strong></td>";
		echo "<td class=\"payslip\"><strong>Narration</strong></td>";
		echo "<td class=\"payslip\"><strong>Receipt No</strong></td>";
		echo "<td class=\"payslip\"><strong>Debit</strong></td>";
		echo "<td class=\"payslip\"><strong>Credit</strong></td>";
		echo "<td class=\"payslip\"><strong>Balance</strong></td>";
		echo "</tr>";
		
		$opening_balance = $opb[0]["amount"];
		
		//if (($opb[0]["categoryid"] == 1) || ($opb[0]["categoryid"] == 4)){
		//	$opening_balance = ($opening_balance*-1);
		//}
		
		echo "<tr class=\"payslip\">";
		echo "<td align=\"left\"  class=\"payslip\" colspan=\"2\"></td>";
		echo "<td align=\"left\"  class=\"payslip\">".$opb[0]["opendesc"]."</td>";
		echo "<td align=\"left\"  class=\"payslip\" colspan=\"3\"></td>";
		echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($opening_balance, true)."</td>";
		echo "</tr>";		
	
		$running_balance = $opening_balance;
		
		if(!empty($arr)){
			foreach($arr as $val) {											
				if(($val['categoryid'] == 2) || ($val['categoryid'] == 3)){
					$running_balance = 	$running_balance + ($val['debit'] - $val['credit']);
				}
				
				if(($val['categoryid'] == 1) || ($val['categoryid'] == 4) || ($val['categoryid'] == 5)){
					$running_balance = 	$running_balance + ($val['credit'] - $val['debit']);
				}							
				
				echo "<tr class=\"payslip\">";
				echo "<td align=\"left\"  class=\"payslip\">".$val['ledgerid']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['valuedate']))."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$val['transactionnote']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$val['receiptno']."</td>";				
				echo "<td align=\"left\"  class=\"payslip\">".$val['debit']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$val['credit']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($running_balance, true)."</td>";
				echo "</tr>";
			}	
		}
			
			echo "<tr class=\"payslip\">";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"2\"></td>";
			echo "<td align=\"left\"  class=\"payslip\">Closing Balance</td>";
			echo "<td align=\"left\"  class=\"payslip\" colspan=\"3\"></td>";
			echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($running_balance, true)."</td>";
			echo "</tr>";
		
			echo "</table>";
	}else{
			echo "No data was found!";
	}			
	}

	//Income Statement
	public function reportIncomeStatement($valuedate,$summary){
		$fromdate = date("Y",strtotime($valuedate)).'-01-01';
		if($summary == 1){
			$rpt_incomestatement = $this->dbconnect->prepare("SELECT categoryid,categoryname,ledgeraccountcode AS number,parentacc AS accountname,SUM(balance) AS balance FROM (
			SELECT tb_ledgerformat.ledgeraccountcode,tb_ledgerformat.description AS parentacc,tb_ledgeraccounts.number,tb_ledgeraccounts.description AS accountname,
			  tb_ledgercategory.description AS categoryname,tb_ledgercategory.categoryid,fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (1,2) 
			  )incomestatement
			  GROUP BY categoryid,categoryname,ledgeraccountcode,parentacc
			  ORDER BY categoryid,ledgeraccountcode");
		}else{
			$rpt_incomestatement = $this->dbconnect->prepare("
			SELECT tb_ledgeraccounts.number,tb_ledgeraccounts.description AS accountname,
			  tb_ledgercategory.description AS categoryname,fn_ledgerbalancebetweendates(tb_ledgeraccounts.accountid,?,?) AS balance 
			  FROM tb_ledgeraccounts 
			  INNER JOIN tb_ledgercategory 
			  ON tb_ledgercategory.categoryid=tb_ledgeraccounts.categoryid 
			  INNER JOIN tb_ledgerformat 
			  ON tb_ledgerformat.formatid=tb_ledgeraccounts.formatid 
			  WHERE tb_ledgercategory.categoryid IN (1,2) 
			  ORDER BY tb_ledgercategory.categoryid,tb_ledgeraccounts.formatid ");			
		}
				
		$rpt_incomestatement->execute(array($fromdate,$valuedate));
		$results = $rpt_incomestatement->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)) {		
			$arr_keys = array_keys($results);
			if (is_array($results[$arr_keys[0]]) ) {
				$arr = array();
				foreach( $results as $row ) {
					$arr[$row['categoryname']][] = array(
								 'number'=>$row['number']
								 , 'accountname'=>$row['accountname']
								 , 'balance'=>$row['balance']
								);
				}
			}
	if(!empty($arr)){
		echo "<table id=\"report_table\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>INCOME STATEMENT</strong></td></tr>";		
		echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>AS AT ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";		
		echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>A/C Number</strong></td><td class=\"payslip\"> <strong>A/C Name</strong></td><td class=\"payslip\"><strong>Balance</strong></td></tr>";
		
		$arr_d = array('Income', 'Expense');
		$total_income = 0;
		$total_expnses  = 0;
		foreach($arr_d as $TYPE){
			$dblTotal=0;
			echo "<tr><td class=\"payslip\" colspan=\"3\"><strong>".$TYPE."</strong></td></tr>";
			foreach($arr[$TYPE] as $row => $val) {
				$dblTotal=$dblTotal+$val['balance'];

				echo "<tr class=\"payslip\">";
				echo "<td align=\"left\"  class=\"payslip\">".$val['number']."</td>";
				echo "<td align=\"left\"  class=\"payslip\">".$val['accountname']."</td>";		
				echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['balance'], true)."</td>";
				echo "</tr>";
			}
			echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($dblTotal, true)."</strong></td></tr>";
			if($TYPE =='Income'){
				$total_income = $dblTotal;					
			}else{
				$total_expnses  = $dblTotal;			
			}
			
		}
		if($total_income > $total_expnses){
			echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Profit</strong></td><td class=\"payslip\"><strong>".$this->formatMoney(($total_income-$total_expnses), true)."</strong></td></tr>";
		}elseif($total_expnses > $total_income){
			echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Loss</strong></td><td class=\"payslip\"><strong>".$this->formatMoney(($total_income-$total_expnses), true)."</strong></td></tr>";
		}else{
			echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Difference</strong></td><td class=\"payslip\"><strong>".$this->formatMoney(($total_income-$total_expnses), true)."</strong></td></tr>";
		}
		echo "</table>";
	}else{
		echo "No data was found!";
	}
		}else{
			echo "No data was found!";
		}		
	}

	//Account Balance
	public function reportAccountBalance($valuedate,$producttype){
		$rpt_accountbalance = $this->dbconnect->prepare("SELECT customerid,customername,accountid,accountnumber,fn_accountbalanceatdate(accountid,?) AS balance,fn_getinterestdue(accountid,?) AS interestdue,productname,producttype,activestate FROM v_customeraccountdetails 
		WHERE fn_accountbalanceatdate(accountid,?)<>0 
		AND (producttypeid = ? OR ? = 0)
		ORDER BY customerid");
		
		$rpt_accountbalance->execute(array($valuedate,$valuedate,$valuedate,$producttype,$producttype));
		$results = $rpt_accountbalance->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			//Get transaction per product
			$arr = array();
			foreach( $results as $row ) {
				$arr[$row['producttype']][] = array(
									 'customerid'=>$row['customerid']
									 , 'customername'=>$row['customername']
									 , 'accountid'=>$row['accountid']
									 , 'accountnumber'=>$row['accountnumber'] 
									 , 'balance'=>$row['balance']
									 , 'interestdue'=>$row['interestdue']
									 , 'productname'=>$row['productname']
									 , 'activestate'=>$row['activestate']
									);
			}
		
			if(!empty($arr)){
				echo "<table id=\"report_accountbalance\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"8\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"8\"><strong>ACCOUNT BALANCE REPORT</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"8\"><strong>AS AT ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
				
				$arr_producttype = array_keys($arr);
				foreach($arr_producttype as $producttype){
					$totalbalance=0;
					$totalinterest=0;
					
					echo "<tr><td class=\"payslip\" colspan=\"8\" align=\"center\"><strong>Product Type - ".$producttype."</strong></td></tr>";
					
					$arr_accounts = $arr[$producttype];
					$arr_products = array();					
					foreach($arr_accounts as $accounts ){						
						$arr_products[$accounts['productname']][] = array(
											 'customerid'=>$accounts['customerid']
											 , 'customername'=>$accounts['customername']
											 , 'accountid'=>$accounts['accountid']
											 , 'accountnumber'=>$accounts['accountnumber']
											 , 'balance'=>$accounts['balance']
											 , 'interestdue'=>$accounts['interestdue']
											 , 'activestate'=>$accounts['activestate']
											);
					}
					
					$arr_product_items = array_keys($arr_products);
					foreach($arr_product_items as $product){
						$dblBalanceTotal=0;
						$dblInterestTotal=0;
						
						echo "<tr><td class=\"payslip\" colspan=\"8\"><strong>Product - ".$product."</strong></td></tr>";
						echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"> <strong>Customer Name</strong></td><td class=\"payslip\"><strong>Account ID</strong></td><td class=\"payslip\"><strong>Account #</strong></td><td class=\"payslip\"><strong>Balance</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>Active</strong></td></tr>";
					
						foreach($arr_products[$product] as $row => $val){
							$dblBalanceTotal=$dblBalanceTotal+$val['balance'];	
							$dblInterestTotal=$dblInterestTotal+$val['interestdue'];
							
							echo "<tr class=\"payslip\">";
							echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['customername']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['accountid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['accountnumber']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['balance'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['interestdue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['balance']+$val['interestdue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['activestate']."</td>";
							echo "</tr>";
						}
						echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>".$product." Total</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong>".$this->formatMoney($dblBalanceTotal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($dblInterestTotal, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($dblInterestTotal+$dblBalanceTotal, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";				
						$totalbalance=$totalbalance+$dblBalanceTotal;
						$totalinterest=$totalinterest+$dblInterestTotal;
					}
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"2\"><strong>".$producttype." Total</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong>".$this->formatMoney($totalbalance, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($totalinterest, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($totalinterest+$totalbalance, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";				
				}
				echo "</table>";		
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}
	
	//Trial Balance
	public function reportTrialBalance($valuedate){
		$rpt_trialbalance = $this->dbconnect->prepare("SELECT accountnumber,accountid,accountname,categoryid,CASE WHEN COALESCE(SUM(debit),0) > COALESCE(SUM(credit),0) THEN (COALESCE(SUM(debit),0)- COALESCE(SUM(credit),0)) ELSE 0 END AS debit, 
		CASE WHEN COALESCE(SUM(credit),0) > COALESCE(SUM(debit),0)  THEN (COALESCE(SUM(credit),0)- COALESCE(SUM(debit),0)) ELSE 0 END AS credit FROM v_getledgertransactions	
		WHERE valuedate <= ?
		GROUP BY accountnumber,accountid,accountname,categoryid
		ORDER BY categoryid,accountid
		");
		
		$rpt_trialbalance->execute(array($valuedate));
		$results = $rpt_trialbalance->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)) {		
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
							 'accountnumber'=>$row['accountnumber']
							 , 'accountname'=>$row['accountname']
							 , 'debit'=>$row['debit']
							 , 'credit'=>$row['credit']
							);
			}

			if(!empty($arr)){
				echo "<table id=\"report_table\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>TRIAL BALANCE</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"7\"><strong>AS AT ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>A/C Number</strong></td><td class=\"payslip\"> <strong>A/C Name</strong></td><td class=\"payslip\"><strong>Debit</strong></td><td class=\"payslip\"><strong>Credit</strong></td></tr>";
				
				$total_debits = 0;
				$total_credits = 0;
				$diff_debits = 0;
				$diff_credits = 0;	
				
					foreach($arr as $val) {
						$total_debits += $val['debit'];
						$total_credits += $val['credit'];
						
						echo "<tr class=\"payslip\">";
						echo "<td align=\"left\"  class=\"payslip\">".$val['accountnumber']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['accountname']."</td>";		
						echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['debit'], true)."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['credit'], true)."</td>";
						echo "</tr>";
					}

					if ($total_debits > $total_credits){
						$diff_debits = $total_debits - $total_credits;
					}elseif ($total_credits > $total_debits){
						$diff_credits = $total_credits - $total_debits;
					}
					
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_debits, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($total_credits, true)."</strong></td></tr>";
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Diff</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($diff_debits, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($diff_credits, true)."</strong></td></tr>";
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}
	}

	//Customer Register
	public function reportCustomerRegister($joindate){
		$rpt_customerregister = $this->dbconnect->prepare("SELECT customerid,customernumber,branchname,name,joindate,customertypedesc,loanofficername,idno,idtypedesc 
		FROM v_customerdetails WHERE joindate <= ?");
		
		$rpt_customerregister->execute(array($joindate));
		$results = $rpt_customerregister->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)) {		
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
							 'customerid'=>$row['customerid']
							 , 'customernumber'=>$row['customernumber']
							 , 'branchname'=>$row['branchname']
							 , 'name'=>$row['name']
							 , 'joindate'=>$row['joindate']
							 , 'customertypedesc'=>$row['customertypedesc']
							 , 'loanofficername'=>$row['loanofficername']
							 , 'idno'=>$row['idno']
							 , 'idtypedesc'=>$row['idtypedesc']
							);
			}

			if(!empty($arr)){
				echo "<table id=\"table_customerregister\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"9\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"9\"><strong>CUSTOMER REGISTER</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"9\"><strong>AS AT JOIN DATE ".date("d-m-Y",strtotime($joindate))."</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"> <strong>Customer #</strong>";
				echo "</td><td class=\"payslip\"><strong>Branch</strong></td><td class=\"payslip\"><strong>Name</strong></td>";
				echo "<td class=\"payslip\"><strong>Join Date</strong></td><td class=\"payslip\"><strong>Customer Type</strong></td>";
				echo "<td class=\"payslip\"><strong>Loan Officer</strong></td><td class=\"payslip\"><strong>ID No</strong></td><td class=\"payslip\"><strong>ID Type</strong></td></tr>";
				
				$total = 0;
				
					foreach($arr as $val) {
						$total += 1;
						echo "<tr class=\"payslip\">";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customernumber']."</td>";		
						echo "<td align=\"left\"  class=\"payslip\">".$val['branchname']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['joindate']))."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customertypedesc']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['loanofficername']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['idno']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['idtypedesc']."</td>";
						echo "</tr>";
					}

					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$total."</strong></td><td class=\"payslip\" colspan=\"6\"></td></tr>";					
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}
	}

	//Loan Portfolio Report
	public function reportLoanPortfolio($valuedate,$loanofficerid,$branchid,$arrearsonly,$rlpsummary){	
		if($arrearsonly == 1){
			$rpt_loanportfolio = $this->dbconnect->prepare("SELECT tb_loans.loanid,tb_loans.accountid,tb_loans.customerid,v_customerdetails.name,v_customerdetails.loanofficername,v_customerdetails.mobileno,v_customerdetails.idno,
				tb_loans.productid,tb_products.description AS productname,
				tb_loans.loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = tb_loans.loanstatus) AS loanstatusdesc,
				tb_loans.approvedamount,tb_loans.term,tb_loans.rate,
				tb_loans.graceperiod,tb_loans.repaymentamount,tb_loans.disbursedon,
				fn_getprincipalbalance(tb_loans.accountid, ?) AS principalbal,
				fn_getprincipaldue(tb_loans.accountid, ?) AS principaldue,
				fn_getinterestdue(tb_loans.accountid, ?) AS interestdue,
				fn_getarrearsamount(tb_loans.accountid, ?) AS arrearsamount,
				fn_getarrearsdays(tb_loans.accountid, ?) AS arrearsdays
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE fn_getprincipalbalance(tb_loans.accountid, ?)<>0
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)
				AND fn_getarrearsamount(tb_loans.accountid, ?) > 0
				AND tb_loans.loanstatus<>4
				ORDER BY tb_loans.customerid,tb_loans.accountid
				");
				
			$rpt_loanportfolio->execute(array($valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$loanofficerid,$loanofficerid,$branchid,$branchid,$valuedate));
			$results = $rpt_loanportfolio->fetchAll(PDO::FETCH_ASSOC);				
		}else{
			$rpt_loanportfolio = $this->dbconnect->prepare("SELECT tb_loans.loanid,tb_loans.accountid,tb_loans.customerid,v_customerdetails.name,v_customerdetails.loanofficername,v_customerdetails.mobileno,v_customerdetails.idno,
				tb_loans.productid,tb_products.description AS productname,
				tb_loans.loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = tb_loans.loanstatus) AS loanstatusdesc,
				tb_loans.approvedamount,tb_loans.term,tb_loans.rate,
				tb_loans.graceperiod,tb_loans.repaymentamount,tb_loans.disbursedon,
				fn_getprincipalbalance(tb_loans.accountid, ?) AS principalbal,
				fn_getprincipaldue(tb_loans.accountid, ?) AS principaldue,
				fn_getinterestdue(tb_loans.accountid, ?) AS interestdue,
				fn_getarrearsamount(tb_loans.accountid, ?) AS arrearsamount,
				fn_getarrearsdays(tb_loans.accountid, ?) AS arrearsdays
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE fn_getprincipalbalance(tb_loans.accountid, ?)<>0
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)
				ORDER BY tb_loans.customerid,tb_loans.accountid
				");	

			$rpt_loanportfolio->execute(array($valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$loanofficerid,$loanofficerid,$branchid,$branchid));
			$results = $rpt_loanportfolio->fetchAll(PDO::FETCH_ASSOC);				
		}
	
		if (is_array($results)){
			//Get transaction per product
			$arr = array();
			foreach( $results as $row ) {
				$arr[$row['productname']][] = array(
									 'loanid'=>$row['loanid']
									 , 'accountid'=>$row['accountid']
									 , 'customerid'=>$row['customerid']
									 , 'name'=>$row['name']
									 , 'productid'=>$row['productid']
									 , 'loanstatus'=>$row['loanstatus']
									 , 'loanstatusdesc'=>$row['loanstatusdesc']
									 , 'approvedamount'=>$row['approvedamount']
									 , 'term'=>$row['term']
									 , 'rate'=>$row['rate']
									 , 'graceperiod'=>$row['graceperiod']									 
									 , 'repaymentamount'=>$row['repaymentamount']
									 , 'disbursedon'=>$row['disbursedon']									 
									 , 'principalbal'=>$row['principalbal']
									 , 'principaldue'=>$row['principaldue']									 
									 , 'interestdue'=>$row['interestdue']
									 , 'arrearsamount'=>$row['arrearsamount']									 
									 , 'arrearsdays'=>$row['arrearsdays']
									 , 'loanofficername'=>$row['loanofficername']
									 , 'idno'=>$row['idno']
									 , 'mobileno'=>$row['mobileno']
									);
			}
		
			if(!empty($arr)){

				$products = array_keys($arr);
				if($rlpsummary == 1){
					echo "<table id=\"report_accountbalance\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong>LOAN PORTFOLIO REPORT</strong></td></tr>";		
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong>Report Date ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
					
					foreach($products as $product){
						echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"15\" align=\"left\"><strong>Product - ".$product."</strong></td></tr>";	
						echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Names</strong></td><td class=\"payslip\"> <strong>ID #</strong></td><td class=\"payslip\"><strong>Mobile No</strong></td><td class=\"payslip\"><strong>Loanofficer Name</strong></td><td class=\"payslip\"><strong>Loan Status</strong></td><td class=\"payslip\"><strong>Loan Amount</strong></td><td class=\"payslip\"><strong>Term</strong></td><td class=\"payslip\"><strong>Disbursed On</strong></td><td class=\"payslip\"><strong>Arrears Amount (P+I)</strong></td><td class=\"payslip\"><strong>Arrears Days</strong></td></tr>";
							$loanamount_Total	=  0;
							$principalbal_Total	=  0;
							$principaldue_Total	=  0;
							$interestdue_Total	=  0;
							$arrearsamount_Total	=  0;
						
							foreach($arr[$product] as $row => $val){	
								$loanamount_Total	=  $loanamount_Total+$val['approvedamount'];
								$principalbal_Total	=  $principalbal_Total+$val['principalbal'];
								$principaldue_Total	=  $principaldue_Total+$val['principaldue'];
								$interestdue_Total	=  $interestdue_Total+$val['interestdue'];
								$arrearsamount_Total =  $arrearsamount_Total+$val['arrearsamount'];
					
								echo "<tr class=\"payslip\">";
								echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['idno']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['mobileno']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['loanofficername']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['loanstatusdesc']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['approvedamount'], true)."</td>";							
								echo "<td align=\"left\"  class=\"payslip\">".$val['term']."</td>";						
								echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['disbursedon']))."</td>";							
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['arrearsamount'], true)."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['arrearsdays']."</td>";								
								echo "</tr>";
							}
						echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"5\"><strong>".$product." Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($loanamount_Total, true)."</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong>".$this->formatMoney($arrearsamount_Total, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";			
					}					
				}else{
					echo "<table id=\"report_accountbalance\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong>LOAN PORTFOLIO REPORT</strong></td></tr>";		
					echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"15\"><strong>Report Date ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
										
					foreach($products as $product){
						echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"15\" align=\"left\"><strong>Product - ".$product."</strong></td></tr>";	
						echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Loan ID</strong></td><td class=\"payslip\"> <strong>Account ID</strong></td><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"><strong>Customer Name</strong></td><td class=\"payslip\"><strong>Loan Status</strong></td><td class=\"payslip\"><strong>Loan Amount</strong></td><td class=\"payslip\"><strong>Term</strong></td><td class=\"payslip\"><strong>Rate</strong></td><td class=\"payslip\"><strong>Repayment Amount</strong></td><td class=\"payslip\"><strong>Disbursed On</strong></td><td class=\"payslip\"><strong>Principal Balance</strong></td><td class=\"payslip\"><strong>Principal Due</strong></td><td class=\"payslip\"><strong>Interest Due</strong></td><td class=\"payslip\"><strong>Arrears Amount (P+I)</strong></td><td class=\"payslip\"><strong>Arrears Days</strong></td></tr>";
							$loanamount_Total	=  0;
							$principalbal_Total	=  0;
							$principaldue_Total	=  0;
							$interestdue_Total	=  0;
							$arrearsamount_Total	=  0;
						
							foreach($arr[$product] as $row => $val){	
								$loanamount_Total	=  $loanamount_Total+$val['approvedamount'];
								$principalbal_Total	=  $principalbal_Total+$val['principalbal'];
								$principaldue_Total	=  $principaldue_Total+$val['principaldue'];
								$interestdue_Total	=  $interestdue_Total+$val['interestdue'];
								$arrearsamount_Total =  $arrearsamount_Total+$val['arrearsamount'];
					
								echo "<tr class=\"payslip\">";
								echo "<td align=\"left\"  class=\"payslip\">".$val['loanid']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['accountid']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['loanstatusdesc']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['approvedamount'], true)."</td>";							
								echo "<td align=\"left\"  class=\"payslip\">".$val['term']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['rate']."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['repaymentamount'], true)."</td>";							
								echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['disbursedon']))."</td>";							
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principalbal'], true)."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principaldue'], true)."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['interestdue'], true)."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['arrearsamount'], true)."</td>";
								echo "<td align=\"left\"  class=\"payslip\">".$val['arrearsdays']."</td>";
								echo "</tr>";
							}
						echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"4\"><strong>".$product." Total</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong>".$this->formatMoney($loanamount_Total, true)."</strong></td><td class=\"payslip\" colspan=\"4\"><strong></strong></td><td class=\"payslip\"><strong>".$this->formatMoney($principalbal_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($arrearsamount_Total, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";			
					}
				}
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}	
	
	//Loan Portfolio Report
	public function reportpar($valuedate,$loanofficerid,$branchid,$groupby){	
		$groupbycaption = 'Global PAR';
		if($groupby == 0){
			$groupbycaption = 'Loan Officer';
			$rpt_loanpar = $this->dbconnect->prepare("SELECT 
				v_customerdetails.loanofficername AS groupbyname,v_customerdetails.loanofficerid AS groupbyid,
				CASE 
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) <=0 THEN '0'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 1 AND 30 THEN '1 - 30'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 31 AND 60 THEN '31 - 60'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 61 AND 90 THEN '61 - 90'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 91 AND 180 THEN '91 - 180'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) > 180 THEN '> 180'
				END AS arrearsdays,
				COUNT(*) AS numberofloans,
				SUM(fn_getprincipalbalance(tb_loans.accountid, ?)) AS principalbal,
				SUM(fn_getprincipaldue(tb_loans.accountid, ?)) AS principaldue,
				SUM(fn_getinterestdue(tb_loans.accountid, ?)) AS interestdue,
				SUM(fn_getarrearsamount(tb_loans.accountid, ?)) AS arrearsamount
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE fn_getprincipalbalance(tb_loans.accountid, ?)<>0
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)			
				GROUP BY arrearsdays,v_customerdetails.loanofficerid,v_customerdetails.loanofficername
				");
		}else if($groupby == 1){
			$groupbycaption = 'Branch';
			$rpt_loanpar = $this->dbconnect->prepare("SELECT 
				v_customerdetails.branchname AS groupbyname,v_customerdetails.branchid AS groupbyid,
				CASE 
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) <=0 THEN '0'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 1 AND 30 THEN '1 - 30'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 31 AND 60 THEN '31 - 60'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 61 AND 90 THEN '61 - 90'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 91 AND 180 THEN '91 - 180'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) > 180 THEN '> 180'
				END AS arrearsdays,
				COUNT(*) AS numberofloans,
				SUM(fn_getprincipalbalance(tb_loans.accountid, ?)) AS principalbal,
				SUM(fn_getprincipaldue(tb_loans.accountid, ?)) AS principaldue,
				SUM(fn_getinterestdue(tb_loans.accountid, ?)) AS interestdue,
				SUM(fn_getarrearsamount(tb_loans.accountid, ?)) AS arrearsamount
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE fn_getprincipalbalance(tb_loans.accountid, ?)<>0
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)			
				GROUP BY arrearsdays,v_customerdetails.branchid,v_customerdetails.branchname
				");			
		}else{
			$rpt_loanpar = $this->dbconnect->prepare("SELECT 
				'Global PAR' AS groupbyname,'Global PAR' AS groupbyid,
				CASE 
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) <=0 THEN '0'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 1 AND 30 THEN '1 - 30'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 31 AND 60 THEN '31 - 60'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 61 AND 90 THEN '61 - 90'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) BETWEEN 91 AND 180 THEN '91 - 180'
				WHEN fn_getarrearsdays(tb_loans.accountid, ?) > 180 THEN '> 180'
				END AS arrearsdays,
				COUNT(*) AS numberofloans,
				SUM(fn_getprincipalbalance(tb_loans.accountid, ?)) AS principalbal,
				SUM(fn_getprincipaldue(tb_loans.accountid, ?)) AS principaldue,
				SUM(fn_getinterestdue(tb_loans.accountid, ?)) AS interestdue,
				SUM(fn_getarrearsamount(tb_loans.accountid, ?)) AS arrearsamount
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE fn_getprincipalbalance(tb_loans.accountid, ?)<>0
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)			
				GROUP BY arrearsdays
				");				
		}		
		
		$rpt_loanpar->execute(array($valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$loanofficerid,$loanofficerid,$branchid,$branchid));
		$results = $rpt_loanpar->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			//Get transaction per loanofficer
			$arr = array();

			foreach( $results as $row ) {
				$arr[$row['groupbyname']][] = array(
									 'arrearsdays'=>$row['arrearsdays']
									 , 'numberofloans'=>$row['numberofloans'] 
									 , 'principalbal'=>$row['principalbal']
									 , 'principaldue'=>$row['principaldue']
									 , 'interestdue'=>$row['interestdue']
									 , 'arrearsamount'=>$row['arrearsamount']									 
									);
			}
			
			if(!empty($arr)){
				echo "<table id=\"report_paranalysis\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong>PAR ANALYSIS REPORT</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong>Report Date ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
				
				$groupings = array_keys($arr);
				foreach($groupings as $group){
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"6\" align=\"left\"><strong>".$groupbycaption." - ".$group."</strong></td></tr>";	
					echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Days From To</strong></td><td class=\"payslip\"> <strong>No of Loans</strong></td><td class=\"payslip\"><strong>Principal Balance</strong></td><td class=\"payslip\"><strong>Principal Due</strong></td><td class=\"payslip\"><strong>Interest Due</strong></td><td class=\"payslip\"><strong>Arrears Amount (P+I)</strong></td></tr>";
						$numberofloans_Total	=  0;
						$principalbal_Total	=  0;
						$principaldue_Total	=  0;
						$interestdue_Total	=  0;
						$arrearsamount_Total	=  0;
					
						foreach($arr[$group] as $row => $val){	
							$numberofloans_Total	=  $numberofloans_Total+$val['numberofloans'];
							$principalbal_Total	=  $principalbal_Total+$val['principalbal'];
							$principaldue_Total	=  $principaldue_Total+$val['principaldue'];
							$interestdue_Total	=  $interestdue_Total+$val['interestdue'];
							$arrearsamount_Total =  $arrearsamount_Total+$val['arrearsamount'];
							
							
				
							echo "<tr class=\"payslip\">";
							echo "<td align=\"left\"  class=\"payslip\">".$val['arrearsdays']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['numberofloans']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principalbal'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principaldue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['interestdue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['arrearsamount'], true)."</td>";
							//echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['approvedamount'], true)."</td>";							
							echo "</tr>";
						}
					echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>".$group." Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($numberofloans_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($principalbal_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($arrearsamount_Total, true)."</strong></td></tr>";			
				}
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}

	//Expected Repayments Report
	public function reportexpectedrepayment($valuedate,$loanofficerid,$branchid,$exprepsummary){
		if($exprepsummary == 0){
			$rpt_loanportfolio = $this->dbconnect->prepare("SELECT tb_loans.loanid,tb_loans.accountid,tb_loans.customerid,v_customerdetails.name,v_customerdetails.loanofficerid,v_customerdetails.loanofficername,
				tb_loans.productid,tb_products.description AS productname,
				tb_loans.loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = tb_loans.loanstatus) AS loanstatusdesc,
				tb_loans.approvedamount,tb_loans.term,tb_loans.rate,
				tb_loans.graceperiod,tb_loans.repaymentamount,tb_loans.disbursedon,
				fn_getprincipaldue(tb_loans.accountid, ?) AS principaldue,
				fn_getinterestdue(tb_loans.accountid, ?) AS interestdue,
				COALESCE(fn_getprincipaldue(tb_loans.accountid, ?),0)+COALESCE(fn_getinterestdue(tb_loans.accountid, ?),0) AS totaldue,
				COALESCE((fn_getinterestbalance(tb_loans.accountid, ?)+fn_getprincipalbalance(tb_loans.accountid, ?)),0) AS payoffamount,
				(SELECT repaymentdate FROM tb_loanschedule WHERE tb_loanschedule.loanid = tb_loans.loanid ORDER BY period LIMIT 1) AS lastinstallmentdate,
				v_customerdetails.mobileno
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE COALESCE(fn_getprincipaldue(tb_loans.accountid, ?),0)+COALESCE(fn_getinterestdue(tb_loans.accountid, ?),0)<>0 
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)
				ORDER BY tb_loans.customerid,tb_loans.accountid
				");
		}else{
				$rpt_loanportfolio = $this->dbconnect->prepare("SELECT tb_loans.customerid,v_customerdetails.name,v_customerdetails.mobileno,v_customerdetails.loanofficerid,v_customerdetails.loanofficername,
				SUM(COALESCE(fn_getprincipaldue(tb_loans.accountid, ?),0)) AS principaldue,
				SUM(COALESCE(fn_getinterestdue(tb_loans.accountid, ?),0)) AS interestdue,
				SUM(COALESCE(fn_getprincipaldue(tb_loans.accountid, ?),0)+COALESCE(fn_getinterestdue(tb_loans.accountid, ?),0)) AS totaldue,
				SUM(COALESCE((fn_getinterestbalance(tb_loans.accountid, ?)+fn_getprincipalbalance(tb_loans.accountid, ?)),0)) AS payoffamount
				FROM tb_accounts
				INNER JOIN tb_loans
				ON tb_accounts.accountid = tb_loans.accountid
				INNER JOIN tb_products
				ON tb_products.productid = tb_accounts.productid
				INNER JOIN v_customerdetails
				ON v_customerdetails.customerid = tb_accounts.customerid
				WHERE COALESCE(fn_getprincipaldue(tb_loans.accountid, ?),0)+COALESCE(fn_getinterestdue(tb_loans.accountid, ?),0)<>0 
				AND (v_customerdetails.loanofficerid = ? OR ? = 0)
				AND (v_customerdetails.branchid = ? OR ? = 0)
				GROUP BY tb_loans.customerid,v_customerdetails.name,v_customerdetails.mobileno,v_customerdetails.loanofficerid,v_customerdetails.loanofficername
				");
		}
		
		$rpt_loanportfolio->execute(array($valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$valuedate,$loanofficerid,$loanofficerid,$branchid,$branchid));
		$results = $rpt_loanportfolio->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
		if($exprepsummary == 0){
			//Get transaction per product
			$arr = array();
			foreach( $results as $row ) {
				$arr[$row['loanofficername']][] = array(
									 'loanid'=>$row['loanid']
									 , 'accountid'=>$row['accountid']
									 , 'customerid'=>$row['customerid']
									 , 'name'=>$row['name']
									 , 'productid'=>$row['productid']
									 , 'loanstatusdesc'=>$row['loanstatusdesc']
									 , 'approvedamount'=>$row['approvedamount']
									 , 'lastinstallmentdate'=>$row['lastinstallmentdate']
									 , 'rate'=>$row['rate']
									 , 'graceperiod'=>$row['graceperiod']									 
									 , 'repaymentamount'=>$row['repaymentamount']
									 , 'disbursedon'=>$row['disbursedon']									 
									 , 'principaldue'=>$row['principaldue']									 
									 , 'interestdue'=>$row['interestdue']
									 , 'totaldue'=>$row['totaldue']									 
									 , 'payoffamount'=>$row['payoffamount']
									 , 'mobileno'=>$row['mobileno']
									);
			}
		
			if(!empty($arr)){
				echo "<table id=\"report_expectedrepayment\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"11\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"11\"><strong>EXPECTED REPAYMENT REPORT - DETAILED</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"11\"><strong>Report Date ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
				
				$products = array_keys($arr);
				//Over all totals
				$oat_totaldue_Total	=  0;
				$oat_principaldue_Total	=  0;
				$oat_interestdue_Total	=  0;
				$oat_payoffamount_Total	=  0;
						
				foreach($products as $product){
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"11\" align=\"left\"><strong>Loan Officer - ".$product."</strong></td></tr>";	
					echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Loan ID</strong></td><td class=\"payslip\"> <strong>Account ID</strong></td><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"><strong>Customer Name</strong></td><td class=\"payslip\"><strong>Last Installment Date</strong></td><td class=\"payslip\"><strong>Repayment Amount</strong></td><td class=\"payslip\"><strong>Disbursed On</strong></td><td class=\"payslip\"><strong>Principal Due</strong></td><td class=\"payslip\"><strong>Interest Due</strong></td><td class=\"payslip\"><strong>Total Amount Due</strong></td><td class=\"payslip\"><strong>Mobile No</strong></td></tr>";//<td class=\"payslip\"><strong>Total Payoff Amount</strong></td>
						$totaldue_Total	=  0;
						$principaldue_Total	=  0;
						$interestdue_Total	=  0;
						$payoffamount_Total	=  0;
					
						foreach($arr[$product] as $row => $val){	
							$totaldue_Total	=  $totaldue_Total+$val['totaldue'];
							$principaldue_Total	=  $principaldue_Total+$val['principaldue'];
							$interestdue_Total	=  $interestdue_Total+$val['interestdue'];
							$payoffamount_Total =  $payoffamount_Total+$val['payoffamount'];
							
							$oat_totaldue_Total	=  $oat_totaldue_Total+$val['totaldue'];
							$oat_principaldue_Total	=  $oat_principaldue_Total+$val['principaldue'];
							$oat_interestdue_Total	=  $oat_interestdue_Total+$val['interestdue'];
							$oat_payoffamount_Total =  $oat_payoffamount_Total+$val['payoffamount'];							
				
							echo "<tr class=\"payslip\">";
							echo "<td align=\"left\"  class=\"payslip\">".$val['loanid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['accountid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";						
							echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['lastinstallmentdate']))."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['repaymentamount'], true)."</td>";							
							echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['disbursedon']))."</td>";														
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principaldue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['interestdue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['totaldue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['mobileno']."</td>";	
							//echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['payoffamount'], true)."</td>";
							echo "</tr>";
						}
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"3\"></td><td class=\"payslip\"><strong>".$product." Total</strong></td><td class=\"payslip\" colspan=\"3\"></td><td class=\"payslip\"><strong>".$this->formatMoney($principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($totaldue_Total, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";//<td class=\"payslip\"><strong>".$this->formatMoney($payoffamount_Total, true)."</strong></td>			
				}
				echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"3\"></td><td class=\"payslip\"><strong> Total</strong></td><td class=\"payslip\" colspan=\"3\"></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_totaldue_Total, true)."</strong></td><td class=\"payslip\"><strong></strong></td></tr>";//<td class=\"payslip\"><strong>".$this->formatMoney($oat_payoffamount_Total, true)."</strong></td>			
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			//Get transaction per loanofficername
			//Summary Report
			$arr = array();
			foreach( $results as $row ) {
				$arr[$row['loanofficername']][] = array(
									  'customerid'=>$row['customerid']
									 , 'name'=>$row['name']									 									 
									 , 'principaldue'=>$row['principaldue']									 
									 , 'interestdue'=>$row['interestdue']
									 , 'totaldue'=>$row['totaldue']									 
									 , 'payoffamount'=>$row['payoffamount']
									 , 'mobileno'=>$row['mobileno']
									);
			}
		
			if(!empty($arr)){
				echo "<table id=\"report_expectedrepayment\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong>EXPECTED REPAYMENT REPORT - SUMMARY</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"6\"><strong>Report Date ".date("d-m-Y",strtotime($valuedate))."</strong></td></tr>";				
				
				$products = array_keys($arr);
				//Over all totals
				$oat_totaldue_Total	=  0;
				$oat_principaldue_Total	=  0;
				$oat_interestdue_Total	=  0;
				$oat_payoffamount_Total	=  0;
				
				foreach($products as $product){
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"6\" align=\"left\"><strong>Loan Officer - ".$product."</strong></td></tr>";	
					echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"><strong>Mobile No</strong></td><td class=\"payslip\"><strong>Customer Name</strong></td><td class=\"payslip\"><strong>Principal Due</strong></td><td class=\"payslip\"><strong>Interest Due</strong></td><td class=\"payslip\"><strong>Total Amount Due</strong></td></tr>";
						$totaldue_Total	=  0;
						$principaldue_Total	=  0;
						$interestdue_Total	=  0;
						$payoffamount_Total	=  0;
					
						foreach($arr[$product] as $row => $val){	
							$totaldue_Total	=  $totaldue_Total+$val['totaldue'];
							$principaldue_Total	=  $principaldue_Total+$val['principaldue'];
							$interestdue_Total	=  $interestdue_Total+$val['interestdue'];
							$payoffamount_Total =  $payoffamount_Total+$val['payoffamount'];
							
							$oat_totaldue_Total	=  $oat_totaldue_Total+$val['totaldue'];
							$oat_principaldue_Total	=  $oat_principaldue_Total+$val['principaldue'];
							$oat_interestdue_Total	=  $oat_interestdue_Total+$val['interestdue'];
							$oat_payoffamount_Total =  $oat_payoffamount_Total+$val['payoffamount'];							
				
							echo "<tr class=\"payslip\">";
							echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['mobileno']."</td>";	
							echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";							
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['principaldue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['interestdue'], true)."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['totaldue'], true)."</td>";
							//echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['payoffamount'], true)."</td>";
							echo "</tr>";
						}
					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong>".$product." Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($totaldue_Total, true)."</strong></td></tr>";			
				}
				echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_principaldue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_interestdue_Total, true)."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($oat_totaldue_Total, true)."</strong></td></tr>";			
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}
		}else{
			echo "No data was found!";
		}		
	}	
	
	//Loan Disbursement Report
	public function reportLoanDisbursement($fromdate,$todate,$loanofficerid,$branchid,$loanstatus){	
		$rpt_loandisbursement = $this->dbconnect->prepare("SELECT tb_loans.loanid,tb_loans.accountid,tb_loans.customerid,v_customerdetails.name,v_customerdetails.mobileno,
			tb_loans.productid,tb_products.description AS productname,
			tb_loans.loanstatus,(SELECT description FROM tb_miscellaneouslists WHERE parentid = 8 AND valueMember = tb_loans.loanstatus) AS loanstatusdesc,
			tb_loans.approvedamount,tb_loans.term,tb_loans.rate,
			tb_loans.graceperiod,tb_loans.repaymentamount,tb_loans.disbursedon
			FROM tb_accounts
			INNER JOIN tb_loans
			ON tb_accounts.accountid = tb_loans.accountid
			INNER JOIN tb_products
			ON tb_products.productid = tb_accounts.productid
			INNER JOIN v_customerdetails
			ON v_customerdetails.customerid = tb_accounts.customerid
			WHERE tb_loans.disbursedon BETWEEN ? AND ?
			AND (v_customerdetails.loanofficerid = ? OR ? = 0)
			AND (v_customerdetails.branchid = ? OR ? = 0)
			AND (tb_loans.loanstatus = ? OR ? = 0)
			ORDER BY tb_loans.customerid,tb_loans.accountid
			");
		
		$rpt_loandisbursement->execute(array($fromdate,$todate,$loanofficerid,$loanofficerid,$branchid,$branchid,$loanstatus,$loanstatus));
		$results = $rpt_loandisbursement->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			//Get transaction per product
			$arr = array();
			foreach( $results as $row ) {
				$arr[$row['productname']][] = array(
									 'loanid'=>$row['loanid']
									 , 'accountid'=>$row['accountid']
									 , 'customerid'=>$row['customerid']
									 , 'name'=>$row['name']
									 , 'productid'=>$row['productid']
									 , 'loanstatus'=>$row['loanstatus']
									 , 'loanstatusdesc'=>$row['loanstatusdesc']
									 , 'approvedamount'=>$row['approvedamount']
									 , 'term'=>$row['term']
									 , 'rate'=>$row['rate']
									 , 'graceperiod'=>$row['graceperiod']									 
									 , 'repaymentamount'=>$row['repaymentamount']
									 , 'disbursedon'=>$row['disbursedon']	
									 , 'mobileno'=>$row['mobileno']									 
									);
			}
		
			if(!empty($arr)){
				echo "<table id=\"report_accountbalance\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"16\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"16\"><strong>LOAN DISBURSEMENT REPORT</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"16\"><strong>From Date ".date("d-m-Y",strtotime($fromdate))." To Date ".date("d-m-Y",strtotime($todate))."</strong></td></tr>";				
				
				$products = array_keys($arr);
				foreach($products as $product){
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"16\" align=\"left\"><strong>Product - ".$product."</strong></td></tr>";	
					echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Loan ID</strong></td><td class=\"payslip\"> <strong>Account ID</strong></td><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"><strong>Customer Name</strong></td><td class=\"payslip\"><strong>Loan Status</strong></td><td class=\"payslip\"><strong>Loan Amount</strong></td><td class=\"payslip\"><strong>Term</strong></td><td class=\"payslip\"><strong>Rate</strong></td><td class=\"payslip\"><strong>Repayment Amount</strong></td><td class=\"payslip\"><strong>Disbursed On</strong></td><td class=\"payslip\"><strong>Mobile No</strong></td></tr>";
						$loancount	=  0;
						$loanamount_Total	=  0;
					
						foreach($arr[$product] as $row => $val){	
							$loancount	=  $loancount+1;
							$loanamount_Total	=  $loanamount_Total+$val['approvedamount'];
				
							echo "<tr class=\"payslip\">";
							echo "<td align=\"left\"  class=\"payslip\">".$val['loanid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['accountid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['loanstatusdesc']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['approvedamount'], true)."</td>";							
							echo "<td align=\"left\"  class=\"payslip\">".$val['term']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$val['rate']."</td>";
							echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($val['repaymentamount'], true)."</td>";							
							echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['disbursedon']))."</td>";	
							echo "<td align=\"left\"  class=\"payslip\">".$val['mobileno']."</td>";							
							echo "</tr>";
						}
					echo "<tr class=\"payslip\"><td class=\"payslip\" colspan=\"3\"></td><td class=\"payslip\"><strong>".$product." </strong></td><td class=\"payslip\"><strong>".$loancount."</strong></td><td class=\"payslip\"><strong>".$this->formatMoney($loanamount_Total, true)."</strong></td><td class=\"payslip\" colspan=\"5\"><strong></strong></td></tr>";			
				}
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}
	
	//M-Pesa Repayments Report
	public function reportMpesaRepayments($fromdate,$todate,$paymentstatus,$paymentprocessing){	
		$str_paymentprocessing = '';
	
		if($paymentprocessing == 1){
			$str_paymentprocessing = 'UPLOAD';
		}else if($paymentprocessing == 2){
			$str_paymentprocessing = 'HTTP';
		}
						
		$rpt_mpesarepayments = $this->dbconnect->prepare("SELECT mpesa_msisdn,mpesa_acc,mpesa_code,mpesa_trx_date,mpesa_sender,mpesa_amt,receiptno,
			CASE processed 
			WHEN 0 THEN 'Not Processed' 
			WHEN 1 THEN 'Pocessed Successfully' 
			WHEN 2 THEN 'Customer Not Found' 
			WHEN 3 THEN 'Invalid Transaction Date' 
			ELSE 'General Failure' END AS processed,
			CASE routemethod_name WHEN 'HTTP' THEN 'IPN' WHEN 'UPLOAD' THEN 'File Upload' END AS paymentprocess,
			filename
			FROM tb_mpesarepayments 
			WHERE (processed = ? OR ? = 99)
			AND (routemethod_name = ? OR  ? = '')
			AND DATE(mpesa_trx_date) BETWEEN ? AND ?					
			ORDER BY mpesa_trx_date	");
			
		$rpt_mpesarepayments->execute(array($paymentstatus,$paymentstatus,$str_paymentprocessing,$str_paymentprocessing,$fromdate,$todate));
		$results = $rpt_mpesarepayments->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
									 'mpesa_msisdn'=>$row['mpesa_msisdn']
									 , 'mpesa_acc'=>$row['mpesa_acc']
									 , 'mpesa_code'=>$row['mpesa_code']
									 , 'mpesa_trx_date'=>$row['mpesa_trx_date']
									 , 'mpesa_sender'=>$row['mpesa_sender']
									 , 'mpesa_amt'=>$row['mpesa_amt']
									, 'receiptno'=>$row['receiptno']
									, 'processed'=>$row['processed']	
									, 'paymentprocess'=>$row['paymentprocess']	
									, 'filename'=>$row['filename']										
									);
			}
			
			if(!empty($arr)){
				$count = 0;
				$totalamount = 0;
				
				echo "<table id=\"report_mpesarepayments\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"10\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"10\"><strong>M-PESA REPAYMENTS REPORT</strong></td></tr>";						
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Mobile No</strong></td><td class=\"payslip\"><strong>M-Pesa Acc</strong></td><td class=\"payslip\"> <strong>M-Pesa Ref No</strong></td><td class=\"payslip\"><strong>Transaction Date</strong></td><td class=\"payslip\"><strong>Name</strong></td><td class=\"payslip\"><strong>Amount</strong></td><td class=\"payslip\"><strong>IMAB Receipt No</strong></td><td class=\"payslip\"><strong>Status</strong></td><td class=\"payslip\"><strong>Payment Processing</strong></td><td class=\"payslip\"><strong>File Name</strong></td></tr>";
				foreach($arr as $arr_row){	
					$count++;
					$totalamount = $totalamount+$arr_row['mpesa_amt'];
					
					echo "<tr class=\"payslip\">";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['mpesa_msisdn']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['mpesa_acc']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['mpesa_code']."</td>";	
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['mpesa_trx_date']."</td>";	
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['mpesa_sender']."</td>";	
					echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($arr_row['mpesa_amt'], true)."</td>";			
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['receiptno']."</td>";	
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['processed']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['paymentprocess']."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['filename']."</td>";					
					echo "</tr>";
				}
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>No of Repayments</strong></td><td class=\"payslip\"> <strong>".$count."</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong>Amount</strong></td><td class=\"payslip\"><strong>".$totalamount."</strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td><td class=\"payslip\"><strong></strong></td></tr>";
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}
	
	//Repayment Schedule
	public function reportrepaymentschedule($loanid){
		$rpt_repaymentschedule = $this->dbconnect->prepare("SELECT period,repaymentdate,principal,interest,balance FROM tb_loanschedule WHERE loanid = ? ORDER BY period 
			");
		
		$rpt_repaymentschedule->execute(array($loanid));
		$results = $rpt_repaymentschedule->fetchAll(PDO::FETCH_ASSOC);
		
		if (is_array($results)){
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
									 'period'=>$row['period']
									 , 'repaymentdate'=>$row['repaymentdate']
									 , 'principal'=>$row['principal']
									 , 'interest'=>$row['interest']
									 , 'balance'=>$row['balance']									
									);
			}
		
			if(!empty($arr)){
				echo "<table id=\"report_repaymentschedule\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"5\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"5\"><strong>LOAN REPAYMENT SCHEDULE</strong></td></tr>";		
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"5\"><strong>LOAN ID : ".$loanid."</strong></td></tr>";				
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Period</strong></td><td class=\"payslip\"> <strong>Repayment Date</strong></td><td class=\"payslip\"><strong>Principal</strong></td><td class=\"payslip\"><strong>Interest</strong></td><td class=\"payslip\"><strong>Balance</strong></td></tr>";
				foreach($arr as $arr_row){						
					echo "<tr class=\"payslip\">";
					echo "<td align=\"left\"  class=\"payslip\">".$arr_row['period']."</td>";					
					echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($arr_row['repaymentdate']))."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($arr_row['principal'], true)."</td>";																			
					echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($arr_row['interest'], true)."</td>";
					echo "<td align=\"left\"  class=\"payslip\">".$this->formatMoney($arr_row['balance'], true)."</td>";
					echo "</tr>";
				}
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}		
	}
	
	//USSD Subscription
	public function reportUSSDSubscription($mobileno,$ussdstatus){
		$rpt_customerregister = $this->dbconnect->prepare("SELECT v_customerdetails.customerid,customernumber,branchname,name,joindate,customertypedesc,loanofficername,idno,idtypedesc,v_customerdetails.mobileno,v_customerdetails.ussdstatus,v_customerdetails.ussdstatusid 
		FROM v_customerdetails INNER JOIN tb_ussdsubscriptions ON tb_ussdsubscriptions.cbscustomerid = v_customerdetails.customerid WHERE (v_customerdetails.ussdstatusid = ? OR ? = 0) AND (v_customerdetails.mobileno = ? OR ? = 0)");
		
		$rpt_customerregister->execute(array($ussdstatus,$ussdstatus,$mobileno,$mobileno));
		$results = $rpt_customerregister->fetchAll(PDO::FETCH_ASSOC);

		if (is_array($results)) {		
			$arr = array();
			foreach( $results as $row ) {
				$arr[] = array(
							 'customerid'=>$row['customerid']
							 , 'customernumber'=>$row['customernumber']
							 , 'branchname'=>$row['branchname']
							 , 'name'=>$row['name']
							 , 'joindate'=>$row['joindate']
							 , 'customertypedesc'=>$row['customertypedesc']
							 , 'loanofficername'=>$row['loanofficername']
							 , 'idno'=>$row['idno']
							 , 'idtypedesc'=>$row['idtypedesc']
							 , 'mobileno'=>$row['mobileno']
							 , 'ussdstatus'=>$row['ussdstatus']
							);
			}

			if(!empty($arr)){
				echo "<table id=\"table_customerregister\" cellpadding=\"2\" cellspacing=\"2\" class=\"payslip\">";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"9\"><strong> <img src=\"".$this->report_logo."\" alt=\"IMAB MIS\"> </strong></td></tr>";//width=\"42\" height=\"42\";
				echo "<tr class=\"payslip\"><td align=\"center\"  class=\"payslip\" colspan=\"9\"><strong>USSD SUBSCRIPTION REPORT</strong></td></tr>";				
				echo "<tr class=\"payslip\"><td class=\"payslip\"><strong>Customer ID</strong></td><td class=\"payslip\"> <strong>Customer #</strong>";
				echo "</td><td class=\"payslip\"><strong>Branch</strong></td><td class=\"payslip\"><strong>Name</strong></td>";
				echo "<td class=\"payslip\"><strong>Join Date</strong></td><td class=\"payslip\"><strong>Customer Type</strong></td>";
				echo "<td class=\"payslip\"><strong>Loan Officer</strong></td><td class=\"payslip\"><strong>ID No</strong></td><td class=\"payslip\"><strong>ID Type</strong></td><td class=\"payslip\"><strong>Mobile No</strong></td><td class=\"payslip\"><strong>USSD Status</strong></td></tr>";
				
				$total = 0;
				
					foreach($arr as $val) {
						$total += 1;
						echo "<tr class=\"payslip\">";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customerid']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customernumber']."</td>";		
						echo "<td align=\"left\"  class=\"payslip\">".$val['branchname']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['name']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".date("d-m-Y",strtotime($val['joindate']))."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['customertypedesc']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['loanofficername']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['idno']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['idtypedesc']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['mobileno']."</td>";
						echo "<td align=\"left\"  class=\"payslip\">".$val['ussdstatus']."</td>";
						echo "</tr>";
					}

					echo "<tr class=\"payslip\"><td class=\"payslip\"></td><td class=\"payslip\"><strong>Total</strong></td><td class=\"payslip\"><strong>".$total."</strong></td><td class=\"payslip\" colspan=\"8\"></td></tr>";					
				echo "</table>";
			}else{
				echo "No data was found!";
			}
		}else{
			echo "No data was found!";
		}
	}	
}

$getreport=new GetReports();
$date = date('Y-m-d');	
	
switch ($action)
{	
	case 1://Balance Sheet Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportBalanceSheet($valuedate,$_GET['summary']);		
	break;	
	case 2://Account Statement
		$customerid = 0;
		$idno = 0;
		$accountno = 0;
		$fromdate = $date;
		$todate = $date;
		$ignorereversal = 0;
		
		if (isset($_GET['customerid'])) {
			$customerid = $_GET['customerid'];
		}
		
		if (isset($_GET['idno'])) {
			$idno = $_GET['idno'];
		}
		
		if (isset($_GET['accountno'])) {
			$accountno = $_GET['accountno'];
		}	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}

		if (isset($_GET['ignorereversal'])) {	
			$ignorereversal = $_GET['ignorereversal'];
		}
		
		$getreport->reportAccountStatement($customerid,$idno,$accountno,$fromdate,$todate,$ignorereversal);		
	break;
	case 3://Ledger Account Statement
		$accountno = 0;
		$fromdate = $date;
		$todate = $date;
		
		if (isset($_GET['accountno'])) {
			$accountno = $_GET['accountno'];
		}	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}
		
		$getreport->reportLedgerAccountStatement($accountno,$fromdate,$todate);		
	break;	
	case 4://Income Statement Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportIncomeStatement($valuedate,$_GET['summary']);		
	break;
	case 5://Trial Balance Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportTrialBalance($valuedate);		
	break;		
	case 6://Customer Register
		$joindate = $date;
		
		if (isset($_GET['joindate'])) {
			$joindate = date("Y-m-d",strtotime($_GET['joindate']));
		}
		
		$getreport->reportCustomerRegister($joindate);		
	break;	
	case 7://Account Balance Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportAccountBalance($valuedate,$_GET['producttype']);		
	break;
	case 8://Loan Portfolio Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportLoanPortfolio($valuedate,$_GET['loanofficerid'],$_GET['branchid'],$_GET['arrearsonly'],$_GET['rlpsummary']);		
	break;	
	case 9://PAR Analysis Report
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportpar($valuedate,$_GET['loanofficerid'],$_GET['branchid'],$_GET['groupby']);		
	break;	
	case 10://Expected Repayments
		$valuedate = $date;
		
		if (isset($_GET['valuedate'])) {
			$valuedate = date("Y-m-d",strtotime($_GET['valuedate']));
		}
		
		$getreport->reportexpectedrepayment($valuedate,$_GET['loanofficerid'],$_GET['branchid'],$_GET['exprepsummary']);		
	break;
	case 11://Repayment Schedule
		$loanid = $date;
		
		if (isset($_GET['loanid'])) {
			$loanid = $_GET['loanid'];
		}
		
		$getreport->reportrepaymentschedule($loanid);		
	break;
	case 12://Loan Disbursement Report
		$fromdate = $date;
		$todate = $date;	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}
		
		$getreport->reportLoanDisbursement($fromdate,$todate,$_GET['loanofficerid'],$_GET['branchid'],$_GET['loanstatus']);		
	break;	
	case 13://M-Pesa Repayment Report
		$fromdate = $date;
		$todate = $date;	

		if (isset($_GET['fromdate'])) {
			$fromdate = date("Y-m-d",strtotime($_GET['fromdate']));
		}
		
		if (isset($_GET['todate'])) {	
			$todate = date("Y-m-d",strtotime($_GET['todate']));
		}
		
		$getreport->reportMpesaRepayments($fromdate,$todate,$_GET['paymentstatus'],$_GET['paymentprocessing']);	 	
	break;
	case 14://USSD Register
		$mobileno = 0;
		
		if (isset($_GET['mobileno'])) {
			$mobileno = $_GET['mobileno'];
		}
		
		if($mobileno == ''){
			$mobileno = 0;
		}
		
		$getreport->reportUSSDSubscription($mobileno,$_GET['ussdstatus']);		
	break;		
	default: //when nothing is passed  getProductType
		echo json_encode(array('retcode'=>$retcode,'retmsg'=>$retmsg,'results'=>$results));
}

?>