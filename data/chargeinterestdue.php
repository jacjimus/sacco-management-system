<?php	
	include('config.inc');

	class ProcessInterest {
		private $dbconnect;
		
	   //Database connect
		public function __construct()
		{
			$db = new DB_Class;
			$this->dbconnect=$db->Myconn();					
		}

		//Post Loan Repayment - When making changed to loan posting also check transfer posting
		public function chargeinterest() {
			#Get loans that has fallen due today
			#DATE_ADD(NOW(),INTERVAL 6 DAY) - Is logic specific to Sun Group
			$sql="SELECT tb_loanschedule.loanid,tb_loans.accountid,tb_loanschedule.repaymentdate,tb_loanschedule.interest,tb_loanschedule.period,tb_loans.productid
			FROM tb_loanschedule 
			INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE repaymentdate = DATE(DATE_ADD(NOW(),INTERVAL 6 DAY)) AND loanstatus = 3";
			
			$loans = array();
			
			if($res=$this->dbconnect->query($sql)){						
				foreach($res as $row) {
					$node=array();	
					$node['loanid']=$row['loanid'];
					$node['accountid']=$row['accountid'];
					$node['interest']=$row['interest'];			
					$node['period']=$row['period'];
					$node['productid']=$row['productid'];
					$node['repaymentdate']=$row['repaymentdate'];						
					array_push($loans, $node);
				}
			}
						
			//loop through loans due to post interest charged
			foreach($loans as $loan){
				$totalinterestdue = 0;
				$totalinterestcharged = 0; 
				$accrueinterest = 0;
			
				//First check if the product allow to accrue interest
				//Check Loan Interest Calculation Method
				$sql_product_settings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
					settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
					FROM v_getloanproducts WHERE productid = ?");
				if($sql_product_settings->execute(array($loan['productid'])))
				{					
					$productsettings = $sql_product_settings->fetchAll(PDO::FETCH_ASSOC);
					
					if(!empty($productsettings)){		
						$product_settings  = json_decode($productsettings[0]["settings"], true);								
						$accrueinterest = $product_settings['accrueinterest'];						
					}
				}
				unset($sql_product_settings);
								
				if($accrueinterest == "1"){
					//Get repayments that has fallen due in order to compare with interest charged in the customer loan account. This must be equal or less than else discard interest accrual				
					$sql_totalinterestdue = "SELECT COALESCE(SUM(interest),0) AS totalinterestdue FROM tb_loanschedule WHERE loanid = ".$loan['loanid']." AND repaymentdate <=  DATE(DATE_ADD(NOW(),INTERVAL 6 DAY))";			
					if($res_totalinterestdue=$this->dbconnect->query($sql_totalinterestdue)){
						if($res_totalinterestdue->rowCount()>0){	
							foreach($res_totalinterestdue as $row_intdue) {					
								$totalinterestdue = $row_intdue['totalinterestdue'];
							}
						}
					}
					
					//Get total interest charged in customer loan account from transactions. 7 is interest charged
					$sql_totalinterestcharged = "SELECT  COALESCE(SUM(amount),0) as totalinterestcharged FROM tb_transactions 
					INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno 
					AND tb_receipts.isreversed = 0  
					WHERE tb_transactions.accountid = ".$loan['accountid']."  AND tb_transactions.transactiontype = 7 AND tb_transactions.valuedate <= DATE(NOW())";			
					if($res_totalinterestcharged=$this->dbconnect->query($sql_totalinterestcharged)){
						if($res_totalinterestcharged->rowCount()>0){	
							foreach($res_totalinterestcharged as $row_intcharged) {					
								$totalinterestcharged = $row_intcharged['totalinterestcharged'];
							}
						}
					}

					$glcontrol = 0;
					$interestreceivable = 0;
					$interestincome = 0;
					
					$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
							INNER JOIN tb_ledgeraccounts
							ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
							WHERE productid =".$loan['productid'];	

					if($res_gllinks=$this->dbconnect->query($sql_gllinks)){
						if($res_gllinks->rowCount()<=0){
							//Do not process this product
						}else{
							foreach($res_gllinks as $row_gllink) {	
								if($row_gllink['gltype'] == 1){
									$glcontrol = $row_gllink['accountid'];	
								}
								if($row_gllink['gltype'] == 9){
									$interestincome = $row_gllink['accountid'];	
								}
								if($row_gllink['gltype'] == 10){
									$interestreceivable = $row_gllink['accountid'];	
								}
							}					
						}
					}
			
					//Check if there is interest to post
					if ($loan['interest'] > 0){
						#If difference between total interest due and total interest charged = interest due today. Post interest due as charged
						if($totalinterestdue > $totalinterestcharged){
							if(($totalinterestdue - $totalinterestcharged) == $loan['interest']){
								//post interest charged
								try
								{			
									$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
									$this->dbconnect->beginTransaction();
									
									$serverdate = date('Y-m-d');
									$receiptno = 0;
									$ledgerid = 0;	
									
									$userid = 1;//Please avoid hardcoding
									$trxdescription = 'Maintenance Fee or Interest - System';
									//This logic is specific to Sun Group
									//$valuedate = $loan['repaymentdate'];
									$valuedate = $serverdate;
									$interestamount = $loan['interest'];
									$accountid = $loan['accountid'];
																		
									$debitaccid = $glcontrol;
									$creditaccid = $interestreceivable;	
									
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
									$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));									
									$receiptno = $this->dbconnect->lastInsertId();	
									unset($res_posttrx);
									//Note hard coded for Sun Group. Divinding interest amount by 2
									//$interestamount_sungroup = $interestamount/2;
									
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
									$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount,7,$debitaccid,$creditaccid));
									unset($res_posttrx);
									
									//$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
									//$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount_sungroup,7,$debitaccid,$creditaccid));
									//unset($res_posttrx);
									
									if($ledgerid == 0){
										$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
										$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
										
										$ledgerid = $this->dbconnect->lastInsertId();
										unset($res_posttrx);										
									}
									
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
									$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
									unset($res_posttrx);
									
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
									$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));	
									unset($res_posttrx);
									
									$this->dbconnect->commit();	
									//Think of logging the results and sending them via e-mail
								} catch (Exception $e) {
									$this->dbconnect->rollBack();
									//$e->getMessage();
									//Log error message
								}							
							}else if(($totalinterestdue - $totalinterestcharged) > $loan['interest']){
								#If difference between total interest due and total interest charged  > interest due today. Loop through loan schedule and post unposted interest due
								#Loop through to post uncharged fee
								$sql_loopthroughloanschedule = "SELECT COALESCE(interest,0) AS interest,period,repaymentdate FROM tb_loanschedule WHERE loanid = ".$loan['loanid']." AND repaymentdate <=  DATE(DATE_ADD(NOW(),INTERVAL 6 DAY)) ORDER BY period";			
								if($res_loopthroughloanschedule = $this->dbconnect->query($sql_loopthroughloanschedule)){
									if($res_loopthroughloanschedule->rowCount()>0){	
										foreach($res_loopthroughloanschedule as $row_loanschedule) {
											$totalinterestdue = 0;
											$totalinterestcharged = 0;
											
											//Get repayments that has fallen due in order to compare with interest charged in the customer loan account. This must be equal or less than else discard interest accrual				
											$sql_totalinterestdue = "SELECT COALESCE(SUM(interest),0) AS totalinterestdue 
											FROM tb_loanschedule 
											WHERE loanid = ".$loan['loanid']." 
											AND repaymentdate <=  DATE('".$row_loanschedule['repaymentdate']."')";	
											
											if($res_totalinterestdue=$this->dbconnect->query($sql_totalinterestdue)){
												if($res_totalinterestdue->rowCount()>0){	
													foreach($res_totalinterestdue as $row_intdue) {					
														$totalinterestdue = $row_intdue['totalinterestdue'];
													}
												}
											}
					
											//Get total interest charged in customer loan account from transactions. 7 is interest charged
											$sql_totalinterestcharged = "SELECT COALESCE(SUM(amount),0) as totalinterestcharged 
											FROM tb_transactions 
											INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno 
											AND tb_receipts.isreversed = 0  
											WHERE tb_transactions.accountid = ".$loan['accountid']."  
											AND tb_transactions.transactiontype = 7 
											AND tb_transactions.valuedate <= DATE('".$row_loanschedule['repaymentdate']."')";	
											
											if($res_totalinterestcharged=$this->dbconnect->query($sql_totalinterestcharged)){
												if($res_totalinterestcharged->rowCount()>0){	
													foreach($res_totalinterestcharged as $row_intcharged) {					
														$totalinterestcharged = $row_intcharged['totalinterestcharged'];
													}
												}
											}
											
											if($totalinterestdue > $totalinterestcharged){
												if(($totalinterestdue - $totalinterestcharged) == $row_loanschedule['interest']){					
													//post interest charged
													try
													{			
														$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
														$this->dbconnect->beginTransaction();
														
														$serverdate = date('Y-m-d');
														$receiptno = 0;
														$ledgerid = 0;	
														
														$userid = 1;//Please avoid hardcoding
														$trxdescription = 'Maintenance Fee or Interest - System';
														//This logic is specific to Sun Group
														//$valuedate = $loan['repaymentdate'];
														//For sungroup use DATE(DATE_ADD(NOW(),INTERVAL 6 DAY))
														$repaymentdate =new DateTime($row_loanschedule['repaymentdate']);
														$repaymentdate->sub(new DateInterval('P6D'));
														
														$valuedate = date_format($repaymentdate, 'Y-m-d');
														$interestamount = $row_loanschedule['interest'];
														$accountid = $loan['accountid'];
																							
														$debitaccid = $glcontrol;
														$creditaccid = $interestreceivable;	
														
														$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
														$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));									
														$receiptno = $this->dbconnect->lastInsertId();	
														unset($res_posttrx);
														//Note hard coded for Sun Group. Divinding interest amount by 2
														$interestamount_sungroup = $interestamount/2;
														
														$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
														$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount_sungroup,7,$debitaccid,$creditaccid));
														
														$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
														$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount_sungroup,7,$debitaccid,$creditaccid));
														
														unset($res_posttrx);
														if($ledgerid == 0){
															$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
															$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
															
															$ledgerid = $this->dbconnect->lastInsertId();
															unset($res_posttrx);										
														}
														
														$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
														$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
														unset($res_posttrx);
														
														$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
														$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));	
														unset($res_posttrx);
														
														$this->dbconnect->commit();	
														//Think of logging the results and sending them via e-mail
													} catch (Exception $e) {
														$this->dbconnect->rollBack();
													}
												}
											}												
										}
									}
								}								
							}
						}					
					}
				}				
			}			
		}

	    #Check for loan that is not current in interest charging
		public function chargeinterest_checks(){
			$sql="SELECT  tb_loans.loanid,tb_loans.accountid,tb_loans.productid,
				(SELECT COALESCE(SUM(interest),0) AS totalinterestdue FROM tb_loanschedule WHERE loanid = tb_loans.loanid AND repaymentdate <=  DATE(NOW())) AS interestdue,

				(SELECT  COALESCE(SUM(amount),0) AS totalinterestcharged FROM tb_transactions 
									INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno 
									AND tb_receipts.isreversed = 0  
									WHERE tb_transactions.accountid = tb_loans.accountid  AND tb_transactions.transactiontype = 7 AND tb_transactions.valuedate <= DATE(NOW())) AS interestcharged

				FROM tb_loans
				WHERE 
				(SELECT COALESCE(SUM(interest),0) AS totalinterestdue FROM tb_loanschedule WHERE loanid = tb_loans.loanid AND repaymentdate <=  DATE(NOW())) > 
				(SELECT  COALESCE(SUM(amount),0) AS totalinterestcharged FROM tb_transactions 
									INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno 
									AND tb_receipts.isreversed = 0  
									WHERE tb_transactions.accountid = tb_loans.accountid  AND tb_transactions.transactiontype = 7 AND tb_transactions.valuedate <= DATE(NOW()))
									
									
				";
			
			$loans = array();
			
			if($res=$this->dbconnect->query($sql)){						
				foreach($res as $row) {
					$node=array();	
					$node['loanid']=$row['loanid'];
					$node['accountid']=$row['accountid'];
					$node['productid']=$row['productid'];
					$node['interestdue']=$row['interestdue'];
					$node['interestcharged']=$row['interestcharged'];			
					
					array_push($loans, $node);
				}
			}
						
			//loop through loans due to post interest charged
			foreach($loans as $loan){
				$totalinterestdue = 0;
				$totalinterestcharged = 0; 
				$accrueinterest = 0;
			
				//First check if the product allow to accrue interest
				//Check Loan Interest Calculation Method
				$sql_product_settings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
					settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
					FROM v_getloanproducts WHERE productid = ?");
				if($sql_product_settings->execute(array($loan['productid'])))
				{					
					$productsettings = $sql_product_settings->fetchAll(PDO::FETCH_ASSOC);
					
					if(!empty($productsettings)){		
						$product_settings  = json_decode($productsettings[0]["settings"], true);								
						$accrueinterest = $product_settings['accrueinterest'];						
					}
				}
				unset($sql_product_settings);
								
				if($accrueinterest == "1"){
					$glcontrol = 0;
					$interestreceivable = 0;
					$interestincome = 0;
					
					$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
							INNER JOIN tb_ledgeraccounts
							ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
							WHERE productid = ".$loan['productid'];	

					if($res_gllinks=$this->dbconnect->query($sql_gllinks)){
						if($res_gllinks->rowCount()<=0){
							//Do not process this product
						}else{
							foreach($res_gllinks as $row_gllink) {	
								if($row_gllink['gltype'] == 1){
									$glcontrol = $row_gllink['accountid'];	
								}
								if($row_gllink['gltype'] == 9){
									$interestincome = $row_gllink['accountid'];	
								}
								if($row_gllink['gltype'] == 10){
									$interestreceivable = $row_gllink['accountid'];	
								}
							}					
						}
					}
				
					$sql_loopthroughloanschedule = "SELECT COALESCE(interest,0) AS interest,period,repaymentdate FROM tb_loanschedule WHERE loanid = ".$loan['loanid']." AND repaymentdate <=  DATE(DATE_ADD(NOW(),INTERVAL 6 DAY)) ORDER BY period";			
					if($res_loopthroughloanschedule = $this->dbconnect->query($sql_loopthroughloanschedule)){
						if($res_loopthroughloanschedule->rowCount()>0){	
							foreach($res_loopthroughloanschedule as $row_loanschedule) {
								$totalinterestdue = 0;
								$totalinterestcharged = 0;
								
								//Get repayments that has fallen due in order to compare with interest charged in the customer loan account. This must be equal or less than else discard interest accrual				
								$sql_totalinterestdue = "SELECT COALESCE(SUM(interest),0) AS totalinterestdue 
								FROM tb_loanschedule 
								WHERE loanid = ".$loan['loanid']." 
								AND repaymentdate <=  DATE('".$row_loanschedule['repaymentdate']."')";	
								
								if($res_totalinterestdue=$this->dbconnect->query($sql_totalinterestdue)){
									if($res_totalinterestdue->rowCount()>0){	
										foreach($res_totalinterestdue as $row_intdue) {					
											$totalinterestdue = $row_intdue['totalinterestdue'];
										}
									}
								}

								//Get total interest charged in customer loan account from transactions. 7 is interest charged
								$sql_totalinterestcharged = "SELECT COALESCE(SUM(amount),0) as totalinterestcharged 
								FROM tb_transactions 
								INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno 
								AND tb_receipts.isreversed = 0  
								WHERE tb_transactions.accountid = ".$loan['accountid']."  
								AND tb_transactions.transactiontype = 7 
								AND tb_transactions.valuedate <= DATE('".$row_loanschedule['repaymentdate']."')";	
								
								if($res_totalinterestcharged=$this->dbconnect->query($sql_totalinterestcharged)){
									if($res_totalinterestcharged->rowCount()>0){	
										foreach($res_totalinterestcharged as $row_intcharged) {					
											$totalinterestcharged = $row_intcharged['totalinterestcharged'];
										}
									}
								}
								
								if($totalinterestdue > $totalinterestcharged){
									if(($totalinterestdue - $totalinterestcharged) == $row_loanschedule['interest']){					
										//post interest charged
										try
										{			
											$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
											$this->dbconnect->beginTransaction();
											
											$serverdate = date('Y-m-d');
											$receiptno = 0;
											$ledgerid = 0;	
											
											$userid = 1;//Please avoid hardcoding
											$trxdescription = 'Maintenance Fee or Interest - System';
											//This logic is specific to Sun Group
											//$valuedate = $loan['repaymentdate'];
											//For sungroup use DATE(DATE_ADD(NOW(),INTERVAL 6 DAY))
											$repaymentdate =new DateTime($row_loanschedule['repaymentdate']);
											$repaymentdate->sub(new DateInterval('P6D'));
											
											$valuedate = date_format($repaymentdate, 'Y-m-d');
											$interestamount = $row_loanschedule['interest'];
											$accountid = $loan['accountid'];
																				
											$debitaccid = $glcontrol;
											$creditaccid = $interestreceivable;	
											
											$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
											$res_posttrx->execute( array($trxdescription,$valuedate,$userid,0));									
											$receiptno = $this->dbconnect->lastInsertId();	
											unset($res_posttrx);
											//Note hard coded for Sun Group. Divinding interest amount by 2
											$interestamount_sungroup = $interestamount/2;
											
											$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
											$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount_sungroup,7,$debitaccid,$creditaccid));
											
											$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
											$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount_sungroup,7,$debitaccid,$creditaccid));
											
											unset($res_posttrx);
											if($ledgerid == 0){
												$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
												$res_posttrx->execute( array(1,1,0,$valuedate,$userid,$receiptno,$trxdescription));
												
												$ledgerid = $this->dbconnect->lastInsertId();
												unset($res_posttrx);										
											}
											
											$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
											$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
											unset($res_posttrx);
											
											$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
											$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));	
											unset($res_posttrx);
											
											$this->dbconnect->commit();	
											//Think of logging the results and sending them via e-mail
										} catch (Exception $e) {
											$this->dbconnect->rollBack();
										}
									}
								}												
							}
						}
					}
				}
			}
		}		
	}
	//Instantiate the class
	$processinterest = new ProcessInterest();
	
	//Process Interest
	$processinterest->chargeinterest();
	
	//Check interest due but not charged
	$processinterest->chargeinterest_checks();
?>