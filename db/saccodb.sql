/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : saccodb

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-03-15 09:52:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_accounts
-- ----------------------------
DROP TABLE IF EXISTS `tb_accounts`;
CREATE TABLE `tb_accounts` (
  `accountid` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountnumber` varchar(50) DEFAULT NULL,
  `customerid` bigint(20) NOT NULL,
  `productid` int(11) NOT NULL,
  `drAmount` double(18,2) DEFAULT '0.00',
  `crAmount` double(18,2) DEFAULT '0.00',
  `accountstatus` int(11) DEFAULT NULL,
  `balancedate` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `creationdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountid`),
  KEY `FK_tb_products_tb_accounts` (`productid`),
  KEY `FK_tb_customer_tb_accounts` (`customerid`),
  CONSTRAINT `FK_tb_customer_tb_accounts` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`),
  CONSTRAINT `FK_tb_products_tb_accounts` FOREIGN KEY (`productid`) REFERENCES `tb_products` (`productid`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_accounts
-- ----------------------------
INSERT INTO `tb_accounts` VALUES ('10', 'KAG273N', '1', '1', '0.00', '191300.00', '1', '2016-12-08 00:00:00', '1', '2016-12-08 00:00:00', '2016-12-08 14:36:30');
INSERT INTO `tb_accounts` VALUES ('11', 'KAR463J', '1', '1', '0.00', '92300.00', '1', '2016-12-08 00:00:00', '1', '2016-12-08 00:00:00', '2016-12-08 15:06:01');
INSERT INTO `tb_accounts` VALUES ('12', 'KAR041D', '3', '1', '0.00', '0.00', '1', '2016-12-08 00:00:00', '1', '2016-12-08 00:00:00', '2016-12-08 15:11:26');
INSERT INTO `tb_accounts` VALUES ('13', 'KBA013A', '5', '1', '0.00', '900.00', '1', '2016-12-08 00:00:00', '1', '2016-12-08 00:00:00', '2016-12-08 15:16:02');
INSERT INTO `tb_accounts` VALUES ('14', 'KAU183U', '6', '1', '0.00', '129800.00', '1', '2016-12-08 00:00:00', '1', '2016-12-08 00:00:00', '2016-12-08 17:04:47');
INSERT INTO `tb_accounts` VALUES ('15', 'KAQ621P', '14', '1', '0.00', '63600.00', '1', '2016-12-19 00:00:00', '1', '2016-12-19 00:00:00', '2016-12-19 13:42:34');
INSERT INTO `tb_accounts` VALUES ('18', 'KAQ682D', '4', '1', '0.00', '89900.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 12:50:18');
INSERT INTO `tb_accounts` VALUES ('19', 'KAV641R', '7', '1', '0.00', '107000.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 12:55:38');
INSERT INTO `tb_accounts` VALUES ('20', 'KAP347Y', '9', '1', '0.00', '65000.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 12:59:56');
INSERT INTO `tb_accounts` VALUES ('21', 'KAR877C', '10', '1', '0.00', '74600.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:01:05');
INSERT INTO `tb_accounts` VALUES ('22', 'KAP114S', '15', '1', '0.00', '52300.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:02:33');
INSERT INTO `tb_accounts` VALUES ('23', 'KBA327K', '20', '1', '0.00', '19400.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:04:07');
INSERT INTO `tb_accounts` VALUES ('24', 'KAX425W', '11', '1', '0.00', '31200.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:05:09');
INSERT INTO `tb_accounts` VALUES ('25', 'KAS103C', '21', '1', '0.00', '21500.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:06:44');
INSERT INTO `tb_accounts` VALUES ('26', 'KAZ789Q', '22', '1', '0.00', '1000.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:08:47');
INSERT INTO `tb_accounts` VALUES ('27', 'KAQ418M', '23', '1', '0.00', '162100.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:09:47');
INSERT INTO `tb_accounts` VALUES ('28', 'KAP347J', '26', '1', '0.00', '17000.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:11:36');
INSERT INTO `tb_accounts` VALUES ('29', 'KAP003B', '27', '1', '0.00', '200.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:13:40');
INSERT INTO `tb_accounts` VALUES ('30', 'KBB275K', '29', '1', '0.00', '200.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:14:19');
INSERT INTO `tb_accounts` VALUES ('31', 'KBW019X', '30', '1', '0.00', '200.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:15:10');
INSERT INTO `tb_accounts` VALUES ('32', 'KAT106K', '35', '1', '0.00', '225200.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:16:31');
INSERT INTO `tb_accounts` VALUES ('33', 'KBA354G', '31', '1', '0.00', '5000.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:17:39');
INSERT INTO `tb_accounts` VALUES ('34', 'KAS304T', '32', '1', '0.00', '2400.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:18:30');
INSERT INTO `tb_accounts` VALUES ('35', 'KAS213R', '33', '1', '0.00', '101500.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:24:51');
INSERT INTO `tb_accounts` VALUES ('36', 'KCC829K', '34', '1', '0.00', '9800.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:25:43');
INSERT INTO `tb_accounts` VALUES ('37', 'KBJ336H', '42', '1', '0.00', '4600.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:55:18');
INSERT INTO `tb_accounts` VALUES ('38', 'KBL686D', '43', '1', '0.00', '7800.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:56:43');
INSERT INTO `tb_accounts` VALUES ('39', 'KAW920A', '44', '1', '0.00', '0.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:57:19');
INSERT INTO `tb_accounts` VALUES ('40', 'KAW150L', '13', '1', '0.00', '0.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:58:54');
INSERT INTO `tb_accounts` VALUES ('41', 'KAN154L', '12', '1', '0.00', '0.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:59:12');
INSERT INTO `tb_accounts` VALUES ('42', 'KAS067N', '8', '1', '0.00', '0.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 13:59:44');
INSERT INTO `tb_accounts` VALUES ('43', 'NONE', '16', '1', '0.00', '90900.00', '1', '2016-12-22 00:00:00', '1', '2016-12-22 00:00:00', '2016-12-22 14:08:50');
INSERT INTO `tb_accounts` VALUES ('44', 'KAS537S', '2', '1', '0.00', '0.00', '1', '2016-12-29 00:00:00', '1', '2016-12-29 00:00:00', '2016-12-29 14:05:41');
INSERT INTO `tb_accounts` VALUES ('45', 'KAR486W', '2', '1', '0.00', '438000.00', '1', '2016-12-29 00:00:00', '1', '2016-12-29 00:00:00', '2016-12-29 14:07:08');
INSERT INTO `tb_accounts` VALUES ('46', '29649919', '37', '1', '0.00', '20500.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:05:26');
INSERT INTO `tb_accounts` VALUES ('47', '25453919', '17', '1', '0.00', '30800.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:16:43');
INSERT INTO `tb_accounts` VALUES ('48', '22274786', '18', '1', '0.00', '96700.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:18:40');
INSERT INTO `tb_accounts` VALUES ('49', '23775457', '19', '1', '0.00', '95000.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:19:21');
INSERT INTO `tb_accounts` VALUES ('50', '6485831', '25', '1', '0.00', '13000.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:20:04');
INSERT INTO `tb_accounts` VALUES ('51', '24630387', '36', '1', '0.00', '170000.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:21:41');
INSERT INTO `tb_accounts` VALUES ('52', '25149222', '40', '1', '0.00', '82400.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:22:58');
INSERT INTO `tb_accounts` VALUES ('53', '13827975', '39', '1', '0.00', '2300.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:24:16');
INSERT INTO `tb_accounts` VALUES ('54', '24759038', '41', '1', '0.00', '0.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:25:12');
INSERT INTO `tb_accounts` VALUES ('55', '11406754', '24', '1', '0.00', '15000.00', '1', '2017-01-02 00:00:00', '1', '2017-01-02 00:00:00', '2017-01-02 11:28:14');
INSERT INTO `tb_accounts` VALUES ('56', '25072176', '28', '1', '0.00', '5600.00', '1', '2017-01-05 00:00:00', '1', '2017-01-05 00:00:00', '2017-01-05 15:21:35');
INSERT INTO `tb_accounts` VALUES ('57', 'LN-1-4', '1', '4', '100000.00', '0.00', '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', '2017-02-05 17:53:07');
INSERT INTO `tb_accounts` VALUES ('58', 'LN-1-4', '1', '4', '90000.00', '0.00', '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', '2017-02-05 17:56:17');
INSERT INTO `tb_accounts` VALUES ('59', 'LN-23-41109f526c8fcb2fd0ec9c1f938cc90f', '2', '3', '40000.00', '0.00', '1', '2017-02-05 00:00:00', '1', '2017-02-05 00:00:00', '2017-02-05 18:03:37');
INSERT INTO `tb_accounts` VALUES ('60', '00000060', '1', '3', '20000.00', '8000.00', '1', '2017-02-05 00:00:00', '1', '2017-02-05 00:00:00', '2017-02-05 18:20:26');
INSERT INTO `tb_accounts` VALUES ('61', '00000061', '1', '4', '90000.00', '0.00', '1', '2017-02-05 00:00:00', '1', '2017-02-05 00:00:00', '2017-02-05 18:21:20');
INSERT INTO `tb_accounts` VALUES ('62', 'LN-54-D168BBDBD83327EB0181241E710', '5', '4', '20000.00', '0.00', '1', '2017-02-06 00:00:00', '1', '2017-02-06 00:00:00', '2017-02-06 15:38:12');
INSERT INTO `tb_accounts` VALUES ('63', '00000063', '5', '4', '20000.00', '0.00', '1', '2017-02-06 00:00:00', '1', '2017-02-06 00:00:00', '2017-02-06 15:41:22');
INSERT INTO `tb_accounts` VALUES ('64', 'LN-43-63291C7F7B2A52B543A23652E0A', '4', '3', '30000.00', '0.00', '1', '2017-02-06 00:00:00', '1', '2017-02-06 00:00:00', '2017-02-06 15:58:19');
INSERT INTO `tb_accounts` VALUES ('65', '00000065', '4', '3', '30000.00', '0.00', '1', '2017-02-06 00:00:00', '1', '2017-02-06 00:00:00', '2017-02-06 16:01:57');
INSERT INTO `tb_accounts` VALUES ('66', 'LN-34-3B055DAAF7AFC37D8AC4D51BDAE', '3', '4', '1000.00', '0.00', '1', '2017-02-06 00:00:00', '1', '2017-02-06 00:00:00', '2017-02-06 16:26:28');

-- ----------------------------
-- Table structure for tb_address
-- ----------------------------
DROP TABLE IF EXISTS `tb_address`;
CREATE TABLE `tb_address` (
  `addressid` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) DEFAULT NULL,
  `town` int(11) DEFAULT NULL,
  `county` int(200) DEFAULT NULL,
  `province` varchar(200) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`addressid`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_address
-- ----------------------------
INSERT INTO `tb_address` VALUES ('1', '', '1', '1', '', '1', '2016-11-29 00:00:00', '2016-12-07 00:00:00', '2016-11-29 15:29:13');
INSERT INTO `tb_address` VALUES ('2', '', '1', '1', '', '1', '2016-11-29 00:00:00', '2016-11-29 00:00:00', '2016-11-29 15:34:04');
INSERT INTO `tb_address` VALUES ('3', '', '1', '1', '', '1', '2016-11-29 00:00:00', null, '2016-11-29 15:54:44');
INSERT INTO `tb_address` VALUES ('4', '', '1', '1', '', '1', '2016-11-29 00:00:00', null, '2016-11-29 16:01:44');
INSERT INTO `tb_address` VALUES ('5', '', '1', '1', '', '1', '2016-11-29 00:00:00', null, '2016-11-29 16:06:33');
INSERT INTO `tb_address` VALUES ('6', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:16:34');
INSERT INTO `tb_address` VALUES ('7', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:29:19');
INSERT INTO `tb_address` VALUES ('8', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:30:58');
INSERT INTO `tb_address` VALUES ('9', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:32:16');
INSERT INTO `tb_address` VALUES ('10', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:33:48');
INSERT INTO `tb_address` VALUES ('11', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:34:48');
INSERT INTO `tb_address` VALUES ('12', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:50:06');
INSERT INTO `tb_address` VALUES ('13', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:51:30');
INSERT INTO `tb_address` VALUES ('14', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:52:30');
INSERT INTO `tb_address` VALUES ('15', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 13:54:11');
INSERT INTO `tb_address` VALUES ('16', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 14:13:37');
INSERT INTO `tb_address` VALUES ('17', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 14:22:24');
INSERT INTO `tb_address` VALUES ('18', '', '1', '1', '', '1', '2016-12-08 00:00:00', null, '2016-12-08 14:24:59');
INSERT INTO `tb_address` VALUES ('19', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:41:12');
INSERT INTO `tb_address` VALUES ('20', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:43:32');
INSERT INTO `tb_address` VALUES ('21', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:44:49');
INSERT INTO `tb_address` VALUES ('22', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:45:57');
INSERT INTO `tb_address` VALUES ('23', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:55:07');
INSERT INTO `tb_address` VALUES ('24', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:58:22');
INSERT INTO `tb_address` VALUES ('25', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:06:33');
INSERT INTO `tb_address` VALUES ('26', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:07:43');
INSERT INTO `tb_address` VALUES ('27', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:08:58');
INSERT INTO `tb_address` VALUES ('28', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:10:17');
INSERT INTO `tb_address` VALUES ('29', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:11:21');
INSERT INTO `tb_address` VALUES ('30', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:13:13');
INSERT INTO `tb_address` VALUES ('31', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:16:27');
INSERT INTO `tb_address` VALUES ('32', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:19:29');
INSERT INTO `tb_address` VALUES ('33', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:20:40');
INSERT INTO `tb_address` VALUES ('34', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:21:41');
INSERT INTO `tb_address` VALUES ('35', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:23:22');
INSERT INTO `tb_address` VALUES ('36', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:24:52');
INSERT INTO `tb_address` VALUES ('37', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:26:26');
INSERT INTO `tb_address` VALUES ('38', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:27:46');
INSERT INTO `tb_address` VALUES ('39', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:30:20');
INSERT INTO `tb_address` VALUES ('40', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:31:41');
INSERT INTO `tb_address` VALUES ('41', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:35:03');
INSERT INTO `tb_address` VALUES ('42', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:35:52');
INSERT INTO `tb_address` VALUES ('43', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:37:02');
INSERT INTO `tb_address` VALUES ('44', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:38:28');
INSERT INTO `tb_address` VALUES ('45', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:39:56');
INSERT INTO `tb_address` VALUES ('46', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:41:06');
INSERT INTO `tb_address` VALUES ('47', '', '1', '1', '', '1', '2016-12-14 00:00:00', null, '2016-12-14 16:16:55');
INSERT INTO `tb_address` VALUES ('48', '', '1', '1', '', '1', '2016-12-19 00:00:00', null, '2016-12-19 10:01:56');
INSERT INTO `tb_address` VALUES ('49', '', '1', '1', '', '1', '2016-12-19 00:00:00', null, '2016-12-19 10:03:42');

-- ----------------------------
-- Table structure for tb_branches
-- ----------------------------
DROP TABLE IF EXISTS `tb_branches`;
CREATE TABLE `tb_branches` (
  `branchid` int(11) NOT NULL AUTO_INCREMENT,
  `branchname` varchar(200) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `addressid` varchar(50) DEFAULT NULL,
  `phoneno` bigint(12) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`branchid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_branches
-- ----------------------------
INSERT INTO `tb_branches` VALUES ('1', 'KISERIAN BRANCH', '001', '1', 'P.O Box 162-00206 Nairobi', '254728324866', '2015-09-15 05:10:22', '2013-10-13 13:37:31');

-- ----------------------------
-- Table structure for tb_campaigns
-- ----------------------------
DROP TABLE IF EXISTS `tb_campaigns`;
CREATE TABLE `tb_campaigns` (
  `campaignid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `templateid` int(11) DEFAULT NULL,
  `ruleid` int(11) DEFAULT NULL,
  `scheduleid` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `createdby` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  PRIMARY KEY (`campaignid`),
  UNIQUE KEY `description` (`description`),
  KEY `templateid` (`templateid`),
  KEY `ruleid` (`ruleid`),
  KEY `scheduleid` (`scheduleid`),
  CONSTRAINT `tb_campaigns_ibfk_1` FOREIGN KEY (`templateid`) REFERENCES `tb_templates` (`templateid`),
  CONSTRAINT `tb_campaigns_ibfk_2` FOREIGN KEY (`ruleid`) REFERENCES `tb_rules` (`ruleid`),
  CONSTRAINT `tb_campaigns_ibfk_3` FOREIGN KEY (`scheduleid`) REFERENCES `tb_schedules` (`scheduleid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_campaigns
-- ----------------------------
INSERT INTO `tb_campaigns` VALUES ('1', 'MEETING', '1', '1', '2', '1', null, null);

-- ----------------------------
-- Table structure for tb_contacts
-- ----------------------------
DROP TABLE IF EXISTS `tb_contacts`;
CREATE TABLE `tb_contacts` (
  `contactid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `phoneno` bigint(12) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ruleid` int(11) NOT NULL,
  `contacttypeid` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`contactid`),
  KEY `FK_tb_contacts_tb_contacttype` (`contacttypeid`),
  KEY `FK_tb_contacts_tb_rules` (`ruleid`),
  CONSTRAINT `FK_tb_contacts_tb_contacttype` FOREIGN KEY (`contacttypeid`) REFERENCES `tb_contacttype` (`contacttypeid`),
  CONSTRAINT `FK_tb_contacts_tb_rules` FOREIGN KEY (`ruleid`) REFERENCES `tb_rules` (`ruleid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_contacts
-- ----------------------------
INSERT INTO `tb_contacts` VALUES ('1', 'Andrew Waweru', '254721557528', '', '1', '1', '1');
INSERT INTO `tb_contacts` VALUES ('2', 'Tested', '254723872455', '', '1', '1', '1');

-- ----------------------------
-- Table structure for tb_contacttype
-- ----------------------------
DROP TABLE IF EXISTS `tb_contacttype`;
CREATE TABLE `tb_contacttype` (
  `contacttypeid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`contacttypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_contacttype
-- ----------------------------
INSERT INTO `tb_contacttype` VALUES ('1', 'Mobile Number', '1');
INSERT INTO `tb_contacttype` VALUES ('2', 'Email', '1');

-- ----------------------------
-- Table structure for tb_currency
-- ----------------------------
DROP TABLE IF EXISTS `tb_currency`;
CREATE TABLE `tb_currency` (
  `currencyid` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `symbol` varchar(200) DEFAULT NULL,
  `rounding` tinyint(1) DEFAULT NULL,
  `Homecurrency` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_currency
-- ----------------------------
INSERT INTO `tb_currency` VALUES ('1', 'KSH', 'Kenyan Shilling', 'K', '3', '1', '1', '1970-01-01 00:00:00', '2014-02-28 08:05:42');

-- ----------------------------
-- Table structure for tb_customer
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer`;
CREATE TABLE `tb_customer` (
  `customerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(30) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `branchid` int(11) DEFAULT NULL,
  `joindate` datetime DEFAULT NULL,
  `customertype` int(11) DEFAULT NULL,
  `loanofficerid` bigint(20) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerid`),
  KEY `FK_branches_person` (`branchid`),
  KEY `FK_loanofficers_person` (`loanofficerid`),
  CONSTRAINT `FK_branches_person` FOREIGN KEY (`branchid`) REFERENCES `tb_branches` (`branchid`),
  CONSTRAINT `FK_loanofficers_person` FOREIGN KEY (`loanofficerid`) REFERENCES `tb_loanofficers` (`loanofficerid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customer
-- ----------------------------
INSERT INTO `tb_customer` VALUES ('1', '1', 'KIBUTHA, JOHN KARANJA', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:16:34');
INSERT INTO `tb_customer` VALUES ('2', '2', 'NJOROGE, SIMON MWANGI', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:29:19');
INSERT INTO `tb_customer` VALUES ('3', '3', 'MBUGUA, STEPHEN ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:30:58');
INSERT INTO `tb_customer` VALUES ('4', '4', 'MWAURA, PETER ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:32:16');
INSERT INTO `tb_customer` VALUES ('5', '5', 'WAITA, AMBROSE ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:33:48');
INSERT INTO `tb_customer` VALUES ('6', '6', 'GACHERU, EZEKIEL ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:34:48');
INSERT INTO `tb_customer` VALUES ('7', '7', 'NJUGUNA, ROBERT ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:50:06');
INSERT INTO `tb_customer` VALUES ('8', '8', 'KAMAU, STEPHEN ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:51:30');
INSERT INTO `tb_customer` VALUES ('9', '9', 'IRUNGU, EDWARD ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:52:30');
INSERT INTO `tb_customer` VALUES ('10', '10', 'GACHIRA, JOHN ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 13:54:11');
INSERT INTO `tb_customer` VALUES ('11', '11', 'MBUGUA, PETER ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 14:13:37');
INSERT INTO `tb_customer` VALUES ('12', '12', 'MURIMA, SIMON ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 14:22:24');
INSERT INTO `tb_customer` VALUES ('13', '13', 'MUTUKU, ANTONY NZWILI', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-07 00:00:00', null, '2016-12-08 14:24:59');
INSERT INTO `tb_customer` VALUES ('14', '14', 'MALOI, GEDION ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:41:12');
INSERT INTO `tb_customer` VALUES ('15', '15', 'KIRAMBA, SAMMY ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:43:32');
INSERT INTO `tb_customer` VALUES ('16', '16', 'NDERITU, SALOME ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:44:49');
INSERT INTO `tb_customer` VALUES ('17', '17', 'MUGO, GERALD ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:45:57');
INSERT INTO `tb_customer` VALUES ('18', '18', 'PATRICK, KINUTHIA ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:55:07');
INSERT INTO `tb_customer` VALUES ('19', '19', 'WAWERU, SAMUEL ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 14:58:22');
INSERT INTO `tb_customer` VALUES ('20', '20', 'MUNAI, OLE ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:06:33');
INSERT INTO `tb_customer` VALUES ('21', '21', 'WAMUGI, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:07:43');
INSERT INTO `tb_customer` VALUES ('22', '22', 'MOKUA, KENNEDY ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:08:58');
INSERT INTO `tb_customer` VALUES ('23', '23', 'NJAGI, ANTONY ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:10:17');
INSERT INTO `tb_customer` VALUES ('24', '24', 'NDUNGU, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:11:21');
INSERT INTO `tb_customer` VALUES ('25', '25', 'WAITHERA, NANCY ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:13:13');
INSERT INTO `tb_customer` VALUES ('26', '26', 'NJUGUNA, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:16:27');
INSERT INTO `tb_customer` VALUES ('27', '27', 'NUNGARI, SUSAN ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:19:29');
INSERT INTO `tb_customer` VALUES ('28', '28', 'MUINDI, NICHOLUS ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:20:40');
INSERT INTO `tb_customer` VALUES ('29', '29', 'OTARA, ZABLON ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:21:41');
INSERT INTO `tb_customer` VALUES ('30', '30', 'MWANGI, SIMON ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:23:22');
INSERT INTO `tb_customer` VALUES ('31', '31', 'WANJIRU, JOYCE ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:24:52');
INSERT INTO `tb_customer` VALUES ('32', '32', 'MOSES, MARTIN ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:26:26');
INSERT INTO `tb_customer` VALUES ('33', '33', 'KARIUKI, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:27:46');
INSERT INTO `tb_customer` VALUES ('34', '34', 'KAPITA, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:30:20');
INSERT INTO `tb_customer` VALUES ('35', '35', 'NJOROGE, SAMUEL ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:31:41');
INSERT INTO `tb_customer` VALUES ('36', '36', 'NYAMBURA, SARAH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:35:03');
INSERT INTO `tb_customer` VALUES ('37', '37', 'MUTINDA, CAROLINE ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:35:52');
INSERT INTO `tb_customer` VALUES ('38', '38', 'NJATHI, SAMUEL ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:37:02');
INSERT INTO `tb_customer` VALUES ('39', '39', 'MAINA, WILSON ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:38:28');
INSERT INTO `tb_customer` VALUES ('40', '40', 'MWANGI, DAVID ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:39:56');
INSERT INTO `tb_customer` VALUES ('41', '41', 'KARUGA, JAMES ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 15:41:06');
INSERT INTO `tb_customer` VALUES ('42', '42', 'NJONGE, FRANCIS ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-14 00:00:00', null, '2016-12-14 16:16:55');
INSERT INTO `tb_customer` VALUES ('43', '43', 'EZEKIEL, SAITOTI ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-19 00:00:00', null, '2016-12-19 10:01:56');
INSERT INTO `tb_customer` VALUES ('44', '44', 'MUCHIRI, JOSEPH ', '1', '2016-12-07 00:00:00', '1', '1', '2016-12-19 00:00:00', null, '2016-12-19 10:03:42');

-- ----------------------------
-- Table structure for tb_customeraddress
-- ----------------------------
DROP TABLE IF EXISTS `tb_customeraddress`;
CREATE TABLE `tb_customeraddress` (
  `customeraddressid` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` bigint(20) NOT NULL,
  `addressid` bigint(20) NOT NULL,
  `addresstype` int(11) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customeraddressid`),
  KEY `FK_tb_customeraddress_tb_customer` (`customerid`),
  KEY `FK_tb_customeraddress_tb_address` (`addressid`),
  CONSTRAINT `FK_tb_customeraddress_tb_address` FOREIGN KEY (`addressid`) REFERENCES `tb_address` (`addressid`),
  CONSTRAINT `FK_tb_customeraddress_tb_customer` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customeraddress
-- ----------------------------
INSERT INTO `tb_customeraddress` VALUES ('1', '1', '6', '1', '2016-12-08 00:00:00', '2016-12-08 13:16:34');
INSERT INTO `tb_customeraddress` VALUES ('2', '2', '7', '1', '2016-12-08 00:00:00', '2016-12-08 13:29:19');
INSERT INTO `tb_customeraddress` VALUES ('3', '3', '8', '1', '2016-12-08 00:00:00', '2016-12-08 13:30:58');
INSERT INTO `tb_customeraddress` VALUES ('4', '4', '9', '1', '2016-12-08 00:00:00', '2016-12-08 13:32:16');
INSERT INTO `tb_customeraddress` VALUES ('5', '5', '10', '1', '2016-12-08 00:00:00', '2016-12-08 13:33:48');
INSERT INTO `tb_customeraddress` VALUES ('6', '6', '11', '1', '2016-12-08 00:00:00', '2016-12-08 13:34:48');
INSERT INTO `tb_customeraddress` VALUES ('7', '7', '12', '1', '2016-12-08 00:00:00', '2016-12-08 13:50:06');
INSERT INTO `tb_customeraddress` VALUES ('8', '8', '13', '1', '2016-12-08 00:00:00', '2016-12-08 13:51:30');
INSERT INTO `tb_customeraddress` VALUES ('9', '9', '14', '1', '2016-12-08 00:00:00', '2016-12-08 13:52:30');
INSERT INTO `tb_customeraddress` VALUES ('10', '10', '15', '1', '2016-12-08 00:00:00', '2016-12-08 13:54:11');
INSERT INTO `tb_customeraddress` VALUES ('11', '11', '16', '1', '2016-12-08 00:00:00', '2016-12-08 14:13:37');
INSERT INTO `tb_customeraddress` VALUES ('12', '12', '17', '1', '2016-12-08 00:00:00', '2016-12-08 14:22:24');
INSERT INTO `tb_customeraddress` VALUES ('13', '13', '18', '1', '2016-12-08 00:00:00', '2016-12-08 14:24:59');
INSERT INTO `tb_customeraddress` VALUES ('14', '14', '19', '1', '2016-12-14 00:00:00', '2016-12-14 14:41:12');
INSERT INTO `tb_customeraddress` VALUES ('15', '15', '20', '1', '2016-12-14 00:00:00', '2016-12-14 14:43:32');
INSERT INTO `tb_customeraddress` VALUES ('16', '16', '21', '1', '2016-12-14 00:00:00', '2016-12-14 14:44:49');
INSERT INTO `tb_customeraddress` VALUES ('17', '17', '22', '1', '2016-12-14 00:00:00', '2016-12-14 14:45:57');
INSERT INTO `tb_customeraddress` VALUES ('18', '18', '23', '1', '2016-12-14 00:00:00', '2016-12-14 14:55:07');
INSERT INTO `tb_customeraddress` VALUES ('19', '19', '24', '1', '2016-12-14 00:00:00', '2016-12-14 14:58:22');
INSERT INTO `tb_customeraddress` VALUES ('20', '20', '25', '1', '2016-12-14 00:00:00', '2016-12-14 15:06:33');
INSERT INTO `tb_customeraddress` VALUES ('21', '21', '26', '1', '2016-12-14 00:00:00', '2016-12-14 15:07:43');
INSERT INTO `tb_customeraddress` VALUES ('22', '22', '27', '1', '2016-12-14 00:00:00', '2016-12-14 15:08:58');
INSERT INTO `tb_customeraddress` VALUES ('23', '23', '28', '1', '2016-12-14 00:00:00', '2016-12-14 15:10:17');
INSERT INTO `tb_customeraddress` VALUES ('24', '24', '29', '1', '2016-12-14 00:00:00', '2016-12-14 15:11:21');
INSERT INTO `tb_customeraddress` VALUES ('25', '25', '30', '1', '2016-12-14 00:00:00', '2016-12-14 15:13:13');
INSERT INTO `tb_customeraddress` VALUES ('26', '26', '31', '1', '2016-12-14 00:00:00', '2016-12-14 15:16:27');
INSERT INTO `tb_customeraddress` VALUES ('27', '27', '32', '1', '2016-12-14 00:00:00', '2016-12-14 15:19:29');
INSERT INTO `tb_customeraddress` VALUES ('28', '28', '33', '1', '2016-12-14 00:00:00', '2016-12-14 15:20:40');
INSERT INTO `tb_customeraddress` VALUES ('29', '29', '34', '1', '2016-12-14 00:00:00', '2016-12-14 15:21:41');
INSERT INTO `tb_customeraddress` VALUES ('30', '30', '35', '1', '2016-12-14 00:00:00', '2016-12-14 15:23:22');
INSERT INTO `tb_customeraddress` VALUES ('31', '31', '36', '1', '2016-12-14 00:00:00', '2016-12-14 15:24:52');
INSERT INTO `tb_customeraddress` VALUES ('32', '32', '37', '1', '2016-12-14 00:00:00', '2016-12-14 15:26:26');
INSERT INTO `tb_customeraddress` VALUES ('33', '33', '38', '1', '2016-12-14 00:00:00', '2016-12-14 15:27:46');
INSERT INTO `tb_customeraddress` VALUES ('34', '34', '39', '1', '2016-12-14 00:00:00', '2016-12-14 15:30:20');
INSERT INTO `tb_customeraddress` VALUES ('35', '35', '40', '1', '2016-12-14 00:00:00', '2016-12-14 15:31:41');
INSERT INTO `tb_customeraddress` VALUES ('36', '36', '41', '1', '2016-12-14 00:00:00', '2016-12-14 15:35:03');
INSERT INTO `tb_customeraddress` VALUES ('37', '37', '42', '1', '2016-12-14 00:00:00', '2016-12-14 15:35:52');
INSERT INTO `tb_customeraddress` VALUES ('38', '38', '43', '1', '2016-12-14 00:00:00', '2016-12-14 15:37:02');
INSERT INTO `tb_customeraddress` VALUES ('39', '39', '44', '1', '2016-12-14 00:00:00', '2016-12-14 15:38:28');
INSERT INTO `tb_customeraddress` VALUES ('40', '40', '45', '1', '2016-12-14 00:00:00', '2016-12-14 15:39:56');
INSERT INTO `tb_customeraddress` VALUES ('41', '41', '46', '1', '2016-12-14 00:00:00', '2016-12-14 15:41:06');
INSERT INTO `tb_customeraddress` VALUES ('42', '42', '47', '1', '2016-12-14 00:00:00', '2016-12-14 16:16:55');
INSERT INTO `tb_customeraddress` VALUES ('43', '43', '48', '1', '2016-12-19 00:00:00', '2016-12-19 10:01:56');
INSERT INTO `tb_customeraddress` VALUES ('44', '44', '49', '1', '2016-12-19 00:00:00', '2016-12-19 10:03:42');

-- ----------------------------
-- Table structure for tb_customercommunity
-- ----------------------------
DROP TABLE IF EXISTS `tb_customercommunity`;
CREATE TABLE `tb_customercommunity` (
  `communityid` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerid` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `statusdate` datetime DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`communityid`),
  KEY `FK_customercommunity_customer` (`customerid`),
  CONSTRAINT `FK_customercommunity_customer` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customercommunity
-- ----------------------------

-- ----------------------------
-- Table structure for tb_customerperson
-- ----------------------------
DROP TABLE IF EXISTS `tb_customerperson`;
CREATE TABLE `tb_customerperson` (
  `customerpersonid` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerid` bigint(20) DEFAULT NULL,
  `personid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`customerpersonid`),
  KEY `FK_customerperson_customer` (`customerid`),
  KEY `FK_customerperson` (`personid`),
  CONSTRAINT `FK_customerperson` FOREIGN KEY (`personid`) REFERENCES `tb_person` (`personid`),
  CONSTRAINT `FK_customerperson_customer` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customerperson
-- ----------------------------
INSERT INTO `tb_customerperson` VALUES ('1', '1', '1');
INSERT INTO `tb_customerperson` VALUES ('2', '2', '2');
INSERT INTO `tb_customerperson` VALUES ('3', '3', '3');
INSERT INTO `tb_customerperson` VALUES ('4', '4', '4');
INSERT INTO `tb_customerperson` VALUES ('5', '5', '5');
INSERT INTO `tb_customerperson` VALUES ('6', '6', '6');
INSERT INTO `tb_customerperson` VALUES ('7', '7', '7');
INSERT INTO `tb_customerperson` VALUES ('8', '8', '8');
INSERT INTO `tb_customerperson` VALUES ('9', '9', '9');
INSERT INTO `tb_customerperson` VALUES ('10', '10', '10');
INSERT INTO `tb_customerperson` VALUES ('11', '11', '11');
INSERT INTO `tb_customerperson` VALUES ('12', '12', '12');
INSERT INTO `tb_customerperson` VALUES ('13', '13', '13');
INSERT INTO `tb_customerperson` VALUES ('14', '14', '14');
INSERT INTO `tb_customerperson` VALUES ('15', '15', '15');
INSERT INTO `tb_customerperson` VALUES ('16', '16', '16');
INSERT INTO `tb_customerperson` VALUES ('17', '17', '17');
INSERT INTO `tb_customerperson` VALUES ('18', '18', '18');
INSERT INTO `tb_customerperson` VALUES ('19', '19', '19');
INSERT INTO `tb_customerperson` VALUES ('20', '20', '20');
INSERT INTO `tb_customerperson` VALUES ('21', '21', '21');
INSERT INTO `tb_customerperson` VALUES ('22', '22', '22');
INSERT INTO `tb_customerperson` VALUES ('23', '23', '23');
INSERT INTO `tb_customerperson` VALUES ('24', '24', '24');
INSERT INTO `tb_customerperson` VALUES ('25', '25', '25');
INSERT INTO `tb_customerperson` VALUES ('26', '26', '26');
INSERT INTO `tb_customerperson` VALUES ('27', '27', '27');
INSERT INTO `tb_customerperson` VALUES ('28', '28', '28');
INSERT INTO `tb_customerperson` VALUES ('29', '29', '29');
INSERT INTO `tb_customerperson` VALUES ('30', '30', '30');
INSERT INTO `tb_customerperson` VALUES ('31', '31', '31');
INSERT INTO `tb_customerperson` VALUES ('32', '32', '32');
INSERT INTO `tb_customerperson` VALUES ('33', '33', '33');
INSERT INTO `tb_customerperson` VALUES ('34', '34', '34');
INSERT INTO `tb_customerperson` VALUES ('35', '35', '35');
INSERT INTO `tb_customerperson` VALUES ('36', '36', '36');
INSERT INTO `tb_customerperson` VALUES ('37', '37', '37');
INSERT INTO `tb_customerperson` VALUES ('38', '38', '38');
INSERT INTO `tb_customerperson` VALUES ('39', '39', '39');
INSERT INTO `tb_customerperson` VALUES ('40', '40', '40');
INSERT INTO `tb_customerperson` VALUES ('41', '41', '41');
INSERT INTO `tb_customerperson` VALUES ('42', '42', '42');
INSERT INTO `tb_customerperson` VALUES ('43', '43', '43');
INSERT INTO `tb_customerperson` VALUES ('44', '44', '44');

-- ----------------------------
-- Table structure for tb_customerstatus
-- ----------------------------
DROP TABLE IF EXISTS `tb_customerstatus`;
CREATE TABLE `tb_customerstatus` (
  `customerstatusid` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerid` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusdate` datetime DEFAULT NULL,
  PRIMARY KEY (`customerstatusid`),
  KEY `FK_customerstatus_customer` (`customerid`),
  CONSTRAINT `FK_customerstatus_customer` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customerstatus
-- ----------------------------

-- ----------------------------
-- Table structure for tb_definations
-- ----------------------------
DROP TABLE IF EXISTS `tb_definations`;
CREATE TABLE `tb_definations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_definations
-- ----------------------------
INSERT INTO `tb_definations` VALUES ('1', 'Ledgerlinks');
INSERT INTO `tb_definations` VALUES ('2', 'Gender');
INSERT INTO `tb_definations` VALUES ('3', 'Country');
INSERT INTO `tb_definations` VALUES ('4', 'Payment Method');
INSERT INTO `tb_definations` VALUES ('5', 'Title');
INSERT INTO `tb_definations` VALUES ('6', 'FeeType');
INSERT INTO `tb_definations` VALUES ('7', 'Customer Type');
INSERT INTO `tb_definations` VALUES ('8', 'Loan Status');
INSERT INTO `tb_definations` VALUES ('9', 'Address Type');
INSERT INTO `tb_definations` VALUES ('10', 'ID Type');
INSERT INTO `tb_definations` VALUES ('11', 'Community Status');
INSERT INTO `tb_definations` VALUES ('12', 'Marital Status');
INSERT INTO `tb_definations` VALUES ('13', 'Customer Status');
INSERT INTO `tb_definations` VALUES ('14', 'County');
INSERT INTO `tb_definations` VALUES ('15', 'Fee Options');
INSERT INTO `tb_definations` VALUES ('16', 'Account Status');
INSERT INTO `tb_definations` VALUES ('17', 'Town');
INSERT INTO `tb_definations` VALUES ('18', 'Transaction Type');
INSERT INTO `tb_definations` VALUES ('19', 'Grace Period Options');
INSERT INTO `tb_definations` VALUES ('20', 'Interest Calculation Method');
INSERT INTO `tb_definations` VALUES ('21', 'Day Count');
INSERT INTO `tb_definations` VALUES ('22', 'Disbursement Options');
INSERT INTO `tb_definations` VALUES ('23', 'Loan Source Of Funds');
INSERT INTO `tb_definations` VALUES ('24', 'Loan Reason');
INSERT INTO `tb_definations` VALUES ('25', 'Loan Term Type');

-- ----------------------------
-- Table structure for tb_directmessages
-- ----------------------------
DROP TABLE IF EXISTS `tb_directmessages`;
CREATE TABLE `tb_directmessages` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(15) NOT NULL,
  `templateid` int(11) DEFAULT NULL,
  `ruleid` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`messageid`),
  UNIQUE KEY `description` (`description`),
  KEY `FK_tb_directmessages_tb_rules` (`ruleid`),
  KEY `FK_tb_directmessages_tb_templates` (`templateid`),
  CONSTRAINT `FK_tb_directmessages_tb_rules` FOREIGN KEY (`ruleid`) REFERENCES `tb_rules` (`ruleid`),
  CONSTRAINT `FK_tb_directmessages_tb_templates` FOREIGN KEY (`templateid`) REFERENCES `tb_templates` (`templateid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_directmessages
-- ----------------------------
INSERT INTO `tb_directmessages` VALUES ('1', 'Sun Group Staff', '1', '1', '1');
INSERT INTO `tb_directmessages` VALUES ('2', 'Customer in Arr', '2', '4', '1');

-- ----------------------------
-- Table structure for tb_fee
-- ----------------------------
DROP TABLE IF EXISTS `tb_fee`;
CREATE TABLE `tb_fee` (
  `feeid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  `feetype` tinyint(1) NOT NULL,
  `feeoption` tinyint(1) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` double(18,2) DEFAULT '0.00',
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`feeid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_fee
-- ----------------------------
INSERT INTO `tb_fee` VALUES ('1', 'Registration Fee', 'Registration Fee', '1', '3', '2015-09-19 16:00:00', '150.00', '1');
INSERT INTO `tb_fee` VALUES ('2', 'Loan Application Fee', 'Loan Application Fee', '1', '3', '2015-09-19 16:00:00', '500.00', '1');
INSERT INTO `tb_fee` VALUES ('3', 'Loan Topup  Application Fee', 'Loan Topup  Application Fee', '1', '3', '2016-01-15 09:00:00', '350.00', '1');
INSERT INTO `tb_fee` VALUES ('4', 'Daily Sacco Contribution', 'sacco contribution', '1', '4', '2016-11-29 01:00:00', '300.00', '1');

-- ----------------------------
-- Table structure for tb_guarantors
-- ----------------------------
DROP TABLE IF EXISTS `tb_guarantors`;
CREATE TABLE `tb_guarantors` (
  `guarantorid` bigint(20) NOT NULL AUTO_INCREMENT,
  `loanid` bigint(20) NOT NULL,
  `customerid` bigint(20) NOT NULL,
  `guarantorstatus` tinyint(1) DEFAULT NULL,
  `guaranteedamount` decimal(18,2) DEFAULT '0.00',
  PRIMARY KEY (`guarantorid`),
  KEY `customerid` (`customerid`),
  KEY `loanid` (`loanid`),
  CONSTRAINT `tb_guarantors_ibfk_1` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`),
  CONSTRAINT `tb_guarantors_ibfk_2` FOREIGN KEY (`loanid`) REFERENCES `tb_loans` (`loanid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_guarantors
-- ----------------------------
INSERT INTO `tb_guarantors` VALUES ('1', '1', '10', '1', '74600.00');
INSERT INTO `tb_guarantors` VALUES ('2', '2', '5', '1', '900.00');
INSERT INTO `tb_guarantors` VALUES ('3', '2', '2', '1', '200000.00');
INSERT INTO `tb_guarantors` VALUES ('4', '3', '20', '1', '19400.00');
INSERT INTO `tb_guarantors` VALUES ('5', '3', '17', '1', '30800.00');
INSERT INTO `tb_guarantors` VALUES ('6', '3', '7', '1', '107000.00');
INSERT INTO `tb_guarantors` VALUES ('7', '3', '4', '1', '89000.00');
INSERT INTO `tb_guarantors` VALUES ('8', '4', '2', '1', '125000.00');
INSERT INTO `tb_guarantors` VALUES ('9', '5', '2', '1', '100000.00');
INSERT INTO `tb_guarantors` VALUES ('10', '6', '2', '1', '100000.00');
INSERT INTO `tb_guarantors` VALUES ('11', '7', '2', '1', '40000.00');
INSERT INTO `tb_guarantors` VALUES ('12', '8', '4', '1', '20000.00');
INSERT INTO `tb_guarantors` VALUES ('13', '9', '10', '1', '30000.00');

-- ----------------------------
-- Table structure for tb_ledgeraccounts
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgeraccounts`;
CREATE TABLE `tb_ledgeraccounts` (
  `accountid` bigint(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `formatid` int(11) NOT NULL,
  `subformatid` int(11) DEFAULT NULL,
  `branchid` int(11) NOT NULL,
  `dramount` decimal(18,2) DEFAULT '0.00',
  `cramount` decimal(18,2) DEFAULT '0.00',
  `balancedate` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`accountid`),
  KEY `FK_tb_ledgeraccounts_tb_ledgercategory` (`categoryid`),
  KEY `FK_tb_ledgeraccounts_tb_ledgerformat` (`formatid`),
  KEY `FK_tb_ledgeraccounts_tb_branches` (`branchid`),
  CONSTRAINT `FK_tb_ledgeraccounts_tb_branches` FOREIGN KEY (`branchid`) REFERENCES `tb_branches` (`branchid`),
  CONSTRAINT `FK_tb_ledgeraccounts_tb_ledgercategory` FOREIGN KEY (`categoryid`) REFERENCES `tb_ledgercategory` (`categoryid`),
  CONSTRAINT `FK_tb_ledgeraccounts_tb_ledgerformat` FOREIGN KEY (`formatid`) REFERENCES `tb_ledgerformat` (`formatid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgeraccounts
-- ----------------------------
INSERT INTO `tb_ledgeraccounts` VALUES ('1', '101001', 'Registration Fee', '1', '49', '50', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('2', '304001', 'Accrued Interest - Registration Fee', '3', '9', '10', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('3', '101002', 'Loan Application Fee', '1', '49', '50', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('4', '304002', 'Accrued Interest - Loan Application Fee', '3', '9', '10', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('5', '401001', 'Customer Acc. Daily Savings', '4', '25', '26', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('6', '201001', 'Customer Savings Interest', '2', '57', '59', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('7', '209001', 'Tax on Savings Interest', '2', '92', '94', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('8', '303001', 'Gross Loan Book - Emergency Loans', '3', '7', '8', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('9', '303002', 'Gross Loan Book-Normal Loans', '3', '7', '8', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('10', '103001', 'Interest Income- Emergency Loans', '1', '53', '54', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('11', '103002', 'Interest Income- Normal Loans', '1', '53', '54', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('12', '304003', 'Interest Receivable - Emergency Loans', '3', '9', '10', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('13', '304004', 'Interest Receivable - Normal Loans', '3', '9', '10', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('14', '301001', 'Cooperative Bank Account', '3', '1', '2', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('15', '302001', 'Mpesa Paybill Account', '3', '5', '6', '1', '0.00', '0.00', '2015-09-18 00:00:00', '1', '2015-09-19 16:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('16', '30101001', 'Share Capital', '5', '2', '2147483647', '1', '0.00', '0.00', '2015-11-18 00:00:00', '1', '2015-11-19 17:00:00', '2');
INSERT INTO `tb_ledgeraccounts` VALUES ('17', '103002', 'Interest Income - Emergency Loans', '1', '53', '54', '1', '0.00', '0.00', '2015-12-07 00:00:00', '1', '2015-12-08 01:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('18', '304005', 'Suspense Account', '3', '9', '13', '1', '0.00', '0.00', '2016-01-07 00:00:00', '1', '2016-01-08 01:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('19', '209002', 'Transport', '2', '92', '93', '1', '0.00', '0.00', '2016-01-11 00:00:00', '1', '2016-01-12 01:00:00', '3');
INSERT INTO `tb_ledgeraccounts` VALUES ('20', '101003', 'Daily Sacco Contribution', '1', '49', '50', '1', '0.00', '0.00', '2016-12-08 00:00:00', '1', '2016-12-08 01:00:00', '1');
INSERT INTO `tb_ledgeraccounts` VALUES ('21', '304006', 'Accrued Daily Sacco Contribution', '3', '9', '13', '1', '0.00', '0.00', '2016-12-08 00:00:00', '1', '2016-12-08 01:00:00', '1');

-- ----------------------------
-- Table structure for tb_ledgercategory
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgercategory`;
CREATE TABLE `tb_ledgercategory` (
  `categoryid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgercategory
-- ----------------------------
INSERT INTO `tb_ledgercategory` VALUES ('1', 'Income');
INSERT INTO `tb_ledgercategory` VALUES ('2', 'Expense');
INSERT INTO `tb_ledgercategory` VALUES ('3', 'Asset');
INSERT INTO `tb_ledgercategory` VALUES ('4', 'Liability');
INSERT INTO `tb_ledgercategory` VALUES ('5', 'Capital/Reserve');

-- ----------------------------
-- Table structure for tb_ledgerformat
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgerformat`;
CREATE TABLE `tb_ledgerformat` (
  `formatid` int(11) NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  `ledgeraccountcode` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `formatlevel` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`formatid`),
  KEY `FK_tb_ledgerformat_tb_ledgercategory` (`categoryid`),
  CONSTRAINT `FK_tb_ledgerformat_tb_ledgercategory` FOREIGN KEY (`categoryid`) REFERENCES `tb_ledgercategory` (`categoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgerformat
-- ----------------------------
INSERT INTO `tb_ledgerformat` VALUES ('1', null, '301', 'Cash and Cash Equivalents  ', '1', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('2', '1', '30101', 'Cash at Bank  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('3', '1', '30102', 'Cash at Hand ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('4', '1', '30103', 'Petty Cash', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('5', null, '302', 'Mpesa Accounts', '1', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('6', '5', '30201', 'Mpesa Accounts', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('7', null, '303', 'Loans', '1', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('8', '7', '30301', 'Loans', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('9', null, '304', 'Other Current Assets  ', '1', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('10', '9', '30401', 'Accrued Interest Receivable  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('11', '9', '30402', 'Prepaid Expenses  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('12', '9', '30403', 'Salary Advances  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('13', '9', '30404', 'Other Current Assets  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('14', null, '305', 'Fixed and Intangible Assets  ', '1', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('15', '14', '30501', 'Intangible Assets  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('16', '14', '30502', 'Fixtures, Fittings & Office Equipment  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('17', '14', '30503', 'Vehicles  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('18', '14', '30506', 'Other Fixed Assets  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('19', '14', '30507', 'Amortization of Intangible Assets  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('20', '14', '30508', 'Depreciation of Fixtues, Fittings & Office Equipment  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('21', '14', '30509', 'Depreciation of Vehicles  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('22', '14', '30512', 'Depreciation of Other Fixed Assets  ', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('23', '14', '30504', 'Computer Equipment', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('24', '14', '30511', 'Depreciation of Office Partitions', '2', '3', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('25', null, '401', 'Deposits', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('26', '25', '40101', 'Deposits', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('27', null, '402', 'Entrance Fees', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('28', '27', '40201', 'Entrance Fees', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('29', null, '403', 'Current Liabilities  ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('30', '29', '40301', 'Deferred Income  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('31', '29', '40302', 'Accounts Payable  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('32', '29', '40303', 'Tax  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('33', '29', '40304', 'Short Term Borrowing  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('34', '29', '40305', 'Provisions  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('35', '29', '40306', 'Other Current Liabilities  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('36', null, '404', 'Long Term Liabilities  ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('37', '36', '40401', 'Long Term Borrowing  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('38', '36', '40402', 'Other Long Term Liabilities  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('39', null, '405', 'Share Capital  ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('40', '39', '40501', 'Paid in Capital  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('41', null, '406', 'Retained Earnings  ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('42', '41', '40601', 'Retained Earnings  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('43', '41', '40602', 'Proposed Dividends', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('44', null, '407', 'Current year profit/loss  ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('45', '44', '40701', 'Profit/Loss  ', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('46', null, '408', 'Reserves ', '1', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('47', '46', '40801', 'General Reserves', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('48', '46', '40802', 'Statutory Reserves', '2', '4', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('49', null, '101', 'Fee Income  ', '1', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('50', '49', '10101', 'Fee Income  ', '2', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('51', null, '102', 'Extraordinary Income  ', '1', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('52', '51', '10201', 'Exceptional Item  ', '2', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('53', null, '103', 'Interest On Loans', '1', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('54', '53', '10301', 'Interest On Loans', '2', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('55', null, '104', 'Other Income', '1', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('56', '55', '10401', 'Other Income', '2', '1', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('57', null, '201', 'Financial Expenses  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('58', '57', '20101', 'Interest expenses on borrowing  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('59', '57', '20102', 'Savings Interest Expense', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('60', null, '202', 'Bad Debt Expense', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('61', '60', '20201', 'Bad Debt Expense', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('62', null, '203', 'Payroll Expenses  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('63', '62', '20301', 'Salaries and Wages', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('64', '62', '20302', 'Payroll Processing Fees', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('65', '62', '20303', 'Payroll Taxes', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('66', null, '204', 'Administrative expenses  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('67', '66', '20401', 'Rent   ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('68', '66', '20402', 'Licenses, permits, Dues & Subscriptions', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('69', '66', '20403', 'Utilities  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('70', '66', '20404', 'Communication  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('71', '66', '20405', 'Insurance - Liability', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('72', '66', '20406', 'Printing and stationary  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('73', '66', '20407', 'Maintenance and repairs  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('74', '66', '20408', 'Directors\' fees  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('75', '66', '20409', 'Other office expenses  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('76', '66', '20410', 'Advertising and Marketing', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('77', '66', '20411', 'Insurance - Health', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('78', '66', '20412', 'Postage and Delivery', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('79', '66', '20413', 'Staff Meetings & Kitchen Supplies', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('80', '66', '20414', 'Telephone', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('81', null, '205', 'Transportation and travel  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('82', '81', '20501', 'Motorvehicle expenses  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('83', '81', '20502', 'Travel expenses  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('84', '81', '20503', 'Travel - Meals & Entertainment', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('85', null, '206', 'Training   ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('86', '85', '20601', 'Training & staff development  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('87', null, '207', 'External Services  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('88', '87', '20701', 'Professional Fees  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('89', '87', '20702', 'Legal Fees', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('90', null, '208', 'Depreciation  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('91', '90', '20801', 'Depreciation of Fixed Assets  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('92', null, '209', 'Extraordinary Expenses  ', '1', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('93', '92', '20901', 'Extraordinary Expenses  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('94', '92', '20902', 'Income Tax Expense  ', '2', '2', '1', '2015-09-18 05:51:42');
INSERT INTO `tb_ledgerformat` VALUES ('95', '92', '20903', 'PAYE', '2', '2', '1', '2015-09-18 05:51:42');

-- ----------------------------
-- Table structure for tb_ledgerlinks
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgerlinks`;
CREATE TABLE `tb_ledgerlinks` (
  `ledgerlinkid` int(11) NOT NULL AUTO_INCREMENT,
  `feeid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `gltype` int(11) NOT NULL,
  `accountid` bigint(20) NOT NULL,
  PRIMARY KEY (`ledgerlinkid`),
  KEY `FK_gllinks` (`accountid`),
  KEY `FK_tb_ledgerlinks_tb_users` (`userid`),
  KEY `FK_tb_ledgerlinks_tb_products` (`productid`),
  KEY `FK_tb_ledgerlinks_tb_fee` (`feeid`),
  CONSTRAINT `FK_tb_ledgerlinks_tb_fee` FOREIGN KEY (`feeid`) REFERENCES `tb_fee` (`feeid`),
  CONSTRAINT `FK_tb_ledgerlinks_tb_ledgeraccounts` FOREIGN KEY (`accountid`) REFERENCES `tb_ledgeraccounts` (`accountid`),
  CONSTRAINT `FK_tb_ledgerlinks_tb_products` FOREIGN KEY (`productid`) REFERENCES `tb_products` (`productid`),
  CONSTRAINT `FK_tb_ledgerlinks_tb_users` FOREIGN KEY (`userid`) REFERENCES `tb_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgerlinks
-- ----------------------------
INSERT INTO `tb_ledgerlinks` VALUES ('1', '1', '1', null, '2', '1');
INSERT INTO `tb_ledgerlinks` VALUES ('2', '1', '1', null, '3', '2');
INSERT INTO `tb_ledgerlinks` VALUES ('25', null, '1', null, '8', '14');
INSERT INTO `tb_ledgerlinks` VALUES ('95', null, '1', null, '8', '15');
INSERT INTO `tb_ledgerlinks` VALUES ('96', null, '1', null, '8', '15');
INSERT INTO `tb_ledgerlinks` VALUES ('97', null, '1', null, '8', '15');
INSERT INTO `tb_ledgerlinks` VALUES ('113', '3', '1', null, '2', '3');
INSERT INTO `tb_ledgerlinks` VALUES ('114', '3', '1', null, '3', '4');
INSERT INTO `tb_ledgerlinks` VALUES ('116', '2', '1', null, '2', '3');
INSERT INTO `tb_ledgerlinks` VALUES ('117', '2', '1', null, '3', '4');
INSERT INTO `tb_ledgerlinks` VALUES ('139', '4', '2', null, '2', '20');
INSERT INTO `tb_ledgerlinks` VALUES ('140', '4', '2', null, '3', '21');
INSERT INTO `tb_ledgerlinks` VALUES ('142', null, '2', '1', '1', '5');
INSERT INTO `tb_ledgerlinks` VALUES ('143', null, '2', '1', '4', '6');
INSERT INTO `tb_ledgerlinks` VALUES ('144', null, '2', '1', '6', '7');
INSERT INTO `tb_ledgerlinks` VALUES ('160', null, null, '4', '1', '14');
INSERT INTO `tb_ledgerlinks` VALUES ('161', null, null, '4', '9', '10');
INSERT INTO `tb_ledgerlinks` VALUES ('162', null, null, '4', '10', '12');
INSERT INTO `tb_ledgerlinks` VALUES ('169', null, null, '3', '1', '14');
INSERT INTO `tb_ledgerlinks` VALUES ('170', null, null, '3', '9', '11');
INSERT INTO `tb_ledgerlinks` VALUES ('171', null, null, '3', '10', '9');

-- ----------------------------
-- Table structure for tb_ledgers
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgers`;
CREATE TABLE `tb_ledgers` (
  `ledgerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ledgertypeid` int(11) NOT NULL,
  `periodid` int(11) NOT NULL,
  `yearend` tinyint(1) NOT NULL DEFAULT '0',
  `valuedate` datetime NOT NULL,
  `transactionnote` varchar(500) DEFAULT NULL,
  `receiptno` bigint(20) DEFAULT '0',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`ledgerid`),
  KEY `FK_journals` (`ledgertypeid`),
  KEY `FK_tb_ledgers_tb_period` (`periodid`),
  KEY `FK_tb_ledgers_tb_users` (`createdby`),
  KEY `tb_ledgers_receiptno` (`receiptno`),
  CONSTRAINT `FK_tb_ledgers_tb_period` FOREIGN KEY (`periodid`) REFERENCES `tb_period` (`periodid`),
  CONSTRAINT `FK_tb_ledgers_tb_users` FOREIGN KEY (`createdby`) REFERENCES `tb_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=815 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgers
-- ----------------------------
INSERT INTO `tb_ledgers` VALUES ('1', '1', '1', '0', '2016-11-29 00:00:00', 'Opening Balance', '1', '2016-11-29 15:47:56', '1');
INSERT INTO `tb_ledgers` VALUES ('2', '1', '1', '0', '2016-11-29 00:00:00', '', '2', '2016-11-29 15:50:29', '1');
INSERT INTO `tb_ledgers` VALUES ('3', '1', '1', '0', '2016-11-29 00:00:00', 'Opening Balance', '3', '2016-11-29 15:59:19', '1');
INSERT INTO `tb_ledgers` VALUES ('4', '1', '1', '0', '2016-11-29 00:00:00', 'Opening Balance', '4', '2016-11-29 16:02:45', '1');
INSERT INTO `tb_ledgers` VALUES ('5', '1', '1', '0', '2016-11-29 00:00:00', '', '5', '2016-11-29 16:05:06', '1');
INSERT INTO `tb_ledgers` VALUES ('6', '1', '1', '0', '2016-11-29 00:00:00', 'Opening Balance', '6', '2016-11-29 16:08:21', '1');
INSERT INTO `tb_ledgers` VALUES ('7', '1', '1', '0', '2016-11-29 00:00:00', '', '7', '2016-11-29 16:22:32', '1');
INSERT INTO `tb_ledgers` VALUES ('8', '1', '1', '0', '2016-11-29 00:00:00', '', '9', '2016-11-29 17:00:25', '1');
INSERT INTO `tb_ledgers` VALUES ('9', '1', '1', '0', '2016-11-29 00:00:00', '', '10', '2016-11-29 17:10:27', '1');
INSERT INTO `tb_ledgers` VALUES ('10', '1', '1', '0', '2016-11-29 00:00:00', '', '11', '2016-11-29 17:21:58', '1');
INSERT INTO `tb_ledgers` VALUES ('11', '1', '1', '0', '2016-11-29 00:00:00', '', '20', '2016-11-29 17:24:23', '1');
INSERT INTO `tb_ledgers` VALUES ('12', '1', '1', '0', '2016-11-29 00:00:00', '', '21', '2016-11-29 22:13:04', '1');
INSERT INTO `tb_ledgers` VALUES ('13', '1', '1', '0', '2016-12-08 00:00:00', 'Opening Balance', '17', '2016-12-08 14:36:30', '1');
INSERT INTO `tb_ledgers` VALUES ('14', '1', '1', '0', '2016-12-08 00:00:00', 'Opening Balance', '18', '2016-12-08 15:06:01', '1');
INSERT INTO `tb_ledgers` VALUES ('15', '1', '1', '0', '2016-12-08 00:00:00', 'Opening Balance', '19', '2016-12-08 15:11:26', '1');
INSERT INTO `tb_ledgers` VALUES ('16', '1', '1', '0', '2016-12-08 00:00:00', '', '20', '2016-12-08 15:32:15', '1');
INSERT INTO `tb_ledgers` VALUES ('17', '1', '1', '0', '2016-12-08 00:00:00', '21', '21', '2016-12-08 17:00:21', '1');
INSERT INTO `tb_ledgers` VALUES ('18', '1', '1', '0', '2016-12-08 00:00:00', 'Opening Balance', '22', '2016-12-08 17:04:47', '1');
INSERT INTO `tb_ledgers` VALUES ('19', '1', '1', '0', '2016-12-08 00:00:00', '', '23', '2016-12-14 13:36:41', '1');
INSERT INTO `tb_ledgers` VALUES ('20', '1', '1', '0', '2016-12-09 00:00:00', '', '24', '2016-12-14 13:38:19', '1');
INSERT INTO `tb_ledgers` VALUES ('21', '1', '1', '0', '2016-12-10 00:00:00', '', '25', '2016-12-14 13:39:04', '1');
INSERT INTO `tb_ledgers` VALUES ('22', '1', '1', '0', '2016-12-11 00:00:00', '', '26', '2016-12-14 13:39:55', '1');
INSERT INTO `tb_ledgers` VALUES ('23', '1', '1', '0', '2016-12-12 00:00:00', '', '27', '2016-12-14 13:40:49', '1');
INSERT INTO `tb_ledgers` VALUES ('24', '1', '1', '0', '2016-12-13 00:00:00', '', '28', '2016-12-14 13:41:33', '1');
INSERT INTO `tb_ledgers` VALUES ('25', '1', '1', '0', '2016-12-14 00:00:00', '', '29', '2016-12-14 13:42:15', '1');
INSERT INTO `tb_ledgers` VALUES ('26', '1', '1', '0', '2016-12-15 00:00:00', '', '30', '2016-12-14 13:43:05', '1');
INSERT INTO `tb_ledgers` VALUES ('27', '1', '1', '0', '2016-12-10 00:00:00', '', '31', '2016-12-14 13:51:02', '1');
INSERT INTO `tb_ledgers` VALUES ('28', '1', '1', '0', '2016-12-11 00:00:00', '', '32', '2016-12-14 13:52:07', '1');
INSERT INTO `tb_ledgers` VALUES ('29', '1', '1', '0', '2016-12-13 00:00:00', '', '33', '2016-12-14 13:53:02', '1');
INSERT INTO `tb_ledgers` VALUES ('30', '1', '1', '0', '2016-12-01 00:00:00', '', '34', '2016-12-14 13:55:45', '1');
INSERT INTO `tb_ledgers` VALUES ('31', '1', '1', '0', '2016-12-03 00:00:00', '', '35', '2016-12-14 13:58:01', '1');
INSERT INTO `tb_ledgers` VALUES ('32', '1', '1', '0', '2016-12-05 00:00:00', '', '36', '2016-12-14 13:59:00', '1');
INSERT INTO `tb_ledgers` VALUES ('33', '1', '1', '0', '2016-12-06 00:00:00', '', '37', '2016-12-14 14:00:04', '1');
INSERT INTO `tb_ledgers` VALUES ('34', '1', '1', '0', '2016-12-09 00:00:00', '', '38', '2016-12-14 14:01:38', '1');
INSERT INTO `tb_ledgers` VALUES ('35', '1', '1', '0', '2016-12-10 00:00:00', '', '39', '2016-12-14 14:03:33', '1');
INSERT INTO `tb_ledgers` VALUES ('36', '1', '1', '0', '2016-12-01 00:00:00', '', '40', '2016-12-14 14:06:42', '1');
INSERT INTO `tb_ledgers` VALUES ('37', '1', '1', '0', '2016-12-03 00:00:00', '', '41', '2016-12-14 14:07:55', '1');
INSERT INTO `tb_ledgers` VALUES ('38', '1', '1', '0', '2016-12-05 00:00:00', '', '42', '2016-12-14 14:08:56', '1');
INSERT INTO `tb_ledgers` VALUES ('39', '1', '1', '0', '2016-12-09 00:00:00', '', '43', '2016-12-14 14:14:17', '1');
INSERT INTO `tb_ledgers` VALUES ('40', '1', '1', '0', '2016-12-10 00:00:00', '', '44', '2016-12-14 14:15:07', '1');
INSERT INTO `tb_ledgers` VALUES ('41', '1', '1', '0', '2016-12-11 00:00:00', '', '45', '2016-12-14 14:22:22', '1');
INSERT INTO `tb_ledgers` VALUES ('42', '1', '1', '0', '2016-12-15 00:00:00', '', '46', '2016-12-19 10:13:19', '1');
INSERT INTO `tb_ledgers` VALUES ('43', '1', '1', '0', '2016-12-08 00:00:00', '', '47', '2016-12-19 10:20:22', '1');
INSERT INTO `tb_ledgers` VALUES ('44', '1', '1', '0', '2016-12-09 00:00:00', '', '48', '2016-12-19 10:21:05', '1');
INSERT INTO `tb_ledgers` VALUES ('45', '1', '1', '0', '2016-12-10 00:00:00', '', '49', '2016-12-19 10:21:46', '1');
INSERT INTO `tb_ledgers` VALUES ('46', '1', '1', '0', '2016-12-11 00:00:00', '', '50', '2016-12-19 10:22:33', '1');
INSERT INTO `tb_ledgers` VALUES ('47', '1', '1', '0', '2016-12-13 00:00:00', '', '51', '2016-12-19 10:23:12', '1');
INSERT INTO `tb_ledgers` VALUES ('48', '1', '1', '0', '2016-12-14 00:00:00', '', '52', '2016-12-19 10:24:14', '1');
INSERT INTO `tb_ledgers` VALUES ('49', '1', '1', '0', '2016-12-16 00:00:00', '', '53', '2016-12-19 10:24:54', '1');
INSERT INTO `tb_ledgers` VALUES ('50', '1', '1', '0', '2016-12-01 00:00:00', '', '54', '2016-12-19 10:26:53', '1');
INSERT INTO `tb_ledgers` VALUES ('51', '1', '1', '0', '2016-12-02 00:00:00', '', '55', '2016-12-19 10:32:00', '1');
INSERT INTO `tb_ledgers` VALUES ('52', '1', '1', '0', '2016-12-03 00:00:00', '', '56', '2016-12-19 10:33:05', '1');
INSERT INTO `tb_ledgers` VALUES ('53', '1', '1', '0', '2016-12-04 00:00:00', '', '57', '2016-12-19 10:34:05', '1');
INSERT INTO `tb_ledgers` VALUES ('54', '1', '1', '0', '2016-12-05 00:00:00', '', '58', '2016-12-19 10:34:32', '1');
INSERT INTO `tb_ledgers` VALUES ('55', '1', '1', '0', '2016-12-08 00:00:00', '', '59', '2016-12-19 10:34:54', '1');
INSERT INTO `tb_ledgers` VALUES ('56', '1', '1', '0', '2016-12-09 00:00:00', '', '60', '2016-12-19 10:35:17', '1');
INSERT INTO `tb_ledgers` VALUES ('57', '1', '1', '0', '2016-12-10 00:00:00', '', '61', '2016-12-19 10:37:29', '1');
INSERT INTO `tb_ledgers` VALUES ('58', '1', '1', '0', '2016-12-11 00:00:00', '', '62', '2016-12-19 10:37:47', '1');
INSERT INTO `tb_ledgers` VALUES ('59', '1', '1', '0', '2016-12-13 00:00:00', '', '63', '2016-12-19 10:38:07', '1');
INSERT INTO `tb_ledgers` VALUES ('60', '1', '1', '0', '2016-12-14 00:00:00', '', '64', '2016-12-19 10:38:25', '1');
INSERT INTO `tb_ledgers` VALUES ('61', '1', '1', '0', '2016-12-16 00:00:00', '', '65', '2016-12-19 10:38:54', '1');
INSERT INTO `tb_ledgers` VALUES ('62', '1', '1', '0', '2016-12-19 00:00:00', 'Opening Balance', '66', '2016-12-19 13:42:34', '1');
INSERT INTO `tb_ledgers` VALUES ('63', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '67', '2016-12-22 12:39:55', '1');
INSERT INTO `tb_ledgers` VALUES ('64', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '68', '2016-12-22 12:40:16', '1');
INSERT INTO `tb_ledgers` VALUES ('65', '1', '1', '0', '2016-12-07 00:00:00', '', '69', '2016-12-22 12:47:16', '1');
INSERT INTO `tb_ledgers` VALUES ('66', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '70', '2016-12-22 12:50:18', '1');
INSERT INTO `tb_ledgers` VALUES ('67', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '71', '2016-12-22 12:55:38', '1');
INSERT INTO `tb_ledgers` VALUES ('68', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '72', '2016-12-22 12:59:56', '1');
INSERT INTO `tb_ledgers` VALUES ('69', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '73', '2016-12-22 13:01:05', '1');
INSERT INTO `tb_ledgers` VALUES ('70', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '74', '2016-12-22 13:02:33', '1');
INSERT INTO `tb_ledgers` VALUES ('71', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '75', '2016-12-22 13:04:07', '1');
INSERT INTO `tb_ledgers` VALUES ('72', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '76', '2016-12-22 13:05:09', '1');
INSERT INTO `tb_ledgers` VALUES ('73', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '77', '2016-12-22 13:06:44', '1');
INSERT INTO `tb_ledgers` VALUES ('74', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '78', '2016-12-22 13:08:47', '1');
INSERT INTO `tb_ledgers` VALUES ('75', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '79', '2016-12-22 13:09:47', '1');
INSERT INTO `tb_ledgers` VALUES ('76', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '80', '2016-12-22 13:11:36', '1');
INSERT INTO `tb_ledgers` VALUES ('77', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '81', '2016-12-22 13:13:40', '1');
INSERT INTO `tb_ledgers` VALUES ('78', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '82', '2016-12-22 13:14:19', '1');
INSERT INTO `tb_ledgers` VALUES ('79', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '83', '2016-12-22 13:15:10', '1');
INSERT INTO `tb_ledgers` VALUES ('80', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '84', '2016-12-22 13:16:31', '1');
INSERT INTO `tb_ledgers` VALUES ('81', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '85', '2016-12-22 13:17:39', '1');
INSERT INTO `tb_ledgers` VALUES ('82', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '86', '2016-12-22 13:18:30', '1');
INSERT INTO `tb_ledgers` VALUES ('83', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '87', '2016-12-22 13:24:51', '1');
INSERT INTO `tb_ledgers` VALUES ('84', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '88', '2016-12-22 13:25:43', '1');
INSERT INTO `tb_ledgers` VALUES ('85', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '89', '2016-12-22 13:55:18', '1');
INSERT INTO `tb_ledgers` VALUES ('86', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '90', '2016-12-22 13:56:43', '1');
INSERT INTO `tb_ledgers` VALUES ('87', '1', '1', '0', '2016-12-22 00:00:00', 'Opening Balance', '91', '2016-12-22 14:08:50', '1');
INSERT INTO `tb_ledgers` VALUES ('88', '1', '1', '0', '2016-12-17 00:00:00', '', '92', '2016-12-29 13:21:04', '1');
INSERT INTO `tb_ledgers` VALUES ('89', '1', '1', '0', '2016-12-18 00:00:00', '', '93', '2016-12-29 13:21:50', '1');
INSERT INTO `tb_ledgers` VALUES ('90', '1', '1', '0', '2016-12-19 00:00:00', '', '94', '2016-12-29 13:22:26', '1');
INSERT INTO `tb_ledgers` VALUES ('91', '1', '1', '0', '2016-12-22 00:00:00', '', '95', '2016-12-29 13:26:53', '1');
INSERT INTO `tb_ledgers` VALUES ('92', '1', '1', '0', '2016-12-23 00:00:00', '', '96', '2016-12-29 13:27:39', '1');
INSERT INTO `tb_ledgers` VALUES ('93', '1', '1', '0', '2016-12-24 00:00:00', '', '97', '2016-12-29 13:28:07', '1');
INSERT INTO `tb_ledgers` VALUES ('94', '1', '1', '0', '2016-12-25 00:00:00', '', '98', '2016-12-29 13:32:24', '1');
INSERT INTO `tb_ledgers` VALUES ('95', '1', '1', '0', '2016-12-26 00:00:00', '', '99', '2016-12-29 13:33:08', '1');
INSERT INTO `tb_ledgers` VALUES ('96', '1', '1', '0', '2016-12-26 00:00:00', '', '100', '2016-12-29 13:35:32', '1');
INSERT INTO `tb_ledgers` VALUES ('97', '1', '1', '0', '2016-12-27 00:00:00', '', '101', '2016-12-29 13:36:14', '1');
INSERT INTO `tb_ledgers` VALUES ('98', '1', '1', '0', '2016-12-16 00:00:00', '', '102', '2016-12-29 13:43:51', '1');
INSERT INTO `tb_ledgers` VALUES ('99', '1', '1', '0', '2016-12-17 00:00:00', '', '103', '2016-12-29 13:44:24', '1');
INSERT INTO `tb_ledgers` VALUES ('100', '1', '1', '0', '2016-12-17 00:00:00', '', '104', '2016-12-29 13:56:49', '1');
INSERT INTO `tb_ledgers` VALUES ('101', '1', '1', '0', '2016-12-18 00:00:00', '', '105', '2016-12-29 13:57:24', '1');
INSERT INTO `tb_ledgers` VALUES ('102', '1', '1', '0', '2016-12-19 00:00:00', '', '106', '2016-12-29 13:58:37', '1');
INSERT INTO `tb_ledgers` VALUES ('103', '1', '1', '0', '2016-12-20 00:00:00', '', '107', '2016-12-29 13:59:15', '1');
INSERT INTO `tb_ledgers` VALUES ('104', '1', '1', '0', '2016-12-21 00:00:00', '', '108', '2016-12-29 13:59:57', '1');
INSERT INTO `tb_ledgers` VALUES ('105', '1', '1', '0', '2016-12-29 00:00:00', 'Opening Balance', '109', '2016-12-29 14:07:08', '1');
INSERT INTO `tb_ledgers` VALUES ('106', '1', '1', '0', '2016-12-15 00:00:00', '', '110', '2016-12-29 14:35:59', '1');
INSERT INTO `tb_ledgers` VALUES ('107', '1', '1', '0', '2016-12-28 00:00:00', '', '111', '2016-12-29 14:52:24', '1');
INSERT INTO `tb_ledgers` VALUES ('108', '1', '1', '0', '2016-12-14 00:00:00', '', '112', '2016-12-31 10:19:30', '1');
INSERT INTO `tb_ledgers` VALUES ('109', '1', '1', '0', '2016-12-21 00:00:00', '', '113', '2016-12-31 10:19:55', '1');
INSERT INTO `tb_ledgers` VALUES ('110', '1', '1', '0', '2016-12-22 00:00:00', '', '114', '2016-12-31 10:20:17', '1');
INSERT INTO `tb_ledgers` VALUES ('111', '1', '1', '0', '2016-12-25 00:00:00', '', '115', '2016-12-31 10:20:39', '1');
INSERT INTO `tb_ledgers` VALUES ('112', '1', '1', '0', '2016-12-26 00:00:00', '', '116', '2016-12-31 10:21:05', '1');
INSERT INTO `tb_ledgers` VALUES ('113', '1', '1', '0', '2016-12-27 00:00:00', '', '117', '2016-12-31 10:21:21', '1');
INSERT INTO `tb_ledgers` VALUES ('114', '1', '1', '0', '2016-12-19 00:00:00', '', '118', '2016-12-31 10:26:25', '1');
INSERT INTO `tb_ledgers` VALUES ('115', '1', '1', '0', '2016-12-20 00:00:00', '', '119', '2016-12-31 10:26:47', '1');
INSERT INTO `tb_ledgers` VALUES ('116', '1', '1', '0', '2016-12-21 00:00:00', '', '120', '2016-12-31 10:27:06', '1');
INSERT INTO `tb_ledgers` VALUES ('117', '1', '1', '0', '2016-12-22 00:00:00', '', '121', '2016-12-31 10:27:25', '1');
INSERT INTO `tb_ledgers` VALUES ('118', '1', '1', '0', '2016-12-27 00:00:00', '', '122', '2016-12-31 10:27:50', '1');
INSERT INTO `tb_ledgers` VALUES ('119', '1', '1', '0', '2016-12-28 00:00:00', '', '123', '2016-12-31 10:28:15', '1');
INSERT INTO `tb_ledgers` VALUES ('120', '1', '1', '0', '2016-12-29 00:00:00', '', '124', '2016-12-31 10:28:36', '1');
INSERT INTO `tb_ledgers` VALUES ('121', '1', '1', '0', '2016-12-01 00:00:00', '', '125', '2016-12-31 10:57:54', '1');
INSERT INTO `tb_ledgers` VALUES ('122', '1', '1', '0', '2016-12-03 00:00:00', '', '126', '2016-12-31 10:58:23', '1');
INSERT INTO `tb_ledgers` VALUES ('123', '1', '1', '0', '2016-12-06 00:00:00', '', '127', '2016-12-31 10:58:56', '1');
INSERT INTO `tb_ledgers` VALUES ('124', '1', '1', '0', '2016-12-07 00:00:00', '', '128', '2016-12-31 10:59:16', '1');
INSERT INTO `tb_ledgers` VALUES ('125', '1', '1', '0', '2016-12-10 00:00:00', '', '129', '2016-12-31 11:00:40', '1');
INSERT INTO `tb_ledgers` VALUES ('126', '1', '1', '0', '2016-12-14 00:00:00', '', '130', '2016-12-31 11:01:03', '1');
INSERT INTO `tb_ledgers` VALUES ('127', '1', '1', '0', '2016-12-16 00:00:00', '', '131', '2016-12-31 11:01:23', '1');
INSERT INTO `tb_ledgers` VALUES ('128', '1', '1', '0', '2016-12-17 00:00:00', '', '132', '2016-12-31 11:01:41', '1');
INSERT INTO `tb_ledgers` VALUES ('129', '1', '1', '0', '2016-12-19 00:00:00', '', '133', '2016-12-31 11:01:59', '1');
INSERT INTO `tb_ledgers` VALUES ('130', '1', '1', '0', '2016-12-21 00:00:00', '', '134', '2016-12-31 11:02:18', '1');
INSERT INTO `tb_ledgers` VALUES ('131', '1', '1', '0', '2016-12-22 00:00:00', '', '135', '2016-12-31 11:02:37', '1');
INSERT INTO `tb_ledgers` VALUES ('132', '1', '1', '0', '2016-12-23 00:00:00', '', '136', '2016-12-31 11:02:55', '1');
INSERT INTO `tb_ledgers` VALUES ('133', '1', '1', '0', '2016-12-01 00:00:00', '', '137', '2016-12-31 11:04:23', '1');
INSERT INTO `tb_ledgers` VALUES ('134', '1', '1', '0', '2016-12-03 00:00:00', '', '138', '2016-12-31 11:04:52', '1');
INSERT INTO `tb_ledgers` VALUES ('135', '1', '1', '0', '2016-12-05 00:00:00', '', '139', '2016-12-31 11:05:11', '1');
INSERT INTO `tb_ledgers` VALUES ('136', '1', '1', '0', '2016-12-05 00:00:00', '', '140', '2016-12-31 11:06:56', '1');
INSERT INTO `tb_ledgers` VALUES ('137', '1', '1', '0', '2016-12-01 00:00:00', '', '141', '2016-12-31 11:09:15', '1');
INSERT INTO `tb_ledgers` VALUES ('138', '1', '1', '0', '2016-12-17 00:00:00', '', '142', '2016-12-31 11:10:23', '1');
INSERT INTO `tb_ledgers` VALUES ('139', '1', '1', '0', '2016-12-18 00:00:00', '', '143', '2016-12-31 11:10:42', '1');
INSERT INTO `tb_ledgers` VALUES ('140', '1', '1', '0', '2016-12-20 00:00:00', '', '144', '2016-12-31 11:11:05', '1');
INSERT INTO `tb_ledgers` VALUES ('141', '1', '1', '0', '2016-12-21 00:00:00', '', '145', '2016-12-31 11:11:24', '1');
INSERT INTO `tb_ledgers` VALUES ('142', '1', '1', '0', '2016-12-22 00:00:00', '', '146', '2016-12-31 11:11:40', '1');
INSERT INTO `tb_ledgers` VALUES ('143', '1', '1', '0', '2016-12-23 00:00:00', '', '147', '2016-12-31 11:12:04', '1');
INSERT INTO `tb_ledgers` VALUES ('144', '1', '1', '0', '2016-12-24 00:00:00', '', '148', '2016-12-31 11:12:20', '1');
INSERT INTO `tb_ledgers` VALUES ('145', '1', '1', '0', '2016-12-25 00:00:00', '', '149', '2016-12-31 11:12:38', '1');
INSERT INTO `tb_ledgers` VALUES ('146', '1', '1', '0', '2016-12-26 00:00:00', '', '150', '2016-12-31 11:12:56', '1');
INSERT INTO `tb_ledgers` VALUES ('147', '1', '1', '0', '2016-12-27 00:00:00', '', '151', '2016-12-31 11:13:25', '1');
INSERT INTO `tb_ledgers` VALUES ('148', '1', '1', '0', '2016-12-28 00:00:00', '', '152', '2016-12-31 11:13:50', '1');
INSERT INTO `tb_ledgers` VALUES ('149', '1', '1', '0', '2016-12-29 00:00:00', '', '153', '2016-12-31 11:14:09', '1');
INSERT INTO `tb_ledgers` VALUES ('150', '1', '1', '0', '2016-12-01 00:00:00', '', '154', '2016-12-31 11:18:50', '1');
INSERT INTO `tb_ledgers` VALUES ('151', '1', '1', '0', '2016-12-02 00:00:00', '', '155', '2016-12-31 11:19:16', '1');
INSERT INTO `tb_ledgers` VALUES ('152', '1', '1', '0', '2016-12-03 00:00:00', '', '156', '2016-12-31 11:19:31', '1');
INSERT INTO `tb_ledgers` VALUES ('153', '1', '1', '0', '2016-12-04 00:00:00', '', '157', '2016-12-31 11:19:46', '1');
INSERT INTO `tb_ledgers` VALUES ('154', '1', '1', '0', '2016-12-07 00:00:00', '', '158', '2016-12-31 11:20:08', '1');
INSERT INTO `tb_ledgers` VALUES ('155', '1', '1', '0', '2016-12-09 00:00:00', '', '159', '2016-12-31 11:20:31', '1');
INSERT INTO `tb_ledgers` VALUES ('156', '1', '1', '0', '2016-12-10 00:00:00', '', '160', '2016-12-31 11:20:51', '1');
INSERT INTO `tb_ledgers` VALUES ('157', '1', '1', '0', '2016-12-11 00:00:00', '', '161', '2016-12-31 11:21:07', '1');
INSERT INTO `tb_ledgers` VALUES ('158', '1', '1', '0', '2016-12-17 00:00:00', '', '162', '2016-12-31 11:21:27', '1');
INSERT INTO `tb_ledgers` VALUES ('159', '1', '1', '0', '2016-12-18 00:00:00', '', '163', '2016-12-31 11:21:42', '1');
INSERT INTO `tb_ledgers` VALUES ('160', '1', '1', '0', '2016-12-19 00:00:00', '', '164', '2016-12-31 11:21:58', '1');
INSERT INTO `tb_ledgers` VALUES ('161', '1', '1', '0', '2016-12-21 00:00:00', '', '165', '2016-12-31 11:22:14', '1');
INSERT INTO `tb_ledgers` VALUES ('162', '1', '1', '0', '2016-12-22 00:00:00', '', '166', '2016-12-31 11:22:30', '1');
INSERT INTO `tb_ledgers` VALUES ('163', '1', '1', '0', '2016-12-25 00:00:00', '', '167', '2016-12-31 11:22:51', '1');
INSERT INTO `tb_ledgers` VALUES ('164', '1', '1', '0', '2016-12-26 00:00:00', '', '168', '2016-12-31 11:23:11', '1');
INSERT INTO `tb_ledgers` VALUES ('165', '1', '1', '0', '2016-12-27 00:00:00', '', '169', '2016-12-31 11:23:32', '1');
INSERT INTO `tb_ledgers` VALUES ('166', '1', '1', '0', '2016-12-02 00:00:00', '', '170', '2016-12-31 11:26:14', '1');
INSERT INTO `tb_ledgers` VALUES ('167', '1', '1', '0', '2016-12-05 00:00:00', '', '171', '2016-12-31 11:26:44', '1');
INSERT INTO `tb_ledgers` VALUES ('168', '1', '1', '0', '2016-12-06 00:00:00', '', '172', '2016-12-31 11:27:03', '1');
INSERT INTO `tb_ledgers` VALUES ('169', '1', '1', '0', '2016-12-07 00:00:00', '', '173', '2016-12-31 11:27:21', '1');
INSERT INTO `tb_ledgers` VALUES ('170', '1', '1', '0', '2016-12-08 00:00:00', '', '174', '2016-12-31 11:27:36', '1');
INSERT INTO `tb_ledgers` VALUES ('171', '1', '1', '0', '2016-12-10 00:00:00', '', '175', '2016-12-31 11:27:53', '1');
INSERT INTO `tb_ledgers` VALUES ('172', '1', '1', '0', '2016-12-13 00:00:00', '', '176', '2016-12-31 11:28:12', '1');
INSERT INTO `tb_ledgers` VALUES ('173', '1', '1', '0', '2016-12-14 00:00:00', '', '177', '2016-12-31 11:28:32', '1');
INSERT INTO `tb_ledgers` VALUES ('174', '1', '1', '0', '2016-12-19 00:00:00', '', '178', '2016-12-31 11:28:53', '1');
INSERT INTO `tb_ledgers` VALUES ('175', '1', '1', '0', '2016-12-20 00:00:00', '', '179', '2016-12-31 11:29:08', '1');
INSERT INTO `tb_ledgers` VALUES ('176', '1', '1', '0', '2016-12-29 00:00:00', '', '180', '2016-12-31 11:29:28', '1');
INSERT INTO `tb_ledgers` VALUES ('177', '1', '1', '0', '2016-12-30 00:00:00', '', '181', '2016-12-31 11:29:47', '1');
INSERT INTO `tb_ledgers` VALUES ('178', '1', '1', '0', '2016-12-22 00:00:00', '', '182', '2016-12-31 11:30:42', '1');
INSERT INTO `tb_ledgers` VALUES ('179', '1', '1', '0', '2016-12-23 00:00:00', '', '183', '2016-12-31 11:30:59', '1');
INSERT INTO `tb_ledgers` VALUES ('180', '1', '1', '0', '2016-12-24 00:00:00', '', '184', '2016-12-31 11:31:17', '1');
INSERT INTO `tb_ledgers` VALUES ('181', '1', '1', '0', '2016-12-26 00:00:00', '', '185', '2016-12-31 11:31:45', '1');
INSERT INTO `tb_ledgers` VALUES ('182', '1', '1', '0', '2016-12-27 00:00:00', '', '186', '2016-12-31 11:32:12', '1');
INSERT INTO `tb_ledgers` VALUES ('183', '1', '1', '0', '2016-12-28 00:00:00', '', '187', '2016-12-31 11:32:27', '1');
INSERT INTO `tb_ledgers` VALUES ('184', '1', '1', '0', '2016-12-03 00:00:00', '', '188', '2016-12-31 11:34:47', '1');
INSERT INTO `tb_ledgers` VALUES ('185', '1', '1', '0', '2016-12-07 00:00:00', '', '189', '2016-12-31 11:35:27', '1');
INSERT INTO `tb_ledgers` VALUES ('186', '1', '1', '0', '2016-12-08 00:00:00', '', '190', '2016-12-31 11:37:33', '1');
INSERT INTO `tb_ledgers` VALUES ('187', '1', '1', '0', '2016-12-10 00:00:00', '', '191', '2016-12-31 11:37:52', '1');
INSERT INTO `tb_ledgers` VALUES ('188', '1', '1', '0', '2016-12-13 00:00:00', '', '192', '2016-12-31 11:38:13', '1');
INSERT INTO `tb_ledgers` VALUES ('189', '1', '1', '0', '2016-12-16 00:00:00', '', '193', '2016-12-31 11:38:33', '1');
INSERT INTO `tb_ledgers` VALUES ('190', '1', '1', '0', '2016-12-17 00:00:00', '', '194', '2016-12-31 11:38:54', '1');
INSERT INTO `tb_ledgers` VALUES ('191', '1', '1', '0', '2016-12-18 00:00:00', '', '195', '2016-12-31 11:39:11', '1');
INSERT INTO `tb_ledgers` VALUES ('192', '1', '1', '0', '2016-12-23 00:00:00', '', '196', '2016-12-31 11:39:33', '1');
INSERT INTO `tb_ledgers` VALUES ('193', '1', '1', '0', '2016-12-24 00:00:00', '', '197', '2016-12-31 11:39:53', '1');
INSERT INTO `tb_ledgers` VALUES ('194', '1', '1', '0', '2016-12-25 00:00:00', '', '198', '2016-12-31 11:40:11', '1');
INSERT INTO `tb_ledgers` VALUES ('195', '1', '1', '0', '2016-12-27 00:00:00', '', '199', '2016-12-31 11:40:29', '1');
INSERT INTO `tb_ledgers` VALUES ('196', '1', '1', '0', '2016-12-28 00:00:00', '', '200', '2016-12-31 11:40:47', '1');
INSERT INTO `tb_ledgers` VALUES ('197', '1', '1', '0', '2016-12-29 00:00:00', '', '201', '2016-12-31 11:41:10', '1');
INSERT INTO `tb_ledgers` VALUES ('198', '1', '1', '0', '2016-12-30 00:00:00', '', '202', '2016-12-31 11:41:56', '1');
INSERT INTO `tb_ledgers` VALUES ('199', '1', '1', '0', '2016-12-05 00:00:00', '', '203', '2016-12-31 11:43:50', '1');
INSERT INTO `tb_ledgers` VALUES ('200', '1', '1', '0', '2016-12-21 00:00:00', '', '204', '2016-12-31 11:44:13', '1');
INSERT INTO `tb_ledgers` VALUES ('201', '1', '1', '0', '2016-12-22 00:00:00', '', '205', '2016-12-31 11:44:32', '1');
INSERT INTO `tb_ledgers` VALUES ('202', '1', '1', '0', '2016-12-10 00:00:00', '', '206', '2016-12-31 11:49:34', '1');
INSERT INTO `tb_ledgers` VALUES ('203', '1', '1', '0', '2016-12-15 00:00:00', '', '207', '2016-12-31 11:49:57', '1');
INSERT INTO `tb_ledgers` VALUES ('204', '1', '1', '0', '2016-12-17 00:00:00', '', '208', '2016-12-31 11:50:16', '1');
INSERT INTO `tb_ledgers` VALUES ('205', '1', '1', '0', '2016-12-19 00:00:00', '', '209', '2016-12-31 11:50:35', '1');
INSERT INTO `tb_ledgers` VALUES ('206', '1', '1', '0', '2016-12-22 00:00:00', '', '210', '2016-12-31 11:50:53', '1');
INSERT INTO `tb_ledgers` VALUES ('207', '1', '1', '0', '2016-12-05 00:00:00', '', '211', '2016-12-31 11:51:22', '1');
INSERT INTO `tb_ledgers` VALUES ('208', '1', '1', '0', '2016-12-08 00:00:00', '', '212', '2016-12-31 11:51:39', '1');
INSERT INTO `tb_ledgers` VALUES ('209', '1', '1', '0', '2016-12-12 00:00:00', '', '213', '2016-12-31 11:51:53', '1');
INSERT INTO `tb_ledgers` VALUES ('210', '1', '1', '0', '2016-12-13 00:00:00', '', '214', '2016-12-31 11:52:10', '1');
INSERT INTO `tb_ledgers` VALUES ('211', '1', '1', '0', '2016-12-26 00:00:00', '', '215', '2016-12-31 11:52:28', '1');
INSERT INTO `tb_ledgers` VALUES ('212', '1', '1', '0', '2016-12-14 00:00:00', '', '216', '2016-12-31 12:24:32', '1');
INSERT INTO `tb_ledgers` VALUES ('213', '1', '1', '0', '2016-12-16 00:00:00', '', '217', '2016-12-31 12:24:51', '1');
INSERT INTO `tb_ledgers` VALUES ('214', '1', '1', '0', '2016-12-17 00:00:00', '', '218', '2016-12-31 12:25:10', '1');
INSERT INTO `tb_ledgers` VALUES ('215', '1', '1', '0', '2016-12-19 00:00:00', '', '219', '2016-12-31 12:25:33', '1');
INSERT INTO `tb_ledgers` VALUES ('216', '1', '1', '0', '2016-12-21 00:00:00', '', '220', '2016-12-31 12:25:53', '1');
INSERT INTO `tb_ledgers` VALUES ('217', '1', '1', '0', '2016-12-23 00:00:00', '', '221', '2016-12-31 12:26:21', '1');
INSERT INTO `tb_ledgers` VALUES ('218', '1', '1', '0', '2016-12-02 00:00:00', '', '222', '2016-12-31 12:27:17', '1');
INSERT INTO `tb_ledgers` VALUES ('219', '1', '1', '0', '2016-12-03 00:00:00', '', '223', '2016-12-31 12:27:54', '1');
INSERT INTO `tb_ledgers` VALUES ('220', '1', '1', '0', '2016-12-01 00:00:00', '', '224', '2016-12-31 12:42:27', '1');
INSERT INTO `tb_ledgers` VALUES ('221', '1', '1', '0', '2016-12-05 00:00:00', '', '225', '2016-12-31 12:42:47', '1');
INSERT INTO `tb_ledgers` VALUES ('222', '1', '1', '0', '2016-12-06 00:00:00', '', '226', '2016-12-31 12:43:05', '1');
INSERT INTO `tb_ledgers` VALUES ('223', '1', '1', '0', '2016-12-09 00:00:00', '', '227', '2016-12-31 12:43:24', '1');
INSERT INTO `tb_ledgers` VALUES ('224', '1', '1', '0', '2016-12-10 00:00:00', '', '228', '2016-12-31 12:43:42', '1');
INSERT INTO `tb_ledgers` VALUES ('225', '1', '1', '0', '2016-12-13 00:00:00', '', '229', '2016-12-31 12:43:59', '1');
INSERT INTO `tb_ledgers` VALUES ('226', '1', '1', '0', '2016-12-15 00:00:00', '', '230', '2016-12-31 12:44:19', '1');
INSERT INTO `tb_ledgers` VALUES ('227', '1', '1', '0', '2016-12-16 00:00:00', '', '231', '2016-12-31 12:44:39', '1');
INSERT INTO `tb_ledgers` VALUES ('228', '1', '1', '0', '2016-12-20 00:00:00', '', '232', '2016-12-31 12:44:58', '1');
INSERT INTO `tb_ledgers` VALUES ('229', '1', '1', '0', '2016-12-21 00:00:00', '', '233', '2016-12-31 12:45:13', '1');
INSERT INTO `tb_ledgers` VALUES ('230', '1', '1', '0', '2016-12-23 00:00:00', '', '234', '2016-12-31 12:45:30', '1');
INSERT INTO `tb_ledgers` VALUES ('231', '1', '1', '0', '2016-12-25 00:00:00', '', '235', '2016-12-31 12:45:49', '1');
INSERT INTO `tb_ledgers` VALUES ('232', '1', '1', '0', '2016-12-27 00:00:00', '', '236', '2016-12-31 12:46:15', '1');
INSERT INTO `tb_ledgers` VALUES ('233', '1', '1', '0', '2016-12-28 00:00:00', '', '237', '2016-12-31 12:46:37', '1');
INSERT INTO `tb_ledgers` VALUES ('234', '1', '1', '0', '2016-12-29 00:00:00', '', '238', '2016-12-31 12:47:13', '1');
INSERT INTO `tb_ledgers` VALUES ('235', '1', '1', '0', '2016-12-01 00:00:00', '', '239', '2016-12-31 12:49:37', '1');
INSERT INTO `tb_ledgers` VALUES ('236', '1', '1', '0', '2016-12-02 00:00:00', '', '240', '2016-12-31 12:49:56', '1');
INSERT INTO `tb_ledgers` VALUES ('237', '1', '1', '0', '2016-12-03 00:00:00', '', '241', '2016-12-31 12:50:21', '1');
INSERT INTO `tb_ledgers` VALUES ('238', '1', '1', '0', '2016-12-05 00:00:00', '', '242', '2016-12-31 12:50:37', '1');
INSERT INTO `tb_ledgers` VALUES ('239', '1', '1', '0', '2016-12-04 00:00:00', '', '243', '2016-12-31 12:51:50', '1');
INSERT INTO `tb_ledgers` VALUES ('240', '1', '1', '0', '2016-12-07 00:00:00', '', '244', '2016-12-31 12:52:08', '1');
INSERT INTO `tb_ledgers` VALUES ('241', '1', '1', '0', '2016-12-09 00:00:00', '', '245', '2016-12-31 12:52:30', '1');
INSERT INTO `tb_ledgers` VALUES ('242', '1', '1', '0', '2016-12-10 00:00:00', '', '246', '2016-12-31 12:52:46', '1');
INSERT INTO `tb_ledgers` VALUES ('243', '1', '1', '0', '2016-12-12 00:00:00', '', '247', '2016-12-31 12:53:05', '1');
INSERT INTO `tb_ledgers` VALUES ('244', '1', '1', '0', '2016-12-13 00:00:00', '', '248', '2016-12-31 12:53:23', '1');
INSERT INTO `tb_ledgers` VALUES ('245', '1', '1', '0', '2016-12-21 00:00:00', '', '249', '2016-12-31 12:53:44', '1');
INSERT INTO `tb_ledgers` VALUES ('246', '1', '1', '0', '2016-12-25 00:00:00', '', '250', '2016-12-31 12:54:03', '1');
INSERT INTO `tb_ledgers` VALUES ('247', '1', '1', '0', '2016-12-26 00:00:00', '', '251', '2016-12-31 12:54:20', '1');
INSERT INTO `tb_ledgers` VALUES ('248', '1', '1', '0', '2016-12-27 00:00:00', '', '252', '2016-12-31 12:54:39', '1');
INSERT INTO `tb_ledgers` VALUES ('249', '1', '1', '0', '2016-12-29 00:00:00', '', '253', '2016-12-31 12:54:58', '1');
INSERT INTO `tb_ledgers` VALUES ('250', '1', '1', '0', '2016-12-01 00:00:00', '', '254', '2016-12-31 12:56:34', '1');
INSERT INTO `tb_ledgers` VALUES ('251', '1', '1', '0', '2016-12-03 00:00:00', '', '255', '2016-12-31 12:56:53', '1');
INSERT INTO `tb_ledgers` VALUES ('252', '1', '1', '0', '2016-12-05 00:00:00', '', '256', '2016-12-31 12:57:10', '1');
INSERT INTO `tb_ledgers` VALUES ('253', '1', '1', '0', '2016-12-08 00:00:00', '', '257', '2016-12-31 12:57:29', '1');
INSERT INTO `tb_ledgers` VALUES ('254', '1', '1', '0', '2016-12-09 00:00:00', '', '258', '2016-12-31 12:57:44', '1');
INSERT INTO `tb_ledgers` VALUES ('255', '1', '1', '0', '2016-12-10 00:00:00', '', '259', '2016-12-31 12:57:58', '1');
INSERT INTO `tb_ledgers` VALUES ('256', '1', '1', '0', '2016-12-12 00:00:00', '', '260', '2016-12-31 12:58:23', '1');
INSERT INTO `tb_ledgers` VALUES ('257', '1', '1', '0', '2016-12-13 00:00:00', '', '261', '2016-12-31 12:58:38', '1');
INSERT INTO `tb_ledgers` VALUES ('258', '1', '1', '0', '2016-12-14 00:00:00', '', '262', '2016-12-31 12:58:52', '1');
INSERT INTO `tb_ledgers` VALUES ('259', '1', '1', '0', '2016-12-15 00:00:00', '', '263', '2016-12-31 12:59:08', '1');
INSERT INTO `tb_ledgers` VALUES ('260', '1', '1', '0', '2016-12-16 00:00:00', '', '264', '2016-12-31 12:59:28', '1');
INSERT INTO `tb_ledgers` VALUES ('261', '1', '1', '0', '2016-12-17 00:00:00', '', '265', '2016-12-31 12:59:41', '1');
INSERT INTO `tb_ledgers` VALUES ('262', '1', '1', '0', '2016-12-19 00:00:00', '', '266', '2016-12-31 13:00:10', '1');
INSERT INTO `tb_ledgers` VALUES ('263', '1', '1', '0', '2016-12-20 00:00:00', '', '267', '2016-12-31 13:00:24', '1');
INSERT INTO `tb_ledgers` VALUES ('264', '1', '1', '0', '2016-12-21 00:00:00', '', '268', '2016-12-31 13:00:41', '1');
INSERT INTO `tb_ledgers` VALUES ('265', '1', '1', '0', '2016-12-22 00:00:00', '', '269', '2016-12-31 13:01:00', '1');
INSERT INTO `tb_ledgers` VALUES ('266', '1', '1', '0', '2016-12-23 00:00:00', '', '270', '2016-12-31 13:01:22', '1');
INSERT INTO `tb_ledgers` VALUES ('267', '1', '1', '0', '2016-12-24 00:00:00', '', '271', '2016-12-31 13:01:38', '1');
INSERT INTO `tb_ledgers` VALUES ('268', '1', '1', '0', '2016-12-27 00:00:00', '', '272', '2016-12-31 13:08:18', '1');
INSERT INTO `tb_ledgers` VALUES ('269', '1', '1', '0', '2016-12-28 00:00:00', '', '273', '2016-12-31 13:08:37', '1');
INSERT INTO `tb_ledgers` VALUES ('270', '1', '1', '0', '2016-12-29 00:00:00', '', '274', '2016-12-31 13:08:54', '1');
INSERT INTO `tb_ledgers` VALUES ('271', '1', '1', '0', '2016-12-30 00:00:00', '', '275', '2016-12-31 13:09:09', '1');
INSERT INTO `tb_ledgers` VALUES ('272', '1', '1', '0', '2016-12-01 00:00:00', '', '276', '2016-12-31 13:09:49', '1');
INSERT INTO `tb_ledgers` VALUES ('273', '1', '1', '0', '2016-12-02 00:00:00', '', '277', '2016-12-31 13:10:03', '1');
INSERT INTO `tb_ledgers` VALUES ('274', '1', '1', '0', '2016-12-05 00:00:00', '', '278', '2016-12-31 13:10:23', '1');
INSERT INTO `tb_ledgers` VALUES ('275', '1', '1', '0', '2016-12-09 00:00:00', '', '279', '2016-12-31 13:10:41', '1');
INSERT INTO `tb_ledgers` VALUES ('276', '1', '1', '0', '2016-12-10 00:00:00', '', '280', '2016-12-31 13:10:56', '1');
INSERT INTO `tb_ledgers` VALUES ('277', '1', '1', '0', '2016-12-14 00:00:00', '', '281', '2016-12-31 13:11:15', '1');
INSERT INTO `tb_ledgers` VALUES ('278', '1', '1', '0', '2016-12-21 00:00:00', '', '282', '2016-12-31 13:11:33', '1');
INSERT INTO `tb_ledgers` VALUES ('279', '1', '1', '0', '2016-12-23 00:00:00', '', '283', '2016-12-31 13:11:54', '1');
INSERT INTO `tb_ledgers` VALUES ('280', '1', '1', '0', '2016-12-24 00:00:00', '', '284', '2016-12-31 13:12:09', '1');
INSERT INTO `tb_ledgers` VALUES ('281', '1', '1', '0', '2016-12-27 00:00:00', '', '285', '2016-12-31 13:12:26', '1');
INSERT INTO `tb_ledgers` VALUES ('282', '1', '1', '0', '2016-12-28 00:00:00', '', '286', '2016-12-31 13:12:47', '1');
INSERT INTO `tb_ledgers` VALUES ('283', '1', '1', '0', '2016-12-29 00:00:00', '', '287', '2016-12-31 13:13:06', '1');
INSERT INTO `tb_ledgers` VALUES ('284', '1', '1', '0', '2016-12-30 00:00:00', '', '288', '2016-12-31 13:13:21', '1');
INSERT INTO `tb_ledgers` VALUES ('285', '1', '1', '0', '2016-12-02 00:00:00', '', '289', '2016-12-31 13:20:31', '1');
INSERT INTO `tb_ledgers` VALUES ('286', '1', '1', '0', '2016-12-09 00:00:00', '', '290', '2016-12-31 13:20:49', '1');
INSERT INTO `tb_ledgers` VALUES ('287', '1', '1', '0', '2016-12-12 00:00:00', '', '291', '2016-12-31 13:21:07', '1');
INSERT INTO `tb_ledgers` VALUES ('288', '1', '1', '0', '2016-12-14 00:00:00', '', '292', '2016-12-31 13:21:24', '1');
INSERT INTO `tb_ledgers` VALUES ('289', '1', '1', '0', '2016-12-15 00:00:00', '', '293', '2016-12-31 13:21:39', '1');
INSERT INTO `tb_ledgers` VALUES ('290', '1', '1', '0', '2016-12-19 00:00:00', '', '294', '2016-12-31 13:21:59', '1');
INSERT INTO `tb_ledgers` VALUES ('291', '1', '1', '0', '2016-12-21 00:00:00', '', '295', '2016-12-31 13:22:19', '1');
INSERT INTO `tb_ledgers` VALUES ('292', '1', '1', '0', '2016-12-03 00:00:00', '', '296', '2016-12-31 13:27:00', '1');
INSERT INTO `tb_ledgers` VALUES ('293', '1', '1', '0', '2016-12-07 00:00:00', '', '297', '2016-12-31 13:27:24', '1');
INSERT INTO `tb_ledgers` VALUES ('294', '1', '1', '0', '2016-12-08 00:00:00', '', '298', '2016-12-31 13:27:45', '1');
INSERT INTO `tb_ledgers` VALUES ('295', '1', '1', '0', '2016-12-10 00:00:00', '', '299', '2016-12-31 13:28:07', '1');
INSERT INTO `tb_ledgers` VALUES ('296', '1', '1', '0', '2016-12-11 00:00:00', '', '300', '2016-12-31 13:28:22', '1');
INSERT INTO `tb_ledgers` VALUES ('297', '1', '1', '0', '2016-12-13 00:00:00', '', '301', '2016-12-31 13:28:41', '1');
INSERT INTO `tb_ledgers` VALUES ('298', '1', '1', '0', '2016-12-14 00:00:00', '', '302', '2016-12-31 13:28:55', '1');
INSERT INTO `tb_ledgers` VALUES ('299', '1', '1', '0', '2016-12-22 00:00:00', '', '303', '2016-12-31 13:29:19', '1');
INSERT INTO `tb_ledgers` VALUES ('300', '1', '1', '0', '2016-12-23 00:00:00', '', '304', '2016-12-31 13:29:35', '1');
INSERT INTO `tb_ledgers` VALUES ('301', '1', '1', '0', '2016-12-24 00:00:00', '', '305', '2016-12-31 13:29:53', '1');
INSERT INTO `tb_ledgers` VALUES ('302', '1', '1', '0', '2016-12-25 00:00:00', '', '306', '2016-12-31 13:30:20', '1');
INSERT INTO `tb_ledgers` VALUES ('303', '1', '1', '0', '2016-12-26 00:00:00', '', '307', '2016-12-31 13:30:36', '1');
INSERT INTO `tb_ledgers` VALUES ('304', '1', '1', '0', '2016-12-28 00:00:00', '', '308', '2016-12-31 13:31:01', '1');
INSERT INTO `tb_ledgers` VALUES ('305', '1', '1', '0', '2016-12-05 00:00:00', '', '309', '2016-12-31 13:33:32', '1');
INSERT INTO `tb_ledgers` VALUES ('306', '1', '1', '0', '2016-12-07 00:00:00', '', '310', '2016-12-31 13:33:55', '1');
INSERT INTO `tb_ledgers` VALUES ('307', '1', '1', '0', '2016-12-09 00:00:00', '', '311', '2016-12-31 13:34:12', '1');
INSERT INTO `tb_ledgers` VALUES ('308', '1', '1', '0', '2016-12-01 00:00:00', '', '312', '2016-12-31 13:35:05', '1');
INSERT INTO `tb_ledgers` VALUES ('309', '1', '1', '0', '2016-12-03 00:00:00', '', '313', '2016-12-31 13:35:26', '1');
INSERT INTO `tb_ledgers` VALUES ('310', '1', '1', '0', '2016-12-04 00:00:00', '', '314', '2016-12-31 13:35:45', '1');
INSERT INTO `tb_ledgers` VALUES ('311', '1', '1', '0', '2016-12-05 00:00:00', '', '315', '2016-12-31 13:36:06', '1');
INSERT INTO `tb_ledgers` VALUES ('312', '1', '1', '0', '2016-12-06 00:00:00', '', '316', '2016-12-31 13:36:21', '1');
INSERT INTO `tb_ledgers` VALUES ('313', '1', '1', '0', '2016-12-08 00:00:00', '', '317', '2016-12-31 13:36:42', '1');
INSERT INTO `tb_ledgers` VALUES ('314', '1', '1', '0', '2016-12-12 00:00:00', '', '318', '2016-12-31 13:37:00', '1');
INSERT INTO `tb_ledgers` VALUES ('315', '1', '1', '0', '2016-12-16 00:00:00', '', '319', '2016-12-31 13:37:17', '1');
INSERT INTO `tb_ledgers` VALUES ('316', '1', '1', '0', '2016-12-17 00:00:00', '', '320', '2016-12-31 13:37:35', '1');
INSERT INTO `tb_ledgers` VALUES ('317', '1', '1', '0', '2016-12-19 00:00:00', '', '321', '2016-12-31 13:38:04', '1');
INSERT INTO `tb_ledgers` VALUES ('318', '1', '1', '0', '2016-12-20 00:00:00', '', '322', '2016-12-31 13:38:21', '1');
INSERT INTO `tb_ledgers` VALUES ('319', '1', '1', '0', '2016-12-21 00:00:00', '', '323', '2016-12-31 13:38:43', '1');
INSERT INTO `tb_ledgers` VALUES ('320', '1', '1', '0', '2016-12-22 00:00:00', '', '324', '2016-12-31 13:39:00', '1');
INSERT INTO `tb_ledgers` VALUES ('321', '1', '1', '0', '2016-12-23 00:00:00', '', '325', '2016-12-31 13:39:17', '1');
INSERT INTO `tb_ledgers` VALUES ('322', '1', '1', '0', '2016-12-24 00:00:00', '', '326', '2016-12-31 13:39:43', '1');
INSERT INTO `tb_ledgers` VALUES ('323', '1', '1', '0', '2016-12-25 00:00:00', '', '327', '2016-12-31 13:40:01', '1');
INSERT INTO `tb_ledgers` VALUES ('324', '1', '1', '0', '2016-12-26 00:00:00', '', '328', '2016-12-31 13:40:16', '1');
INSERT INTO `tb_ledgers` VALUES ('325', '1', '1', '0', '2016-12-27 00:00:00', '', '329', '2016-12-31 13:40:34', '1');
INSERT INTO `tb_ledgers` VALUES ('326', '1', '1', '0', '2016-12-28 00:00:00', '', '330', '2016-12-31 13:41:00', '1');
INSERT INTO `tb_ledgers` VALUES ('327', '1', '1', '0', '2016-12-04 00:00:00', '', '331', '2016-12-31 13:44:40', '1');
INSERT INTO `tb_ledgers` VALUES ('328', '1', '1', '0', '2016-12-05 00:00:00', '', '332', '2016-12-31 13:44:59', '1');
INSERT INTO `tb_ledgers` VALUES ('329', '1', '1', '0', '2016-12-07 00:00:00', '', '333', '2016-12-31 13:45:39', '1');
INSERT INTO `tb_ledgers` VALUES ('330', '1', '1', '0', '2016-12-09 00:00:00', '', '334', '2016-12-31 13:46:07', '1');
INSERT INTO `tb_ledgers` VALUES ('331', '1', '1', '0', '2016-12-11 00:00:00', '', '335', '2016-12-31 13:46:30', '1');
INSERT INTO `tb_ledgers` VALUES ('332', '1', '1', '0', '2016-12-12 00:00:00', '', '336', '2016-12-31 13:46:54', '1');
INSERT INTO `tb_ledgers` VALUES ('333', '1', '1', '0', '2016-12-13 00:00:00', '', '337', '2016-12-31 13:47:14', '1');
INSERT INTO `tb_ledgers` VALUES ('334', '1', '1', '0', '2016-12-19 00:00:00', '', '338', '2016-12-31 13:47:35', '1');
INSERT INTO `tb_ledgers` VALUES ('335', '1', '1', '0', '2016-12-22 00:00:00', '', '339', '2016-12-31 13:48:00', '1');
INSERT INTO `tb_ledgers` VALUES ('336', '1', '1', '0', '2016-12-23 00:00:00', '', '340', '2016-12-31 13:48:16', '1');
INSERT INTO `tb_ledgers` VALUES ('337', '1', '1', '0', '2016-12-25 00:00:00', '', '341', '2016-12-31 13:48:38', '1');
INSERT INTO `tb_ledgers` VALUES ('338', '1', '1', '0', '2016-12-26 00:00:00', '', '342', '2016-12-31 13:48:57', '1');
INSERT INTO `tb_ledgers` VALUES ('339', '1', '1', '0', '2016-12-28 00:00:00', '', '343', '2016-12-31 13:49:21', '1');
INSERT INTO `tb_ledgers` VALUES ('340', '1', '1', '0', '2016-12-29 00:00:00', '', '344', '2016-12-31 13:49:48', '1');
INSERT INTO `tb_ledgers` VALUES ('341', '1', '1', '0', '2016-12-02 00:00:00', '', '345', '2016-12-31 13:52:13', '1');
INSERT INTO `tb_ledgers` VALUES ('342', '1', '1', '0', '2016-12-05 00:00:00', '', '346', '2016-12-31 13:52:31', '1');
INSERT INTO `tb_ledgers` VALUES ('343', '1', '1', '0', '2016-12-06 00:00:00', '', '347', '2016-12-31 13:52:50', '1');
INSERT INTO `tb_ledgers` VALUES ('344', '1', '1', '0', '2016-12-07 00:00:00', '', '348', '2016-12-31 13:53:14', '1');
INSERT INTO `tb_ledgers` VALUES ('345', '1', '1', '0', '2016-12-08 00:00:00', '', '349', '2016-12-31 13:53:34', '1');
INSERT INTO `tb_ledgers` VALUES ('346', '1', '1', '0', '2016-12-13 00:00:00', '', '350', '2016-12-31 13:54:04', '1');
INSERT INTO `tb_ledgers` VALUES ('347', '1', '1', '0', '2016-12-14 00:00:00', '', '351', '2016-12-31 13:54:40', '1');
INSERT INTO `tb_ledgers` VALUES ('348', '1', '1', '0', '2016-12-15 00:00:00', '', '352', '2016-12-31 14:00:17', '1');
INSERT INTO `tb_ledgers` VALUES ('349', '1', '1', '0', '2016-12-17 00:00:00', '', '353', '2016-12-31 14:00:45', '1');
INSERT INTO `tb_ledgers` VALUES ('350', '1', '1', '0', '2016-12-20 00:00:00', '', '354', '2016-12-31 14:01:47', '1');
INSERT INTO `tb_ledgers` VALUES ('351', '1', '1', '0', '2016-12-21 00:00:00', '', '355', '2016-12-31 14:02:11', '1');
INSERT INTO `tb_ledgers` VALUES ('352', '1', '1', '0', '2016-12-22 00:00:00', '', '356', '2016-12-31 14:02:34', '1');
INSERT INTO `tb_ledgers` VALUES ('353', '1', '1', '0', '2016-12-30 00:00:00', '', '357', '2016-12-31 14:02:54', '1');
INSERT INTO `tb_ledgers` VALUES ('354', '1', '1', '0', '2016-12-02 00:00:00', '', '358', '2016-12-31 14:04:07', '1');
INSERT INTO `tb_ledgers` VALUES ('355', '1', '1', '0', '2016-12-06 00:00:00', '', '359', '2016-12-31 14:12:26', '1');
INSERT INTO `tb_ledgers` VALUES ('356', '1', '1', '0', '2016-12-07 00:00:00', '', '360', '2016-12-31 14:13:29', '1');
INSERT INTO `tb_ledgers` VALUES ('357', '1', '1', '0', '2016-12-08 00:00:00', '', '361', '2016-12-31 14:13:50', '1');
INSERT INTO `tb_ledgers` VALUES ('358', '1', '1', '0', '2016-12-09 00:00:00', '', '362', '2016-12-31 14:14:14', '1');
INSERT INTO `tb_ledgers` VALUES ('359', '1', '1', '0', '2016-12-12 00:00:00', '', '363', '2016-12-31 14:14:35', '1');
INSERT INTO `tb_ledgers` VALUES ('360', '1', '1', '0', '2016-12-15 00:00:00', '', '364', '2016-12-31 14:15:03', '1');
INSERT INTO `tb_ledgers` VALUES ('361', '1', '1', '0', '2016-12-17 00:00:00', '', '365', '2017-01-02 10:28:19', '1');
INSERT INTO `tb_ledgers` VALUES ('362', '1', '1', '0', '2016-12-21 00:00:00', '', '366', '2017-01-02 10:28:49', '1');
INSERT INTO `tb_ledgers` VALUES ('363', '1', '1', '0', '2016-12-22 00:00:00', '', '367', '2017-01-02 10:29:11', '1');
INSERT INTO `tb_ledgers` VALUES ('364', '1', '1', '0', '2016-12-26 00:00:00', '', '368', '2017-01-02 10:29:37', '1');
INSERT INTO `tb_ledgers` VALUES ('365', '1', '1', '0', '2016-12-29 00:00:00', '', '369', '2017-01-02 10:30:30', '1');
INSERT INTO `tb_ledgers` VALUES ('366', '1', '1', '0', '2017-01-30 00:00:00', '', '370', '2017-01-02 10:30:46', '1');
INSERT INTO `tb_ledgers` VALUES ('367', '1', '1', '0', '2016-12-01 00:00:00', '', '371', '2017-01-02 10:31:15', '1');
INSERT INTO `tb_ledgers` VALUES ('368', '1', '1', '0', '2016-12-10 00:00:00', '', '372', '2017-01-02 10:31:39', '1');
INSERT INTO `tb_ledgers` VALUES ('369', '1', '1', '0', '2016-12-16 00:00:00', '', '373', '2017-01-02 10:32:00', '1');
INSERT INTO `tb_ledgers` VALUES ('370', '1', '1', '0', '2016-12-19 00:00:00', '', '374', '2017-01-02 10:32:22', '1');
INSERT INTO `tb_ledgers` VALUES ('371', '1', '1', '0', '2016-12-02 00:00:00', '', '375', '2017-01-02 10:33:07', '1');
INSERT INTO `tb_ledgers` VALUES ('372', '1', '1', '0', '2016-12-05 00:00:00', '', '376', '2017-01-02 10:33:25', '1');
INSERT INTO `tb_ledgers` VALUES ('373', '1', '1', '0', '2016-12-07 00:00:00', '', '377', '2017-01-02 10:33:44', '1');
INSERT INTO `tb_ledgers` VALUES ('374', '1', '1', '0', '2016-12-13 00:00:00', '', '378', '2017-01-02 10:34:01', '1');
INSERT INTO `tb_ledgers` VALUES ('375', '1', '1', '0', '2016-12-16 00:00:00', '', '379', '2017-01-02 10:34:20', '1');
INSERT INTO `tb_ledgers` VALUES ('376', '1', '1', '0', '2016-12-17 00:00:00', '', '380', '2017-01-02 10:34:39', '1');
INSERT INTO `tb_ledgers` VALUES ('377', '1', '1', '0', '2016-12-20 00:00:00', '', '381', '2017-01-02 10:34:58', '1');
INSERT INTO `tb_ledgers` VALUES ('378', '1', '1', '0', '2016-12-22 00:00:00', '', '382', '2017-01-02 10:35:15', '1');
INSERT INTO `tb_ledgers` VALUES ('379', '1', '1', '0', '2016-12-23 00:00:00', '', '383', '2017-01-02 10:35:35', '1');
INSERT INTO `tb_ledgers` VALUES ('380', '1', '1', '0', '2016-12-24 00:00:00', '', '384', '2017-01-02 10:35:53', '1');
INSERT INTO `tb_ledgers` VALUES ('381', '1', '1', '0', '2016-12-27 00:00:00', '', '385', '2017-01-02 10:36:11', '1');
INSERT INTO `tb_ledgers` VALUES ('382', '1', '1', '0', '2016-12-29 00:00:00', '', '386', '2017-01-02 10:36:35', '1');
INSERT INTO `tb_ledgers` VALUES ('383', '1', '1', '0', '2016-12-30 00:00:00', '', '387', '2017-01-02 10:36:52', '1');
INSERT INTO `tb_ledgers` VALUES ('384', '1', '1', '0', '2016-12-01 00:00:00', '', '388', '2017-01-02 10:37:21', '1');
INSERT INTO `tb_ledgers` VALUES ('385', '1', '1', '0', '2016-12-02 00:00:00', '', '389', '2017-01-02 10:37:39', '1');
INSERT INTO `tb_ledgers` VALUES ('386', '1', '1', '0', '2016-12-09 00:00:00', '', '390', '2017-01-02 10:38:06', '1');
INSERT INTO `tb_ledgers` VALUES ('387', '1', '1', '0', '2016-12-10 00:00:00', '', '391', '2017-01-02 10:38:27', '1');
INSERT INTO `tb_ledgers` VALUES ('388', '1', '1', '0', '2016-12-13 00:00:00', '', '392', '2017-01-02 10:38:48', '1');
INSERT INTO `tb_ledgers` VALUES ('389', '1', '1', '0', '2016-12-15 00:00:00', '', '393', '2017-01-02 10:39:08', '1');
INSERT INTO `tb_ledgers` VALUES ('390', '1', '1', '0', '2016-12-18 00:00:00', '', '394', '2017-01-02 10:39:32', '1');
INSERT INTO `tb_ledgers` VALUES ('391', '1', '1', '0', '2016-12-20 00:00:00', '', '395', '2017-01-02 10:39:53', '1');
INSERT INTO `tb_ledgers` VALUES ('392', '1', '1', '0', '2016-12-22 00:00:00', '', '396', '2017-01-02 10:40:14', '1');
INSERT INTO `tb_ledgers` VALUES ('393', '1', '1', '0', '2016-12-23 00:00:00', '', '397', '2017-01-02 10:40:35', '1');
INSERT INTO `tb_ledgers` VALUES ('394', '1', '1', '0', '2016-12-24 00:00:00', '', '398', '2017-01-02 10:40:52', '1');
INSERT INTO `tb_ledgers` VALUES ('395', '1', '1', '0', '2016-12-25 00:00:00', '', '399', '2017-01-02 10:41:23', '1');
INSERT INTO `tb_ledgers` VALUES ('396', '1', '1', '0', '2016-12-26 00:00:00', '', '400', '2017-01-02 10:41:42', '1');
INSERT INTO `tb_ledgers` VALUES ('397', '1', '1', '0', '2016-12-28 00:00:00', '', '401', '2017-01-02 10:42:09', '1');
INSERT INTO `tb_ledgers` VALUES ('398', '1', '1', '0', '2016-12-01 00:00:00', '', '402', '2017-01-02 10:51:14', '1');
INSERT INTO `tb_ledgers` VALUES ('399', '1', '1', '0', '2016-12-02 00:00:00', '', '403', '2017-01-02 10:51:33', '1');
INSERT INTO `tb_ledgers` VALUES ('400', '1', '1', '0', '2016-12-03 00:00:00', '', '404', '2017-01-02 10:51:53', '1');
INSERT INTO `tb_ledgers` VALUES ('401', '1', '1', '0', '2016-12-08 00:00:00', '', '405', '2017-01-02 10:52:12', '1');
INSERT INTO `tb_ledgers` VALUES ('402', '1', '1', '0', '2016-12-09 00:00:00', '', '406', '2017-01-02 10:52:37', '1');
INSERT INTO `tb_ledgers` VALUES ('403', '1', '1', '0', '2016-12-13 00:00:00', '', '407', '2017-01-02 10:52:58', '1');
INSERT INTO `tb_ledgers` VALUES ('404', '1', '1', '0', '2016-12-14 00:00:00', '', '408', '2017-01-02 10:53:16', '1');
INSERT INTO `tb_ledgers` VALUES ('405', '1', '1', '0', '2016-12-15 00:00:00', '', '409', '2017-01-02 10:53:32', '1');
INSERT INTO `tb_ledgers` VALUES ('406', '1', '1', '0', '2016-12-16 00:00:00', '', '410', '2017-01-02 10:53:51', '1');
INSERT INTO `tb_ledgers` VALUES ('407', '1', '1', '0', '2016-12-19 00:00:00', '', '411', '2017-01-02 10:54:09', '1');
INSERT INTO `tb_ledgers` VALUES ('408', '1', '1', '0', '2016-12-20 00:00:00', '', '412', '2017-01-02 10:54:34', '1');
INSERT INTO `tb_ledgers` VALUES ('409', '1', '1', '0', '2016-12-21 00:00:00', '', '413', '2017-01-02 10:54:50', '1');
INSERT INTO `tb_ledgers` VALUES ('410', '1', '1', '0', '2016-12-22 00:00:00', '', '414', '2017-01-02 10:55:10', '1');
INSERT INTO `tb_ledgers` VALUES ('411', '1', '1', '0', '2016-12-23 00:00:00', '', '415', '2017-01-02 10:55:30', '1');
INSERT INTO `tb_ledgers` VALUES ('412', '1', '1', '0', '2016-12-24 00:00:00', '', '416', '2017-01-02 10:55:47', '1');
INSERT INTO `tb_ledgers` VALUES ('413', '1', '1', '0', '2016-12-26 00:00:00', '', '417', '2017-01-02 10:56:08', '1');
INSERT INTO `tb_ledgers` VALUES ('414', '1', '1', '0', '2016-12-28 00:00:00', '', '418', '2017-01-02 10:56:26', '1');
INSERT INTO `tb_ledgers` VALUES ('415', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '419', '2017-01-02 11:05:26', '1');
INSERT INTO `tb_ledgers` VALUES ('416', '1', '1', '0', '2017-01-01 00:00:00', '', '420', '2017-01-02 11:10:25', '1');
INSERT INTO `tb_ledgers` VALUES ('417', '1', '1', '0', '2017-01-02 00:00:00', '', '421', '2017-01-02 11:11:23', '1');
INSERT INTO `tb_ledgers` VALUES ('418', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '422', '2017-01-02 11:16:43', '1');
INSERT INTO `tb_ledgers` VALUES ('419', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '423', '2017-01-02 11:18:40', '1');
INSERT INTO `tb_ledgers` VALUES ('420', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '424', '2017-01-02 11:19:21', '1');
INSERT INTO `tb_ledgers` VALUES ('421', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '425', '2017-01-02 11:20:04', '1');
INSERT INTO `tb_ledgers` VALUES ('422', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '426', '2017-01-02 11:21:41', '1');
INSERT INTO `tb_ledgers` VALUES ('423', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '427', '2017-01-02 11:22:58', '1');
INSERT INTO `tb_ledgers` VALUES ('424', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '428', '2017-01-02 11:24:16', '1');
INSERT INTO `tb_ledgers` VALUES ('425', '1', '1', '0', '2017-01-02 00:00:00', 'Opening Balance', '429', '2017-01-02 11:28:14', '1');
INSERT INTO `tb_ledgers` VALUES ('426', '1', '1', '0', '2016-12-22 00:00:00', '', '430', '2017-01-04 13:13:16', '1');
INSERT INTO `tb_ledgers` VALUES ('427', '1', '1', '0', '2016-12-23 00:00:00', '', '431', '2017-01-04 13:14:22', '1');
INSERT INTO `tb_ledgers` VALUES ('428', '1', '1', '0', '2016-12-24 00:00:00', '', '432', '2017-01-04 13:14:53', '1');
INSERT INTO `tb_ledgers` VALUES ('429', '1', '1', '0', '2016-12-26 00:00:00', '', '433', '2017-01-04 13:15:23', '1');
INSERT INTO `tb_ledgers` VALUES ('430', '1', '1', '0', '2016-12-27 00:00:00', '', '434', '2017-01-04 13:15:59', '1');
INSERT INTO `tb_ledgers` VALUES ('431', '1', '1', '0', '2016-12-28 00:00:00', '', '435', '2017-01-04 13:16:29', '1');
INSERT INTO `tb_ledgers` VALUES ('432', '1', '1', '0', '2016-12-25 00:00:00', '', '436', '2017-01-04 13:18:18', '1');
INSERT INTO `tb_ledgers` VALUES ('433', '1', '1', '0', '2017-01-01 00:00:00', '', '437', '2017-01-04 13:19:48', '1');
INSERT INTO `tb_ledgers` VALUES ('434', '1', '1', '0', '2017-01-02 00:00:00', '', '438', '2017-01-04 13:20:12', '1');
INSERT INTO `tb_ledgers` VALUES ('435', '1', '1', '0', '2016-12-29 00:00:00', '', '439', '2017-01-04 13:21:44', '1');
INSERT INTO `tb_ledgers` VALUES ('436', '1', '1', '0', '2016-12-31 00:00:00', '', '440', '2017-01-04 13:22:16', '1');
INSERT INTO `tb_ledgers` VALUES ('437', '1', '1', '0', '2017-01-02 00:00:00', '', '441', '2017-01-04 13:22:58', '1');
INSERT INTO `tb_ledgers` VALUES ('438', '1', '1', '0', '2016-12-29 00:00:00', '', '442', '2017-01-04 13:24:10', '1');
INSERT INTO `tb_ledgers` VALUES ('439', '1', '1', '0', '2016-12-30 00:00:00', '', '443', '2017-01-04 13:24:52', '1');
INSERT INTO `tb_ledgers` VALUES ('440', '1', '1', '0', '2016-12-31 00:00:00', '', '444', '2017-01-04 13:25:31', '1');
INSERT INTO `tb_ledgers` VALUES ('441', '1', '1', '0', '2017-01-01 00:00:00', '', '445', '2017-01-04 13:26:05', '1');
INSERT INTO `tb_ledgers` VALUES ('442', '1', '1', '0', '2017-01-02 00:00:00', '', '446', '2017-01-04 13:26:33', '1');
INSERT INTO `tb_ledgers` VALUES ('443', '1', '1', '0', '2016-12-29 00:00:00', '', '447', '2017-01-04 13:32:39', '1');
INSERT INTO `tb_ledgers` VALUES ('444', '1', '1', '0', '2016-12-30 00:00:00', '', '448', '2017-01-04 13:33:38', '1');
INSERT INTO `tb_ledgers` VALUES ('445', '1', '1', '0', '2016-12-31 00:00:00', '', '449', '2017-01-04 13:34:21', '1');
INSERT INTO `tb_ledgers` VALUES ('446', '1', '1', '0', '2017-01-02 00:00:00', '', '450', '2017-01-04 13:35:05', '1');
INSERT INTO `tb_ledgers` VALUES ('447', '1', '1', '0', '2017-01-03 00:00:00', '', '451', '2017-01-04 13:35:42', '1');
INSERT INTO `tb_ledgers` VALUES ('448', '1', '1', '0', '2016-12-29 00:00:00', '', '452', '2017-01-04 13:38:27', '1');
INSERT INTO `tb_ledgers` VALUES ('449', '1', '1', '0', '2016-12-31 00:00:00', '', '453', '2017-01-04 13:38:59', '1');
INSERT INTO `tb_ledgers` VALUES ('450', '1', '1', '0', '2016-12-23 00:00:00', '', '454', '2017-01-04 13:41:30', '1');
INSERT INTO `tb_ledgers` VALUES ('451', '1', '1', '0', '2016-12-24 00:00:00', '', '455', '2017-01-04 13:42:03', '1');
INSERT INTO `tb_ledgers` VALUES ('452', '1', '1', '0', '2016-12-25 00:00:00', '', '456', '2017-01-04 13:45:30', '1');
INSERT INTO `tb_ledgers` VALUES ('453', '1', '1', '0', '2016-12-26 00:00:00', '', '457', '2017-01-04 13:46:21', '1');
INSERT INTO `tb_ledgers` VALUES ('454', '1', '1', '0', '2016-12-27 00:00:00', '', '458', '2017-01-05 14:13:49', '1');
INSERT INTO `tb_ledgers` VALUES ('455', '1', '1', '0', '2016-12-28 00:00:00', '', '459', '2017-01-05 14:14:29', '1');
INSERT INTO `tb_ledgers` VALUES ('456', '1', '1', '0', '2016-12-30 00:00:00', '', '460', '2017-01-05 14:15:09', '1');
INSERT INTO `tb_ledgers` VALUES ('457', '1', '1', '0', '2016-12-31 00:00:00', '', '461', '2017-01-05 14:15:49', '1');
INSERT INTO `tb_ledgers` VALUES ('458', '1', '1', '0', '2017-01-01 00:00:00', '', '462', '2017-01-05 14:16:27', '1');
INSERT INTO `tb_ledgers` VALUES ('459', '1', '1', '0', '2017-01-02 00:00:00', '', '463', '2017-01-05 14:17:04', '1');
INSERT INTO `tb_ledgers` VALUES ('460', '1', '1', '0', '2017-01-03 00:00:00', '', '464', '2017-01-05 14:17:41', '1');
INSERT INTO `tb_ledgers` VALUES ('461', '1', '1', '0', '2017-01-04 00:00:00', '', '465', '2017-01-05 14:18:21', '1');
INSERT INTO `tb_ledgers` VALUES ('462', '1', '1', '0', '2017-01-04 00:00:00', '', '466', '2017-01-05 14:23:56', '1');
INSERT INTO `tb_ledgers` VALUES ('463', '1', '1', '0', '2017-01-03 00:00:00', '', '467', '2017-01-05 14:28:58', '1');
INSERT INTO `tb_ledgers` VALUES ('464', '1', '1', '0', '2017-01-04 00:00:00', '', '468', '2017-01-05 14:29:50', '1');
INSERT INTO `tb_ledgers` VALUES ('465', '1', '1', '0', '2016-12-23 00:00:00', '', '469', '2017-01-05 14:32:36', '1');
INSERT INTO `tb_ledgers` VALUES ('466', '1', '1', '0', '2016-12-28 00:00:00', '', '470', '2017-01-05 14:33:36', '1');
INSERT INTO `tb_ledgers` VALUES ('467', '1', '1', '0', '2016-12-29 00:00:00', '', '471', '2017-01-05 14:34:32', '1');
INSERT INTO `tb_ledgers` VALUES ('468', '1', '1', '0', '2016-12-30 00:00:00', '', '472', '2017-01-05 14:35:11', '1');
INSERT INTO `tb_ledgers` VALUES ('469', '1', '1', '0', '2016-12-31 00:00:00', '', '473', '2017-01-05 14:35:54', '1');
INSERT INTO `tb_ledgers` VALUES ('470', '1', '1', '0', '2017-01-02 00:00:00', '', '474', '2017-01-05 14:36:57', '1');
INSERT INTO `tb_ledgers` VALUES ('471', '1', '1', '0', '2017-01-03 00:00:00', '', '475', '2017-01-05 14:37:41', '1');
INSERT INTO `tb_ledgers` VALUES ('472', '1', '1', '0', '2017-01-03 00:00:00', '', '476', '2017-01-05 14:40:33', '1');
INSERT INTO `tb_ledgers` VALUES ('473', '1', '1', '0', '2016-12-23 00:00:00', '', '477', '2017-01-05 14:43:24', '1');
INSERT INTO `tb_ledgers` VALUES ('474', '1', '1', '0', '2016-12-25 00:00:00', '', '478', '2017-01-05 14:44:26', '1');
INSERT INTO `tb_ledgers` VALUES ('475', '1', '1', '0', '2016-12-26 00:00:00', '', '479', '2017-01-05 14:45:07', '1');
INSERT INTO `tb_ledgers` VALUES ('476', '1', '1', '0', '2016-12-27 00:00:00', '', '480', '2017-01-05 14:46:02', '1');
INSERT INTO `tb_ledgers` VALUES ('477', '1', '1', '0', '2016-12-28 00:00:00', '', '481', '2017-01-05 14:46:47', '1');
INSERT INTO `tb_ledgers` VALUES ('478', '1', '1', '0', '2016-12-29 00:00:00', '', '482', '2017-01-05 14:47:25', '1');
INSERT INTO `tb_ledgers` VALUES ('479', '1', '1', '0', '2017-01-02 00:00:00', '', '483', '2017-01-05 14:48:39', '1');
INSERT INTO `tb_ledgers` VALUES ('480', '1', '1', '0', '2017-01-03 00:00:00', '', '484', '2017-01-05 14:49:21', '1');
INSERT INTO `tb_ledgers` VALUES ('481', '1', '1', '0', '2017-01-04 00:00:00', '', '485', '2017-01-05 14:49:56', '1');
INSERT INTO `tb_ledgers` VALUES ('482', '1', '1', '0', '2016-12-23 00:00:00', '', '486', '2017-01-05 14:58:08', '1');
INSERT INTO `tb_ledgers` VALUES ('483', '1', '1', '0', '2016-12-26 00:00:00', '', '487', '2017-01-05 14:59:21', '1');
INSERT INTO `tb_ledgers` VALUES ('484', '1', '1', '0', '2017-01-03 00:00:00', '', '488', '2017-01-05 15:00:09', '1');
INSERT INTO `tb_ledgers` VALUES ('485', '1', '1', '0', '2016-12-23 00:00:00', '', '489', '2017-01-05 15:04:54', '1');
INSERT INTO `tb_ledgers` VALUES ('486', '1', '1', '0', '2016-12-24 00:00:00', '', '490', '2017-01-05 15:05:53', '1');
INSERT INTO `tb_ledgers` VALUES ('487', '1', '1', '0', '2016-12-25 00:00:00', '', '491', '2017-01-05 15:06:33', '1');
INSERT INTO `tb_ledgers` VALUES ('488', '1', '1', '0', '2016-12-26 00:00:00', '', '492', '2017-01-05 15:07:20', '1');
INSERT INTO `tb_ledgers` VALUES ('489', '1', '1', '0', '2016-12-27 00:00:00', '', '493', '2017-01-05 15:07:58', '1');
INSERT INTO `tb_ledgers` VALUES ('490', '1', '1', '0', '2016-12-28 00:00:00', '', '494', '2017-01-05 15:08:46', '1');
INSERT INTO `tb_ledgers` VALUES ('491', '1', '1', '0', '2016-12-29 00:00:00', '', '495', '2017-01-05 15:09:27', '1');
INSERT INTO `tb_ledgers` VALUES ('492', '1', '1', '0', '2016-12-30 00:00:00', '', '496', '2017-01-05 15:10:05', '1');
INSERT INTO `tb_ledgers` VALUES ('493', '1', '1', '0', '2016-12-31 00:00:00', '', '497', '2017-01-05 15:10:49', '1');
INSERT INTO `tb_ledgers` VALUES ('494', '1', '1', '0', '2017-01-01 00:00:00', '', '498', '2017-01-05 15:11:28', '1');
INSERT INTO `tb_ledgers` VALUES ('495', '1', '1', '0', '2017-01-02 00:00:00', '', '499', '2017-01-05 15:12:03', '1');
INSERT INTO `tb_ledgers` VALUES ('496', '1', '1', '0', '2017-01-03 00:00:00', '', '500', '2017-01-05 15:12:41', '1');
INSERT INTO `tb_ledgers` VALUES ('497', '1', '1', '0', '2017-01-04 00:00:00', '', '501', '2017-01-05 15:13:14', '1');
INSERT INTO `tb_ledgers` VALUES ('498', '1', '1', '0', '2017-01-05 00:00:00', 'Opening Balance', '502', '2017-01-05 15:21:35', '1');
INSERT INTO `tb_ledgers` VALUES ('499', '1', '1', '0', '2016-12-30 00:00:00', '', '503', '2017-01-11 11:54:06', '1');
INSERT INTO `tb_ledgers` VALUES ('500', '1', '1', '0', '2016-12-31 00:00:00', '', '504', '2017-01-11 11:54:23', '1');
INSERT INTO `tb_ledgers` VALUES ('501', '1', '1', '0', '2017-01-01 00:00:00', '', '505', '2017-01-11 11:54:47', '1');
INSERT INTO `tb_ledgers` VALUES ('502', '1', '1', '0', '2017-01-03 00:00:00', '', '506', '2017-01-11 11:55:10', '1');
INSERT INTO `tb_ledgers` VALUES ('503', '1', '1', '0', '2017-01-04 00:00:00', '', '507', '2017-01-11 11:55:25', '1');
INSERT INTO `tb_ledgers` VALUES ('504', '1', '1', '0', '2017-01-05 00:00:00', '', '508', '2017-01-11 11:55:39', '1');
INSERT INTO `tb_ledgers` VALUES ('505', '1', '1', '0', '2017-01-06 00:00:00', '', '509', '2017-01-11 11:56:05', '1');
INSERT INTO `tb_ledgers` VALUES ('506', '1', '1', '0', '2017-01-07 00:00:00', '', '510', '2017-01-11 12:00:26', '1');
INSERT INTO `tb_ledgers` VALUES ('507', '1', '1', '0', '2017-01-08 00:00:00', '', '511', '2017-01-11 12:00:45', '1');
INSERT INTO `tb_ledgers` VALUES ('508', '1', '1', '0', '2017-01-09 00:00:00', '', '512', '2017-01-11 12:02:15', '1');
INSERT INTO `tb_ledgers` VALUES ('509', '1', '1', '0', '2016-12-30 00:00:00', '', '513', '2017-01-11 12:31:59', '1');
INSERT INTO `tb_ledgers` VALUES ('510', '1', '1', '0', '2016-12-31 00:00:00', '', '514', '2017-01-11 12:32:14', '1');
INSERT INTO `tb_ledgers` VALUES ('511', '1', '1', '0', '2017-01-01 00:00:00', '', '515', '2017-01-11 12:32:32', '1');
INSERT INTO `tb_ledgers` VALUES ('512', '1', '1', '0', '2017-01-02 00:00:00', '', '516', '2017-01-11 12:32:55', '1');
INSERT INTO `tb_ledgers` VALUES ('513', '1', '1', '0', '2017-01-03 00:00:00', '', '517', '2017-01-11 12:34:07', '1');
INSERT INTO `tb_ledgers` VALUES ('514', '1', '1', '0', '2017-01-04 00:00:00', '', '518', '2017-01-11 12:34:23', '1');
INSERT INTO `tb_ledgers` VALUES ('515', '1', '1', '0', '2017-01-05 00:00:00', '', '519', '2017-01-11 12:34:46', '1');
INSERT INTO `tb_ledgers` VALUES ('516', '1', '1', '0', '2017-01-06 00:00:00', '', '520', '2017-01-11 12:34:59', '1');
INSERT INTO `tb_ledgers` VALUES ('517', '1', '1', '0', '2017-01-07 00:00:00', '', '521', '2017-01-11 12:35:14', '1');
INSERT INTO `tb_ledgers` VALUES ('518', '1', '1', '0', '2017-01-08 00:00:00', '', '522', '2017-01-11 12:35:29', '1');
INSERT INTO `tb_ledgers` VALUES ('519', '1', '1', '0', '2017-01-09 00:00:00', '', '523', '2017-01-11 12:35:42', '1');
INSERT INTO `tb_ledgers` VALUES ('520', '1', '1', '0', '2017-01-03 00:00:00', '', '524', '2017-01-11 12:36:44', '1');
INSERT INTO `tb_ledgers` VALUES ('521', '1', '1', '0', '2017-01-04 00:00:00', '', '525', '2017-01-11 12:37:01', '1');
INSERT INTO `tb_ledgers` VALUES ('522', '1', '1', '0', '2017-01-06 00:00:00', '', '526', '2017-01-11 12:37:20', '1');
INSERT INTO `tb_ledgers` VALUES ('523', '1', '1', '0', '2017-01-09 00:00:00', '', '527', '2017-01-11 12:37:42', '1');
INSERT INTO `tb_ledgers` VALUES ('524', '1', '1', '0', '2017-01-04 00:00:00', '', '528', '2017-01-11 12:38:48', '1');
INSERT INTO `tb_ledgers` VALUES ('525', '1', '1', '0', '2017-01-06 00:00:00', '', '529', '2017-01-11 12:39:05', '1');
INSERT INTO `tb_ledgers` VALUES ('526', '1', '1', '0', '2016-12-30 00:00:00', '', '530', '2017-01-11 12:40:27', '1');
INSERT INTO `tb_ledgers` VALUES ('527', '1', '1', '0', '2016-12-31 00:00:00', '', '531', '2017-01-11 12:40:46', '1');
INSERT INTO `tb_ledgers` VALUES ('528', '1', '1', '0', '2017-01-01 00:00:00', '', '532', '2017-01-11 12:40:58', '1');
INSERT INTO `tb_ledgers` VALUES ('529', '1', '1', '0', '2017-01-02 00:00:00', '', '533', '2017-01-11 12:41:13', '1');
INSERT INTO `tb_ledgers` VALUES ('530', '1', '1', '0', '2017-01-03 00:00:00', '', '534', '2017-01-11 12:41:27', '1');
INSERT INTO `tb_ledgers` VALUES ('531', '1', '1', '0', '2017-01-04 00:00:00', '', '535', '2017-01-11 12:41:42', '1');
INSERT INTO `tb_ledgers` VALUES ('532', '1', '1', '0', '2017-01-05 00:00:00', '', '536', '2017-01-11 12:41:56', '1');
INSERT INTO `tb_ledgers` VALUES ('533', '1', '1', '0', '2017-01-06 00:00:00', '', '537', '2017-01-11 12:42:10', '1');
INSERT INTO `tb_ledgers` VALUES ('534', '1', '1', '0', '2017-01-07 00:00:00', '', '538', '2017-01-11 12:42:34', '1');
INSERT INTO `tb_ledgers` VALUES ('535', '1', '1', '0', '2017-01-08 00:00:00', '', '539', '2017-01-11 12:42:56', '1');
INSERT INTO `tb_ledgers` VALUES ('536', '1', '1', '0', '2017-01-09 00:00:00', '', '540', '2017-01-11 12:43:15', '1');
INSERT INTO `tb_ledgers` VALUES ('537', '1', '1', '0', '2016-12-21 00:00:00', '', '541', '2017-01-12 10:24:05', '1');
INSERT INTO `tb_ledgers` VALUES ('538', '1', '1', '0', '2016-12-22 00:00:00', '', '542', '2017-01-12 10:24:25', '1');
INSERT INTO `tb_ledgers` VALUES ('539', '1', '1', '0', '2016-12-25 00:00:00', '', '543', '2017-01-12 10:24:47', '1');
INSERT INTO `tb_ledgers` VALUES ('540', '1', '1', '0', '2016-12-26 00:00:00', '', '544', '2017-01-12 10:25:10', '1');
INSERT INTO `tb_ledgers` VALUES ('541', '1', '1', '0', '2016-12-27 00:00:00', '', '545', '2017-01-12 10:25:31', '1');
INSERT INTO `tb_ledgers` VALUES ('542', '1', '1', '0', '2017-01-02 00:00:00', '', '546', '2017-01-12 10:25:59', '1');
INSERT INTO `tb_ledgers` VALUES ('543', '1', '1', '0', '2017-01-03 00:00:00', '', '547', '2017-01-12 10:26:21', '1');
INSERT INTO `tb_ledgers` VALUES ('544', '1', '1', '0', '2017-01-05 00:00:00', '', '548', '2017-01-12 10:26:46', '1');
INSERT INTO `tb_ledgers` VALUES ('545', '1', '1', '0', '2017-01-07 00:00:00', '', '549', '2017-01-12 10:27:11', '1');
INSERT INTO `tb_ledgers` VALUES ('546', '1', '1', '0', '2017-01-09 00:00:00', '', '550', '2017-01-12 10:27:32', '1');
INSERT INTO `tb_ledgers` VALUES ('547', '1', '1', '0', '2016-12-31 00:00:00', '', '551', '2017-01-12 10:34:07', '1');
INSERT INTO `tb_ledgers` VALUES ('548', '1', '1', '0', '2017-01-02 00:00:00', '', '552', '2017-01-12 10:34:56', '1');
INSERT INTO `tb_ledgers` VALUES ('549', '1', '1', '0', '2017-01-06 00:00:00', '', '553', '2017-01-12 10:35:17', '1');
INSERT INTO `tb_ledgers` VALUES ('550', '1', '1', '0', '2017-01-07 00:00:00', '', '554', '2017-01-12 10:35:38', '1');
INSERT INTO `tb_ledgers` VALUES ('551', '1', '1', '0', '2017-01-09 00:00:00', '', '555', '2017-01-12 10:35:59', '1');
INSERT INTO `tb_ledgers` VALUES ('552', '1', '1', '0', '2016-12-31 00:00:00', '', '556', '2017-01-12 10:37:19', '1');
INSERT INTO `tb_ledgers` VALUES ('553', '1', '1', '0', '2017-01-01 00:00:00', '', '557', '2017-01-12 10:37:39', '1');
INSERT INTO `tb_ledgers` VALUES ('554', '1', '1', '0', '2017-01-04 00:00:00', '', '558', '2017-01-12 10:38:25', '1');
INSERT INTO `tb_ledgers` VALUES ('555', '1', '1', '0', '2017-01-05 00:00:00', '', '559', '2017-01-12 10:38:45', '1');
INSERT INTO `tb_ledgers` VALUES ('556', '1', '1', '0', '2017-01-07 00:00:00', '', '560', '2017-01-12 10:39:09', '1');
INSERT INTO `tb_ledgers` VALUES ('557', '1', '1', '0', '2017-01-10 00:00:00', '', '561', '2017-01-12 10:39:32', '1');
INSERT INTO `tb_ledgers` VALUES ('558', '1', '1', '0', '2016-12-31 00:00:00', '', '562', '2017-01-12 10:43:28', '1');
INSERT INTO `tb_ledgers` VALUES ('559', '1', '1', '0', '2017-01-02 00:00:00', '', '563', '2017-01-12 10:44:01', '1');
INSERT INTO `tb_ledgers` VALUES ('560', '1', '1', '0', '2017-01-03 00:00:00', '', '564', '2017-01-12 10:44:19', '1');
INSERT INTO `tb_ledgers` VALUES ('561', '1', '1', '0', '2017-01-04 00:00:00', '', '565', '2017-01-12 10:44:39', '1');
INSERT INTO `tb_ledgers` VALUES ('562', '1', '1', '0', '2017-01-05 00:00:00', '', '566', '2017-01-12 10:44:56', '1');
INSERT INTO `tb_ledgers` VALUES ('563', '1', '1', '0', '2017-01-06 00:00:00', '', '567', '2017-01-12 10:45:14', '1');
INSERT INTO `tb_ledgers` VALUES ('564', '1', '1', '0', '2017-01-09 00:00:00', '', '568', '2017-01-12 10:45:36', '1');
INSERT INTO `tb_ledgers` VALUES ('565', '1', '1', '0', '2017-01-10 00:00:00', '', '569', '2017-01-12 10:45:56', '1');
INSERT INTO `tb_ledgers` VALUES ('566', '1', '1', '0', '2017-01-03 00:00:00', '', '570', '2017-01-12 10:48:03', '1');
INSERT INTO `tb_ledgers` VALUES ('567', '1', '1', '0', '2017-01-02 00:00:00', '', '571', '2017-01-12 10:49:27', '1');
INSERT INTO `tb_ledgers` VALUES ('568', '1', '1', '0', '2017-01-03 00:00:00', '', '572', '2017-01-12 10:49:41', '1');
INSERT INTO `tb_ledgers` VALUES ('569', '1', '1', '0', '2017-01-06 00:00:00', '', '573', '2017-01-12 10:50:00', '1');
INSERT INTO `tb_ledgers` VALUES ('570', '1', '1', '0', '2016-12-30 00:00:00', '', '574', '2017-01-12 10:53:22', '1');
INSERT INTO `tb_ledgers` VALUES ('571', '1', '1', '0', '2016-12-31 00:00:00', '', '575', '2017-01-12 10:53:41', '1');
INSERT INTO `tb_ledgers` VALUES ('572', '1', '1', '0', '2017-01-05 00:00:00', '', '576', '2017-01-12 10:54:08', '1');
INSERT INTO `tb_ledgers` VALUES ('573', '1', '1', '0', '2017-01-06 00:00:00', '', '577', '2017-01-12 10:54:26', '1');
INSERT INTO `tb_ledgers` VALUES ('574', '1', '1', '0', '2017-01-08 00:00:00', '', '578', '2017-01-12 10:54:45', '1');
INSERT INTO `tb_ledgers` VALUES ('575', '1', '1', '0', '2016-12-31 00:00:00', '', '579', '2017-01-12 10:56:19', '1');
INSERT INTO `tb_ledgers` VALUES ('576', '1', '1', '0', '2017-01-04 00:00:00', '', '580', '2017-01-12 10:56:46', '1');
INSERT INTO `tb_ledgers` VALUES ('577', '1', '1', '0', '2017-01-05 00:00:00', '', '581', '2017-01-12 10:57:18', '1');
INSERT INTO `tb_ledgers` VALUES ('578', '1', '1', '0', '2017-01-07 00:00:00', '', '582', '2017-01-12 10:57:37', '1');
INSERT INTO `tb_ledgers` VALUES ('579', '1', '1', '0', '2017-01-09 00:00:00', '', '583', '2017-01-12 10:58:00', '1');
INSERT INTO `tb_ledgers` VALUES ('580', '1', '1', '0', '2016-12-31 00:00:00', '', '584', '2017-01-12 10:59:24', '1');
INSERT INTO `tb_ledgers` VALUES ('581', '1', '1', '0', '2017-01-07 00:00:00', '', '585', '2017-01-12 10:59:46', '1');
INSERT INTO `tb_ledgers` VALUES ('582', '1', '1', '0', '2017-01-09 00:00:00', '', '586', '2017-01-12 11:00:05', '1');
INSERT INTO `tb_ledgers` VALUES ('583', '1', '1', '0', '2017-01-04 00:00:00', '', '587', '2017-01-12 11:03:46', '1');
INSERT INTO `tb_ledgers` VALUES ('584', '1', '1', '0', '2017-01-05 00:00:00', '', '588', '2017-01-12 11:04:07', '1');
INSERT INTO `tb_ledgers` VALUES ('585', '1', '1', '0', '2017-01-07 00:00:00', '', '589', '2017-01-12 11:04:25', '1');
INSERT INTO `tb_ledgers` VALUES ('586', '1', '1', '0', '2016-12-30 00:00:00', '', '590', '2017-01-12 11:10:57', '1');
INSERT INTO `tb_ledgers` VALUES ('587', '1', '1', '0', '2017-01-09 00:00:00', '', '591', '2017-01-12 11:14:17', '1');
INSERT INTO `tb_ledgers` VALUES ('588', '1', '1', '0', '2017-01-10 00:00:00', '', '592', '2017-01-12 11:14:33', '1');
INSERT INTO `tb_ledgers` VALUES ('589', '1', '1', '0', '2016-12-30 00:00:00', '', '593', '2017-01-12 11:16:09', '1');
INSERT INTO `tb_ledgers` VALUES ('590', '1', '1', '0', '2016-12-31 00:00:00', '', '594', '2017-01-12 11:16:28', '1');
INSERT INTO `tb_ledgers` VALUES ('591', '1', '1', '0', '2017-01-01 00:00:00', '', '595', '2017-01-12 11:17:02', '1');
INSERT INTO `tb_ledgers` VALUES ('592', '1', '1', '0', '2017-01-02 00:00:00', '', '596', '2017-01-12 11:17:21', '1');
INSERT INTO `tb_ledgers` VALUES ('593', '1', '1', '0', '2017-01-03 00:00:00', '', '597', '2017-01-12 11:17:40', '1');
INSERT INTO `tb_ledgers` VALUES ('594', '1', '1', '0', '2017-01-04 00:00:00', '', '598', '2017-01-12 11:18:01', '1');
INSERT INTO `tb_ledgers` VALUES ('595', '1', '1', '0', '2017-01-05 00:00:00', '', '599', '2017-01-12 11:18:15', '1');
INSERT INTO `tb_ledgers` VALUES ('596', '1', '1', '0', '2017-01-06 00:00:00', '', '600', '2017-01-12 11:18:32', '1');
INSERT INTO `tb_ledgers` VALUES ('597', '1', '1', '0', '2017-01-07 00:00:00', '', '601', '2017-01-12 11:18:56', '1');
INSERT INTO `tb_ledgers` VALUES ('598', '1', '1', '0', '2017-01-13 00:00:00', '', '602', '2017-01-16 11:14:31', '1');
INSERT INTO `tb_ledgers` VALUES ('599', '1', '1', '0', '2017-01-08 00:00:00', '', '603', '2017-01-16 11:23:44', '1');
INSERT INTO `tb_ledgers` VALUES ('600', '1', '1', '0', '2017-01-09 00:00:00', '', '604', '2017-01-16 11:23:57', '1');
INSERT INTO `tb_ledgers` VALUES ('601', '1', '1', '0', '2017-01-11 00:00:00', '', '605', '2017-01-16 11:24:13', '1');
INSERT INTO `tb_ledgers` VALUES ('602', '1', '1', '0', '2017-01-31 00:00:00', '', '606', '2017-01-16 11:26:04', '1');
INSERT INTO `tb_ledgers` VALUES ('603', '1', '1', '0', '2017-01-01 00:00:00', '', '607', '2017-01-16 11:26:24', '1');
INSERT INTO `tb_ledgers` VALUES ('604', '1', '1', '0', '2017-01-02 00:00:00', '', '608', '2017-01-16 11:26:42', '1');
INSERT INTO `tb_ledgers` VALUES ('605', '1', '1', '0', '2017-01-03 00:00:00', '', '609', '2017-01-16 11:26:59', '1');
INSERT INTO `tb_ledgers` VALUES ('606', '1', '1', '0', '2017-01-04 00:00:00', '', '610', '2017-01-16 11:27:20', '1');
INSERT INTO `tb_ledgers` VALUES ('607', '1', '1', '0', '2017-01-05 00:00:00', '', '611', '2017-01-16 11:27:37', '1');
INSERT INTO `tb_ledgers` VALUES ('608', '1', '1', '0', '2017-01-06 00:00:00', '', '612', '2017-01-16 11:27:54', '1');
INSERT INTO `tb_ledgers` VALUES ('609', '1', '1', '0', '2017-01-07 00:00:00', '', '613', '2017-01-16 11:28:32', '1');
INSERT INTO `tb_ledgers` VALUES ('610', '1', '1', '0', '2017-01-08 00:00:00', '', '614', '2017-01-16 11:28:50', '1');
INSERT INTO `tb_ledgers` VALUES ('611', '1', '1', '0', '2017-01-09 00:00:00', '', '615', '2017-01-16 11:29:14', '1');
INSERT INTO `tb_ledgers` VALUES ('612', '1', '1', '0', '2017-01-11 00:00:00', '', '616', '2017-01-16 11:29:32', '1');
INSERT INTO `tb_ledgers` VALUES ('613', '1', '1', '0', '2016-12-31 00:00:00', '', '617', '2017-01-16 11:30:51', '1');
INSERT INTO `tb_ledgers` VALUES ('614', '1', '1', '0', '2017-01-02 00:00:00', '', '618', '2017-01-16 11:31:10', '1');
INSERT INTO `tb_ledgers` VALUES ('615', '1', '1', '0', '2017-01-03 00:00:00', '', '619', '2017-01-16 11:31:25', '1');
INSERT INTO `tb_ledgers` VALUES ('616', '1', '1', '0', '2017-01-04 00:00:00', '', '620', '2017-01-16 11:31:42', '1');
INSERT INTO `tb_ledgers` VALUES ('617', '1', '1', '0', '2017-01-05 00:00:00', '', '621', '2017-01-16 11:31:58', '1');
INSERT INTO `tb_ledgers` VALUES ('618', '1', '1', '0', '2017-01-06 00:00:00', '', '622', '2017-01-16 11:32:17', '1');
INSERT INTO `tb_ledgers` VALUES ('619', '1', '1', '0', '2017-01-07 00:00:00', '', '623', '2017-01-16 11:32:35', '1');
INSERT INTO `tb_ledgers` VALUES ('620', '1', '1', '0', '2017-01-09 00:00:00', '', '624', '2017-01-16 11:32:52', '1');
INSERT INTO `tb_ledgers` VALUES ('621', '1', '1', '0', '2017-01-10 00:00:00', '', '625', '2017-01-16 11:33:09', '1');
INSERT INTO `tb_ledgers` VALUES ('622', '1', '1', '0', '2017-01-13 00:00:00', '', '626', '2017-01-16 11:33:29', '1');
INSERT INTO `tb_ledgers` VALUES ('623', '1', '1', '0', '2017-01-14 00:00:00', '', '627', '2017-01-16 11:33:44', '1');
INSERT INTO `tb_ledgers` VALUES ('624', '1', '1', '0', '2017-01-02 00:00:00', '', '628', '2017-01-17 12:22:27', '1');
INSERT INTO `tb_ledgers` VALUES ('625', '1', '1', '0', '2017-01-03 00:00:00', '', '629', '2017-01-17 12:22:47', '1');
INSERT INTO `tb_ledgers` VALUES ('626', '1', '1', '0', '2017-01-04 00:00:00', '', '630', '2017-01-17 12:23:03', '1');
INSERT INTO `tb_ledgers` VALUES ('627', '1', '1', '0', '2017-01-06 00:00:00', '', '631', '2017-01-17 12:23:28', '1');
INSERT INTO `tb_ledgers` VALUES ('628', '1', '1', '0', '2017-01-09 00:00:00', '', '632', '2017-01-17 12:23:45', '1');
INSERT INTO `tb_ledgers` VALUES ('629', '1', '1', '0', '2017-01-11 00:00:00', '', '633', '2017-01-17 12:24:03', '1');
INSERT INTO `tb_ledgers` VALUES ('630', '1', '1', '0', '2017-01-12 00:00:00', '', '634', '2017-01-17 12:26:20', '1');
INSERT INTO `tb_ledgers` VALUES ('631', '1', '1', '0', '2017-01-13 00:00:00', '', '635', '2017-01-17 12:26:44', '1');
INSERT INTO `tb_ledgers` VALUES ('632', '1', '1', '0', '2017-01-14 00:00:00', '', '636', '2017-01-17 12:27:13', '1');
INSERT INTO `tb_ledgers` VALUES ('633', '1', '1', '0', '2017-01-12 00:00:00', '', '637', '2017-01-17 12:31:21', '1');
INSERT INTO `tb_ledgers` VALUES ('634', '1', '1', '0', '2017-01-03 00:00:00', '', '638', '2017-01-17 12:33:19', '1');
INSERT INTO `tb_ledgers` VALUES ('635', '1', '1', '0', '2017-01-04 00:00:00', '', '639', '2017-01-17 12:33:40', '1');
INSERT INTO `tb_ledgers` VALUES ('636', '1', '1', '0', '2017-01-06 00:00:00', '', '640', '2017-01-17 12:33:58', '1');
INSERT INTO `tb_ledgers` VALUES ('637', '1', '1', '0', '2017-01-11 00:00:00', '', '641', '2017-01-17 12:34:40', '1');
INSERT INTO `tb_ledgers` VALUES ('638', '1', '1', '0', '2017-01-13 00:00:00', '', '642', '2017-01-17 12:35:03', '1');
INSERT INTO `tb_ledgers` VALUES ('639', '1', '1', '0', '2017-01-14 00:00:00', '', '643', '2017-01-17 12:35:19', '1');
INSERT INTO `tb_ledgers` VALUES ('640', '1', '1', '0', '2016-12-31 00:00:00', '', '644', '2017-01-17 12:36:54', '1');
INSERT INTO `tb_ledgers` VALUES ('641', '1', '1', '0', '2017-01-01 00:00:00', '', '645', '2017-01-17 12:37:20', '1');
INSERT INTO `tb_ledgers` VALUES ('642', '1', '1', '0', '2017-01-02 00:00:00', '', '646', '2017-01-17 12:37:33', '1');
INSERT INTO `tb_ledgers` VALUES ('643', '1', '1', '0', '2017-01-03 00:00:00', '', '647', '2017-01-17 12:37:47', '1');
INSERT INTO `tb_ledgers` VALUES ('644', '1', '1', '0', '2017-01-04 00:00:00', '', '648', '2017-01-17 12:38:02', '1');
INSERT INTO `tb_ledgers` VALUES ('645', '1', '1', '0', '2017-01-05 00:00:00', '', '649', '2017-01-17 12:38:16', '1');
INSERT INTO `tb_ledgers` VALUES ('646', '1', '1', '0', '2017-01-06 00:00:00', '', '650', '2017-01-17 12:38:33', '1');
INSERT INTO `tb_ledgers` VALUES ('647', '1', '1', '0', '2017-01-11 00:00:00', '', '651', '2017-01-17 12:38:55', '1');
INSERT INTO `tb_ledgers` VALUES ('648', '1', '1', '0', '2017-01-13 00:00:00', '', '652', '2017-01-17 12:39:11', '1');
INSERT INTO `tb_ledgers` VALUES ('649', '1', '1', '0', '2017-01-14 00:00:00', '', '653', '2017-01-17 12:39:32', '1');
INSERT INTO `tb_ledgers` VALUES ('650', '1', '1', '0', '2017-01-15 00:00:00', '', '654', '2017-01-17 12:39:48', '1');
INSERT INTO `tb_ledgers` VALUES ('651', '1', '1', '0', '2017-01-16 00:00:00', '', '655', '2017-01-17 12:40:05', '1');
INSERT INTO `tb_ledgers` VALUES ('652', '1', '1', '0', '2016-12-30 00:00:00', '', '656', '2017-01-17 12:42:14', '1');
INSERT INTO `tb_ledgers` VALUES ('653', '1', '1', '0', '2016-12-31 00:00:00', '', '657', '2017-01-17 12:42:29', '1');
INSERT INTO `tb_ledgers` VALUES ('654', '1', '1', '0', '2017-01-01 00:00:00', '', '658', '2017-01-17 12:42:52', '1');
INSERT INTO `tb_ledgers` VALUES ('655', '1', '1', '0', '2017-01-02 00:00:00', '', '659', '2017-01-17 12:43:11', '1');
INSERT INTO `tb_ledgers` VALUES ('656', '1', '1', '0', '2017-01-03 00:00:00', '', '660', '2017-01-17 12:43:24', '1');
INSERT INTO `tb_ledgers` VALUES ('657', '1', '1', '0', '2017-01-04 00:00:00', '', '661', '2017-01-17 12:43:37', '1');
INSERT INTO `tb_ledgers` VALUES ('658', '1', '1', '0', '2017-01-05 00:00:00', '', '662', '2017-01-17 12:43:50', '1');
INSERT INTO `tb_ledgers` VALUES ('659', '1', '1', '0', '2017-01-06 00:00:00', '', '663', '2017-01-17 12:44:03', '1');
INSERT INTO `tb_ledgers` VALUES ('660', '1', '1', '0', '2017-01-07 00:00:00', '', '664', '2017-01-17 12:44:25', '1');
INSERT INTO `tb_ledgers` VALUES ('661', '1', '1', '0', '2017-01-09 00:00:00', '', '665', '2017-01-17 12:44:43', '1');
INSERT INTO `tb_ledgers` VALUES ('662', '1', '1', '0', '2017-01-10 00:00:00', '', '666', '2017-01-17 12:44:59', '1');
INSERT INTO `tb_ledgers` VALUES ('663', '1', '1', '0', '2017-01-11 00:00:00', '', '667', '2017-01-17 12:45:14', '1');
INSERT INTO `tb_ledgers` VALUES ('664', '1', '1', '0', '2017-01-12 00:00:00', '', '668', '2017-01-17 12:45:27', '1');
INSERT INTO `tb_ledgers` VALUES ('665', '1', '1', '0', '2017-01-13 00:00:00', '', '669', '2017-01-17 12:45:43', '1');
INSERT INTO `tb_ledgers` VALUES ('666', '1', '1', '0', '2017-01-14 00:00:00', '', '670', '2017-01-17 12:45:59', '1');
INSERT INTO `tb_ledgers` VALUES ('667', '1', '1', '0', '2017-01-09 00:00:00', '', '671', '2017-01-17 12:50:15', '1');
INSERT INTO `tb_ledgers` VALUES ('668', '1', '1', '0', '2017-01-10 00:00:00', '', '672', '2017-01-17 12:50:31', '1');
INSERT INTO `tb_ledgers` VALUES ('669', '1', '1', '0', '2017-01-12 00:00:00', '', '673', '2017-01-17 12:50:48', '1');
INSERT INTO `tb_ledgers` VALUES ('670', '1', '1', '0', '2017-01-13 00:00:00', '', '674', '2017-01-17 12:51:02', '1');
INSERT INTO `tb_ledgers` VALUES ('671', '1', '1', '0', '2017-01-14 00:00:00', '', '675', '2017-01-17 12:51:18', '1');
INSERT INTO `tb_ledgers` VALUES ('672', '1', '1', '0', '2017-01-15 00:00:00', '', '676', '2017-01-17 12:51:31', '1');
INSERT INTO `tb_ledgers` VALUES ('673', '1', '1', '0', '2017-01-16 00:00:00', '', '677', '2017-01-17 12:51:58', '1');
INSERT INTO `tb_ledgers` VALUES ('674', '1', '1', '0', '2017-01-10 00:00:00', '', '678', '2017-01-17 12:53:18', '1');
INSERT INTO `tb_ledgers` VALUES ('675', '1', '1', '0', '2017-01-11 00:00:00', '', '679', '2017-01-17 12:53:40', '1');
INSERT INTO `tb_ledgers` VALUES ('676', '1', '1', '0', '2017-01-12 00:00:00', '', '680', '2017-01-17 12:54:01', '1');
INSERT INTO `tb_ledgers` VALUES ('677', '1', '1', '0', '2017-01-13 00:00:00', '', '681', '2017-01-17 13:09:20', '1');
INSERT INTO `tb_ledgers` VALUES ('678', '1', '1', '0', '2017-01-14 00:00:00', '', '682', '2017-01-17 13:09:46', '1');
INSERT INTO `tb_ledgers` VALUES ('679', '1', '1', '0', '2017-01-15 00:00:00', '', '683', '2017-01-17 13:10:14', '1');
INSERT INTO `tb_ledgers` VALUES ('680', '1', '1', '0', '2017-01-16 00:00:00', '', '684', '2017-01-17 13:10:29', '1');
INSERT INTO `tb_ledgers` VALUES ('681', '1', '1', '0', '2017-01-11 00:00:00', '', '685', '2017-01-17 13:12:55', '1');
INSERT INTO `tb_ledgers` VALUES ('682', '1', '1', '0', '2017-01-13 00:00:00', '', '686', '2017-01-17 13:13:16', '1');
INSERT INTO `tb_ledgers` VALUES ('683', '1', '1', '0', '2017-01-10 00:00:00', '', '687', '2017-01-17 13:18:31', '1');
INSERT INTO `tb_ledgers` VALUES ('684', '1', '1', '0', '2017-01-11 00:00:00', '', '688', '2017-01-17 13:18:47', '1');
INSERT INTO `tb_ledgers` VALUES ('685', '1', '1', '0', '2017-01-12 00:00:00', '', '689', '2017-01-17 13:19:01', '1');
INSERT INTO `tb_ledgers` VALUES ('686', '1', '1', '0', '2017-01-13 00:00:00', '', '690', '2017-01-17 13:19:17', '1');
INSERT INTO `tb_ledgers` VALUES ('687', '1', '1', '0', '2017-01-14 00:00:00', '', '691', '2017-01-17 13:19:44', '1');
INSERT INTO `tb_ledgers` VALUES ('688', '1', '1', '0', '2017-01-15 00:00:00', '', '692', '2017-01-17 13:20:01', '1');
INSERT INTO `tb_ledgers` VALUES ('689', '1', '1', '0', '2017-01-16 00:00:00', '', '693', '2017-01-17 13:20:18', '1');
INSERT INTO `tb_ledgers` VALUES ('690', '1', '1', '0', '2017-01-10 00:00:00', '', '694', '2017-01-17 13:24:46', '1');
INSERT INTO `tb_ledgers` VALUES ('691', '1', '1', '0', '2017-01-11 00:00:00', '', '695', '2017-01-17 13:24:59', '1');
INSERT INTO `tb_ledgers` VALUES ('692', '1', '1', '0', '2017-01-13 00:00:00', '', '696', '2017-01-17 13:25:17', '1');
INSERT INTO `tb_ledgers` VALUES ('693', '1', '1', '0', '2017-01-14 00:00:00', '', '697', '2017-01-17 13:25:36', '1');
INSERT INTO `tb_ledgers` VALUES ('694', '1', '1', '0', '2017-01-02 00:00:00', '', '698', '2017-01-17 15:05:34', '1');
INSERT INTO `tb_ledgers` VALUES ('695', '1', '1', '0', '2017-01-03 00:00:00', '', '699', '2017-01-17 15:06:00', '1');
INSERT INTO `tb_ledgers` VALUES ('696', '1', '1', '0', '2017-01-05 00:00:00', '', '700', '2017-01-17 15:06:22', '1');
INSERT INTO `tb_ledgers` VALUES ('697', '1', '1', '0', '2016-12-28 00:00:00', '', '701', '2017-01-24 11:00:09', '1');
INSERT INTO `tb_ledgers` VALUES ('698', '1', '1', '0', '2017-01-01 00:00:00', '', '702', '2017-01-24 11:01:06', '1');
INSERT INTO `tb_ledgers` VALUES ('699', '1', '1', '0', '2017-01-02 00:00:00', '', '703', '2017-01-24 11:02:22', '1');
INSERT INTO `tb_ledgers` VALUES ('700', '1', '1', '0', '2017-01-03 00:00:00', '', '704', '2017-01-24 11:03:30', '1');
INSERT INTO `tb_ledgers` VALUES ('701', '1', '1', '0', '2017-01-04 00:00:00', '', '705', '2017-01-24 11:04:34', '1');
INSERT INTO `tb_ledgers` VALUES ('702', '1', '1', '0', '2017-01-05 00:00:00', '', '706', '2017-01-24 11:05:25', '1');
INSERT INTO `tb_ledgers` VALUES ('703', '1', '1', '0', '2017-01-06 00:00:00', '', '707', '2017-01-24 11:08:15', '1');
INSERT INTO `tb_ledgers` VALUES ('704', '1', '1', '0', '2017-01-07 00:00:00', '', '708', '2017-01-24 11:09:09', '1');
INSERT INTO `tb_ledgers` VALUES ('705', '1', '1', '0', '2017-01-08 00:00:00', '', '709', '2017-01-24 11:10:15', '1');
INSERT INTO `tb_ledgers` VALUES ('706', '1', '1', '0', '2017-01-09 00:00:00', '', '710', '2017-01-24 11:11:22', '1');
INSERT INTO `tb_ledgers` VALUES ('707', '1', '1', '0', '2017-01-10 00:00:00', '', '711', '2017-01-24 11:12:27', '1');
INSERT INTO `tb_ledgers` VALUES ('708', '1', '1', '0', '2017-01-11 00:00:00', '', '712', '2017-01-24 11:14:08', '1');
INSERT INTO `tb_ledgers` VALUES ('709', '1', '1', '0', '2017-01-12 00:00:00', '', '713', '2017-01-24 11:15:14', '1');
INSERT INTO `tb_ledgers` VALUES ('710', '1', '1', '0', '2017-01-13 00:00:00', '', '714', '2017-01-24 11:16:26', '1');
INSERT INTO `tb_ledgers` VALUES ('711', '1', '1', '0', '2017-01-14 00:00:00', '', '715', '2017-01-24 11:17:20', '1');
INSERT INTO `tb_ledgers` VALUES ('712', '1', '1', '0', '2017-01-15 00:00:00', '', '716', '2017-01-24 11:18:22', '1');
INSERT INTO `tb_ledgers` VALUES ('713', '1', '1', '0', '2017-01-16 00:00:00', '', '717', '2017-01-24 11:19:19', '1');
INSERT INTO `tb_ledgers` VALUES ('714', '1', '1', '0', '2017-01-15 00:00:00', '', '718', '2017-01-24 11:22:55', '1');
INSERT INTO `tb_ledgers` VALUES ('715', '1', '1', '0', '2017-01-16 00:00:00', '', '719', '2017-01-24 11:24:14', '1');
INSERT INTO `tb_ledgers` VALUES ('716', '1', '1', '0', '2017-01-17 00:00:00', '', '720', '2017-01-24 11:25:24', '1');
INSERT INTO `tb_ledgers` VALUES ('717', '1', '1', '0', '2016-12-31 00:00:00', '', '721', '2017-01-24 11:34:20', '1');
INSERT INTO `tb_ledgers` VALUES ('718', '1', '1', '0', '2017-01-02 00:00:00', '', '722', '2017-01-24 11:35:45', '1');
INSERT INTO `tb_ledgers` VALUES ('719', '1', '1', '0', '2017-01-03 00:00:00', '', '723', '2017-01-24 11:36:41', '1');
INSERT INTO `tb_ledgers` VALUES ('720', '1', '1', '0', '2017-01-04 00:00:00', '', '724', '2017-01-24 11:37:38', '1');
INSERT INTO `tb_ledgers` VALUES ('721', '1', '1', '0', '2017-01-05 00:00:00', '', '725', '2017-01-24 11:45:29', '1');
INSERT INTO `tb_ledgers` VALUES ('722', '1', '1', '0', '2017-01-07 00:00:00', '', '726', '2017-01-24 11:46:45', '1');
INSERT INTO `tb_ledgers` VALUES ('723', '1', '1', '0', '2017-01-08 00:00:00', '', '727', '2017-01-24 11:47:37', '1');
INSERT INTO `tb_ledgers` VALUES ('724', '1', '1', '0', '2017-01-09 00:00:00', '', '728', '2017-01-24 11:48:37', '1');
INSERT INTO `tb_ledgers` VALUES ('725', '1', '1', '0', '2017-01-10 00:00:00', '', '729', '2017-01-24 11:49:51', '1');
INSERT INTO `tb_ledgers` VALUES ('726', '1', '1', '0', '2017-01-11 00:00:00', '', '730', '2017-01-24 11:50:40', '1');
INSERT INTO `tb_ledgers` VALUES ('727', '1', '1', '0', '2017-01-12 00:00:00', '', '731', '2017-01-24 11:51:36', '1');
INSERT INTO `tb_ledgers` VALUES ('728', '1', '1', '0', '2017-01-13 00:00:00', '', '732', '2017-01-24 11:52:44', '1');
INSERT INTO `tb_ledgers` VALUES ('729', '1', '1', '0', '2017-01-14 00:00:00', '', '733', '2017-01-24 11:53:43', '1');
INSERT INTO `tb_ledgers` VALUES ('730', '1', '1', '0', '2017-01-15 00:00:00', '', '734', '2017-01-24 11:54:49', '1');
INSERT INTO `tb_ledgers` VALUES ('731', '1', '1', '0', '2017-01-16 00:00:00', '', '735', '2017-01-24 11:55:47', '1');
INSERT INTO `tb_ledgers` VALUES ('732', '1', '1', '0', '2017-01-17 00:00:00', '', '736', '2017-01-24 11:56:43', '1');
INSERT INTO `tb_ledgers` VALUES ('733', '1', '1', '0', '2017-01-05 00:00:00', '', '737', '2017-01-24 12:01:35', '1');
INSERT INTO `tb_ledgers` VALUES ('734', '1', '1', '0', '2017-01-06 00:00:00', '', '738', '2017-01-24 12:02:46', '1');
INSERT INTO `tb_ledgers` VALUES ('735', '1', '1', '0', '2017-01-07 00:00:00', '', '739', '2017-01-24 12:03:41', '1');
INSERT INTO `tb_ledgers` VALUES ('736', '1', '1', '0', '2017-01-09 00:00:00', '', '740', '2017-01-24 12:04:54', '1');
INSERT INTO `tb_ledgers` VALUES ('737', '1', '1', '0', '2017-01-10 00:00:00', '', '741', '2017-01-24 12:06:36', '1');
INSERT INTO `tb_ledgers` VALUES ('738', '1', '1', '0', '2017-01-11 00:00:00', '', '742', '2017-01-24 12:07:36', '1');
INSERT INTO `tb_ledgers` VALUES ('739', '1', '1', '0', '2017-01-12 00:00:00', '', '743', '2017-01-24 12:08:32', '1');
INSERT INTO `tb_ledgers` VALUES ('740', '1', '1', '0', '2017-01-13 00:00:00', '', '744', '2017-01-24 12:09:22', '1');
INSERT INTO `tb_ledgers` VALUES ('741', '1', '1', '0', '2017-01-14 00:00:00', '', '745', '2017-01-24 12:10:28', '1');
INSERT INTO `tb_ledgers` VALUES ('742', '1', '1', '0', '2017-01-16 00:00:00', '', '746', '2017-01-24 12:11:33', '1');
INSERT INTO `tb_ledgers` VALUES ('743', '1', '1', '0', '2017-01-17 00:00:00', '', '747', '2017-01-24 12:12:30', '1');
INSERT INTO `tb_ledgers` VALUES ('744', '1', '1', '0', '2017-01-18 00:00:00', '', '748', '2017-01-24 12:13:09', '1');
INSERT INTO `tb_ledgers` VALUES ('745', '1', '1', '0', '2017-01-19 00:00:00', '', '749', '2017-01-24 12:14:11', '1');
INSERT INTO `tb_ledgers` VALUES ('746', '1', '1', '0', '2017-01-20 00:00:00', '', '750', '2017-01-24 12:14:52', '1');
INSERT INTO `tb_ledgers` VALUES ('747', '1', '1', '0', '2017-01-21 00:00:00', '', '751', '2017-01-24 12:15:52', '1');
INSERT INTO `tb_ledgers` VALUES ('748', '1', '1', '0', '2017-01-05 00:00:00', '', '752', '2017-01-24 13:18:45', '1');
INSERT INTO `tb_ledgers` VALUES ('749', '1', '1', '0', '2017-01-07 00:00:00', '', '753', '2017-01-24 13:19:34', '1');
INSERT INTO `tb_ledgers` VALUES ('750', '1', '1', '0', '2017-01-10 00:00:00', '', '754', '2017-01-24 13:30:32', '1');
INSERT INTO `tb_ledgers` VALUES ('751', '1', '1', '0', '2017-01-16 00:00:00', '', '755', '2017-01-24 13:31:12', '1');
INSERT INTO `tb_ledgers` VALUES ('752', '1', '1', '0', '2017-01-17 00:00:00', '', '756', '2017-01-24 13:31:56', '1');
INSERT INTO `tb_ledgers` VALUES ('753', '1', '1', '0', '2017-01-19 00:00:00', '', '757', '2017-01-24 13:32:59', '1');
INSERT INTO `tb_ledgers` VALUES ('754', '1', '1', '0', '2017-01-16 00:00:00', '', '758', '2017-01-25 11:39:08', '1');
INSERT INTO `tb_ledgers` VALUES ('755', '1', '1', '0', '2017-01-17 00:00:00', '', '759', '2017-01-25 11:40:12', '1');
INSERT INTO `tb_ledgers` VALUES ('756', '1', '1', '0', '2017-01-18 00:00:00', '', '760', '2017-01-25 11:41:19', '1');
INSERT INTO `tb_ledgers` VALUES ('757', '1', '1', '0', '2017-01-19 00:00:00', '', '761', '2017-01-25 11:42:18', '1');
INSERT INTO `tb_ledgers` VALUES ('758', '1', '1', '0', '2017-01-20 00:00:00', '', '762', '2017-01-25 11:44:18', '1');
INSERT INTO `tb_ledgers` VALUES ('759', '1', '1', '0', '2017-01-21 00:00:00', '', '763', '2017-01-25 11:45:15', '1');
INSERT INTO `tb_ledgers` VALUES ('760', '1', '1', '0', '2017-01-22 00:00:00', '', '764', '2017-01-25 11:46:25', '1');
INSERT INTO `tb_ledgers` VALUES ('761', '1', '1', '0', '2017-01-23 00:00:00', '', '765', '2017-01-25 11:47:54', '1');
INSERT INTO `tb_ledgers` VALUES ('762', '1', '1', '0', '2017-01-24 00:00:00', '', '766', '2017-01-25 11:49:09', '1');
INSERT INTO `tb_ledgers` VALUES ('763', '1', '1', '0', '2017-01-23 00:00:00', '', '767', '2017-01-25 11:57:44', '1');
INSERT INTO `tb_ledgers` VALUES ('764', '1', '1', '0', '2017-01-04 00:00:00', '', '768', '2017-01-25 12:00:38', '1');
INSERT INTO `tb_ledgers` VALUES ('765', '1', '1', '0', '2017-01-05 00:00:00', '', '769', '2017-01-25 12:02:33', '1');
INSERT INTO `tb_ledgers` VALUES ('766', '1', '1', '0', '2017-01-06 00:00:00', '', '770', '2017-01-25 12:03:29', '1');
INSERT INTO `tb_ledgers` VALUES ('767', '1', '1', '0', '2017-01-07 00:00:00', '', '771', '2017-01-25 12:04:55', '1');
INSERT INTO `tb_ledgers` VALUES ('768', '1', '1', '0', '2017-01-09 00:00:00', '', '772', '2017-01-25 12:17:30', '1');
INSERT INTO `tb_ledgers` VALUES ('769', '1', '1', '0', '2017-01-10 00:00:00', '', '773', '2017-01-25 12:18:59', '1');
INSERT INTO `tb_ledgers` VALUES ('770', '1', '1', '0', '2017-01-11 00:00:00', '', '774', '2017-01-25 12:20:14', '1');
INSERT INTO `tb_ledgers` VALUES ('771', '1', '1', '0', '2017-01-12 00:00:00', '', '775', '2017-01-25 12:21:37', '1');
INSERT INTO `tb_ledgers` VALUES ('772', '1', '1', '0', '2017-01-13 00:00:00', '', '776', '2017-01-25 12:22:46', '1');
INSERT INTO `tb_ledgers` VALUES ('773', '1', '1', '0', '2017-01-14 00:00:00', '', '777', '2017-01-25 12:23:58', '1');
INSERT INTO `tb_ledgers` VALUES ('774', '1', '1', '0', '2017-01-15 00:00:00', '', '778', '2017-01-25 12:25:06', '1');
INSERT INTO `tb_ledgers` VALUES ('775', '1', '1', '0', '2017-01-16 00:00:00', '', '779', '2017-01-25 12:34:43', '1');
INSERT INTO `tb_ledgers` VALUES ('776', '1', '1', '0', '2017-01-17 00:00:00', '', '780', '2017-01-25 12:36:16', '1');
INSERT INTO `tb_ledgers` VALUES ('777', '1', '1', '0', '2017-01-18 00:00:00', '', '781', '2017-01-25 12:41:03', '1');
INSERT INTO `tb_ledgers` VALUES ('778', '1', '1', '0', '2017-01-19 00:00:00', '', '782', '2017-01-25 12:43:25', '1');
INSERT INTO `tb_ledgers` VALUES ('779', '1', '1', '0', '2017-01-20 00:00:00', '', '783', '2017-01-25 12:46:04', '1');
INSERT INTO `tb_ledgers` VALUES ('780', '1', '1', '0', '2017-01-23 00:00:00', '', '784', '2017-01-25 12:49:27', '1');
INSERT INTO `tb_ledgers` VALUES ('781', '1', '1', '0', '2017-01-21 00:00:00', '', '785', '2017-01-25 12:53:06', '1');
INSERT INTO `tb_ledgers` VALUES ('782', '1', '1', '0', '2017-01-09 00:00:00', '', '786', '2017-01-25 12:58:59', '1');
INSERT INTO `tb_ledgers` VALUES ('783', '1', '1', '0', '2017-01-17 00:00:00', '', '787', '2017-01-25 13:00:01', '1');
INSERT INTO `tb_ledgers` VALUES ('784', '1', '1', '0', '2017-01-18 00:00:00', '', '788', '2017-01-25 13:01:29', '1');
INSERT INTO `tb_ledgers` VALUES ('785', '1', '1', '0', '2017-01-05 00:00:00', '', '789', '2017-01-25 13:10:26', '1');
INSERT INTO `tb_ledgers` VALUES ('786', '1', '1', '0', '2017-01-06 00:00:00', '', '790', '2017-01-25 13:23:16', '1');
INSERT INTO `tb_ledgers` VALUES ('787', '1', '1', '0', '2017-01-07 00:00:00', '', '791', '2017-01-25 13:26:58', '1');
INSERT INTO `tb_ledgers` VALUES ('788', '1', '1', '0', '2017-01-08 00:00:00', '', '792', '2017-01-25 13:28:27', '1');
INSERT INTO `tb_ledgers` VALUES ('789', '1', '1', '0', '2017-01-09 00:00:00', '', '793', '2017-01-25 13:31:20', '1');
INSERT INTO `tb_ledgers` VALUES ('790', '1', '1', '0', '2017-01-11 00:00:00', '', '794', '2017-01-25 13:32:20', '1');
INSERT INTO `tb_ledgers` VALUES ('791', '1', '1', '0', '2017-01-12 00:00:00', '', '795', '2017-01-25 13:33:18', '1');
INSERT INTO `tb_ledgers` VALUES ('792', '1', '1', '0', '2017-01-13 00:00:00', '', '796', '2017-01-25 13:35:26', '1');
INSERT INTO `tb_ledgers` VALUES ('793', '1', '1', '0', '2017-01-14 00:00:00', '', '797', '2017-01-25 13:36:57', '1');
INSERT INTO `tb_ledgers` VALUES ('794', '1', '1', '0', '2017-01-15 00:00:00', '', '798', '2017-01-25 13:38:32', '1');
INSERT INTO `tb_ledgers` VALUES ('795', '1', '1', '0', '2017-01-16 00:00:00', '', '799', '2017-01-25 13:56:36', '1');
INSERT INTO `tb_ledgers` VALUES ('796', '1', '1', '0', '2017-01-17 00:00:00', '', '800', '2017-01-25 13:58:07', '1');
INSERT INTO `tb_ledgers` VALUES ('797', '1', '1', '0', '2017-01-18 00:00:00', '', '801', '2017-01-25 13:59:13', '1');
INSERT INTO `tb_ledgers` VALUES ('798', '1', '1', '0', '2017-01-19 00:00:00', '', '802', '2017-01-25 13:59:54', '1');
INSERT INTO `tb_ledgers` VALUES ('799', '1', '1', '0', '2017-01-23 00:00:00', '', '803', '2017-01-25 14:00:48', '1');
INSERT INTO `tb_ledgers` VALUES ('800', '1', '1', '0', '2017-01-07 00:00:00', '', '804', '2017-01-25 14:05:01', '1');
INSERT INTO `tb_ledgers` VALUES ('801', '1', '1', '0', '2017-01-08 00:00:00', '', '805', '2017-01-25 14:06:25', '1');
INSERT INTO `tb_ledgers` VALUES ('802', '1', '1', '0', '2017-01-09 00:00:00', '', '806', '2017-01-25 14:08:25', '1');
INSERT INTO `tb_ledgers` VALUES ('803', '1', '1', '0', '2017-01-11 00:00:00', '', '807', '2017-01-25 14:09:40', '1');
INSERT INTO `tb_ledgers` VALUES ('804', '1', '1', '0', '2017-01-12 00:00:00', '', '808', '2017-01-25 14:17:41', '1');
INSERT INTO `tb_ledgers` VALUES ('805', '1', '1', '0', '2017-01-14 00:00:00', '', '809', '2017-01-25 14:18:35', '1');
INSERT INTO `tb_ledgers` VALUES ('806', '1', '1', '0', '2017-01-15 00:00:00', '', '810', '2017-01-25 14:19:29', '1');
INSERT INTO `tb_ledgers` VALUES ('807', '1', '1', '0', '2017-01-19 00:00:00', '', '811', '2017-01-25 14:20:24', '1');
INSERT INTO `tb_ledgers` VALUES ('808', '1', '1', '0', '2017-01-20 00:00:00', '', '812', '2017-01-25 14:21:45', '1');
INSERT INTO `tb_ledgers` VALUES ('809', '1', '1', '0', '2017-01-21 00:00:00', '', '813', '2017-01-25 14:22:43', '1');
INSERT INTO `tb_ledgers` VALUES ('810', '1', '1', '0', '2017-02-06 00:00:00', 'Loan Disbursement', '814', '2017-02-05 18:20:26', '1');
INSERT INTO `tb_ledgers` VALUES ('811', '1', '1', '0', '2017-02-07 00:00:00', 'Loan Disbursement', '815', '2017-02-05 18:21:21', '1');
INSERT INTO `tb_ledgers` VALUES ('812', '1', '1', '0', '2017-02-07 00:00:00', 'Loan Disbursement', '816', '2017-02-06 15:41:22', '1');
INSERT INTO `tb_ledgers` VALUES ('813', '1', '1', '0', '2017-02-08 00:00:00', 'Loan Disbursement', '817', '2017-02-06 16:01:57', '1');
INSERT INTO `tb_ledgers` VALUES ('814', '1', '1', '0', '2017-02-07 00:00:00', '890', '818', '2017-02-06 16:57:27', '1');

-- ----------------------------
-- Table structure for tb_ledgertransactions
-- ----------------------------
DROP TABLE IF EXISTS `tb_ledgertransactions`;
CREATE TABLE `tb_ledgertransactions` (
  `ledgertrxid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ledgerid` bigint(20) NOT NULL,
  `valuedate` datetime NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accountid` bigint(20) NOT NULL,
  `iscredit` tinyint(1) NOT NULL,
  `amount` double(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ledgertrxid`),
  KEY `FK_journalpost_journals` (`ledgerid`),
  KEY `FK_journalpost_ledgeraccounts` (`accountid`),
  KEY `tb_ledgertransactions_ledgerid` (`ledgerid`),
  CONSTRAINT `FK_tb_ledgertransactions_tb_ledgeraccounts` FOREIGN KEY (`accountid`) REFERENCES `tb_ledgeraccounts` (`accountid`),
  CONSTRAINT `FK_tb_ledgertransactions_tb_ledgers` FOREIGN KEY (`ledgerid`) REFERENCES `tb_ledgers` (`ledgerid`)
) ENGINE=InnoDB AUTO_INCREMENT=1621 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_ledgertransactions
-- ----------------------------
INSERT INTO `tb_ledgertransactions` VALUES ('1', '13', '2016-12-08 00:00:00', '2016-12-08 14:36:30', '14', '0', '169800.00');
INSERT INTO `tb_ledgertransactions` VALUES ('2', '13', '2016-12-08 00:00:00', '2016-12-08 14:36:30', '5', '1', '169800.00');
INSERT INTO `tb_ledgertransactions` VALUES ('3', '14', '2016-12-08 00:00:00', '2016-12-08 15:06:01', '14', '0', '86000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('4', '14', '2016-12-08 00:00:00', '2016-12-08 15:06:01', '5', '1', '86000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('5', '15', '2016-12-08 00:00:00', '2016-12-08 15:11:26', '14', '0', '86000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('6', '15', '2016-12-08 00:00:00', '2016-12-08 15:11:26', '5', '1', '86000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('7', '16', '2016-12-08 00:00:00', '2016-12-08 15:32:15', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('8', '16', '2016-12-08 00:00:00', '2016-12-08 15:32:15', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('9', '17', '2016-12-08 00:00:00', '2016-12-08 17:00:21', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('10', '17', '2016-12-08 00:00:00', '2016-12-08 17:00:21', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('11', '18', '2016-12-08 00:00:00', '2016-12-08 17:04:47', '14', '0', '115500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('12', '18', '2016-12-08 00:00:00', '2016-12-08 17:04:47', '5', '1', '115500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('13', '19', '2016-12-08 00:00:00', '2016-12-14 13:36:41', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('14', '19', '2016-12-08 00:00:00', '2016-12-14 13:36:41', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('15', '20', '2016-12-09 00:00:00', '2016-12-14 13:38:19', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('16', '20', '2016-12-09 00:00:00', '2016-12-14 13:38:19', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('17', '21', '2016-12-10 00:00:00', '2016-12-14 13:39:04', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('18', '21', '2016-12-10 00:00:00', '2016-12-14 13:39:04', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('19', '22', '2016-12-11 00:00:00', '2016-12-14 13:39:55', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('20', '22', '2016-12-11 00:00:00', '2016-12-14 13:39:55', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('21', '23', '2016-12-12 00:00:00', '2016-12-14 13:40:49', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('22', '23', '2016-12-12 00:00:00', '2016-12-14 13:40:49', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('23', '24', '2016-12-13 00:00:00', '2016-12-14 13:41:33', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('24', '24', '2016-12-13 00:00:00', '2016-12-14 13:41:33', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('25', '25', '2016-12-14 00:00:00', '2016-12-14 13:42:15', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('26', '25', '2016-12-14 00:00:00', '2016-12-14 13:42:15', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('27', '26', '2016-12-15 00:00:00', '2016-12-14 13:43:05', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('28', '26', '2016-12-15 00:00:00', '2016-12-14 13:43:05', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('29', '27', '2016-12-10 00:00:00', '2016-12-14 13:51:02', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('30', '27', '2016-12-10 00:00:00', '2016-12-14 13:51:02', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('31', '28', '2016-12-11 00:00:00', '2016-12-14 13:52:07', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('32', '28', '2016-12-11 00:00:00', '2016-12-14 13:52:07', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('33', '29', '2016-12-13 00:00:00', '2016-12-14 13:53:02', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('34', '29', '2016-12-13 00:00:00', '2016-12-14 13:53:02', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('35', '30', '2016-12-01 00:00:00', '2016-12-14 13:55:45', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('36', '30', '2016-12-01 00:00:00', '2016-12-14 13:55:45', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('37', '31', '2016-12-03 00:00:00', '2016-12-14 13:58:01', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('38', '31', '2016-12-03 00:00:00', '2016-12-14 13:58:01', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('39', '32', '2016-12-05 00:00:00', '2016-12-14 13:59:00', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('40', '32', '2016-12-05 00:00:00', '2016-12-14 13:59:00', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('41', '33', '2016-12-06 00:00:00', '2016-12-14 14:00:04', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('42', '33', '2016-12-06 00:00:00', '2016-12-14 14:00:04', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('43', '34', '2016-12-09 00:00:00', '2016-12-14 14:01:38', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('44', '34', '2016-12-09 00:00:00', '2016-12-14 14:01:38', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('45', '35', '2016-12-10 00:00:00', '2016-12-14 14:03:33', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('46', '35', '2016-12-10 00:00:00', '2016-12-14 14:03:33', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('47', '36', '2016-12-01 00:00:00', '2016-12-14 14:06:42', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('48', '36', '2016-12-01 00:00:00', '2016-12-14 14:06:42', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('49', '37', '2016-12-03 00:00:00', '2016-12-14 14:07:55', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('50', '37', '2016-12-03 00:00:00', '2016-12-14 14:07:55', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('51', '38', '2016-12-05 00:00:00', '2016-12-14 14:08:56', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('52', '38', '2016-12-05 00:00:00', '2016-12-14 14:08:56', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('53', '39', '2016-12-09 00:00:00', '2016-12-14 14:14:17', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('54', '39', '2016-12-09 00:00:00', '2016-12-14 14:14:17', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('55', '40', '2016-12-10 00:00:00', '2016-12-14 14:15:07', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('56', '40', '2016-12-10 00:00:00', '2016-12-14 14:15:07', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('57', '41', '2016-12-11 00:00:00', '2016-12-14 14:22:22', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('58', '41', '2016-12-11 00:00:00', '2016-12-14 14:22:22', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('59', '42', '2016-12-15 00:00:00', '2016-12-19 10:13:19', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('60', '42', '2016-12-15 00:00:00', '2016-12-19 10:13:19', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('61', '43', '2016-12-08 00:00:00', '2016-12-19 10:20:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('62', '43', '2016-12-08 00:00:00', '2016-12-19 10:20:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('63', '44', '2016-12-09 00:00:00', '2016-12-19 10:21:05', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('64', '44', '2016-12-09 00:00:00', '2016-12-19 10:21:05', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('65', '45', '2016-12-10 00:00:00', '2016-12-19 10:21:46', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('66', '45', '2016-12-10 00:00:00', '2016-12-19 10:21:46', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('67', '46', '2016-12-11 00:00:00', '2016-12-19 10:22:33', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('68', '46', '2016-12-11 00:00:00', '2016-12-19 10:22:33', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('69', '47', '2016-12-13 00:00:00', '2016-12-19 10:23:12', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('70', '47', '2016-12-13 00:00:00', '2016-12-19 10:23:12', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('71', '48', '2016-12-14 00:00:00', '2016-12-19 10:24:14', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('72', '48', '2016-12-14 00:00:00', '2016-12-19 10:24:14', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('73', '49', '2016-12-16 00:00:00', '2016-12-19 10:24:54', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('74', '49', '2016-12-16 00:00:00', '2016-12-19 10:24:54', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('75', '50', '2016-12-01 00:00:00', '2016-12-19 10:26:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('76', '50', '2016-12-01 00:00:00', '2016-12-19 10:26:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('77', '51', '2016-12-02 00:00:00', '2016-12-19 10:32:00', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('78', '51', '2016-12-02 00:00:00', '2016-12-19 10:32:00', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('79', '52', '2016-12-03 00:00:00', '2016-12-19 10:33:05', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('80', '52', '2016-12-03 00:00:00', '2016-12-19 10:33:05', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('81', '53', '2016-12-04 00:00:00', '2016-12-19 10:34:05', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('82', '53', '2016-12-04 00:00:00', '2016-12-19 10:34:05', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('83', '54', '2016-12-05 00:00:00', '2016-12-19 10:34:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('84', '54', '2016-12-05 00:00:00', '2016-12-19 10:34:32', '1', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('85', '55', '2016-12-08 00:00:00', '2016-12-19 10:34:54', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('86', '55', '2016-12-08 00:00:00', '2016-12-19 10:34:54', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('87', '56', '2016-12-09 00:00:00', '2016-12-19 10:35:17', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('88', '56', '2016-12-09 00:00:00', '2016-12-19 10:35:17', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('89', '57', '2016-12-10 00:00:00', '2016-12-19 10:37:29', '14', '0', '10.00');
INSERT INTO `tb_ledgertransactions` VALUES ('90', '57', '2016-12-10 00:00:00', '2016-12-19 10:37:29', '20', '1', '10.00');
INSERT INTO `tb_ledgertransactions` VALUES ('91', '58', '2016-12-11 00:00:00', '2016-12-19 10:37:47', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('92', '58', '2016-12-11 00:00:00', '2016-12-19 10:37:47', '1', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('93', '59', '2016-12-13 00:00:00', '2016-12-19 10:38:07', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('94', '59', '2016-12-13 00:00:00', '2016-12-19 10:38:07', '1', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('95', '60', '2016-12-14 00:00:00', '2016-12-19 10:38:25', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('96', '60', '2016-12-14 00:00:00', '2016-12-19 10:38:25', '1', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('97', '61', '2016-12-16 00:00:00', '2016-12-19 10:38:54', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('98', '61', '2016-12-16 00:00:00', '2016-12-19 10:38:54', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('99', '62', '2016-12-19 00:00:00', '2016-12-19 13:42:34', '14', '0', '53600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('100', '62', '2016-12-19 00:00:00', '2016-12-19 13:42:34', '5', '1', '53600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('101', '63', '2016-12-22 00:00:00', '2016-12-22 12:39:55', '14', '0', '418500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('102', '63', '2016-12-22 00:00:00', '2016-12-22 12:39:55', '5', '1', '418500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('103', '64', '2016-12-22 00:00:00', '2016-12-22 12:40:16', '14', '0', '418500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('104', '64', '2016-12-22 00:00:00', '2016-12-22 12:40:16', '5', '1', '418500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('105', '65', '2016-12-07 00:00:00', '2016-12-22 12:47:16', '14', '0', '900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('106', '65', '2016-12-07 00:00:00', '2016-12-22 12:47:16', '5', '1', '900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('107', '66', '2016-12-22 00:00:00', '2016-12-22 12:50:18', '14', '0', '89900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('108', '66', '2016-12-22 00:00:00', '2016-12-22 12:50:18', '5', '1', '89900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('109', '67', '2016-12-22 00:00:00', '2016-12-22 12:55:38', '14', '0', '104000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('110', '67', '2016-12-22 00:00:00', '2016-12-22 12:55:38', '5', '1', '104000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('111', '68', '2016-12-22 00:00:00', '2016-12-22 12:59:56', '14', '0', '65000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('112', '68', '2016-12-22 00:00:00', '2016-12-22 12:59:56', '5', '1', '65000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('113', '69', '2016-12-22 00:00:00', '2016-12-22 13:01:05', '14', '0', '74600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('114', '69', '2016-12-22 00:00:00', '2016-12-22 13:01:05', '5', '1', '74600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('115', '70', '2016-12-22 00:00:00', '2016-12-22 13:02:33', '14', '0', '52300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('116', '70', '2016-12-22 00:00:00', '2016-12-22 13:02:33', '5', '1', '52300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('117', '71', '2016-12-22 00:00:00', '2016-12-22 13:04:07', '14', '0', '19400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('118', '71', '2016-12-22 00:00:00', '2016-12-22 13:04:07', '5', '1', '19400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('119', '72', '2016-12-22 00:00:00', '2016-12-22 13:05:09', '14', '0', '31200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('120', '72', '2016-12-22 00:00:00', '2016-12-22 13:05:09', '5', '1', '31200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('121', '73', '2016-12-22 00:00:00', '2016-12-22 13:06:44', '14', '0', '21500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('122', '73', '2016-12-22 00:00:00', '2016-12-22 13:06:44', '5', '1', '21500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('123', '74', '2016-12-22 00:00:00', '2016-12-22 13:08:47', '14', '0', '1000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('124', '74', '2016-12-22 00:00:00', '2016-12-22 13:08:47', '5', '1', '1000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('125', '75', '2016-12-22 00:00:00', '2016-12-22 13:09:47', '14', '0', '162100.00');
INSERT INTO `tb_ledgertransactions` VALUES ('126', '75', '2016-12-22 00:00:00', '2016-12-22 13:09:47', '5', '1', '162100.00');
INSERT INTO `tb_ledgertransactions` VALUES ('127', '76', '2016-12-22 00:00:00', '2016-12-22 13:11:36', '14', '0', '10500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('128', '76', '2016-12-22 00:00:00', '2016-12-22 13:11:36', '5', '1', '10500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('129', '77', '2016-12-22 00:00:00', '2016-12-22 13:13:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('130', '77', '2016-12-22 00:00:00', '2016-12-22 13:13:40', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('131', '78', '2016-12-22 00:00:00', '2016-12-22 13:14:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('132', '78', '2016-12-22 00:00:00', '2016-12-22 13:14:19', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('133', '79', '2016-12-22 00:00:00', '2016-12-22 13:15:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('134', '79', '2016-12-22 00:00:00', '2016-12-22 13:15:10', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('135', '80', '2016-12-22 00:00:00', '2016-12-22 13:16:31', '14', '0', '225200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('136', '80', '2016-12-22 00:00:00', '2016-12-22 13:16:31', '5', '1', '225200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('137', '81', '2016-12-22 00:00:00', '2016-12-22 13:17:39', '14', '0', '2000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('138', '81', '2016-12-22 00:00:00', '2016-12-22 13:17:39', '5', '1', '2000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('139', '82', '2016-12-22 00:00:00', '2016-12-22 13:18:30', '14', '0', '2400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('140', '82', '2016-12-22 00:00:00', '2016-12-22 13:18:30', '5', '1', '2400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('141', '83', '2016-12-22 00:00:00', '2016-12-22 13:24:51', '14', '0', '93400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('142', '83', '2016-12-22 00:00:00', '2016-12-22 13:24:51', '5', '1', '93400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('143', '84', '2016-12-22 00:00:00', '2016-12-22 13:25:43', '14', '0', '6000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('144', '84', '2016-12-22 00:00:00', '2016-12-22 13:25:43', '5', '1', '6000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('145', '85', '2016-12-22 00:00:00', '2016-12-22 13:55:18', '14', '0', '4600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('146', '85', '2016-12-22 00:00:00', '2016-12-22 13:55:18', '5', '1', '4600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('147', '86', '2016-12-22 00:00:00', '2016-12-22 13:56:43', '14', '0', '7200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('148', '86', '2016-12-22 00:00:00', '2016-12-22 13:56:43', '5', '1', '7200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('149', '87', '2016-12-22 00:00:00', '2016-12-22 14:08:50', '14', '0', '85900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('150', '87', '2016-12-22 00:00:00', '2016-12-22 14:08:50', '5', '1', '85900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('151', '88', '2016-12-17 00:00:00', '2016-12-29 13:21:04', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('152', '88', '2016-12-17 00:00:00', '2016-12-29 13:21:04', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('153', '89', '2016-12-18 00:00:00', '2016-12-29 13:21:50', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('154', '89', '2016-12-18 00:00:00', '2016-12-29 13:21:50', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('155', '90', '2016-12-19 00:00:00', '2016-12-29 13:22:26', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('156', '90', '2016-12-19 00:00:00', '2016-12-29 13:22:26', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('157', '91', '2016-12-22 00:00:00', '2016-12-29 13:26:53', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('158', '91', '2016-12-22 00:00:00', '2016-12-29 13:26:53', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('159', '92', '2016-12-23 00:00:00', '2016-12-29 13:27:39', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('160', '92', '2016-12-23 00:00:00', '2016-12-29 13:27:39', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('161', '93', '2016-12-24 00:00:00', '2016-12-29 13:28:07', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('162', '93', '2016-12-24 00:00:00', '2016-12-29 13:28:07', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('163', '94', '2016-12-25 00:00:00', '2016-12-29 13:32:24', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('164', '94', '2016-12-25 00:00:00', '2016-12-29 13:32:24', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('165', '95', '2016-12-26 00:00:00', '2016-12-29 13:33:08', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('166', '95', '2016-12-26 00:00:00', '2016-12-29 13:33:08', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('167', '96', '2016-12-26 00:00:00', '2016-12-29 13:35:32', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('168', '96', '2016-12-26 00:00:00', '2016-12-29 13:35:32', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('169', '97', '2016-12-27 00:00:00', '2016-12-29 13:36:14', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('170', '97', '2016-12-27 00:00:00', '2016-12-29 13:36:14', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('171', '98', '2016-12-16 00:00:00', '2016-12-29 13:43:51', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('172', '98', '2016-12-16 00:00:00', '2016-12-29 13:43:51', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('173', '99', '2016-12-17 00:00:00', '2016-12-29 13:44:24', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('174', '99', '2016-12-17 00:00:00', '2016-12-29 13:44:24', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('175', '100', '2016-12-17 00:00:00', '2016-12-29 13:56:49', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('176', '100', '2016-12-17 00:00:00', '2016-12-29 13:56:49', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('177', '101', '2016-12-18 00:00:00', '2016-12-29 13:57:24', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('178', '101', '2016-12-18 00:00:00', '2016-12-29 13:57:24', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('179', '102', '2016-12-19 00:00:00', '2016-12-29 13:58:37', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('180', '102', '2016-12-19 00:00:00', '2016-12-29 13:58:37', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('181', '103', '2016-12-20 00:00:00', '2016-12-29 13:59:15', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('182', '103', '2016-12-20 00:00:00', '2016-12-29 13:59:15', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('183', '104', '2016-12-21 00:00:00', '2016-12-29 13:59:57', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('184', '104', '2016-12-21 00:00:00', '2016-12-29 13:59:57', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('185', '105', '2016-12-29 00:00:00', '2016-12-29 14:07:08', '14', '0', '427500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('186', '105', '2016-12-29 00:00:00', '2016-12-29 14:07:08', '5', '1', '427500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('187', '106', '2016-12-15 00:00:00', '2016-12-29 14:35:59', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('188', '106', '2016-12-15 00:00:00', '2016-12-29 14:35:59', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('189', '107', '2016-12-28 00:00:00', '2016-12-29 14:52:24', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('190', '107', '2016-12-28 00:00:00', '2016-12-29 14:52:24', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('191', '108', '2016-12-14 00:00:00', '2016-12-31 10:19:30', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('192', '108', '2016-12-14 00:00:00', '2016-12-31 10:19:30', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('193', '109', '2016-12-21 00:00:00', '2016-12-31 10:19:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('194', '109', '2016-12-21 00:00:00', '2016-12-31 10:19:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('195', '110', '2016-12-22 00:00:00', '2016-12-31 10:20:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('196', '110', '2016-12-22 00:00:00', '2016-12-31 10:20:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('197', '111', '2016-12-25 00:00:00', '2016-12-31 10:20:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('198', '111', '2016-12-25 00:00:00', '2016-12-31 10:20:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('199', '112', '2016-12-26 00:00:00', '2016-12-31 10:21:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('200', '112', '2016-12-26 00:00:00', '2016-12-31 10:21:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('201', '113', '2016-12-27 00:00:00', '2016-12-31 10:21:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('202', '113', '2016-12-27 00:00:00', '2016-12-31 10:21:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('203', '114', '2016-12-19 00:00:00', '2016-12-31 10:26:25', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('204', '114', '2016-12-19 00:00:00', '2016-12-31 10:26:25', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('205', '115', '2016-12-20 00:00:00', '2016-12-31 10:26:47', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('206', '115', '2016-12-20 00:00:00', '2016-12-31 10:26:47', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('207', '116', '2016-12-21 00:00:00', '2016-12-31 10:27:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('208', '116', '2016-12-21 00:00:00', '2016-12-31 10:27:06', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('209', '117', '2016-12-22 00:00:00', '2016-12-31 10:27:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('210', '117', '2016-12-22 00:00:00', '2016-12-31 10:27:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('211', '118', '2016-12-27 00:00:00', '2016-12-31 10:27:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('212', '118', '2016-12-27 00:00:00', '2016-12-31 10:27:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('213', '119', '2016-12-28 00:00:00', '2016-12-31 10:28:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('214', '119', '2016-12-28 00:00:00', '2016-12-31 10:28:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('215', '120', '2016-12-29 00:00:00', '2016-12-31 10:28:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('216', '120', '2016-12-29 00:00:00', '2016-12-31 10:28:36', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('217', '121', '2016-12-01 00:00:00', '2016-12-31 10:57:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('218', '121', '2016-12-01 00:00:00', '2016-12-31 10:57:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('219', '122', '2016-12-03 00:00:00', '2016-12-31 10:58:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('220', '122', '2016-12-03 00:00:00', '2016-12-31 10:58:23', '1', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('221', '123', '2016-12-06 00:00:00', '2016-12-31 10:58:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('222', '123', '2016-12-06 00:00:00', '2016-12-31 10:58:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('223', '124', '2016-12-07 00:00:00', '2016-12-31 10:59:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('224', '124', '2016-12-07 00:00:00', '2016-12-31 10:59:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('225', '125', '2016-12-10 00:00:00', '2016-12-31 11:00:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('226', '125', '2016-12-10 00:00:00', '2016-12-31 11:00:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('227', '126', '2016-12-14 00:00:00', '2016-12-31 11:01:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('228', '126', '2016-12-14 00:00:00', '2016-12-31 11:01:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('229', '127', '2016-12-16 00:00:00', '2016-12-31 11:01:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('230', '127', '2016-12-16 00:00:00', '2016-12-31 11:01:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('231', '128', '2016-12-17 00:00:00', '2016-12-31 11:01:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('232', '128', '2016-12-17 00:00:00', '2016-12-31 11:01:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('233', '129', '2016-12-19 00:00:00', '2016-12-31 11:01:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('234', '129', '2016-12-19 00:00:00', '2016-12-31 11:01:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('235', '130', '2016-12-21 00:00:00', '2016-12-31 11:02:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('236', '130', '2016-12-21 00:00:00', '2016-12-31 11:02:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('237', '131', '2016-12-22 00:00:00', '2016-12-31 11:02:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('238', '131', '2016-12-22 00:00:00', '2016-12-31 11:02:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('239', '132', '2016-12-23 00:00:00', '2016-12-31 11:02:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('240', '132', '2016-12-23 00:00:00', '2016-12-31 11:02:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('241', '133', '2016-12-01 00:00:00', '2016-12-31 11:04:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('242', '133', '2016-12-01 00:00:00', '2016-12-31 11:04:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('243', '134', '2016-12-03 00:00:00', '2016-12-31 11:04:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('244', '134', '2016-12-03 00:00:00', '2016-12-31 11:04:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('245', '135', '2016-12-05 00:00:00', '2016-12-31 11:05:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('246', '135', '2016-12-05 00:00:00', '2016-12-31 11:05:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('247', '136', '2016-12-05 00:00:00', '2016-12-31 11:06:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('248', '136', '2016-12-05 00:00:00', '2016-12-31 11:06:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('249', '137', '2016-12-01 00:00:00', '2016-12-31 11:09:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('250', '137', '2016-12-01 00:00:00', '2016-12-31 11:09:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('251', '138', '2016-12-17 00:00:00', '2016-12-31 11:10:23', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('252', '138', '2016-12-17 00:00:00', '2016-12-31 11:10:23', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('253', '139', '2016-12-18 00:00:00', '2016-12-31 11:10:42', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('254', '139', '2016-12-18 00:00:00', '2016-12-31 11:10:42', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('255', '140', '2016-12-20 00:00:00', '2016-12-31 11:11:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('256', '140', '2016-12-20 00:00:00', '2016-12-31 11:11:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('257', '141', '2016-12-21 00:00:00', '2016-12-31 11:11:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('258', '141', '2016-12-21 00:00:00', '2016-12-31 11:11:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('259', '142', '2016-12-22 00:00:00', '2016-12-31 11:11:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('260', '142', '2016-12-22 00:00:00', '2016-12-31 11:11:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('261', '143', '2016-12-23 00:00:00', '2016-12-31 11:12:04', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('262', '143', '2016-12-23 00:00:00', '2016-12-31 11:12:04', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('263', '144', '2016-12-24 00:00:00', '2016-12-31 11:12:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('264', '144', '2016-12-24 00:00:00', '2016-12-31 11:12:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('265', '145', '2016-12-25 00:00:00', '2016-12-31 11:12:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('266', '145', '2016-12-25 00:00:00', '2016-12-31 11:12:38', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('267', '146', '2016-12-26 00:00:00', '2016-12-31 11:12:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('268', '146', '2016-12-26 00:00:00', '2016-12-31 11:12:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('269', '147', '2016-12-27 00:00:00', '2016-12-31 11:13:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('270', '147', '2016-12-27 00:00:00', '2016-12-31 11:13:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('271', '148', '2016-12-28 00:00:00', '2016-12-31 11:13:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('272', '148', '2016-12-28 00:00:00', '2016-12-31 11:13:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('273', '149', '2016-12-29 00:00:00', '2016-12-31 11:14:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('274', '149', '2016-12-29 00:00:00', '2016-12-31 11:14:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('275', '150', '2016-12-01 00:00:00', '2016-12-31 11:18:50', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('276', '150', '2016-12-01 00:00:00', '2016-12-31 11:18:50', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('277', '151', '2016-12-02 00:00:00', '2016-12-31 11:19:16', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('278', '151', '2016-12-02 00:00:00', '2016-12-31 11:19:16', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('279', '152', '2016-12-03 00:00:00', '2016-12-31 11:19:31', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('280', '152', '2016-12-03 00:00:00', '2016-12-31 11:19:31', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('281', '153', '2016-12-04 00:00:00', '2016-12-31 11:19:46', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('282', '153', '2016-12-04 00:00:00', '2016-12-31 11:19:46', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('283', '154', '2016-12-07 00:00:00', '2016-12-31 11:20:08', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('284', '154', '2016-12-07 00:00:00', '2016-12-31 11:20:08', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('285', '155', '2016-12-09 00:00:00', '2016-12-31 11:20:31', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('286', '155', '2016-12-09 00:00:00', '2016-12-31 11:20:31', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('287', '156', '2016-12-10 00:00:00', '2016-12-31 11:20:51', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('288', '156', '2016-12-10 00:00:00', '2016-12-31 11:20:51', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('289', '157', '2016-12-11 00:00:00', '2016-12-31 11:21:07', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('290', '157', '2016-12-11 00:00:00', '2016-12-31 11:21:07', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('291', '158', '2016-12-17 00:00:00', '2016-12-31 11:21:27', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('292', '158', '2016-12-17 00:00:00', '2016-12-31 11:21:27', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('293', '159', '2016-12-18 00:00:00', '2016-12-31 11:21:42', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('294', '159', '2016-12-18 00:00:00', '2016-12-31 11:21:42', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('295', '160', '2016-12-19 00:00:00', '2016-12-31 11:21:58', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('296', '160', '2016-12-19 00:00:00', '2016-12-31 11:21:58', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('297', '161', '2016-12-21 00:00:00', '2016-12-31 11:22:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('298', '161', '2016-12-21 00:00:00', '2016-12-31 11:22:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('299', '162', '2016-12-22 00:00:00', '2016-12-31 11:22:30', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('300', '162', '2016-12-22 00:00:00', '2016-12-31 11:22:30', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('301', '163', '2016-12-25 00:00:00', '2016-12-31 11:22:51', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('302', '163', '2016-12-25 00:00:00', '2016-12-31 11:22:51', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('303', '164', '2016-12-26 00:00:00', '2016-12-31 11:23:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('304', '164', '2016-12-26 00:00:00', '2016-12-31 11:23:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('305', '165', '2016-12-27 00:00:00', '2016-12-31 11:23:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('306', '165', '2016-12-27 00:00:00', '2016-12-31 11:23:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('307', '166', '2016-12-02 00:00:00', '2016-12-31 11:26:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('308', '166', '2016-12-02 00:00:00', '2016-12-31 11:26:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('309', '167', '2016-12-05 00:00:00', '2016-12-31 11:26:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('310', '167', '2016-12-05 00:00:00', '2016-12-31 11:26:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('311', '168', '2016-12-06 00:00:00', '2016-12-31 11:27:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('312', '168', '2016-12-06 00:00:00', '2016-12-31 11:27:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('313', '169', '2016-12-07 00:00:00', '2016-12-31 11:27:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('314', '169', '2016-12-07 00:00:00', '2016-12-31 11:27:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('315', '170', '2016-12-08 00:00:00', '2016-12-31 11:27:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('316', '170', '2016-12-08 00:00:00', '2016-12-31 11:27:36', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('317', '171', '2016-12-10 00:00:00', '2016-12-31 11:27:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('318', '171', '2016-12-10 00:00:00', '2016-12-31 11:27:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('319', '172', '2016-12-13 00:00:00', '2016-12-31 11:28:12', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('320', '172', '2016-12-13 00:00:00', '2016-12-31 11:28:12', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('321', '173', '2016-12-14 00:00:00', '2016-12-31 11:28:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('322', '173', '2016-12-14 00:00:00', '2016-12-31 11:28:32', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('323', '174', '2016-12-19 00:00:00', '2016-12-31 11:28:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('324', '174', '2016-12-19 00:00:00', '2016-12-31 11:28:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('325', '175', '2016-12-20 00:00:00', '2016-12-31 11:29:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('326', '175', '2016-12-20 00:00:00', '2016-12-31 11:29:08', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('327', '176', '2016-12-29 00:00:00', '2016-12-31 11:29:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('328', '176', '2016-12-29 00:00:00', '2016-12-31 11:29:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('329', '177', '2016-12-30 00:00:00', '2016-12-31 11:29:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('330', '177', '2016-12-30 00:00:00', '2016-12-31 11:29:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('331', '178', '2016-12-22 00:00:00', '2016-12-31 11:30:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('332', '178', '2016-12-22 00:00:00', '2016-12-31 11:30:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('333', '179', '2016-12-23 00:00:00', '2016-12-31 11:30:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('334', '179', '2016-12-23 00:00:00', '2016-12-31 11:30:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('335', '180', '2016-12-24 00:00:00', '2016-12-31 11:31:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('336', '180', '2016-12-24 00:00:00', '2016-12-31 11:31:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('337', '181', '2016-12-26 00:00:00', '2016-12-31 11:31:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('338', '181', '2016-12-26 00:00:00', '2016-12-31 11:31:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('339', '182', '2016-12-27 00:00:00', '2016-12-31 11:32:12', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('340', '182', '2016-12-27 00:00:00', '2016-12-31 11:32:12', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('341', '183', '2016-12-28 00:00:00', '2016-12-31 11:32:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('342', '183', '2016-12-28 00:00:00', '2016-12-31 11:32:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('343', '184', '2016-12-03 00:00:00', '2016-12-31 11:34:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('344', '184', '2016-12-03 00:00:00', '2016-12-31 11:34:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('345', '185', '2016-12-07 00:00:00', '2016-12-31 11:35:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('346', '185', '2016-12-07 00:00:00', '2016-12-31 11:35:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('347', '186', '2016-12-08 00:00:00', '2016-12-31 11:37:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('348', '186', '2016-12-08 00:00:00', '2016-12-31 11:37:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('349', '187', '2016-12-10 00:00:00', '2016-12-31 11:37:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('350', '187', '2016-12-10 00:00:00', '2016-12-31 11:37:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('351', '188', '2016-12-13 00:00:00', '2016-12-31 11:38:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('352', '188', '2016-12-13 00:00:00', '2016-12-31 11:38:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('353', '189', '2016-12-16 00:00:00', '2016-12-31 11:38:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('354', '189', '2016-12-16 00:00:00', '2016-12-31 11:38:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('355', '190', '2016-12-17 00:00:00', '2016-12-31 11:38:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('356', '190', '2016-12-17 00:00:00', '2016-12-31 11:38:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('357', '191', '2016-12-18 00:00:00', '2016-12-31 11:39:11', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('358', '191', '2016-12-18 00:00:00', '2016-12-31 11:39:11', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('359', '192', '2016-12-23 00:00:00', '2016-12-31 11:39:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('360', '192', '2016-12-23 00:00:00', '2016-12-31 11:39:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('361', '193', '2016-12-24 00:00:00', '2016-12-31 11:39:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('362', '193', '2016-12-24 00:00:00', '2016-12-31 11:39:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('363', '194', '2016-12-25 00:00:00', '2016-12-31 11:40:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('364', '194', '2016-12-25 00:00:00', '2016-12-31 11:40:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('365', '195', '2016-12-27 00:00:00', '2016-12-31 11:40:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('366', '195', '2016-12-27 00:00:00', '2016-12-31 11:40:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('367', '196', '2016-12-28 00:00:00', '2016-12-31 11:40:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('368', '196', '2016-12-28 00:00:00', '2016-12-31 11:40:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('369', '197', '2016-12-29 00:00:00', '2016-12-31 11:41:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('370', '197', '2016-12-29 00:00:00', '2016-12-31 11:41:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('371', '198', '2016-12-30 00:00:00', '2016-12-31 11:41:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('372', '198', '2016-12-30 00:00:00', '2016-12-31 11:41:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('373', '199', '2016-12-05 00:00:00', '2016-12-31 11:43:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('374', '199', '2016-12-05 00:00:00', '2016-12-31 11:43:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('375', '200', '2016-12-21 00:00:00', '2016-12-31 11:44:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('376', '200', '2016-12-21 00:00:00', '2016-12-31 11:44:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('377', '201', '2016-12-22 00:00:00', '2016-12-31 11:44:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('378', '201', '2016-12-22 00:00:00', '2016-12-31 11:44:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('379', '202', '2016-12-10 00:00:00', '2016-12-31 11:49:34', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('380', '202', '2016-12-10 00:00:00', '2016-12-31 11:49:34', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('381', '203', '2016-12-15 00:00:00', '2016-12-31 11:49:57', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('382', '203', '2016-12-15 00:00:00', '2016-12-31 11:49:57', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('383', '204', '2016-12-17 00:00:00', '2016-12-31 11:50:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('384', '204', '2016-12-17 00:00:00', '2016-12-31 11:50:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('385', '205', '2016-12-19 00:00:00', '2016-12-31 11:50:35', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('386', '205', '2016-12-19 00:00:00', '2016-12-31 11:50:35', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('387', '206', '2016-12-22 00:00:00', '2016-12-31 11:50:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('388', '206', '2016-12-22 00:00:00', '2016-12-31 11:50:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('389', '207', '2016-12-05 00:00:00', '2016-12-31 11:51:22', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('390', '207', '2016-12-05 00:00:00', '2016-12-31 11:51:22', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('391', '208', '2016-12-08 00:00:00', '2016-12-31 11:51:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('392', '208', '2016-12-08 00:00:00', '2016-12-31 11:51:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('393', '209', '2016-12-12 00:00:00', '2016-12-31 11:51:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('394', '209', '2016-12-12 00:00:00', '2016-12-31 11:51:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('395', '210', '2016-12-13 00:00:00', '2016-12-31 11:52:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('396', '210', '2016-12-13 00:00:00', '2016-12-31 11:52:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('397', '211', '2016-12-26 00:00:00', '2016-12-31 11:52:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('398', '211', '2016-12-26 00:00:00', '2016-12-31 11:52:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('399', '212', '2016-12-14 00:00:00', '2016-12-31 12:24:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('400', '212', '2016-12-14 00:00:00', '2016-12-31 12:24:32', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('401', '213', '2016-12-16 00:00:00', '2016-12-31 12:24:51', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('402', '213', '2016-12-16 00:00:00', '2016-12-31 12:24:51', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('403', '214', '2016-12-17 00:00:00', '2016-12-31 12:25:10', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('404', '214', '2016-12-17 00:00:00', '2016-12-31 12:25:10', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('405', '215', '2016-12-19 00:00:00', '2016-12-31 12:25:33', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('406', '215', '2016-12-19 00:00:00', '2016-12-31 12:25:33', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('407', '216', '2016-12-21 00:00:00', '2016-12-31 12:25:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('408', '216', '2016-12-21 00:00:00', '2016-12-31 12:25:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('409', '217', '2016-12-23 00:00:00', '2016-12-31 12:26:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('410', '217', '2016-12-23 00:00:00', '2016-12-31 12:26:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('411', '218', '2016-12-02 00:00:00', '2016-12-31 12:27:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('412', '218', '2016-12-02 00:00:00', '2016-12-31 12:27:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('413', '219', '2016-12-03 00:00:00', '2016-12-31 12:27:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('414', '219', '2016-12-03 00:00:00', '2016-12-31 12:27:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('415', '220', '2016-12-01 00:00:00', '2016-12-31 12:42:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('416', '220', '2016-12-01 00:00:00', '2016-12-31 12:42:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('417', '221', '2016-12-05 00:00:00', '2016-12-31 12:42:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('418', '221', '2016-12-05 00:00:00', '2016-12-31 12:42:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('419', '222', '2016-12-06 00:00:00', '2016-12-31 12:43:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('420', '222', '2016-12-06 00:00:00', '2016-12-31 12:43:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('421', '223', '2016-12-09 00:00:00', '2016-12-31 12:43:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('422', '223', '2016-12-09 00:00:00', '2016-12-31 12:43:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('423', '224', '2016-12-10 00:00:00', '2016-12-31 12:43:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('424', '224', '2016-12-10 00:00:00', '2016-12-31 12:43:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('425', '225', '2016-12-13 00:00:00', '2016-12-31 12:43:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('426', '225', '2016-12-13 00:00:00', '2016-12-31 12:43:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('427', '226', '2016-12-15 00:00:00', '2016-12-31 12:44:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('428', '226', '2016-12-15 00:00:00', '2016-12-31 12:44:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('429', '227', '2016-12-16 00:00:00', '2016-12-31 12:44:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('430', '227', '2016-12-16 00:00:00', '2016-12-31 12:44:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('431', '228', '2016-12-20 00:00:00', '2016-12-31 12:44:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('432', '228', '2016-12-20 00:00:00', '2016-12-31 12:44:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('433', '229', '2016-12-21 00:00:00', '2016-12-31 12:45:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('434', '229', '2016-12-21 00:00:00', '2016-12-31 12:45:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('435', '230', '2016-12-23 00:00:00', '2016-12-31 12:45:30', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('436', '230', '2016-12-23 00:00:00', '2016-12-31 12:45:30', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('437', '231', '2016-12-25 00:00:00', '2016-12-31 12:45:49', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('438', '231', '2016-12-25 00:00:00', '2016-12-31 12:45:49', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('439', '232', '2016-12-27 00:00:00', '2016-12-31 12:46:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('440', '232', '2016-12-27 00:00:00', '2016-12-31 12:46:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('441', '233', '2016-12-28 00:00:00', '2016-12-31 12:46:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('442', '233', '2016-12-28 00:00:00', '2016-12-31 12:46:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('443', '234', '2016-12-29 00:00:00', '2016-12-31 12:47:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('444', '234', '2016-12-29 00:00:00', '2016-12-31 12:47:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('445', '235', '2016-12-01 00:00:00', '2016-12-31 12:49:37', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('446', '235', '2016-12-01 00:00:00', '2016-12-31 12:49:37', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('447', '236', '2016-12-02 00:00:00', '2016-12-31 12:49:56', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('448', '236', '2016-12-02 00:00:00', '2016-12-31 12:49:56', '20', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('449', '237', '2016-12-03 00:00:00', '2016-12-31 12:50:21', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('450', '237', '2016-12-03 00:00:00', '2016-12-31 12:50:21', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('451', '238', '2016-12-05 00:00:00', '2016-12-31 12:50:37', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('452', '238', '2016-12-05 00:00:00', '2016-12-31 12:50:37', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('453', '239', '2016-12-04 00:00:00', '2016-12-31 12:51:50', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('454', '239', '2016-12-04 00:00:00', '2016-12-31 12:51:50', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('455', '240', '2016-12-07 00:00:00', '2016-12-31 12:52:08', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('456', '240', '2016-12-07 00:00:00', '2016-12-31 12:52:08', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('457', '241', '2016-12-09 00:00:00', '2016-12-31 12:52:30', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('458', '241', '2016-12-09 00:00:00', '2016-12-31 12:52:30', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('459', '242', '2016-12-10 00:00:00', '2016-12-31 12:52:46', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('460', '242', '2016-12-10 00:00:00', '2016-12-31 12:52:46', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('461', '243', '2016-12-12 00:00:00', '2016-12-31 12:53:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('462', '243', '2016-12-12 00:00:00', '2016-12-31 12:53:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('463', '244', '2016-12-13 00:00:00', '2016-12-31 12:53:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('464', '244', '2016-12-13 00:00:00', '2016-12-31 12:53:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('465', '245', '2016-12-21 00:00:00', '2016-12-31 12:53:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('466', '245', '2016-12-21 00:00:00', '2016-12-31 12:53:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('467', '246', '2016-12-25 00:00:00', '2016-12-31 12:54:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('468', '246', '2016-12-25 00:00:00', '2016-12-31 12:54:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('469', '247', '2016-12-26 00:00:00', '2016-12-31 12:54:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('470', '247', '2016-12-26 00:00:00', '2016-12-31 12:54:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('471', '248', '2016-12-27 00:00:00', '2016-12-31 12:54:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('472', '248', '2016-12-27 00:00:00', '2016-12-31 12:54:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('473', '249', '2016-12-29 00:00:00', '2016-12-31 12:54:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('474', '249', '2016-12-29 00:00:00', '2016-12-31 12:54:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('475', '250', '2016-12-01 00:00:00', '2016-12-31 12:56:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('476', '250', '2016-12-01 00:00:00', '2016-12-31 12:56:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('477', '251', '2016-12-03 00:00:00', '2016-12-31 12:56:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('478', '251', '2016-12-03 00:00:00', '2016-12-31 12:56:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('479', '252', '2016-12-05 00:00:00', '2016-12-31 12:57:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('480', '252', '2016-12-05 00:00:00', '2016-12-31 12:57:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('481', '253', '2016-12-08 00:00:00', '2016-12-31 12:57:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('482', '253', '2016-12-08 00:00:00', '2016-12-31 12:57:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('483', '254', '2016-12-09 00:00:00', '2016-12-31 12:57:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('484', '254', '2016-12-09 00:00:00', '2016-12-31 12:57:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('485', '255', '2016-12-10 00:00:00', '2016-12-31 12:57:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('486', '255', '2016-12-10 00:00:00', '2016-12-31 12:57:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('487', '256', '2016-12-12 00:00:00', '2016-12-31 12:58:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('488', '256', '2016-12-12 00:00:00', '2016-12-31 12:58:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('489', '257', '2016-12-13 00:00:00', '2016-12-31 12:58:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('490', '257', '2016-12-13 00:00:00', '2016-12-31 12:58:38', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('491', '258', '2016-12-14 00:00:00', '2016-12-31 12:58:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('492', '258', '2016-12-14 00:00:00', '2016-12-31 12:58:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('493', '259', '2016-12-15 00:00:00', '2016-12-31 12:59:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('494', '259', '2016-12-15 00:00:00', '2016-12-31 12:59:08', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('495', '260', '2016-12-16 00:00:00', '2016-12-31 12:59:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('496', '260', '2016-12-16 00:00:00', '2016-12-31 12:59:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('497', '261', '2016-12-17 00:00:00', '2016-12-31 12:59:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('498', '261', '2016-12-17 00:00:00', '2016-12-31 12:59:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('499', '262', '2016-12-19 00:00:00', '2016-12-31 13:00:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('500', '262', '2016-12-19 00:00:00', '2016-12-31 13:00:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('501', '263', '2016-12-20 00:00:00', '2016-12-31 13:00:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('502', '263', '2016-12-20 00:00:00', '2016-12-31 13:00:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('503', '264', '2016-12-21 00:00:00', '2016-12-31 13:00:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('504', '264', '2016-12-21 00:00:00', '2016-12-31 13:00:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('505', '265', '2016-12-22 00:00:00', '2016-12-31 13:01:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('506', '265', '2016-12-22 00:00:00', '2016-12-31 13:01:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('507', '266', '2016-12-23 00:00:00', '2016-12-31 13:01:22', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('508', '266', '2016-12-23 00:00:00', '2016-12-31 13:01:22', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('509', '267', '2016-12-24 00:00:00', '2016-12-31 13:01:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('510', '267', '2016-12-24 00:00:00', '2016-12-31 13:01:38', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('511', '268', '2016-12-27 00:00:00', '2016-12-31 13:08:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('512', '268', '2016-12-27 00:00:00', '2016-12-31 13:08:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('513', '269', '2016-12-28 00:00:00', '2016-12-31 13:08:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('514', '269', '2016-12-28 00:00:00', '2016-12-31 13:08:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('515', '270', '2016-12-29 00:00:00', '2016-12-31 13:08:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('516', '270', '2016-12-29 00:00:00', '2016-12-31 13:08:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('517', '271', '2016-12-30 00:00:00', '2016-12-31 13:09:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('518', '271', '2016-12-30 00:00:00', '2016-12-31 13:09:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('519', '272', '2016-12-01 00:00:00', '2016-12-31 13:09:49', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('520', '272', '2016-12-01 00:00:00', '2016-12-31 13:09:49', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('521', '273', '2016-12-02 00:00:00', '2016-12-31 13:10:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('522', '273', '2016-12-02 00:00:00', '2016-12-31 13:10:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('523', '274', '2016-12-05 00:00:00', '2016-12-31 13:10:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('524', '274', '2016-12-05 00:00:00', '2016-12-31 13:10:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('525', '275', '2016-12-09 00:00:00', '2016-12-31 13:10:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('526', '275', '2016-12-09 00:00:00', '2016-12-31 13:10:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('527', '276', '2016-12-10 00:00:00', '2016-12-31 13:10:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('528', '276', '2016-12-10 00:00:00', '2016-12-31 13:10:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('529', '277', '2016-12-14 00:00:00', '2016-12-31 13:11:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('530', '277', '2016-12-14 00:00:00', '2016-12-31 13:11:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('531', '278', '2016-12-21 00:00:00', '2016-12-31 13:11:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('532', '278', '2016-12-21 00:00:00', '2016-12-31 13:11:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('533', '279', '2016-12-23 00:00:00', '2016-12-31 13:11:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('534', '279', '2016-12-23 00:00:00', '2016-12-31 13:11:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('535', '280', '2016-12-24 00:00:00', '2016-12-31 13:12:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('536', '280', '2016-12-24 00:00:00', '2016-12-31 13:12:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('537', '281', '2016-12-27 00:00:00', '2016-12-31 13:12:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('538', '281', '2016-12-27 00:00:00', '2016-12-31 13:12:26', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('539', '282', '2016-12-28 00:00:00', '2016-12-31 13:12:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('540', '282', '2016-12-28 00:00:00', '2016-12-31 13:12:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('541', '283', '2016-12-29 00:00:00', '2016-12-31 13:13:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('542', '283', '2016-12-29 00:00:00', '2016-12-31 13:13:06', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('543', '284', '2016-12-30 00:00:00', '2016-12-31 13:13:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('544', '284', '2016-12-30 00:00:00', '2016-12-31 13:13:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('545', '285', '2016-12-02 00:00:00', '2016-12-31 13:20:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('546', '285', '2016-12-02 00:00:00', '2016-12-31 13:20:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('547', '286', '2016-12-09 00:00:00', '2016-12-31 13:20:49', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('548', '286', '2016-12-09 00:00:00', '2016-12-31 13:20:49', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('549', '287', '2016-12-12 00:00:00', '2016-12-31 13:21:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('550', '287', '2016-12-12 00:00:00', '2016-12-31 13:21:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('551', '288', '2016-12-14 00:00:00', '2016-12-31 13:21:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('552', '288', '2016-12-14 00:00:00', '2016-12-31 13:21:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('553', '289', '2016-12-15 00:00:00', '2016-12-31 13:21:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('554', '289', '2016-12-15 00:00:00', '2016-12-31 13:21:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('555', '290', '2016-12-19 00:00:00', '2016-12-31 13:21:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('556', '290', '2016-12-19 00:00:00', '2016-12-31 13:21:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('557', '291', '2016-12-21 00:00:00', '2016-12-31 13:22:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('558', '291', '2016-12-21 00:00:00', '2016-12-31 13:22:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('559', '292', '2016-12-03 00:00:00', '2016-12-31 13:27:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('560', '292', '2016-12-03 00:00:00', '2016-12-31 13:27:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('561', '293', '2016-12-07 00:00:00', '2016-12-31 13:27:24', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('562', '293', '2016-12-07 00:00:00', '2016-12-31 13:27:24', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('563', '294', '2016-12-08 00:00:00', '2016-12-31 13:27:45', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('564', '294', '2016-12-08 00:00:00', '2016-12-31 13:27:45', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('565', '295', '2016-12-10 00:00:00', '2016-12-31 13:28:07', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('566', '295', '2016-12-10 00:00:00', '2016-12-31 13:28:07', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('567', '296', '2016-12-11 00:00:00', '2016-12-31 13:28:22', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('568', '296', '2016-12-11 00:00:00', '2016-12-31 13:28:22', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('569', '297', '2016-12-13 00:00:00', '2016-12-31 13:28:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('570', '297', '2016-12-13 00:00:00', '2016-12-31 13:28:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('571', '298', '2016-12-14 00:00:00', '2016-12-31 13:28:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('572', '298', '2016-12-14 00:00:00', '2016-12-31 13:28:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('573', '299', '2016-12-22 00:00:00', '2016-12-31 13:29:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('574', '299', '2016-12-22 00:00:00', '2016-12-31 13:29:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('575', '300', '2016-12-23 00:00:00', '2016-12-31 13:29:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('576', '300', '2016-12-23 00:00:00', '2016-12-31 13:29:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('577', '301', '2016-12-24 00:00:00', '2016-12-31 13:29:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('578', '301', '2016-12-24 00:00:00', '2016-12-31 13:29:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('579', '302', '2016-12-25 00:00:00', '2016-12-31 13:30:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('580', '302', '2016-12-25 00:00:00', '2016-12-31 13:30:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('581', '303', '2016-12-26 00:00:00', '2016-12-31 13:30:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('582', '303', '2016-12-26 00:00:00', '2016-12-31 13:30:36', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('583', '304', '2016-12-28 00:00:00', '2016-12-31 13:31:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('584', '304', '2016-12-28 00:00:00', '2016-12-31 13:31:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('585', '305', '2016-12-05 00:00:00', '2016-12-31 13:33:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('586', '305', '2016-12-05 00:00:00', '2016-12-31 13:33:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('587', '306', '2016-12-07 00:00:00', '2016-12-31 13:33:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('588', '306', '2016-12-07 00:00:00', '2016-12-31 13:33:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('589', '307', '2016-12-09 00:00:00', '2016-12-31 13:34:12', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('590', '307', '2016-12-09 00:00:00', '2016-12-31 13:34:12', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('591', '308', '2016-12-01 00:00:00', '2016-12-31 13:35:05', '14', '0', '600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('592', '308', '2016-12-01 00:00:00', '2016-12-31 13:35:05', '20', '1', '600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('593', '309', '2016-12-03 00:00:00', '2016-12-31 13:35:26', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('594', '309', '2016-12-03 00:00:00', '2016-12-31 13:35:26', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('595', '310', '2016-12-04 00:00:00', '2016-12-31 13:35:45', '14', '0', '600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('596', '310', '2016-12-04 00:00:00', '2016-12-31 13:35:45', '20', '1', '600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('597', '311', '2016-12-05 00:00:00', '2016-12-31 13:36:06', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('598', '311', '2016-12-05 00:00:00', '2016-12-31 13:36:06', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('599', '312', '2016-12-06 00:00:00', '2016-12-31 13:36:21', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('600', '312', '2016-12-06 00:00:00', '2016-12-31 13:36:21', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('601', '313', '2016-12-08 00:00:00', '2016-12-31 13:36:42', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('602', '313', '2016-12-08 00:00:00', '2016-12-31 13:36:42', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('603', '314', '2016-12-12 00:00:00', '2016-12-31 13:37:00', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('604', '314', '2016-12-12 00:00:00', '2016-12-31 13:37:00', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('605', '315', '2016-12-16 00:00:00', '2016-12-31 13:37:17', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('606', '315', '2016-12-16 00:00:00', '2016-12-31 13:37:17', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('607', '316', '2016-12-17 00:00:00', '2016-12-31 13:37:35', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('608', '316', '2016-12-17 00:00:00', '2016-12-31 13:37:35', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('609', '317', '2016-12-19 00:00:00', '2016-12-31 13:38:04', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('610', '317', '2016-12-19 00:00:00', '2016-12-31 13:38:04', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('611', '318', '2016-12-20 00:00:00', '2016-12-31 13:38:21', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('612', '318', '2016-12-20 00:00:00', '2016-12-31 13:38:21', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('613', '319', '2016-12-21 00:00:00', '2016-12-31 13:38:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('614', '319', '2016-12-21 00:00:00', '2016-12-31 13:38:43', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('615', '320', '2016-12-22 00:00:00', '2016-12-31 13:39:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('616', '320', '2016-12-22 00:00:00', '2016-12-31 13:39:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('617', '321', '2016-12-23 00:00:00', '2016-12-31 13:39:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('618', '321', '2016-12-23 00:00:00', '2016-12-31 13:39:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('619', '322', '2016-12-24 00:00:00', '2016-12-31 13:39:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('620', '322', '2016-12-24 00:00:00', '2016-12-31 13:39:43', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('621', '323', '2016-12-25 00:00:00', '2016-12-31 13:40:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('622', '323', '2016-12-25 00:00:00', '2016-12-31 13:40:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('623', '324', '2016-12-26 00:00:00', '2016-12-31 13:40:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('624', '324', '2016-12-26 00:00:00', '2016-12-31 13:40:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('625', '325', '2016-12-27 00:00:00', '2016-12-31 13:40:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('626', '325', '2016-12-27 00:00:00', '2016-12-31 13:40:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('627', '326', '2016-12-28 00:00:00', '2016-12-31 13:41:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('628', '326', '2016-12-28 00:00:00', '2016-12-31 13:41:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('629', '327', '2016-12-04 00:00:00', '2016-12-31 13:44:40', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('630', '327', '2016-12-04 00:00:00', '2016-12-31 13:44:40', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('631', '328', '2016-12-05 00:00:00', '2016-12-31 13:44:59', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('632', '328', '2016-12-05 00:00:00', '2016-12-31 13:44:59', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('633', '329', '2016-12-07 00:00:00', '2016-12-31 13:45:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('634', '329', '2016-12-07 00:00:00', '2016-12-31 13:45:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('635', '330', '2016-12-09 00:00:00', '2016-12-31 13:46:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('636', '330', '2016-12-09 00:00:00', '2016-12-31 13:46:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('637', '331', '2016-12-11 00:00:00', '2016-12-31 13:46:30', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('638', '331', '2016-12-11 00:00:00', '2016-12-31 13:46:30', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('639', '332', '2016-12-12 00:00:00', '2016-12-31 13:46:54', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('640', '332', '2016-12-12 00:00:00', '2016-12-31 13:46:54', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('641', '333', '2016-12-13 00:00:00', '2016-12-31 13:47:14', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('642', '333', '2016-12-13 00:00:00', '2016-12-31 13:47:14', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('643', '334', '2016-12-19 00:00:00', '2016-12-31 13:47:35', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('644', '334', '2016-12-19 00:00:00', '2016-12-31 13:47:35', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('645', '335', '2016-12-22 00:00:00', '2016-12-31 13:48:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('646', '335', '2016-12-22 00:00:00', '2016-12-31 13:48:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('647', '336', '2016-12-23 00:00:00', '2016-12-31 13:48:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('648', '336', '2016-12-23 00:00:00', '2016-12-31 13:48:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('649', '337', '2016-12-25 00:00:00', '2016-12-31 13:48:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('650', '337', '2016-12-25 00:00:00', '2016-12-31 13:48:38', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('651', '338', '2016-12-26 00:00:00', '2016-12-31 13:48:57', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('652', '338', '2016-12-26 00:00:00', '2016-12-31 13:48:57', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('653', '339', '2016-12-28 00:00:00', '2016-12-31 13:49:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('654', '339', '2016-12-28 00:00:00', '2016-12-31 13:49:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('655', '340', '2016-12-29 00:00:00', '2016-12-31 13:49:48', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('656', '340', '2016-12-29 00:00:00', '2016-12-31 13:49:48', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('657', '341', '2016-12-02 00:00:00', '2016-12-31 13:52:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('658', '341', '2016-12-02 00:00:00', '2016-12-31 13:52:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('659', '342', '2016-12-05 00:00:00', '2016-12-31 13:52:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('660', '342', '2016-12-05 00:00:00', '2016-12-31 13:52:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('661', '343', '2016-12-06 00:00:00', '2016-12-31 13:52:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('662', '343', '2016-12-06 00:00:00', '2016-12-31 13:52:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('663', '344', '2016-12-07 00:00:00', '2016-12-31 13:53:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('664', '344', '2016-12-07 00:00:00', '2016-12-31 13:53:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('665', '345', '2016-12-08 00:00:00', '2016-12-31 13:53:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('666', '345', '2016-12-08 00:00:00', '2016-12-31 13:53:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('667', '346', '2016-12-13 00:00:00', '2016-12-31 13:54:04', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('668', '346', '2016-12-13 00:00:00', '2016-12-31 13:54:04', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('669', '347', '2016-12-14 00:00:00', '2016-12-31 13:54:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('670', '347', '2016-12-14 00:00:00', '2016-12-31 13:54:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('671', '348', '2016-12-15 00:00:00', '2016-12-31 14:00:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('672', '348', '2016-12-15 00:00:00', '2016-12-31 14:00:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('673', '349', '2016-12-17 00:00:00', '2016-12-31 14:00:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('674', '349', '2016-12-17 00:00:00', '2016-12-31 14:00:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('675', '350', '2016-12-20 00:00:00', '2016-12-31 14:01:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('676', '350', '2016-12-20 00:00:00', '2016-12-31 14:01:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('677', '351', '2016-12-21 00:00:00', '2016-12-31 14:02:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('678', '351', '2016-12-21 00:00:00', '2016-12-31 14:02:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('679', '352', '2016-12-22 00:00:00', '2016-12-31 14:02:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('680', '352', '2016-12-22 00:00:00', '2016-12-31 14:02:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('681', '353', '2016-12-30 00:00:00', '2016-12-31 14:02:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('682', '353', '2016-12-30 00:00:00', '2016-12-31 14:02:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('683', '354', '2016-12-02 00:00:00', '2016-12-31 14:04:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('684', '354', '2016-12-02 00:00:00', '2016-12-31 14:04:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('685', '355', '2016-12-06 00:00:00', '2016-12-31 14:12:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('686', '355', '2016-12-06 00:00:00', '2016-12-31 14:12:26', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('687', '356', '2016-12-07 00:00:00', '2016-12-31 14:13:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('688', '356', '2016-12-07 00:00:00', '2016-12-31 14:13:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('689', '357', '2016-12-08 00:00:00', '2016-12-31 14:13:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('690', '357', '2016-12-08 00:00:00', '2016-12-31 14:13:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('691', '358', '2016-12-09 00:00:00', '2016-12-31 14:14:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('692', '358', '2016-12-09 00:00:00', '2016-12-31 14:14:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('693', '359', '2016-12-12 00:00:00', '2016-12-31 14:14:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('694', '359', '2016-12-12 00:00:00', '2016-12-31 14:14:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('695', '360', '2016-12-15 00:00:00', '2016-12-31 14:15:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('696', '360', '2016-12-15 00:00:00', '2016-12-31 14:15:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('697', '361', '2016-12-17 00:00:00', '2017-01-02 10:28:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('698', '361', '2016-12-17 00:00:00', '2017-01-02 10:28:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('699', '362', '2016-12-21 00:00:00', '2017-01-02 10:28:49', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('700', '362', '2016-12-21 00:00:00', '2017-01-02 10:28:49', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('701', '363', '2016-12-22 00:00:00', '2017-01-02 10:29:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('702', '363', '2016-12-22 00:00:00', '2017-01-02 10:29:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('703', '364', '2016-12-26 00:00:00', '2017-01-02 10:29:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('704', '364', '2016-12-26 00:00:00', '2017-01-02 10:29:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('705', '365', '2016-12-29 00:00:00', '2017-01-02 10:30:30', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('706', '365', '2016-12-29 00:00:00', '2017-01-02 10:30:30', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('707', '366', '2017-01-30 00:00:00', '2017-01-02 10:30:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('708', '366', '2017-01-30 00:00:00', '2017-01-02 10:30:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('709', '367', '2016-12-01 00:00:00', '2017-01-02 10:31:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('710', '367', '2016-12-01 00:00:00', '2017-01-02 10:31:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('711', '368', '2016-12-10 00:00:00', '2017-01-02 10:31:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('712', '368', '2016-12-10 00:00:00', '2017-01-02 10:31:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('713', '369', '2016-12-16 00:00:00', '2017-01-02 10:32:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('714', '369', '2016-12-16 00:00:00', '2017-01-02 10:32:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('715', '370', '2016-12-19 00:00:00', '2017-01-02 10:32:22', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('716', '370', '2016-12-19 00:00:00', '2017-01-02 10:32:22', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('717', '371', '2016-12-02 00:00:00', '2017-01-02 10:33:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('718', '371', '2016-12-02 00:00:00', '2017-01-02 10:33:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('719', '372', '2016-12-05 00:00:00', '2017-01-02 10:33:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('720', '372', '2016-12-05 00:00:00', '2017-01-02 10:33:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('721', '373', '2016-12-07 00:00:00', '2017-01-02 10:33:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('722', '373', '2016-12-07 00:00:00', '2017-01-02 10:33:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('723', '374', '2016-12-13 00:00:00', '2017-01-02 10:34:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('724', '374', '2016-12-13 00:00:00', '2017-01-02 10:34:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('725', '375', '2016-12-16 00:00:00', '2017-01-02 10:34:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('726', '375', '2016-12-16 00:00:00', '2017-01-02 10:34:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('727', '376', '2016-12-17 00:00:00', '2017-01-02 10:34:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('728', '376', '2016-12-17 00:00:00', '2017-01-02 10:34:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('729', '377', '2016-12-20 00:00:00', '2017-01-02 10:34:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('730', '377', '2016-12-20 00:00:00', '2017-01-02 10:34:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('731', '378', '2016-12-22 00:00:00', '2017-01-02 10:35:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('732', '378', '2016-12-22 00:00:00', '2017-01-02 10:35:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('733', '379', '2016-12-23 00:00:00', '2017-01-02 10:35:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('734', '379', '2016-12-23 00:00:00', '2017-01-02 10:35:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('735', '380', '2016-12-24 00:00:00', '2017-01-02 10:35:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('736', '380', '2016-12-24 00:00:00', '2017-01-02 10:35:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('737', '381', '2016-12-27 00:00:00', '2017-01-02 10:36:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('738', '381', '2016-12-27 00:00:00', '2017-01-02 10:36:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('739', '382', '2016-12-29 00:00:00', '2017-01-02 10:36:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('740', '382', '2016-12-29 00:00:00', '2017-01-02 10:36:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('741', '383', '2016-12-30 00:00:00', '2017-01-02 10:36:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('742', '383', '2016-12-30 00:00:00', '2017-01-02 10:36:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('743', '384', '2016-12-01 00:00:00', '2017-01-02 10:37:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('744', '384', '2016-12-01 00:00:00', '2017-01-02 10:37:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('745', '385', '2016-12-02 00:00:00', '2017-01-02 10:37:39', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('746', '385', '2016-12-02 00:00:00', '2017-01-02 10:37:39', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('747', '386', '2016-12-09 00:00:00', '2017-01-02 10:38:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('748', '386', '2016-12-09 00:00:00', '2017-01-02 10:38:06', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('749', '387', '2016-12-10 00:00:00', '2017-01-02 10:38:27', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('750', '387', '2016-12-10 00:00:00', '2017-01-02 10:38:27', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('751', '388', '2016-12-13 00:00:00', '2017-01-02 10:38:48', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('752', '388', '2016-12-13 00:00:00', '2017-01-02 10:38:48', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('753', '389', '2016-12-15 00:00:00', '2017-01-02 10:39:08', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('754', '389', '2016-12-15 00:00:00', '2017-01-02 10:39:08', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('755', '390', '2016-12-18 00:00:00', '2017-01-02 10:39:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('756', '390', '2016-12-18 00:00:00', '2017-01-02 10:39:32', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('757', '391', '2016-12-20 00:00:00', '2017-01-02 10:39:53', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('758', '391', '2016-12-20 00:00:00', '2017-01-02 10:39:53', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('759', '392', '2016-12-22 00:00:00', '2017-01-02 10:40:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('760', '392', '2016-12-22 00:00:00', '2017-01-02 10:40:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('761', '393', '2016-12-23 00:00:00', '2017-01-02 10:40:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('762', '393', '2016-12-23 00:00:00', '2017-01-02 10:40:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('763', '394', '2016-12-24 00:00:00', '2017-01-02 10:40:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('764', '394', '2016-12-24 00:00:00', '2017-01-02 10:40:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('765', '395', '2016-12-25 00:00:00', '2017-01-02 10:41:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('766', '395', '2016-12-25 00:00:00', '2017-01-02 10:41:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('767', '396', '2016-12-26 00:00:00', '2017-01-02 10:41:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('768', '396', '2016-12-26 00:00:00', '2017-01-02 10:41:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('769', '397', '2016-12-28 00:00:00', '2017-01-02 10:42:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('770', '397', '2016-12-28 00:00:00', '2017-01-02 10:42:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('771', '398', '2016-12-01 00:00:00', '2017-01-02 10:51:14', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('772', '398', '2016-12-01 00:00:00', '2017-01-02 10:51:14', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('773', '399', '2016-12-02 00:00:00', '2017-01-02 10:51:33', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('774', '399', '2016-12-02 00:00:00', '2017-01-02 10:51:33', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('775', '400', '2016-12-03 00:00:00', '2017-01-02 10:51:53', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('776', '400', '2016-12-03 00:00:00', '2017-01-02 10:51:53', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('777', '401', '2016-12-08 00:00:00', '2017-01-02 10:52:12', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('778', '401', '2016-12-08 00:00:00', '2017-01-02 10:52:12', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('779', '402', '2016-12-09 00:00:00', '2017-01-02 10:52:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('780', '402', '2016-12-09 00:00:00', '2017-01-02 10:52:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('781', '403', '2016-12-13 00:00:00', '2017-01-02 10:52:58', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('782', '403', '2016-12-13 00:00:00', '2017-01-02 10:52:58', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('783', '404', '2016-12-14 00:00:00', '2017-01-02 10:53:16', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('784', '404', '2016-12-14 00:00:00', '2017-01-02 10:53:16', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('785', '405', '2016-12-15 00:00:00', '2017-01-02 10:53:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('786', '405', '2016-12-15 00:00:00', '2017-01-02 10:53:32', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('787', '406', '2016-12-16 00:00:00', '2017-01-02 10:53:51', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('788', '406', '2016-12-16 00:00:00', '2017-01-02 10:53:51', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('789', '407', '2016-12-19 00:00:00', '2017-01-02 10:54:09', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('790', '407', '2016-12-19 00:00:00', '2017-01-02 10:54:09', '20', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('791', '408', '2016-12-20 00:00:00', '2017-01-02 10:54:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('792', '408', '2016-12-20 00:00:00', '2017-01-02 10:54:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('793', '409', '2016-12-21 00:00:00', '2017-01-02 10:54:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('794', '409', '2016-12-21 00:00:00', '2017-01-02 10:54:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('795', '410', '2016-12-22 00:00:00', '2017-01-02 10:55:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('796', '410', '2016-12-22 00:00:00', '2017-01-02 10:55:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('797', '411', '2016-12-23 00:00:00', '2017-01-02 10:55:30', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('798', '411', '2016-12-23 00:00:00', '2017-01-02 10:55:30', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('799', '412', '2016-12-24 00:00:00', '2017-01-02 10:55:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('800', '412', '2016-12-24 00:00:00', '2017-01-02 10:55:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('801', '413', '2016-12-26 00:00:00', '2017-01-02 10:56:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('802', '413', '2016-12-26 00:00:00', '2017-01-02 10:56:08', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('803', '414', '2016-12-28 00:00:00', '2017-01-02 10:56:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('804', '414', '2016-12-28 00:00:00', '2017-01-02 10:56:26', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('805', '415', '2017-01-02 00:00:00', '2017-01-02 11:05:26', '14', '0', '19900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('806', '415', '2017-01-02 00:00:00', '2017-01-02 11:05:26', '5', '1', '19900.00');
INSERT INTO `tb_ledgertransactions` VALUES ('807', '416', '2017-01-01 00:00:00', '2017-01-02 11:10:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('808', '416', '2017-01-01 00:00:00', '2017-01-02 11:10:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('809', '417', '2017-01-02 00:00:00', '2017-01-02 11:11:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('810', '417', '2017-01-02 00:00:00', '2017-01-02 11:11:23', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('811', '418', '2017-01-02 00:00:00', '2017-01-02 11:16:43', '14', '0', '30800.00');
INSERT INTO `tb_ledgertransactions` VALUES ('812', '418', '2017-01-02 00:00:00', '2017-01-02 11:16:43', '5', '1', '30800.00');
INSERT INTO `tb_ledgertransactions` VALUES ('813', '419', '2017-01-02 00:00:00', '2017-01-02 11:18:40', '14', '0', '96700.00');
INSERT INTO `tb_ledgertransactions` VALUES ('814', '419', '2017-01-02 00:00:00', '2017-01-02 11:18:40', '5', '1', '96700.00');
INSERT INTO `tb_ledgertransactions` VALUES ('815', '420', '2017-01-02 00:00:00', '2017-01-02 11:19:21', '14', '0', '95000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('816', '420', '2017-01-02 00:00:00', '2017-01-02 11:19:21', '5', '1', '95000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('817', '421', '2017-01-02 00:00:00', '2017-01-02 11:20:04', '14', '0', '13000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('818', '421', '2017-01-02 00:00:00', '2017-01-02 11:20:04', '5', '1', '13000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('819', '422', '2017-01-02 00:00:00', '2017-01-02 11:21:41', '14', '0', '170000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('820', '422', '2017-01-02 00:00:00', '2017-01-02 11:21:41', '5', '1', '170000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('821', '423', '2017-01-02 00:00:00', '2017-01-02 11:22:58', '14', '0', '82400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('822', '423', '2017-01-02 00:00:00', '2017-01-02 11:22:58', '5', '1', '82400.00');
INSERT INTO `tb_ledgertransactions` VALUES ('823', '424', '2017-01-02 00:00:00', '2017-01-02 11:24:16', '14', '0', '2300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('824', '424', '2017-01-02 00:00:00', '2017-01-02 11:24:16', '5', '1', '2300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('825', '425', '2017-01-02 00:00:00', '2017-01-02 11:28:14', '14', '0', '15000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('826', '425', '2017-01-02 00:00:00', '2017-01-02 11:28:14', '5', '1', '15000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('827', '426', '2016-12-22 00:00:00', '2017-01-04 13:13:16', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('828', '426', '2016-12-22 00:00:00', '2017-01-04 13:13:16', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('829', '427', '2016-12-23 00:00:00', '2017-01-04 13:14:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('830', '427', '2016-12-23 00:00:00', '2017-01-04 13:14:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('831', '428', '2016-12-24 00:00:00', '2017-01-04 13:14:53', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('832', '428', '2016-12-24 00:00:00', '2017-01-04 13:14:53', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('833', '429', '2016-12-26 00:00:00', '2017-01-04 13:15:23', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('834', '429', '2016-12-26 00:00:00', '2017-01-04 13:15:23', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('835', '430', '2016-12-27 00:00:00', '2017-01-04 13:15:59', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('836', '430', '2016-12-27 00:00:00', '2017-01-04 13:15:59', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('837', '431', '2016-12-28 00:00:00', '2017-01-04 13:16:29', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('838', '431', '2016-12-28 00:00:00', '2017-01-04 13:16:29', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('839', '432', '2016-12-25 00:00:00', '2017-01-04 13:18:18', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('840', '432', '2016-12-25 00:00:00', '2017-01-04 13:18:18', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('841', '433', '2017-01-01 00:00:00', '2017-01-04 13:19:48', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('842', '433', '2017-01-01 00:00:00', '2017-01-04 13:19:48', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('843', '434', '2017-01-02 00:00:00', '2017-01-04 13:20:12', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('844', '434', '2017-01-02 00:00:00', '2017-01-04 13:20:12', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('845', '435', '2016-12-29 00:00:00', '2017-01-04 13:21:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('846', '435', '2016-12-29 00:00:00', '2017-01-04 13:21:44', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('847', '436', '2016-12-31 00:00:00', '2017-01-04 13:22:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('848', '436', '2016-12-31 00:00:00', '2017-01-04 13:22:16', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('849', '437', '2017-01-02 00:00:00', '2017-01-04 13:22:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('850', '437', '2017-01-02 00:00:00', '2017-01-04 13:22:58', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('851', '438', '2016-12-29 00:00:00', '2017-01-04 13:24:10', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('852', '438', '2016-12-29 00:00:00', '2017-01-04 13:24:10', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('853', '439', '2016-12-30 00:00:00', '2017-01-04 13:24:52', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('854', '439', '2016-12-30 00:00:00', '2017-01-04 13:24:52', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('855', '440', '2016-12-31 00:00:00', '2017-01-04 13:25:31', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('856', '440', '2016-12-31 00:00:00', '2017-01-04 13:25:31', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('857', '441', '2017-01-01 00:00:00', '2017-01-04 13:26:05', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('858', '441', '2017-01-01 00:00:00', '2017-01-04 13:26:05', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('859', '442', '2017-01-02 00:00:00', '2017-01-04 13:26:33', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('860', '442', '2017-01-02 00:00:00', '2017-01-04 13:26:33', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('861', '443', '2016-12-29 00:00:00', '2017-01-04 13:32:39', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('862', '443', '2016-12-29 00:00:00', '2017-01-04 13:32:39', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('863', '444', '2016-12-30 00:00:00', '2017-01-04 13:33:38', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('864', '444', '2016-12-30 00:00:00', '2017-01-04 13:33:38', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('865', '445', '2016-12-31 00:00:00', '2017-01-04 13:34:21', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('866', '445', '2016-12-31 00:00:00', '2017-01-04 13:34:21', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('867', '446', '2017-01-02 00:00:00', '2017-01-04 13:35:05', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('868', '446', '2017-01-02 00:00:00', '2017-01-04 13:35:05', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('869', '447', '2017-01-03 00:00:00', '2017-01-04 13:35:42', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('870', '447', '2017-01-03 00:00:00', '2017-01-04 13:35:42', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('871', '448', '2016-12-29 00:00:00', '2017-01-04 13:38:27', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('872', '448', '2016-12-29 00:00:00', '2017-01-04 13:38:27', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('873', '449', '2016-12-31 00:00:00', '2017-01-04 13:38:59', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('874', '449', '2016-12-31 00:00:00', '2017-01-04 13:38:59', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('875', '450', '2016-12-23 00:00:00', '2017-01-04 13:41:30', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('876', '450', '2016-12-23 00:00:00', '2017-01-04 13:41:30', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('877', '451', '2016-12-24 00:00:00', '2017-01-04 13:42:03', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('878', '451', '2016-12-24 00:00:00', '2017-01-04 13:42:03', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('879', '452', '2016-12-25 00:00:00', '2017-01-04 13:45:30', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('880', '452', '2016-12-25 00:00:00', '2017-01-04 13:45:30', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('881', '453', '2016-12-26 00:00:00', '2017-01-04 13:46:21', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('882', '453', '2016-12-26 00:00:00', '2017-01-04 13:46:21', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('883', '454', '2016-12-27 00:00:00', '2017-01-05 14:13:49', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('884', '454', '2016-12-27 00:00:00', '2017-01-05 14:13:49', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('885', '455', '2016-12-28 00:00:00', '2017-01-05 14:14:29', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('886', '455', '2016-12-28 00:00:00', '2017-01-05 14:14:29', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('887', '456', '2016-12-30 00:00:00', '2017-01-05 14:15:09', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('888', '456', '2016-12-30 00:00:00', '2017-01-05 14:15:09', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('889', '457', '2016-12-31 00:00:00', '2017-01-05 14:15:49', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('890', '457', '2016-12-31 00:00:00', '2017-01-05 14:15:49', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('891', '458', '2017-01-01 00:00:00', '2017-01-05 14:16:27', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('892', '458', '2017-01-01 00:00:00', '2017-01-05 14:16:27', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('893', '459', '2017-01-02 00:00:00', '2017-01-05 14:17:04', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('894', '459', '2017-01-02 00:00:00', '2017-01-05 14:17:04', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('895', '460', '2017-01-03 00:00:00', '2017-01-05 14:17:41', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('896', '460', '2017-01-03 00:00:00', '2017-01-05 14:17:41', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('897', '461', '2017-01-04 00:00:00', '2017-01-05 14:18:21', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('898', '461', '2017-01-04 00:00:00', '2017-01-05 14:18:21', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('899', '462', '2017-01-04 00:00:00', '2017-01-05 14:23:56', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('900', '462', '2017-01-04 00:00:00', '2017-01-05 14:23:56', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('901', '463', '2017-01-03 00:00:00', '2017-01-05 14:28:58', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('902', '463', '2017-01-03 00:00:00', '2017-01-05 14:28:58', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('903', '464', '2017-01-04 00:00:00', '2017-01-05 14:29:50', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('904', '464', '2017-01-04 00:00:00', '2017-01-05 14:29:50', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('905', '465', '2016-12-23 00:00:00', '2017-01-05 14:32:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('906', '465', '2016-12-23 00:00:00', '2017-01-05 14:32:36', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('907', '466', '2016-12-28 00:00:00', '2017-01-05 14:33:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('908', '466', '2016-12-28 00:00:00', '2017-01-05 14:33:36', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('909', '467', '2016-12-29 00:00:00', '2017-01-05 14:34:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('910', '467', '2016-12-29 00:00:00', '2017-01-05 14:34:32', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('911', '468', '2016-12-30 00:00:00', '2017-01-05 14:35:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('912', '468', '2016-12-30 00:00:00', '2017-01-05 14:35:11', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('913', '469', '2016-12-31 00:00:00', '2017-01-05 14:35:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('914', '469', '2016-12-31 00:00:00', '2017-01-05 14:35:54', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('915', '470', '2017-01-02 00:00:00', '2017-01-05 14:36:57', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('916', '470', '2017-01-02 00:00:00', '2017-01-05 14:36:57', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('917', '471', '2017-01-03 00:00:00', '2017-01-05 14:37:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('918', '471', '2017-01-03 00:00:00', '2017-01-05 14:37:41', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('919', '472', '2017-01-03 00:00:00', '2017-01-05 14:40:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('920', '472', '2017-01-03 00:00:00', '2017-01-05 14:40:33', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('921', '473', '2016-12-23 00:00:00', '2017-01-05 14:43:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('922', '473', '2016-12-23 00:00:00', '2017-01-05 14:43:24', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('923', '474', '2016-12-25 00:00:00', '2017-01-05 14:44:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('924', '474', '2016-12-25 00:00:00', '2017-01-05 14:44:26', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('925', '475', '2016-12-26 00:00:00', '2017-01-05 14:45:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('926', '475', '2016-12-26 00:00:00', '2017-01-05 14:45:07', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('927', '476', '2016-12-27 00:00:00', '2017-01-05 14:46:02', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('928', '476', '2016-12-27 00:00:00', '2017-01-05 14:46:02', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('929', '477', '2016-12-28 00:00:00', '2017-01-05 14:46:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('930', '477', '2016-12-28 00:00:00', '2017-01-05 14:46:47', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('931', '478', '2016-12-29 00:00:00', '2017-01-05 14:47:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('932', '478', '2016-12-29 00:00:00', '2017-01-05 14:47:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('933', '479', '2017-01-02 00:00:00', '2017-01-05 14:48:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('934', '479', '2017-01-02 00:00:00', '2017-01-05 14:48:39', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('935', '480', '2017-01-03 00:00:00', '2017-01-05 14:49:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('936', '480', '2017-01-03 00:00:00', '2017-01-05 14:49:21', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('937', '481', '2017-01-04 00:00:00', '2017-01-05 14:49:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('938', '481', '2017-01-04 00:00:00', '2017-01-05 14:49:56', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('939', '482', '2016-12-23 00:00:00', '2017-01-05 14:58:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('940', '482', '2016-12-23 00:00:00', '2017-01-05 14:58:08', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('941', '483', '2016-12-26 00:00:00', '2017-01-05 14:59:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('942', '483', '2016-12-26 00:00:00', '2017-01-05 14:59:21', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('943', '484', '2017-01-03 00:00:00', '2017-01-05 15:00:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('944', '484', '2017-01-03 00:00:00', '2017-01-05 15:00:09', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('945', '485', '2016-12-23 00:00:00', '2017-01-05 15:04:54', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('946', '485', '2016-12-23 00:00:00', '2017-01-05 15:04:54', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('947', '486', '2016-12-24 00:00:00', '2017-01-05 15:05:53', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('948', '486', '2016-12-24 00:00:00', '2017-01-05 15:05:53', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('949', '487', '2016-12-25 00:00:00', '2017-01-05 15:06:33', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('950', '487', '2016-12-25 00:00:00', '2017-01-05 15:06:33', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('951', '488', '2016-12-26 00:00:00', '2017-01-05 15:07:20', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('952', '488', '2016-12-26 00:00:00', '2017-01-05 15:07:20', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('953', '489', '2016-12-27 00:00:00', '2017-01-05 15:07:58', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('954', '489', '2016-12-27 00:00:00', '2017-01-05 15:07:58', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('955', '490', '2016-12-28 00:00:00', '2017-01-05 15:08:46', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('956', '490', '2016-12-28 00:00:00', '2017-01-05 15:08:46', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('957', '491', '2016-12-29 00:00:00', '2017-01-05 15:09:27', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('958', '491', '2016-12-29 00:00:00', '2017-01-05 15:09:27', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('959', '492', '2016-12-30 00:00:00', '2017-01-05 15:10:05', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('960', '492', '2016-12-30 00:00:00', '2017-01-05 15:10:05', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('961', '493', '2016-12-31 00:00:00', '2017-01-05 15:10:49', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('962', '493', '2016-12-31 00:00:00', '2017-01-05 15:10:49', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('963', '494', '2017-01-01 00:00:00', '2017-01-05 15:11:28', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('964', '494', '2017-01-01 00:00:00', '2017-01-05 15:11:28', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('965', '495', '2017-01-02 00:00:00', '2017-01-05 15:12:03', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('966', '495', '2017-01-02 00:00:00', '2017-01-05 15:12:03', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('967', '496', '2017-01-03 00:00:00', '2017-01-05 15:12:41', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('968', '496', '2017-01-03 00:00:00', '2017-01-05 15:12:41', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('969', '497', '2017-01-04 00:00:00', '2017-01-05 15:13:14', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('970', '497', '2017-01-04 00:00:00', '2017-01-05 15:13:14', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('971', '498', '2017-01-05 00:00:00', '2017-01-05 15:21:35', '14', '0', '5600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('972', '498', '2017-01-05 00:00:00', '2017-01-05 15:21:35', '5', '1', '5600.00');
INSERT INTO `tb_ledgertransactions` VALUES ('973', '499', '2016-12-30 00:00:00', '2017-01-11 11:54:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('974', '499', '2016-12-30 00:00:00', '2017-01-11 11:54:06', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('975', '500', '2016-12-31 00:00:00', '2017-01-11 11:54:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('976', '500', '2016-12-31 00:00:00', '2017-01-11 11:54:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('977', '501', '2017-01-01 00:00:00', '2017-01-11 11:54:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('978', '501', '2017-01-01 00:00:00', '2017-01-11 11:54:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('979', '502', '2017-01-03 00:00:00', '2017-01-11 11:55:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('980', '502', '2017-01-03 00:00:00', '2017-01-11 11:55:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('981', '503', '2017-01-04 00:00:00', '2017-01-11 11:55:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('982', '503', '2017-01-04 00:00:00', '2017-01-11 11:55:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('983', '504', '2017-01-05 00:00:00', '2017-01-11 11:55:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('984', '504', '2017-01-05 00:00:00', '2017-01-11 11:55:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('985', '505', '2017-01-06 00:00:00', '2017-01-11 11:56:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('986', '505', '2017-01-06 00:00:00', '2017-01-11 11:56:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('987', '506', '2017-01-07 00:00:00', '2017-01-11 12:00:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('988', '506', '2017-01-07 00:00:00', '2017-01-11 12:00:26', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('989', '507', '2017-01-08 00:00:00', '2017-01-11 12:00:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('990', '507', '2017-01-08 00:00:00', '2017-01-11 12:00:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('991', '508', '2017-01-09 00:00:00', '2017-01-11 12:02:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('992', '508', '2017-01-09 00:00:00', '2017-01-11 12:02:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('993', '509', '2016-12-30 00:00:00', '2017-01-11 12:31:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('994', '509', '2016-12-30 00:00:00', '2017-01-11 12:31:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('995', '510', '2016-12-31 00:00:00', '2017-01-11 12:32:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('996', '510', '2016-12-31 00:00:00', '2017-01-11 12:32:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('997', '511', '2017-01-01 00:00:00', '2017-01-11 12:32:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('998', '511', '2017-01-01 00:00:00', '2017-01-11 12:32:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('999', '512', '2017-01-02 00:00:00', '2017-01-11 12:32:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1000', '512', '2017-01-02 00:00:00', '2017-01-11 12:32:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1001', '513', '2017-01-03 00:00:00', '2017-01-11 12:34:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1002', '513', '2017-01-03 00:00:00', '2017-01-11 12:34:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1003', '514', '2017-01-04 00:00:00', '2017-01-11 12:34:23', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1004', '514', '2017-01-04 00:00:00', '2017-01-11 12:34:23', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1005', '515', '2017-01-05 00:00:00', '2017-01-11 12:34:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1006', '515', '2017-01-05 00:00:00', '2017-01-11 12:34:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1007', '516', '2017-01-06 00:00:00', '2017-01-11 12:34:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1008', '516', '2017-01-06 00:00:00', '2017-01-11 12:34:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1009', '517', '2017-01-07 00:00:00', '2017-01-11 12:35:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1010', '517', '2017-01-07 00:00:00', '2017-01-11 12:35:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1011', '518', '2017-01-08 00:00:00', '2017-01-11 12:35:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1012', '518', '2017-01-08 00:00:00', '2017-01-11 12:35:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1013', '519', '2017-01-09 00:00:00', '2017-01-11 12:35:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1014', '519', '2017-01-09 00:00:00', '2017-01-11 12:35:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1015', '520', '2017-01-03 00:00:00', '2017-01-11 12:36:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1016', '520', '2017-01-03 00:00:00', '2017-01-11 12:36:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1017', '521', '2017-01-04 00:00:00', '2017-01-11 12:37:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1018', '521', '2017-01-04 00:00:00', '2017-01-11 12:37:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1019', '522', '2017-01-06 00:00:00', '2017-01-11 12:37:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1020', '522', '2017-01-06 00:00:00', '2017-01-11 12:37:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1021', '523', '2017-01-09 00:00:00', '2017-01-11 12:37:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1022', '523', '2017-01-09 00:00:00', '2017-01-11 12:37:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1023', '524', '2017-01-04 00:00:00', '2017-01-11 12:38:48', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1024', '524', '2017-01-04 00:00:00', '2017-01-11 12:38:48', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1025', '525', '2017-01-06 00:00:00', '2017-01-11 12:39:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1026', '525', '2017-01-06 00:00:00', '2017-01-11 12:39:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1027', '526', '2016-12-30 00:00:00', '2017-01-11 12:40:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1028', '526', '2016-12-30 00:00:00', '2017-01-11 12:40:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1029', '527', '2016-12-31 00:00:00', '2017-01-11 12:40:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1030', '527', '2016-12-31 00:00:00', '2017-01-11 12:40:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1031', '528', '2017-01-01 00:00:00', '2017-01-11 12:40:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1032', '528', '2017-01-01 00:00:00', '2017-01-11 12:40:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1033', '529', '2017-01-02 00:00:00', '2017-01-11 12:41:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1034', '529', '2017-01-02 00:00:00', '2017-01-11 12:41:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1035', '530', '2017-01-03 00:00:00', '2017-01-11 12:41:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1036', '530', '2017-01-03 00:00:00', '2017-01-11 12:41:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1037', '531', '2017-01-04 00:00:00', '2017-01-11 12:41:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1038', '531', '2017-01-04 00:00:00', '2017-01-11 12:41:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1039', '532', '2017-01-05 00:00:00', '2017-01-11 12:41:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1040', '532', '2017-01-05 00:00:00', '2017-01-11 12:41:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1041', '533', '2017-01-06 00:00:00', '2017-01-11 12:42:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1042', '533', '2017-01-06 00:00:00', '2017-01-11 12:42:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1043', '534', '2017-01-07 00:00:00', '2017-01-11 12:42:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1044', '534', '2017-01-07 00:00:00', '2017-01-11 12:42:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1045', '535', '2017-01-08 00:00:00', '2017-01-11 12:42:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1046', '535', '2017-01-08 00:00:00', '2017-01-11 12:42:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1047', '536', '2017-01-09 00:00:00', '2017-01-11 12:43:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1048', '536', '2017-01-09 00:00:00', '2017-01-11 12:43:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1049', '537', '2016-12-21 00:00:00', '2017-01-12 10:24:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1050', '537', '2016-12-21 00:00:00', '2017-01-12 10:24:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1051', '538', '2016-12-22 00:00:00', '2017-01-12 10:24:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1052', '538', '2016-12-22 00:00:00', '2017-01-12 10:24:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1053', '539', '2016-12-25 00:00:00', '2017-01-12 10:24:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1054', '539', '2016-12-25 00:00:00', '2017-01-12 10:24:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1055', '540', '2016-12-26 00:00:00', '2017-01-12 10:25:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1056', '540', '2016-12-26 00:00:00', '2017-01-12 10:25:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1057', '541', '2016-12-27 00:00:00', '2017-01-12 10:25:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1058', '541', '2016-12-27 00:00:00', '2017-01-12 10:25:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1059', '542', '2017-01-02 00:00:00', '2017-01-12 10:25:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1060', '542', '2017-01-02 00:00:00', '2017-01-12 10:25:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1061', '543', '2017-01-03 00:00:00', '2017-01-12 10:26:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1062', '543', '2017-01-03 00:00:00', '2017-01-12 10:26:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1063', '544', '2017-01-05 00:00:00', '2017-01-12 10:26:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1064', '544', '2017-01-05 00:00:00', '2017-01-12 10:26:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1065', '545', '2017-01-07 00:00:00', '2017-01-12 10:27:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1066', '545', '2017-01-07 00:00:00', '2017-01-12 10:27:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1067', '546', '2017-01-09 00:00:00', '2017-01-12 10:27:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1068', '546', '2017-01-09 00:00:00', '2017-01-12 10:27:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1069', '547', '2016-12-31 00:00:00', '2017-01-12 10:34:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1070', '547', '2016-12-31 00:00:00', '2017-01-12 10:34:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1071', '548', '2017-01-02 00:00:00', '2017-01-12 10:34:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1072', '548', '2017-01-02 00:00:00', '2017-01-12 10:34:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1073', '549', '2017-01-06 00:00:00', '2017-01-12 10:35:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1074', '549', '2017-01-06 00:00:00', '2017-01-12 10:35:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1075', '550', '2017-01-07 00:00:00', '2017-01-12 10:35:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1076', '550', '2017-01-07 00:00:00', '2017-01-12 10:35:38', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1077', '551', '2017-01-09 00:00:00', '2017-01-12 10:35:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1078', '551', '2017-01-09 00:00:00', '2017-01-12 10:35:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1079', '552', '2016-12-31 00:00:00', '2017-01-12 10:37:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1080', '552', '2016-12-31 00:00:00', '2017-01-12 10:37:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1081', '553', '2017-01-01 00:00:00', '2017-01-12 10:37:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1082', '553', '2017-01-01 00:00:00', '2017-01-12 10:37:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1083', '554', '2017-01-04 00:00:00', '2017-01-12 10:38:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1084', '554', '2017-01-04 00:00:00', '2017-01-12 10:38:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1085', '555', '2017-01-05 00:00:00', '2017-01-12 10:38:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1086', '555', '2017-01-05 00:00:00', '2017-01-12 10:38:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1087', '556', '2017-01-07 00:00:00', '2017-01-12 10:39:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1088', '556', '2017-01-07 00:00:00', '2017-01-12 10:39:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1089', '557', '2017-01-10 00:00:00', '2017-01-12 10:39:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1090', '557', '2017-01-10 00:00:00', '2017-01-12 10:39:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1091', '558', '2016-12-31 00:00:00', '2017-01-12 10:43:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1092', '558', '2016-12-31 00:00:00', '2017-01-12 10:43:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1093', '559', '2017-01-02 00:00:00', '2017-01-12 10:44:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1094', '559', '2017-01-02 00:00:00', '2017-01-12 10:44:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1095', '560', '2017-01-03 00:00:00', '2017-01-12 10:44:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1096', '560', '2017-01-03 00:00:00', '2017-01-12 10:44:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1097', '561', '2017-01-04 00:00:00', '2017-01-12 10:44:39', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1098', '561', '2017-01-04 00:00:00', '2017-01-12 10:44:39', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1099', '562', '2017-01-05 00:00:00', '2017-01-12 10:44:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1100', '562', '2017-01-05 00:00:00', '2017-01-12 10:44:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1101', '563', '2017-01-06 00:00:00', '2017-01-12 10:45:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1102', '563', '2017-01-06 00:00:00', '2017-01-12 10:45:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1103', '564', '2017-01-09 00:00:00', '2017-01-12 10:45:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1104', '564', '2017-01-09 00:00:00', '2017-01-12 10:45:36', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1105', '565', '2017-01-10 00:00:00', '2017-01-12 10:45:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1106', '565', '2017-01-10 00:00:00', '2017-01-12 10:45:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1107', '566', '2017-01-03 00:00:00', '2017-01-12 10:48:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1108', '566', '2017-01-03 00:00:00', '2017-01-12 10:48:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1109', '567', '2017-01-02 00:00:00', '2017-01-12 10:49:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1110', '567', '2017-01-02 00:00:00', '2017-01-12 10:49:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1111', '568', '2017-01-03 00:00:00', '2017-01-12 10:49:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1112', '568', '2017-01-03 00:00:00', '2017-01-12 10:49:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1113', '569', '2017-01-06 00:00:00', '2017-01-12 10:50:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1114', '569', '2017-01-06 00:00:00', '2017-01-12 10:50:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1115', '570', '2016-12-30 00:00:00', '2017-01-12 10:53:22', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1116', '570', '2016-12-30 00:00:00', '2017-01-12 10:53:22', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1117', '571', '2016-12-31 00:00:00', '2017-01-12 10:53:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1118', '571', '2016-12-31 00:00:00', '2017-01-12 10:53:41', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1119', '572', '2017-01-05 00:00:00', '2017-01-12 10:54:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1120', '572', '2017-01-05 00:00:00', '2017-01-12 10:54:08', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1121', '573', '2017-01-06 00:00:00', '2017-01-12 10:54:26', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1122', '573', '2017-01-06 00:00:00', '2017-01-12 10:54:26', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1123', '574', '2017-01-08 00:00:00', '2017-01-12 10:54:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1124', '574', '2017-01-08 00:00:00', '2017-01-12 10:54:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1125', '575', '2016-12-31 00:00:00', '2017-01-12 10:56:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1126', '575', '2016-12-31 00:00:00', '2017-01-12 10:56:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1127', '576', '2017-01-04 00:00:00', '2017-01-12 10:56:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1128', '576', '2017-01-04 00:00:00', '2017-01-12 10:56:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1129', '577', '2017-01-05 00:00:00', '2017-01-12 10:57:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1130', '577', '2017-01-05 00:00:00', '2017-01-12 10:57:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1131', '578', '2017-01-07 00:00:00', '2017-01-12 10:57:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1132', '578', '2017-01-07 00:00:00', '2017-01-12 10:57:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1133', '579', '2017-01-09 00:00:00', '2017-01-12 10:58:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1134', '579', '2017-01-09 00:00:00', '2017-01-12 10:58:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1135', '580', '2016-12-31 00:00:00', '2017-01-12 10:59:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1136', '580', '2016-12-31 00:00:00', '2017-01-12 10:59:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1137', '581', '2017-01-07 00:00:00', '2017-01-12 10:59:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1138', '581', '2017-01-07 00:00:00', '2017-01-12 10:59:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1139', '582', '2017-01-09 00:00:00', '2017-01-12 11:00:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1140', '582', '2017-01-09 00:00:00', '2017-01-12 11:00:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1141', '583', '2017-01-04 00:00:00', '2017-01-12 11:03:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1142', '583', '2017-01-04 00:00:00', '2017-01-12 11:03:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1143', '584', '2017-01-05 00:00:00', '2017-01-12 11:04:07', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1144', '584', '2017-01-05 00:00:00', '2017-01-12 11:04:07', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1145', '585', '2017-01-07 00:00:00', '2017-01-12 11:04:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1146', '585', '2017-01-07 00:00:00', '2017-01-12 11:04:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1147', '586', '2016-12-30 00:00:00', '2017-01-12 11:10:57', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1148', '586', '2016-12-30 00:00:00', '2017-01-12 11:10:57', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1149', '587', '2017-01-09 00:00:00', '2017-01-12 11:14:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1150', '587', '2017-01-09 00:00:00', '2017-01-12 11:14:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1151', '588', '2017-01-10 00:00:00', '2017-01-12 11:14:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1152', '588', '2017-01-10 00:00:00', '2017-01-12 11:14:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1153', '589', '2016-12-30 00:00:00', '2017-01-12 11:16:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1154', '589', '2016-12-30 00:00:00', '2017-01-12 11:16:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1155', '590', '2016-12-31 00:00:00', '2017-01-12 11:16:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1156', '590', '2016-12-31 00:00:00', '2017-01-12 11:16:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1157', '591', '2017-01-01 00:00:00', '2017-01-12 11:17:02', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1158', '591', '2017-01-01 00:00:00', '2017-01-12 11:17:02', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1159', '592', '2017-01-02 00:00:00', '2017-01-12 11:17:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1160', '592', '2017-01-02 00:00:00', '2017-01-12 11:17:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1161', '593', '2017-01-03 00:00:00', '2017-01-12 11:17:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1162', '593', '2017-01-03 00:00:00', '2017-01-12 11:17:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1163', '594', '2017-01-04 00:00:00', '2017-01-12 11:18:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1164', '594', '2017-01-04 00:00:00', '2017-01-12 11:18:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1165', '595', '2017-01-05 00:00:00', '2017-01-12 11:18:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1166', '595', '2017-01-05 00:00:00', '2017-01-12 11:18:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1167', '596', '2017-01-06 00:00:00', '2017-01-12 11:18:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1168', '596', '2017-01-06 00:00:00', '2017-01-12 11:18:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1169', '597', '2017-01-07 00:00:00', '2017-01-12 11:18:56', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1170', '597', '2017-01-07 00:00:00', '2017-01-12 11:18:56', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1171', '598', '2017-01-13 00:00:00', '2017-01-16 11:14:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1172', '598', '2017-01-13 00:00:00', '2017-01-16 11:14:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1173', '599', '2017-01-08 00:00:00', '2017-01-16 11:23:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1174', '599', '2017-01-08 00:00:00', '2017-01-16 11:23:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1175', '600', '2017-01-09 00:00:00', '2017-01-16 11:23:57', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1176', '600', '2017-01-09 00:00:00', '2017-01-16 11:23:57', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1177', '601', '2017-01-11 00:00:00', '2017-01-16 11:24:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1178', '601', '2017-01-11 00:00:00', '2017-01-16 11:24:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1179', '602', '2017-01-31 00:00:00', '2017-01-16 11:26:04', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1180', '602', '2017-01-31 00:00:00', '2017-01-16 11:26:04', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1181', '603', '2017-01-01 00:00:00', '2017-01-16 11:26:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1182', '603', '2017-01-01 00:00:00', '2017-01-16 11:26:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1183', '604', '2017-01-02 00:00:00', '2017-01-16 11:26:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1184', '604', '2017-01-02 00:00:00', '2017-01-16 11:26:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1185', '605', '2017-01-03 00:00:00', '2017-01-16 11:26:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1186', '605', '2017-01-03 00:00:00', '2017-01-16 11:26:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1187', '606', '2017-01-04 00:00:00', '2017-01-16 11:27:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1188', '606', '2017-01-04 00:00:00', '2017-01-16 11:27:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1189', '607', '2017-01-05 00:00:00', '2017-01-16 11:27:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1190', '607', '2017-01-05 00:00:00', '2017-01-16 11:27:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1191', '608', '2017-01-06 00:00:00', '2017-01-16 11:27:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1192', '608', '2017-01-06 00:00:00', '2017-01-16 11:27:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1193', '609', '2017-01-07 00:00:00', '2017-01-16 11:28:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1194', '609', '2017-01-07 00:00:00', '2017-01-16 11:28:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1195', '610', '2017-01-08 00:00:00', '2017-01-16 11:28:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1196', '610', '2017-01-08 00:00:00', '2017-01-16 11:28:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1197', '611', '2017-01-09 00:00:00', '2017-01-16 11:29:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1198', '611', '2017-01-09 00:00:00', '2017-01-16 11:29:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1199', '612', '2017-01-11 00:00:00', '2017-01-16 11:29:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1200', '612', '2017-01-11 00:00:00', '2017-01-16 11:29:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1201', '613', '2016-12-31 00:00:00', '2017-01-16 11:30:51', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1202', '613', '2016-12-31 00:00:00', '2017-01-16 11:30:51', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1203', '614', '2017-01-02 00:00:00', '2017-01-16 11:31:10', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1204', '614', '2017-01-02 00:00:00', '2017-01-16 11:31:10', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1205', '615', '2017-01-03 00:00:00', '2017-01-16 11:31:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1206', '615', '2017-01-03 00:00:00', '2017-01-16 11:31:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1207', '616', '2017-01-04 00:00:00', '2017-01-16 11:31:42', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1208', '616', '2017-01-04 00:00:00', '2017-01-16 11:31:42', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1209', '617', '2017-01-05 00:00:00', '2017-01-16 11:31:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1210', '617', '2017-01-05 00:00:00', '2017-01-16 11:31:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1211', '618', '2017-01-06 00:00:00', '2017-01-16 11:32:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1212', '618', '2017-01-06 00:00:00', '2017-01-16 11:32:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1213', '619', '2017-01-07 00:00:00', '2017-01-16 11:32:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1214', '619', '2017-01-07 00:00:00', '2017-01-16 11:32:35', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1215', '620', '2017-01-09 00:00:00', '2017-01-16 11:32:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1216', '620', '2017-01-09 00:00:00', '2017-01-16 11:32:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1217', '621', '2017-01-10 00:00:00', '2017-01-16 11:33:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1218', '621', '2017-01-10 00:00:00', '2017-01-16 11:33:09', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1219', '622', '2017-01-13 00:00:00', '2017-01-16 11:33:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1220', '622', '2017-01-13 00:00:00', '2017-01-16 11:33:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1221', '623', '2017-01-14 00:00:00', '2017-01-16 11:33:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1222', '623', '2017-01-14 00:00:00', '2017-01-16 11:33:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1223', '624', '2017-01-02 00:00:00', '2017-01-17 12:22:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1224', '624', '2017-01-02 00:00:00', '2017-01-17 12:22:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1225', '625', '2017-01-03 00:00:00', '2017-01-17 12:22:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1226', '625', '2017-01-03 00:00:00', '2017-01-17 12:22:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1227', '626', '2017-01-04 00:00:00', '2017-01-17 12:23:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1228', '626', '2017-01-04 00:00:00', '2017-01-17 12:23:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1229', '627', '2017-01-06 00:00:00', '2017-01-17 12:23:28', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1230', '627', '2017-01-06 00:00:00', '2017-01-17 12:23:28', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1231', '628', '2017-01-09 00:00:00', '2017-01-17 12:23:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1232', '628', '2017-01-09 00:00:00', '2017-01-17 12:23:45', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1233', '629', '2017-01-11 00:00:00', '2017-01-17 12:24:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1234', '629', '2017-01-11 00:00:00', '2017-01-17 12:24:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1235', '630', '2017-01-12 00:00:00', '2017-01-17 12:26:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1236', '630', '2017-01-12 00:00:00', '2017-01-17 12:26:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1237', '631', '2017-01-13 00:00:00', '2017-01-17 12:26:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1238', '631', '2017-01-13 00:00:00', '2017-01-17 12:26:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1239', '632', '2017-01-14 00:00:00', '2017-01-17 12:27:13', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1240', '632', '2017-01-14 00:00:00', '2017-01-17 12:27:13', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1241', '633', '2017-01-12 00:00:00', '2017-01-17 12:31:21', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1242', '633', '2017-01-12 00:00:00', '2017-01-17 12:31:21', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1243', '634', '2017-01-03 00:00:00', '2017-01-17 12:33:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1244', '634', '2017-01-03 00:00:00', '2017-01-17 12:33:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1245', '635', '2017-01-04 00:00:00', '2017-01-17 12:33:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1246', '635', '2017-01-04 00:00:00', '2017-01-17 12:33:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1247', '636', '2017-01-06 00:00:00', '2017-01-17 12:33:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1248', '636', '2017-01-06 00:00:00', '2017-01-17 12:33:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1249', '637', '2017-01-11 00:00:00', '2017-01-17 12:34:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1250', '637', '2017-01-11 00:00:00', '2017-01-17 12:34:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1251', '638', '2017-01-13 00:00:00', '2017-01-17 12:35:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1252', '638', '2017-01-13 00:00:00', '2017-01-17 12:35:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1253', '639', '2017-01-14 00:00:00', '2017-01-17 12:35:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1254', '639', '2017-01-14 00:00:00', '2017-01-17 12:35:19', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1255', '640', '2016-12-31 00:00:00', '2017-01-17 12:36:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1256', '640', '2016-12-31 00:00:00', '2017-01-17 12:36:54', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1257', '641', '2017-01-01 00:00:00', '2017-01-17 12:37:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1258', '641', '2017-01-01 00:00:00', '2017-01-17 12:37:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1259', '642', '2017-01-02 00:00:00', '2017-01-17 12:37:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1260', '642', '2017-01-02 00:00:00', '2017-01-17 12:37:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1261', '643', '2017-01-03 00:00:00', '2017-01-17 12:37:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1262', '643', '2017-01-03 00:00:00', '2017-01-17 12:37:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1263', '644', '2017-01-04 00:00:00', '2017-01-17 12:38:02', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1264', '644', '2017-01-04 00:00:00', '2017-01-17 12:38:02', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1265', '645', '2017-01-05 00:00:00', '2017-01-17 12:38:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1266', '645', '2017-01-05 00:00:00', '2017-01-17 12:38:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1267', '646', '2017-01-06 00:00:00', '2017-01-17 12:38:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1268', '646', '2017-01-06 00:00:00', '2017-01-17 12:38:33', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1269', '647', '2017-01-11 00:00:00', '2017-01-17 12:38:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1270', '647', '2017-01-11 00:00:00', '2017-01-17 12:38:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1271', '648', '2017-01-13 00:00:00', '2017-01-17 12:39:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1272', '648', '2017-01-13 00:00:00', '2017-01-17 12:39:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1273', '649', '2017-01-14 00:00:00', '2017-01-17 12:39:32', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1274', '649', '2017-01-14 00:00:00', '2017-01-17 12:39:32', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1275', '650', '2017-01-15 00:00:00', '2017-01-17 12:39:48', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1276', '650', '2017-01-15 00:00:00', '2017-01-17 12:39:48', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1277', '651', '2017-01-16 00:00:00', '2017-01-17 12:40:05', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1278', '651', '2017-01-16 00:00:00', '2017-01-17 12:40:05', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1279', '652', '2016-12-30 00:00:00', '2017-01-17 12:42:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1280', '652', '2016-12-30 00:00:00', '2017-01-17 12:42:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1281', '653', '2016-12-31 00:00:00', '2017-01-17 12:42:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1282', '653', '2016-12-31 00:00:00', '2017-01-17 12:42:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1283', '654', '2017-01-01 00:00:00', '2017-01-17 12:42:52', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1284', '654', '2017-01-01 00:00:00', '2017-01-17 12:42:52', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1285', '655', '2017-01-02 00:00:00', '2017-01-17 12:43:11', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1286', '655', '2017-01-02 00:00:00', '2017-01-17 12:43:11', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1287', '656', '2017-01-03 00:00:00', '2017-01-17 12:43:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1288', '656', '2017-01-03 00:00:00', '2017-01-17 12:43:24', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1289', '657', '2017-01-04 00:00:00', '2017-01-17 12:43:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1290', '657', '2017-01-04 00:00:00', '2017-01-17 12:43:37', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1291', '658', '2017-01-05 00:00:00', '2017-01-17 12:43:50', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1292', '658', '2017-01-05 00:00:00', '2017-01-17 12:43:50', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1293', '659', '2017-01-06 00:00:00', '2017-01-17 12:44:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1294', '659', '2017-01-06 00:00:00', '2017-01-17 12:44:03', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1295', '660', '2017-01-07 00:00:00', '2017-01-17 12:44:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1296', '660', '2017-01-07 00:00:00', '2017-01-17 12:44:25', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1297', '661', '2017-01-09 00:00:00', '2017-01-17 12:44:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1298', '661', '2017-01-09 00:00:00', '2017-01-17 12:44:43', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1299', '662', '2017-01-10 00:00:00', '2017-01-17 12:44:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1300', '662', '2017-01-10 00:00:00', '2017-01-17 12:44:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1301', '663', '2017-01-11 00:00:00', '2017-01-17 12:45:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1302', '663', '2017-01-11 00:00:00', '2017-01-17 12:45:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1303', '664', '2017-01-12 00:00:00', '2017-01-17 12:45:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1304', '664', '2017-01-12 00:00:00', '2017-01-17 12:45:27', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1305', '665', '2017-01-13 00:00:00', '2017-01-17 12:45:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1306', '665', '2017-01-13 00:00:00', '2017-01-17 12:45:43', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1307', '666', '2017-01-14 00:00:00', '2017-01-17 12:45:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1308', '666', '2017-01-14 00:00:00', '2017-01-17 12:45:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1309', '667', '2017-01-09 00:00:00', '2017-01-17 12:50:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1310', '667', '2017-01-09 00:00:00', '2017-01-17 12:50:15', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1311', '668', '2017-01-10 00:00:00', '2017-01-17 12:50:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1312', '668', '2017-01-10 00:00:00', '2017-01-17 12:50:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1313', '669', '2017-01-12 00:00:00', '2017-01-17 12:50:48', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1314', '669', '2017-01-12 00:00:00', '2017-01-17 12:50:48', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1315', '670', '2017-01-13 00:00:00', '2017-01-17 12:51:02', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1316', '670', '2017-01-13 00:00:00', '2017-01-17 12:51:02', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1317', '671', '2017-01-14 00:00:00', '2017-01-17 12:51:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1318', '671', '2017-01-14 00:00:00', '2017-01-17 12:51:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1319', '672', '2017-01-15 00:00:00', '2017-01-17 12:51:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1320', '672', '2017-01-15 00:00:00', '2017-01-17 12:51:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1321', '673', '2017-01-16 00:00:00', '2017-01-17 12:51:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1322', '673', '2017-01-16 00:00:00', '2017-01-17 12:51:58', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1323', '674', '2017-01-10 00:00:00', '2017-01-17 12:53:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1324', '674', '2017-01-10 00:00:00', '2017-01-17 12:53:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1325', '675', '2017-01-11 00:00:00', '2017-01-17 12:53:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1326', '675', '2017-01-11 00:00:00', '2017-01-17 12:53:40', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1327', '676', '2017-01-12 00:00:00', '2017-01-17 12:54:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1328', '676', '2017-01-12 00:00:00', '2017-01-17 12:54:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1329', '677', '2017-01-13 00:00:00', '2017-01-17 13:09:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1330', '677', '2017-01-13 00:00:00', '2017-01-17 13:09:20', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1331', '678', '2017-01-14 00:00:00', '2017-01-17 13:09:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1332', '678', '2017-01-14 00:00:00', '2017-01-17 13:09:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1333', '679', '2017-01-15 00:00:00', '2017-01-17 13:10:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1334', '679', '2017-01-15 00:00:00', '2017-01-17 13:10:14', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1335', '680', '2017-01-16 00:00:00', '2017-01-17 13:10:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1336', '680', '2017-01-16 00:00:00', '2017-01-17 13:10:29', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1337', '681', '2017-01-11 00:00:00', '2017-01-17 13:12:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1338', '681', '2017-01-11 00:00:00', '2017-01-17 13:12:55', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1339', '682', '2017-01-13 00:00:00', '2017-01-17 13:13:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1340', '682', '2017-01-13 00:00:00', '2017-01-17 13:13:16', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1341', '683', '2017-01-10 00:00:00', '2017-01-17 13:18:31', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1342', '683', '2017-01-10 00:00:00', '2017-01-17 13:18:31', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1343', '684', '2017-01-11 00:00:00', '2017-01-17 13:18:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1344', '684', '2017-01-11 00:00:00', '2017-01-17 13:18:47', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1345', '685', '2017-01-12 00:00:00', '2017-01-17 13:19:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1346', '685', '2017-01-12 00:00:00', '2017-01-17 13:19:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1347', '686', '2017-01-13 00:00:00', '2017-01-17 13:19:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1348', '686', '2017-01-13 00:00:00', '2017-01-17 13:19:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1349', '687', '2017-01-14 00:00:00', '2017-01-17 13:19:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1350', '687', '2017-01-14 00:00:00', '2017-01-17 13:19:44', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1351', '688', '2017-01-15 00:00:00', '2017-01-17 13:20:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1352', '688', '2017-01-15 00:00:00', '2017-01-17 13:20:01', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1353', '689', '2017-01-16 00:00:00', '2017-01-17 13:20:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1354', '689', '2017-01-16 00:00:00', '2017-01-17 13:20:18', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1355', '690', '2017-01-10 00:00:00', '2017-01-17 13:24:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1356', '690', '2017-01-10 00:00:00', '2017-01-17 13:24:46', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1357', '691', '2017-01-11 00:00:00', '2017-01-17 13:24:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1358', '691', '2017-01-11 00:00:00', '2017-01-17 13:24:59', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1359', '692', '2017-01-13 00:00:00', '2017-01-17 13:25:17', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1360', '692', '2017-01-13 00:00:00', '2017-01-17 13:25:17', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1361', '693', '2017-01-14 00:00:00', '2017-01-17 13:25:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1362', '693', '2017-01-14 00:00:00', '2017-01-17 13:25:36', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1363', '694', '2017-01-02 00:00:00', '2017-01-17 15:05:34', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1364', '694', '2017-01-02 00:00:00', '2017-01-17 15:05:34', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1365', '695', '2017-01-03 00:00:00', '2017-01-17 15:06:00', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1366', '695', '2017-01-03 00:00:00', '2017-01-17 15:06:00', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1367', '696', '2017-01-05 00:00:00', '2017-01-17 15:06:22', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1368', '696', '2017-01-05 00:00:00', '2017-01-17 15:06:22', '20', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1369', '697', '2016-12-28 00:00:00', '2017-01-24 11:00:09', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1370', '697', '2016-12-28 00:00:00', '2017-01-24 11:00:09', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1371', '698', '2017-01-01 00:00:00', '2017-01-24 11:01:06', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1372', '698', '2017-01-01 00:00:00', '2017-01-24 11:01:06', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1373', '699', '2017-01-02 00:00:00', '2017-01-24 11:02:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1374', '699', '2017-01-02 00:00:00', '2017-01-24 11:02:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1375', '700', '2017-01-03 00:00:00', '2017-01-24 11:03:30', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1376', '700', '2017-01-03 00:00:00', '2017-01-24 11:03:30', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1377', '701', '2017-01-04 00:00:00', '2017-01-24 11:04:34', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1378', '701', '2017-01-04 00:00:00', '2017-01-24 11:04:34', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1379', '702', '2017-01-05 00:00:00', '2017-01-24 11:05:25', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1380', '702', '2017-01-05 00:00:00', '2017-01-24 11:05:25', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1381', '703', '2017-01-06 00:00:00', '2017-01-24 11:08:15', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1382', '703', '2017-01-06 00:00:00', '2017-01-24 11:08:15', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1383', '704', '2017-01-07 00:00:00', '2017-01-24 11:09:09', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1384', '704', '2017-01-07 00:00:00', '2017-01-24 11:09:09', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1385', '705', '2017-01-08 00:00:00', '2017-01-24 11:10:15', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1386', '705', '2017-01-08 00:00:00', '2017-01-24 11:10:15', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1387', '706', '2017-01-09 00:00:00', '2017-01-24 11:11:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1388', '706', '2017-01-09 00:00:00', '2017-01-24 11:11:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1389', '707', '2017-01-10 00:00:00', '2017-01-24 11:12:27', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1390', '707', '2017-01-10 00:00:00', '2017-01-24 11:12:27', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1391', '708', '2017-01-11 00:00:00', '2017-01-24 11:14:08', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1392', '708', '2017-01-11 00:00:00', '2017-01-24 11:14:08', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1393', '709', '2017-01-12 00:00:00', '2017-01-24 11:15:14', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1394', '709', '2017-01-12 00:00:00', '2017-01-24 11:15:14', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1395', '710', '2017-01-13 00:00:00', '2017-01-24 11:16:26', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1396', '710', '2017-01-13 00:00:00', '2017-01-24 11:16:26', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1397', '711', '2017-01-14 00:00:00', '2017-01-24 11:17:20', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1398', '711', '2017-01-14 00:00:00', '2017-01-24 11:17:20', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1399', '712', '2017-01-15 00:00:00', '2017-01-24 11:18:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1400', '712', '2017-01-15 00:00:00', '2017-01-24 11:18:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1401', '713', '2017-01-16 00:00:00', '2017-01-24 11:19:19', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1402', '713', '2017-01-16 00:00:00', '2017-01-24 11:19:19', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1403', '714', '2017-01-15 00:00:00', '2017-01-24 11:22:55', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1404', '714', '2017-01-15 00:00:00', '2017-01-24 11:22:55', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1405', '715', '2017-01-16 00:00:00', '2017-01-24 11:24:14', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1406', '715', '2017-01-16 00:00:00', '2017-01-24 11:24:14', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1407', '716', '2017-01-17 00:00:00', '2017-01-24 11:25:24', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1408', '716', '2017-01-17 00:00:00', '2017-01-24 11:25:24', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1409', '717', '2016-12-31 00:00:00', '2017-01-24 11:34:20', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1410', '717', '2016-12-31 00:00:00', '2017-01-24 11:34:20', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1411', '718', '2017-01-02 00:00:00', '2017-01-24 11:35:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1412', '718', '2017-01-02 00:00:00', '2017-01-24 11:35:45', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1413', '719', '2017-01-03 00:00:00', '2017-01-24 11:36:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1414', '719', '2017-01-03 00:00:00', '2017-01-24 11:36:41', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1415', '720', '2017-01-04 00:00:00', '2017-01-24 11:37:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1416', '720', '2017-01-04 00:00:00', '2017-01-24 11:37:38', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1417', '721', '2017-01-05 00:00:00', '2017-01-24 11:45:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1418', '721', '2017-01-05 00:00:00', '2017-01-24 11:45:29', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1419', '722', '2017-01-07 00:00:00', '2017-01-24 11:46:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1420', '722', '2017-01-07 00:00:00', '2017-01-24 11:46:45', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1421', '723', '2017-01-08 00:00:00', '2017-01-24 11:47:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1422', '723', '2017-01-08 00:00:00', '2017-01-24 11:47:37', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1423', '724', '2017-01-09 00:00:00', '2017-01-24 11:48:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1424', '724', '2017-01-09 00:00:00', '2017-01-24 11:48:37', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1425', '725', '2017-01-10 00:00:00', '2017-01-24 11:49:51', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1426', '725', '2017-01-10 00:00:00', '2017-01-24 11:49:51', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1427', '726', '2017-01-11 00:00:00', '2017-01-24 11:50:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1428', '726', '2017-01-11 00:00:00', '2017-01-24 11:50:40', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1429', '727', '2017-01-12 00:00:00', '2017-01-24 11:51:36', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1430', '727', '2017-01-12 00:00:00', '2017-01-24 11:51:36', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1431', '728', '2017-01-13 00:00:00', '2017-01-24 11:52:44', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1432', '728', '2017-01-13 00:00:00', '2017-01-24 11:52:44', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1433', '729', '2017-01-14 00:00:00', '2017-01-24 11:53:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1434', '729', '2017-01-14 00:00:00', '2017-01-24 11:53:43', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1435', '730', '2017-01-15 00:00:00', '2017-01-24 11:54:49', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1436', '730', '2017-01-15 00:00:00', '2017-01-24 11:54:49', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1437', '731', '2017-01-16 00:00:00', '2017-01-24 11:55:47', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1438', '731', '2017-01-16 00:00:00', '2017-01-24 11:55:47', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1439', '732', '2017-01-17 00:00:00', '2017-01-24 11:56:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1440', '732', '2017-01-17 00:00:00', '2017-01-24 11:56:43', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1441', '733', '2017-01-05 00:00:00', '2017-01-24 12:01:35', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1442', '733', '2017-01-05 00:00:00', '2017-01-24 12:01:35', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1443', '734', '2017-01-06 00:00:00', '2017-01-24 12:02:46', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1444', '734', '2017-01-06 00:00:00', '2017-01-24 12:02:46', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1445', '735', '2017-01-07 00:00:00', '2017-01-24 12:03:41', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1446', '735', '2017-01-07 00:00:00', '2017-01-24 12:03:41', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1447', '736', '2017-01-09 00:00:00', '2017-01-24 12:04:54', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1448', '736', '2017-01-09 00:00:00', '2017-01-24 12:04:54', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1449', '737', '2017-01-10 00:00:00', '2017-01-24 12:06:36', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1450', '737', '2017-01-10 00:00:00', '2017-01-24 12:06:36', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1451', '738', '2017-01-11 00:00:00', '2017-01-24 12:07:36', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1452', '738', '2017-01-11 00:00:00', '2017-01-24 12:07:36', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1453', '739', '2017-01-12 00:00:00', '2017-01-24 12:08:32', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1454', '739', '2017-01-12 00:00:00', '2017-01-24 12:08:32', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1455', '740', '2017-01-13 00:00:00', '2017-01-24 12:09:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1456', '740', '2017-01-13 00:00:00', '2017-01-24 12:09:22', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1457', '741', '2017-01-14 00:00:00', '2017-01-24 12:10:28', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1458', '741', '2017-01-14 00:00:00', '2017-01-24 12:10:28', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1459', '742', '2017-01-16 00:00:00', '2017-01-24 12:11:33', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1460', '742', '2017-01-16 00:00:00', '2017-01-24 12:11:33', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1461', '743', '2017-01-17 00:00:00', '2017-01-24 12:12:30', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1462', '743', '2017-01-17 00:00:00', '2017-01-24 12:12:30', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1463', '744', '2017-01-18 00:00:00', '2017-01-24 12:13:09', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1464', '744', '2017-01-18 00:00:00', '2017-01-24 12:13:09', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1465', '745', '2017-01-19 00:00:00', '2017-01-24 12:14:11', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1466', '745', '2017-01-19 00:00:00', '2017-01-24 12:14:11', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1467', '746', '2017-01-20 00:00:00', '2017-01-24 12:14:52', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1468', '746', '2017-01-20 00:00:00', '2017-01-24 12:14:52', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1469', '747', '2017-01-21 00:00:00', '2017-01-24 12:15:52', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1470', '747', '2017-01-21 00:00:00', '2017-01-24 12:15:52', '5', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1471', '748', '2017-01-05 00:00:00', '2017-01-24 13:18:45', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1472', '748', '2017-01-05 00:00:00', '2017-01-24 13:18:45', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1473', '749', '2017-01-07 00:00:00', '2017-01-24 13:19:34', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1474', '749', '2017-01-07 00:00:00', '2017-01-24 13:19:34', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1475', '750', '2017-01-10 00:00:00', '2017-01-24 13:30:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1476', '750', '2017-01-10 00:00:00', '2017-01-24 13:30:32', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1477', '751', '2017-01-16 00:00:00', '2017-01-24 13:31:12', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1478', '751', '2017-01-16 00:00:00', '2017-01-24 13:31:12', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1479', '752', '2017-01-17 00:00:00', '2017-01-24 13:31:56', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1480', '752', '2017-01-17 00:00:00', '2017-01-24 13:31:56', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1481', '753', '2017-01-19 00:00:00', '2017-01-24 13:32:59', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1482', '753', '2017-01-19 00:00:00', '2017-01-24 13:32:59', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1483', '754', '2017-01-16 00:00:00', '2017-01-25 11:39:08', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1484', '754', '2017-01-16 00:00:00', '2017-01-25 11:39:08', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1485', '755', '2017-01-17 00:00:00', '2017-01-25 11:40:12', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1486', '755', '2017-01-17 00:00:00', '2017-01-25 11:40:12', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1487', '756', '2017-01-18 00:00:00', '2017-01-25 11:41:19', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1488', '756', '2017-01-18 00:00:00', '2017-01-25 11:41:19', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1489', '757', '2017-01-19 00:00:00', '2017-01-25 11:42:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1490', '757', '2017-01-19 00:00:00', '2017-01-25 11:42:18', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1491', '758', '2017-01-20 00:00:00', '2017-01-25 11:44:18', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1492', '758', '2017-01-20 00:00:00', '2017-01-25 11:44:18', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1493', '759', '2017-01-21 00:00:00', '2017-01-25 11:45:15', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1494', '759', '2017-01-21 00:00:00', '2017-01-25 11:45:15', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1495', '760', '2017-01-22 00:00:00', '2017-01-25 11:46:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1496', '760', '2017-01-22 00:00:00', '2017-01-25 11:46:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1497', '761', '2017-01-23 00:00:00', '2017-01-25 11:47:54', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1498', '761', '2017-01-23 00:00:00', '2017-01-25 11:47:54', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1499', '762', '2017-01-24 00:00:00', '2017-01-25 11:49:09', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1500', '762', '2017-01-24 00:00:00', '2017-01-25 11:49:09', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1501', '763', '2017-01-23 00:00:00', '2017-01-25 11:57:44', '14', '0', '10000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1502', '763', '2017-01-23 00:00:00', '2017-01-25 11:57:44', '5', '1', '10000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1503', '764', '2017-01-04 00:00:00', '2017-01-25 12:00:38', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1504', '764', '2017-01-04 00:00:00', '2017-01-25 12:00:38', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1505', '765', '2017-01-05 00:00:00', '2017-01-25 12:02:33', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1506', '765', '2017-01-05 00:00:00', '2017-01-25 12:02:33', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1507', '766', '2017-01-06 00:00:00', '2017-01-25 12:03:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1508', '766', '2017-01-06 00:00:00', '2017-01-25 12:03:29', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1509', '767', '2017-01-07 00:00:00', '2017-01-25 12:04:55', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1510', '767', '2017-01-07 00:00:00', '2017-01-25 12:04:55', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1511', '768', '2017-01-09 00:00:00', '2017-01-25 12:17:30', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1512', '768', '2017-01-09 00:00:00', '2017-01-25 12:17:30', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1513', '769', '2017-01-10 00:00:00', '2017-01-25 12:18:59', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1514', '769', '2017-01-10 00:00:00', '2017-01-25 12:18:59', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1515', '770', '2017-01-11 00:00:00', '2017-01-25 12:20:14', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1516', '770', '2017-01-11 00:00:00', '2017-01-25 12:20:14', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1517', '771', '2017-01-12 00:00:00', '2017-01-25 12:21:37', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1518', '771', '2017-01-12 00:00:00', '2017-01-25 12:21:37', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1519', '772', '2017-01-13 00:00:00', '2017-01-25 12:22:46', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1520', '772', '2017-01-13 00:00:00', '2017-01-25 12:22:46', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1521', '773', '2017-01-14 00:00:00', '2017-01-25 12:23:58', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1522', '773', '2017-01-14 00:00:00', '2017-01-25 12:23:58', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1523', '774', '2017-01-15 00:00:00', '2017-01-25 12:25:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1524', '774', '2017-01-15 00:00:00', '2017-01-25 12:25:06', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1525', '775', '2017-01-16 00:00:00', '2017-01-25 12:34:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1526', '775', '2017-01-16 00:00:00', '2017-01-25 12:34:43', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1527', '776', '2017-01-17 00:00:00', '2017-01-25 12:36:16', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1528', '776', '2017-01-17 00:00:00', '2017-01-25 12:36:16', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1529', '777', '2017-01-18 00:00:00', '2017-01-25 12:41:03', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1530', '777', '2017-01-18 00:00:00', '2017-01-25 12:41:03', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1531', '778', '2017-01-19 00:00:00', '2017-01-25 12:43:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1532', '778', '2017-01-19 00:00:00', '2017-01-25 12:43:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1533', '779', '2017-01-20 00:00:00', '2017-01-25 12:46:04', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1534', '779', '2017-01-20 00:00:00', '2017-01-25 12:46:04', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1535', '780', '2017-01-23 00:00:00', '2017-01-25 12:49:27', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1536', '780', '2017-01-23 00:00:00', '2017-01-25 12:49:27', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1537', '781', '2017-01-21 00:00:00', '2017-01-25 12:53:06', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1538', '781', '2017-01-21 00:00:00', '2017-01-25 12:53:06', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1539', '782', '2017-01-09 00:00:00', '2017-01-25 12:58:59', '14', '0', '1500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1540', '782', '2017-01-09 00:00:00', '2017-01-25 12:58:59', '5', '1', '1500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1541', '783', '2017-01-17 00:00:00', '2017-01-25 13:00:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1542', '783', '2017-01-17 00:00:00', '2017-01-25 13:00:01', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1543', '784', '2017-01-18 00:00:00', '2017-01-25 13:01:29', '14', '0', '1300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1544', '784', '2017-01-18 00:00:00', '2017-01-25 13:01:29', '5', '1', '1300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1545', '785', '2017-01-05 00:00:00', '2017-01-25 13:10:26', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1546', '785', '2017-01-05 00:00:00', '2017-01-25 13:10:26', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1547', '786', '2017-01-06 00:00:00', '2017-01-25 13:23:16', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1548', '786', '2017-01-06 00:00:00', '2017-01-25 13:23:16', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1549', '787', '2017-01-07 00:00:00', '2017-01-25 13:26:58', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1550', '787', '2017-01-07 00:00:00', '2017-01-25 13:26:58', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1551', '788', '2017-01-08 00:00:00', '2017-01-25 13:28:27', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1552', '788', '2017-01-08 00:00:00', '2017-01-25 13:28:27', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1553', '789', '2017-01-09 00:00:00', '2017-01-25 13:31:20', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1554', '789', '2017-01-09 00:00:00', '2017-01-25 13:31:20', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1555', '790', '2017-01-11 00:00:00', '2017-01-25 13:32:20', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1556', '790', '2017-01-11 00:00:00', '2017-01-25 13:32:20', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1557', '791', '2017-01-12 00:00:00', '2017-01-25 13:33:18', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1558', '791', '2017-01-12 00:00:00', '2017-01-25 13:33:18', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1559', '792', '2017-01-13 00:00:00', '2017-01-25 13:35:26', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1560', '792', '2017-01-13 00:00:00', '2017-01-25 13:35:26', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1561', '793', '2017-01-14 00:00:00', '2017-01-25 13:36:57', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1562', '793', '2017-01-14 00:00:00', '2017-01-25 13:36:57', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1563', '794', '2017-01-15 00:00:00', '2017-01-25 13:38:32', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1564', '794', '2017-01-15 00:00:00', '2017-01-25 13:38:32', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1565', '795', '2017-01-16 00:00:00', '2017-01-25 13:56:36', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1566', '795', '2017-01-16 00:00:00', '2017-01-25 13:56:36', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1567', '796', '2017-01-17 00:00:00', '2017-01-25 13:58:07', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1568', '796', '2017-01-17 00:00:00', '2017-01-25 13:58:07', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1569', '797', '2017-01-18 00:00:00', '2017-01-25 13:59:13', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1570', '797', '2017-01-18 00:00:00', '2017-01-25 13:59:13', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1571', '798', '2017-01-19 00:00:00', '2017-01-25 13:59:54', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1572', '798', '2017-01-19 00:00:00', '2017-01-25 13:59:54', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1573', '799', '2017-01-23 00:00:00', '2017-01-25 14:00:48', '14', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1574', '799', '2017-01-23 00:00:00', '2017-01-25 14:00:48', '5', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1575', '800', '2017-01-07 00:00:00', '2017-01-25 14:05:01', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1576', '800', '2017-01-07 00:00:00', '2017-01-25 14:05:01', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1577', '801', '2017-01-08 00:00:00', '2017-01-25 14:06:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1578', '801', '2017-01-08 00:00:00', '2017-01-25 14:06:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1579', '802', '2017-01-09 00:00:00', '2017-01-25 14:08:25', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1580', '802', '2017-01-09 00:00:00', '2017-01-25 14:08:25', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1581', '803', '2017-01-11 00:00:00', '2017-01-25 14:09:40', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1582', '803', '2017-01-11 00:00:00', '2017-01-25 14:09:40', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1583', '804', '2017-01-12 00:00:00', '2017-01-25 14:17:41', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1584', '804', '2017-01-12 00:00:00', '2017-01-25 14:17:41', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1585', '805', '2017-01-14 00:00:00', '2017-01-25 14:18:35', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1586', '805', '2017-01-14 00:00:00', '2017-01-25 14:18:35', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1587', '806', '2017-01-15 00:00:00', '2017-01-25 14:19:29', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1588', '806', '2017-01-15 00:00:00', '2017-01-25 14:19:29', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1589', '807', '2017-01-19 00:00:00', '2017-01-25 14:20:24', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1590', '807', '2017-01-19 00:00:00', '2017-01-25 14:20:24', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1591', '808', '2017-01-20 00:00:00', '2017-01-25 14:21:45', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1592', '808', '2017-01-20 00:00:00', '2017-01-25 14:21:45', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1593', '809', '2017-01-21 00:00:00', '2017-01-25 14:22:43', '14', '0', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1594', '809', '2017-01-21 00:00:00', '2017-01-25 14:22:43', '5', '1', '200.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1595', '30', '2016-12-01 00:00:00', '2017-02-04 10:43:04', '20', '0', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1596', '30', '2016-12-01 00:00:00', '2017-02-04 10:43:04', '14', '1', '300.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1597', '810', '2017-02-06 00:00:00', '2017-02-05 18:20:26', '14', '0', '19500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1598', '810', '2017-02-06 00:00:00', '2017-02-05 18:20:26', '8', '1', '19500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1599', '810', '2017-02-06 00:00:00', '2017-02-05 18:20:26', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1600', '810', '2017-02-06 00:00:00', '2017-02-05 18:20:26', '3', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1601', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '14', '0', '89350.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1602', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '15', '1', '89350.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1603', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '14', '0', '150.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1604', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '1', '1', '150.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1605', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1606', '811', '2017-02-07 00:00:00', '2017-02-05 18:21:21', '3', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1607', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '14', '0', '19350.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1608', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '8', '1', '19350.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1609', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '14', '0', '150.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1610', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '1', '1', '150.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1611', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1612', '812', '2017-02-07 00:00:00', '2017-02-06 15:41:22', '3', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1613', '813', '2017-02-08 00:00:00', '2017-02-06 16:01:57', '14', '0', '29500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1614', '813', '2017-02-08 00:00:00', '2017-02-06 16:01:57', '8', '1', '29500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1615', '813', '2017-02-08 00:00:00', '2017-02-06 16:01:57', '14', '0', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1616', '813', '2017-02-08 00:00:00', '2017-02-06 16:01:57', '3', '1', '500.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1617', '814', '2017-02-07 00:00:00', '2017-02-06 16:57:27', '14', '0', '8000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1618', '814', '2017-02-07 00:00:00', '2017-02-06 16:57:27', '14', '1', '8000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1619', '814', '2017-02-07 00:00:00', '2017-02-06 16:57:27', '14', '0', '12000.00');
INSERT INTO `tb_ledgertransactions` VALUES ('1620', '814', '2017-02-07 00:00:00', '2017-02-06 16:57:27', '9', '1', '12000.00');

-- ----------------------------
-- Table structure for tb_loanofficers
-- ----------------------------
DROP TABLE IF EXISTS `tb_loanofficers`;
CREATE TABLE `tb_loanofficers` (
  `loanofficerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) DEFAULT NULL,
  `branchid` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`loanofficerid`),
  KEY `FK_branches_loanofficers` (`branchid`),
  CONSTRAINT `FK_branches_loanofficers` FOREIGN KEY (`branchid`) REFERENCES `tb_branches` (`branchid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_loanofficers
-- ----------------------------
INSERT INTO `tb_loanofficers` VALUES ('1', 'Caroline', '1', '1', '2015-09-16 13:36:43', null, '2015-09-18 05:36:43');

-- ----------------------------
-- Table structure for tb_loans
-- ----------------------------
DROP TABLE IF EXISTS `tb_loans`;
CREATE TABLE `tb_loans` (
  `loanid` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountid` bigint(20) DEFAULT NULL,
  `customerid` bigint(20) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `loanstatus` int(11) NOT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `term` int(11) NOT NULL,
  `rate` decimal(18,2) DEFAULT '0.00',
  `graceperiod` int(11) DEFAULT '0',
  `repaymentamount` decimal(18,2) DEFAULT '0.00',
  `expecteddisbursementdate` datetime DEFAULT NULL,
  `loanreason` int(11) DEFAULT NULL,
  `sourceoffund` int(11) DEFAULT NULL,
  `applicationdate` date DEFAULT NULL,
  `applicationnote` varchar(500) DEFAULT NULL,
  `applicationby` int(11) DEFAULT NULL,
  `approvedon` datetime DEFAULT NULL,
  `approvedamount` decimal(18,2) DEFAULT '0.00',
  `approvedby` int(11) DEFAULT NULL,
  `approvalnote` varchar(500) DEFAULT NULL,
  `disbursedon` datetime DEFAULT NULL,
  `disbursedby` int(11) DEFAULT NULL,
  `disbursedamount` decimal(18,2) DEFAULT '0.00',
  `disbursenote` varchar(500) DEFAULT NULL,
  `disbursementoption` int(11) DEFAULT NULL,
  `istopup` tinyint(1) DEFAULT '0',
  `topuploanid` bigint(20) DEFAULT NULL,
  `topupamount` double(18,2) DEFAULT '0.00',
  PRIMARY KEY (`loanid`),
  KEY `FK_tb_loans_tbaccounts` (`accountid`),
  KEY `FK_tb_loans_tb_customer` (`customerid`),
  KEY `FK_tb_loans_tb_products` (`productid`),
  CONSTRAINT `FK_tb_loans_tb_customer` FOREIGN KEY (`customerid`) REFERENCES `tb_customer` (`customerid`),
  CONSTRAINT `FK_tb_loans_tb_products` FOREIGN KEY (`productid`) REFERENCES `tb_products` (`productid`),
  CONSTRAINT `FK_tb_loans_tbaccounts` FOREIGN KEY (`accountid`) REFERENCES `tb_accounts` (`accountid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_loans
-- ----------------------------
INSERT INTO `tb_loans` VALUES ('1', null, '3', '4', '1', '200000.00', '12', '12.00', '0', '40666.67', '2017-01-31 00:00:00', '1', '1', '2017-01-30', 'n/a', '1', null, '0.00', null, null, null, null, '0.00', null, null, '0', '0', '0.00');
INSERT INTO `tb_loans` VALUES ('2', '60', '1', '3', '3', '20000.00', '12', '12.00', '0', '4066.67', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', '2017-02-05 00:00:00', '20000.00', '1', '', '2017-02-06 00:00:00', '1', '19500.00', 'eee', '1', '0', '0', '0.00');
INSERT INTO `tb_loans` VALUES ('3', null, '1', '3', '2', '200000.00', '10', '12.00', '0', '44000.00', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', '2017-02-05 00:00:00', '200000.00', '1', '', null, null, '0.00', null, null, '0', '0', '0.00');
INSERT INTO `tb_loans` VALUES ('4', null, '1', '4', '2', '120000.00', '9', '12.00', '0', '27733.33', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', '2017-02-05 00:00:00', '120000.00', '1', '', null, null, '0.00', null, null, '0', '0', '0.00');
INSERT INTO `tb_loans` VALUES ('5', null, '1', '4', '2', '100000.00', '9', '12.00', '0', '23111.11', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', '2017-02-05 00:00:00', '100000.00', '1', '', null, null, '0.00', null, null, '0', '0', '0.00');
INSERT INTO `tb_loans` VALUES ('6', '61', '1', '4', '3', '90000.00', '10', '12.00', '0', '19800.00', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', '2017-02-05 00:00:00', '90000.00', '1', '', '2017-02-07 00:00:00', '1', '89350.00', 'e', '4', '0', null, '0.00');
INSERT INTO `tb_loans` VALUES ('7', '59', '2', '3', '1', '40000.00', '10', '12.00', '0', '8800.00', '2017-02-06 00:00:00', '1', '1', '2017-02-05', '', '1', null, '0.00', null, null, null, null, '0.00', null, null, '0', null, '0.00');
INSERT INTO `tb_loans` VALUES ('8', '63', '5', '4', '3', '20000.00', '12', '12.00', '0', '4066.67', '2017-02-08 00:00:00', '1', '1', '2017-02-06', '', '1', '2017-02-06 00:00:00', '20000.00', '1', '', '2017-02-07 00:00:00', '1', '19350.00', 'ok', '1', '0', null, '0.00');
INSERT INTO `tb_loans` VALUES ('9', '65', '4', '3', '3', '30000.00', '2', '12.00', '0', '18600.00', '2017-02-09 00:00:00', '1', '1', '2017-02-06', '', '1', '2017-02-06 00:00:00', '30000.00', '1', '', '2017-02-08 00:00:00', '1', '29500.00', 'ok', '1', '0', null, '0.00');
INSERT INTO `tb_loans` VALUES ('10', '66', '3', '4', '1', '1000.00', '1', '12.00', '0', '1120.00', '2017-02-07 00:00:00', '1', '1', '2017-02-06', '', '1', null, '0.00', null, null, null, null, '0.00', null, null, '0', null, '0.00');

-- ----------------------------
-- Table structure for tb_loanschedule
-- ----------------------------
DROP TABLE IF EXISTS `tb_loanschedule`;
CREATE TABLE `tb_loanschedule` (
  `loanscheduleid` bigint(20) NOT NULL AUTO_INCREMENT,
  `loanid` bigint(20) NOT NULL,
  `period` int(11) NOT NULL,
  `repaymentdate` datetime NOT NULL,
  `periodlenght` int(11) NOT NULL,
  `balance` decimal(18,2) NOT NULL DEFAULT '0.00',
  `principal` decimal(18,2) NOT NULL DEFAULT '0.00',
  `interest` decimal(18,2) NOT NULL DEFAULT '0.00',
  `repayment` decimal(18,2) NOT NULL DEFAULT '0.00',
  `principalpaid` decimal(18,2) DEFAULT '0.00',
  `interestpaid` decimal(18,2) DEFAULT '0.00',
  PRIMARY KEY (`loanscheduleid`),
  KEY `FK_tb_loanschedule_tb_loans` (`loanid`),
  CONSTRAINT `FK_tb_loanschedule_tb_loans` FOREIGN KEY (`loanid`) REFERENCES `tb_loans` (`loanid`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_loanschedule
-- ----------------------------
INSERT INTO `tb_loanschedule` VALUES ('1', '1', '1', '2017-03-03 00:00:00', '31', '447333.33', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('2', '1', '2', '2017-04-03 00:00:00', '31', '406666.66', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('3', '1', '3', '2017-05-03 00:00:00', '30', '365999.99', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('4', '1', '4', '2017-06-03 00:00:00', '31', '325333.32', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('5', '1', '5', '2017-07-03 00:00:00', '30', '284666.65', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('6', '1', '6', '2017-08-03 00:00:00', '31', '243999.98', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('7', '1', '7', '2017-09-03 00:00:00', '31', '203333.31', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('8', '1', '8', '2017-10-03 00:00:00', '30', '162666.64', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('9', '1', '9', '2017-11-03 00:00:00', '31', '121999.97', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('10', '1', '10', '2017-12-03 00:00:00', '30', '81333.30', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('11', '1', '11', '2018-01-03 00:00:00', '31', '40666.63', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('12', '1', '12', '2018-02-03 00:00:00', '31', '-0.04', '16666.67', '24000.00', '40666.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('13', '2', '1', '2017-03-06 00:00:00', '28', '44733.33', '1666.67', '2400.00', '4066.67', '1666.67', '2400.00');
INSERT INTO `tb_loanschedule` VALUES ('14', '2', '2', '2017-04-06 00:00:00', '31', '40666.66', '1666.67', '2400.00', '4066.67', '1666.67', '2400.00');
INSERT INTO `tb_loanschedule` VALUES ('15', '2', '3', '2017-05-06 00:00:00', '30', '36599.99', '1666.67', '2400.00', '4066.67', '1666.67', '2400.00');
INSERT INTO `tb_loanschedule` VALUES ('16', '2', '4', '2017-06-06 00:00:00', '31', '32533.32', '1666.67', '2400.00', '4066.67', '1666.67', '2400.00');
INSERT INTO `tb_loanschedule` VALUES ('17', '2', '5', '2017-07-06 00:00:00', '30', '28466.65', '1666.67', '2400.00', '4066.67', '1333.32', '2400.00');
INSERT INTO `tb_loanschedule` VALUES ('18', '2', '6', '2017-08-06 00:00:00', '31', '24399.98', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('19', '2', '7', '2017-09-06 00:00:00', '31', '20333.31', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('20', '2', '8', '2017-10-06 00:00:00', '30', '16266.64', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('21', '2', '9', '2017-11-06 00:00:00', '31', '12199.97', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('22', '2', '10', '2017-12-06 00:00:00', '30', '8133.30', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('23', '2', '11', '2018-01-06 00:00:00', '31', '4066.63', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('24', '2', '12', '2018-02-06 00:00:00', '31', '-0.04', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('25', '3', '1', '2017-03-06 00:00:00', '28', '396000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('26', '3', '2', '2017-04-06 00:00:00', '31', '352000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('27', '3', '3', '2017-05-06 00:00:00', '30', '308000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('28', '3', '4', '2017-06-06 00:00:00', '31', '264000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('29', '3', '5', '2017-07-06 00:00:00', '30', '220000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('30', '3', '6', '2017-08-06 00:00:00', '31', '176000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('31', '3', '7', '2017-09-06 00:00:00', '31', '132000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('32', '3', '8', '2017-10-06 00:00:00', '30', '88000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('33', '3', '9', '2017-11-06 00:00:00', '31', '44000.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('34', '3', '10', '2017-12-06 00:00:00', '30', '0.00', '20000.00', '24000.00', '44000.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('35', '4', '1', '2017-03-06 00:00:00', '28', '221866.67', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('36', '4', '2', '2017-04-06 00:00:00', '31', '194133.34', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('37', '4', '3', '2017-05-06 00:00:00', '30', '166400.01', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('38', '4', '4', '2017-06-06 00:00:00', '31', '138666.68', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('39', '4', '5', '2017-07-06 00:00:00', '30', '110933.35', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('40', '4', '6', '2017-08-06 00:00:00', '31', '83200.02', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('41', '4', '7', '2017-09-06 00:00:00', '31', '55466.69', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('42', '4', '8', '2017-10-06 00:00:00', '30', '27733.36', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('43', '4', '9', '2017-11-06 00:00:00', '31', '0.03', '13333.33', '14400.00', '27733.33', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('44', '5', '1', '2017-03-06 00:00:00', '28', '184888.89', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('45', '5', '2', '2017-04-06 00:00:00', '31', '161777.78', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('46', '5', '3', '2017-05-06 00:00:00', '30', '138666.67', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('47', '5', '4', '2017-06-06 00:00:00', '31', '115555.56', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('48', '5', '5', '2017-07-06 00:00:00', '30', '92444.45', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('49', '5', '6', '2017-08-06 00:00:00', '31', '69333.34', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('50', '5', '7', '2017-09-06 00:00:00', '31', '46222.23', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('51', '5', '8', '2017-10-06 00:00:00', '30', '23111.12', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('52', '5', '9', '2017-11-06 00:00:00', '31', '0.01', '11111.11', '12000.00', '23111.11', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('53', '6', '1', '2017-03-06 00:00:00', '28', '178200.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('54', '6', '2', '2017-04-06 00:00:00', '31', '158400.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('55', '6', '3', '2017-05-06 00:00:00', '30', '138600.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('56', '6', '4', '2017-06-06 00:00:00', '31', '118800.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('57', '6', '5', '2017-07-06 00:00:00', '30', '99000.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('58', '6', '6', '2017-08-06 00:00:00', '31', '79200.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('59', '6', '7', '2017-09-06 00:00:00', '31', '59400.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('60', '6', '8', '2017-10-06 00:00:00', '30', '39600.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('61', '6', '9', '2017-11-06 00:00:00', '31', '19800.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('62', '6', '10', '2017-12-06 00:00:00', '30', '0.00', '9000.00', '10800.00', '19800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('63', '7', '1', '2017-03-06 00:00:00', '28', '79200.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('64', '7', '2', '2017-04-06 00:00:00', '31', '70400.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('65', '7', '3', '2017-05-06 00:00:00', '30', '61600.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('66', '7', '4', '2017-06-06 00:00:00', '31', '52800.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('67', '7', '5', '2017-07-06 00:00:00', '30', '44000.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('68', '7', '6', '2017-08-06 00:00:00', '31', '35200.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('69', '7', '7', '2017-09-06 00:00:00', '31', '26400.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('70', '7', '8', '2017-10-06 00:00:00', '30', '17600.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('71', '7', '9', '2017-11-06 00:00:00', '31', '8800.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('72', '7', '10', '2017-12-06 00:00:00', '30', '0.00', '4000.00', '4800.00', '8800.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('73', '8', '1', '2017-03-08 00:00:00', '28', '44733.33', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('74', '8', '2', '2017-04-08 00:00:00', '31', '40666.66', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('75', '8', '3', '2017-05-08 00:00:00', '30', '36599.99', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('76', '8', '4', '2017-06-08 00:00:00', '31', '32533.32', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('77', '8', '5', '2017-07-08 00:00:00', '30', '28466.65', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('78', '8', '6', '2017-08-08 00:00:00', '31', '24399.98', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('79', '8', '7', '2017-09-08 00:00:00', '31', '20333.31', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('80', '8', '8', '2017-10-08 00:00:00', '30', '16266.64', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('81', '8', '9', '2017-11-08 00:00:00', '31', '12199.97', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('82', '8', '10', '2017-12-08 00:00:00', '30', '8133.30', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('83', '8', '11', '2018-01-08 00:00:00', '31', '4066.63', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('84', '8', '12', '2018-02-08 00:00:00', '31', '-0.04', '1666.67', '2400.00', '4066.67', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('85', '9', '1', '2017-03-09 00:00:00', '28', '18600.00', '15000.00', '3600.00', '18600.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('86', '9', '2', '2017-04-09 00:00:00', '31', '0.00', '15000.00', '3600.00', '18600.00', '0.00', '0.00');
INSERT INTO `tb_loanschedule` VALUES ('87', '10', '1', '2017-03-07 00:00:00', '28', '0.00', '1000.00', '120.00', '1120.00', '0.00', '0.00');

-- ----------------------------
-- Table structure for tb_menuitems
-- ----------------------------
DROP TABLE IF EXISTS `tb_menuitems`;
CREATE TABLE `tb_menuitems` (
  `itemid` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) NOT NULL,
  `itemname` varchar(50) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `orderby` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`itemid`),
  KEY `fk_tb_menuitems_tb_menus` (`menuid`),
  CONSTRAINT `fk_tb_menuitems_tb_menus` FOREIGN KEY (`menuid`) REFERENCES `tb_menus` (`menuid`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_menuitems
-- ----------------------------
INSERT INTO `tb_menuitems` VALUES ('1', '1', 'Customer Details', 'customerdetails', '1', 'icon-customer');
INSERT INTO `tb_menuitems` VALUES ('2', '2', 'View Accounts', 'savingsaccounts', '1', 'icon-edit');
INSERT INTO `tb_menuitems` VALUES ('3', '3', 'Loan Application', 'loanapplication', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('4', '1', 'Change Loan Officer Portfolio', 'changeloanofficer', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('5', '1', 'Customer Enquiry', 'customerregistration', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('6', '3', 'Post Customer Transactions', 'customertransactions', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('7', '3', 'Reverse Transaction', 'reversetransactions', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('8', '3', 'Loan Disbursement', 'loandisbursement', '5', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('9', '3', 'Cancle Loan Issue', 'customerregistration', '6', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('10', '2', 'Fixed Deposits', 'fixeddeposits', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('11', '3', 'Loan Approval', 'loanapproval', '4', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('12', '4', 'Maintain Ledger Account', 'ledgeraccounts', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('13', '4', 'Post Ledger Transactions', 'ledgertransaction', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('14', '5', 'Company Details', 'companyinformation', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('15', '5', 'Loan Products Settings', 'loanproduct', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('16', '5', 'Savings Products Settings', 'savingsproduct', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('17', '5', 'Miscellaneous Settings', 'miscellaneouslists', '5', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('18', '5', 'Currency', 'currency', '6', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('19', '5', 'Maintain Financial Year', 'periodyear', '10', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('20', '6', 'EOY', 'eoy', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('21', '7', 'Customer Register', 'rcustomerregister', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('22', '7', 'Account Balance Report', 'raccountbalance', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('23', '7', 'Loan Portfolio Report', 'rloanportfolio', '4', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('24', '7', 'PAR Analysis Report', 'rparanalysis', '5', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('25', '7', 'Expected Repayments', 'rexpectedrepayments', '6', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('26', '7', 'Loan Repayment Schedule', 'rloanrepaymentschedule', '7', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('27', '7', 'Ledger Account Statement', 'rledgeraccountstatement', '8', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('28', '7', 'Trial Balance', 'rtrialbalance', '9', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('29', '7', 'Balance Sheet', 'rbalancesheet', '10', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('30', '7', 'Income Statement', 'rincomestatement', '11', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('31', '5', 'Users', 'users', '7', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('32', '5', 'User Profile', 'userprofile', '8', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('33', '5', 'Maintain Loan Officers', 'loanofficers', '9', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('34', '5', 'Maintain Fee', 'fee', '4', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('35', '7', 'Account Statement Report', 'raccountstatement', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('36', '4', 'View Ledger Transactions', 'viewledgertransactions', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('37', '5', 'Maintain Financial Periods', 'period', '11', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('38', '6', 'Process Financial Period', 'processfinancialperiod', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('39', '3', 'Maintain Guarantors', 'guarantors', '7', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('40', '3', 'Internal Transfer', 'internaltransfer', '8', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('41', '8', 'Password Reset', 'pwdreset', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('42', '7', 'Disbursed Loans Report', 'rloandisbursement', '12', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('43', '3', 'Import M-Pesa Repayments', 'uploadrepayment', '9', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('44', '7', 'M-Pesa Repayments Report', 'rmpesarepayments', '13', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('45', '9', 'Manage Campaigns', 'smscampaigns', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('46', '9', 'Direct Message', 'smsdirectmessage', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('47', '9', 'SMS Templates', 'smstemplates', '3', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('48', '9', 'Business Rules', 'smsrules', '4', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('49', '9', 'Manage Contacts', 'smscontacts', '5', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('50', '9', 'Tags', 'smstags', '6', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('51', '9', 'Schedules', 'smsschedules', '7', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('52', '9', 'Process SMS', 'processsms', '9', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('53', '9', 'SMS Report', 'smsreport', '10', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('54', '10', 'USSD Subscription', 'ussdsubscription', '1', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('55', '10', 'USSD Subscription Report', 'ussdsubscriptionreport', '2', 'icon-add');
INSERT INTO `tb_menuitems` VALUES ('56', '10', 'USSD Simulator', 'ussdsimulator', '3', 'icon-add');

-- ----------------------------
-- Table structure for tb_menus
-- ----------------------------
DROP TABLE IF EXISTS `tb_menus`;
CREATE TABLE `tb_menus` (
  `menuid` int(11) NOT NULL AUTO_INCREMENT,
  `menuname` varchar(50) NOT NULL,
  `icons` varchar(90) DEFAULT NULL,
  `orderby` int(11) NOT NULL,
  PRIMARY KEY (`menuid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_menus
-- ----------------------------
INSERT INTO `tb_menus` VALUES ('1', 'Customer', 'icon-customer', '1');
INSERT INTO `tb_menus` VALUES ('2', 'Vehicle Accounts', 'icon-accounts', '2');
INSERT INTO `tb_menus` VALUES ('3', 'Transactions', 'icon-sum', '3');
INSERT INTO `tb_menus` VALUES ('4', 'General Ledger', 'icon-accounting', '4');
INSERT INTO `tb_menus` VALUES ('5', 'Settings', 'icon-settings', '5');
INSERT INTO `tb_menus` VALUES ('6', 'Back Office', 'icon-backoffice', '6');
INSERT INTO `tb_menus` VALUES ('7', 'Reports', 'icon-report', '7');
INSERT INTO `tb_menus` VALUES ('8', 'User', 'icon-settings', '8');
INSERT INTO `tb_menus` VALUES ('9', 'SMS', 'icon-settings', '9');
INSERT INTO `tb_menus` VALUES ('10', 'USSD', 'icon-accounts', '10');

-- ----------------------------
-- Table structure for tb_miscellaneouslists
-- ----------------------------
DROP TABLE IF EXISTS `tb_miscellaneouslists`;
CREATE TABLE `tb_miscellaneouslists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) DEFAULT NULL,
  `valueMember` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `defaultitem` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ParentID` (`parentid`),
  CONSTRAINT `tb_miscellaneouslists_ibfk_1` FOREIGN KEY (`parentid`) REFERENCES `tb_definations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_miscellaneouslists
-- ----------------------------
INSERT INTO `tb_miscellaneouslists` VALUES ('1', '1', '1', 'GL Control Account', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('2', '1', '2', 'Fee Income Account', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('3', '1', '3', 'Fee Accrual Account', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('4', '1', '4', 'Savings Interest Account', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('8', '3', '1', 'Kenya', 'KE', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('9', '1', '5', 'Suspense Account', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('11', '2', '1', 'Male', 'Male', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('12', '2', '2', 'Female', 'Female', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('13', '3', '2', 'Tanzania', 'TZ', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('14', '2', '3', 'Unknown', 'Unknown', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('15', '3', '3', 'Uganda', 'UG', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('16', '4', '1', 'Cash', 'Cash', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('18', '4', '2', 'Bank', 'Bank', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('19', '5', '1', 'MR', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('20', '5', '2', 'MRS', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('21', '6', '1', 'Fixed Amount', 'FA', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('22', '6', '2', 'Variable Amount', 'VA', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('23', '6', '3', 'Percentage', 'P', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('24', '7', '1', 'Person', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('25', '7', '2', 'Organisation', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('26', '7', '3', 'Group', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('27', '5', '3', 'MISS', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('28', '9', '1', 'Home', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('29', '9', '2', 'Work', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('30', '9', '3', 'Business', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('31', '8', '1', 'Applied', 'AP', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('32', '8', '2', 'Approved', 'Ap', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('33', '8', '3', 'Issued', 'IS', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('34', '8', '4', 'Cancelled', 'CA', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('35', '1', '6', 'Savings Interest Tax Payable', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('36', '5', '4', 'DR', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('37', '3', '4', 'Ethiopia', 'ET', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('38', '10', '1', 'National ID', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('39', '10', '2', 'Passport', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('40', '14', '1', 'Mombasa', '1', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('41', '14', '2', 'Kwale', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('42', '14', '3', 'Kilifi', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('43', '14', '4', 'Tana River', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('44', '14', '5', 'Lamu', '5', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('45', '14', '6', 'Taita-Taveta', '6', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('46', '14', '7', 'Garissa', '7', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('47', '14', '8', 'Wajir', '8', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('48', '14', '9', 'Mandera', '9', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('49', '14', '10', 'Marsabit', '10', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('50', '14', '11', 'Isiolo', '11', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('51', '14', '12', 'Meru', '12', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('52', '14', '13', 'Tharaka-Nithi', '13', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('53', '14', '14', 'Embu', '14', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('54', '14', '15', 'Kitui', '15', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('55', '14', '16', 'Machakos', '16', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('56', '14', '17', 'Makueni', '17', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('57', '14', '18', 'Nyandarua', '18', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('58', '14', '19', 'Nyeri', '19', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('59', '14', '20', 'Kirinyaga', '20', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('60', '14', '21', 'Murang\'a', '21', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('61', '14', '22', 'Kiambu', '22', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('62', '14', '23', 'Turkana', '23', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('63', '14', '24', 'West Pokot', '24', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('64', '14', '25', 'Samburu', '25', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('65', '14', '26', 'Trans Nzoia', '26', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('66', '14', '27', 'Uasin Gishu', '27', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('67', '14', '28', 'Elgeyo-Marakwet', '28', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('68', '14', '29', 'Nandi', '29', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('69', '14', '30', 'Baringo', '30', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('70', '14', '31', 'Laikipia', '31', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('71', '14', '32', 'Nakuru', '32', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('72', '14', '33', 'Narok', '33', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('73', '14', '34', 'Kajiado', '34', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('74', '14', '35', 'Kericho', '35', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('75', '14', '36', 'Bomet', '36', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('76', '14', '37', 'Kakamega', '37', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('77', '14', '38', 'Vihiga', '38', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('78', '14', '39', 'Bungoma', '39', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('79', '14', '40', 'Busia', '40', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('80', '14', '41', 'Siaya', '41', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('81', '14', '42', 'Kisumu', '42', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('82', '14', '43', 'Homa Bay', '43', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('83', '14', '44', 'Migori', '44', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('84', '14', '45', 'Kisii', '45', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('85', '14', '46', 'Nyamira', '46', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('86', '14', '47', 'Nairobi', '47', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('87', '12', '1', 'Single', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('88', '12', '2', 'Married', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('89', '12', '3', 'Divorced', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('90', '15', '1', 'Annual', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('91', '15', '2', 'Monthly', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('92', '15', '3', 'Once', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('93', '16', '1', 'Active', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('94', '16', '2', 'Dormant', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('95', '16', '3', 'Blocked', '1', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('96', '16', '7', 'Closed', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('97', '17', '1', 'Ruiru', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('98', '1', '8', 'USER/Terminal Contra', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('99', '18', '1', 'Deposit', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('100', '18', '2', 'Withdraw', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('101', '18', '3', 'Fee Payment', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('102', '18', '4', 'Loan Repayment', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('103', '19', '1', 'Principal', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('104', '19', '2', 'Interest', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('105', '19', '3', 'Principal and Interest', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('106', '20', '1', 'Declining Balance With Equal Installments', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('107', '20', '2', 'Declining Balance With Recalculate Interest', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('108', '20', '3', 'Flat Interest Rate', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('109', '21', '1', 'Actual / 365', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('110', '21', '2', 'Actual / 360', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('111', '22', '1', 'Bank Cheque', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('112', '22', '2', 'Savings Account', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('113', '22', '3', 'Cash', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('114', '22', '4', 'MPESA', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('115', '1', '9', 'Loan Interest Income', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('116', '1', '10', 'Loan Interest Receivable', 'Ledgerlinks', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('117', '23', '1', 'Vehicle Financing', '1', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('118', '24', '1', 'Business', '1', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('119', '18', '5', 'Loan Disbursement', '5', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('120', '18', '6', 'Loan Repayment', '6', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('121', '18', '7', 'Charged', '7', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('122', '18', '8', 'Interest Paid', '8', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('123', '8', '5', 'Closed', 'CL', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('124', '18', '9', 'Share Transfer', '9', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('125', '18', '10', 'Deposit Transfer', '10', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('126', '18', '11', 'Loan Repayment Transfer', '11', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('127', '18', '12', 'Other Transfer', '12', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('128', '20', '4', 'Flat Interest Amount', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('129', '25', '1', 'Monthly', 'M', '1', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('130', '25', '2', 'Weekly', 'W', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('131', '17', '2', 'Mombasa', '2', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('132', '17', '3', 'Mtwapa', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('133', '17', '4', 'Kisauni', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('134', '17', '5', 'Changamwe', '5', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('135', '17', '6', 'Likoni', '2', '6', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('136', '3', '5', 'USA', 'C04', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('137', '2', '4', 'Select Gender', '', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('138', '15', '4', 'Daily', '4', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('139', '25', '3', 'Annual', '3', '0', '1');
INSERT INTO `tb_miscellaneouslists` VALUES ('140', '23', '2', 'Personal Use', '2', '0', '1');

-- ----------------------------
-- Table structure for tb_mpesarepayments
-- ----------------------------
DROP TABLE IF EXISTS `tb_mpesarepayments`;
CREATE TABLE `tb_mpesarepayments` (
  `rowid` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `orig` varchar(50) DEFAULT NULL,
  `dest` decimal(12,0) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  `customer_id` varchar(100) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `routemethod_id` int(11) DEFAULT NULL,
  `routemethod_name` varchar(20) DEFAULT NULL,
  `mpesa_code` varchar(30) DEFAULT NULL,
  `mpesa_acc` varchar(30) DEFAULT NULL,
  `mpesa_msisdn` varchar(30) DEFAULT NULL,
  `mpesa_trx_date` datetime DEFAULT NULL,
  `mpesa_trx_time` varchar(10) DEFAULT NULL,
  `mpesa_amt` double(18,2) DEFAULT NULL,
  `mpesa_sender` varchar(50) DEFAULT NULL,
  `mpesa_trx_status` varchar(50) DEFAULT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed` tinyint(1) DEFAULT '0',
  `receiptno` bigint(20) DEFAULT NULL,
  `filename` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  UNIQUE KEY `mpesa_code` (`mpesa_code`)
) ENGINE=MyISAM AUTO_INCREMENT=280 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_mpesarepayments
-- ----------------------------
INSERT INTO `tb_mpesarepayments` VALUES ('1', null, null, null, null, 'Pay Bill from 254712550641 - SARAH BITOK Acc. 20462716', null, null, null, null, 'UPLOAD', 'KAE5UEAU9R', '20462716', '254712550641 ', '2016-01-14 16:29:50', null, '500.00', ' SARAH BITOK', 'Completed', '2016-01-15 02:13:01', '1', '202', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('2', null, null, null, null, 'Pay Bill from 254702370172 - RACHAEL MATANDA Acc. 30328890', null, null, null, null, 'UPLOAD', 'KAE4UE1E6E', '30328890', '254702370172 ', '2016-01-14 15:58:42', null, '500.00', ' RACHAEL MATANDA', 'Completed', '2016-01-15 02:13:01', '1', '944', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('3', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KAE6UDZ72O', '23321763', '254728143256 ', '2016-01-14 15:50:45', null, '400.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('4', null, null, null, null, 'Pay Bill from 254726150317 - KEMEI JEPOTIP Acc. 21419429', null, null, null, null, 'UPLOAD', 'KAE1UDYAYF', '21419429', '254726150317 ', '2016-01-14 15:48:01', null, '1750.00', ' KEMEI JEPOTIP', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('5', null, null, null, null, 'Pay Bill from 254728523241 - ALMAH CHELIMO Acc. 23607890', null, null, null, null, 'UPLOAD', 'KAE3UDEPWT', '23607890', '254728523241 ', '2016-01-14 14:39:43', null, '2500.00', ' ALMAH CHELIMO', 'Completed', '2016-01-15 02:13:01', '1', '201', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('6', null, null, null, null, 'Pay Bill from 254727587869 - NICODEMUS KIBET Acc. 28610939', null, null, null, null, 'UPLOAD', 'KAE5UDDV7L', '28610939', '254727587869 ', '2016-01-14 14:36:41', null, '5000.00', ' NICODEMUS KIBET', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('7', null, null, null, null, 'Pay Bill from 254706867060 - JALOT NEKESA Acc. 29728448', null, null, null, null, 'UPLOAD', 'KAE7UD8DRL', '29728448', '254706867060 ', '2016-01-14 14:17:24', null, '1750.00', ' JALOT NEKESA', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('8', null, null, null, null, 'Pay Bill from 254727081071 - CAROLINE NDUGU Acc. 14660114', null, null, null, null, 'UPLOAD', 'KAE6UCJWP8', '14660114', '254727081071 ', '2016-01-14 12:51:23', null, '500.00', ' CAROLINE NDUGU', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('9', null, null, null, null, 'Pay Bill from 254713394486 - DORCAS BORBOREI Acc. 8713242', null, null, null, null, 'UPLOAD', 'KAE5UC9O7D', '8713242', '254713394486 ', '2016-01-14 12:17:00', null, '3000.00', ' DORCAS BORBOREI', 'Completed', '2016-01-15 02:13:01', '1', '943', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('10', null, null, null, null, 'Pay Bill from 254729961262 - RUTHY KHAVERE Acc. 23378247', null, null, null, null, 'UPLOAD', 'KAE8UBW8ZC', '23378247', '254729961262 ', '2016-01-14 11:31:45', null, '50.00', ' RUTHY KHAVERE', 'Completed', '2016-01-15 02:13:01', '1', '942', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('11', null, null, null, null, 'Pay Bill from 254727740616 - SHEILAH TOO Acc. 21826441', null, null, null, null, 'UPLOAD', 'KAE1UBHZMB', '21826441', '254727740616 ', '2016-01-14 10:43:13', null, '2500.00', ' SHEILAH TOO', 'Completed', '2016-01-15 02:13:01', '1', '941', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('12', null, null, null, null, 'Pay Bill from 254718362710 - LOICE AGALA Acc. 20103536', null, null, null, null, 'UPLOAD', 'KAE5UBD7JZ', '20103536', '254718362710 ', '2016-01-14 10:26:47', null, '1750.00', ' LOICE AGALA', 'Completed', '2016-01-15 02:13:01', '1', '200', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('13', null, null, null, null, 'Pay Bill from 254726061605 - MARY WANJERU Acc. 11829749', null, null, null, null, 'UPLOAD', 'KAE5UAYZ01', '11829749', '254726061605 ', '2016-01-14 09:37:51', null, '1400.00', ' MARY WANJERU', 'Completed', '2016-01-15 02:13:01', '1', '199', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('14', null, null, null, null, 'Pay Bill from 254729961262 - RUTHY KHAVERE Acc. 23378247', null, null, null, null, 'UPLOAD', 'KAE0UAR1PW', '23378247', '254729961262 ', '2016-01-14 09:09:33', null, '800.00', ' RUTHY KHAVERE', 'Completed', '2016-01-15 02:13:01', '1', '940', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('15', null, null, null, null, 'Pay Bill from 254722977517 - LINET MINANGE KAMADI Acc. 11790849', null, null, null, null, 'UPLOAD', 'KAE2UAQVU0', '11790849', '254722977517 ', '2016-01-14 09:08:58', null, '1000.00', ' LINET MINANGE KAMADI', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('16', null, null, null, null, 'Pay Bill from 254726233236 - VIVIAN KEMBOI Acc. 29361813', null, null, null, null, 'UPLOAD', 'KAE2UAP1V0', '29361813', '254726233236 ', '2016-01-14 09:02:02', null, '2500.00', ' VIVIAN KEMBOI', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('17', null, null, null, null, 'Pay Bill from 254704415285 - SUSAN HIKAH Acc. 21151947', null, null, null, null, 'UPLOAD', 'KAE2UAE3U0', '21151947', '254704415285 ', '2016-01-14 08:18:57', null, '250.00', ' SUSAN HIKAH', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('18', null, null, null, null, 'Pay Bill from 254716589064 - LONAH CHARLES Acc. 27942097', null, null, null, null, 'UPLOAD', 'KAE4UACW5A', '27942097', '254716589064 ', '2016-01-14 08:13:33', null, '1750.00', ' LONAH CHARLES', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('19', null, null, null, null, 'Pay Bill from 254720533146 - STEPHEN KUTO Acc. 20692367', null, null, null, null, 'UPLOAD', 'KAE1U9VEQV', '20692367', '254720533146 ', '2016-01-14 04:20:19', null, '1700.00', ' STEPHEN KUTO', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('20', null, null, null, null, 'Pay Bill from 254710198004 - LILIAN NJERI KAMAU Acc. 1229511', null, null, null, null, 'UPLOAD', 'KAD9U953FH', '1229511', '254710198004 ', '2016-01-13 21:13:53', null, '1750.00', ' LILIAN NJERI KAMAU', 'Completed', '2016-01-15 02:13:01', '1', '939', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('21', null, null, null, null, 'Pay Bill from 254725370049 - FLOMENAH TOO Acc. 20846361', null, null, null, null, 'UPLOAD', 'KAD2U8I3YE', '20846361', '254725370049 ', '2016-01-13 20:05:47', null, '2000.00', ' FLOMENAH TOO', 'Completed', '2016-01-15 02:13:01', '1', '938', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('22', null, null, null, null, 'Pay Bill from 254728809266 - WILSON MUIGAI Acc. 22994508', null, null, null, null, 'UPLOAD', 'KAD5U8FMJB', '22994508', '254728809266 ', '2016-01-13 19:59:31', null, '2600.00', ' WILSON MUIGAI', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('23', null, null, null, null, 'Pay Bill from 254728926818 - SARAH NJERI MBURU Acc. 21574199', null, null, null, null, 'UPLOAD', 'KAD2U8DREA', '21574199', '254728926818 ', '2016-01-13 19:54:54', null, '500.00', ' SARAH NJERI MBURU', 'Completed', '2016-01-15 02:13:01', '1', '937', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('24', null, null, null, null, 'Pay Bill from 254724798203 - LUCY APONDI WESONGA Acc. 21332399', null, null, null, null, 'UPLOAD', 'KAD0U8BYAW', '21332399', '254724798203 ', '2016-01-13 19:50:24', null, '170.00', ' LUCY APONDI WESONGA', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('25', null, null, null, null, 'Pay Bill from 254725661377 - SELLY JESANG BIRGEN Acc. 8731598', null, null, null, null, 'UPLOAD', 'KAD3U81OBJ', '8731598', '254725661377 ', '2016-01-13 19:26:14', null, '5500.00', ' SELLY JESANG BIRGEN', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('26', null, null, null, null, 'Pay Bill from 254712550641 - SARAH BITOK Acc. 20462716', null, null, null, null, 'UPLOAD', 'KAD2U7AQO0', '20462716', '254712550641 ', '2016-01-13 18:20:08', null, '1250.00', ' SARAH BITOK', 'Completed', '2016-01-15 02:13:01', '1', '198', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('27', null, null, null, null, 'Pay Bill from 254722217742 - ANNE KIMANI Acc. 21361313', null, null, null, null, 'UPLOAD', 'KAD3U6R7UN', '21361313', '254722217742 ', '2016-01-13 17:28:18', null, '1750.00', ' ANNE KIMANI', 'Completed', '2016-01-15 02:13:01', '1', '936', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('28', null, null, null, null, 'Pay Bill from 254712450221 - LUCY PATRICK Acc.   0925162', null, null, null, null, 'UPLOAD', 'KAD5U6QXWD', '0925162', '254712450221 ', '2016-01-13 17:27:39', null, '1500.00', ' LUCY PATRICK', 'Completed', '2016-01-15 02:13:01', '1', '935', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('29', null, null, null, null, 'Pay Bill from 254703937300 - AGNES GITAU Acc. 22665200', null, null, null, null, 'UPLOAD', 'KAD1U5ML0D', '22665200', '254703937300 ', '2016-01-13 15:24:31', null, '2500.00', ' AGNES GITAU', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('30', null, null, null, null, 'Pay Bill from 254721290546 - PENINA NJERI MUGA Acc. 1332341', null, null, null, null, 'UPLOAD', 'KAD9U5BGKR', '1332341', '254721290546 ', '2016-01-13 14:47:16', null, '1750.00', ' PENINA NJERI MUGA', 'Completed', '2016-01-15 02:13:01', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('31', null, null, null, null, 'Pay Bill from 254723848989 - MARGARET NDIRANGU Acc. 1232001', null, null, null, null, 'UPLOAD', 'KAD0U59OYS', '1232001', '254723848989 ', '2016-01-13 14:41:05', null, '2500.00', ' MARGARET NDIRANGU', 'Completed', '2016-01-15 02:13:01', '1', '934', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('32', null, null, null, null, 'Pay Bill from 254721262911 - ANNE MWANGI Acc. 22478980', null, null, null, null, 'UPLOAD', 'KAD6U59JPI', '22478980', '254721262911 ', '2016-01-13 14:40:56', null, '2000.00', ' ANNE MWANGI', 'Completed', '2016-01-15 02:13:02', '1', '197', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('33', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KAD6U4YLSO', '23321763', '254728143256 ', '2016-01-13 14:03:26', null, '400.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('34', null, null, null, null, 'Pay Bill from 254721665675 - MARY NGOME Acc. 0721665675', null, null, null, null, 'UPLOAD', 'KAD0U4UYZC', '0721665675', '254721665675 ', '2016-01-13 13:51:04', null, '2000.00', ' MARY NGOME', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('35', null, null, null, null, 'Pay Bill from 254719633660 - RAEL KERICH Acc. 25026556', null, null, null, null, 'UPLOAD', 'KAD0U4GIYS', '25026556', '254719633660 ', '2016-01-13 13:02:39', null, '500.00', ' RAEL KERICH', 'Completed', '2016-01-15 02:13:02', '1', '933', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('36', null, null, null, null, 'Pay Bill from 254724752217 - ANN MWANGI Acc. 22255424', null, null, null, null, 'UPLOAD', 'KAD7U4GGC9', '22255424', '254724752217 ', '2016-01-13 13:02:23', null, '500.00', ' ANN MWANGI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('37', null, null, null, null, 'Pay Bill from 254715769406 - ALICE OCHIENG Acc. 31356589', null, null, null, null, 'UPLOAD', 'KAD7U4GCP5', '31356589', '254715769406 ', '2016-01-13 13:02:08', null, '500.00', ' ALICE OCHIENG', 'Completed', '2016-01-15 02:13:02', '1', '932', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('38', null, null, null, null, 'Pay Bill from 254723733036 - THOMSON AGARI MOKAYA Acc. 24157909', null, null, null, null, 'UPLOAD', 'KAD2U4C7CM', '24157909', '254723733036 ', '2016-01-13 12:48:04', null, '4500.00', ' THOMSON AGARI MOKAYA', 'Completed', '2016-01-15 02:13:02', '1', '196', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('39', null, null, null, null, 'Pay Bill from 254729878510 - JULIA CHEPLETING Acc. 23153896', null, null, null, null, 'UPLOAD', 'KAD6U4B3D0', '23153896', '254729878510 ', '2016-01-13 12:44:10', null, '500.00', ' JULIA CHEPLETING', 'Completed', '2016-01-15 02:13:02', '1', '931', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('40', null, null, null, null, 'Pay Bill from 254720409155 - PURITY CHEPNGETICH LIMO Acc. 13808004', null, null, null, null, 'UPLOAD', 'KAD8U47NCM', '13808004', '254720409155 ', '2016-01-13 12:33:00', null, '1750.00', ' PURITY CHEPNGETICH LIMO', 'Completed', '2016-01-15 02:13:02', '1', '195', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('41', null, null, null, null, 'Pay Bill from 254726691113 - NELLY MOI Acc. 22548603', null, null, null, null, 'UPLOAD', 'KAD7U45E3V', '22548603', '254726691113 ', '2016-01-13 12:25:27', null, '500.00', ' NELLY MOI', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('42', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KAD7U41K1L', '', '454700 ', '2016-01-13 12:12:26', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('43', null, null, null, null, 'Pay Bill from 254722245389 - EDDAH NAFULAH Acc. 7618763', null, null, null, null, 'UPLOAD', 'KAD4U40OVA', '7618763', '254722245389 ', '2016-01-13 12:10:03', null, '500.00', ' EDDAH NAFULAH', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('44', null, null, null, null, 'Pay Bill from 254702816237 - joseph kioi Acc. 22641363', null, null, null, null, 'UPLOAD', 'KAD1U3QGLP', '22641363', '254702816237 ', '2016-01-13 11:35:58', null, '550.00', ' joseph kioi', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('45', null, null, null, null, 'Pay Bill from 254725268971 - DAVID KARIUKI Acc. 7664769', null, null, null, null, 'UPLOAD', 'KAD6U3JAT0', '7664769', '254725268971 ', '2016-01-13 11:12:28', null, '3000.00', ' DAVID KARIUKI', 'Completed', '2016-01-15 02:13:02', '1', '194', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('46', null, null, null, null, 'Pay Bill from 254728257576 - NELLY CHESOLI Acc. 21132612', null, null, null, null, 'UPLOAD', 'KAD9U3DC83', '21132612', '254728257576 ', '2016-01-13 10:52:42', null, '2000.00', ' NELLY CHESOLI', 'Completed', '2016-01-15 02:13:02', '1', '930', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('47', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KAD6U38GOK', '23321763', '254728143256 ', '2016-01-13 10:36:57', null, '300.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('48', null, null, null, null, 'Pay Bill from 254723245770 - BARNABA KIPTEIMET Acc. 25680099', null, null, null, null, 'UPLOAD', 'KAD6U363EA', '25680099', '254723245770 ', '2016-01-13 10:29:10', null, '3000.00', ' BARNABA KIPTEIMET', 'Completed', '2016-01-15 02:13:02', '1', '193', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('49', null, null, null, null, 'Pay Bill from 254712163702 - MERCY MUREY Acc. 0712163702', null, null, null, null, 'UPLOAD', 'KAD5U357NH', '0712163702', '254712163702 ', '2016-01-13 10:26:28', null, '450.00', ' MERCY MUREY', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('50', null, null, null, null, 'Pay Bill from 254726847766 - FRANCIS YEGO Acc. 11339416', null, null, null, null, 'UPLOAD', 'KAD7U2W3AZ', '11339416', '254726847766 ', '2016-01-13 09:56:54', null, '1000.00', ' FRANCIS YEGO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('51', null, null, null, null, 'Pay Bill from 254712703243 - EVERLYNE ODUOR Acc. 25840720', null, null, null, null, 'UPLOAD', 'KAD8U2UF7O', '25840720', '254712703243 ', '2016-01-13 09:50:59', null, '1750.00', ' EVERLYNE ODUOR', 'Completed', '2016-01-15 02:13:02', '1', '192', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('52', null, null, null, null, 'Pay Bill from 254725565008 - CAROLINE ESOI Acc. 32275854', null, null, null, null, 'UPLOAD', 'KAD5U2PPQR', '32275854', '254725565008 ', '2016-01-13 09:35:28', null, '500.00', ' CAROLINE ESOI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('53', null, null, null, null, 'Pay Bill from 254721868779 - SARAH KARIUKI Acc. 21240569', null, null, null, null, 'UPLOAD', 'KAD5U2BMU5', '21240569', '254721868779 ', '2016-01-13 08:45:35', null, '1750.00', ' SARAH KARIUKI', 'Completed', '2016-01-15 02:13:02', '1', '191', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('54', null, null, null, null, 'Pay Bill from 254722988688 - ANJELAH KIBET Acc. 24098281', null, null, null, null, 'UPLOAD', 'KAD0U29Z18', '24098281', '254722988688 ', '2016-01-13 08:39:01', null, '500.00', ' ANJELAH KIBET', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('55', null, null, null, null, 'Pay Bill from 254724966696 - WILSON KIPLAGAT Acc. 23218583', null, null, null, null, 'UPLOAD', 'KAD4U1V87W', '23218583', '254724966696 ', '2016-01-13 07:28:12', null, '1500.00', ' WILSON KIPLAGAT', 'Completed', '2016-01-15 02:13:02', '1', '190', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('56', null, null, null, null, 'Pay Bill from 254721223174 - MARGARET JELAGAT KOECH Acc. 8717025', null, null, null, null, 'UPLOAD', 'KAD6U1SSBI', '8717025', '254721223174 ', '2016-01-13 07:11:06', null, '2000.00', ' MARGARET JELAGAT KOECH', 'Completed', '2016-01-15 02:13:02', '1', '189', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('57', null, null, null, null, 'Pay Bill from 254728467892 - MARY WANJIKU Acc. 22956147', null, null, null, null, 'UPLOAD', 'KAC0U1AKLS', '22956147', '254728467892 ', '2016-01-12 22:16:26', null, '3500.00', ' MARY WANJIKU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('58', null, null, null, null, 'Pay Bill from 254711672583 - SOLOMON MAIYO Acc. 28339093', null, null, null, null, 'UPLOAD', 'KAC1U0LKYB', '28339093', '254711672583 ', '2016-01-12 20:38:39', null, '1500.00', ' SOLOMON MAIYO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('59', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KAC5U0KFFZ', '23321763', '254728143256 ', '2016-01-12 20:35:03', null, '300.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('60', null, null, null, null, 'Pay Bill from 254704474347 - JANE ODERO Acc. ,12646520', null, null, null, null, 'UPLOAD', 'KAC4U0AYS2', ',12646520', '254704474347 ', '2016-01-12 20:08:43', null, '2000.00', ' JANE ODERO', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('61', null, null, null, null, 'Pay Bill from 254728174233 - JOY ALUMASA Acc. 27661460', null, null, null, null, 'UPLOAD', 'KAC0U03BQA', '27661460', '254728174233 ', '2016-01-12 19:48:48', null, '470.00', ' JOY ALUMASA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('62', null, null, null, null, 'Pay Bill from 254723283992 - ELIUD MELI Acc. 22548351', null, null, null, null, 'UPLOAD', 'KAC7TZOH1L', '22548351', '254723283992 ', '2016-01-12 19:12:28', null, '1750.00', ' ELIUD MELI', 'Completed', '2016-01-15 02:13:02', '1', '929', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('63', null, null, null, null, 'Pay Bill from 254728495886 - ROSE MUIRURI Acc. 10510427', null, null, null, null, 'UPLOAD', 'KAC0TZ9DZE', '10510427', '254728495886 ', '2016-01-12 18:34:24', null, '1350.00', ' ROSE MUIRURI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('64', null, null, null, null, 'Pay Bill from 254720179629 - JOYCE NDUNGU Acc. 13613377', null, null, null, null, 'UPLOAD', 'KAC4TYVRT4', '13613377', '254720179629 ', '2016-01-12 17:58:32', null, '2000.00', ' JOYCE NDUNGU', 'Completed', '2016-01-15 02:13:02', '1', '188', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('65', null, null, null, null, 'Pay Bill from 254715448834 - TERESA WARUI Acc. 0724423', null, null, null, null, 'UPLOAD', 'KAC7TY51SP', '0724423', '254715448834 ', '2016-01-12 16:42:11', null, '230.00', ' TERESA WARUI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('66', null, null, null, null, 'Pay Bill from 254721476444 - REGINA CHERONO Acc. 21893796', null, null, null, null, 'UPLOAD', 'KAC2TY0N1A', '21893796', '254721476444 ', '2016-01-12 16:28:19', null, '500.00', ' REGINA CHERONO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('67', null, null, null, null, 'Pay Bill from 254724227905 - JANET OTIENO Acc. 11799055', null, null, null, null, 'UPLOAD', 'KAC4TXTZDU', '11799055', '254724227905 ', '2016-01-12 16:06:59', null, '3000.00', ' JANET OTIENO', 'Completed', '2016-01-15 02:13:02', '1', '187', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('68', null, null, null, null, 'Pay Bill from 254713484368 - MARGARET CHIZI Acc. 24640433', null, null, null, null, 'UPLOAD', 'KAC8TXQ7I0', '24640433', '254713484368 ', '2016-01-12 15:54:47', null, '2000.00', ' MARGARET CHIZI', 'Completed', '2016-01-15 02:13:02', '1', '186', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('69', null, null, null, null, 'Pay Bill from 254702826395 - selestine oyugi Acc. 11726296', null, null, null, null, 'UPLOAD', 'KAC7TXP2HB', '11726296', '254702826395 ', '2016-01-12 15:50:48', null, '400.00', ' selestine oyugi', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('70', null, null, null, null, 'Pay Bill from 254717034845 - ELIZABETH FRANCIS Acc. 13131794', null, null, null, null, 'UPLOAD', 'KAC8TX4MTG', '13131794', '254717034845 ', '2016-01-12 14:43:31', null, '2000.00', ' ELIZABETH FRANCIS', 'Completed', '2016-01-15 02:13:02', '1', '928', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('71', null, null, null, null, 'Pay Bill from 254721385626 - NAUMI TUWEI Acc. 21771265', null, null, null, null, 'UPLOAD', 'KAC2TWY2E6', '21771265', '254721385626 ', '2016-01-12 14:21:34', null, '1000.00', ' NAUMI TUWEI', 'Completed', '2016-01-15 02:13:02', '1', '185', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('72', null, null, null, null, 'Pay Bill from 254705261430 - MBUGUA JACINTA Acc. 28298244', null, null, null, null, 'UPLOAD', 'KAC0TWVJPY', '28298244', '254705261430 ', '2016-01-12 14:13:05', null, '1750.00', ' MBUGUA JACINTA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('73', null, null, null, null, 'Pay Bill from 254720094327 - SERAH MUTHONI KIMANI Acc. 8715102', null, null, null, null, 'UPLOAD', 'KAC8TWJ332', '8715102', '254720094327 ', '2016-01-12 13:31:35', null, '1750.00', ' SERAH MUTHONI KIMANI', 'Completed', '2016-01-15 02:13:02', '1', '927', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('74', null, null, null, null, 'Pay Bill from 254721314169 - CELESTINE OBALA Acc. 10219775', null, null, null, null, 'UPLOAD', 'KAC6TWFTMY', '10219775', '254721314169 ', '2016-01-12 13:20:52', null, '1750.00', ' CELESTINE OBALA', 'Completed', '2016-01-15 02:13:02', '1', '184', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('75', null, null, null, null, 'Pay Bill from 254717367333 - VIOLET EKAKORO Acc. NAM WA', null, null, null, null, 'UPLOAD', 'KAC0TW95D2', 'NAM WA', '254717367333 ', '2016-01-12 12:58:49', null, '750.00', ' VIOLET EKAKORO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('76', null, null, null, null, 'Pay Bill from 254721746489 - DORCAS NYOKABI Acc. 30525433', null, null, null, null, 'UPLOAD', 'KAC1TVHHOD', '30525433', '254721746489 ', '2016-01-12 11:27:37', null, '5000.00', ' DORCAS NYOKABI', 'Completed', '2016-01-15 02:13:02', '1', '926', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('77', null, null, null, null, 'Pay Bill from 254716267629 - EVANS SIMIYU Acc. 32009817', null, null, null, null, 'UPLOAD', 'KAC2TVBHJK', '32009817', '254716267629 ', '2016-01-12 11:08:14', null, '500.00', ' EVANS SIMIYU', 'Completed', '2016-01-15 02:13:02', '1', '925', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('78', null, null, null, null, 'Pay Bill from 254716267629 - EVANS SIMIYU Acc. 32009817', null, null, null, null, 'UPLOAD', 'KAC2TV5NKE', '32009817', '254716267629 ', '2016-01-12 10:48:59', null, '5500.00', ' EVANS SIMIYU', 'Completed', '2016-01-15 02:13:02', '1', '924', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('79', null, null, null, null, 'Pay Bill from 254703696922 - GEORGE MUREU Acc. 13612217', null, null, null, null, 'UPLOAD', 'KAC1TV35JT', '13612217', '254703696922 ', '2016-01-12 10:40:40', null, '2000.00', ' GEORGE MUREU', 'Completed', '2016-01-15 02:13:02', '1', '923', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('80', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KAC2TV1REU', '', '454700 ', '2016-01-12 10:36:17', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('81', null, null, null, null, 'Pay Bill from 254713366708 - SILAH CHEBOI Acc. 7061595', null, null, null, null, 'UPLOAD', 'KAC1TTTH6L', '7061595', '254713366708 ', '2016-01-12 07:52:20', null, '3500.00', ' SILAH CHEBOI', 'Completed', '2016-01-15 02:13:02', '1', '922', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('82', null, null, null, null, 'Pay Bill from 254721688788 - JAMES OUMA OTINA Acc. 14482453', null, null, null, null, 'UPLOAD', 'KAB8TTAJ6A', '14482453', '254721688788 ', '2016-01-11 22:54:07', null, '2500.00', ' JAMES OUMA OTINA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('83', null, null, null, null, 'Pay Bill from 254729961262 - RUTHY KHAVERE Acc. 23378247', null, null, null, null, 'UPLOAD', 'KAB8TSQHK8', '23378247', '254729961262 ', '2016-01-11 21:00:23', null, '3000.00', ' RUTHY KHAVERE', 'Completed', '2016-01-15 02:13:02', '1', '921', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('84', null, null, null, null, 'Pay Bill from 254722988688 - ANJELAH KIBET Acc. 24098281', null, null, null, null, 'UPLOAD', 'KAB0TSPIKU', '24098281', '254722988688 ', '2016-01-11 20:57:04', null, '1500.00', ' ANJELAH KIBET', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('85', null, null, null, null, 'Pay Bill from 254722977517 - LINET MINANGE KAMADI Acc. 11790849', null, null, null, null, 'UPLOAD', 'KAB5TSONC9', '11790849', '254722977517 ', '2016-01-11 20:54:09', null, '900.00', ' LINET MINANGE KAMADI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('86', null, null, null, null, 'Pay Bill from 254704232855 - Ann Rotich Acc. 3445749', null, null, null, null, 'UPLOAD', 'KAB0TSMAUQ', '3445749', '254704232855 ', '2016-01-11 20:46:03', null, '2000.00', ' Ann Rotich', 'Completed', '2016-01-15 02:13:02', '1', '920', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('87', null, null, null, null, 'Pay Bill from 254720318495 - CAROLINE NJUGUNA Acc. 14656799', null, null, null, null, 'UPLOAD', 'KAB6TSC5KM', '14656799', '254720318495 ', '2016-01-11 20:14:04', null, '3500.00', ' CAROLINE NJUGUNA', 'Completed', '2016-01-15 02:13:02', '1', '919', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('88', null, null, null, null, 'Pay Bill from 254720756923 - PHILIP CHEPSIROR Acc. 24731224 ', null, null, null, null, 'UPLOAD', 'KAB7TQCQ0J', '24731224', '254720756923 ', '2016-01-11 17:01:27', null, '5000.00', ' PHILIP CHEPSIROR', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('89', null, null, null, null, 'Pay Bill from 254710863528 - TIMOTHY MURUNGA Acc. 31253537', null, null, null, null, 'UPLOAD', 'KAB8TQ9BGM', '31253537', '254710863528 ', '2016-01-11 16:51:30', null, '5000.00', ' TIMOTHY MURUNGA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('90', null, null, null, null, 'Pay Bill from 254721455371 - NOAH KEMBOI Acc. 22242550', null, null, null, null, 'UPLOAD', 'KAB0TQ8CC8', '22242550', '254721455371 ', '2016-01-11 16:48:36', null, '1750.00', ' NOAH KEMBOI', 'Completed', '2016-01-15 02:13:02', '1', '918', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('91', null, null, null, null, 'Pay Bill from 254718184681 - CHARLES ASHILENJE Acc. 6375864', null, null, null, null, 'UPLOAD', 'KAB6TQ745A', '6375864', '254718184681 ', '2016-01-11 16:44:48', null, '1750.00', ' CHARLES ASHILENJE', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('92', null, null, null, null, 'Pay Bill from 254723010571 - ELIZABETH LAGAT Acc. 23572582', null, null, null, null, 'UPLOAD', 'KAB7TQ6029', '23572582', '254723010571 ', '2016-01-11 16:41:36', null, '2000.00', ' ELIZABETH LAGAT', 'Completed', '2016-01-15 02:13:02', '1', '917', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('93', null, null, null, null, 'Pay Bill from 254720124508 - PETER YEGO KISANG Acc. 11731813', null, null, null, null, 'UPLOAD', 'KAB0TQ5IBQ', '11731813', '254720124508 ', '2016-01-11 16:39:55', null, '1750.00', ' PETER YEGO KISANG', 'Completed', '2016-01-15 02:13:02', '1', '916', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('94', null, null, null, null, 'Pay Bill from 254720598326 - JUSTUS KIPYEGO MELI Acc. 23493339', null, null, null, null, 'UPLOAD', 'KAB6TPTO22', '23493339', '254720598326 ', '2016-01-11 16:03:28', null, '500.00', ' JUSTUS KIPYEGO MELI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('95', null, null, null, null, 'Pay Bill from 254720262674 - RHODAH NJUGU Acc. 23632543', null, null, null, null, 'UPLOAD', 'KAB4TPNRO6', '23632543', '254720262674 ', '2016-01-11 15:44:08', null, '2000.00', ' RHODAH NJUGU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('96', null, null, null, null, 'Pay Bill from 254711672583 - SOLOMON MAIYO Acc. 28339093', null, null, null, null, 'UPLOAD', 'KAB1TP5VBF', '28339093', '254711672583 ', '2016-01-11 14:44:59', null, '1000.00', ' SOLOMON MAIYO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('97', null, null, null, null, 'Pay Bill from 254725487603 - GRACE ALIVIZA OKECHI Acc. 5275934', null, null, null, null, 'UPLOAD', 'KAB6TP0WGE', '5275934', '254725487603 ', '2016-01-11 14:28:47', null, '2000.00', ' GRACE ALIVIZA OKECHI', 'Completed', '2016-01-15 02:13:02', '1', '915', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('98', null, null, null, null, 'Pay Bill from 254720409652 - EVERLYNE WALEKHWA Acc. 43577757', null, null, null, null, 'UPLOAD', 'KAB7TOHXLN', '43577757', '254720409652 ', '2016-01-11 13:27:04', null, '2000.00', ' EVERLYNE WALEKHWA', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('99', null, null, null, null, 'Pay Bill from 254726017638 - STEPHEN LETTING Acc. Rachael wairimu', null, null, null, null, 'UPLOAD', 'KAB7TOE0EV', 'Rachael wairimu', '254726017638 ', '2016-01-11 13:14:12', null, '950.00', ' STEPHEN LETTING', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('100', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KAB1TOA7ZV', '', '454700 ', '2016-01-11 13:01:27', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('101', null, null, null, null, 'Pay Bill from 254700055607 - J0HANA MWAURA Acc. 24005550', null, null, null, null, 'UPLOAD', 'KAB9TNQNVH', '24005550', '254700055607 ', '2016-01-11 11:58:12', null, '1750.00', ' J0HANA MWAURA', 'Completed', '2016-01-15 02:13:02', '1', '914', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('102', null, null, null, null, 'Pay Bill from 254721239731 - KEVIN KAMANDA Acc. 22530888', null, null, null, null, 'UPLOAD', 'KAB9TNK5WZ', '22530888', '254721239731 ', '2016-01-11 11:37:20', null, '2500.00', ' KEVIN KAMANDA', 'Completed', '2016-01-15 02:13:02', '1', '913', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('103', null, null, null, null, 'Pay Bill from 254714650176 - MONICAH KIBOR Acc. 27720389', null, null, null, null, 'UPLOAD', 'KAB3TNIX5Z', '27720389', '254714650176 ', '2016-01-11 11:33:31', null, '1500.00', ' MONICAH KIBOR', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('104', null, null, null, null, 'Pay Bill from 254728926818 - SARAH NJERI MBURU Acc. 21574199', null, null, null, null, 'UPLOAD', 'KAB4TNBAII', '21574199', '254728926818 ', '2016-01-11 11:09:24', null, '1000.00', ' SARAH NJERI MBURU', 'Completed', '2016-01-15 02:13:02', '1', '912', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('105', null, null, null, null, 'Pay Bill from 254702573947 - Damaris Kahiba Acc. 13208255', null, null, null, null, 'UPLOAD', 'KAB8TN1CF8', '13208255', '254702573947 ', '2016-01-11 10:38:08', null, '3000.00', ' Damaris Kahiba', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('106', null, null, null, null, 'Pay Bill from 254722446291 - MATILDA ADHIAMBO AGENGA Acc. 11232100', null, null, null, null, 'UPLOAD', 'KAB8TN08YY', '11232100', '254722446291 ', '2016-01-11 10:34:35', null, '3000.00', ' MATILDA ADHIAMBO AGENGA', 'Completed', '2016-01-15 02:13:02', '1', '911', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('107', null, null, null, null, 'Pay Bill from 254702816025 - TOM MUKANGAI Acc. 28342112', null, null, null, null, 'UPLOAD', 'KAB8TN09AM', '28342112', '254702816025 ', '2016-01-11 10:34:30', null, '500.00', ' TOM MUKANGAI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('108', null, null, null, null, 'Pay Bill from 254706867141 - MARY MWIKUNYI Acc. 13204062', null, null, null, null, 'UPLOAD', 'KAB7TMZB0Z', '13204062', '254706867141 ', '2016-01-11 10:31:57', null, '5500.00', ' MARY MWIKUNYI', 'Completed', '2016-01-15 02:13:02', '1', '910', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('109', null, null, null, null, 'Pay Bill from 254728174233 - JOY ALUMASA Acc. 27661460', null, null, null, null, 'UPLOAD', 'KAB3TMMEZV', '27661460', '254728174233 ', '2016-01-11 09:51:04', null, '400.00', ' JOY ALUMASA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('110', null, null, null, null, 'Pay Bill from 254724331508 - ANNE WANJIKU Acc. 23608001', null, null, null, null, 'UPLOAD', 'KAB8TMJRZ6', '23608001', '254724331508 ', '2016-01-11 09:42:27', null, '1000.00', ' ANNE WANJIKU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('111', null, null, null, null, 'Pay Bill from 254713278519 - DENNIS SANG Acc. sold items', null, null, null, null, 'UPLOAD', 'KAB3TMGRUJ', 'sold items', '254713278519 ', '2016-01-11 09:32:52', null, '12000.00', ' DENNIS SANG', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('112', null, null, null, null, 'Pay Bill from 254727082064 - MARY KARANJA Acc. 24146822', null, null, null, null, 'UPLOAD', 'KAB9TM8R0D', '24146822', '254727082064 ', '2016-01-11 09:06:14', null, '1500.00', ' MARY KARANJA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('113', null, null, null, null, 'Pay Bill from 254724289479 - NAOMI WANGECHI Acc. 22235549', null, null, null, null, 'UPLOAD', 'KAB2TLUJLW', '22235549', '254724289479 ', '2016-01-11 08:11:29', null, '500.00', ' NAOMI WANGECHI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('114', null, null, null, null, 'Pay Bill from 254719344865 - PETERSON MURITHI Acc. 16089089', null, null, null, null, 'UPLOAD', 'KAB7TLQFAL', '16089089', '254719344865 ', '2016-01-11 07:51:04', null, '500.00', ' PETERSON MURITHI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('115', null, null, null, null, 'Pay Bill from 254718871437 - RUTH JEROBON Acc. 28216076', null, null, null, null, 'UPLOAD', 'KAB5TLIIDB', '28216076', '254718871437 ', '2016-01-11 06:54:12', null, '1500.00', ' RUTH JEROBON', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('116', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KAA5TKIMID', '23321763', '254728143256 ', '2016-01-10 20:39:56', null, '800.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('117', null, null, null, null, 'Pay Bill from 254725930353 - SOPHIA ABUKUTSA Acc. 22053485', null, null, null, null, 'UPLOAD', 'KAA4TJWXQA', '22053485', '254725930353 ', '2016-01-10 19:32:21', null, '4500.00', ' SOPHIA ABUKUTSA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('118', null, null, null, null, 'Pay Bill from 254727127669 - AGNETA KIBULO Acc. 20159077', null, null, null, null, 'UPLOAD', 'KAA2TJW688', '20159077', '254727127669 ', '2016-01-10 19:30:14', null, '700.00', ' AGNETA KIBULO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('119', null, null, null, null, 'Pay Bill from 254703206502 - MATTHEW KEMBOI Acc. \n27931696', null, null, null, null, 'UPLOAD', 'KAA0TJV7QI', '27931696', '254703206502 ', '2016-01-10 19:27:23', null, '2500.00', ' MATTHEW KEMBOI', 'Completed', '2016-01-15 02:13:02', '1', '909', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('120', null, null, null, null, 'Pay Bill from 254722977517 - LINET MINANGE KAMADI Acc. 11790849', null, null, null, null, 'UPLOAD', 'KAA5TJ6AQP', '11790849', '254722977517 ', '2016-01-10 18:15:00', null, '700.00', ' LINET MINANGE KAMADI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('121', null, null, null, null, 'Pay Bill from 254728257576 - NELLY CHESOLI Acc. 21132612', null, null, null, null, 'UPLOAD', 'KAA3TIP1CD', '21132612', '254728257576 ', '2016-01-10 17:22:52', null, '2000.00', ' NELLY CHESOLI', 'Completed', '2016-01-15 02:13:02', '1', '908', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('122', null, null, null, null, 'Pay Bill from 254704474915 - NELIS NABISWA Acc. 27211483', null, null, null, null, 'UPLOAD', 'KAA7THDDHN', '27211483', '254704474915 ', '2016-01-10 14:55:48', null, '460.00', ' NELIS NABISWA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('123', null, null, null, null, 'Pay Bill from 254713643079 - MARGARET OMOLLO Acc. 9181134', null, null, null, null, 'UPLOAD', 'KAA9TFSDWH', '9181134', '254713643079 ', '2016-01-10 11:25:49', null, '2500.00', ' MARGARET OMOLLO', 'Completed', '2016-01-15 02:13:02', '1', '907', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('124', null, null, null, null, 'Pay Bill from 254728057972 - MILKAH GITHUKA Acc. 28947751', null, null, null, null, 'UPLOAD', 'KAA5TF6SKH', '28947751', '254728057972 ', '2016-01-10 09:57:28', null, '300.00', ' MILKAH GITHUKA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('125', null, null, null, null, 'Pay Bill from 254721239731 - KEVIN KAMANDA Acc. 22530888', null, null, null, null, 'UPLOAD', 'KA95TD2ZOH', '22530888', '254721239731 ', '2016-01-09 20:29:21', null, '3000.00', ' KEVIN KAMANDA', 'Completed', '2016-01-15 02:13:02', '1', '906', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('126', null, null, null, null, 'Pay Bill from 254720387110 - REGINA WAGURA Acc. 11319950', null, null, null, null, 'UPLOAD', 'KA98TCOP9E', '11319950', '254720387110 ', '2016-01-09 19:49:45', null, '1350.00', ' REGINA WAGURA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('127', null, null, null, null, 'Pay Bill from 254703839421 - BEATRICE WANJIRU Acc. 20215403', null, null, null, null, 'UPLOAD', 'KA91TBVM2L', '20215403', '254703839421 ', '2016-01-09 18:33:11', null, '500.00', ' BEATRICE WANJIRU', 'Completed', '2016-01-15 02:13:02', '1', '183', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('128', null, null, null, null, 'Pay Bill from 254727426797 - HELLEN AWINJE Acc. 23034353', null, null, null, null, 'UPLOAD', 'KA93T9AAWD', '23034353', '254727426797 ', '2016-01-09 14:10:12', null, '1500.00', ' HELLEN AWINJE', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('129', null, null, null, null, 'Pay Bill from 254720031113 - CATHERINE SAIDI Acc. 13340232', null, null, null, null, 'UPLOAD', 'KA94T8AFQ6', '13340232', '254720031113 ', '2016-01-09 12:19:21', null, '400.00', ' CATHERINE SAIDI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('130', null, null, null, null, 'Pay Bill from 254723721796 - JOYCE SITIENEI Acc. 21175901', null, null, null, null, 'UPLOAD', 'KA92T83H1E', '21175901', '254723721796 ', '2016-01-09 11:58:04', null, '700.00', ' JOYCE SITIENEI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('131', null, null, null, null, 'Pay Bill from 254720960448 - PAUL KEINO Acc. 11465784', null, null, null, null, 'UPLOAD', 'KA95T7X2GN', '11465784', '254720960448 ', '2016-01-09 11:38:36', null, '1750.00', ' PAUL KEINO', 'Completed', '2016-01-15 02:13:02', '1', '905', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('132', null, null, null, null, 'Pay Bill from 254713672627 - RACHAEL NJUGUNA Acc. 26485537', null, null, null, null, 'UPLOAD', 'KA92T7LQJ0', '26485537', '254713672627 ', '2016-01-09 11:04:34', null, '800.00', ' RACHAEL NJUGUNA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('133', null, null, null, null, 'Pay Bill from 254723945194 - WILFRIDAH AKOTH ODONGO Acc. 23231669', null, null, null, null, 'UPLOAD', 'KA92T76LRW', '23231669', '254723945194 ', '2016-01-09 10:17:03', null, '1750.00', ' WILFRIDAH AKOTH ODONGO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('134', null, null, null, null, 'Pay Bill from 254724289479 - NAOMI WANGECHI Acc. 22235549', null, null, null, null, 'UPLOAD', 'KA92T6C0RI', '22235549', '254724289479 ', '2016-01-09 08:32:13', null, '500.00', ' NAOMI WANGECHI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('135', null, null, null, null, 'Pay Bill from 254712303243 - VIRGINIA NJAU Acc. 12877417', null, null, null, null, 'UPLOAD', 'KA88T52QY2', '12877417', '254712303243 ', '2016-01-08 21:19:31', null, '2000.00', ' VIRGINIA NJAU', 'Completed', '2016-01-15 02:13:02', '1', '904', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('136', null, null, null, null, 'Pay Bill from 254712298131 - CATHERINE OPAGALA Acc. 4384977', null, null, null, null, 'UPLOAD', 'KA87T4PJRN', '4384977', '254712298131 ', '2016-01-08 20:35:35', null, '1700.00', ' CATHERINE OPAGALA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('137', null, null, null, null, 'Pay Bill from 254726850122 - LEAH SIRMA Acc. 13613544', null, null, null, null, 'UPLOAD', 'KA84T4007O', '13613544', '254726850122 ', '2016-01-08 19:26:44', null, '1750.00', ' LEAH SIRMA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('138', null, null, null, null, 'Pay Bill from 254791329168 - PURITY CHEBII Acc. 217O14O4', null, null, null, null, 'UPLOAD', 'KA89T3INLF', '217O14O4', '254791329168 ', '2016-01-08 18:41:50', null, '500.00', ' PURITY CHEBII', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('139', null, null, null, null, 'Pay Bill from 254712628680 - MARY KAMAU Acc. 0724400', null, null, null, null, 'UPLOAD', 'KA85T3ADHX', '0724400', '254712628680 ', '2016-01-08 18:19:41', null, '2000.00', ' MARY KAMAU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('140', null, null, null, null, 'Pay Bill from 254714627703 - WINNY KOSGEI Acc. 28040491', null, null, null, null, 'UPLOAD', 'KA81T31KDZ', '28040491', '254714627703 ', '2016-01-08 17:55:19', null, '1720.00', ' WINNY KOSGEI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('141', null, null, null, null, 'Pay Bill from 254729961262 - RUTHY KHAVERE Acc. 23378247', null, null, null, null, 'UPLOAD', 'KA87T3037Z', '23378247', '254729961262 ', '2016-01-08 17:51:19', null, '3500.00', ' RUTHY KHAVERE', 'Completed', '2016-01-15 02:13:02', '1', '903', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('142', null, null, null, null, 'Pay Bill from 254726150317 - KEMEI JEPOTIP Acc. 21419429', null, null, null, null, 'UPLOAD', 'KA80T2WRNW', '21419429', '254726150317 ', '2016-01-08 17:42:18', null, '1750.00', ' KEMEI JEPOTIP', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('143', null, null, null, null, 'Pay Bill from 254722260583 - JOB LUNALO Acc. 11650418', null, null, null, null, 'UPLOAD', 'KA82T2TCIC', '11650418', '254722260583 ', '2016-01-08 17:32:25', null, '2500.00', ' JOB LUNALO', 'Completed', '2016-01-15 02:13:02', '1', '902', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('144', null, null, null, null, 'Pay Bill from 254720068107 - SARAH CHERUIYOT Acc. 4907976', null, null, null, null, 'UPLOAD', 'KA84T27MKE', '4907976', '254720068107 ', '2016-01-08 16:26:54', null, '1750.00', ' SARAH CHERUIYOT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('145', null, null, null, null, 'Pay Bill from 254729924504 - SAMUEL MALEYA KEDENGE Acc. 20978448', null, null, null, null, 'UPLOAD', 'KA89T20ZIP', '20978448', '254729924504 ', '2016-01-08 16:05:54', null, '500.00', ' SAMUEL MALEYA KEDENGE', 'Completed', '2016-01-15 02:13:02', '1', '901', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('146', null, null, null, null, 'Pay Bill from 254702826395 - selestine oyugi Acc. 11726296', null, null, null, null, 'UPLOAD', 'KA88T20L4C', '11726296', '254702826395 ', '2016-01-08 16:04:45', null, '550.00', ' selestine oyugi', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('147', null, null, null, null, 'Pay Bill from 254721471826 - DAVID KINUTHIA- Acc. 22397903', null, null, null, null, 'UPLOAD', 'KA81T1WOLT', '', '254721471826 ', '2016-01-08 15:52:17', null, '1000.00', ' DAVID KINUTHIA', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('148', null, null, null, null, 'Pay Bill from 254705292896 - ELIZABETH OWITI Acc. 9893255', null, null, null, null, 'UPLOAD', 'KA80T1SUJI', '9893255', '254705292896 ', '2016-01-08 15:39:51', null, '500.00', ' ELIZABETH OWITI', 'Completed', '2016-01-15 02:13:02', '1', '900', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('149', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KA86T1D9MW', '', '454700 ', '2016-01-08 14:49:07', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('150', null, null, null, null, 'Pay Bill from 254703696922 - GEORGE MUREU Acc. 13612217', null, null, null, null, 'UPLOAD', 'KA81T18TUT', '13612217', '254703696922 ', '2016-01-08 14:35:02', null, '1000.00', ' GEORGE MUREU', 'Completed', '2016-01-15 02:13:02', '1', '899', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('151', null, null, null, null, 'Pay Bill from 254717126858 - JANE KAGWEMA Acc. 6847786', null, null, null, null, 'UPLOAD', 'KA85T18CJV', '6847786', '254717126858 ', '2016-01-08 14:33:18', null, '50.00', ' JANE KAGWEMA', 'Completed', '2016-01-15 02:13:02', '1', '898', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('152', null, null, null, null, 'Pay Bill from 254718546096 - PAUL MANDALE Acc. 25346375', null, null, null, null, 'UPLOAD', 'KA84T15LJ4', '25346375', '254718546096 ', '2016-01-08 14:24:29', null, '3500.00', ' PAUL MANDALE', 'Completed', '2016-01-15 02:13:02', '1', '182', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('153', null, null, null, null, 'Pay Bill from 254727575640 - ISAAC ROTICH CHEPTORUS Acc. 0863920', null, null, null, null, 'UPLOAD', 'KA80T0K4A2', '0863920', '254727575640 ', '2016-01-08 13:13:06', null, '1400.00', ' ISAAC ROTICH CHEPTORUS', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('154', null, null, null, null, 'Pay Bill from 254714244002 - ESTHER ROTICH Acc. 8041930', null, null, null, null, 'UPLOAD', 'KA86T06NQM', '8041930', '254714244002 ', '2016-01-08 12:26:57', null, '250.00', ' ESTHER ROTICH', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('155', null, null, null, null, 'Pay Bill from 254720529747 - DANIEL NDUNGU Acc. 22855073', null, null, null, null, 'UPLOAD', 'KA81SZHRE5', '22855073', '254720529747 ', '2016-01-08 11:06:49', null, '700.00', ' DANIEL NDUNGU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('156', null, null, null, null, 'Pay Bill from 254717112010 - LUCY KARIUKI Acc. 24996238', null, null, null, null, 'UPLOAD', 'KA84SZGV36', '24996238', '254717112010 ', '2016-01-08 11:04:08', null, '300.00', ' LUCY KARIUKI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('157', null, null, null, null, 'Pay Bill from 254721339336 - SAMMY KIBET Acc. 23827304', null, null, null, null, 'UPLOAD', 'KA88SZ04Y2', '23827304', '254721339336 ', '2016-01-08 10:11:42', null, '1660.00', ' SAMMY KIBET', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('158', null, null, null, null, 'Pay Bill from 254721468952 - NEATBY AFANDI NANGAME Acc. 23240977', null, null, null, null, 'UPLOAD', 'KA89SYUNSL', '23240977', '254721468952 ', '2016-01-08 09:53:33', null, '1000.00', ' NEATBY AFANDI NANGAME', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('159', null, null, null, null, 'Pay Bill from 254728926818 - SARAH NJERI MBURU Acc. 21574199', null, null, null, null, 'UPLOAD', 'KA81SYTGBX', '21574199', '254728926818 ', '2016-01-08 09:49:58', null, '600.00', ' SARAH NJERI MBURU', 'Completed', '2016-01-15 02:13:02', '1', '897', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('160', null, null, null, null, 'Pay Bill from 254721239731 - KEVIN KAMANDA Acc. 22530888', null, null, null, null, 'UPLOAD', 'KA83SYQ3R5', '22530888', '254721239731 ', '2016-01-08 09:39:05', null, '2500.00', ' KEVIN KAMANDA', 'Completed', '2016-01-15 02:13:02', '1', '896', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('161', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KA86SYGT18', '23321763', '254728143256 ', '2016-01-08 09:08:11', null, '1000.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('162', null, null, null, null, 'Pay Bill from 254728832886 - PRISCILLAH WANJIRU Acc. 25752997', null, null, null, null, 'UPLOAD', 'KA83SY8EYF', '25752997', '254728832886 ', '2016-01-08 08:37:14', null, '3500.00', ' PRISCILLAH WANJIRU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('163', null, null, null, null, 'Pay Bill from 254723699551 - ELIZABETH WANJIRU NDERITU Acc. 13742294', null, null, null, null, 'UPLOAD', 'KA83SXR18L', '13742294', '254723699551 ', '2016-01-08 07:06:00', null, '500.00', ' ELIZABETH WANJIRU NDERITU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('164', null, null, null, null, 'Pay Bill from 254721929340 - DOLLY MASITSA Acc. 13614818', null, null, null, null, 'UPLOAD', 'KA71SWP5MJ', '13614818', '254721929340 ', '2016-01-07 20:45:38', null, '3500.00', ' DOLLY MASITSA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('165', null, null, null, null, 'Pay Bill from 254725370049 - FLOMENAH TOO Acc. 20846361', null, null, null, null, 'UPLOAD', 'KA78SWL8J0', '20846361', '254725370049 ', '2016-01-07 20:32:56', null, '2000.00', ' FLOMENAH TOO', 'Completed', '2016-01-15 02:13:02', '1', '895', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('166', null, null, null, null, 'Pay Bill from 254720221674 - ALBERT KURGAT Acc. 20688218', null, null, null, null, 'UPLOAD', 'KA78SW01Q6', '20688218', '254720221674 ', '2016-01-07 19:33:06', null, '1750.00', ' ALBERT KURGAT', 'Completed', '2016-01-15 02:13:02', '1', '894', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('167', null, null, null, null, 'Pay Bill from 254727314066 - MIRRIAM MUTURI Acc. 8717109', null, null, null, null, 'UPLOAD', 'KA71SVQ6G9', '8717109', '254727314066 ', '2016-01-07 19:07:29', null, '400.00', ' MIRRIAM MUTURI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('168', null, null, null, null, 'Pay Bill from 254728495886 - ROSE MUIRURI Acc. 10510427', null, null, null, null, 'UPLOAD', 'KA72SV21LE', '10510427', '254728495886 ', '2016-01-07 18:01:56', null, '500.00', ' ROSE MUIRURI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('169', null, null, null, null, 'Pay Bill from 254710276484 - JANE KAMAU Acc. 27834923', null, null, null, null, 'UPLOAD', 'KA74SUTLHY', '27834923', '254710276484 ', '2016-01-07 17:38:16', null, '3500.00', ' JANE KAMAU', 'Completed', '2016-01-15 02:13:02', '1', '893', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('170', null, null, null, null, 'Pay Bill from 254723025932 - LILIAN KOROS Acc. 0723025932', null, null, null, null, 'UPLOAD', 'KA74SUMTZA', '0723025932', '254723025932 ', '2016-01-07 17:18:43', null, '2000.00', ' LILIAN KOROS', 'Completed', '2016-01-15 02:13:02', '0', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('171', null, null, null, null, 'Pay Bill from 254727635222 - ANN THIONGO Acc. 22994508', null, null, null, null, 'UPLOAD', 'KA79SUGBS1', '22994508', '254727635222 ', '2016-01-07 16:59:06', null, '2650.00', ' ANN THIONGO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('172', null, null, null, null, 'Pay Bill from 254727025581 - SHADRACK LIMO Acc. 30097065', null, null, null, null, 'UPLOAD', 'KA74SUDTMC', '30097065', '254727025581 ', '2016-01-07 16:51:28', null, '1750.00', ' SHADRACK LIMO', 'Completed', '2016-01-15 02:13:02', '1', '892', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('173', null, null, null, null, 'Pay Bill from 254728354319 - 254728354319 MUGARO Acc. 13859168', null, null, null, null, 'UPLOAD', 'KA79SUABUP', '13859168', '254728354319 ', '2016-01-07 16:40:34', null, '1500.00', ' 254728354319 MUGARO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('174', null, null, null, null, 'Pay Bill from 254790706785 - RODAH MALIGA Acc. 32476001', null, null, null, null, 'UPLOAD', 'KA78SU55HI', '32476001', '254790706785 ', '2016-01-07 16:24:09', null, '500.00', ' RODAH MALIGA', 'Completed', '2016-01-15 02:13:02', '1', '891', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('175', null, null, null, null, 'Pay Bill from 254720832509 - CAROLINE NDIWA Acc. 21700143', null, null, null, null, 'UPLOAD', 'KA76ST5RH0', '21700143', '254720832509 ', '2016-01-07 14:25:27', null, '1700.00', ' CAROLINE NDIWA', 'Completed', '2016-01-15 02:13:02', '1', '890', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('176', null, null, null, null, 'Pay Bill from 254701034460 - IZABELLAH JUMA Acc. 31605738', null, null, null, null, 'UPLOAD', 'KA79ST0BL5', '31605738', '254701034460 ', '2016-01-07 14:06:44', null, '2100.00', ' IZABELLAH JUMA', 'Completed', '2016-01-15 02:13:02', '1', '181', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('177', null, null, null, null, 'Pay Bill from 254728143256 - NANCY CHELAGAT Acc. 23321763', null, null, null, null, 'UPLOAD', 'KA76SSSDY0', '23321763', '254728143256 ', '2016-01-07 13:40:16', null, '400.00', ' NANCY CHELAGAT', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('178', null, null, null, null, 'Pay Bill from 254703696922 - GEORGE MUREU Acc. 13612217', null, null, null, null, 'UPLOAD', 'KA76SSB5O0', '13612217', '254703696922 ', '2016-01-07 12:42:03', null, '1000.00', ' GEORGE MUREU', 'Completed', '2016-01-15 02:13:02', '1', '889', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('179', null, null, null, null, 'Pay Bill from 254723699551 - ELIZABETH WANJIRU NDERITU Acc. 13742294', null, null, null, null, 'UPLOAD', 'KA72SS6YZ6', '13742294', '254723699551 ', '2016-01-07 12:28:02', null, '500.00', ' ELIZABETH WANJIRU NDERITU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('180', null, null, null, null, 'Pay Bill from 254724764039 - NAOM MAIYO Acc. 24024669', null, null, null, null, 'UPLOAD', 'KA71SRVDVL', '24024669', '254724764039 ', '2016-01-07 11:49:38', null, '2000.00', ' NAOM MAIYO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('181', null, null, null, null, 'Pay Bill from 254723783177 - ESTHER KOMEN Acc. 9864993', null, null, null, null, 'UPLOAD', 'KA79SRMYQH', '9864993', '254723783177 ', '2016-01-07 11:22:00', null, '5700.00', ' ESTHER KOMEN', 'Completed', '2016-01-15 02:13:02', '1', '180', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('182', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KA74SRENNE', '', '454700 ', '2016-01-07 10:54:51', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('183', null, null, null, null, 'Pay Bill from 254724421381 - EVERLYNE ENDASI Acc. 22166973', null, null, null, null, 'UPLOAD', 'KA79SQRLZH', '22166973', '254724421381 ', '2016-01-07 09:40:16', null, '1750.00', ' EVERLYNE ENDASI', 'Completed', '2016-01-15 02:13:02', '1', '888', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('184', null, null, null, null, 'Pay Bill from 254703937300 - AGNES GITAU Acc. 22665200', null, null, null, null, 'UPLOAD', 'KA74SQLN4M', '22665200', '254703937300 ', '2016-01-07 09:20:09', null, '2500.00', ' AGNES GITAU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('185', null, null, null, null, 'Pay Bill from 254723848989 - MARGARET NDIRANGU Acc. 1232001', null, null, null, null, 'UPLOAD', 'KA71SQIXCJ', '1232001', '254723848989 ', '2016-01-07 09:10:46', null, '800.00', ' MARGARET NDIRANGU', 'Completed', '2016-01-15 02:13:02', '1', '887', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('186', null, null, null, null, 'Pay Bill from 254728926818 - SARAH NJERI MBURU Acc. 21574199', null, null, null, null, 'UPLOAD', 'KA70SPXMRU', '21574199', '254728926818 ', '2016-01-07 07:39:53', null, '1600.00', ' SARAH NJERI MBURU', 'Completed', '2016-01-15 02:13:02', '1', '886', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('187', null, null, null, null, 'Pay Bill from 254701034460 - IZABELLAH JUMA Acc. 31605738', null, null, null, null, 'UPLOAD', 'KA76SPWR3Q', '31605738', '254701034460 ', '2016-01-07 07:34:19', null, '1500.00', ' IZABELLAH JUMA', 'Completed', '2016-01-15 02:13:02', '1', '179', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('188', null, null, null, null, 'Pay Bill from 254724305359 - JANE ONSASE Acc. 6554882', null, null, null, null, 'UPLOAD', 'KA61SPCBYL', '6554882', '254724305359 ', '2016-01-06 22:19:20', null, '1000.00', ' JANE ONSASE', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('189', null, null, null, null, 'Pay Bill from 254720361544 - URSILAH KARONEY Acc. 20801084', null, null, null, null, 'UPLOAD', 'KA65SP301N', '20801084', '254720361544 ', '2016-01-06 21:32:22', null, '500.00', ' URSILAH KARONEY', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('190', null, null, null, null, 'Pay Bill from 254714395222 - MARY NJUGUNA Acc. 23392695', null, null, null, null, 'UPLOAD', 'KA65SOZ9L3', '23392695', '254714395222 ', '2016-01-06 21:17:35', null, '300.00', ' MARY NJUGUNA', 'Completed', '2016-01-15 02:13:02', '1', '885', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('191', null, null, null, null, 'Pay Bill from 254720387110 - REGINA WAGURA Acc. 11319950', null, null, null, null, 'UPLOAD', 'KA68SOC4HI', '11319950', '254720387110 ', '2016-01-06 20:05:37', null, '1200.00', ' REGINA WAGURA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('192', null, null, null, null, 'Pay Bill from 254707877689 - REBECCA KOSGEI Acc. 31667081', null, null, null, null, 'UPLOAD', 'KA66SNLX6K', '31667081', '254707877689 ', '2016-01-06 19:01:47', null, '2300.00', ' REBECCA KOSGEI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('193', null, null, null, null, 'Pay Bill from 254728354319 - 254728354319 MUGARO Acc. 13859168', null, null, null, null, 'UPLOAD', 'KA67SNJY9T', '13859168', '254728354319 ', '2016-01-06 18:57:10', null, '1250.00', ' 254728354319 MUGARO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('194', null, null, null, null, 'Pay Bill from 254791329168 - PURITY CHEBII Acc. 217O14O4', null, null, null, null, 'UPLOAD', 'KA68SN8DWY', '217O14O4', '254791329168 ', '2016-01-06 18:27:06', null, '500.00', ' PURITY CHEBII', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('195', null, null, null, null, 'Pay Bill from 254720252852 - SUSAN EGO Acc. 11338397', null, null, null, null, 'UPLOAD', 'KA64SMSS9O', '11338397', '254720252852 ', '2016-01-06 17:45:39', null, '1750.00', ' SUSAN EGO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('196', null, null, null, null, 'Pay Bill from 254723709005 - NASHON VUGUSU ALIGULA Acc. 10560369', null, null, null, null, 'UPLOAD', 'KA60SMF8CK', '10560369', '254723709005 ', '2016-01-06 17:07:53', null, '5000.00', ' NASHON VUGUSU ALIGULA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('197', null, null, null, null, 'Pay Bill from 254720318495 - CAROLINE NJUGUNA Acc. 14656799', null, null, null, null, 'UPLOAD', 'KA68SMA1MA', '14656799', '254720318495 ', '2016-01-06 16:53:27', null, '1750.00', ' CAROLINE NJUGUNA', 'Completed', '2016-01-15 02:13:02', '1', '884', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('198', null, null, null, null, 'Pay Bill from 254721696395 - ESTHER JERUTO KANGOGO Acc. 3257963', null, null, null, null, 'UPLOAD', 'KA61SM5OG1', '3257963', '254721696395 ', '2016-01-06 16:41:39', null, '2500.00', ' ESTHER JERUTO KANGOGO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('199', null, null, null, null, 'Pay Bill from 254720031113 - CATHERINE SAIDI Acc. 13340232', null, null, null, null, 'UPLOAD', 'KA68SM3GJU', '13340232', '254720031113 ', '2016-01-06 16:35:42', null, '1500.00', ' CATHERINE SAIDI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('200', null, null, null, null, 'Pay Bill from 254712450221 - LUCY PATRICK Acc.  0925162', null, null, null, null, 'UPLOAD', 'KA64SLYXNU', '0925162', '254712450221 ', '2016-01-06 16:23:09', null, '1750.00', ' LUCY PATRICK', 'Completed', '2016-01-15 02:13:02', '1', '883', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('201', null, null, null, null, 'Pay Bill from 254713643079 - MARGARET OMOLLO Acc. 9181134', null, null, null, null, 'UPLOAD', 'KA63SLTM3P', '9181134', '254713643079 ', '2016-01-06 16:07:29', null, '2500.00', ' MARGARET OMOLLO', 'Completed', '2016-01-15 02:13:02', '1', '882', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('202', null, null, null, null, 'Pay Bill from 254726344131 - PATRICK BUTAKI WANJALA Acc. 24262767', null, null, null, null, 'UPLOAD', 'KA61SLK5TD', '24262767', '254726344131 ', '2016-01-06 15:39:16', null, '3500.00', ' PATRICK BUTAKI WANJALA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('203', null, null, null, null, 'Pay Bill from 254729662636 - JANE WAMBUI Acc. 25599757', null, null, null, null, 'UPLOAD', 'KA64SKQY24', '25599757', '254729662636 ', '2016-01-06 14:07:54', null, '1500.00', ' JANE WAMBUI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('204', null, null, null, null, 'Pay Bill from 254728748037 - Biliha Okanga Acc. 20905492', null, null, null, null, 'UPLOAD', 'KA66SKF2NI', '20905492', '254728748037 ', '2016-01-06 13:31:43', null, '300.00', ' Biliha Okanga', 'Completed', '2016-01-15 02:13:02', '1', '881', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('205', null, null, null, null, 'Pay Bill from 254722794600 - MARY WANJIKU KINGORI Acc. 5552539', null, null, null, null, 'UPLOAD', 'KA68SK0OYM', '5552539', '254722794600 ', '2016-01-06 12:47:10', null, '1050.00', ' MARY WANJIKU KINGORI', 'Completed', '2016-01-15 02:13:02', '1', '178', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('206', null, null, null, null, 'Pay Bill from 254720994371 - HELLEN SIMATEI Acc. 13204347', null, null, null, null, 'UPLOAD', 'KA64SJXCD4', '13204347', '254720994371 ', '2016-01-06 12:36:42', null, '3500.00', ' HELLEN SIMATEI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('207', null, null, null, null, 'Pay Bill from 254725776340 - MONICA ROTICH Acc. 25523242', null, null, null, null, 'UPLOAD', 'KA60SJIXFC', '25523242', '254725776340 ', '2016-01-06 11:52:40', null, '1750.00', ' MONICA ROTICH', 'Completed', '2016-01-15 02:13:02', '1', '880', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('208', null, null, null, null, 'Pay Bill from 254721934941 - RAPHAEL RUTTOH Acc. 11181461', null, null, null, null, 'UPLOAD', 'KA63SJGKHX', '11181461', '254721934941 ', '2016-01-06 11:45:13', null, '400.00', ' RAPHAEL RUTTOH', 'Completed', '2016-01-15 02:13:02', '1', '879', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('209', null, null, null, null, 'Pay Bill from 254714590910 - BEATRICE MURIMA Acc. 3937994', null, null, null, null, 'UPLOAD', 'KA65SJDJL5', '3937994', '254714590910 ', '2016-01-06 11:36:06', null, '1750.00', ' BEATRICE MURIMA', 'Completed', '2016-01-15 02:13:02', '1', '878', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('210', null, null, null, null, 'Pay Bill from 254722794600 - MARY WANJIKU KINGORI Acc. 5552539', null, null, null, null, 'UPLOAD', 'KA67SJCEVD', '5552539', '254722794600 ', '2016-01-06 11:32:23', null, '2650.00', ' MARY WANJIKU KINGORI', 'Completed', '2016-01-15 02:13:02', '1', '177', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('211', null, null, null, null, 'Pay Bill from 254727575640 - ISAAC ROTICH CHEPTORUS Acc. 0863920', null, null, null, null, 'UPLOAD', 'KA60SJC65K', '0863920', '254727575640 ', '2016-01-06 11:31:45', null, '350.00', ' ISAAC ROTICH CHEPTORUS', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('212', null, null, null, null, 'Pay Bill from 254715448834 - TERESA WARUI Acc. 0724423', null, null, null, null, 'UPLOAD', 'KA64SIU9U4', '0724423', '254715448834 ', '2016-01-06 10:36:58', null, '300.00', ' TERESA WARUI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('213', null, null, null, null, 'Pay Bill from 254719344865 - PETERSON MURITHI Acc. 16089089', null, null, null, null, 'UPLOAD', 'KA68SIRX3Y', '16089089', '254719344865 ', '2016-01-06 10:29:45', null, '500.00', ' PETERSON MURITHI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('214', null, null, null, null, 'Pay Bill from 254723634927 - NAOM WANGOI NJANE Acc. 3447986', null, null, null, null, 'UPLOAD', 'KA69SIAHHT', '3447986', '254723634927 ', '2016-01-06 09:35:10', null, '1000.00', ' NAOM WANGOI NJANE', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('215', null, null, null, null, 'Pay Bill from 254712303243 - VIRGINIA NJAU Acc. 12877417', null, null, null, null, 'UPLOAD', 'KA69SI9OF5', '12877417', '254712303243 ', '2016-01-06 09:32:41', null, '2500.00', ' VIRGINIA NJAU', 'Completed', '2016-01-15 02:13:02', '1', '877', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('216', null, null, null, null, 'Pay Bill from 254723356619 - GRACE NGUGI Acc. 5281582', null, null, null, null, 'UPLOAD', 'KA67SI2D9R', '5281582', '254723356619 ', '2016-01-06 09:08:39', null, '250.00', ' GRACE NGUGI', 'Completed', '2016-01-15 02:13:02', '1', '176', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('217', null, null, null, null, 'Pay Bill from 254720240231 - EMILY KUNDU Acc. 22501570   ', null, null, null, null, 'UPLOAD', 'KA61SHOUMP', '22501570', '254720240231 ', '2016-01-06 08:18:13', null, '500.00', ' EMILY KUNDU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('218', null, null, null, null, 'Pay Bill from 254721688788 - JAMES OUMA OTINA Acc. 14482453', null, null, null, null, 'UPLOAD', 'KA57SH2K9L', '14482453', '254721688788 ', '2016-01-05 23:59:03', null, '2500.00', ' JAMES OUMA OTINA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('219', null, null, null, null, 'Pay Bill from 254723356619 - GRACE NGUGI Acc. 5281582', null, null, null, null, 'UPLOAD', 'KA54SGTIUS', '5281582', '254723356619 ', '2016-01-05 22:07:56', null, '2100.00', ' GRACE NGUGI', 'Completed', '2016-01-15 02:13:02', '1', '175', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('220', null, null, null, null, 'Pay Bill from 254714395222 - MARY NJUGUNA Acc. 23392695', null, null, null, null, 'UPLOAD', 'KA58SGMZX4', '23392695', '254714395222 ', '2016-01-05 21:36:09', null, '300.00', ' MARY NJUGUNA', 'Completed', '2016-01-15 02:13:02', '1', '876', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('221', null, null, null, null, 'Pay Bill from 254710198004 - LILIAN NJERI KAMAU Acc. 1229511', null, null, null, null, 'UPLOAD', 'KA59SFW617', '1229511', '254710198004 ', '2016-01-05 20:07:47', null, '1750.00', ' LILIAN NJERI KAMAU', 'Completed', '2016-01-15 02:13:02', '1', '875', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('222', null, null, null, null, 'Pay Bill from 254723283992 - ELIUD MELI Acc. 22548351', null, null, null, null, 'UPLOAD', 'KA56SFSWHG', '22548351', '254723283992 ', '2016-01-05 19:59:08', null, '1750.00', ' ELIUD MELI', 'Completed', '2016-01-15 02:13:02', '1', '874', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('223', null, null, null, null, 'Pay Bill from 254712544196 - MORAA MOBEGI Acc. 4102865', null, null, null, null, 'UPLOAD', 'KA52SEWZ8A', '4102865', '254712544196 ', '2016-01-05 18:37:12', null, '3000.00', ' MORAA MOBEGI', 'Completed', '2016-01-15 02:13:02', '1', '174', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('224', null, null, null, null, 'Utility Account to Organization Settlement Account', null, null, null, null, 'UPLOAD', 'KA53SERRNT', '', '454700 ', '2016-01-05 18:22:55', null, '0.00', ' SUNGROUP MICRO AFRICA LIMITED', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('225', null, null, null, null, 'Pay Bill from 254723699551 - ELIZABETH WANJIRU NDERITU Acc. 13742294', null, null, null, null, 'UPLOAD', 'KA54SEHZ3C', '13742294', '254723699551 ', '2016-01-05 17:56:53', null, '500.00', ' ELIZABETH WANJIRU NDERITU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('226', null, null, null, null, 'Pay Bill from 254728748037 - Biliha Okanga Acc. 20905492', null, null, null, null, 'UPLOAD', 'KA51SEFIUL', '20905492', '254728748037 ', '2016-01-05 17:50:12', null, '1500.00', ' Biliha Okanga', 'Completed', '2016-01-15 02:13:02', '1', '873', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('227', null, null, null, null, 'Pay Bill from 254720094327 - SERAH MUTHONI KIMANI Acc. 8715102', null, null, null, null, 'UPLOAD', 'KA50SEC204', '8715102', '254720094327 ', '2016-01-05 17:40:25', null, '1750.00', ' SERAH MUTHONI KIMANI', 'Completed', '2016-01-15 02:13:02', '1', '872', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('228', null, null, null, null, 'Pay Bill from 254705261430 - MBUGUA JACINTA Acc. 28298244', null, null, null, null, 'UPLOAD', 'KA55SEBOYJ', '28298244', '254705261430 ', '2016-01-05 17:39:21', null, '1750.00', ' MBUGUA JACINTA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('229', null, null, null, null, 'Pay Bill from 254724966696 - WILSON KIPLAGAT Acc. 23218583', null, null, null, null, 'UPLOAD', 'KA58SE0PHE', '23218583', '254724966696 ', '2016-01-05 17:08:40', null, '600.00', ' WILSON KIPLAGAT', 'Completed', '2016-01-15 02:13:02', '1', '173', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('230', null, null, null, null, 'Pay Bill from 254720533146 - STEPHEN KUTO Acc. 20692367', null, null, null, null, 'UPLOAD', 'KA58SDYGT8', '20692367', '254720533146 ', '2016-01-05 17:02:03', null, '1100.00', ' STEPHEN KUTO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('231', null, null, null, null, 'Pay Bill from 254724421381 - EVERLYNE ENDASI Acc. 22166973', null, null, null, null, 'UPLOAD', 'KA50SDY8RK', '22166973', '254724421381 ', '2016-01-05 17:01:39', null, '1950.00', ' EVERLYNE ENDASI', 'Completed', '2016-01-15 02:13:02', '1', '871', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('232', null, null, null, null, 'Pay Bill from 254728354319 - 254728354319 MUGARO Acc. 13859168', null, null, null, null, 'UPLOAD', 'KA58SDPSWW', '13859168', '254728354319 ', '2016-01-05 16:37:14', null, '1250.00', ' 254728354319 MUGARO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('233', null, null, null, null, 'Pay Bill from 254717034845 - ELIZABETH FRANCIS Acc. 13131794', null, null, null, null, 'UPLOAD', 'KA52SDP58I', '13131794', '254717034845 ', '2016-01-05 16:35:06', null, '2000.00', ' ELIZABETH FRANCIS', 'Completed', '2016-01-15 02:13:02', '1', '870', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('234', null, null, null, null, 'Pay Bill from 254720501901 - CLARE KAHAOYA Acc. 21741604', null, null, null, null, 'UPLOAD', 'KA54SDMIC6', '21741604', '254720501901 ', '2016-01-05 16:27:07', null, '2300.00', ' CLARE KAHAOYA', 'Completed', '2016-01-15 02:13:02', '1', '172', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('235', null, null, null, null, 'Pay Bill from 254722446291 - MATILDA ADHIAMBO AGENGA Acc. 11232100', null, null, null, null, 'UPLOAD', 'KA53SDGXOT', '11232100', '254722446291 ', '2016-01-05 16:10:21', null, '3000.00', ' MATILDA ADHIAMBO AGENGA', 'Completed', '2016-01-15 02:13:02', '1', '869', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('236', null, null, null, null, 'Pay Bill from 254711421742 - ANNE ORAYO Acc. 22256418', null, null, null, null, 'UPLOAD', 'KA58SCYHZM', '22256418', '254711421742 ', '2016-01-05 15:14:28', null, '4000.00', ' ANNE ORAYO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('237', null, null, null, null, 'Pay Bill from 254716589064 - LONAH CHARLES Acc. 27942097', null, null, null, null, 'UPLOAD', 'KA58SCY2E8', '27942097', '254716589064 ', '2016-01-05 15:12:59', null, '1750.00', ' LONAH CHARLES', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('238', null, null, null, null, 'Pay Bill from 254710995854 - DENNIS THEURI Acc. 29161063', null, null, null, null, 'UPLOAD', 'KA59SCO21R', '29161063', '254710995854 ', '2016-01-05 14:42:05', null, '3000.00', ' DENNIS THEURI', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('239', null, null, null, null, 'Pay Bill from 254720266700 - MARY NDIRANGU Acc. 1232001', null, null, null, null, 'UPLOAD', 'KA51SCHK6N', '1232001', '254720266700 ', '2016-01-05 14:22:04', null, '1000.00', ' MARY NDIRANGU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('240', null, null, null, null, 'Pay Bill from 254706867060 - JALOT NEKESA Acc. 29728448', null, null, null, null, 'UPLOAD', 'KA52SC1L6O', '29728448', '254706867060 ', '2016-01-05 13:32:34', null, '1750.00', ' JALOT NEKESA', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('241', null, null, null, null, 'Pay Bill from 254726402646 - MICHAEL MUASYA NGUNDO Acc. 12539713', null, null, null, null, 'UPLOAD', 'KA57SBQ3O5', '12539713', '254726402646 ', '2016-01-05 12:57:00', null, '3900.00', ' MICHAEL MUASYA NGUNDO', 'Completed', '2016-01-15 02:13:02', '1', '171', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('242', null, null, null, null, 'Pay Bill from 254711708395 - JUDY NJERI MUHORO Acc. 12583264', null, null, null, null, 'UPLOAD', 'KA53SBH8XV', '12583264', '254711708395 ', '2016-01-05 12:29:32', null, '300.00', ' JUDY NJERI MUHORO', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('243', null, null, null, null, 'Pay Bill from 254791433505 - PHILICE LIYAYI Acc. 25346375', null, null, null, null, 'UPLOAD', 'KA54SB7VOI', '25346375', '254791433505 ', '2016-01-05 12:01:35', null, '5250.00', ' PHILICE LIYAYI', 'Completed', '2016-01-15 02:13:02', '1', '170', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('244', null, null, null, null, 'Pay Bill from 254723100817 - EVERLINE EDAGIZA Acc. 20884714', null, null, null, null, 'UPLOAD', 'KA52SB3IDE', '20884714', '254723100817 ', '2016-01-05 11:48:12', null, '500.00', ' EVERLINE EDAGIZA', 'Completed', '2016-01-15 02:13:02', '1', '868', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('245', null, null, null, null, 'Pay Bill from 254729961262 - RUTHY KHAVERE Acc. 23378247', null, null, null, null, 'UPLOAD', 'KA50SAULD8', '23378247', '254729961262 ', '2016-01-05 11:21:13', null, '1100.00', ' RUTHY KHAVERE', 'Completed', '2016-01-15 02:13:02', '1', '867', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('246', null, null, null, null, 'Pay Bill from 254716267629 - EVANS SIMIYU Acc. 32009817', null, null, null, null, 'UPLOAD', 'KA57SA67GF', '32009817', '254716267629 ', '2016-01-05 10:08:01', null, '1750.00', ' EVANS SIMIYU', 'Completed', '2016-01-15 02:13:02', '1', '866', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('247', null, null, null, null, 'Pay Bill from 254724966696 - WILSON KIPLAGAT Acc. 23218583', null, null, null, null, 'UPLOAD', 'KA54SA40XY', '23218583', '254724966696 ', '2016-01-05 10:01:00', null, '1900.00', ' WILSON KIPLAGAT', 'Completed', '2016-01-15 02:13:02', '1', '169', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('248', null, null, null, null, 'Pay Bill from 254718871437 - RUTH JEROBON Acc. 28216076', null, null, null, null, 'UPLOAD', 'KA57S9ZK55', '28216076', '254718871437 ', '2016-01-05 09:47:11', null, '1990.00', ' RUTH JEROBON', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('249', null, null, null, null, 'Pay Bill from 254723648472 - RAEL JEPKORIR SANG Acc. 3254385', null, null, null, null, 'UPLOAD', 'KA57S9VIF1', '3254385', '254723648472 ', '2016-01-05 09:34:33', null, '1450.00', ' RAEL JEPKORIR SANG', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('250', null, null, null, null, 'Pay Bill from 254720529747 - DANIEL NDUNGU Acc. 22855073', null, null, null, null, 'UPLOAD', 'KA55S9EJ5P', '22855073', '254720529747 ', '2016-01-05 08:36:10', null, '1000.00', ' DANIEL NDUNGU', 'Completed', '2016-01-15 02:13:02', '2', null, 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('251', null, null, null, null, 'Pay Bill from 254722217742 - ANNE KIMANI Acc. 21361313', null, null, null, null, 'UPLOAD', 'KA56S93C04', '21361313', '254722217742 ', '2016-01-05 07:46:34', null, '1750.00', ' ANNE KIMANI', 'Completed', '2016-01-15 02:13:02', '1', '865', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('252', null, null, null, null, 'Pay Bill from 254723613472 - MARY NJENGA Acc. 21497089', null, null, null, null, 'UPLOAD', 'KA51S8SNJF', '21497089', '254723613472 ', '2016-01-05 05:50:34', null, '2500.00', ' MARY NJENGA', 'Completed', '2016-01-15 02:13:02', '1', '168', 'ORG_454700_Utility Account_Completed_20160114170949.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('253', null, null, null, null, 'Pay Bill from 254721333272 - GRACE WAIRIMU K0MOTHO Acc. 11888344', null, null, null, null, 'UPLOAD', 'KB21YQ51BJ', '11888344', '254721333272 ', '2016-02-02 18:08:15', null, '1750.00', ' GRACE WAIRIMU K0MOTHO', 'Completed', '2016-02-02 19:11:40', '1', '965', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('254', null, null, null, null, 'Pay Bill from 254704232855 - Ann Rotich Acc. 3445749', null, null, null, null, 'UPLOAD', 'KB28YPZNTC', '3445749', '254704232855 ', '2016-02-02 17:55:07', null, '2610.00', ' Ann Rotich', 'Completed', '2016-02-02 19:11:40', '1', '964', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('255', null, null, null, null, 'Pay Bill from 254724002427 - KEVIN KEBENEI Acc. 25886264', null, null, null, null, 'UPLOAD', 'KB22YPWM2C', '25886264', '254724002427 ', '2016-02-02 17:47:45', null, '7000.00', ' KEVIN KEBENEI', 'Completed', '2016-02-02 19:11:40', '1', '963', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('256', null, null, null, null, 'Pay Bill from 254711988026 - ISAAC ORENGE Acc. 20696390', null, null, null, null, 'UPLOAD', 'KB26YPPCOQ', '20696390', '254711988026 ', '2016-02-02 17:29:51', null, '2900.00', ' ISAAC ORENGE', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('257', null, null, null, null, 'Pay Bill from 254722260583 - JOB LUNALO Acc. 11650418', null, null, null, null, 'UPLOAD', 'KB26YPN2IY', '11650418', '254722260583 ', '2016-02-02 17:23:56', null, '2500.00', ' JOB LUNALO', 'Completed', '2016-02-02 19:11:40', '1', '962', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('258', null, null, null, null, 'Pay Bill from 254710276484 - JANE KAMAU Acc. 27834923', null, null, null, null, 'UPLOAD', 'KB29YPGBDZ', '27834923', '254710276484 ', '2016-02-02 17:06:25', null, '2640.00', ' JANE KAMAU', 'Completed', '2016-02-02 19:11:40', '1', '961', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('259', null, null, null, null, 'Pay Bill from 254721900066 - MAGDALENE AMWAYI Acc. 11072965', null, null, null, null, 'UPLOAD', 'KB29YP5Z8R', '11072965', '254721900066 ', '2016-02-02 16:38:02', null, '1700.00', ' MAGDALENE AMWAYI', 'Completed', '2016-02-02 19:11:40', '1', '960', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('260', null, null, null, null, 'Pay Bill from 254710143655 - JENIFFER KIBOR Acc. 1278552', null, null, null, null, 'UPLOAD', 'KB22YP4UIQ', '1278552', '254710143655 ', '2016-02-02 16:34:59', null, '2000.00', ' JENIFFER KIBOR', 'Completed', '2016-02-02 19:11:40', '1', '959', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('261', null, null, null, null, 'Pay Bill from 254712298131 - CATHERINE OPAGALA Acc. 4384977', null, null, null, null, 'UPLOAD', 'KB21YP0ZFH', '4384977', '254712298131 ', '2016-02-02 16:23:38', null, '500.00', ' CATHERINE OPAGALA', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('262', null, null, null, null, 'Pay Bill from 254725472020 - JENIFFER CHERUTICH Acc. 9493946', null, null, null, null, 'UPLOAD', 'KB27YOQ9UJ', '9493946', '254725472020 ', '2016-02-02 15:52:01', null, '3000.00', ' JENIFFER CHERUTICH', 'Completed', '2016-02-02 19:11:40', '1', '958', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('263', null, null, null, null, 'Pay Bill from 254724227905 - JANET OTIENO Acc. 11799055', null, null, null, null, 'UPLOAD', 'KB20YOLXAI', '11799055', '254724227905 ', '2016-02-02 15:39:07', null, '3000.00', ' JANET OTIENO', 'Completed', '2016-02-02 19:11:40', '1', '957', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('264', null, null, null, null, 'Pay Bill from 254717034845 - ELIZABETH FRANCIS Acc. 13131794', null, null, null, null, 'UPLOAD', 'KB24YOI64S', '13131794', '254717034845 ', '2016-02-02 15:27:41', null, '3000.00', ' ELIZABETH FRANCIS', 'Completed', '2016-02-02 19:11:40', '1', '956', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('265', null, null, null, null, 'Pay Bill from 254721868779 - SARAH KARIUKI Acc. 21240569', null, null, null, null, 'UPLOAD', 'KB27YOGJGN', '21240569', '254721868779 ', '2016-02-02 15:22:51', null, '1750.00', ' SARAH KARIUKI', 'Completed', '2016-02-02 19:11:40', '1', '955', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('266', null, null, null, null, 'Pay Bill from 254723524049 - JOYCE KANINI Acc. 4270544', null, null, null, null, 'UPLOAD', 'KB20YOE2XS', '4270544', '254723524049 ', '2016-02-02 15:15:23', null, '2050.00', ' JOYCE KANINI', 'Completed', '2016-02-02 19:11:40', '1', '954', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('267', null, null, null, null, 'Pay Bill from 254725652582 - CORNELIUS RONOH Acc. 31520143', null, null, null, null, 'UPLOAD', 'KB25YOCX3T', '31520143', '254725652582 ', '2016-02-02 15:11:32', null, '1750.00', ' CORNELIUS RONOH', 'Completed', '2016-02-02 19:11:40', '1', '953', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('268', null, null, null, null, 'Pay Bill from 254710482944 - DAISY LELEY Acc. 22548636', null, null, null, null, 'UPLOAD', 'KB29YOCHZH', '22548636', '254710482944 ', '2016-02-02 15:10:37', null, '1050.00', ' DAISY LELEY', 'Completed', '2016-02-02 19:11:40', '1', '952', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('269', null, null, null, null, 'Pay Bill from 254721385626 - NAUMI TUWEI Acc. 21771265', null, null, null, null, 'UPLOAD', 'KB21YNSJ2Z', '21771265', '254721385626 ', '2016-02-02 14:08:18', null, '1500.00', ' NAUMI TUWEI', 'Completed', '2016-02-02 19:11:40', '1', '951', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('270', null, null, null, null, 'Pay Bill from 254712550641 - SARAH BITOK Acc. 20462716', null, null, null, null, 'UPLOAD', 'KB27YNSBRT', '20462716', '254712550641 ', '2016-02-02 14:07:40', null, '1750.00', ' SARAH BITOK', 'Completed', '2016-02-02 19:11:40', '1', '950', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('271', null, null, null, null, 'Pay Bill from 254716267629 - EVANS SIMIYU Acc. 32009817', null, null, null, null, 'UPLOAD', 'KB23YN9V97', '32009817', '254716267629 ', '2016-02-02 13:11:14', null, '1750.00', ' EVANS SIMIYU', 'Completed', '2016-02-02 19:11:40', '1', '949', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('272', null, null, null, null, 'Pay Bill from 254721688788 - JAMES OUMA OTINA Acc. 14482453', null, null, null, null, 'UPLOAD', 'KB23YMWL3L', '14482453', '254721688788 ', '2016-02-02 12:30:14', null, '2000.00', ' JAMES OUMA OTINA', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('273', null, null, null, null, 'Pay Bill from 254720598326 - JUSTUS KIPYEGO MELI Acc. 23493339', null, null, null, null, 'UPLOAD', 'KB23YMIW9X', '23493339', '254720598326 ', '2016-02-02 11:47:59', null, '500.00', ' JUSTUS KIPYEGO MELI', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('274', null, null, null, null, 'Pay Bill from 254723037226 - NICHOLAS KETER Acc. 25533301', null, null, null, null, 'UPLOAD', 'KB29YMF37D', '25533301', '254723037226 ', '2016-02-02 11:36:32', null, '3500.00', ' NICHOLAS KETER', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('275', null, null, null, null, 'Pay Bill from 254722576985 - JAIRUS WANJALA Acc. 21599663', null, null, null, null, 'UPLOAD', 'KB25YLZH67', '21599663', '254722576985 ', '2016-02-02 10:49:39', null, '1750.00', ' JAIRUS WANJALA', 'Completed', '2016-02-02 19:11:40', '1', '948', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('276', null, null, null, null, 'Pay Bill from 254705292896 - ELIZABETH OWITI Acc. 9893255', null, null, null, null, 'UPLOAD', 'KB29YLVHKV', '9893255', '254705292896 ', '2016-02-02 10:37:49', null, '1750.00', ' ELIZABETH OWITI', 'Completed', '2016-02-02 19:11:40', '1', '947', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('277', null, null, null, null, 'Pay Bill from 254722417793 - PAUSTINE LUNALO Acc. 11339647', null, null, null, null, 'UPLOAD', 'KB25YLHX5Z', '11339647', '254722417793 ', '2016-02-02 09:57:26', null, '1000.00', ' PAUSTINE LUNALO', 'Completed', '2016-02-02 19:11:40', '2', null, 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('278', null, null, null, null, 'Pay Bill from 254725268971 - DAVID KARIUKI Acc. 7664769', null, null, null, null, 'UPLOAD', 'KB29YLDBBJ', '7664769', '254725268971 ', '2016-02-02 09:43:28', null, '3000.00', ' DAVID KARIUKI', 'Completed', '2016-02-02 19:11:40', '1', '946', 'sma8.csv');
INSERT INTO `tb_mpesarepayments` VALUES ('279', null, null, null, null, 'Pay Bill from 254727614968 - VICTORIA AKUMU Acc. 31745924', null, null, null, null, 'UPLOAD', 'KB22YL15YW', '31745924', '254727614968 ', '2016-02-02 09:05:42', null, '1750.00', ' VICTORIA AKUMU', 'Completed', '2016-02-02 19:11:40', '1', '945', 'sma8.csv');

-- ----------------------------
-- Table structure for tb_mpesarepayments_test
-- ----------------------------
DROP TABLE IF EXISTS `tb_mpesarepayments_test`;
CREATE TABLE `tb_mpesarepayments_test` (
  `rowid` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `orig` varchar(50) DEFAULT NULL,
  `dest` decimal(12,0) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  `customer_id` varchar(100) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `routemethod_id` int(11) DEFAULT NULL,
  `routemethod_name` varchar(20) DEFAULT NULL,
  `mpesa_code` varchar(30) DEFAULT NULL,
  `mpesa_acc` varchar(30) DEFAULT NULL,
  `mpesa_trx_date` datetime DEFAULT NULL,
  `mpesa_trx_time` varchar(10) DEFAULT NULL,
  `mpesa_amt` double(18,2) DEFAULT NULL,
  `mpesa_sender` varchar(50) DEFAULT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed` tinyint(1) DEFAULT '0',
  `receiptno` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  UNIQUE KEY `mpesa_code` (`mpesa_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_mpesarepayments_test
-- ----------------------------

-- ----------------------------
-- Table structure for tb_organization
-- ----------------------------
DROP TABLE IF EXISTS `tb_organization`;
CREATE TABLE `tb_organization` (
  `organisationid` int(11) NOT NULL AUTO_INCREMENT,
  `regnumber` varchar(100) DEFAULT NULL,
  `phoneno` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdateate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`organisationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_organization
-- ----------------------------

-- ----------------------------
-- Table structure for tb_period
-- ----------------------------
DROP TABLE IF EXISTS `tb_period`;
CREATE TABLE `tb_period` (
  `periodid` int(11) NOT NULL AUTO_INCREMENT,
  `periodyearid` int(11) NOT NULL,
  `monthno` int(11) NOT NULL,
  `quarter` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `closed` tinyint(1) DEFAULT '1',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`periodid`),
  KEY `FK_tb_period_tb_periodyear` (`periodyearid`),
  CONSTRAINT `FK_tb_period_tb_periodyear` FOREIGN KEY (`periodyearid`) REFERENCES `tb_periodyear` (`periodyearid`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_period
-- ----------------------------
INSERT INTO `tb_period` VALUES ('1', '1', '10', '4', 'October', 'Oct', '2013-10-01 00:00:00', '2013-10-31 00:00:00', '0', '2012-10-25 08:55:57', '1');
INSERT INTO `tb_period` VALUES ('2', '1', '11', '4', 'November', 'Nov', '2013-11-01 00:00:00', '2013-11-30 00:00:00', '0', '2014-07-16 16:12:59', '3');
INSERT INTO `tb_period` VALUES ('3', '1', '12', '4', 'December', 'Dec', '2013-12-01 00:00:00', '2013-12-31 00:00:00', '0', '2014-07-17 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('4', '2', '1', '1', 'January', 'Jan', '2014-01-01 00:00:00', '2014-01-31 00:00:00', '0', '2014-07-17 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('5', '2', '2', '1', 'February', 'Feb', '2014-02-01 00:00:00', '2014-02-28 00:00:00', '0', '2014-07-17 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('6', '2', '3', '1', 'March', 'Mar', '2014-03-01 00:00:00', '2014-03-31 00:00:00', '0', '2014-07-17 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('7', '2', '4', '2', 'April', 'April', '2014-04-01 00:00:00', '2014-04-30 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('8', '2', '5', '2', 'May', 'May', '2014-05-01 00:00:00', '2014-05-30 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('9', '2', '6', '2', 'June', 'June', '2014-06-01 00:00:00', '2014-06-30 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('10', '2', '7', '3', 'July', 'July', '2014-07-01 00:00:00', '2014-07-31 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('11', '2', '8', '3', 'August', 'Aug', '2014-08-01 00:00:00', '2014-08-31 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('12', '2', '9', '3', 'September', 'Sep', '2014-09-01 00:00:00', '2014-09-30 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('13', '2', '10', '4', 'October', 'Oct', '2014-10-01 00:00:00', '2014-10-31 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('14', '2', '11', '4', 'November', 'Nov', '2014-11-01 00:00:00', '2014-11-30 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('15', '2', '12', '4', 'December', 'Dec', '2014-12-01 00:00:00', '2014-12-31 00:00:00', '0', '2014-07-18 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('16', '3', '1', '1', 'January', 'Jan', '2015-01-01 00:00:00', '2015-01-31 00:00:00', '0', '2014-07-20 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('17', '3', '2', '1', 'February', 'Feb', '2015-02-01 00:00:00', '2015-02-28 00:00:00', '0', '2014-07-21 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('18', '3', '3', '1', 'March', 'March', '2015-03-01 00:00:00', '2015-03-31 00:00:00', '0', '2015-03-07 17:00:00', '3');
INSERT INTO `tb_period` VALUES ('19', '3', '4', '2', 'April', 'Apri', '2015-04-01 00:00:00', '2015-04-30 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('20', '3', '5', '2', 'May', 'May', '2015-05-01 00:00:00', '2015-05-31 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('21', '3', '6', '2', 'June', 'Jun', '2015-06-01 00:00:00', '2015-06-30 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('22', '3', '7', '3', 'July', 'July', '2015-07-01 00:00:00', '2015-07-31 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('23', '3', '8', '3', 'August', 'Aug', '2015-08-01 00:00:00', '2015-08-31 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('24', '3', '9', '3', 'September', 'Sept', '2015-09-01 00:00:00', '2015-09-30 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('25', '3', '10', '4', 'October', 'Oct', '2015-10-01 00:00:00', '2015-10-31 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('26', '3', '11', '4', 'November', 'Nov', '2015-11-01 00:00:00', '2015-11-30 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('27', '3', '12', '4', 'December', 'Dec', '2015-12-01 00:00:00', '2015-12-31 00:00:00', '0', '2015-04-03 16:00:00', '3');
INSERT INTO `tb_period` VALUES ('28', '4', '1', '1', 'January', 'Jan', '2016-01-01 00:00:00', '2016-01-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('29', '4', '2', '1', 'February', 'Feb', '2016-02-01 00:00:00', '2016-02-29 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('30', '4', '3', '1', 'March', 'March', '2016-03-01 00:00:00', '2016-03-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('31', '4', '4', '2', 'April', 'Apri', '2016-04-01 00:00:00', '2016-04-30 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('32', '4', '5', '2', 'May', 'May', '2016-05-01 00:00:00', '2016-05-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('33', '4', '6', '2', 'June', 'Jun', '2016-06-01 00:00:00', '2016-06-30 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('34', '4', '7', '3', 'July', 'July', '2016-07-01 00:00:00', '2016-07-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('35', '4', '8', '3', 'August', 'Aug', '2016-08-01 00:00:00', '2016-08-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('36', '4', '9', '3', 'September', 'Sept', '2016-09-01 00:00:00', '2016-09-30 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('37', '4', '10', '4', 'October', 'Oct', '2016-10-01 00:00:00', '2016-10-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('38', '4', '11', '4', 'November', 'Nov', '2016-11-01 00:00:00', '2016-11-30 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('39', '4', '12', '4', 'December', 'Dec', '2016-12-01 00:00:00', '2016-12-31 00:00:00', '0', '2016-01-07 12:45:03', null);
INSERT INTO `tb_period` VALUES ('40', '5', '1', '1', 'January', 'Jan 2017', '2017-01-01 00:00:00', '2017-01-31 00:00:00', '0', '2016-11-29 01:00:00', '1');
INSERT INTO `tb_period` VALUES ('41', '5', '2', '1', 'February', 'Feb 2017', '2017-02-01 00:00:00', '2017-02-28 00:00:00', '1', '2017-02-05 00:00:00', '1');

-- ----------------------------
-- Table structure for tb_periodyear
-- ----------------------------
DROP TABLE IF EXISTS `tb_periodyear`;
CREATE TABLE `tb_periodyear` (
  `periodyearid` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `eoydone` tinyint(1) DEFAULT '0',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`periodyearid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_periodyear
-- ----------------------------
INSERT INTO `tb_periodyear` VALUES ('1', '2013', '1', '2012-10-25 07:01:41', '1');
INSERT INTO `tb_periodyear` VALUES ('2', '2014', '1', '2014-07-01 14:49:00', '1');
INSERT INTO `tb_periodyear` VALUES ('3', '2015', '0', '2014-07-04 11:53:05', '1');
INSERT INTO `tb_periodyear` VALUES ('4', '2016', '0', '2014-07-04 11:53:18', '1');
INSERT INTO `tb_periodyear` VALUES ('5', '2017', '0', '2014-07-04 11:59:17', '3');
INSERT INTO `tb_periodyear` VALUES ('6', '2018', '0', '2014-07-04 11:59:26', '3');
INSERT INTO `tb_periodyear` VALUES ('7', '2019', '0', '2014-07-04 12:02:23', '3');
INSERT INTO `tb_periodyear` VALUES ('8', '2020', '0', '2014-07-21 01:32:31', '3');
INSERT INTO `tb_periodyear` VALUES ('9', '2021', '0', '2014-07-21 11:59:13', '3');

-- ----------------------------
-- Table structure for tb_person
-- ----------------------------
DROP TABLE IF EXISTS `tb_person`;
CREATE TABLE `tb_person` (
  `personid` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` int(11) DEFAULT NULL,
  `surname` varchar(200) DEFAULT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `maritalstatus` int(11) DEFAULT NULL,
  `dateofbirth` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `idtype` int(11) DEFAULT NULL,
  `idno` varchar(200) DEFAULT NULL,
  `phoneno` bigint(20) DEFAULT NULL,
  `mobileno` varchar(15) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`personid`),
  KEY `tb_person_idno` (`idno`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_person
-- ----------------------------
INSERT INTO `tb_person` VALUES ('1', '1', 'KIBUTHA', 'JOHN', 'KARANJA', '1', '2', '2016-12-08 00:00:00', '', '1', '26274194', null, '0728324866', '2016-12-08 00:00:00', null, '2016-12-08 13:16:34');
INSERT INTO `tb_person` VALUES ('2', '1', 'NJOROGE', 'SIMON', 'MWANGI', '1', '2', '2016-12-08 00:00:00', '', '1', '24276747', null, '0726171891', '2016-12-08 00:00:00', null, '2016-12-08 13:29:19');
INSERT INTO `tb_person` VALUES ('3', '1', 'MBUGUA', 'STEPHEN', '', '1', '2', '2016-12-08 00:00:00', '', '1', '13426614', null, '0722589958', '2016-12-08 00:00:00', null, '2016-12-08 13:30:58');
INSERT INTO `tb_person` VALUES ('4', '1', 'MWAURA', 'PETER', '', '1', '2', '2016-12-08 00:00:00', '', '1', '10863651', null, '0720121205', '2016-12-08 00:00:00', null, '2016-12-08 13:32:16');
INSERT INTO `tb_person` VALUES ('5', '1', 'WAITA', 'AMBROSE', '', '1', '2', '2016-12-08 00:00:00', '', '1', '25849372', null, '0707620660', '2016-12-08 00:00:00', null, '2016-12-08 13:33:48');
INSERT INTO `tb_person` VALUES ('6', '1', 'GACHERU', 'EZEKIEL', '', '1', '1', '2016-12-08 00:00:00', '', '1', '26556381', null, '0723169824', '2016-12-08 00:00:00', null, '2016-12-08 13:34:48');
INSERT INTO `tb_person` VALUES ('7', '1', 'NJUGUNA', 'ROBERT', '', '1', '2', '2016-12-08 00:00:00', '', '1', '14727596', null, '0716078608', '2016-12-08 00:00:00', null, '2016-12-08 13:50:06');
INSERT INTO `tb_person` VALUES ('8', '1', 'KAMAU', 'STEPHEN', '', '1', '2', '2016-12-08 00:00:00', '', '1', '21632605', null, '0723130745', '2016-12-08 00:00:00', null, '2016-12-08 13:51:30');
INSERT INTO `tb_person` VALUES ('9', '1', 'IRUNGU', 'EDWARD', '', '1', '2', '2016-12-08 00:00:00', '', '1', '0784529', null, '0722304273', '2016-12-08 00:00:00', null, '2016-12-08 13:52:30');
INSERT INTO `tb_person` VALUES ('10', '1', 'GACHIRA', 'JOHN', '', '1', '2', '2016-12-08 00:00:00', '', '1', '0513339', null, '0722967310', '2016-12-08 00:00:00', null, '2016-12-08 13:54:11');
INSERT INTO `tb_person` VALUES ('11', '1', 'MBUGUA', 'PETER', '', '1', '2', '2016-12-08 00:00:00', '', '1', '11273089', null, '0711238244', '2016-12-08 00:00:00', null, '2016-12-08 14:13:37');
INSERT INTO `tb_person` VALUES ('12', '1', 'MURIMA', 'SIMON', '', '1', '2', '2016-12-08 00:00:00', '', '1', '9830639', null, '0724328239', '2016-12-08 00:00:00', null, '2016-12-08 14:22:24');
INSERT INTO `tb_person` VALUES ('13', '1', 'MUTUKU', 'ANTONY', 'NZWILI', '1', '2', '2016-12-08 00:00:00', '', '1', '25626928', null, '0722941373', '2016-12-08 00:00:00', null, '2016-12-08 14:24:59');
INSERT INTO `tb_person` VALUES ('14', '1', 'MALOI', 'GEDION', '', '1', '2', '2016-12-14 00:00:00', '', '1', '2291670', null, '722752903', '2016-12-14 00:00:00', null, '2016-12-14 14:41:12');
INSERT INTO `tb_person` VALUES ('15', '0', 'KIRAMBA', 'SAMMY', '', '1', '2', '2016-12-14 00:00:00', '', '1', '13029673', null, '727988700', '2016-12-14 00:00:00', null, '2016-12-14 14:43:32');
INSERT INTO `tb_person` VALUES ('16', '0', 'NDERITU', 'SALOME', '', '2', '2', '2016-12-14 00:00:00', '', '1', '22435094', null, '721880885', '2016-12-14 00:00:00', null, '2016-12-14 14:44:49');
INSERT INTO `tb_person` VALUES ('17', '0', 'MUGO', 'GERALD', '', '1', '2', '2016-12-14 00:00:00', '', '1', '25453919', null, '728783321', '2016-12-14 00:00:00', null, '2016-12-14 14:45:57');
INSERT INTO `tb_person` VALUES ('18', '0', 'PATRICK', 'KINUTHIA', '', '1', '2', '2016-12-14 00:00:00', '', '1', '22274786', null, '720789667', '2016-12-14 00:00:00', null, '2016-12-14 14:55:07');
INSERT INTO `tb_person` VALUES ('19', '0', 'WAWERU', 'SAMUEL', '', '1', '2', '2016-12-14 00:00:00', '', '1', '23775457', null, '720945083', '2016-12-14 00:00:00', null, '2016-12-14 14:58:22');
INSERT INTO `tb_person` VALUES ('20', '0', 'MUNAI', 'OLE', '', '1', '2', '2016-12-14 00:00:00', '', '1', '3088511', null, '722561945', '2016-12-14 00:00:00', null, '2016-12-14 15:06:33');
INSERT INTO `tb_person` VALUES ('21', '0', 'WAMUGI', 'JOSEPH', '', '1', '2', '2016-12-14 00:00:00', '', '1', '24763417', null, '728542594', '2016-12-14 00:00:00', null, '2016-12-14 15:07:43');
INSERT INTO `tb_person` VALUES ('22', '0', 'MOKUA', 'KENNEDY', '', '1', '2', '2016-12-14 00:00:00', '', '1', '10928761', null, '722458669', '2016-12-14 00:00:00', null, '2016-12-14 15:08:58');
INSERT INTO `tb_person` VALUES ('23', '0', 'NJAGI', 'ANTONY', '', '1', '2', '2016-12-14 00:00:00', '', '1', '23343835', null, '713104016', '2016-12-14 00:00:00', null, '2016-12-14 15:10:17');
INSERT INTO `tb_person` VALUES ('24', '0', 'NDUNGU', 'JOSEPH', '', '1', '2', '2016-12-14 00:00:00', '', '1', '11406754', null, '721584712', '2016-12-14 00:00:00', null, '2016-12-14 15:11:21');
INSERT INTO `tb_person` VALUES ('25', '0', 'WAITHERA', 'NANCY', '', '2', '2', '2016-12-14 00:00:00', '', '1', '6485831', null, '720391323', '2016-12-14 00:00:00', null, '2016-12-14 15:13:13');
INSERT INTO `tb_person` VALUES ('26', '0', 'NJUGUNA', 'JOSEPH', '', '1', '2', '2016-12-14 00:00:00', '', '1', '9089703', null, '722673494', '2016-12-14 00:00:00', null, '2016-12-14 15:16:27');
INSERT INTO `tb_person` VALUES ('27', '0', 'NUNGARI', 'SUSAN', '', '2', '2', '2016-12-14 00:00:00', '', '1', '13511773', null, '726311918', '2016-12-14 00:00:00', null, '2016-12-14 15:19:29');
INSERT INTO `tb_person` VALUES ('28', '0', 'MUINDI', 'NICHOLUS', '', '1', '2', '2016-12-14 00:00:00', '', '1', '25072176', null, '725320437', '2016-12-14 00:00:00', null, '2016-12-14 15:20:40');
INSERT INTO `tb_person` VALUES ('29', '0', 'OTARA', 'ZABLON', '', '1', '2', '1970-01-01 00:00:00', '', '1', '24701787', null, '733124150', '2016-12-14 00:00:00', null, '2016-12-14 15:21:41');
INSERT INTO `tb_person` VALUES ('30', '0', 'MWANGI', 'SIMON', '', '1', '2', '2016-12-14 00:00:00', '', '1', '3200697', null, '726754026', '2016-12-14 00:00:00', null, '2016-12-14 15:23:22');
INSERT INTO `tb_person` VALUES ('31', '0', 'WANJIRU', 'JOYCE', '', '2', '2', '2016-12-14 00:00:00', '', '1', '1900367', null, '722311411', '2016-12-14 00:00:00', null, '2016-12-14 15:24:52');
INSERT INTO `tb_person` VALUES ('32', '0', 'MOSES', 'MARTIN', '', '1', '2', '2016-12-14 00:00:00', '', '1', '22179149', null, '724558567', '2016-12-14 00:00:00', null, '2016-12-14 15:26:26');
INSERT INTO `tb_person` VALUES ('33', '0', 'KARIUKI', 'JOSEPH', '', '1', '2', '2016-12-14 00:00:00', '', '1', '1459498', null, '724968123', '2016-12-14 00:00:00', null, '2016-12-14 15:27:46');
INSERT INTO `tb_person` VALUES ('34', '0', 'KAPITA', 'JOSEPH', '', '1', '2', '2016-12-14 00:00:00', '', '1', '5855176', null, '726503611', '2016-12-14 00:00:00', null, '2016-12-14 15:30:20');
INSERT INTO `tb_person` VALUES ('35', '0', 'NJOROGE', 'SAMUEL', '', '1', '1', '2016-12-14 00:00:00', '', '1', '30159321', null, '791426458', '2016-12-14 00:00:00', null, '2016-12-14 15:31:41');
INSERT INTO `tb_person` VALUES ('36', '0', 'NYAMBURA', 'SARAH', '', '2', '2', '2016-12-14 00:00:00', '', '1', '24630387', null, '726964530', '2016-12-14 00:00:00', null, '2016-12-14 15:35:03');
INSERT INTO `tb_person` VALUES ('37', '0', 'MUTINDA', 'CAROLINE', '', '2', '1', '2016-12-14 00:00:00', '', '1', '29649919', null, '705434532', '2016-12-14 00:00:00', null, '2016-12-14 15:35:52');
INSERT INTO `tb_person` VALUES ('38', '0', 'NJATHI', 'SAMUEL', '', '1', '2', '2016-12-14 00:00:00', '', '1', '22602021', null, '725409304', '2016-12-14 00:00:00', null, '2016-12-14 15:37:02');
INSERT INTO `tb_person` VALUES ('39', '0', 'MAINA', 'WILSON', '', '1', '2', '2016-12-14 00:00:00', '', '1', '13827975', null, '713483874', '2016-12-14 00:00:00', null, '2016-12-14 15:38:28');
INSERT INTO `tb_person` VALUES ('40', '0', 'MWANGI', 'DAVID', '', '1', '2', '2016-12-14 00:00:00', '', '1', '25149222', null, '725543694', '2016-12-14 00:00:00', null, '2016-12-14 15:39:56');
INSERT INTO `tb_person` VALUES ('41', '0', 'KARUGA', 'JAMES', '', '1', '2', '2016-12-14 00:00:00', '', '1', '24759038', null, '729698375', '2016-12-14 00:00:00', null, '2016-12-14 15:41:06');
INSERT INTO `tb_person` VALUES ('42', '0', 'NJONGE', 'FRANCIS', '', '1', '2', '2016-12-14 00:00:00', '', '1', '23624510', null, '723226439', '2016-12-14 00:00:00', null, '2016-12-14 16:16:55');
INSERT INTO `tb_person` VALUES ('43', '0', 'EZEKIEL', 'SAITOTI', '', '1', '2', '2016-12-19 00:00:00', '', '1', '22812342', null, '722719363', '2016-12-19 00:00:00', null, '2016-12-19 10:01:56');
INSERT INTO `tb_person` VALUES ('44', '0', 'MUCHIRI', 'JOSEPH', '', '1', '2', '2016-12-19 00:00:00', '', '1', '27876365', null, '707063819', '2016-12-19 00:00:00', null, '2016-12-19 10:03:42');

-- ----------------------------
-- Table structure for tb_productloan
-- ----------------------------
DROP TABLE IF EXISTS `tb_productloan`;
CREATE TABLE `tb_productloan` (
  `productid` int(11) DEFAULT NULL,
  `settings` text,
  `effectivedate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_productloan
-- ----------------------------
INSERT INTO `tb_productloan` VALUES ('2', '{ \"interestrate\": \"5\", \"effectivedate\": \"2016-12-08\", \"minloanterm\": \"1\", \"maxloanterm\": \"8\", \"minloanamount\": \"1000\", \"maxloanamount\": \"20000\", \"interestcalculationmethod\": \"3\", \"minimumsavings\": \"0\", \"savingsproduct\":\"\", \"graceperiod\": \"0\", \"percentage\": \"0\", \"guaranteeproducts\": [], \"noofguarantor\": \"0\", \"useguaranteedshares\": \"0\", \"graceperiodon\": \"1\", \"daycount\": \"1\", \"termperiod\": \"2\", \"flatinterestamount\": \"\", \"ignoreholidays\": \"0\", \"fee\": [], \"futureinterest\": \"0\", \"accrueinterest\": \"1\", \"allowloantopup\": \"1\", \"percentageoftopuploanpaid\": \"75\", \"topupfeeamount\" : \"350\" }', '2016-12-08 00:00:00');
INSERT INTO `tb_productloan` VALUES ('3', '{ \"interestrate\": \"12\", \"effectivedate\": \"2017-03-09\", \"minloanterm\": \"1\", \"maxloanterm\": \"60\", \"minloanamount\": \"1\", \"maxloanamount\": \"250000\", \"interestcalculationmethod\": \"3\", \"minimumsavings\": \"100\", \"savingsproduct\":\"1\", \"graceperiod\": \"0\", \"percentage\": \"100\", \"guaranteeproducts\": [{\"productid\":\"1\",\"productname\":\"Customer Savings\"},{\"productid\":\"1\",\"productname\":\"Customer Contibutions\"}], \"noofguarantor\": \"0\", \"useguaranteedshares\": \"1\", \"graceperiodon\": \"1\", \"daycount\": \"1\", \"termperiod\": \"3\", \"flatinterestamount\": \"\", \"ignoreholidays\": \"0\", \"fee\": [{\"feeid\":\"2\",\"feename\":\"Loan Application Fee\"}], \"futureinterest\": \"1\", \"accrueinterest\": \"1\", \"allowloantopup\": \"0\", \"percentageoftopuploanpaid\": \"0\", \"topupfeeamount\" : \"\" }', '2017-03-09 00:00:00');
INSERT INTO `tb_productloan` VALUES ('4', '{ \"interestrate\": \"12\", \"effectivedate\": \"2016-12-08\", \"minloanterm\": \"1\", \"maxloanterm\": \"60\", \"minloanamount\": \"1\", \"maxloanamount\": \"250000\", \"interestcalculationmethod\": \"3\", \"minimumsavings\": \"0\", \"savingsproduct\":\"1\", \"graceperiod\": \"0\", \"percentage\": \"\", \"guaranteeproducts\": [{\"productid\":\"1\",\"productname\":\"Customer Savings\"}], \"noofguarantor\": \"1\", \"useguaranteedshares\": \"0\", \"graceperiodon\": \"1\", \"daycount\": \"2\", \"termperiod\": \"3\", \"flatinterestamount\": \"\", \"ignoreholidays\": \"0\", \"fee\": [{\"feeid\":\"1\",\"feename\":\"Registration Fee\"},{\"feeid\":\"2\",\"feename\":\"Loan Application Fee\"}], \"futureinterest\": \"0\", \"accrueinterest\": \"1\", \"allowloantopup\": \"0\", \"percentageoftopuploanpaid\": \"0\", \"topupfeeamount\" : \"\" }', '2016-12-08 00:00:00');

-- ----------------------------
-- Table structure for tb_products
-- ----------------------------
DROP TABLE IF EXISTS `tb_products`;
CREATE TABLE `tb_products` (
  `productid` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `producttypeid` int(11) DEFAULT NULL,
  `currencyid` int(11) DEFAULT NULL,
  `maxaccounts` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_products
-- ----------------------------
INSERT INTO `tb_products` VALUES ('1', 'S01', 'Customer Contibutions', '3', '1', '10', '1', '2015-09-18 00:00:00', '2016-12-08 00:00:00', '2015-09-19 23:04:31');
INSERT INTO `tb_products` VALUES ('3', 'L01', 'Normal Loan', '4', '1', '1', '1', '2016-11-09 00:00:00', '2017-03-09 00:00:00', '2016-11-09 19:30:18');
INSERT INTO `tb_products` VALUES ('4', 'L03', 'Emergency Loans', '4', '1', '1', '1', '2016-11-10 00:00:00', '2016-12-08 00:00:00', '2016-11-10 12:18:38');

-- ----------------------------
-- Table structure for tb_productshare
-- ----------------------------
DROP TABLE IF EXISTS `tb_productshare`;
CREATE TABLE `tb_productshare` (
  `productid` int(11) DEFAULT NULL,
  `termdeposit` tinyint(1) DEFAULT NULL,
  `minbalance` double(18,2) DEFAULT NULL,
  `maxbalance` double(18,2) DEFAULT NULL,
  `minage` int(11) DEFAULT NULL,
  `maxage` int(11) DEFAULT NULL,
  `minterm` int(11) DEFAULT NULL,
  `maxterm` int(11) DEFAULT NULL,
  `contributions` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_productshare
-- ----------------------------
INSERT INTO `tb_productshare` VALUES ('1', '0', '200.00', '99999999.00', '18', '99999', '0', '0', '[]');

-- ----------------------------
-- Table structure for tb_producttype
-- ----------------------------
DROP TABLE IF EXISTS `tb_producttype`;
CREATE TABLE `tb_producttype` (
  `producttypeid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `producttype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`producttypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_producttype
-- ----------------------------
INSERT INTO `tb_producttype` VALUES ('1', 'Shares', 'savings');
INSERT INTO `tb_producttype` VALUES ('2', 'Deposits', 'savings');
INSERT INTO `tb_producttype` VALUES ('3', 'Savings', 'savings');
INSERT INTO `tb_producttype` VALUES ('4', 'Loan', 'loan');

-- ----------------------------
-- Table structure for tb_quesms
-- ----------------------------
DROP TABLE IF EXISTS `tb_quesms`;
CREATE TABLE `tb_quesms` (
  `queid` bigint(20) NOT NULL AUTO_INCREMENT,
  `phoneno` bigint(12) DEFAULT NULL,
  `message` varchar(160) DEFAULT NULL,
  `datein` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sent` tinyint(1) DEFAULT '0',
  `dateout` datetime DEFAULT NULL,
  `statusid` int(11) DEFAULT '0',
  `batchid` bigint(20) DEFAULT NULL,
  `statusdesc` varchar(200) DEFAULT NULL,
  `source` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`queid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_quesms
-- ----------------------------
INSERT INTO `tb_quesms` VALUES ('1', '254721557528', 'Andrew testing', '2016-03-12 14:00:12', '0', null, '1', '213870778', null, null);
INSERT INTO `tb_quesms` VALUES ('2', '254723872455', 'Andrew testing', '2016-03-12 14:00:12', '0', null, '0', '213870778', null, null);
INSERT INTO `tb_quesms` VALUES ('3', '254721557528', 'Andrew testing. Demo', '2016-03-12 17:04:59', '0', null, '1', '1451442995', null, null);
INSERT INTO `tb_quesms` VALUES ('4', '254723872455', 'Andrew testing. Demo', '2016-03-12 17:04:59', '0', null, '0', '1451442995', null, null);

-- ----------------------------
-- Table structure for tb_receipts
-- ----------------------------
DROP TABLE IF EXISTS `tb_receipts`;
CREATE TABLE `tb_receipts` (
  `receiptno` bigint(20) NOT NULL AUTO_INCREMENT,
  `valuedate` datetime NOT NULL,
  `trxdescription` varchar(200) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  `isprinted` tinyint(1) DEFAULT '0',
  `isreversed` tinyint(1) NOT NULL DEFAULT '0',
  `reversedby` int(11) DEFAULT NULL,
  `reversedon` datetime DEFAULT NULL,
  PRIMARY KEY (`receiptno`),
  KEY `FK_tb_receipts_tb_users` (`createdby`),
  CONSTRAINT `FK_tb_receipts_tb_users` FOREIGN KEY (`createdby`) REFERENCES `tb_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=819 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_receipts
-- ----------------------------
INSERT INTO `tb_receipts` VALUES ('6', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 13:34:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('7', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 13:48:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('8', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 14:01:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('9', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 14:02:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('10', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 14:08:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('11', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 14:10:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('17', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 14:36:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('18', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 15:06:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('19', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 15:11:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('20', '2016-12-08 00:00:00', '', '2016-12-08 15:32:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('21', '2016-12-08 00:00:00', '21', '2016-12-08 17:00:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('22', '2016-12-08 00:00:00', 'Opening Balance', '2016-12-08 17:04:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('23', '2016-12-08 00:00:00', '', '2016-12-14 13:36:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('24', '2016-12-09 00:00:00', '', '2016-12-14 13:38:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('25', '2016-12-10 00:00:00', '', '2016-12-14 13:39:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('26', '2016-12-11 00:00:00', '', '2016-12-14 13:39:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('27', '2016-12-12 00:00:00', '', '2016-12-14 13:40:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('28', '2016-12-13 00:00:00', '', '2016-12-14 13:41:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('29', '2016-12-14 00:00:00', '', '2016-12-14 13:42:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('30', '2016-12-15 00:00:00', '', '2016-12-14 13:43:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('31', '2016-12-10 00:00:00', '', '2016-12-14 13:51:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('32', '2016-12-11 00:00:00', '', '2016-12-14 13:52:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('33', '2016-12-13 00:00:00', '', '2016-12-14 13:53:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('34', '2016-12-01 00:00:00', '', '2016-12-14 13:55:45', '1', '0', '1', '1', '2017-02-04 10:43:04');
INSERT INTO `tb_receipts` VALUES ('35', '2016-12-03 00:00:00', '', '2016-12-14 13:58:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('36', '2016-12-05 00:00:00', '', '2016-12-14 13:59:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('37', '2016-12-06 00:00:00', '', '2016-12-14 14:00:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('38', '2016-12-09 00:00:00', '', '2016-12-14 14:01:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('39', '2016-12-10 00:00:00', '', '2016-12-14 14:03:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('40', '2016-12-01 00:00:00', '', '2016-12-14 14:06:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('41', '2016-12-03 00:00:00', '', '2016-12-14 14:07:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('42', '2016-12-05 00:00:00', '', '2016-12-14 14:08:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('43', '2016-12-09 00:00:00', '', '2016-12-14 14:14:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('44', '2016-12-10 00:00:00', '', '2016-12-14 14:15:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('45', '2016-12-11 00:00:00', '', '2016-12-14 14:22:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('46', '2016-12-15 00:00:00', '', '2016-12-19 10:13:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('47', '2016-12-08 00:00:00', '', '2016-12-19 10:20:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('48', '2016-12-09 00:00:00', '', '2016-12-19 10:21:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('49', '2016-12-10 00:00:00', '', '2016-12-19 10:21:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('50', '2016-12-11 00:00:00', '', '2016-12-19 10:22:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('51', '2016-12-13 00:00:00', '', '2016-12-19 10:23:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('52', '2016-12-14 00:00:00', '', '2016-12-19 10:24:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('53', '2016-12-16 00:00:00', '', '2016-12-19 10:24:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('54', '2016-12-01 00:00:00', '', '2016-12-19 10:26:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('55', '2016-12-02 00:00:00', '', '2016-12-19 10:32:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('56', '2016-12-03 00:00:00', '', '2016-12-19 10:33:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('57', '2016-12-04 00:00:00', '', '2016-12-19 10:34:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('58', '2016-12-05 00:00:00', '', '2016-12-19 10:34:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('59', '2016-12-08 00:00:00', '', '2016-12-19 10:34:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('60', '2016-12-09 00:00:00', '', '2016-12-19 10:35:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('61', '2016-12-10 00:00:00', '', '2016-12-19 10:37:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('62', '2016-12-11 00:00:00', '', '2016-12-19 10:37:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('63', '2016-12-13 00:00:00', '', '2016-12-19 10:38:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('64', '2016-12-14 00:00:00', '', '2016-12-19 10:38:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('65', '2016-12-16 00:00:00', '', '2016-12-19 10:38:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('66', '2016-12-19 00:00:00', 'Opening Balance', '2016-12-19 13:42:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('67', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 12:39:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('68', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 12:40:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('69', '2016-12-07 00:00:00', '', '2016-12-22 12:47:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('70', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 12:50:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('71', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 12:55:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('72', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 12:59:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('73', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:01:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('74', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:02:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('75', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:04:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('76', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:05:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('77', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:06:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('78', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:08:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('79', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:09:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('80', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:11:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('81', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:13:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('82', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:14:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('83', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:15:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('84', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:16:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('85', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:17:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('86', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:18:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('87', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:24:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('88', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:25:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('89', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:55:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('90', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 13:56:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('91', '2016-12-22 00:00:00', 'Opening Balance', '2016-12-22 14:08:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('92', '2016-12-17 00:00:00', '', '2016-12-29 13:21:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('93', '2016-12-18 00:00:00', '', '2016-12-29 13:21:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('94', '2016-12-19 00:00:00', '', '2016-12-29 13:22:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('95', '2016-12-22 00:00:00', '', '2016-12-29 13:26:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('96', '2016-12-23 00:00:00', '', '2016-12-29 13:27:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('97', '2016-12-24 00:00:00', '', '2016-12-29 13:28:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('98', '2016-12-25 00:00:00', '', '2016-12-29 13:32:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('99', '2016-12-26 00:00:00', '', '2016-12-29 13:33:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('100', '2016-12-26 00:00:00', '', '2016-12-29 13:35:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('101', '2016-12-27 00:00:00', '', '2016-12-29 13:36:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('102', '2016-12-16 00:00:00', '', '2016-12-29 13:43:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('103', '2016-12-17 00:00:00', '', '2016-12-29 13:44:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('104', '2016-12-17 00:00:00', '', '2016-12-29 13:56:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('105', '2016-12-18 00:00:00', '', '2016-12-29 13:57:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('106', '2016-12-19 00:00:00', '', '2016-12-29 13:58:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('107', '2016-12-20 00:00:00', '', '2016-12-29 13:59:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('108', '2016-12-21 00:00:00', '', '2016-12-29 13:59:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('109', '2016-12-29 00:00:00', 'Opening Balance', '2016-12-29 14:07:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('110', '2016-12-15 00:00:00', '', '2016-12-29 14:35:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('111', '2016-12-28 00:00:00', '', '2016-12-29 14:52:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('112', '2016-12-14 00:00:00', '', '2016-12-31 10:19:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('113', '2016-12-21 00:00:00', '', '2016-12-31 10:19:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('114', '2016-12-22 00:00:00', '', '2016-12-31 10:20:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('115', '2016-12-25 00:00:00', '', '2016-12-31 10:20:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('116', '2016-12-26 00:00:00', '', '2016-12-31 10:21:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('117', '2016-12-27 00:00:00', '', '2016-12-31 10:21:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('118', '2016-12-19 00:00:00', '', '2016-12-31 10:26:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('119', '2016-12-20 00:00:00', '', '2016-12-31 10:26:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('120', '2016-12-21 00:00:00', '', '2016-12-31 10:27:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('121', '2016-12-22 00:00:00', '', '2016-12-31 10:27:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('122', '2016-12-27 00:00:00', '', '2016-12-31 10:27:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('123', '2016-12-28 00:00:00', '', '2016-12-31 10:28:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('124', '2016-12-29 00:00:00', '', '2016-12-31 10:28:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('125', '2016-12-01 00:00:00', '', '2016-12-31 10:57:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('126', '2016-12-03 00:00:00', '', '2016-12-31 10:58:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('127', '2016-12-06 00:00:00', '', '2016-12-31 10:58:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('128', '2016-12-07 00:00:00', '', '2016-12-31 10:59:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('129', '2016-12-10 00:00:00', '', '2016-12-31 11:00:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('130', '2016-12-14 00:00:00', '', '2016-12-31 11:01:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('131', '2016-12-16 00:00:00', '', '2016-12-31 11:01:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('132', '2016-12-17 00:00:00', '', '2016-12-31 11:01:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('133', '2016-12-19 00:00:00', '', '2016-12-31 11:01:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('134', '2016-12-21 00:00:00', '', '2016-12-31 11:02:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('135', '2016-12-22 00:00:00', '', '2016-12-31 11:02:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('136', '2016-12-23 00:00:00', '', '2016-12-31 11:02:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('137', '2016-12-01 00:00:00', '', '2016-12-31 11:04:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('138', '2016-12-03 00:00:00', '', '2016-12-31 11:04:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('139', '2016-12-05 00:00:00', '', '2016-12-31 11:05:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('140', '2016-12-05 00:00:00', '', '2016-12-31 11:06:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('141', '2016-12-01 00:00:00', '', '2016-12-31 11:09:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('142', '2016-12-17 00:00:00', '', '2016-12-31 11:10:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('143', '2016-12-18 00:00:00', '', '2016-12-31 11:10:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('144', '2016-12-20 00:00:00', '', '2016-12-31 11:11:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('145', '2016-12-21 00:00:00', '', '2016-12-31 11:11:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('146', '2016-12-22 00:00:00', '', '2016-12-31 11:11:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('147', '2016-12-23 00:00:00', '', '2016-12-31 11:12:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('148', '2016-12-24 00:00:00', '', '2016-12-31 11:12:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('149', '2016-12-25 00:00:00', '', '2016-12-31 11:12:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('150', '2016-12-26 00:00:00', '', '2016-12-31 11:12:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('151', '2016-12-27 00:00:00', '', '2016-12-31 11:13:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('152', '2016-12-28 00:00:00', '', '2016-12-31 11:13:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('153', '2016-12-29 00:00:00', '', '2016-12-31 11:14:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('154', '2016-12-01 00:00:00', '', '2016-12-31 11:18:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('155', '2016-12-02 00:00:00', '', '2016-12-31 11:19:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('156', '2016-12-03 00:00:00', '', '2016-12-31 11:19:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('157', '2016-12-04 00:00:00', '', '2016-12-31 11:19:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('158', '2016-12-07 00:00:00', '', '2016-12-31 11:20:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('159', '2016-12-09 00:00:00', '', '2016-12-31 11:20:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('160', '2016-12-10 00:00:00', '', '2016-12-31 11:20:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('161', '2016-12-11 00:00:00', '', '2016-12-31 11:21:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('162', '2016-12-17 00:00:00', '', '2016-12-31 11:21:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('163', '2016-12-18 00:00:00', '', '2016-12-31 11:21:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('164', '2016-12-19 00:00:00', '', '2016-12-31 11:21:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('165', '2016-12-21 00:00:00', '', '2016-12-31 11:22:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('166', '2016-12-22 00:00:00', '', '2016-12-31 11:22:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('167', '2016-12-25 00:00:00', '', '2016-12-31 11:22:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('168', '2016-12-26 00:00:00', '', '2016-12-31 11:23:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('169', '2016-12-27 00:00:00', '', '2016-12-31 11:23:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('170', '2016-12-02 00:00:00', '', '2016-12-31 11:26:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('171', '2016-12-05 00:00:00', '', '2016-12-31 11:26:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('172', '2016-12-06 00:00:00', '', '2016-12-31 11:27:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('173', '2016-12-07 00:00:00', '', '2016-12-31 11:27:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('174', '2016-12-08 00:00:00', '', '2016-12-31 11:27:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('175', '2016-12-10 00:00:00', '', '2016-12-31 11:27:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('176', '2016-12-13 00:00:00', '', '2016-12-31 11:28:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('177', '2016-12-14 00:00:00', '', '2016-12-31 11:28:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('178', '2016-12-19 00:00:00', '', '2016-12-31 11:28:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('179', '2016-12-20 00:00:00', '', '2016-12-31 11:29:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('180', '2016-12-29 00:00:00', '', '2016-12-31 11:29:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('181', '2016-12-30 00:00:00', '', '2016-12-31 11:29:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('182', '2016-12-22 00:00:00', '', '2016-12-31 11:30:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('183', '2016-12-23 00:00:00', '', '2016-12-31 11:30:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('184', '2016-12-24 00:00:00', '', '2016-12-31 11:31:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('185', '2016-12-26 00:00:00', '', '2016-12-31 11:31:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('186', '2016-12-27 00:00:00', '', '2016-12-31 11:32:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('187', '2016-12-28 00:00:00', '', '2016-12-31 11:32:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('188', '2016-12-03 00:00:00', '', '2016-12-31 11:34:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('189', '2016-12-07 00:00:00', '', '2016-12-31 11:35:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('190', '2016-12-08 00:00:00', '', '2016-12-31 11:37:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('191', '2016-12-10 00:00:00', '', '2016-12-31 11:37:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('192', '2016-12-13 00:00:00', '', '2016-12-31 11:38:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('193', '2016-12-16 00:00:00', '', '2016-12-31 11:38:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('194', '2016-12-17 00:00:00', '', '2016-12-31 11:38:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('195', '2016-12-18 00:00:00', '', '2016-12-31 11:39:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('196', '2016-12-23 00:00:00', '', '2016-12-31 11:39:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('197', '2016-12-24 00:00:00', '', '2016-12-31 11:39:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('198', '2016-12-25 00:00:00', '', '2016-12-31 11:40:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('199', '2016-12-27 00:00:00', '', '2016-12-31 11:40:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('200', '2016-12-28 00:00:00', '', '2016-12-31 11:40:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('201', '2016-12-29 00:00:00', '', '2016-12-31 11:41:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('202', '2016-12-30 00:00:00', '', '2016-12-31 11:41:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('203', '2016-12-05 00:00:00', '', '2016-12-31 11:43:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('204', '2016-12-21 00:00:00', '', '2016-12-31 11:44:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('205', '2016-12-22 00:00:00', '', '2016-12-31 11:44:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('206', '2016-12-10 00:00:00', '', '2016-12-31 11:49:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('207', '2016-12-15 00:00:00', '', '2016-12-31 11:49:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('208', '2016-12-17 00:00:00', '', '2016-12-31 11:50:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('209', '2016-12-19 00:00:00', '', '2016-12-31 11:50:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('210', '2016-12-22 00:00:00', '', '2016-12-31 11:50:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('211', '2016-12-05 00:00:00', '', '2016-12-31 11:51:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('212', '2016-12-08 00:00:00', '', '2016-12-31 11:51:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('213', '2016-12-12 00:00:00', '', '2016-12-31 11:51:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('214', '2016-12-13 00:00:00', '', '2016-12-31 11:52:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('215', '2016-12-26 00:00:00', '', '2016-12-31 11:52:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('216', '2016-12-14 00:00:00', '', '2016-12-31 12:24:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('217', '2016-12-16 00:00:00', '', '2016-12-31 12:24:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('218', '2016-12-17 00:00:00', '', '2016-12-31 12:25:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('219', '2016-12-19 00:00:00', '', '2016-12-31 12:25:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('220', '2016-12-21 00:00:00', '', '2016-12-31 12:25:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('221', '2016-12-23 00:00:00', '', '2016-12-31 12:26:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('222', '2016-12-02 00:00:00', '', '2016-12-31 12:27:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('223', '2016-12-03 00:00:00', '', '2016-12-31 12:27:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('224', '2016-12-01 00:00:00', '', '2016-12-31 12:42:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('225', '2016-12-05 00:00:00', '', '2016-12-31 12:42:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('226', '2016-12-06 00:00:00', '', '2016-12-31 12:43:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('227', '2016-12-09 00:00:00', '', '2016-12-31 12:43:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('228', '2016-12-10 00:00:00', '', '2016-12-31 12:43:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('229', '2016-12-13 00:00:00', '', '2016-12-31 12:43:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('230', '2016-12-15 00:00:00', '', '2016-12-31 12:44:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('231', '2016-12-16 00:00:00', '', '2016-12-31 12:44:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('232', '2016-12-20 00:00:00', '', '2016-12-31 12:44:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('233', '2016-12-21 00:00:00', '', '2016-12-31 12:45:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('234', '2016-12-23 00:00:00', '', '2016-12-31 12:45:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('235', '2016-12-25 00:00:00', '', '2016-12-31 12:45:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('236', '2016-12-27 00:00:00', '', '2016-12-31 12:46:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('237', '2016-12-28 00:00:00', '', '2016-12-31 12:46:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('238', '2016-12-29 00:00:00', '', '2016-12-31 12:47:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('239', '2016-12-01 00:00:00', '', '2016-12-31 12:49:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('240', '2016-12-02 00:00:00', '', '2016-12-31 12:49:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('241', '2016-12-03 00:00:00', '', '2016-12-31 12:50:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('242', '2016-12-05 00:00:00', '', '2016-12-31 12:50:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('243', '2016-12-04 00:00:00', '', '2016-12-31 12:51:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('244', '2016-12-07 00:00:00', '', '2016-12-31 12:52:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('245', '2016-12-09 00:00:00', '', '2016-12-31 12:52:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('246', '2016-12-10 00:00:00', '', '2016-12-31 12:52:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('247', '2016-12-12 00:00:00', '', '2016-12-31 12:53:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('248', '2016-12-13 00:00:00', '', '2016-12-31 12:53:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('249', '2016-12-21 00:00:00', '', '2016-12-31 12:53:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('250', '2016-12-25 00:00:00', '', '2016-12-31 12:54:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('251', '2016-12-26 00:00:00', '', '2016-12-31 12:54:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('252', '2016-12-27 00:00:00', '', '2016-12-31 12:54:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('253', '2016-12-29 00:00:00', '', '2016-12-31 12:54:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('254', '2016-12-01 00:00:00', '', '2016-12-31 12:56:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('255', '2016-12-03 00:00:00', '', '2016-12-31 12:56:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('256', '2016-12-05 00:00:00', '', '2016-12-31 12:57:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('257', '2016-12-08 00:00:00', '', '2016-12-31 12:57:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('258', '2016-12-09 00:00:00', '', '2016-12-31 12:57:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('259', '2016-12-10 00:00:00', '', '2016-12-31 12:57:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('260', '2016-12-12 00:00:00', '', '2016-12-31 12:58:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('261', '2016-12-13 00:00:00', '', '2016-12-31 12:58:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('262', '2016-12-14 00:00:00', '', '2016-12-31 12:58:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('263', '2016-12-15 00:00:00', '', '2016-12-31 12:59:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('264', '2016-12-16 00:00:00', '', '2016-12-31 12:59:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('265', '2016-12-17 00:00:00', '', '2016-12-31 12:59:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('266', '2016-12-19 00:00:00', '', '2016-12-31 13:00:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('267', '2016-12-20 00:00:00', '', '2016-12-31 13:00:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('268', '2016-12-21 00:00:00', '', '2016-12-31 13:00:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('269', '2016-12-22 00:00:00', '', '2016-12-31 13:01:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('270', '2016-12-23 00:00:00', '', '2016-12-31 13:01:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('271', '2016-12-24 00:00:00', '', '2016-12-31 13:01:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('272', '2016-12-27 00:00:00', '', '2016-12-31 13:08:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('273', '2016-12-28 00:00:00', '', '2016-12-31 13:08:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('274', '2016-12-29 00:00:00', '', '2016-12-31 13:08:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('275', '2016-12-30 00:00:00', '', '2016-12-31 13:09:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('276', '2016-12-01 00:00:00', '', '2016-12-31 13:09:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('277', '2016-12-02 00:00:00', '', '2016-12-31 13:10:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('278', '2016-12-05 00:00:00', '', '2016-12-31 13:10:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('279', '2016-12-09 00:00:00', '', '2016-12-31 13:10:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('280', '2016-12-10 00:00:00', '', '2016-12-31 13:10:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('281', '2016-12-14 00:00:00', '', '2016-12-31 13:11:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('282', '2016-12-21 00:00:00', '', '2016-12-31 13:11:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('283', '2016-12-23 00:00:00', '', '2016-12-31 13:11:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('284', '2016-12-24 00:00:00', '', '2016-12-31 13:12:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('285', '2016-12-27 00:00:00', '', '2016-12-31 13:12:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('286', '2016-12-28 00:00:00', '', '2016-12-31 13:12:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('287', '2016-12-29 00:00:00', '', '2016-12-31 13:13:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('288', '2016-12-30 00:00:00', '', '2016-12-31 13:13:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('289', '2016-12-02 00:00:00', '', '2016-12-31 13:20:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('290', '2016-12-09 00:00:00', '', '2016-12-31 13:20:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('291', '2016-12-12 00:00:00', '', '2016-12-31 13:21:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('292', '2016-12-14 00:00:00', '', '2016-12-31 13:21:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('293', '2016-12-15 00:00:00', '', '2016-12-31 13:21:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('294', '2016-12-19 00:00:00', '', '2016-12-31 13:21:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('295', '2016-12-21 00:00:00', '', '2016-12-31 13:22:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('296', '2016-12-03 00:00:00', '', '2016-12-31 13:27:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('297', '2016-12-07 00:00:00', '', '2016-12-31 13:27:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('298', '2016-12-08 00:00:00', '', '2016-12-31 13:27:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('299', '2016-12-10 00:00:00', '', '2016-12-31 13:28:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('300', '2016-12-11 00:00:00', '', '2016-12-31 13:28:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('301', '2016-12-13 00:00:00', '', '2016-12-31 13:28:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('302', '2016-12-14 00:00:00', '', '2016-12-31 13:28:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('303', '2016-12-22 00:00:00', '', '2016-12-31 13:29:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('304', '2016-12-23 00:00:00', '', '2016-12-31 13:29:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('305', '2016-12-24 00:00:00', '', '2016-12-31 13:29:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('306', '2016-12-25 00:00:00', '', '2016-12-31 13:30:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('307', '2016-12-26 00:00:00', '', '2016-12-31 13:30:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('308', '2016-12-28 00:00:00', '', '2016-12-31 13:31:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('309', '2016-12-05 00:00:00', '', '2016-12-31 13:33:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('310', '2016-12-07 00:00:00', '', '2016-12-31 13:33:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('311', '2016-12-09 00:00:00', '', '2016-12-31 13:34:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('312', '2016-12-01 00:00:00', '', '2016-12-31 13:35:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('313', '2016-12-03 00:00:00', '', '2016-12-31 13:35:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('314', '2016-12-04 00:00:00', '', '2016-12-31 13:35:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('315', '2016-12-05 00:00:00', '', '2016-12-31 13:36:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('316', '2016-12-06 00:00:00', '', '2016-12-31 13:36:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('317', '2016-12-08 00:00:00', '', '2016-12-31 13:36:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('318', '2016-12-12 00:00:00', '', '2016-12-31 13:37:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('319', '2016-12-16 00:00:00', '', '2016-12-31 13:37:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('320', '2016-12-17 00:00:00', '', '2016-12-31 13:37:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('321', '2016-12-19 00:00:00', '', '2016-12-31 13:38:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('322', '2016-12-20 00:00:00', '', '2016-12-31 13:38:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('323', '2016-12-21 00:00:00', '', '2016-12-31 13:38:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('324', '2016-12-22 00:00:00', '', '2016-12-31 13:39:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('325', '2016-12-23 00:00:00', '', '2016-12-31 13:39:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('326', '2016-12-24 00:00:00', '', '2016-12-31 13:39:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('327', '2016-12-25 00:00:00', '', '2016-12-31 13:40:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('328', '2016-12-26 00:00:00', '', '2016-12-31 13:40:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('329', '2016-12-27 00:00:00', '', '2016-12-31 13:40:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('330', '2016-12-28 00:00:00', '', '2016-12-31 13:41:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('331', '2016-12-04 00:00:00', '', '2016-12-31 13:44:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('332', '2016-12-05 00:00:00', '', '2016-12-31 13:44:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('333', '2016-12-07 00:00:00', '', '2016-12-31 13:45:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('334', '2016-12-09 00:00:00', '', '2016-12-31 13:46:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('335', '2016-12-11 00:00:00', '', '2016-12-31 13:46:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('336', '2016-12-12 00:00:00', '', '2016-12-31 13:46:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('337', '2016-12-13 00:00:00', '', '2016-12-31 13:47:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('338', '2016-12-19 00:00:00', '', '2016-12-31 13:47:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('339', '2016-12-22 00:00:00', '', '2016-12-31 13:48:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('340', '2016-12-23 00:00:00', '', '2016-12-31 13:48:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('341', '2016-12-25 00:00:00', '', '2016-12-31 13:48:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('342', '2016-12-26 00:00:00', '', '2016-12-31 13:48:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('343', '2016-12-28 00:00:00', '', '2016-12-31 13:49:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('344', '2016-12-29 00:00:00', '', '2016-12-31 13:49:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('345', '2016-12-02 00:00:00', '', '2016-12-31 13:52:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('346', '2016-12-05 00:00:00', '', '2016-12-31 13:52:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('347', '2016-12-06 00:00:00', '', '2016-12-31 13:52:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('348', '2016-12-07 00:00:00', '', '2016-12-31 13:53:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('349', '2016-12-08 00:00:00', '', '2016-12-31 13:53:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('350', '2016-12-13 00:00:00', '', '2016-12-31 13:54:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('351', '2016-12-14 00:00:00', '', '2016-12-31 13:54:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('352', '2016-12-15 00:00:00', '', '2016-12-31 14:00:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('353', '2016-12-17 00:00:00', '', '2016-12-31 14:00:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('354', '2016-12-20 00:00:00', '', '2016-12-31 14:01:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('355', '2016-12-21 00:00:00', '', '2016-12-31 14:02:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('356', '2016-12-22 00:00:00', '', '2016-12-31 14:02:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('357', '2016-12-30 00:00:00', '', '2016-12-31 14:02:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('358', '2016-12-02 00:00:00', '', '2016-12-31 14:04:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('359', '2016-12-06 00:00:00', '', '2016-12-31 14:12:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('360', '2016-12-07 00:00:00', '', '2016-12-31 14:13:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('361', '2016-12-08 00:00:00', '', '2016-12-31 14:13:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('362', '2016-12-09 00:00:00', '', '2016-12-31 14:14:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('363', '2016-12-12 00:00:00', '', '2016-12-31 14:14:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('364', '2016-12-15 00:00:00', '', '2016-12-31 14:15:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('365', '2016-12-17 00:00:00', '', '2017-01-02 10:28:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('366', '2016-12-21 00:00:00', '', '2017-01-02 10:28:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('367', '2016-12-22 00:00:00', '', '2017-01-02 10:29:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('368', '2016-12-26 00:00:00', '', '2017-01-02 10:29:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('369', '2016-12-29 00:00:00', '', '2017-01-02 10:30:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('370', '2017-01-30 00:00:00', '', '2017-01-02 10:30:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('371', '2016-12-01 00:00:00', '', '2017-01-02 10:31:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('372', '2016-12-10 00:00:00', '', '2017-01-02 10:31:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('373', '2016-12-16 00:00:00', '', '2017-01-02 10:32:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('374', '2016-12-19 00:00:00', '', '2017-01-02 10:32:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('375', '2016-12-02 00:00:00', '', '2017-01-02 10:33:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('376', '2016-12-05 00:00:00', '', '2017-01-02 10:33:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('377', '2016-12-07 00:00:00', '', '2017-01-02 10:33:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('378', '2016-12-13 00:00:00', '', '2017-01-02 10:34:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('379', '2016-12-16 00:00:00', '', '2017-01-02 10:34:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('380', '2016-12-17 00:00:00', '', '2017-01-02 10:34:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('381', '2016-12-20 00:00:00', '', '2017-01-02 10:34:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('382', '2016-12-22 00:00:00', '', '2017-01-02 10:35:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('383', '2016-12-23 00:00:00', '', '2017-01-02 10:35:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('384', '2016-12-24 00:00:00', '', '2017-01-02 10:35:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('385', '2016-12-27 00:00:00', '', '2017-01-02 10:36:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('386', '2016-12-29 00:00:00', '', '2017-01-02 10:36:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('387', '2016-12-30 00:00:00', '', '2017-01-02 10:36:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('388', '2016-12-01 00:00:00', '', '2017-01-02 10:37:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('389', '2016-12-02 00:00:00', '', '2017-01-02 10:37:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('390', '2016-12-09 00:00:00', '', '2017-01-02 10:38:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('391', '2016-12-10 00:00:00', '', '2017-01-02 10:38:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('392', '2016-12-13 00:00:00', '', '2017-01-02 10:38:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('393', '2016-12-15 00:00:00', '', '2017-01-02 10:39:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('394', '2016-12-18 00:00:00', '', '2017-01-02 10:39:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('395', '2016-12-20 00:00:00', '', '2017-01-02 10:39:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('396', '2016-12-22 00:00:00', '', '2017-01-02 10:40:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('397', '2016-12-23 00:00:00', '', '2017-01-02 10:40:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('398', '2016-12-24 00:00:00', '', '2017-01-02 10:40:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('399', '2016-12-25 00:00:00', '', '2017-01-02 10:41:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('400', '2016-12-26 00:00:00', '', '2017-01-02 10:41:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('401', '2016-12-28 00:00:00', '', '2017-01-02 10:42:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('402', '2016-12-01 00:00:00', '', '2017-01-02 10:51:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('403', '2016-12-02 00:00:00', '', '2017-01-02 10:51:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('404', '2016-12-03 00:00:00', '', '2017-01-02 10:51:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('405', '2016-12-08 00:00:00', '', '2017-01-02 10:52:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('406', '2016-12-09 00:00:00', '', '2017-01-02 10:52:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('407', '2016-12-13 00:00:00', '', '2017-01-02 10:52:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('408', '2016-12-14 00:00:00', '', '2017-01-02 10:53:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('409', '2016-12-15 00:00:00', '', '2017-01-02 10:53:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('410', '2016-12-16 00:00:00', '', '2017-01-02 10:53:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('411', '2016-12-19 00:00:00', '', '2017-01-02 10:54:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('412', '2016-12-20 00:00:00', '', '2017-01-02 10:54:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('413', '2016-12-21 00:00:00', '', '2017-01-02 10:54:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('414', '2016-12-22 00:00:00', '', '2017-01-02 10:55:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('415', '2016-12-23 00:00:00', '', '2017-01-02 10:55:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('416', '2016-12-24 00:00:00', '', '2017-01-02 10:55:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('417', '2016-12-26 00:00:00', '', '2017-01-02 10:56:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('418', '2016-12-28 00:00:00', '', '2017-01-02 10:56:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('419', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:05:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('420', '2017-01-01 00:00:00', '', '2017-01-02 11:10:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('421', '2017-01-02 00:00:00', '', '2017-01-02 11:11:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('422', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:16:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('423', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:18:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('424', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:19:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('425', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:20:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('426', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:21:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('427', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:22:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('428', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:24:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('429', '2017-01-02 00:00:00', 'Opening Balance', '2017-01-02 11:28:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('430', '2016-12-22 00:00:00', '', '2017-01-04 13:13:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('431', '2016-12-23 00:00:00', '', '2017-01-04 13:14:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('432', '2016-12-24 00:00:00', '', '2017-01-04 13:14:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('433', '2016-12-26 00:00:00', '', '2017-01-04 13:15:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('434', '2016-12-27 00:00:00', '', '2017-01-04 13:15:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('435', '2016-12-28 00:00:00', '', '2017-01-04 13:16:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('436', '2016-12-25 00:00:00', '', '2017-01-04 13:18:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('437', '2017-01-01 00:00:00', '', '2017-01-04 13:19:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('438', '2017-01-02 00:00:00', '', '2017-01-04 13:20:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('439', '2016-12-29 00:00:00', '', '2017-01-04 13:21:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('440', '2016-12-31 00:00:00', '', '2017-01-04 13:22:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('441', '2017-01-02 00:00:00', '', '2017-01-04 13:22:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('442', '2016-12-29 00:00:00', '', '2017-01-04 13:24:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('443', '2016-12-30 00:00:00', '', '2017-01-04 13:24:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('444', '2016-12-31 00:00:00', '', '2017-01-04 13:25:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('445', '2017-01-01 00:00:00', '', '2017-01-04 13:26:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('446', '2017-01-02 00:00:00', '', '2017-01-04 13:26:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('447', '2016-12-29 00:00:00', '', '2017-01-04 13:32:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('448', '2016-12-30 00:00:00', '', '2017-01-04 13:33:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('449', '2016-12-31 00:00:00', '', '2017-01-04 13:34:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('450', '2017-01-02 00:00:00', '', '2017-01-04 13:35:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('451', '2017-01-03 00:00:00', '', '2017-01-04 13:35:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('452', '2016-12-29 00:00:00', '', '2017-01-04 13:38:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('453', '2016-12-31 00:00:00', '', '2017-01-04 13:38:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('454', '2016-12-23 00:00:00', '', '2017-01-04 13:41:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('455', '2016-12-24 00:00:00', '', '2017-01-04 13:42:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('456', '2016-12-25 00:00:00', '', '2017-01-04 13:45:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('457', '2016-12-26 00:00:00', '', '2017-01-04 13:46:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('458', '2016-12-27 00:00:00', '', '2017-01-05 14:13:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('459', '2016-12-28 00:00:00', '', '2017-01-05 14:14:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('460', '2016-12-30 00:00:00', '', '2017-01-05 14:15:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('461', '2016-12-31 00:00:00', '', '2017-01-05 14:15:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('462', '2017-01-01 00:00:00', '', '2017-01-05 14:16:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('463', '2017-01-02 00:00:00', '', '2017-01-05 14:17:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('464', '2017-01-03 00:00:00', '', '2017-01-05 14:17:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('465', '2017-01-04 00:00:00', '', '2017-01-05 14:18:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('466', '2017-01-04 00:00:00', '', '2017-01-05 14:23:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('467', '2017-01-03 00:00:00', '', '2017-01-05 14:28:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('468', '2017-01-04 00:00:00', '', '2017-01-05 14:29:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('469', '2016-12-23 00:00:00', '', '2017-01-05 14:32:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('470', '2016-12-28 00:00:00', '', '2017-01-05 14:33:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('471', '2016-12-29 00:00:00', '', '2017-01-05 14:34:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('472', '2016-12-30 00:00:00', '', '2017-01-05 14:35:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('473', '2016-12-31 00:00:00', '', '2017-01-05 14:35:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('474', '2017-01-02 00:00:00', '', '2017-01-05 14:36:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('475', '2017-01-03 00:00:00', '', '2017-01-05 14:37:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('476', '2017-01-03 00:00:00', '', '2017-01-05 14:40:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('477', '2016-12-23 00:00:00', '', '2017-01-05 14:43:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('478', '2016-12-25 00:00:00', '', '2017-01-05 14:44:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('479', '2016-12-26 00:00:00', '', '2017-01-05 14:45:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('480', '2016-12-27 00:00:00', '', '2017-01-05 14:46:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('481', '2016-12-28 00:00:00', '', '2017-01-05 14:46:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('482', '2016-12-29 00:00:00', '', '2017-01-05 14:47:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('483', '2017-01-02 00:00:00', '', '2017-01-05 14:48:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('484', '2017-01-03 00:00:00', '', '2017-01-05 14:49:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('485', '2017-01-04 00:00:00', '', '2017-01-05 14:49:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('486', '2016-12-23 00:00:00', '', '2017-01-05 14:58:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('487', '2016-12-26 00:00:00', '', '2017-01-05 14:59:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('488', '2017-01-03 00:00:00', '', '2017-01-05 15:00:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('489', '2016-12-23 00:00:00', '', '2017-01-05 15:04:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('490', '2016-12-24 00:00:00', '', '2017-01-05 15:05:53', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('491', '2016-12-25 00:00:00', '', '2017-01-05 15:06:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('492', '2016-12-26 00:00:00', '', '2017-01-05 15:07:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('493', '2016-12-27 00:00:00', '', '2017-01-05 15:07:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('494', '2016-12-28 00:00:00', '', '2017-01-05 15:08:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('495', '2016-12-29 00:00:00', '', '2017-01-05 15:09:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('496', '2016-12-30 00:00:00', '', '2017-01-05 15:10:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('497', '2016-12-31 00:00:00', '', '2017-01-05 15:10:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('498', '2017-01-01 00:00:00', '', '2017-01-05 15:11:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('499', '2017-01-02 00:00:00', '', '2017-01-05 15:12:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('500', '2017-01-03 00:00:00', '', '2017-01-05 15:12:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('501', '2017-01-04 00:00:00', '', '2017-01-05 15:13:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('502', '2017-01-05 00:00:00', 'Opening Balance', '2017-01-05 15:21:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('503', '2016-12-30 00:00:00', '', '2017-01-11 11:54:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('504', '2016-12-31 00:00:00', '', '2017-01-11 11:54:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('505', '2017-01-01 00:00:00', '', '2017-01-11 11:54:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('506', '2017-01-03 00:00:00', '', '2017-01-11 11:55:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('507', '2017-01-04 00:00:00', '', '2017-01-11 11:55:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('508', '2017-01-05 00:00:00', '', '2017-01-11 11:55:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('509', '2017-01-06 00:00:00', '', '2017-01-11 11:56:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('510', '2017-01-07 00:00:00', '', '2017-01-11 12:00:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('511', '2017-01-08 00:00:00', '', '2017-01-11 12:00:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('512', '2017-01-09 00:00:00', '', '2017-01-11 12:02:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('513', '2016-12-30 00:00:00', '', '2017-01-11 12:31:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('514', '2016-12-31 00:00:00', '', '2017-01-11 12:32:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('515', '2017-01-01 00:00:00', '', '2017-01-11 12:32:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('516', '2017-01-02 00:00:00', '', '2017-01-11 12:32:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('517', '2017-01-03 00:00:00', '', '2017-01-11 12:34:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('518', '2017-01-04 00:00:00', '', '2017-01-11 12:34:23', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('519', '2017-01-05 00:00:00', '', '2017-01-11 12:34:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('520', '2017-01-06 00:00:00', '', '2017-01-11 12:34:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('521', '2017-01-07 00:00:00', '', '2017-01-11 12:35:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('522', '2017-01-08 00:00:00', '', '2017-01-11 12:35:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('523', '2017-01-09 00:00:00', '', '2017-01-11 12:35:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('524', '2017-01-03 00:00:00', '', '2017-01-11 12:36:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('525', '2017-01-04 00:00:00', '', '2017-01-11 12:37:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('526', '2017-01-06 00:00:00', '', '2017-01-11 12:37:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('527', '2017-01-09 00:00:00', '', '2017-01-11 12:37:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('528', '2017-01-04 00:00:00', '', '2017-01-11 12:38:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('529', '2017-01-06 00:00:00', '', '2017-01-11 12:39:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('530', '2016-12-30 00:00:00', '', '2017-01-11 12:40:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('531', '2016-12-31 00:00:00', '', '2017-01-11 12:40:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('532', '2017-01-01 00:00:00', '', '2017-01-11 12:40:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('533', '2017-01-02 00:00:00', '', '2017-01-11 12:41:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('534', '2017-01-03 00:00:00', '', '2017-01-11 12:41:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('535', '2017-01-04 00:00:00', '', '2017-01-11 12:41:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('536', '2017-01-05 00:00:00', '', '2017-01-11 12:41:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('537', '2017-01-06 00:00:00', '', '2017-01-11 12:42:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('538', '2017-01-07 00:00:00', '', '2017-01-11 12:42:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('539', '2017-01-08 00:00:00', '', '2017-01-11 12:42:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('540', '2017-01-09 00:00:00', '', '2017-01-11 12:43:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('541', '2016-12-21 00:00:00', '', '2017-01-12 10:24:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('542', '2016-12-22 00:00:00', '', '2017-01-12 10:24:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('543', '2016-12-25 00:00:00', '', '2017-01-12 10:24:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('544', '2016-12-26 00:00:00', '', '2017-01-12 10:25:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('545', '2016-12-27 00:00:00', '', '2017-01-12 10:25:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('546', '2017-01-02 00:00:00', '', '2017-01-12 10:25:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('547', '2017-01-03 00:00:00', '', '2017-01-12 10:26:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('548', '2017-01-05 00:00:00', '', '2017-01-12 10:26:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('549', '2017-01-07 00:00:00', '', '2017-01-12 10:27:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('550', '2017-01-09 00:00:00', '', '2017-01-12 10:27:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('551', '2016-12-31 00:00:00', '', '2017-01-12 10:34:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('552', '2017-01-02 00:00:00', '', '2017-01-12 10:34:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('553', '2017-01-06 00:00:00', '', '2017-01-12 10:35:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('554', '2017-01-07 00:00:00', '', '2017-01-12 10:35:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('555', '2017-01-09 00:00:00', '', '2017-01-12 10:35:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('556', '2016-12-31 00:00:00', '', '2017-01-12 10:37:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('557', '2017-01-01 00:00:00', '', '2017-01-12 10:37:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('558', '2017-01-04 00:00:00', '', '2017-01-12 10:38:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('559', '2017-01-05 00:00:00', '', '2017-01-12 10:38:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('560', '2017-01-07 00:00:00', '', '2017-01-12 10:39:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('561', '2017-01-10 00:00:00', '', '2017-01-12 10:39:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('562', '2016-12-31 00:00:00', '', '2017-01-12 10:43:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('563', '2017-01-02 00:00:00', '', '2017-01-12 10:44:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('564', '2017-01-03 00:00:00', '', '2017-01-12 10:44:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('565', '2017-01-04 00:00:00', '', '2017-01-12 10:44:39', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('566', '2017-01-05 00:00:00', '', '2017-01-12 10:44:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('567', '2017-01-06 00:00:00', '', '2017-01-12 10:45:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('568', '2017-01-09 00:00:00', '', '2017-01-12 10:45:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('569', '2017-01-10 00:00:00', '', '2017-01-12 10:45:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('570', '2017-01-03 00:00:00', '', '2017-01-12 10:48:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('571', '2017-01-02 00:00:00', '', '2017-01-12 10:49:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('572', '2017-01-03 00:00:00', '', '2017-01-12 10:49:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('573', '2017-01-06 00:00:00', '', '2017-01-12 10:50:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('574', '2016-12-30 00:00:00', '', '2017-01-12 10:53:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('575', '2016-12-31 00:00:00', '', '2017-01-12 10:53:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('576', '2017-01-05 00:00:00', '', '2017-01-12 10:54:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('577', '2017-01-06 00:00:00', '', '2017-01-12 10:54:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('578', '2017-01-08 00:00:00', '', '2017-01-12 10:54:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('579', '2016-12-31 00:00:00', '', '2017-01-12 10:56:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('580', '2017-01-04 00:00:00', '', '2017-01-12 10:56:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('581', '2017-01-05 00:00:00', '', '2017-01-12 10:57:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('582', '2017-01-07 00:00:00', '', '2017-01-12 10:57:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('583', '2017-01-09 00:00:00', '', '2017-01-12 10:58:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('584', '2016-12-31 00:00:00', '', '2017-01-12 10:59:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('585', '2017-01-07 00:00:00', '', '2017-01-12 10:59:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('586', '2017-01-09 00:00:00', '', '2017-01-12 11:00:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('587', '2017-01-04 00:00:00', '', '2017-01-12 11:03:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('588', '2017-01-05 00:00:00', '', '2017-01-12 11:04:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('589', '2017-01-07 00:00:00', '', '2017-01-12 11:04:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('590', '2016-12-30 00:00:00', '', '2017-01-12 11:10:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('591', '2017-01-09 00:00:00', '', '2017-01-12 11:14:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('592', '2017-01-10 00:00:00', '', '2017-01-12 11:14:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('593', '2016-12-30 00:00:00', '', '2017-01-12 11:16:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('594', '2016-12-31 00:00:00', '', '2017-01-12 11:16:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('595', '2017-01-01 00:00:00', '', '2017-01-12 11:17:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('596', '2017-01-02 00:00:00', '', '2017-01-12 11:17:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('597', '2017-01-03 00:00:00', '', '2017-01-12 11:17:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('598', '2017-01-04 00:00:00', '', '2017-01-12 11:18:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('599', '2017-01-05 00:00:00', '', '2017-01-12 11:18:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('600', '2017-01-06 00:00:00', '', '2017-01-12 11:18:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('601', '2017-01-07 00:00:00', '', '2017-01-12 11:18:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('602', '2017-01-13 00:00:00', '', '2017-01-16 11:14:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('603', '2017-01-08 00:00:00', '', '2017-01-16 11:23:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('604', '2017-01-09 00:00:00', '', '2017-01-16 11:23:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('605', '2017-01-11 00:00:00', '', '2017-01-16 11:24:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('606', '2017-01-31 00:00:00', '', '2017-01-16 11:26:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('607', '2017-01-01 00:00:00', '', '2017-01-16 11:26:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('608', '2017-01-02 00:00:00', '', '2017-01-16 11:26:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('609', '2017-01-03 00:00:00', '', '2017-01-16 11:26:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('610', '2017-01-04 00:00:00', '', '2017-01-16 11:27:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('611', '2017-01-05 00:00:00', '', '2017-01-16 11:27:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('612', '2017-01-06 00:00:00', '', '2017-01-16 11:27:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('613', '2017-01-07 00:00:00', '', '2017-01-16 11:28:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('614', '2017-01-08 00:00:00', '', '2017-01-16 11:28:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('615', '2017-01-09 00:00:00', '', '2017-01-16 11:29:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('616', '2017-01-11 00:00:00', '', '2017-01-16 11:29:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('617', '2016-12-31 00:00:00', '', '2017-01-16 11:30:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('618', '2017-01-02 00:00:00', '', '2017-01-16 11:31:10', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('619', '2017-01-03 00:00:00', '', '2017-01-16 11:31:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('620', '2017-01-04 00:00:00', '', '2017-01-16 11:31:42', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('621', '2017-01-05 00:00:00', '', '2017-01-16 11:31:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('622', '2017-01-06 00:00:00', '', '2017-01-16 11:32:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('623', '2017-01-07 00:00:00', '', '2017-01-16 11:32:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('624', '2017-01-09 00:00:00', '', '2017-01-16 11:32:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('625', '2017-01-10 00:00:00', '', '2017-01-16 11:33:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('626', '2017-01-13 00:00:00', '', '2017-01-16 11:33:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('627', '2017-01-14 00:00:00', '', '2017-01-16 11:33:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('628', '2017-01-02 00:00:00', '', '2017-01-17 12:22:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('629', '2017-01-03 00:00:00', '', '2017-01-17 12:22:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('630', '2017-01-04 00:00:00', '', '2017-01-17 12:23:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('631', '2017-01-06 00:00:00', '', '2017-01-17 12:23:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('632', '2017-01-09 00:00:00', '', '2017-01-17 12:23:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('633', '2017-01-11 00:00:00', '', '2017-01-17 12:24:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('634', '2017-01-12 00:00:00', '', '2017-01-17 12:26:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('635', '2017-01-13 00:00:00', '', '2017-01-17 12:26:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('636', '2017-01-14 00:00:00', '', '2017-01-17 12:27:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('637', '2017-01-12 00:00:00', '', '2017-01-17 12:31:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('638', '2017-01-03 00:00:00', '', '2017-01-17 12:33:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('639', '2017-01-04 00:00:00', '', '2017-01-17 12:33:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('640', '2017-01-06 00:00:00', '', '2017-01-17 12:33:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('641', '2017-01-11 00:00:00', '', '2017-01-17 12:34:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('642', '2017-01-13 00:00:00', '', '2017-01-17 12:35:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('643', '2017-01-14 00:00:00', '', '2017-01-17 12:35:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('644', '2016-12-31 00:00:00', '', '2017-01-17 12:36:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('645', '2017-01-01 00:00:00', '', '2017-01-17 12:37:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('646', '2017-01-02 00:00:00', '', '2017-01-17 12:37:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('647', '2017-01-03 00:00:00', '', '2017-01-17 12:37:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('648', '2017-01-04 00:00:00', '', '2017-01-17 12:38:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('649', '2017-01-05 00:00:00', '', '2017-01-17 12:38:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('650', '2017-01-06 00:00:00', '', '2017-01-17 12:38:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('651', '2017-01-11 00:00:00', '', '2017-01-17 12:38:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('652', '2017-01-13 00:00:00', '', '2017-01-17 12:39:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('653', '2017-01-14 00:00:00', '', '2017-01-17 12:39:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('654', '2017-01-15 00:00:00', '', '2017-01-17 12:39:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('655', '2017-01-16 00:00:00', '', '2017-01-17 12:40:05', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('656', '2016-12-30 00:00:00', '', '2017-01-17 12:42:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('657', '2016-12-31 00:00:00', '', '2017-01-17 12:42:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('658', '2017-01-01 00:00:00', '', '2017-01-17 12:42:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('659', '2017-01-02 00:00:00', '', '2017-01-17 12:43:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('660', '2017-01-03 00:00:00', '', '2017-01-17 12:43:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('661', '2017-01-04 00:00:00', '', '2017-01-17 12:43:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('662', '2017-01-05 00:00:00', '', '2017-01-17 12:43:50', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('663', '2017-01-06 00:00:00', '', '2017-01-17 12:44:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('664', '2017-01-07 00:00:00', '', '2017-01-17 12:44:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('665', '2017-01-09 00:00:00', '', '2017-01-17 12:44:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('666', '2017-01-10 00:00:00', '', '2017-01-17 12:44:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('667', '2017-01-11 00:00:00', '', '2017-01-17 12:45:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('668', '2017-01-12 00:00:00', '', '2017-01-17 12:45:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('669', '2017-01-13 00:00:00', '', '2017-01-17 12:45:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('670', '2017-01-14 00:00:00', '', '2017-01-17 12:45:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('671', '2017-01-09 00:00:00', '', '2017-01-17 12:50:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('672', '2017-01-10 00:00:00', '', '2017-01-17 12:50:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('673', '2017-01-12 00:00:00', '', '2017-01-17 12:50:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('674', '2017-01-13 00:00:00', '', '2017-01-17 12:51:02', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('675', '2017-01-14 00:00:00', '', '2017-01-17 12:51:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('676', '2017-01-15 00:00:00', '', '2017-01-17 12:51:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('677', '2017-01-16 00:00:00', '', '2017-01-17 12:51:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('678', '2017-01-10 00:00:00', '', '2017-01-17 12:53:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('679', '2017-01-11 00:00:00', '', '2017-01-17 12:53:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('680', '2017-01-12 00:00:00', '', '2017-01-17 12:54:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('681', '2017-01-13 00:00:00', '', '2017-01-17 13:09:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('682', '2017-01-14 00:00:00', '', '2017-01-17 13:09:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('683', '2017-01-15 00:00:00', '', '2017-01-17 13:10:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('684', '2017-01-16 00:00:00', '', '2017-01-17 13:10:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('685', '2017-01-11 00:00:00', '', '2017-01-17 13:12:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('686', '2017-01-13 00:00:00', '', '2017-01-17 13:13:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('687', '2017-01-10 00:00:00', '', '2017-01-17 13:18:31', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('688', '2017-01-11 00:00:00', '', '2017-01-17 13:18:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('689', '2017-01-12 00:00:00', '', '2017-01-17 13:19:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('690', '2017-01-13 00:00:00', '', '2017-01-17 13:19:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('691', '2017-01-14 00:00:00', '', '2017-01-17 13:19:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('692', '2017-01-15 00:00:00', '', '2017-01-17 13:20:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('693', '2017-01-16 00:00:00', '', '2017-01-17 13:20:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('694', '2017-01-10 00:00:00', '', '2017-01-17 13:24:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('695', '2017-01-11 00:00:00', '', '2017-01-17 13:24:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('696', '2017-01-13 00:00:00', '', '2017-01-17 13:25:17', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('697', '2017-01-14 00:00:00', '', '2017-01-17 13:25:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('698', '2017-01-02 00:00:00', '', '2017-01-17 15:05:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('699', '2017-01-03 00:00:00', '', '2017-01-17 15:06:00', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('700', '2017-01-05 00:00:00', '', '2017-01-17 15:06:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('701', '2016-12-28 00:00:00', '', '2017-01-24 11:00:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('702', '2017-01-01 00:00:00', '', '2017-01-24 11:01:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('703', '2017-01-02 00:00:00', '', '2017-01-24 11:02:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('704', '2017-01-03 00:00:00', '', '2017-01-24 11:03:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('705', '2017-01-04 00:00:00', '', '2017-01-24 11:04:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('706', '2017-01-05 00:00:00', '', '2017-01-24 11:05:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('707', '2017-01-06 00:00:00', '', '2017-01-24 11:08:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('708', '2017-01-07 00:00:00', '', '2017-01-24 11:09:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('709', '2017-01-08 00:00:00', '', '2017-01-24 11:10:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('710', '2017-01-09 00:00:00', '', '2017-01-24 11:11:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('711', '2017-01-10 00:00:00', '', '2017-01-24 11:12:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('712', '2017-01-11 00:00:00', '', '2017-01-24 11:14:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('713', '2017-01-12 00:00:00', '', '2017-01-24 11:15:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('714', '2017-01-13 00:00:00', '', '2017-01-24 11:16:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('715', '2017-01-14 00:00:00', '', '2017-01-24 11:17:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('716', '2017-01-15 00:00:00', '', '2017-01-24 11:18:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('717', '2017-01-16 00:00:00', '', '2017-01-24 11:19:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('718', '2017-01-15 00:00:00', '', '2017-01-24 11:22:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('719', '2017-01-16 00:00:00', '', '2017-01-24 11:24:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('720', '2017-01-17 00:00:00', '', '2017-01-24 11:25:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('721', '2016-12-31 00:00:00', '', '2017-01-24 11:34:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('722', '2017-01-02 00:00:00', '', '2017-01-24 11:35:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('723', '2017-01-03 00:00:00', '', '2017-01-24 11:36:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('724', '2017-01-04 00:00:00', '', '2017-01-24 11:37:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('725', '2017-01-05 00:00:00', '', '2017-01-24 11:45:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('726', '2017-01-07 00:00:00', '', '2017-01-24 11:46:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('727', '2017-01-08 00:00:00', '', '2017-01-24 11:47:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('728', '2017-01-09 00:00:00', '', '2017-01-24 11:48:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('729', '2017-01-10 00:00:00', '', '2017-01-24 11:49:51', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('730', '2017-01-11 00:00:00', '', '2017-01-24 11:50:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('731', '2017-01-12 00:00:00', '', '2017-01-24 11:51:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('732', '2017-01-13 00:00:00', '', '2017-01-24 11:52:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('733', '2017-01-14 00:00:00', '', '2017-01-24 11:53:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('734', '2017-01-15 00:00:00', '', '2017-01-24 11:54:49', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('735', '2017-01-16 00:00:00', '', '2017-01-24 11:55:47', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('736', '2017-01-17 00:00:00', '', '2017-01-24 11:56:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('737', '2017-01-05 00:00:00', '', '2017-01-24 12:01:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('738', '2017-01-06 00:00:00', '', '2017-01-24 12:02:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('739', '2017-01-07 00:00:00', '', '2017-01-24 12:03:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('740', '2017-01-09 00:00:00', '', '2017-01-24 12:04:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('741', '2017-01-10 00:00:00', '', '2017-01-24 12:06:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('742', '2017-01-11 00:00:00', '', '2017-01-24 12:07:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('743', '2017-01-12 00:00:00', '', '2017-01-24 12:08:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('744', '2017-01-13 00:00:00', '', '2017-01-24 12:09:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('745', '2017-01-14 00:00:00', '', '2017-01-24 12:10:28', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('746', '2017-01-16 00:00:00', '', '2017-01-24 12:11:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('747', '2017-01-17 00:00:00', '', '2017-01-24 12:12:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('748', '2017-01-18 00:00:00', '', '2017-01-24 12:13:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('749', '2017-01-19 00:00:00', '', '2017-01-24 12:14:11', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('750', '2017-01-20 00:00:00', '', '2017-01-24 12:14:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('751', '2017-01-21 00:00:00', '', '2017-01-24 12:15:52', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('752', '2017-01-05 00:00:00', '', '2017-01-24 13:18:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('753', '2017-01-07 00:00:00', '', '2017-01-24 13:19:34', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('754', '2017-01-10 00:00:00', '', '2017-01-24 13:30:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('755', '2017-01-16 00:00:00', '', '2017-01-24 13:31:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('756', '2017-01-17 00:00:00', '', '2017-01-24 13:31:56', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('757', '2017-01-19 00:00:00', '', '2017-01-24 13:32:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('758', '2017-01-16 00:00:00', '', '2017-01-25 11:39:08', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('759', '2017-01-17 00:00:00', '', '2017-01-25 11:40:12', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('760', '2017-01-18 00:00:00', '', '2017-01-25 11:41:19', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('761', '2017-01-19 00:00:00', '', '2017-01-25 11:42:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('762', '2017-01-20 00:00:00', '', '2017-01-25 11:44:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('763', '2017-01-21 00:00:00', '', '2017-01-25 11:45:15', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('764', '2017-01-22 00:00:00', '', '2017-01-25 11:46:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('765', '2017-01-23 00:00:00', '', '2017-01-25 11:47:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('766', '2017-01-24 00:00:00', '', '2017-01-25 11:49:09', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('767', '2017-01-23 00:00:00', '', '2017-01-25 11:57:44', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('768', '2017-01-04 00:00:00', '', '2017-01-25 12:00:38', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('769', '2017-01-05 00:00:00', '', '2017-01-25 12:02:33', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('770', '2017-01-06 00:00:00', '', '2017-01-25 12:03:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('771', '2017-01-07 00:00:00', '', '2017-01-25 12:04:55', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('772', '2017-01-09 00:00:00', '', '2017-01-25 12:17:30', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('773', '2017-01-10 00:00:00', '', '2017-01-25 12:18:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('774', '2017-01-11 00:00:00', '', '2017-01-25 12:20:14', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('775', '2017-01-12 00:00:00', '', '2017-01-25 12:21:37', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('776', '2017-01-13 00:00:00', '', '2017-01-25 12:22:46', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('777', '2017-01-14 00:00:00', '', '2017-01-25 12:23:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('778', '2017-01-15 00:00:00', '', '2017-01-25 12:25:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('779', '2017-01-16 00:00:00', '', '2017-01-25 12:34:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('780', '2017-01-17 00:00:00', '', '2017-01-25 12:36:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('781', '2017-01-18 00:00:00', '', '2017-01-25 12:41:03', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('782', '2017-01-19 00:00:00', '', '2017-01-25 12:43:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('783', '2017-01-20 00:00:00', '', '2017-01-25 12:46:04', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('784', '2017-01-23 00:00:00', '', '2017-01-25 12:49:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('785', '2017-01-21 00:00:00', '', '2017-01-25 12:53:06', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('786', '2017-01-09 00:00:00', '', '2017-01-25 12:58:59', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('787', '2017-01-17 00:00:00', '', '2017-01-25 13:00:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('788', '2017-01-18 00:00:00', '', '2017-01-25 13:01:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('789', '2017-01-05 00:00:00', '', '2017-01-25 13:10:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('790', '2017-01-06 00:00:00', '', '2017-01-25 13:23:16', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('791', '2017-01-07 00:00:00', '', '2017-01-25 13:26:58', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('792', '2017-01-08 00:00:00', '', '2017-01-25 13:28:27', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('793', '2017-01-09 00:00:00', '', '2017-01-25 13:31:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('794', '2017-01-11 00:00:00', '', '2017-01-25 13:32:20', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('795', '2017-01-12 00:00:00', '', '2017-01-25 13:33:18', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('796', '2017-01-13 00:00:00', '', '2017-01-25 13:35:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('797', '2017-01-14 00:00:00', '', '2017-01-25 13:36:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('798', '2017-01-15 00:00:00', '', '2017-01-25 13:38:32', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('799', '2017-01-16 00:00:00', '', '2017-01-25 13:56:36', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('800', '2017-01-17 00:00:00', '', '2017-01-25 13:58:07', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('801', '2017-01-18 00:00:00', '', '2017-01-25 13:59:13', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('802', '2017-01-19 00:00:00', '', '2017-01-25 13:59:54', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('803', '2017-01-23 00:00:00', '', '2017-01-25 14:00:48', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('804', '2017-01-07 00:00:00', '', '2017-01-25 14:05:01', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('805', '2017-01-08 00:00:00', '', '2017-01-25 14:06:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('806', '2017-01-09 00:00:00', '', '2017-01-25 14:08:25', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('807', '2017-01-11 00:00:00', '', '2017-01-25 14:09:40', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('808', '2017-01-12 00:00:00', '', '2017-01-25 14:17:41', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('809', '2017-01-14 00:00:00', '', '2017-01-25 14:18:35', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('810', '2017-01-15 00:00:00', '', '2017-01-25 14:19:29', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('811', '2017-01-19 00:00:00', '', '2017-01-25 14:20:24', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('812', '2017-01-20 00:00:00', '', '2017-01-25 14:21:45', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('813', '2017-01-21 00:00:00', '', '2017-01-25 14:22:43', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('814', '2017-02-06 00:00:00', 'Loan Disbursement', '2017-02-05 18:20:26', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('815', '2017-02-07 00:00:00', 'Loan Disbursement', '2017-02-05 18:21:21', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('816', '2017-02-07 00:00:00', 'Loan Disbursement', '2017-02-06 15:41:22', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('817', '2017-02-08 00:00:00', 'Loan Disbursement', '2017-02-06 16:01:57', '1', '0', '0', null, null);
INSERT INTO `tb_receipts` VALUES ('818', '2017-02-07 00:00:00', '890', '2017-02-06 16:57:27', '1', '0', '0', null, null);

-- ----------------------------
-- Table structure for tb_rules
-- ----------------------------
DROP TABLE IF EXISTS `tb_rules`;
CREATE TABLE `tb_rules` (
  `ruleid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `ruletype` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ruleid`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_rules
-- ----------------------------
INSERT INTO `tb_rules` VALUES ('1', 'Bremak Contacts', '1', '1');
INSERT INTO `tb_rules` VALUES ('2', 'All Customers', '0', '1');
INSERT INTO `tb_rules` VALUES ('3', 'Dormant Customers', '0', '1');
INSERT INTO `tb_rules` VALUES ('4', 'All Customers In Arrears', '0', '1');

-- ----------------------------
-- Table structure for tb_schedules
-- ----------------------------
DROP TABLE IF EXISTS `tb_schedules`;
CREATE TABLE `tb_schedules` (
  `scheduleid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `minutes` int(11) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `months` int(11) DEFAULT NULL,
  `weekdays` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `lastrun` datetime DEFAULT NULL,
  PRIMARY KEY (`scheduleid`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_schedules
-- ----------------------------
INSERT INTO `tb_schedules` VALUES ('1', 'Direct Message', null, null, null, null, null, '1', null);
INSERT INTO `tb_schedules` VALUES ('2', 'Test Schedule', '34', '12', '16', '1', null, '1', null);
INSERT INTO `tb_schedules` VALUES ('4', 'Kenyan Testing', '19', '11', null, null, '2', '1', null);

-- ----------------------------
-- Table structure for tb_securitypolicy
-- ----------------------------
DROP TABLE IF EXISTS `tb_securitypolicy`;
CREATE TABLE `tb_securitypolicy` (
  `securitypolicyid` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `securityvalues` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`securitypolicyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_securitypolicy
-- ----------------------------

-- ----------------------------
-- Table structure for tb_syssettings
-- ----------------------------
DROP TABLE IF EXISTS `tb_syssettings`;
CREATE TABLE `tb_syssettings` (
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_syssettings
-- ----------------------------
INSERT INTO `tb_syssettings` VALUES ('{\"institutionname\":\"Sun Group Micro Africa\",\"institutioncode\":\"IMAB00003\",\"mpesarepmandatoryfeeid\":\"0\",\"loanapplicationfeeid\":\"3\",\"mpesacontraaccountid\":\"15\",\"systemuserid\":\"1\",\"excessrepaymentamountproductid\":\"1\"}');

-- ----------------------------
-- Table structure for tb_tags
-- ----------------------------
DROP TABLE IF EXISTS `tb_tags`;
CREATE TABLE `tb_tags` (
  `tagid` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`tagid`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_tags
-- ----------------------------
INSERT INTO `tb_tags` VALUES ('1', '${Name}', 'Customer Name', '1');
INSERT INTO `tb_tags` VALUES ('2', '${SavingsBalance}', 'Savings Balance', '1');
INSERT INTO `tb_tags` VALUES ('3', '${LoanAmount}', 'Loan Amount', '1');
INSERT INTO `tb_tags` VALUES ('4', '${ArrearsAmount}', 'Arrears Amount', '1');
INSERT INTO `tb_tags` VALUES ('5', '${AccountNumber}', 'Account Number', '1');
INSERT INTO `tb_tags` VALUES ('6', '${Receiptno}', 'Receipt No', '1');
INSERT INTO `tb_tags` VALUES ('7', '${Amount}', 'Amount', '1');

-- ----------------------------
-- Table structure for tb_templates
-- ----------------------------
DROP TABLE IF EXISTS `tb_templates`;
CREATE TABLE `tb_templates` (
  `templateid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `content` varchar(160) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`templateid`),
  UNIQUE KEY `description` (`description`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_templates
-- ----------------------------
INSERT INTO `tb_templates` VALUES ('1', 'Sun Group Staff Greetings', 'SG01', 'Andrew testing. Demo', '1');
INSERT INTO `tb_templates` VALUES ('2', 'Customers In Arrears', 'SMA02', 'Dear ${Name}, your account no ${AccountNumber} is in arrears for amount ${ArrearsAmount}', '1');

-- ----------------------------
-- Table structure for tb_transactions
-- ----------------------------
DROP TABLE IF EXISTS `tb_transactions`;
CREATE TABLE `tb_transactions` (
  `transactionid` bigint(20) NOT NULL AUTO_INCREMENT,
  `receiptno` bigint(20) NOT NULL,
  `valuedate` date NOT NULL,
  `accountid` bigint(11) NOT NULL,
  `feeid` int(11) DEFAULT NULL,
  `iscredit` tinyint(1) NOT NULL,
  `amount` double(18,2) NOT NULL DEFAULT '0.00',
  `transactiontype` tinyint(1) NOT NULL,
  `debitaccid` bigint(20) NOT NULL,
  `creditaccid` bigint(20) NOT NULL,
  `serverdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transactionid`),
  KEY `FK_transactions_account` (`accountid`),
  KEY `FK_transactions_receipts` (`receiptno`),
  KEY `FK_tb_transactions_tb_fee` (`feeid`),
  KEY `FK_tb_transactions_tb_ledgeraccounts_credit` (`creditaccid`),
  KEY `FK_tb_transactions_tb_ledgeraccounts_debit` (`debitaccid`),
  KEY `tb_transactions_accountid` (`accountid`),
  KEY `tb_transactions_valuedate` (`valuedate`),
  KEY `tb_transactions_receiptno` (`receiptno`),
  CONSTRAINT `FK_tb_transactions_tb_accounts` FOREIGN KEY (`accountid`) REFERENCES `tb_accounts` (`accountid`),
  CONSTRAINT `FK_tb_transactions_tb_ledgeraccounts_credit` FOREIGN KEY (`creditaccid`) REFERENCES `tb_ledgeraccounts` (`accountid`),
  CONSTRAINT `FK_tb_transactions_tb_ledgeraccounts_debit` FOREIGN KEY (`debitaccid`) REFERENCES `tb_ledgeraccounts` (`accountid`),
  CONSTRAINT `FK_tb_transactions_tb_receipts` FOREIGN KEY (`receiptno`) REFERENCES `tb_receipts` (`receiptno`)
) ENGINE=InnoDB AUTO_INCREMENT=830 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_transactions
-- ----------------------------
INSERT INTO `tb_transactions` VALUES ('17', '17', '2016-12-07', '10', '0', '1', '169800.00', '1', '14', '5', '2016-12-08 14:36:30');
INSERT INTO `tb_transactions` VALUES ('18', '18', '2016-12-07', '11', '0', '1', '86000.00', '1', '14', '5', '2016-12-08 15:06:01');
INSERT INTO `tb_transactions` VALUES ('20', '20', '2016-12-07', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-08 15:32:15');
INSERT INTO `tb_transactions` VALUES ('22', '22', '2016-12-07', '14', '0', '1', '115500.00', '1', '14', '5', '2016-12-08 17:04:47');
INSERT INTO `tb_transactions` VALUES ('23', '23', '2016-12-08', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:36:41');
INSERT INTO `tb_transactions` VALUES ('24', '24', '2016-12-09', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:38:19');
INSERT INTO `tb_transactions` VALUES ('25', '25', '2016-12-10', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:39:04');
INSERT INTO `tb_transactions` VALUES ('26', '26', '2016-12-11', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:39:55');
INSERT INTO `tb_transactions` VALUES ('27', '27', '2016-12-12', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:40:49');
INSERT INTO `tb_transactions` VALUES ('28', '28', '2016-12-13', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:41:33');
INSERT INTO `tb_transactions` VALUES ('29', '29', '2016-12-14', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:42:15');
INSERT INTO `tb_transactions` VALUES ('30', '30', '2016-12-15', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:43:05');
INSERT INTO `tb_transactions` VALUES ('31', '31', '2016-12-10', '11', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:51:02');
INSERT INTO `tb_transactions` VALUES ('32', '32', '2016-12-11', '11', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:52:07');
INSERT INTO `tb_transactions` VALUES ('33', '33', '2016-12-13', '11', '0', '1', '500.00', '1', '14', '5', '2016-12-14 13:53:02');
INSERT INTO `tb_transactions` VALUES ('34', '34', '2016-12-01', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-14 13:55:45');
INSERT INTO `tb_transactions` VALUES ('35', '35', '2016-12-03', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-14 13:58:01');
INSERT INTO `tb_transactions` VALUES ('36', '36', '2016-12-05', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 13:59:00');
INSERT INTO `tb_transactions` VALUES ('37', '37', '2016-12-06', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:00:04');
INSERT INTO `tb_transactions` VALUES ('38', '38', '2016-12-09', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:01:38');
INSERT INTO `tb_transactions` VALUES ('39', '39', '2016-12-10', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:03:33');
INSERT INTO `tb_transactions` VALUES ('40', '40', '2016-12-01', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:06:42');
INSERT INTO `tb_transactions` VALUES ('41', '41', '2016-12-03', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:07:55');
INSERT INTO `tb_transactions` VALUES ('43', '43', '2016-12-09', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:14:17');
INSERT INTO `tb_transactions` VALUES ('44', '44', '2016-12-10', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:15:07');
INSERT INTO `tb_transactions` VALUES ('45', '45', '2016-12-11', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-14 14:22:22');
INSERT INTO `tb_transactions` VALUES ('46', '46', '2016-12-15', '11', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:13:19');
INSERT INTO `tb_transactions` VALUES ('47', '47', '2016-12-08', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:20:22');
INSERT INTO `tb_transactions` VALUES ('48', '48', '2016-12-09', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:21:05');
INSERT INTO `tb_transactions` VALUES ('49', '49', '2016-12-10', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:21:46');
INSERT INTO `tb_transactions` VALUES ('50', '50', '2016-12-11', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:22:33');
INSERT INTO `tb_transactions` VALUES ('51', '51', '2016-12-13', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:23:12');
INSERT INTO `tb_transactions` VALUES ('52', '52', '2016-12-14', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:24:14');
INSERT INTO `tb_transactions` VALUES ('53', '53', '2016-12-16', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-19 10:24:54');
INSERT INTO `tb_transactions` VALUES ('55', '55', '2016-12-02', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:32:00');
INSERT INTO `tb_transactions` VALUES ('56', '56', '2016-12-03', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:33:05');
INSERT INTO `tb_transactions` VALUES ('57', '57', '2016-12-04', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:34:05');
INSERT INTO `tb_transactions` VALUES ('58', '58', '2016-12-05', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:34:32');
INSERT INTO `tb_transactions` VALUES ('59', '59', '2016-12-08', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:34:54');
INSERT INTO `tb_transactions` VALUES ('60', '60', '2016-12-09', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:35:17');
INSERT INTO `tb_transactions` VALUES ('61', '61', '2016-12-10', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:37:29');
INSERT INTO `tb_transactions` VALUES ('62', '62', '2016-12-11', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:37:47');
INSERT INTO `tb_transactions` VALUES ('63', '63', '2016-12-13', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:38:07');
INSERT INTO `tb_transactions` VALUES ('64', '64', '2016-12-14', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:38:25');
INSERT INTO `tb_transactions` VALUES ('65', '65', '2016-12-16', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-19 10:38:54');
INSERT INTO `tb_transactions` VALUES ('66', '66', '2016-12-19', '15', '0', '1', '53600.00', '1', '14', '5', '2016-12-19 13:42:34');
INSERT INTO `tb_transactions` VALUES ('69', '69', '2016-12-07', '13', '0', '1', '900.00', '1', '14', '5', '2016-12-22 12:47:16');
INSERT INTO `tb_transactions` VALUES ('70', '70', '2016-12-22', '18', '0', '1', '89900.00', '1', '14', '5', '2016-12-22 12:50:18');
INSERT INTO `tb_transactions` VALUES ('71', '71', '2016-12-22', '19', '0', '1', '104000.00', '1', '14', '5', '2016-12-22 12:55:38');
INSERT INTO `tb_transactions` VALUES ('72', '72', '2016-12-22', '20', '0', '1', '65000.00', '1', '14', '5', '2016-12-22 12:59:56');
INSERT INTO `tb_transactions` VALUES ('73', '73', '2016-12-22', '21', '0', '1', '74600.00', '1', '14', '5', '2016-12-22 13:01:05');
INSERT INTO `tb_transactions` VALUES ('74', '74', '2016-12-22', '22', '0', '1', '52300.00', '1', '14', '5', '2016-12-22 13:02:33');
INSERT INTO `tb_transactions` VALUES ('75', '75', '2016-12-22', '23', '0', '1', '19400.00', '1', '14', '5', '2016-12-22 13:04:07');
INSERT INTO `tb_transactions` VALUES ('76', '76', '2016-12-22', '24', '0', '1', '31200.00', '1', '14', '5', '2016-12-22 13:05:09');
INSERT INTO `tb_transactions` VALUES ('77', '77', '2016-12-22', '25', '0', '1', '21500.00', '1', '14', '5', '2016-12-22 13:06:44');
INSERT INTO `tb_transactions` VALUES ('78', '78', '2016-12-22', '26', '0', '1', '1000.00', '1', '14', '5', '2016-12-22 13:08:47');
INSERT INTO `tb_transactions` VALUES ('79', '79', '2016-12-22', '27', '0', '1', '162100.00', '1', '14', '5', '2016-12-22 13:09:47');
INSERT INTO `tb_transactions` VALUES ('80', '80', '2016-12-22', '28', '0', '1', '10500.00', '1', '14', '5', '2016-12-22 13:11:36');
INSERT INTO `tb_transactions` VALUES ('81', '81', '2016-12-22', '29', '0', '1', '0.00', '1', '14', '5', '2016-12-22 13:13:40');
INSERT INTO `tb_transactions` VALUES ('82', '82', '2016-12-22', '30', '0', '1', '0.00', '1', '14', '5', '2016-12-22 13:14:19');
INSERT INTO `tb_transactions` VALUES ('83', '83', '2016-12-22', '31', '0', '1', '0.00', '1', '14', '5', '2016-12-22 13:15:10');
INSERT INTO `tb_transactions` VALUES ('84', '84', '2016-12-22', '32', '0', '1', '225200.00', '1', '14', '5', '2016-12-22 13:16:31');
INSERT INTO `tb_transactions` VALUES ('85', '85', '2016-12-22', '33', '0', '1', '2000.00', '1', '14', '5', '2016-12-22 13:17:39');
INSERT INTO `tb_transactions` VALUES ('86', '86', '2016-12-22', '34', '0', '1', '2400.00', '1', '14', '5', '2016-12-22 13:18:30');
INSERT INTO `tb_transactions` VALUES ('87', '87', '2016-12-22', '35', '0', '1', '93400.00', '1', '14', '5', '2016-12-22 13:24:51');
INSERT INTO `tb_transactions` VALUES ('88', '88', '2016-12-22', '36', '0', '1', '6000.00', '1', '14', '5', '2016-12-22 13:25:43');
INSERT INTO `tb_transactions` VALUES ('89', '89', '2016-12-22', '37', '0', '1', '4600.00', '1', '14', '5', '2016-12-22 13:55:18');
INSERT INTO `tb_transactions` VALUES ('90', '90', '2016-12-22', '38', '0', '1', '7200.00', '1', '14', '5', '2016-12-22 13:56:43');
INSERT INTO `tb_transactions` VALUES ('91', '91', '2016-12-22', '43', '0', '1', '85900.00', '1', '14', '5', '2016-12-22 14:08:50');
INSERT INTO `tb_transactions` VALUES ('92', '92', '2016-12-17', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:21:04');
INSERT INTO `tb_transactions` VALUES ('93', '93', '2016-12-18', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:21:50');
INSERT INTO `tb_transactions` VALUES ('95', '95', '2016-12-22', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:26:53');
INSERT INTO `tb_transactions` VALUES ('96', '96', '2016-12-23', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:27:39');
INSERT INTO `tb_transactions` VALUES ('97', '97', '2016-12-24', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:28:07');
INSERT INTO `tb_transactions` VALUES ('98', '98', '2016-12-25', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:32:24');
INSERT INTO `tb_transactions` VALUES ('99', '99', '2016-12-26', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:33:08');
INSERT INTO `tb_transactions` VALUES ('101', '101', '2016-12-27', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:36:14');
INSERT INTO `tb_transactions` VALUES ('102', '102', '2016-12-16', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:43:51');
INSERT INTO `tb_transactions` VALUES ('104', '104', '2016-12-17', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:56:49');
INSERT INTO `tb_transactions` VALUES ('105', '105', '2016-12-18', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:57:24');
INSERT INTO `tb_transactions` VALUES ('106', '106', '2016-12-19', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:58:37');
INSERT INTO `tb_transactions` VALUES ('107', '107', '2016-12-20', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:59:15');
INSERT INTO `tb_transactions` VALUES ('108', '108', '2016-12-21', '10', '0', '1', '500.00', '1', '14', '5', '2016-12-29 13:59:57');
INSERT INTO `tb_transactions` VALUES ('109', '109', '2016-12-29', '45', '0', '1', '427500.00', '1', '14', '5', '2016-12-29 14:07:08');
INSERT INTO `tb_transactions` VALUES ('110', '110', '2016-12-15', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 14:35:59');
INSERT INTO `tb_transactions` VALUES ('111', '111', '2016-12-28', '14', '0', '1', '500.00', '1', '14', '5', '2016-12-29 14:52:24');
INSERT INTO `tb_transactions` VALUES ('112', '112', '2016-12-14', '10', '4', '1', '300.00', '3', '14', '20', '2016-12-31 10:19:30');
INSERT INTO `tb_transactions` VALUES ('113', '113', '2016-12-21', '10', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:19:55');
INSERT INTO `tb_transactions` VALUES ('114', '114', '2016-12-22', '10', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:20:17');
INSERT INTO `tb_transactions` VALUES ('115', '115', '2016-12-25', '10', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:20:39');
INSERT INTO `tb_transactions` VALUES ('116', '116', '2016-12-26', '10', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:21:05');
INSERT INTO `tb_transactions` VALUES ('117', '117', '2016-12-27', '10', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:21:21');
INSERT INTO `tb_transactions` VALUES ('118', '118', '2016-12-19', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-31 10:26:25');
INSERT INTO `tb_transactions` VALUES ('119', '119', '2016-12-20', '11', '4', '1', '300.00', '3', '14', '20', '2016-12-31 10:26:47');
INSERT INTO `tb_transactions` VALUES ('120', '120', '2016-12-21', '11', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:27:06');
INSERT INTO `tb_transactions` VALUES ('121', '121', '2016-12-22', '11', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:27:25');
INSERT INTO `tb_transactions` VALUES ('122', '122', '2016-12-27', '11', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:27:50');
INSERT INTO `tb_transactions` VALUES ('123', '123', '2016-12-28', '11', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:28:15');
INSERT INTO `tb_transactions` VALUES ('124', '124', '2016-12-29', '11', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:28:36');
INSERT INTO `tb_transactions` VALUES ('125', '125', '2016-12-01', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:57:54');
INSERT INTO `tb_transactions` VALUES ('126', '126', '2016-12-03', '12', '1', '1', '200.00', '3', '14', '1', '2016-12-31 10:58:23');
INSERT INTO `tb_transactions` VALUES ('127', '127', '2016-12-06', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:58:56');
INSERT INTO `tb_transactions` VALUES ('128', '128', '2016-12-07', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 10:59:16');
INSERT INTO `tb_transactions` VALUES ('129', '129', '2016-12-10', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:00:40');
INSERT INTO `tb_transactions` VALUES ('130', '130', '2016-12-14', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:01:03');
INSERT INTO `tb_transactions` VALUES ('131', '131', '2016-12-16', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:01:23');
INSERT INTO `tb_transactions` VALUES ('132', '132', '2016-12-17', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:01:41');
INSERT INTO `tb_transactions` VALUES ('133', '133', '2016-12-19', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:01:59');
INSERT INTO `tb_transactions` VALUES ('134', '134', '2016-12-21', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:02:18');
INSERT INTO `tb_transactions` VALUES ('135', '135', '2016-12-22', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:02:37');
INSERT INTO `tb_transactions` VALUES ('136', '136', '2016-12-23', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:02:55');
INSERT INTO `tb_transactions` VALUES ('137', '137', '2016-12-01', '13', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:04:23');
INSERT INTO `tb_transactions` VALUES ('138', '138', '2016-12-03', '13', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:04:52');
INSERT INTO `tb_transactions` VALUES ('139', '139', '2016-12-05', '12', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:05:11');
INSERT INTO `tb_transactions` VALUES ('140', '140', '2016-12-05', '13', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:06:56');
INSERT INTO `tb_transactions` VALUES ('141', '141', '2016-12-01', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:09:15');
INSERT INTO `tb_transactions` VALUES ('142', '142', '2016-12-17', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:10:23');
INSERT INTO `tb_transactions` VALUES ('143', '143', '2016-12-18', '14', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:10:42');
INSERT INTO `tb_transactions` VALUES ('144', '144', '2016-12-20', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:11:05');
INSERT INTO `tb_transactions` VALUES ('145', '145', '2016-12-21', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:11:24');
INSERT INTO `tb_transactions` VALUES ('146', '146', '2016-12-22', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:11:40');
INSERT INTO `tb_transactions` VALUES ('147', '147', '2016-12-23', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:12:04');
INSERT INTO `tb_transactions` VALUES ('148', '148', '2016-12-24', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:12:20');
INSERT INTO `tb_transactions` VALUES ('149', '149', '2016-12-25', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:12:38');
INSERT INTO `tb_transactions` VALUES ('150', '150', '2016-12-26', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:12:56');
INSERT INTO `tb_transactions` VALUES ('151', '151', '2016-12-27', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:13:25');
INSERT INTO `tb_transactions` VALUES ('152', '152', '2016-12-28', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:13:50');
INSERT INTO `tb_transactions` VALUES ('153', '153', '2016-12-29', '14', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:14:09');
INSERT INTO `tb_transactions` VALUES ('154', '154', '2016-12-01', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:18:50');
INSERT INTO `tb_transactions` VALUES ('155', '155', '2016-12-02', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:19:16');
INSERT INTO `tb_transactions` VALUES ('156', '156', '2016-12-03', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:19:31');
INSERT INTO `tb_transactions` VALUES ('157', '157', '2016-12-04', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:19:46');
INSERT INTO `tb_transactions` VALUES ('158', '158', '2016-12-07', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:20:08');
INSERT INTO `tb_transactions` VALUES ('159', '159', '2016-12-09', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:20:31');
INSERT INTO `tb_transactions` VALUES ('160', '160', '2016-12-10', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:20:51');
INSERT INTO `tb_transactions` VALUES ('161', '161', '2016-12-11', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:21:07');
INSERT INTO `tb_transactions` VALUES ('162', '162', '2016-12-17', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:21:27');
INSERT INTO `tb_transactions` VALUES ('163', '163', '2016-12-18', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:21:42');
INSERT INTO `tb_transactions` VALUES ('164', '164', '2016-12-19', '15', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:21:58');
INSERT INTO `tb_transactions` VALUES ('167', '167', '2016-12-25', '15', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:22:51');
INSERT INTO `tb_transactions` VALUES ('168', '168', '2016-12-26', '15', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:23:11');
INSERT INTO `tb_transactions` VALUES ('170', '170', '2016-12-02', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:26:14');
INSERT INTO `tb_transactions` VALUES ('171', '171', '2016-12-05', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:26:44');
INSERT INTO `tb_transactions` VALUES ('172', '172', '2016-12-06', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:27:03');
INSERT INTO `tb_transactions` VALUES ('173', '173', '2016-12-07', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:27:21');
INSERT INTO `tb_transactions` VALUES ('174', '174', '2016-12-08', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:27:36');
INSERT INTO `tb_transactions` VALUES ('175', '175', '2016-12-10', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:27:53');
INSERT INTO `tb_transactions` VALUES ('176', '176', '2016-12-13', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:28:12');
INSERT INTO `tb_transactions` VALUES ('177', '177', '2016-12-14', '18', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:28:32');
INSERT INTO `tb_transactions` VALUES ('178', '178', '2016-12-19', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:28:53');
INSERT INTO `tb_transactions` VALUES ('179', '179', '2016-12-20', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:29:08');
INSERT INTO `tb_transactions` VALUES ('180', '180', '2016-12-29', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:29:28');
INSERT INTO `tb_transactions` VALUES ('181', '181', '2016-12-30', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:29:47');
INSERT INTO `tb_transactions` VALUES ('182', '182', '2016-12-22', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:30:42');
INSERT INTO `tb_transactions` VALUES ('183', '183', '2016-12-23', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:30:59');
INSERT INTO `tb_transactions` VALUES ('184', '184', '2016-12-24', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:31:17');
INSERT INTO `tb_transactions` VALUES ('185', '185', '2016-12-26', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:31:45');
INSERT INTO `tb_transactions` VALUES ('186', '186', '2016-12-27', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:32:12');
INSERT INTO `tb_transactions` VALUES ('187', '187', '2016-12-28', '18', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:32:27');
INSERT INTO `tb_transactions` VALUES ('188', '188', '2016-12-03', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:34:47');
INSERT INTO `tb_transactions` VALUES ('189', '189', '2016-12-07', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:35:27');
INSERT INTO `tb_transactions` VALUES ('190', '190', '2016-12-08', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:37:33');
INSERT INTO `tb_transactions` VALUES ('191', '191', '2016-12-10', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:37:52');
INSERT INTO `tb_transactions` VALUES ('192', '192', '2016-12-13', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:38:13');
INSERT INTO `tb_transactions` VALUES ('193', '193', '2016-12-16', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:38:33');
INSERT INTO `tb_transactions` VALUES ('194', '194', '2016-12-17', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:38:54');
INSERT INTO `tb_transactions` VALUES ('195', '195', '2016-12-18', '19', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:39:11');
INSERT INTO `tb_transactions` VALUES ('196', '196', '2016-12-23', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:39:33');
INSERT INTO `tb_transactions` VALUES ('197', '197', '2016-12-24', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:39:53');
INSERT INTO `tb_transactions` VALUES ('198', '198', '2016-12-25', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:40:11');
INSERT INTO `tb_transactions` VALUES ('199', '199', '2016-12-27', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:40:29');
INSERT INTO `tb_transactions` VALUES ('200', '200', '2016-12-28', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:40:47');
INSERT INTO `tb_transactions` VALUES ('201', '201', '2016-12-29', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:41:10');
INSERT INTO `tb_transactions` VALUES ('202', '202', '2016-12-30', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:41:56');
INSERT INTO `tb_transactions` VALUES ('203', '203', '2016-12-05', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:43:50');
INSERT INTO `tb_transactions` VALUES ('204', '204', '2016-12-21', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:44:13');
INSERT INTO `tb_transactions` VALUES ('205', '205', '2016-12-22', '19', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:44:32');
INSERT INTO `tb_transactions` VALUES ('206', '206', '2016-12-10', '22', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:49:34');
INSERT INTO `tb_transactions` VALUES ('207', '207', '2016-12-15', '22', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:49:57');
INSERT INTO `tb_transactions` VALUES ('208', '208', '2016-12-17', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:50:16');
INSERT INTO `tb_transactions` VALUES ('209', '209', '2016-12-19', '22', '4', '1', '300.00', '3', '14', '20', '2016-12-31 11:50:35');
INSERT INTO `tb_transactions` VALUES ('210', '210', '2016-12-22', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:50:53');
INSERT INTO `tb_transactions` VALUES ('211', '211', '2016-12-05', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:51:22');
INSERT INTO `tb_transactions` VALUES ('212', '212', '2016-12-08', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:51:39');
INSERT INTO `tb_transactions` VALUES ('213', '213', '2016-12-12', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:51:53');
INSERT INTO `tb_transactions` VALUES ('214', '214', '2016-12-13', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:52:10');
INSERT INTO `tb_transactions` VALUES ('215', '215', '2016-12-26', '22', '4', '1', '200.00', '3', '14', '20', '2016-12-31 11:52:28');
INSERT INTO `tb_transactions` VALUES ('216', '216', '2016-12-14', '23', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:24:32');
INSERT INTO `tb_transactions` VALUES ('217', '217', '2016-12-16', '23', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:24:51');
INSERT INTO `tb_transactions` VALUES ('218', '218', '2016-12-17', '23', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:25:10');
INSERT INTO `tb_transactions` VALUES ('219', '219', '2016-12-19', '23', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:25:33');
INSERT INTO `tb_transactions` VALUES ('220', '220', '2016-12-21', '23', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:25:53');
INSERT INTO `tb_transactions` VALUES ('221', '221', '2016-12-23', '23', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:26:21');
INSERT INTO `tb_transactions` VALUES ('222', '222', '2016-12-02', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:27:17');
INSERT INTO `tb_transactions` VALUES ('223', '223', '2016-12-03', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:27:54');
INSERT INTO `tb_transactions` VALUES ('224', '224', '2016-12-01', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:42:27');
INSERT INTO `tb_transactions` VALUES ('225', '225', '2016-12-05', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:42:47');
INSERT INTO `tb_transactions` VALUES ('226', '226', '2016-12-06', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:43:05');
INSERT INTO `tb_transactions` VALUES ('227', '227', '2016-12-09', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:43:24');
INSERT INTO `tb_transactions` VALUES ('228', '228', '2016-12-10', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:43:42');
INSERT INTO `tb_transactions` VALUES ('229', '229', '2016-12-13', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:43:59');
INSERT INTO `tb_transactions` VALUES ('230', '230', '2016-12-15', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:44:19');
INSERT INTO `tb_transactions` VALUES ('231', '231', '2016-12-16', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:44:39');
INSERT INTO `tb_transactions` VALUES ('232', '232', '2016-12-20', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:44:58');
INSERT INTO `tb_transactions` VALUES ('233', '233', '2016-12-21', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:45:13');
INSERT INTO `tb_transactions` VALUES ('234', '234', '2016-12-23', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:45:30');
INSERT INTO `tb_transactions` VALUES ('235', '235', '2016-12-25', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:45:49');
INSERT INTO `tb_transactions` VALUES ('236', '236', '2016-12-27', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:46:15');
INSERT INTO `tb_transactions` VALUES ('237', '237', '2016-12-28', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:46:37');
INSERT INTO `tb_transactions` VALUES ('238', '238', '2016-12-29', '24', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:47:13');
INSERT INTO `tb_transactions` VALUES ('239', '239', '2016-12-01', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:49:37');
INSERT INTO `tb_transactions` VALUES ('240', '240', '2016-12-02', '27', '4', '1', '500.00', '3', '14', '20', '2016-12-31 12:49:56');
INSERT INTO `tb_transactions` VALUES ('241', '241', '2016-12-03', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:50:21');
INSERT INTO `tb_transactions` VALUES ('242', '242', '2016-12-05', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:50:37');
INSERT INTO `tb_transactions` VALUES ('243', '243', '2016-12-04', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:51:50');
INSERT INTO `tb_transactions` VALUES ('244', '244', '2016-12-07', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:52:08');
INSERT INTO `tb_transactions` VALUES ('245', '245', '2016-12-09', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:52:30');
INSERT INTO `tb_transactions` VALUES ('246', '246', '2016-12-10', '27', '4', '1', '300.00', '3', '14', '20', '2016-12-31 12:52:46');
INSERT INTO `tb_transactions` VALUES ('247', '247', '2016-12-12', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:53:05');
INSERT INTO `tb_transactions` VALUES ('248', '248', '2016-12-13', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:53:23');
INSERT INTO `tb_transactions` VALUES ('249', '249', '2016-12-21', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:53:44');
INSERT INTO `tb_transactions` VALUES ('250', '250', '2016-12-25', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:54:03');
INSERT INTO `tb_transactions` VALUES ('251', '251', '2016-12-26', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:54:20');
INSERT INTO `tb_transactions` VALUES ('252', '252', '2016-12-27', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:54:39');
INSERT INTO `tb_transactions` VALUES ('253', '253', '2016-12-29', '27', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:54:58');
INSERT INTO `tb_transactions` VALUES ('254', '254', '2016-12-01', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:56:34');
INSERT INTO `tb_transactions` VALUES ('255', '255', '2016-12-03', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:56:53');
INSERT INTO `tb_transactions` VALUES ('256', '256', '2016-12-05', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:57:10');
INSERT INTO `tb_transactions` VALUES ('257', '257', '2016-12-08', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:57:29');
INSERT INTO `tb_transactions` VALUES ('258', '258', '2016-12-09', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:57:44');
INSERT INTO `tb_transactions` VALUES ('259', '259', '2016-12-10', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:57:58');
INSERT INTO `tb_transactions` VALUES ('260', '260', '2016-12-12', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:58:23');
INSERT INTO `tb_transactions` VALUES ('261', '261', '2016-12-13', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:58:38');
INSERT INTO `tb_transactions` VALUES ('262', '262', '2016-12-14', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:58:52');
INSERT INTO `tb_transactions` VALUES ('263', '263', '2016-12-15', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:59:08');
INSERT INTO `tb_transactions` VALUES ('264', '264', '2016-12-16', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:59:28');
INSERT INTO `tb_transactions` VALUES ('265', '265', '2016-12-17', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 12:59:41');
INSERT INTO `tb_transactions` VALUES ('266', '266', '2016-12-19', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:00:10');
INSERT INTO `tb_transactions` VALUES ('267', '267', '2016-12-20', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:00:24');
INSERT INTO `tb_transactions` VALUES ('268', '268', '2016-12-21', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:00:41');
INSERT INTO `tb_transactions` VALUES ('269', '269', '2016-12-22', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:01:00');
INSERT INTO `tb_transactions` VALUES ('270', '270', '2016-12-23', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:01:22');
INSERT INTO `tb_transactions` VALUES ('271', '271', '2016-12-24', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:01:38');
INSERT INTO `tb_transactions` VALUES ('272', '272', '2016-12-27', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:08:18');
INSERT INTO `tb_transactions` VALUES ('273', '273', '2016-12-28', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:08:37');
INSERT INTO `tb_transactions` VALUES ('274', '274', '2016-12-29', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:08:54');
INSERT INTO `tb_transactions` VALUES ('275', '275', '2016-12-30', '28', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:09:09');
INSERT INTO `tb_transactions` VALUES ('276', '276', '2016-12-01', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:09:49');
INSERT INTO `tb_transactions` VALUES ('277', '277', '2016-12-02', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:10:03');
INSERT INTO `tb_transactions` VALUES ('278', '278', '2016-12-05', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:10:23');
INSERT INTO `tb_transactions` VALUES ('279', '279', '2016-12-09', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:10:41');
INSERT INTO `tb_transactions` VALUES ('280', '280', '2016-12-10', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:10:56');
INSERT INTO `tb_transactions` VALUES ('281', '281', '2016-12-14', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:11:15');
INSERT INTO `tb_transactions` VALUES ('282', '282', '2016-12-21', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:11:33');
INSERT INTO `tb_transactions` VALUES ('283', '283', '2016-12-23', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:11:54');
INSERT INTO `tb_transactions` VALUES ('284', '284', '2016-12-24', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:12:09');
INSERT INTO `tb_transactions` VALUES ('285', '285', '2016-12-27', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:12:26');
INSERT INTO `tb_transactions` VALUES ('287', '287', '2016-12-29', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:13:06');
INSERT INTO `tb_transactions` VALUES ('288', '288', '2016-12-30', '29', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:13:21');
INSERT INTO `tb_transactions` VALUES ('289', '289', '2016-12-02', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:20:31');
INSERT INTO `tb_transactions` VALUES ('290', '290', '2016-12-09', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:20:49');
INSERT INTO `tb_transactions` VALUES ('291', '291', '2016-12-12', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:21:07');
INSERT INTO `tb_transactions` VALUES ('292', '292', '2016-12-14', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:21:24');
INSERT INTO `tb_transactions` VALUES ('293', '293', '2016-12-15', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:21:39');
INSERT INTO `tb_transactions` VALUES ('294', '294', '2016-12-19', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:21:59');
INSERT INTO `tb_transactions` VALUES ('295', '295', '2016-12-21', '30', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:22:19');
INSERT INTO `tb_transactions` VALUES ('296', '296', '2016-12-03', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:27:00');
INSERT INTO `tb_transactions` VALUES ('297', '297', '2016-12-07', '32', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:27:24');
INSERT INTO `tb_transactions` VALUES ('298', '298', '2016-12-08', '32', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:27:45');
INSERT INTO `tb_transactions` VALUES ('299', '299', '2016-12-10', '32', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:28:07');
INSERT INTO `tb_transactions` VALUES ('300', '300', '2016-12-11', '32', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:28:22');
INSERT INTO `tb_transactions` VALUES ('301', '301', '2016-12-13', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:28:41');
INSERT INTO `tb_transactions` VALUES ('302', '302', '2016-12-14', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:28:55');
INSERT INTO `tb_transactions` VALUES ('303', '303', '2016-12-22', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:29:19');
INSERT INTO `tb_transactions` VALUES ('304', '304', '2016-12-23', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:29:35');
INSERT INTO `tb_transactions` VALUES ('305', '305', '2016-12-24', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:29:53');
INSERT INTO `tb_transactions` VALUES ('306', '306', '2016-12-25', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:30:20');
INSERT INTO `tb_transactions` VALUES ('307', '307', '2016-12-26', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:30:36');
INSERT INTO `tb_transactions` VALUES ('308', '308', '2016-12-28', '32', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:31:01');
INSERT INTO `tb_transactions` VALUES ('309', '309', '2016-12-05', '33', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:33:32');
INSERT INTO `tb_transactions` VALUES ('310', '310', '2016-12-07', '33', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:33:55');
INSERT INTO `tb_transactions` VALUES ('311', '311', '2016-12-09', '33', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:34:12');
INSERT INTO `tb_transactions` VALUES ('312', '312', '2016-12-01', '35', '4', '1', '600.00', '3', '14', '20', '2016-12-31 13:35:05');
INSERT INTO `tb_transactions` VALUES ('313', '313', '2016-12-03', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:35:26');
INSERT INTO `tb_transactions` VALUES ('314', '314', '2016-12-04', '35', '4', '1', '600.00', '3', '14', '20', '2016-12-31 13:35:45');
INSERT INTO `tb_transactions` VALUES ('315', '315', '2016-12-05', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:36:06');
INSERT INTO `tb_transactions` VALUES ('316', '316', '2016-12-06', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:36:21');
INSERT INTO `tb_transactions` VALUES ('317', '317', '2016-12-08', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:36:42');
INSERT INTO `tb_transactions` VALUES ('318', '318', '2016-12-12', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:37:00');
INSERT INTO `tb_transactions` VALUES ('319', '319', '2016-12-16', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:37:17');
INSERT INTO `tb_transactions` VALUES ('320', '320', '2016-12-17', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:37:35');
INSERT INTO `tb_transactions` VALUES ('321', '321', '2016-12-19', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:38:04');
INSERT INTO `tb_transactions` VALUES ('322', '322', '2016-12-20', '35', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:38:21');
INSERT INTO `tb_transactions` VALUES ('323', '323', '2016-12-21', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:38:43');
INSERT INTO `tb_transactions` VALUES ('324', '324', '2016-12-22', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:39:00');
INSERT INTO `tb_transactions` VALUES ('325', '325', '2016-12-23', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:39:17');
INSERT INTO `tb_transactions` VALUES ('326', '326', '2016-12-24', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:39:43');
INSERT INTO `tb_transactions` VALUES ('327', '327', '2016-12-25', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:40:01');
INSERT INTO `tb_transactions` VALUES ('328', '328', '2016-12-26', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:40:16');
INSERT INTO `tb_transactions` VALUES ('329', '329', '2016-12-27', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:40:34');
INSERT INTO `tb_transactions` VALUES ('330', '330', '2016-12-28', '35', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:41:00');
INSERT INTO `tb_transactions` VALUES ('331', '331', '2016-12-04', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:44:40');
INSERT INTO `tb_transactions` VALUES ('332', '332', '2016-12-05', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:44:59');
INSERT INTO `tb_transactions` VALUES ('333', '333', '2016-12-07', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:45:39');
INSERT INTO `tb_transactions` VALUES ('334', '334', '2016-12-09', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:46:07');
INSERT INTO `tb_transactions` VALUES ('335', '335', '2016-12-11', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:46:30');
INSERT INTO `tb_transactions` VALUES ('336', '336', '2016-12-12', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:46:54');
INSERT INTO `tb_transactions` VALUES ('337', '337', '2016-12-13', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:47:14');
INSERT INTO `tb_transactions` VALUES ('338', '338', '2016-12-19', '36', '4', '1', '300.00', '3', '14', '20', '2016-12-31 13:47:35');
INSERT INTO `tb_transactions` VALUES ('339', '339', '2016-12-22', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:48:00');
INSERT INTO `tb_transactions` VALUES ('340', '340', '2016-12-23', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:48:16');
INSERT INTO `tb_transactions` VALUES ('341', '341', '2016-12-25', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:48:38');
INSERT INTO `tb_transactions` VALUES ('342', '342', '2016-12-26', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:48:57');
INSERT INTO `tb_transactions` VALUES ('343', '343', '2016-12-28', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:49:21');
INSERT INTO `tb_transactions` VALUES ('344', '344', '2016-12-29', '36', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:49:48');
INSERT INTO `tb_transactions` VALUES ('345', '345', '2016-12-02', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:52:13');
INSERT INTO `tb_transactions` VALUES ('346', '346', '2016-12-05', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:52:31');
INSERT INTO `tb_transactions` VALUES ('347', '347', '2016-12-06', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:52:50');
INSERT INTO `tb_transactions` VALUES ('348', '348', '2016-12-07', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:53:14');
INSERT INTO `tb_transactions` VALUES ('349', '349', '2016-12-08', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:53:34');
INSERT INTO `tb_transactions` VALUES ('350', '350', '2016-12-13', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:54:04');
INSERT INTO `tb_transactions` VALUES ('351', '351', '2016-12-14', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 13:54:40');
INSERT INTO `tb_transactions` VALUES ('352', '352', '2016-12-15', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:00:17');
INSERT INTO `tb_transactions` VALUES ('353', '353', '2016-12-17', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:00:45');
INSERT INTO `tb_transactions` VALUES ('354', '354', '2016-12-20', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:01:47');
INSERT INTO `tb_transactions` VALUES ('355', '355', '2016-12-21', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:02:11');
INSERT INTO `tb_transactions` VALUES ('356', '356', '2016-12-22', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:02:34');
INSERT INTO `tb_transactions` VALUES ('357', '357', '2016-12-30', '37', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:02:54');
INSERT INTO `tb_transactions` VALUES ('358', '358', '2016-12-02', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:04:07');
INSERT INTO `tb_transactions` VALUES ('359', '359', '2016-12-06', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:12:26');
INSERT INTO `tb_transactions` VALUES ('360', '360', '2016-12-07', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:13:29');
INSERT INTO `tb_transactions` VALUES ('361', '361', '2016-12-08', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:13:50');
INSERT INTO `tb_transactions` VALUES ('362', '362', '2016-12-09', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:14:14');
INSERT INTO `tb_transactions` VALUES ('363', '363', '2016-12-12', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:14:35');
INSERT INTO `tb_transactions` VALUES ('364', '364', '2016-12-15', '38', '4', '1', '200.00', '3', '14', '20', '2016-12-31 14:15:03');
INSERT INTO `tb_transactions` VALUES ('365', '365', '2016-12-17', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:28:19');
INSERT INTO `tb_transactions` VALUES ('366', '366', '2016-12-21', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:28:49');
INSERT INTO `tb_transactions` VALUES ('367', '367', '2016-12-22', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:29:11');
INSERT INTO `tb_transactions` VALUES ('368', '368', '2016-12-26', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:29:37');
INSERT INTO `tb_transactions` VALUES ('369', '369', '2016-12-29', '40', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:30:30');
INSERT INTO `tb_transactions` VALUES ('370', '370', '2017-01-30', '40', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:30:46');
INSERT INTO `tb_transactions` VALUES ('371', '371', '2016-12-01', '41', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:31:15');
INSERT INTO `tb_transactions` VALUES ('372', '372', '2016-12-10', '41', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:31:39');
INSERT INTO `tb_transactions` VALUES ('373', '373', '2016-12-16', '41', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:32:00');
INSERT INTO `tb_transactions` VALUES ('374', '374', '2016-12-19', '41', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:32:22');
INSERT INTO `tb_transactions` VALUES ('375', '375', '2016-12-02', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:33:07');
INSERT INTO `tb_transactions` VALUES ('376', '376', '2016-12-05', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:33:25');
INSERT INTO `tb_transactions` VALUES ('377', '377', '2016-12-07', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:33:44');
INSERT INTO `tb_transactions` VALUES ('378', '378', '2016-12-13', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:34:01');
INSERT INTO `tb_transactions` VALUES ('379', '379', '2016-12-16', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:34:20');
INSERT INTO `tb_transactions` VALUES ('380', '380', '2016-12-17', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:34:39');
INSERT INTO `tb_transactions` VALUES ('381', '381', '2016-12-20', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:34:58');
INSERT INTO `tb_transactions` VALUES ('382', '382', '2016-12-22', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:35:15');
INSERT INTO `tb_transactions` VALUES ('383', '383', '2016-12-23', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:35:35');
INSERT INTO `tb_transactions` VALUES ('384', '384', '2016-12-24', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:35:53');
INSERT INTO `tb_transactions` VALUES ('385', '385', '2016-12-27', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:36:11');
INSERT INTO `tb_transactions` VALUES ('386', '386', '2016-12-29', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:36:35');
INSERT INTO `tb_transactions` VALUES ('387', '387', '2016-12-30', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:36:52');
INSERT INTO `tb_transactions` VALUES ('388', '388', '2016-12-01', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:37:21');
INSERT INTO `tb_transactions` VALUES ('389', '389', '2016-12-02', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:37:39');
INSERT INTO `tb_transactions` VALUES ('390', '390', '2016-12-09', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:38:06');
INSERT INTO `tb_transactions` VALUES ('391', '391', '2016-12-10', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:38:27');
INSERT INTO `tb_transactions` VALUES ('392', '392', '2016-12-13', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:38:48');
INSERT INTO `tb_transactions` VALUES ('393', '393', '2016-12-15', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:39:08');
INSERT INTO `tb_transactions` VALUES ('394', '394', '2016-12-18', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:39:32');
INSERT INTO `tb_transactions` VALUES ('395', '395', '2016-12-20', '44', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:39:53');
INSERT INTO `tb_transactions` VALUES ('396', '396', '2016-12-22', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:40:14');
INSERT INTO `tb_transactions` VALUES ('397', '397', '2016-12-23', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:40:35');
INSERT INTO `tb_transactions` VALUES ('398', '398', '2016-12-24', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:40:52');
INSERT INTO `tb_transactions` VALUES ('399', '399', '2016-12-25', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:41:23');
INSERT INTO `tb_transactions` VALUES ('400', '400', '2016-12-26', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:41:42');
INSERT INTO `tb_transactions` VALUES ('401', '401', '2016-12-28', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:42:09');
INSERT INTO `tb_transactions` VALUES ('402', '402', '2016-12-01', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:51:14');
INSERT INTO `tb_transactions` VALUES ('403', '403', '2016-12-02', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:51:33');
INSERT INTO `tb_transactions` VALUES ('404', '404', '2016-12-03', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:51:53');
INSERT INTO `tb_transactions` VALUES ('405', '405', '2016-12-08', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:52:12');
INSERT INTO `tb_transactions` VALUES ('406', '406', '2016-12-09', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:52:37');
INSERT INTO `tb_transactions` VALUES ('407', '407', '2016-12-13', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:52:58');
INSERT INTO `tb_transactions` VALUES ('408', '408', '2016-12-14', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:53:16');
INSERT INTO `tb_transactions` VALUES ('409', '409', '2016-12-15', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:53:32');
INSERT INTO `tb_transactions` VALUES ('410', '410', '2016-12-16', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:53:51');
INSERT INTO `tb_transactions` VALUES ('411', '411', '2016-12-19', '45', '4', '1', '300.00', '3', '14', '20', '2017-01-02 10:54:09');
INSERT INTO `tb_transactions` VALUES ('412', '412', '2016-12-20', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:54:34');
INSERT INTO `tb_transactions` VALUES ('413', '413', '2016-12-21', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:54:50');
INSERT INTO `tb_transactions` VALUES ('414', '414', '2016-12-22', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:55:10');
INSERT INTO `tb_transactions` VALUES ('415', '415', '2016-12-23', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:55:30');
INSERT INTO `tb_transactions` VALUES ('416', '416', '2016-12-24', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:55:47');
INSERT INTO `tb_transactions` VALUES ('417', '417', '2016-12-26', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:56:08');
INSERT INTO `tb_transactions` VALUES ('418', '418', '2016-12-28', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-02 10:56:26');
INSERT INTO `tb_transactions` VALUES ('420', '420', '2017-01-01', '46', '0', '1', '200.00', '1', '14', '5', '2017-01-02 11:10:25');
INSERT INTO `tb_transactions` VALUES ('421', '421', '2017-01-02', '46', '0', '1', '200.00', '1', '14', '5', '2017-01-02 11:11:23');
INSERT INTO `tb_transactions` VALUES ('422', '422', '2017-01-02', '47', '0', '1', '30800.00', '1', '14', '5', '2017-01-02 11:16:43');
INSERT INTO `tb_transactions` VALUES ('423', '423', '2017-01-02', '48', '0', '1', '96700.00', '1', '14', '5', '2017-01-02 11:18:40');
INSERT INTO `tb_transactions` VALUES ('424', '424', '2017-01-02', '49', '0', '1', '95000.00', '1', '14', '5', '2017-01-02 11:19:21');
INSERT INTO `tb_transactions` VALUES ('425', '425', '2017-01-02', '50', '0', '1', '13000.00', '1', '14', '5', '2017-01-02 11:20:04');
INSERT INTO `tb_transactions` VALUES ('426', '426', '2017-01-02', '51', '0', '1', '170000.00', '1', '14', '5', '2017-01-02 11:21:41');
INSERT INTO `tb_transactions` VALUES ('427', '427', '2017-01-02', '52', '0', '1', '82400.00', '1', '14', '5', '2017-01-02 11:22:58');
INSERT INTO `tb_transactions` VALUES ('428', '428', '2017-01-02', '53', '0', '1', '2300.00', '1', '14', '5', '2017-01-02 11:24:16');
INSERT INTO `tb_transactions` VALUES ('429', '429', '2017-01-02', '55', '0', '1', '15000.00', '1', '14', '5', '2017-01-02 11:28:14');
INSERT INTO `tb_transactions` VALUES ('430', '430', '2016-12-22', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:13:16');
INSERT INTO `tb_transactions` VALUES ('431', '431', '2016-12-23', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:14:22');
INSERT INTO `tb_transactions` VALUES ('432', '432', '2016-12-24', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:14:53');
INSERT INTO `tb_transactions` VALUES ('433', '433', '2016-12-26', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:15:23');
INSERT INTO `tb_transactions` VALUES ('434', '434', '2016-12-27', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:15:59');
INSERT INTO `tb_transactions` VALUES ('436', '436', '2016-12-25', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:18:18');
INSERT INTO `tb_transactions` VALUES ('437', '437', '2017-01-01', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:19:48');
INSERT INTO `tb_transactions` VALUES ('438', '438', '2017-01-02', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:20:12');
INSERT INTO `tb_transactions` VALUES ('439', '439', '2016-12-29', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-04 13:21:44');
INSERT INTO `tb_transactions` VALUES ('442', '442', '2016-12-29', '14', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:24:10');
INSERT INTO `tb_transactions` VALUES ('443', '443', '2016-12-30', '14', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:24:52');
INSERT INTO `tb_transactions` VALUES ('444', '444', '2016-12-31', '14', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:25:31');
INSERT INTO `tb_transactions` VALUES ('445', '445', '2017-01-01', '14', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:26:05');
INSERT INTO `tb_transactions` VALUES ('446', '446', '2017-01-02', '14', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:26:33');
INSERT INTO `tb_transactions` VALUES ('448', '448', '2016-12-30', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:33:38');
INSERT INTO `tb_transactions` VALUES ('449', '449', '2016-12-31', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:34:21');
INSERT INTO `tb_transactions` VALUES ('450', '450', '2017-01-02', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:35:05');
INSERT INTO `tb_transactions` VALUES ('451', '451', '2017-01-03', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-04 13:35:42');
INSERT INTO `tb_transactions` VALUES ('452', '452', '2016-12-29', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:38:27');
INSERT INTO `tb_transactions` VALUES ('453', '453', '2016-12-31', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:38:59');
INSERT INTO `tb_transactions` VALUES ('454', '454', '2016-12-23', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:41:30');
INSERT INTO `tb_transactions` VALUES ('455', '455', '2016-12-24', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:42:03');
INSERT INTO `tb_transactions` VALUES ('456', '456', '2016-12-25', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:45:30');
INSERT INTO `tb_transactions` VALUES ('457', '457', '2016-12-26', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-04 13:46:21');
INSERT INTO `tb_transactions` VALUES ('458', '458', '2016-12-27', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:13:49');
INSERT INTO `tb_transactions` VALUES ('459', '459', '2016-12-28', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:14:29');
INSERT INTO `tb_transactions` VALUES ('460', '460', '2016-12-30', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:15:09');
INSERT INTO `tb_transactions` VALUES ('461', '461', '2016-12-31', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:15:49');
INSERT INTO `tb_transactions` VALUES ('462', '462', '2017-01-01', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:16:27');
INSERT INTO `tb_transactions` VALUES ('463', '463', '2017-01-02', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:17:04');
INSERT INTO `tb_transactions` VALUES ('464', '464', '2017-01-03', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:17:41');
INSERT INTO `tb_transactions` VALUES ('465', '465', '2017-01-04', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:18:21');
INSERT INTO `tb_transactions` VALUES ('466', '466', '2017-01-04', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-05 14:23:56');
INSERT INTO `tb_transactions` VALUES ('467', '467', '2017-01-03', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:28:58');
INSERT INTO `tb_transactions` VALUES ('468', '468', '2017-01-04', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-05 14:29:50');
INSERT INTO `tb_transactions` VALUES ('469', '469', '2016-12-23', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:32:36');
INSERT INTO `tb_transactions` VALUES ('470', '470', '2016-12-28', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:33:36');
INSERT INTO `tb_transactions` VALUES ('471', '471', '2016-12-29', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:34:32');
INSERT INTO `tb_transactions` VALUES ('472', '472', '2016-12-30', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:35:11');
INSERT INTO `tb_transactions` VALUES ('473', '473', '2016-12-31', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:35:54');
INSERT INTO `tb_transactions` VALUES ('474', '474', '2017-01-02', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:36:57');
INSERT INTO `tb_transactions` VALUES ('475', '475', '2017-01-03', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:37:41');
INSERT INTO `tb_transactions` VALUES ('476', '476', '2017-01-03', '46', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:40:33');
INSERT INTO `tb_transactions` VALUES ('477', '477', '2016-12-23', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:43:24');
INSERT INTO `tb_transactions` VALUES ('478', '478', '2016-12-25', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:44:26');
INSERT INTO `tb_transactions` VALUES ('479', '479', '2016-12-26', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:45:07');
INSERT INTO `tb_transactions` VALUES ('480', '480', '2016-12-27', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:46:02');
INSERT INTO `tb_transactions` VALUES ('481', '481', '2016-12-28', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:46:47');
INSERT INTO `tb_transactions` VALUES ('482', '482', '2016-12-29', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:47:25');
INSERT INTO `tb_transactions` VALUES ('483', '483', '2017-01-02', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:48:39');
INSERT INTO `tb_transactions` VALUES ('484', '484', '2017-01-03', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:49:21');
INSERT INTO `tb_transactions` VALUES ('485', '485', '2017-01-04', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:49:56');
INSERT INTO `tb_transactions` VALUES ('486', '486', '2016-12-23', '38', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:58:08');
INSERT INTO `tb_transactions` VALUES ('487', '487', '2016-12-26', '38', '0', '1', '200.00', '1', '14', '5', '2017-01-05 14:59:21');
INSERT INTO `tb_transactions` VALUES ('488', '488', '2017-01-03', '38', '0', '1', '200.00', '1', '14', '5', '2017-01-05 15:00:09');
INSERT INTO `tb_transactions` VALUES ('489', '489', '2016-12-23', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:04:54');
INSERT INTO `tb_transactions` VALUES ('490', '490', '2016-12-24', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:05:53');
INSERT INTO `tb_transactions` VALUES ('491', '491', '2016-12-25', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:06:33');
INSERT INTO `tb_transactions` VALUES ('492', '492', '2016-12-26', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:07:20');
INSERT INTO `tb_transactions` VALUES ('493', '493', '2016-12-27', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:07:58');
INSERT INTO `tb_transactions` VALUES ('494', '494', '2016-12-28', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:08:46');
INSERT INTO `tb_transactions` VALUES ('495', '495', '2016-12-29', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:09:27');
INSERT INTO `tb_transactions` VALUES ('496', '496', '2016-12-30', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:10:05');
INSERT INTO `tb_transactions` VALUES ('497', '497', '2016-12-31', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:10:49');
INSERT INTO `tb_transactions` VALUES ('498', '498', '2017-01-01', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:11:28');
INSERT INTO `tb_transactions` VALUES ('499', '499', '2017-01-02', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:12:03');
INSERT INTO `tb_transactions` VALUES ('500', '500', '2017-01-03', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:12:41');
INSERT INTO `tb_transactions` VALUES ('501', '501', '2017-01-04', '28', '0', '1', '500.00', '1', '14', '5', '2017-01-05 15:13:14');
INSERT INTO `tb_transactions` VALUES ('502', '502', '2017-01-05', '56', '0', '1', '5600.00', '1', '14', '5', '2017-01-05 15:21:35');
INSERT INTO `tb_transactions` VALUES ('503', '503', '2016-12-30', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:54:06');
INSERT INTO `tb_transactions` VALUES ('504', '504', '2016-12-31', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:54:23');
INSERT INTO `tb_transactions` VALUES ('505', '505', '2017-01-01', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:54:47');
INSERT INTO `tb_transactions` VALUES ('506', '506', '2017-01-03', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:55:10');
INSERT INTO `tb_transactions` VALUES ('507', '507', '2017-01-04', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:55:25');
INSERT INTO `tb_transactions` VALUES ('508', '508', '2017-01-05', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:55:39');
INSERT INTO `tb_transactions` VALUES ('509', '509', '2017-01-06', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 11:56:05');
INSERT INTO `tb_transactions` VALUES ('510', '510', '2017-01-07', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:00:26');
INSERT INTO `tb_transactions` VALUES ('511', '511', '2017-01-08', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:00:45');
INSERT INTO `tb_transactions` VALUES ('512', '512', '2017-01-09', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:02:15');
INSERT INTO `tb_transactions` VALUES ('513', '513', '2016-12-30', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:31:59');
INSERT INTO `tb_transactions` VALUES ('514', '514', '2016-12-31', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:32:14');
INSERT INTO `tb_transactions` VALUES ('515', '515', '2017-01-01', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:32:32');
INSERT INTO `tb_transactions` VALUES ('516', '516', '2017-01-02', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:32:55');
INSERT INTO `tb_transactions` VALUES ('517', '517', '2017-01-03', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:34:07');
INSERT INTO `tb_transactions` VALUES ('518', '518', '2017-01-04', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:34:23');
INSERT INTO `tb_transactions` VALUES ('519', '519', '2017-01-05', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:34:46');
INSERT INTO `tb_transactions` VALUES ('520', '520', '2017-01-06', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:34:59');
INSERT INTO `tb_transactions` VALUES ('521', '521', '2017-01-07', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:35:14');
INSERT INTO `tb_transactions` VALUES ('522', '522', '2017-01-08', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:35:29');
INSERT INTO `tb_transactions` VALUES ('523', '523', '2017-01-09', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:35:42');
INSERT INTO `tb_transactions` VALUES ('524', '524', '2017-01-03', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:36:44');
INSERT INTO `tb_transactions` VALUES ('525', '525', '2017-01-04', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:37:01');
INSERT INTO `tb_transactions` VALUES ('526', '526', '2017-01-06', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:37:20');
INSERT INTO `tb_transactions` VALUES ('527', '527', '2017-01-09', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:37:42');
INSERT INTO `tb_transactions` VALUES ('528', '528', '2017-01-04', '13', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:38:48');
INSERT INTO `tb_transactions` VALUES ('529', '529', '2017-01-06', '13', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:39:05');
INSERT INTO `tb_transactions` VALUES ('530', '530', '2016-12-30', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:40:27');
INSERT INTO `tb_transactions` VALUES ('531', '531', '2016-12-31', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:40:46');
INSERT INTO `tb_transactions` VALUES ('532', '532', '2017-01-01', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:40:58');
INSERT INTO `tb_transactions` VALUES ('533', '533', '2017-01-02', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:41:13');
INSERT INTO `tb_transactions` VALUES ('534', '534', '2017-01-03', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:41:27');
INSERT INTO `tb_transactions` VALUES ('535', '535', '2017-01-04', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:41:42');
INSERT INTO `tb_transactions` VALUES ('536', '536', '2017-01-05', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:41:56');
INSERT INTO `tb_transactions` VALUES ('537', '537', '2017-01-06', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:42:10');
INSERT INTO `tb_transactions` VALUES ('538', '538', '2017-01-07', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:42:34');
INSERT INTO `tb_transactions` VALUES ('539', '539', '2017-01-08', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:42:56');
INSERT INTO `tb_transactions` VALUES ('540', '540', '2017-01-09', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-11 12:43:15');
INSERT INTO `tb_transactions` VALUES ('541', '541', '2016-12-21', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:24:05');
INSERT INTO `tb_transactions` VALUES ('542', '542', '2016-12-22', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:24:25');
INSERT INTO `tb_transactions` VALUES ('545', '545', '2016-12-27', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:25:31');
INSERT INTO `tb_transactions` VALUES ('547', '547', '2017-01-03', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:26:21');
INSERT INTO `tb_transactions` VALUES ('548', '548', '2017-01-05', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:26:46');
INSERT INTO `tb_transactions` VALUES ('549', '549', '2017-01-07', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:27:11');
INSERT INTO `tb_transactions` VALUES ('550', '550', '2017-01-09', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:27:32');
INSERT INTO `tb_transactions` VALUES ('551', '551', '2016-12-31', '18', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:34:07');
INSERT INTO `tb_transactions` VALUES ('552', '552', '2017-01-02', '18', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:34:56');
INSERT INTO `tb_transactions` VALUES ('553', '553', '2017-01-06', '18', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:35:17');
INSERT INTO `tb_transactions` VALUES ('554', '554', '2017-01-07', '18', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:35:38');
INSERT INTO `tb_transactions` VALUES ('555', '555', '2017-01-09', '18', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:35:59');
INSERT INTO `tb_transactions` VALUES ('556', '556', '2016-12-31', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:37:19');
INSERT INTO `tb_transactions` VALUES ('557', '557', '2017-01-01', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:37:39');
INSERT INTO `tb_transactions` VALUES ('558', '558', '2017-01-04', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:38:25');
INSERT INTO `tb_transactions` VALUES ('559', '559', '2017-01-05', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:38:45');
INSERT INTO `tb_transactions` VALUES ('560', '560', '2017-01-07', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:39:09');
INSERT INTO `tb_transactions` VALUES ('561', '561', '2017-01-10', '19', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:39:32');
INSERT INTO `tb_transactions` VALUES ('562', '562', '2016-12-31', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:43:28');
INSERT INTO `tb_transactions` VALUES ('563', '563', '2017-01-02', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:44:01');
INSERT INTO `tb_transactions` VALUES ('564', '564', '2017-01-03', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:44:19');
INSERT INTO `tb_transactions` VALUES ('565', '565', '2017-01-04', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:44:39');
INSERT INTO `tb_transactions` VALUES ('566', '566', '2017-01-05', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:44:56');
INSERT INTO `tb_transactions` VALUES ('567', '567', '2017-01-06', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:45:14');
INSERT INTO `tb_transactions` VALUES ('568', '568', '2017-01-09', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:45:36');
INSERT INTO `tb_transactions` VALUES ('569', '569', '2017-01-10', '24', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:45:56');
INSERT INTO `tb_transactions` VALUES ('570', '570', '2017-01-03', '22', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:48:03');
INSERT INTO `tb_transactions` VALUES ('571', '571', '2017-01-02', '23', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:49:27');
INSERT INTO `tb_transactions` VALUES ('572', '572', '2017-01-03', '23', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:49:41');
INSERT INTO `tb_transactions` VALUES ('573', '573', '2017-01-06', '23', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:50:00');
INSERT INTO `tb_transactions` VALUES ('574', '574', '2016-12-30', '27', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:53:22');
INSERT INTO `tb_transactions` VALUES ('575', '575', '2016-12-31', '27', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:53:41');
INSERT INTO `tb_transactions` VALUES ('576', '576', '2017-01-05', '27', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:54:08');
INSERT INTO `tb_transactions` VALUES ('577', '577', '2017-01-06', '27', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:54:26');
INSERT INTO `tb_transactions` VALUES ('578', '578', '2017-01-08', '27', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:54:45');
INSERT INTO `tb_transactions` VALUES ('579', '579', '2016-12-31', '28', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:56:18');
INSERT INTO `tb_transactions` VALUES ('580', '580', '2017-01-04', '28', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:56:46');
INSERT INTO `tb_transactions` VALUES ('581', '581', '2017-01-05', '28', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:57:18');
INSERT INTO `tb_transactions` VALUES ('582', '582', '2017-01-07', '28', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:57:37');
INSERT INTO `tb_transactions` VALUES ('583', '583', '2017-01-09', '28', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:58:00');
INSERT INTO `tb_transactions` VALUES ('584', '584', '2016-12-31', '29', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:59:24');
INSERT INTO `tb_transactions` VALUES ('585', '585', '2017-01-07', '29', '4', '1', '200.00', '3', '14', '20', '2017-01-12 10:59:46');
INSERT INTO `tb_transactions` VALUES ('586', '586', '2017-01-09', '29', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:00:05');
INSERT INTO `tb_transactions` VALUES ('587', '587', '2017-01-04', '30', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:03:46');
INSERT INTO `tb_transactions` VALUES ('588', '588', '2017-01-05', '30', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:04:07');
INSERT INTO `tb_transactions` VALUES ('589', '589', '2017-01-07', '30', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:04:25');
INSERT INTO `tb_transactions` VALUES ('590', '590', '2016-12-30', '32', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:10:57');
INSERT INTO `tb_transactions` VALUES ('591', '591', '2017-01-09', '33', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:14:17');
INSERT INTO `tb_transactions` VALUES ('592', '592', '2017-01-10', '33', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:14:33');
INSERT INTO `tb_transactions` VALUES ('593', '593', '2016-12-30', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:16:09');
INSERT INTO `tb_transactions` VALUES ('594', '594', '2016-12-31', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:16:28');
INSERT INTO `tb_transactions` VALUES ('595', '595', '2017-01-01', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:17:02');
INSERT INTO `tb_transactions` VALUES ('596', '596', '2017-01-02', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:17:21');
INSERT INTO `tb_transactions` VALUES ('597', '597', '2017-01-03', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:17:40');
INSERT INTO `tb_transactions` VALUES ('598', '598', '2017-01-04', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:18:01');
INSERT INTO `tb_transactions` VALUES ('599', '599', '2017-01-05', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:18:15');
INSERT INTO `tb_transactions` VALUES ('600', '600', '2017-01-06', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:18:32');
INSERT INTO `tb_transactions` VALUES ('601', '601', '2017-01-07', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-12 11:18:56');
INSERT INTO `tb_transactions` VALUES ('602', '602', '2017-01-13', '30', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:14:31');
INSERT INTO `tb_transactions` VALUES ('603', '603', '2017-01-08', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:23:44');
INSERT INTO `tb_transactions` VALUES ('604', '604', '2017-01-09', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:23:57');
INSERT INTO `tb_transactions` VALUES ('605', '605', '2017-01-11', '35', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:24:13');
INSERT INTO `tb_transactions` VALUES ('606', '606', '2017-01-31', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:26:04');
INSERT INTO `tb_transactions` VALUES ('607', '607', '2017-01-01', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:26:24');
INSERT INTO `tb_transactions` VALUES ('608', '608', '2017-01-02', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:26:42');
INSERT INTO `tb_transactions` VALUES ('609', '609', '2017-01-03', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:26:59');
INSERT INTO `tb_transactions` VALUES ('610', '610', '2017-01-04', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:27:20');
INSERT INTO `tb_transactions` VALUES ('611', '611', '2017-01-05', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:27:37');
INSERT INTO `tb_transactions` VALUES ('612', '612', '2017-01-06', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:27:54');
INSERT INTO `tb_transactions` VALUES ('613', '613', '2017-01-07', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:28:32');
INSERT INTO `tb_transactions` VALUES ('614', '614', '2017-01-08', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:28:50');
INSERT INTO `tb_transactions` VALUES ('615', '615', '2017-01-09', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:29:14');
INSERT INTO `tb_transactions` VALUES ('616', '616', '2017-01-11', '36', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:29:32');
INSERT INTO `tb_transactions` VALUES ('617', '617', '2016-12-31', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:30:51');
INSERT INTO `tb_transactions` VALUES ('618', '618', '2017-01-02', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:31:10');
INSERT INTO `tb_transactions` VALUES ('619', '619', '2017-01-03', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:31:25');
INSERT INTO `tb_transactions` VALUES ('620', '620', '2017-01-04', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:31:42');
INSERT INTO `tb_transactions` VALUES ('621', '621', '2017-01-05', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:31:58');
INSERT INTO `tb_transactions` VALUES ('622', '622', '2017-01-06', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:32:17');
INSERT INTO `tb_transactions` VALUES ('623', '623', '2017-01-07', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:32:35');
INSERT INTO `tb_transactions` VALUES ('624', '624', '2017-01-09', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:32:52');
INSERT INTO `tb_transactions` VALUES ('625', '625', '2017-01-10', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:33:09');
INSERT INTO `tb_transactions` VALUES ('626', '626', '2017-01-13', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:33:29');
INSERT INTO `tb_transactions` VALUES ('627', '627', '2017-01-14', '37', '4', '1', '200.00', '3', '14', '20', '2017-01-16 11:33:44');
INSERT INTO `tb_transactions` VALUES ('628', '628', '2017-01-02', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:22:27');
INSERT INTO `tb_transactions` VALUES ('629', '629', '2017-01-03', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:22:47');
INSERT INTO `tb_transactions` VALUES ('630', '630', '2017-01-04', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:23:03');
INSERT INTO `tb_transactions` VALUES ('631', '631', '2017-01-06', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:23:28');
INSERT INTO `tb_transactions` VALUES ('632', '632', '2017-01-09', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:23:45');
INSERT INTO `tb_transactions` VALUES ('633', '633', '2017-01-11', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:24:03');
INSERT INTO `tb_transactions` VALUES ('634', '634', '2017-01-12', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:26:20');
INSERT INTO `tb_transactions` VALUES ('635', '635', '2017-01-13', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:26:44');
INSERT INTO `tb_transactions` VALUES ('636', '636', '2017-01-14', '38', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:27:13');
INSERT INTO `tb_transactions` VALUES ('637', '637', '2017-01-12', '41', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:31:21');
INSERT INTO `tb_transactions` VALUES ('638', '638', '2017-01-03', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:33:19');
INSERT INTO `tb_transactions` VALUES ('639', '639', '2017-01-04', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:33:40');
INSERT INTO `tb_transactions` VALUES ('640', '640', '2017-01-06', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:33:58');
INSERT INTO `tb_transactions` VALUES ('641', '641', '2017-01-11', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:34:40');
INSERT INTO `tb_transactions` VALUES ('642', '642', '2017-01-13', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:35:03');
INSERT INTO `tb_transactions` VALUES ('643', '643', '2017-01-14', '42', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:35:19');
INSERT INTO `tb_transactions` VALUES ('644', '644', '2016-12-31', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:36:54');
INSERT INTO `tb_transactions` VALUES ('645', '645', '2017-01-01', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:37:20');
INSERT INTO `tb_transactions` VALUES ('646', '646', '2017-01-02', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:37:33');
INSERT INTO `tb_transactions` VALUES ('647', '647', '2017-01-03', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:37:47');
INSERT INTO `tb_transactions` VALUES ('648', '648', '2017-01-04', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:38:02');
INSERT INTO `tb_transactions` VALUES ('649', '649', '2017-01-05', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:38:16');
INSERT INTO `tb_transactions` VALUES ('650', '650', '2017-01-06', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:38:33');
INSERT INTO `tb_transactions` VALUES ('651', '651', '2017-01-11', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:38:55');
INSERT INTO `tb_transactions` VALUES ('652', '652', '2017-01-13', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:39:11');
INSERT INTO `tb_transactions` VALUES ('653', '653', '2017-01-14', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:39:32');
INSERT INTO `tb_transactions` VALUES ('654', '654', '2017-01-15', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:39:48');
INSERT INTO `tb_transactions` VALUES ('655', '655', '2017-01-16', '44', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:40:05');
INSERT INTO `tb_transactions` VALUES ('656', '656', '2016-12-30', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:42:14');
INSERT INTO `tb_transactions` VALUES ('657', '657', '2016-12-31', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:42:29');
INSERT INTO `tb_transactions` VALUES ('658', '658', '2017-01-01', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:42:52');
INSERT INTO `tb_transactions` VALUES ('659', '659', '2017-01-02', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:43:11');
INSERT INTO `tb_transactions` VALUES ('660', '660', '2017-01-03', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:43:24');
INSERT INTO `tb_transactions` VALUES ('661', '661', '2017-01-04', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:43:37');
INSERT INTO `tb_transactions` VALUES ('662', '662', '2017-01-05', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:43:50');
INSERT INTO `tb_transactions` VALUES ('663', '663', '2017-01-06', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:44:03');
INSERT INTO `tb_transactions` VALUES ('664', '664', '2017-01-07', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:44:25');
INSERT INTO `tb_transactions` VALUES ('665', '665', '2017-01-09', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:44:43');
INSERT INTO `tb_transactions` VALUES ('666', '666', '2017-01-10', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:44:59');
INSERT INTO `tb_transactions` VALUES ('667', '667', '2017-01-11', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:45:14');
INSERT INTO `tb_transactions` VALUES ('668', '668', '2017-01-12', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:45:27');
INSERT INTO `tb_transactions` VALUES ('669', '669', '2017-01-13', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:45:43');
INSERT INTO `tb_transactions` VALUES ('670', '670', '2017-01-14', '45', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:45:59');
INSERT INTO `tb_transactions` VALUES ('672', '672', '2017-01-10', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:50:31');
INSERT INTO `tb_transactions` VALUES ('673', '673', '2017-01-12', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:50:48');
INSERT INTO `tb_transactions` VALUES ('674', '674', '2017-01-13', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:51:02');
INSERT INTO `tb_transactions` VALUES ('675', '675', '2017-01-14', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:51:18');
INSERT INTO `tb_transactions` VALUES ('676', '676', '2017-01-15', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:51:31');
INSERT INTO `tb_transactions` VALUES ('677', '677', '2017-01-16', '10', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:51:58');
INSERT INTO `tb_transactions` VALUES ('678', '678', '2017-01-10', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:53:18');
INSERT INTO `tb_transactions` VALUES ('679', '679', '2017-01-11', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:53:40');
INSERT INTO `tb_transactions` VALUES ('680', '680', '2017-01-12', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 12:54:01');
INSERT INTO `tb_transactions` VALUES ('681', '681', '2017-01-13', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:09:20');
INSERT INTO `tb_transactions` VALUES ('682', '682', '2017-01-14', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:09:46');
INSERT INTO `tb_transactions` VALUES ('683', '683', '2017-01-15', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:10:14');
INSERT INTO `tb_transactions` VALUES ('684', '684', '2017-01-16', '11', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:10:29');
INSERT INTO `tb_transactions` VALUES ('685', '685', '2017-01-11', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:12:55');
INSERT INTO `tb_transactions` VALUES ('686', '686', '2017-01-13', '12', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:13:16');
INSERT INTO `tb_transactions` VALUES ('687', '687', '2017-01-10', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:18:31');
INSERT INTO `tb_transactions` VALUES ('688', '688', '2017-01-11', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:18:47');
INSERT INTO `tb_transactions` VALUES ('689', '689', '2017-01-12', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:19:01');
INSERT INTO `tb_transactions` VALUES ('690', '690', '2017-01-13', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:19:17');
INSERT INTO `tb_transactions` VALUES ('691', '691', '2017-01-14', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:19:44');
INSERT INTO `tb_transactions` VALUES ('692', '692', '2017-01-15', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:20:01');
INSERT INTO `tb_transactions` VALUES ('693', '693', '2017-01-16', '14', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:20:18');
INSERT INTO `tb_transactions` VALUES ('694', '694', '2017-01-10', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:24:46');
INSERT INTO `tb_transactions` VALUES ('695', '695', '2017-01-11', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:24:59');
INSERT INTO `tb_transactions` VALUES ('696', '696', '2017-01-13', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:25:17');
INSERT INTO `tb_transactions` VALUES ('697', '697', '2017-01-14', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-17 13:25:36');
INSERT INTO `tb_transactions` VALUES ('698', '698', '2017-01-02', '15', '4', '1', '200.00', '3', '14', '20', '2017-01-17 15:05:34');
INSERT INTO `tb_transactions` VALUES ('701', '701', '2016-12-28', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:00:09');
INSERT INTO `tb_transactions` VALUES ('704', '704', '2017-01-03', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:03:30');
INSERT INTO `tb_transactions` VALUES ('705', '705', '2017-01-04', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:04:34');
INSERT INTO `tb_transactions` VALUES ('706', '706', '2017-01-05', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:05:25');
INSERT INTO `tb_transactions` VALUES ('707', '707', '2017-01-06', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:08:15');
INSERT INTO `tb_transactions` VALUES ('708', '708', '2017-01-07', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:09:09');
INSERT INTO `tb_transactions` VALUES ('709', '709', '2017-01-08', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:10:15');
INSERT INTO `tb_transactions` VALUES ('710', '710', '2017-01-09', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:11:22');
INSERT INTO `tb_transactions` VALUES ('711', '711', '2017-01-10', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:12:27');
INSERT INTO `tb_transactions` VALUES ('712', '712', '2017-01-11', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:14:08');
INSERT INTO `tb_transactions` VALUES ('713', '713', '2017-01-12', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:15:14');
INSERT INTO `tb_transactions` VALUES ('714', '714', '2017-01-13', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:16:26');
INSERT INTO `tb_transactions` VALUES ('715', '715', '2017-01-14', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:17:20');
INSERT INTO `tb_transactions` VALUES ('716', '716', '2017-01-15', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:18:22');
INSERT INTO `tb_transactions` VALUES ('717', '717', '2017-01-16', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:19:19');
INSERT INTO `tb_transactions` VALUES ('720', '720', '2017-01-17', '10', '0', '1', '500.00', '1', '14', '5', '2017-01-24 11:25:24');
INSERT INTO `tb_transactions` VALUES ('721', '721', '2016-12-31', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:34:20');
INSERT INTO `tb_transactions` VALUES ('722', '722', '2017-01-02', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:35:45');
INSERT INTO `tb_transactions` VALUES ('723', '723', '2017-01-03', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:36:41');
INSERT INTO `tb_transactions` VALUES ('724', '724', '2017-01-04', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:37:38');
INSERT INTO `tb_transactions` VALUES ('725', '725', '2017-01-05', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:45:29');
INSERT INTO `tb_transactions` VALUES ('726', '726', '2017-01-07', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:46:45');
INSERT INTO `tb_transactions` VALUES ('727', '727', '2017-01-08', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:47:37');
INSERT INTO `tb_transactions` VALUES ('728', '728', '2017-01-09', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:48:37');
INSERT INTO `tb_transactions` VALUES ('729', '729', '2017-01-10', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:49:51');
INSERT INTO `tb_transactions` VALUES ('730', '730', '2017-01-11', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:50:40');
INSERT INTO `tb_transactions` VALUES ('731', '731', '2017-01-12', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:51:36');
INSERT INTO `tb_transactions` VALUES ('732', '732', '2017-01-13', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:52:44');
INSERT INTO `tb_transactions` VALUES ('733', '733', '2017-01-14', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:53:43');
INSERT INTO `tb_transactions` VALUES ('734', '734', '2017-01-15', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:54:49');
INSERT INTO `tb_transactions` VALUES ('735', '735', '2017-01-16', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:55:47');
INSERT INTO `tb_transactions` VALUES ('736', '736', '2017-01-17', '11', '0', '1', '200.00', '1', '14', '5', '2017-01-24 11:56:43');
INSERT INTO `tb_transactions` VALUES ('737', '737', '2017-01-05', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:01:35');
INSERT INTO `tb_transactions` VALUES ('738', '738', '2017-01-06', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:02:46');
INSERT INTO `tb_transactions` VALUES ('739', '739', '2017-01-07', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:03:41');
INSERT INTO `tb_transactions` VALUES ('740', '740', '2017-01-09', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:04:54');
INSERT INTO `tb_transactions` VALUES ('741', '741', '2017-01-10', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:06:36');
INSERT INTO `tb_transactions` VALUES ('742', '742', '2017-01-11', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:07:36');
INSERT INTO `tb_transactions` VALUES ('743', '743', '2017-01-12', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:08:32');
INSERT INTO `tb_transactions` VALUES ('744', '744', '2017-01-13', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:09:22');
INSERT INTO `tb_transactions` VALUES ('745', '745', '2017-01-14', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:10:28');
INSERT INTO `tb_transactions` VALUES ('746', '746', '2017-01-16', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:11:33');
INSERT INTO `tb_transactions` VALUES ('747', '747', '2017-01-17', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:12:30');
INSERT INTO `tb_transactions` VALUES ('748', '748', '2017-01-18', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:13:09');
INSERT INTO `tb_transactions` VALUES ('749', '749', '2017-01-19', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:14:11');
INSERT INTO `tb_transactions` VALUES ('750', '750', '2017-01-20', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:14:52');
INSERT INTO `tb_transactions` VALUES ('751', '751', '2017-01-21', '45', '0', '1', '500.00', '1', '14', '5', '2017-01-24 12:15:52');
INSERT INTO `tb_transactions` VALUES ('752', '752', '2017-01-05', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:18:45');
INSERT INTO `tb_transactions` VALUES ('753', '753', '2017-01-07', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:19:34');
INSERT INTO `tb_transactions` VALUES ('754', '754', '2017-01-10', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:30:32');
INSERT INTO `tb_transactions` VALUES ('755', '755', '2017-01-16', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:31:12');
INSERT INTO `tb_transactions` VALUES ('756', '756', '2017-01-17', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:31:56');
INSERT INTO `tb_transactions` VALUES ('757', '757', '2017-01-19', '19', '0', '1', '300.00', '1', '14', '5', '2017-01-24 13:32:59');
INSERT INTO `tb_transactions` VALUES ('758', '758', '2017-01-16', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:39:08');
INSERT INTO `tb_transactions` VALUES ('759', '759', '2017-01-17', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:40:12');
INSERT INTO `tb_transactions` VALUES ('760', '760', '2017-01-18', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:41:19');
INSERT INTO `tb_transactions` VALUES ('761', '761', '2017-01-19', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:42:18');
INSERT INTO `tb_transactions` VALUES ('762', '762', '2017-01-20', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:44:18');
INSERT INTO `tb_transactions` VALUES ('763', '763', '2017-01-21', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:45:15');
INSERT INTO `tb_transactions` VALUES ('764', '764', '2017-01-22', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:46:25');
INSERT INTO `tb_transactions` VALUES ('765', '765', '2017-01-23', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:47:54');
INSERT INTO `tb_transactions` VALUES ('766', '766', '2017-01-24', '14', '0', '1', '200.00', '1', '14', '5', '2017-01-25 11:49:09');
INSERT INTO `tb_transactions` VALUES ('767', '767', '2017-01-23', '15', '0', '1', '10000.00', '1', '14', '5', '2017-01-25 11:57:44');
INSERT INTO `tb_transactions` VALUES ('768', '768', '2017-01-04', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:00:38');
INSERT INTO `tb_transactions` VALUES ('769', '769', '2017-01-05', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:02:33');
INSERT INTO `tb_transactions` VALUES ('770', '770', '2017-01-06', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:03:29');
INSERT INTO `tb_transactions` VALUES ('771', '771', '2017-01-07', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:04:55');
INSERT INTO `tb_transactions` VALUES ('772', '772', '2017-01-09', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:17:30');
INSERT INTO `tb_transactions` VALUES ('773', '773', '2017-01-10', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:18:59');
INSERT INTO `tb_transactions` VALUES ('774', '774', '2017-01-11', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:20:14');
INSERT INTO `tb_transactions` VALUES ('775', '775', '2017-01-12', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:21:37');
INSERT INTO `tb_transactions` VALUES ('776', '776', '2017-01-13', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:22:46');
INSERT INTO `tb_transactions` VALUES ('777', '777', '2017-01-14', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:23:58');
INSERT INTO `tb_transactions` VALUES ('778', '778', '2017-01-15', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:25:06');
INSERT INTO `tb_transactions` VALUES ('779', '779', '2017-01-16', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:34:43');
INSERT INTO `tb_transactions` VALUES ('780', '780', '2017-01-17', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:36:16');
INSERT INTO `tb_transactions` VALUES ('781', '781', '2017-01-18', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:41:03');
INSERT INTO `tb_transactions` VALUES ('782', '782', '2017-01-19', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:43:25');
INSERT INTO `tb_transactions` VALUES ('783', '783', '2017-01-20', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:46:04');
INSERT INTO `tb_transactions` VALUES ('784', '784', '2017-01-23', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:49:27');
INSERT INTO `tb_transactions` VALUES ('785', '785', '2017-01-21', '43', '0', '1', '200.00', '1', '14', '5', '2017-01-25 12:53:06');
INSERT INTO `tb_transactions` VALUES ('786', '786', '2017-01-09', '33', '0', '1', '1500.00', '1', '14', '5', '2017-01-25 12:58:59');
INSERT INTO `tb_transactions` VALUES ('787', '787', '2017-01-17', '33', '0', '1', '200.00', '1', '14', '5', '2017-01-25 13:00:01');
INSERT INTO `tb_transactions` VALUES ('788', '788', '2017-01-18', '33', '0', '1', '1300.00', '1', '14', '5', '2017-01-25 13:01:29');
INSERT INTO `tb_transactions` VALUES ('789', '789', '2017-01-05', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:10:26');
INSERT INTO `tb_transactions` VALUES ('790', '790', '2017-01-06', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:23:16');
INSERT INTO `tb_transactions` VALUES ('791', '791', '2017-01-07', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:26:58');
INSERT INTO `tb_transactions` VALUES ('792', '792', '2017-01-08', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:28:27');
INSERT INTO `tb_transactions` VALUES ('793', '793', '2017-01-09', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:31:20');
INSERT INTO `tb_transactions` VALUES ('794', '794', '2017-01-11', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:32:20');
INSERT INTO `tb_transactions` VALUES ('795', '795', '2017-01-12', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:33:18');
INSERT INTO `tb_transactions` VALUES ('796', '796', '2017-01-13', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:35:26');
INSERT INTO `tb_transactions` VALUES ('797', '797', '2017-01-14', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:36:57');
INSERT INTO `tb_transactions` VALUES ('798', '798', '2017-01-15', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:38:32');
INSERT INTO `tb_transactions` VALUES ('799', '799', '2017-01-16', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:56:36');
INSERT INTO `tb_transactions` VALUES ('800', '800', '2017-01-17', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:58:07');
INSERT INTO `tb_transactions` VALUES ('801', '801', '2017-01-18', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:59:13');
INSERT INTO `tb_transactions` VALUES ('802', '802', '2017-01-19', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 13:59:54');
INSERT INTO `tb_transactions` VALUES ('803', '803', '2017-01-23', '35', '0', '1', '300.00', '1', '14', '5', '2017-01-25 14:00:48');
INSERT INTO `tb_transactions` VALUES ('804', '804', '2017-01-07', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:05:01');
INSERT INTO `tb_transactions` VALUES ('805', '805', '2017-01-08', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:06:25');
INSERT INTO `tb_transactions` VALUES ('806', '806', '2017-01-09', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:08:25');
INSERT INTO `tb_transactions` VALUES ('807', '807', '2017-01-11', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:09:40');
INSERT INTO `tb_transactions` VALUES ('808', '808', '2017-01-12', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:17:41');
INSERT INTO `tb_transactions` VALUES ('809', '809', '2017-01-14', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:18:35');
INSERT INTO `tb_transactions` VALUES ('810', '810', '2017-01-15', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:19:29');
INSERT INTO `tb_transactions` VALUES ('811', '811', '2017-01-19', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:20:24');
INSERT INTO `tb_transactions` VALUES ('812', '812', '2017-01-20', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:21:45');
INSERT INTO `tb_transactions` VALUES ('813', '813', '2017-01-21', '36', '0', '1', '200.00', '1', '14', '5', '2017-01-25 14:22:43');
INSERT INTO `tb_transactions` VALUES ('817', '34', '2016-12-01', '10', '4', '0', '300.00', '3', '20', '14', '2017-02-04 10:43:04');
INSERT INTO `tb_transactions` VALUES ('818', '814', '2017-02-06', '60', '0', '0', '19500.00', '5', '14', '8', '2017-02-05 18:20:26');
INSERT INTO `tb_transactions` VALUES ('819', '814', '2017-02-06', '60', '2', '0', '500.00', '5', '14', '3', '2017-02-05 18:20:26');
INSERT INTO `tb_transactions` VALUES ('820', '815', '2017-02-07', '61', '0', '0', '89350.00', '5', '14', '15', '2017-02-05 18:21:21');
INSERT INTO `tb_transactions` VALUES ('821', '815', '2017-02-07', '61', '1', '0', '150.00', '5', '14', '1', '2017-02-05 18:21:21');
INSERT INTO `tb_transactions` VALUES ('822', '815', '2017-02-07', '61', '2', '0', '500.00', '5', '14', '3', '2017-02-05 18:21:21');
INSERT INTO `tb_transactions` VALUES ('823', '816', '2017-02-07', '63', '0', '0', '19350.00', '5', '14', '8', '2017-02-06 15:41:22');
INSERT INTO `tb_transactions` VALUES ('824', '816', '2017-02-07', '63', '1', '0', '150.00', '5', '14', '1', '2017-02-06 15:41:22');
INSERT INTO `tb_transactions` VALUES ('825', '816', '2017-02-07', '63', '2', '0', '500.00', '5', '14', '3', '2017-02-06 15:41:22');
INSERT INTO `tb_transactions` VALUES ('826', '817', '2017-02-08', '65', '0', '0', '29500.00', '5', '14', '8', '2017-02-06 16:01:57');
INSERT INTO `tb_transactions` VALUES ('827', '817', '2017-02-08', '65', '2', '0', '500.00', '5', '14', '3', '2017-02-06 16:01:57');
INSERT INTO `tb_transactions` VALUES ('828', '818', '2017-02-07', '60', '0', '1', '8000.00', '6', '14', '14', '2017-02-06 16:57:27');
INSERT INTO `tb_transactions` VALUES ('829', '818', '2017-02-07', '60', '0', '1', '12000.00', '8', '14', '9', '2017-02-06 16:57:27');

-- ----------------------------
-- Table structure for tb_userpermissions
-- ----------------------------
DROP TABLE IF EXISTS `tb_userpermissions`;
CREATE TABLE `tb_userpermissions` (
  `permissionid` int(11) NOT NULL AUTO_INCREMENT,
  `profileid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  PRIMARY KEY (`permissionid`),
  KEY `fk_tb_userpermissions_tb_userprofiles` (`profileid`),
  KEY `fk_tb_userpermissions_tb_menuitems` (`itemid`),
  CONSTRAINT `fk_tb_userpermissions_tb_menuitems` FOREIGN KEY (`itemid`) REFERENCES `tb_menuitems` (`itemid`),
  CONSTRAINT `fk_tb_userpermissions_tb_userprofiles` FOREIGN KEY (`profileid`) REFERENCES `tb_userprofiles` (`profileid`)
) ENGINE=InnoDB AUTO_INCREMENT=1009 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_userpermissions
-- ----------------------------
INSERT INTO `tb_userpermissions` VALUES ('665', '4', '21');
INSERT INTO `tb_userpermissions` VALUES ('666', '4', '22');
INSERT INTO `tb_userpermissions` VALUES ('667', '4', '35');
INSERT INTO `tb_userpermissions` VALUES ('668', '4', '23');
INSERT INTO `tb_userpermissions` VALUES ('669', '4', '24');
INSERT INTO `tb_userpermissions` VALUES ('670', '4', '25');
INSERT INTO `tb_userpermissions` VALUES ('671', '4', '26');
INSERT INTO `tb_userpermissions` VALUES ('672', '4', '27');
INSERT INTO `tb_userpermissions` VALUES ('673', '4', '28');
INSERT INTO `tb_userpermissions` VALUES ('674', '4', '29');
INSERT INTO `tb_userpermissions` VALUES ('675', '4', '30');
INSERT INTO `tb_userpermissions` VALUES ('676', '4', '42');
INSERT INTO `tb_userpermissions` VALUES ('687', '3', '1');
INSERT INTO `tb_userpermissions` VALUES ('688', '3', '5');
INSERT INTO `tb_userpermissions` VALUES ('689', '3', '3');
INSERT INTO `tb_userpermissions` VALUES ('690', '3', '21');
INSERT INTO `tb_userpermissions` VALUES ('691', '3', '22');
INSERT INTO `tb_userpermissions` VALUES ('692', '3', '35');
INSERT INTO `tb_userpermissions` VALUES ('693', '3', '23');
INSERT INTO `tb_userpermissions` VALUES ('694', '3', '24');
INSERT INTO `tb_userpermissions` VALUES ('695', '3', '25');
INSERT INTO `tb_userpermissions` VALUES ('696', '3', '26');
INSERT INTO `tb_userpermissions` VALUES ('697', '3', '42');
INSERT INTO `tb_userpermissions` VALUES ('698', '5', '2');
INSERT INTO `tb_userpermissions` VALUES ('699', '5', '10');
INSERT INTO `tb_userpermissions` VALUES ('700', '5', '6');
INSERT INTO `tb_userpermissions` VALUES ('701', '5', '7');
INSERT INTO `tb_userpermissions` VALUES ('702', '5', '8');
INSERT INTO `tb_userpermissions` VALUES ('703', '5', '40');
INSERT INTO `tb_userpermissions` VALUES ('704', '5', '43');
INSERT INTO `tb_userpermissions` VALUES ('705', '5', '12');
INSERT INTO `tb_userpermissions` VALUES ('706', '5', '13');
INSERT INTO `tb_userpermissions` VALUES ('707', '5', '36');
INSERT INTO `tb_userpermissions` VALUES ('708', '5', '20');
INSERT INTO `tb_userpermissions` VALUES ('709', '5', '38');
INSERT INTO `tb_userpermissions` VALUES ('710', '5', '21');
INSERT INTO `tb_userpermissions` VALUES ('711', '5', '22');
INSERT INTO `tb_userpermissions` VALUES ('712', '5', '35');
INSERT INTO `tb_userpermissions` VALUES ('713', '5', '23');
INSERT INTO `tb_userpermissions` VALUES ('714', '5', '24');
INSERT INTO `tb_userpermissions` VALUES ('715', '5', '25');
INSERT INTO `tb_userpermissions` VALUES ('716', '5', '26');
INSERT INTO `tb_userpermissions` VALUES ('717', '5', '27');
INSERT INTO `tb_userpermissions` VALUES ('718', '5', '28');
INSERT INTO `tb_userpermissions` VALUES ('719', '5', '29');
INSERT INTO `tb_userpermissions` VALUES ('720', '5', '30');
INSERT INTO `tb_userpermissions` VALUES ('721', '5', '42');
INSERT INTO `tb_userpermissions` VALUES ('722', '6', '36');
INSERT INTO `tb_userpermissions` VALUES ('723', '6', '21');
INSERT INTO `tb_userpermissions` VALUES ('724', '6', '22');
INSERT INTO `tb_userpermissions` VALUES ('725', '6', '35');
INSERT INTO `tb_userpermissions` VALUES ('726', '6', '23');
INSERT INTO `tb_userpermissions` VALUES ('727', '6', '24');
INSERT INTO `tb_userpermissions` VALUES ('728', '6', '25');
INSERT INTO `tb_userpermissions` VALUES ('729', '6', '26');
INSERT INTO `tb_userpermissions` VALUES ('730', '6', '27');
INSERT INTO `tb_userpermissions` VALUES ('731', '6', '28');
INSERT INTO `tb_userpermissions` VALUES ('732', '6', '29');
INSERT INTO `tb_userpermissions` VALUES ('733', '6', '30');
INSERT INTO `tb_userpermissions` VALUES ('734', '6', '42');
INSERT INTO `tb_userpermissions` VALUES ('894', '7', '1');
INSERT INTO `tb_userpermissions` VALUES ('895', '7', '3');
INSERT INTO `tb_userpermissions` VALUES ('896', '7', '39');
INSERT INTO `tb_userpermissions` VALUES ('897', '7', '21');
INSERT INTO `tb_userpermissions` VALUES ('898', '7', '22');
INSERT INTO `tb_userpermissions` VALUES ('899', '7', '35');
INSERT INTO `tb_userpermissions` VALUES ('900', '7', '23');
INSERT INTO `tb_userpermissions` VALUES ('901', '7', '24');
INSERT INTO `tb_userpermissions` VALUES ('902', '7', '25');
INSERT INTO `tb_userpermissions` VALUES ('903', '7', '26');
INSERT INTO `tb_userpermissions` VALUES ('904', '2', '1');
INSERT INTO `tb_userpermissions` VALUES ('905', '2', '5');
INSERT INTO `tb_userpermissions` VALUES ('906', '2', '6');
INSERT INTO `tb_userpermissions` VALUES ('907', '2', '11');
INSERT INTO `tb_userpermissions` VALUES ('908', '2', '9');
INSERT INTO `tb_userpermissions` VALUES ('909', '2', '39');
INSERT INTO `tb_userpermissions` VALUES ('910', '2', '21');
INSERT INTO `tb_userpermissions` VALUES ('911', '2', '22');
INSERT INTO `tb_userpermissions` VALUES ('912', '2', '35');
INSERT INTO `tb_userpermissions` VALUES ('913', '2', '23');
INSERT INTO `tb_userpermissions` VALUES ('914', '2', '24');
INSERT INTO `tb_userpermissions` VALUES ('915', '2', '25');
INSERT INTO `tb_userpermissions` VALUES ('916', '2', '26');
INSERT INTO `tb_userpermissions` VALUES ('917', '2', '42');
INSERT INTO `tb_userpermissions` VALUES ('918', '9', '1');
INSERT INTO `tb_userpermissions` VALUES ('919', '9', '4');
INSERT INTO `tb_userpermissions` VALUES ('920', '9', '5');
INSERT INTO `tb_userpermissions` VALUES ('921', '9', '2');
INSERT INTO `tb_userpermissions` VALUES ('922', '9', '10');
INSERT INTO `tb_userpermissions` VALUES ('923', '9', '6');
INSERT INTO `tb_userpermissions` VALUES ('924', '9', '7');
INSERT INTO `tb_userpermissions` VALUES ('925', '9', '3');
INSERT INTO `tb_userpermissions` VALUES ('926', '9', '11');
INSERT INTO `tb_userpermissions` VALUES ('927', '9', '8');
INSERT INTO `tb_userpermissions` VALUES ('928', '9', '9');
INSERT INTO `tb_userpermissions` VALUES ('929', '9', '39');
INSERT INTO `tb_userpermissions` VALUES ('930', '9', '40');
INSERT INTO `tb_userpermissions` VALUES ('931', '9', '43');
INSERT INTO `tb_userpermissions` VALUES ('932', '9', '12');
INSERT INTO `tb_userpermissions` VALUES ('933', '9', '13');
INSERT INTO `tb_userpermissions` VALUES ('934', '9', '36');
INSERT INTO `tb_userpermissions` VALUES ('935', '9', '14');
INSERT INTO `tb_userpermissions` VALUES ('936', '9', '15');
INSERT INTO `tb_userpermissions` VALUES ('937', '9', '16');
INSERT INTO `tb_userpermissions` VALUES ('938', '9', '34');
INSERT INTO `tb_userpermissions` VALUES ('939', '9', '17');
INSERT INTO `tb_userpermissions` VALUES ('940', '9', '18');
INSERT INTO `tb_userpermissions` VALUES ('941', '9', '31');
INSERT INTO `tb_userpermissions` VALUES ('942', '9', '32');
INSERT INTO `tb_userpermissions` VALUES ('943', '9', '33');
INSERT INTO `tb_userpermissions` VALUES ('944', '9', '19');
INSERT INTO `tb_userpermissions` VALUES ('945', '9', '37');
INSERT INTO `tb_userpermissions` VALUES ('946', '9', '20');
INSERT INTO `tb_userpermissions` VALUES ('947', '9', '38');
INSERT INTO `tb_userpermissions` VALUES ('948', '9', '21');
INSERT INTO `tb_userpermissions` VALUES ('949', '9', '22');
INSERT INTO `tb_userpermissions` VALUES ('950', '9', '35');
INSERT INTO `tb_userpermissions` VALUES ('951', '9', '23');
INSERT INTO `tb_userpermissions` VALUES ('952', '9', '24');
INSERT INTO `tb_userpermissions` VALUES ('953', '9', '25');
INSERT INTO `tb_userpermissions` VALUES ('954', '9', '26');
INSERT INTO `tb_userpermissions` VALUES ('955', '9', '27');
INSERT INTO `tb_userpermissions` VALUES ('956', '9', '28');
INSERT INTO `tb_userpermissions` VALUES ('957', '9', '29');
INSERT INTO `tb_userpermissions` VALUES ('958', '9', '30');
INSERT INTO `tb_userpermissions` VALUES ('959', '9', '42');
INSERT INTO `tb_userpermissions` VALUES ('960', '9', '44');
INSERT INTO `tb_userpermissions` VALUES ('961', '9', '41');
INSERT INTO `tb_userpermissions` VALUES ('962', '9', '45');
INSERT INTO `tb_userpermissions` VALUES ('963', '9', '46');
INSERT INTO `tb_userpermissions` VALUES ('964', '9', '47');
INSERT INTO `tb_userpermissions` VALUES ('965', '9', '48');
INSERT INTO `tb_userpermissions` VALUES ('966', '9', '49');
INSERT INTO `tb_userpermissions` VALUES ('967', '9', '50');
INSERT INTO `tb_userpermissions` VALUES ('968', '9', '51');
INSERT INTO `tb_userpermissions` VALUES ('969', '9', '52');
INSERT INTO `tb_userpermissions` VALUES ('970', '9', '53');
INSERT INTO `tb_userpermissions` VALUES ('971', '9', '54');
INSERT INTO `tb_userpermissions` VALUES ('972', '9', '55');
INSERT INTO `tb_userpermissions` VALUES ('973', '9', '56');
INSERT INTO `tb_userpermissions` VALUES ('974', '1', '1');
INSERT INTO `tb_userpermissions` VALUES ('975', '1', '2');
INSERT INTO `tb_userpermissions` VALUES ('976', '1', '6');
INSERT INTO `tb_userpermissions` VALUES ('977', '1', '7');
INSERT INTO `tb_userpermissions` VALUES ('978', '1', '3');
INSERT INTO `tb_userpermissions` VALUES ('979', '1', '11');
INSERT INTO `tb_userpermissions` VALUES ('980', '1', '8');
INSERT INTO `tb_userpermissions` VALUES ('981', '1', '39');
INSERT INTO `tb_userpermissions` VALUES ('982', '1', '40');
INSERT INTO `tb_userpermissions` VALUES ('983', '1', '12');
INSERT INTO `tb_userpermissions` VALUES ('984', '1', '13');
INSERT INTO `tb_userpermissions` VALUES ('985', '1', '36');
INSERT INTO `tb_userpermissions` VALUES ('986', '1', '14');
INSERT INTO `tb_userpermissions` VALUES ('987', '1', '15');
INSERT INTO `tb_userpermissions` VALUES ('988', '1', '16');
INSERT INTO `tb_userpermissions` VALUES ('989', '1', '34');
INSERT INTO `tb_userpermissions` VALUES ('990', '1', '17');
INSERT INTO `tb_userpermissions` VALUES ('991', '1', '18');
INSERT INTO `tb_userpermissions` VALUES ('992', '1', '33');
INSERT INTO `tb_userpermissions` VALUES ('993', '1', '19');
INSERT INTO `tb_userpermissions` VALUES ('994', '1', '37');
INSERT INTO `tb_userpermissions` VALUES ('995', '1', '20');
INSERT INTO `tb_userpermissions` VALUES ('996', '1', '38');
INSERT INTO `tb_userpermissions` VALUES ('997', '1', '21');
INSERT INTO `tb_userpermissions` VALUES ('998', '1', '22');
INSERT INTO `tb_userpermissions` VALUES ('999', '1', '35');
INSERT INTO `tb_userpermissions` VALUES ('1000', '1', '23');
INSERT INTO `tb_userpermissions` VALUES ('1001', '1', '25');
INSERT INTO `tb_userpermissions` VALUES ('1002', '1', '26');
INSERT INTO `tb_userpermissions` VALUES ('1003', '1', '27');
INSERT INTO `tb_userpermissions` VALUES ('1004', '1', '28');
INSERT INTO `tb_userpermissions` VALUES ('1005', '1', '29');
INSERT INTO `tb_userpermissions` VALUES ('1006', '1', '30');
INSERT INTO `tb_userpermissions` VALUES ('1007', '1', '42');
INSERT INTO `tb_userpermissions` VALUES ('1008', '1', '41');

-- ----------------------------
-- Table structure for tb_userprofiles
-- ----------------------------
DROP TABLE IF EXISTS `tb_userprofiles`;
CREATE TABLE `tb_userprofiles` (
  `profileid` int(11) NOT NULL AUTO_INCREMENT,
  `profilename` varchar(50) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`profileid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_userprofiles
-- ----------------------------
INSERT INTO `tb_userprofiles` VALUES ('1', 'Administrators', '1');
INSERT INTO `tb_userprofiles` VALUES ('2', 'Branch Operations manager', '1');
INSERT INTO `tb_userprofiles` VALUES ('3', 'Customer Service Officers', '1');
INSERT INTO `tb_userprofiles` VALUES ('4', 'Chief Operations Manager', '1');
INSERT INTO `tb_userprofiles` VALUES ('5', 'Finance Department', '1');
INSERT INTO `tb_userprofiles` VALUES ('6', 'Auditor', '1');
INSERT INTO `tb_userprofiles` VALUES ('7', 'Relationship Officers', '1');
INSERT INTO `tb_userprofiles` VALUES ('8', 'Finance Officer', '1');
INSERT INTO `tb_userprofiles` VALUES ('9', 'Vendor', '1');

-- ----------------------------
-- Table structure for tb_users
-- ----------------------------
DROP TABLE IF EXISTS `tb_users`;
CREATE TABLE `tb_users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `branchid` int(11) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `idtype` int(11) DEFAULT NULL,
  `idno` varchar(20) DEFAULT NULL,
  `profileid` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `createdon` datetime DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `loginattempts` int(11) DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`),
  KEY `fk_tb_users_tb_userprofiles` (`profileid`),
  KEY `fk_tb_users_tb_schools` (`branchid`),
  CONSTRAINT `fk_tb_users_tb_branches` FOREIGN KEY (`branchid`) REFERENCES `tb_branches` (`branchid`),
  CONSTRAINT `fk_tb_users_tb_userprogiles` FOREIGN KEY (`profileid`) REFERENCES `tb_userprofiles` (`profileid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_users
-- ----------------------------
INSERT INTO `tb_users` VALUES ('1', 'admin', '0eae26f5e9741f7b4d1d9fc8a0496fc6', '1', 'Administrator', '1', '1234567', '1', '1', '2013-10-11 21:39:02', '2013-10-13 13:39:02', '3');
INSERT INTO `tb_users` VALUES ('2', 'bremak', 'c71603498873a9536fd1835f5009a499', '1', 'James Makau', '1', '23478504', '9', '1', '2016-11-29 22:06:52', '2016-11-29 23:06:52', '3');

-- ----------------------------
-- Table structure for tb_ussdsession
-- ----------------------------
DROP TABLE IF EXISTS `tb_ussdsession`;
CREATE TABLE `tb_ussdsession` (
  `mobileno` bigint(20) NOT NULL,
  `expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` varchar(50) NOT NULL,
  `newpin` varchar(250) DEFAULT NULL,
  `amount` double(18,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_ussdsession
-- ----------------------------

-- ----------------------------
-- Table structure for tb_ussdstates
-- ----------------------------
DROP TABLE IF EXISTS `tb_ussdstates`;
CREATE TABLE `tb_ussdstates` (
  `state` varchar(50) NOT NULL,
  `timeout` int(11) NOT NULL,
  `next0` varchar(50) DEFAULT NULL,
  `next1` varchar(50) DEFAULT NULL,
  `next2` varchar(50) DEFAULT NULL,
  `next3` varchar(50) DEFAULT NULL,
  `next4` varchar(50) DEFAULT NULL,
  `next5` varchar(50) DEFAULT NULL,
  `next6` varchar(50) DEFAULT NULL,
  `next7` varchar(50) DEFAULT NULL,
  `next8` varchar(50) DEFAULT NULL,
  `next9` varchar(50) DEFAULT NULL,
  `nextother` varchar(50) DEFAULT NULL,
  `nextok` varchar(50) DEFAULT NULL,
  `nexterror` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_ussdstates
-- ----------------------------

-- ----------------------------
-- Table structure for tb_ussdsubscriptions
-- ----------------------------
DROP TABLE IF EXISTS `tb_ussdsubscriptions`;
CREATE TABLE `tb_ussdsubscriptions` (
  `mobileno` bigint(20) NOT NULL,
  `ussdstatus` tinyint(4) NOT NULL DEFAULT '1',
  `pinattempts` tinyint(4) NOT NULL DEFAULT '3',
  `pin` varchar(250) DEFAULT NULL,
  `cbscustomerid` bigint(20) NOT NULL,
  `cbscustomernames` varchar(250) DEFAULT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`mobileno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_ussdsubscriptions
-- ----------------------------
INSERT INTO `tb_ussdsubscriptions` VALUES ('705434532', '1', '3', null, '1', 'MUTINDA, CAROLINE ', '2016-11-29 23:40:26', '2');

-- ----------------------------
-- View structure for v_customeraccountdetails
-- ----------------------------
DROP VIEW IF EXISTS `v_customeraccountdetails`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_customeraccountdetails` AS (select `tb_branches`.`branchname` AS `branchname`,`tb_accounts`.`accountid` AS `accountid`,`tb_customer`.`number` AS `customernumber`,`tb_accounts`.`accountnumber` AS `accountnumber`,`tb_customer`.`name` AS `customername`,`tb_accounts`.`customerid` AS `customerid`,`tb_products`.`productid` AS `productid`,`tb_products`.`description` AS `productname`,(case `tb_products`.`producttypeid` when 4 then coalesce((`fn_getinterestbalance`(`tb_accounts`.`accountid`,now()) + `fn_getprincipalbalance`(`tb_accounts`.`accountid`,now())),0) else `fn_accountbalanceatdate`(`tb_accounts`.`accountid`,now()) end) AS `balance`,`tb_accounts`.`creationdate` AS `creationdate`,`tb_products`.`producttypeid` AS `producttypeid`,(select `tb_producttype`.`description` from `tb_producttype` where (`tb_producttype`.`producttypeid` = `tb_products`.`producttypeid`)) AS `producttype`,`tb_person`.`idno` AS `idno`,`tb_person`.`idtype` AS `idtype`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 10) and (`tb_miscellaneouslists`.`valueMember` = `tb_person`.`idtype`))) AS `idtypedesc`,`tb_accounts`.`active` AS `active`,(case `tb_accounts`.`active` when 1 then 'YES' else 'No' end) AS `activestate` from (((((`tb_accounts` join `tb_customer` on((`tb_customer`.`customerid` = `tb_accounts`.`customerid`))) join `tb_products` on((`tb_products`.`productid` = `tb_accounts`.`productid`))) join `tb_branches` on((`tb_branches`.`branchid` = `tb_customer`.`branchid`))) left join `tb_customerperson` on((`tb_customerperson`.`customerid` = `tb_accounts`.`customerid`))) left join `tb_person` on((`tb_person`.`personid` = `tb_customerperson`.`personid`)))) ;

-- ----------------------------
-- View structure for v_customerdetails
-- ----------------------------
DROP VIEW IF EXISTS `v_customerdetails`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_customerdetails` AS select `tb_branches`.`branchname` AS `branchname`,`tb_branches`.`branchid` AS `branchid`,`tb_customer`.`name` AS `name`,`tb_customer`.`customerid` AS `customerid`,`tb_customer`.`customertype` AS `customertype`,`tb_customer`.`number` AS `customernumber`,`tb_customer`.`joindate` AS `joindate`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 7) and (`tb_miscellaneouslists`.`valueMember` = `tb_customer`.`customertype`))) AS `customertypedesc`,`tb_loanofficers`.`loanofficerid` AS `loanofficerid`,`tb_loanofficers`.`description` AS `loanofficername`,`tb_person`.`idno` AS `idno`,`tb_person`.`idtype` AS `idtype`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 10) and (`tb_miscellaneouslists`.`valueMember` = `tb_customer`.`customertype`))) AS `idtypedesc`,`tb_person`.`mobileno` AS `mobileno` , fn_customerbalance( `tb_customer`.`customerid`, now()) as balance from ((((`tb_customer` join `tb_branches` on((`tb_customer`.`branchid` = `tb_branches`.`branchid`))) left join `tb_loanofficers` on((`tb_loanofficers`.`loanofficerid` = `tb_customer`.`loanofficerid`))) left join `tb_customerperson` on((`tb_customerperson`.`customerid` = `tb_customer`.`customerid`))) left join `tb_person` on((`tb_person`.`personid` = `tb_customerperson`.`personid`))) ;

-- ----------------------------
-- View structure for v_customertransactions
-- ----------------------------
DROP VIEW IF EXISTS `v_customertransactions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_customertransactions` AS (select `tb_transactions`.`transactionid` AS `transactionid`,`tb_receipts`.`receiptno` AS `receiptno`,concat(`tb_receipts`.`trxdescription`,' (',(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 18) and (`tb_miscellaneouslists`.`valueMember` = `tb_transactions`.`transactiontype`))),')') AS `trxdescription`,`tb_transactions`.`valuedate` AS `valuedate`,`tb_products`.`productid` AS `productid`,`tb_products`.`description` AS `productname`,`tb_transactions`.`accountid` AS `accountid`,`tb_accounts`.`customerid` AS `customerid`,`v_customerdetails`.`name` AS `name`,`tb_transactions`.`feeid` AS `feeid`,(select `tb_fee`.`description` from `tb_fee` where (`tb_fee`.`feeid` = `tb_transactions`.`feeid`)) AS `fee`,`tb_transactions`.`iscredit` AS `iscredit`,(case `tb_transactions`.`iscredit` when 1 then 'Credit' else 'Debit' end) AS `trxtype`,`tb_transactions`.`amount` AS `amount`,`tb_transactions`.`debitaccid` AS `debitaccid`,`debitaccounts`.`description` AS `debitaccount`,`tb_transactions`.`creditaccid` AS `creditaccid`,`creditaccounts`.`description` AS `creditaccount`,`tb_transactions`.`serverdate` AS `serverdate`,`tb_receipts`.`isreversed` AS `isreversed`,(case `tb_receipts`.`isreversed` when 0 then 'No' else 'Yes' end) AS `reversed`,`tb_ledgers`.`ledgerid` AS `ledgerid`,`v_customerdetails`.`idno` AS `idno`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 18) and (`tb_miscellaneouslists`.`valueMember` = `tb_transactions`.`transactiontype`))) AS `transactiontype`,(case `tb_transactions`.`iscredit` when 0 then `tb_transactions`.`amount` else 0 end) AS `debit`,(case `tb_transactions`.`iscredit` when 1 then `tb_transactions`.`amount` else 0 end) AS `credit`,`tb_products`.`producttypeid` AS `producttypeid`,`tb_transactions`.`transactiontype` AS `transactiontypeid` from (((((((`tb_transactions` join `tb_receipts` on((`tb_transactions`.`receiptno` = `tb_receipts`.`receiptno`))) join `tb_accounts` on((`tb_transactions`.`accountid` = `tb_accounts`.`accountid`))) join `v_customerdetails` on((`v_customerdetails`.`customerid` = `tb_accounts`.`customerid`))) join `tb_products` on((`tb_products`.`productid` = `tb_accounts`.`productid`))) join `tb_ledgers` on((`tb_ledgers`.`receiptno` = `tb_receipts`.`receiptno`))) join `tb_ledgeraccounts` `debitaccounts` on((`debitaccounts`.`accountid` = `tb_transactions`.`debitaccid`))) join `tb_ledgeraccounts` `creditaccounts` on((`creditaccounts`.`accountid` = `tb_transactions`.`creditaccid`))) group by `tb_receipts`.`receiptno`) ;

-- ----------------------------
-- View structure for v_cutomer_balances
-- ----------------------------
DROP VIEW IF EXISTS `v_cutomer_balances`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_cutomer_balances` AS select `tb_branches`.`branchname` AS `branchname`,`tb_accounts`.`accountid` AS `accountid`,`tb_customer`.`number` AS `customernumber`,
`tb_accounts`.`accountnumber` AS `accountnumber`,`tb_customer`.`name` AS `customername`,`tb_accounts`.`customerid` AS `customerid`,
`tb_products`.`productid` AS `productid`,`tb_products`.`description` AS `productname`,(case `tb_products`.`producttypeid`
 when 4 then coalesce((`fn_getinterestbalance`(`tb_accounts`.`accountid`,now()) + `fn_getprincipalbalance`(`tb_accounts`.`accountid`,
 now())),0) else `fn_accountbalanceatdate`(`tb_accounts`.`accountid`,now()) end) AS `balance`,
 `tb_accounts`.`creationdate` AS `creationdate`,`tb_products`.`producttypeid` AS `producttypeid`,
 (select `tb_producttype`.`description` 
 from `tb_producttype` where (`tb_producttype`.`producttypeid` = `tb_products`.`producttypeid`)) AS `producttype`,
 
 `tb_person`.`idno` AS `idno`,`tb_person`.`idtype` AS `idtype`,(select `tb_miscellaneouslists`.`description` from
  `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 10) and (`tb_miscellaneouslists`.`valueMember` = `tb_person`.`idtype`))) AS `idtypedesc`,
  `tb_accounts`.`active` AS `active`,(case `tb_accounts`.`active` when 1 then 'YES' else 'No' end) AS `activestate` 
  from (((((`tb_accounts` join `tb_customer` on((`tb_customer`.`customerid` = `tb_accounts`.`customerid`))) 
  join `tb_products` on((`tb_products`.`productid` = `tb_accounts`.`productid`)))
   join `tb_branches` on((`tb_branches`.`branchid` = `tb_customer`.`branchid`))) 
	left join `tb_customerperson` on((`tb_customerperson`.`customerid` = `tb_accounts`.`customerid`)))
 left join `tb_person` on((`tb_person`.`personid` = `tb_customerperson`.`personid`))) ;

-- ----------------------------
-- View structure for v_fee
-- ----------------------------
DROP VIEW IF EXISTS `v_fee`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_fee` AS select `tb_fee`.`feeid` AS `feeid`,`tb_fee`.`description` AS `description`,`tb_fee`.`shortname` AS `shortname`,`tb_fee`.`feetype` AS `feetype`,`tb_fee`.`active` AS `active`,(case `tb_fee`.`active` when 1 then 'YES' else 'NO' end) AS `isactive`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 6) and (`tb_miscellaneouslists`.`valueMember` = `tb_fee`.`feetype`))) AS `feetypedesc`,`tb_fee`.`feeoption` AS `feeoption`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 15) and (`tb_miscellaneouslists`.`valueMember` = `tb_fee`.`feeoption`))) AS `feeoptiondesc`,`tb_fee`.`createdon` AS `createdon`,`tb_fee`.`amount` AS `amount`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`feeid` = `tb_fee`.`feeid`) and (`tb_ledgerlinks`.`gltype` = 2))) AS `feeincome`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`feeid` = `tb_fee`.`feeid`) and (`tb_ledgerlinks`.`gltype` = 2))) AS `feeincomedesc`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`feeid` = `tb_fee`.`feeid`) and (`tb_ledgerlinks`.`gltype` = 3))) AS `feeaccrual`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`feeid` = `tb_fee`.`feeid`) and (`tb_ledgerlinks`.`gltype` = 3))) AS `feeaccrualdesc` from `tb_fee` ;

-- ----------------------------
-- View structure for v_getledgertransactions
-- ----------------------------
DROP VIEW IF EXISTS `v_getledgertransactions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_getledgertransactions` AS (select `tb_ledgers`.`ledgerid` AS `ledgerid`,`tb_ledgertransactions`.`ledgertrxid` AS `ledgertrxid`,`tb_ledgers`.`valuedate` AS `valuedate`,`tb_ledgers`.`transactionnote` AS `transactionnote`,`tb_ledgers`.`receiptno` AS `receiptno`,`tb_ledgers`.`createdby` AS `createdby`,`tb_users`.`username` AS `username`,`tb_ledgertransactions`.`accountid` AS `accountid`,`tb_ledgeraccounts`.`description` AS `accountname`,`tb_ledgeraccounts`.`number` AS `accountnumber`,`tb_ledgertransactions`.`iscredit` AS `iscredit`,`tb_ledgertransactions`.`amount` AS `amount`,(case `tb_ledgertransactions`.`iscredit` when 0 then `tb_ledgertransactions`.`amount` else 0 end) AS `debit`,(case `tb_ledgertransactions`.`iscredit` when 1 then `tb_ledgertransactions`.`amount` else 0 end) AS `credit`,`tb_ledgeraccounts`.`categoryid` AS `categoryid` from (((`tb_ledgers` join `tb_ledgertransactions` on((`tb_ledgers`.`ledgerid` = `tb_ledgertransactions`.`ledgerid`))) join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgertransactions`.`accountid`))) join `tb_users` on((`tb_users`.`userid` = `tb_ledgers`.`createdby`))) order by `tb_ledgers`.`ledgerid`) ;

-- ----------------------------
-- View structure for v_getloanproducts
-- ----------------------------
DROP VIEW IF EXISTS `v_getloanproducts`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_getloanproducts` AS (select `tb_products`.`productid` AS `productid`,`tb_products`.`code` AS `code`,`tb_products`.`description` AS `description`,`tb_products`.`producttypeid` AS `producttypeid`,`tb_producttype`.`description` AS `producttype`,`tb_products`.`currencyid` AS `currencyid`,`tb_currency`.`description` AS `currency`,`tb_currency`.`CODE` AS `currencycode`,`tb_products`.`maxaccounts` AS `maxaccounts`,`tb_products`.`active` AS `active`,`tb_products`.`creationdate` AS `creationdate`,(case `tb_products`.`active` when 1 then 'YES' else 'No' end) AS `activestate`,`tb_productloan`.`settings` AS `settings`,`tb_productloan`.`effectivedate` AS `effectivedate`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 1))) AS `productcontrol`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 1))) AS `productcontroldesc`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 9))) AS `interestincome`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 9))) AS `interestincomedesc`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 10))) AS `interestreceivable`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 10))) AS `interestreceivabledesc` from (((`tb_products` join `tb_productloan` on((`tb_products`.`productid` = `tb_productloan`.`productid`))) join `tb_producttype` on((`tb_producttype`.`producttypeid` = `tb_products`.`producttypeid`))) join `tb_currency` on((`tb_currency`.`currencyid` = `tb_products`.`currencyid`)))) ;

-- ----------------------------
-- View structure for v_getsavingsproducts
-- ----------------------------
DROP VIEW IF EXISTS `v_getsavingsproducts`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_getsavingsproducts` AS (select `tb_products`.`productid` AS `productid`,`tb_products`.`code` AS `code`,`tb_products`.`description` AS `description`,`tb_products`.`producttypeid` AS `producttypeid`,`tb_producttype`.`description` AS `producttype`,`tb_products`.`currencyid` AS `currencyid`,`tb_currency`.`description` AS `currency`,`tb_currency`.`CODE` AS `currencycode`,`tb_products`.`maxaccounts` AS `maxaccounts`,`tb_products`.`active` AS `active`,`tb_products`.`creationdate` AS `creationdate`,(case `tb_products`.`active` when 1 then 'YES' else 'No' end) AS `activestate`,`tb_productshare`.`termdeposit` AS `termdeposit`,`tb_productshare`.`minbalance` AS `minbalance`,`tb_productshare`.`maxbalance` AS `maxbalance`,`tb_productshare`.`minage` AS `minage`,`tb_productshare`.`maxage` AS `maxage`,`tb_productshare`.`minterm` AS `minterm`,`tb_productshare`.`maxterm` AS `maxterm`,`tb_productshare`.`contributions` AS `contributions`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 1))) AS `productcontrol`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 1))) AS `productcontroldesc`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 4))) AS `savingsint`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 4))) AS `savingsintdesc`,(select `tb_ledgerlinks`.`accountid` from `tb_ledgerlinks` where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 6))) AS `savingstax`,(select `tb_ledgeraccounts`.`description` from (`tb_ledgerlinks` join `tb_ledgeraccounts` on((`tb_ledgeraccounts`.`accountid` = `tb_ledgerlinks`.`accountid`))) where ((`tb_ledgerlinks`.`productid` = `tb_products`.`productid`) and (`tb_ledgerlinks`.`gltype` = 6))) AS `savingstaxdesc` from (((`tb_products` join `tb_productshare` on((`tb_products`.`productid` = `tb_productshare`.`productid`))) join `tb_producttype` on((`tb_producttype`.`producttypeid` = `tb_products`.`producttypeid`))) join `tb_currency` on((`tb_currency`.`currencyid` = `tb_products`.`currencyid`)))) ;

-- ----------------------------
-- View structure for v_ledgeraccounts
-- ----------------------------
DROP VIEW IF EXISTS `v_ledgeraccounts`;
CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_ledgeraccounts` AS (select `tb_branches`.`branchid` AS `branchid`,`tb_branches`.`branchname` AS `branchname`,`tb_ledgeraccounts`.`accountid` AS `accountid`,`tb_ledgeraccounts`.`number` AS `number`,`tb_ledgeraccounts`.`description` AS `description`,`tb_ledgeraccounts`.`dramount` AS `dramount`,`tb_ledgeraccounts`.`cramount` AS `cramount`,`tb_ledgeraccounts`.`balancedate` AS `balancedate`,`tb_ledgercategory`.`categoryid` AS `categoryid`,`tb_ledgercategory`.`description` AS `categoryname`,`tb_ledgerformat`.`formatid` AS `financialcategoryid`,`tb_ledgerformat`.`description` AS `financialcategory`,`subcategory`.`formatid` AS `subfinancialcategoryid`,`subcategory`.`description` AS `subfinancialcategory`,`tb_ledgeraccounts`.`active` AS `active`,(case `tb_ledgeraccounts`.`active` when 1 then 'YES' else 'No' end) AS `activestate` from ((((`tb_ledgeraccounts` join `tb_ledgercategory` on((`tb_ledgercategory`.`categoryid` = `tb_ledgeraccounts`.`categoryid`))) join `tb_ledgerformat` on(((`tb_ledgerformat`.`formatid` = `tb_ledgeraccounts`.`formatid`) and (`tb_ledgerformat`.`categoryid` = `tb_ledgeraccounts`.`categoryid`)))) join `tb_branches` on((`tb_branches`.`branchid` = `tb_ledgeraccounts`.`branchid`))) join `tb_ledgerformat` `subcategory` on((`subcategory`.`formatid` = `tb_ledgeraccounts`.`subformatid`)))) ;

-- ----------------------------
-- View structure for v_transactions
-- ----------------------------
DROP VIEW IF EXISTS `v_transactions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_transactions` AS select `tb_transactions`.`transactionid` AS `transactionid`,`tb_receipts`.`receiptno` AS `receiptno`,concat(`tb_receipts`.`trxdescription`,' (',(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 18) and (`tb_miscellaneouslists`.`valueMember` = `tb_transactions`.`transactiontype`))),')') AS `trxdescription`,`tb_transactions`.`valuedate` AS `valuedate`,`tb_products`.`productid` AS `productid`,`tb_products`.`description` AS `productname`,`tb_transactions`.`accountid` AS `accountid`,`tb_accounts`.`customerid` AS `customerid`,`tb_customer`.`name` AS `name`,`tb_transactions`.`feeid` AS `feeid`,`tb_fee`.`description` AS `fee`,`tb_transactions`.`iscredit` AS `iscredit`,(case `tb_transactions`.`iscredit` when 1 then 'Credit' else 'Debit' end) AS `trxtype`,`tb_transactions`.`amount` AS `amount`,`tb_transactions`.`debitaccid` AS `debitaccid`,`tb_transactions`.`creditaccid` AS `creditaccid`,`tb_transactions`.`serverdate` AS `serverdate`,`tb_receipts`.`isreversed` AS `isreversed`,(case `tb_receipts`.`isreversed` when 0 then 'No' else 'Yes' end) AS `reversed`,`tb_person`.`idno` AS `idno`,(select `tb_miscellaneouslists`.`description` from `tb_miscellaneouslists` where ((`tb_miscellaneouslists`.`parentid` = 18) and (`tb_miscellaneouslists`.`valueMember` = `tb_transactions`.`transactiontype`))) AS `transactiontype`,(case `tb_transactions`.`iscredit` when 0 then `tb_transactions`.`amount` else 0 end) AS `debit`,(case `tb_transactions`.`iscredit` when 1 then `tb_transactions`.`amount` else 0 end) AS `credit`,`tb_products`.`producttypeid` AS `producttypeid`,`tb_transactions`.`transactiontype` AS `transactiontypeid` from (((((((`tb_transactions` join `tb_receipts` on((`tb_receipts`.`receiptno` = `tb_transactions`.`receiptno`))) join `tb_accounts` on((`tb_accounts`.`accountid` = `tb_transactions`.`accountid`))) join `tb_products` on((`tb_products`.`productid` = `tb_accounts`.`productid`))) join `tb_customer` on((`tb_customer`.`customerid` = `tb_accounts`.`customerid`))) join `tb_customerperson` on((`tb_customerperson`.`customerid` = `tb_customer`.`customerid`))) join `tb_person` on((`tb_person`.`personid` = `tb_customerperson`.`personid`))) left join `tb_fee` on((`tb_fee`.`feeid` = `tb_transactions`.`feeid`))) ;

-- ----------------------------
-- Procedure structure for sp_preview
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_preview`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_preview`(_ruleid INT,_message VARCHAR(160))
BEGIN	
	DECLARE _ruletype  INT;
		SELECT COALESCE(ruletype,0) INTO _ruletype FROM tb_rules WHERE ruleid = _ruleid;
		
	IF _ruletype = 1 THEN 	
	  SELECT phoneno,NAME AS contactname,0 AS amount,0 AS savingsbalance,0 AS loanamount,0 AS arrearsamount,'' AS accountnumber,'' AS receiptno,
	  REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_message,'${Name}',NAME),'${SavingsBalance}',0),'${LoanAmount}',0)
	  ,'${ArrearsAmount}',0),'${AccountNumber}','')
	  ,'${Receiptno}',''),'${Amount}',0) AS message 
	  FROM tb_contacts
	  INNER JOIN tb_rules
	  ON tb_contacts.ruleid = tb_rules.ruleid	
	  WHERE tb_contacts.ruleid = _ruleid
	  AND tb_contacts.active = 1
	  AND tb_rules.active = 1;
	ELSEIF _ruletype = 0 THEN
	  IF _ruleid = 2 THEN
	     SELECT CONCAT('254',RIGHT(TRIM(v_customerdetails.mobileno),9)) AS phoneno,NAME AS contactname,0 AS amount,0 AS savingsbalance,0 AS loanamount,0 AS arrearsamount,'' AS accountnumber,'' AS receiptno,
	     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_message,'${Name}',NAME),'${SavingsBalance}',0),'${LoanAmount}',0)
	     ,'${ArrearsAmount}',0),'${AccountNumber}','')
	     ,'${Receiptno}',''),'${Amount}',0) AS message  FROM v_customerdetails;
	  ELSEIF _ruleid = 3 THEN
	     SELECT CONCAT('254',RIGHT(TRIM(v_customerdetails.mobileno),9)) AS phoneno,NAME AS contactname,0 AS amount,0 AS savingsbalance,0 AS loanamount,0 AS arrearsamount,'' AS accountnumber,'' AS receiptno,
	     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_message,'${Name}',NAME),'${SavingsBalance}',0),'${LoanAmount}',0)
	     ,'${ArrearsAmount}',0),'${AccountNumber}','')
	     ,'${Receiptno}',''),'${Amount}',0) AS message  FROM v_customerdetails WHERE customerid NOT IN (SELECT customerid FROM tb_loans WHERE loanstatus = 3);	  	     
	  ELSEIF _ruleid = 4 THEN
	     SELECT DISTINCT CONCAT('254',RIGHT(TRIM(v_customerdetails.mobileno),9)) AS phoneno,NAME AS contactname,0 AS amount,0 AS savingsbalance,0 AS loanamount,0 AS arrearsamount,'' AS accountnumber,'' AS receiptno,
	     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(_message,'${Name}',NAME),'${SavingsBalance}',0),'${LoanAmount}',0)
	     ,'${ArrearsAmount}',fn_getarrearsamount(tb_loans.accountid, DATE(NOW()))),'${AccountNumber}',tb_loans.accountid)
	     ,'${Receiptno}',''),'${Amount}',0) AS message  FROM tb_accounts
		INNER JOIN tb_loans
		ON tb_accounts.accountid = tb_loans.accountid
		INNER JOIN tb_products
		ON tb_products.productid = tb_accounts.productid
		INNER JOIN v_customerdetails
		ON v_customerdetails.customerid = tb_accounts.customerid
		WHERE fn_getprincipalbalance(tb_loans.accountid, DATE(NOW()))<>0
		AND fn_getarrearsamount(tb_loans.accountid, DATE(NOW())) > 0
		AND tb_loans.loanstatus<>4;	  	     
	     
	  END IF;
	END IF;
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_accountbalanceatdate
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_accountbalanceatdate`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_accountbalanceatdate`(
	`_accountid` INT,
	`_date` DATETIME

) RETURNS double(15,4)
    READS SQL DATA
BEGIN
	DECLARE balance DOUBLE(15,4);
	DECLARE producttype INT;
	
	SELECT tb_products.producttypeid INTO producttype FROM tb_accounts
	INNER JOIN tb_products
	ON tb_accounts.productid = tb_products.productid
	WHERE tb_accounts.accountid = _accountid;
	
	IF (producttype != 4) THEN
		SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE (amount*-1) END),0) INTO balance FROM tb_transactions WHERE valuedate <=  _date  
		AND accountid = _accountid AND COALESCE(feeid,0)=0;	
	ELSE
		SELECT COALESCE(SUM(CASE iscredit WHEN 0 THEN amount ELSE (amount*-1) END),0) INTO balance FROM tb_transactions WHERE valuedate <=  _date  
		AND accountid = _accountid AND transactiontype IN (5,6);		
	END IF;
	
	RETURN COALESCE(balance,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_customerbalance
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_customerbalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_customerbalance`(
	`_customerid` INT,
	`_date` DATE







) RETURNS double(18,2)
    READS SQL DATA
BEGIN
	DECLARE balance DOUBLE(15,4);
	DECLARE producttype INT;

		SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE (amount*-1) END),0) INTO balance FROM tb_transactions WHERE
		
		 valuedate <=  _date  
		AND accountid IN (
		SELECT tb_accounts.accountid FROM tb_accounts
	INNER JOIN tb_products
	ON tb_accounts.productid = tb_products.productid
	WHERE tb_accounts.customerid = _customerid AND tb_products.producttypeid !=4
		) AND COALESCE(feeid,0)=0;	


	RETURN COALESCE(balance,0);
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_customerloanbalance
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_customerloanbalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_customerloanbalance`(
	`_customerid` INT,
	`_date` DATE
) RETURNS double(15,2)
    READS SQL DATA
BEGIN
	DECLARE balance DOUBLE(15,4);

		SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE (amount*-1) END),0) INTO balance FROM tb_transactions WHERE
		
		 valuedate <=  _date  
		AND accountid IN (
		SELECT tb_accounts.accountid FROM tb_accounts
	INNER JOIN tb_products
	ON tb_accounts.productid = tb_products.productid
	WHERE tb_accounts.customerid = _customerid AND tb_products.producttypeid !=4
		) AND COALESCE(feeid,0)=0;	


	RETURN COALESCE(balance,0);
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getarrearsamount
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getarrearsamount`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getarrearsamount`(_accountid BIGINT,_date DATETIME) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
		
	DECLARE _arrearsinterest DECIMAL(18,2);
	DECLARE _arrearsprincipal DECIMAL(18,2);
	DECLARE _arrearsamount DECIMAL(18,2);
	DECLARE _loanid BIGINT;
	DECLARE _currentbalance DECIMAL(18,2);
	DECLARE _repaymentdate DATETIME;
	DECLARE _arrearsdays   INT;
	
	SELECT loanid INTO _loanid FROM tb_loans WHERE accountid = _accountid;	
	
	SET _date = DATE(_date);
	
	SELECT
	((SELECT COALESCE(SUM(interest),0) FROM tb_loanschedule WHERE loanid = _loanid AND repaymentdate < _date)-
	(SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0)  FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed = 0 WHERE transactiontype = 8 AND tb_transactions.valuedate < _date AND accountid = _accountid)) INTO _arrearsinterest;
	SELECT
	((SELECT COALESCE(SUM(principal),0) FROM tb_loanschedule WHERE loanid = _loanid AND repaymentdate < _date)-
	(SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0)  FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed = 0 WHERE transactiontype = 6 AND tb_transactions.valuedate < _date AND accountid = _accountid)) INTO _arrearsprincipal;
	
	SELECT CASE WHEN COALESCE(_arrearsinterest,0)<0 THEN 0 ELSE COALESCE(_arrearsinterest,0) END + CASE WHEN COALESCE(_arrearsprincipal,0)<0 THEN 0 ELSE COALESCE(_arrearsprincipal,0) END INTO _arrearsamount;
		    	
	RETURN COALESCE(_arrearsamount,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getarrearsdays
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getarrearsdays`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getarrearsdays`(_accountid BIGINT,_date DATETIME) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE _currentbalance DECIMAL(18,2);
	declare _repaymentdate Datetime;
	DECLARE _arrearsdays   INT;
	DECLARE _loanid BIGINT;
	
	SELECT loanid INTO _loanid FROM tb_loans WHERE accountid = _accountid;
	
	SET _date = DATE(_date);
	
	if fn_getarrearsamount(_accountid,_date) = 0 then
	    SET _arrearsdays = 0;
	ELSE
	    SELECT fn_getprincipalbalance(_accountid,_date)+fn_getinterestdue(_accountid,_date) INTO _currentbalance;	
	    SELECT repaymentdate INTO _repaymentdate FROM tb_loanschedule WHERE loanid = _loanid AND balance > _currentbalance ORDER BY period ASC LIMIT 1;
		
	    SELECT DATEDIFF(_date,_repaymentdate) INTO _arrearsdays;		
	END IF;
	
	RETURN COALESCE(_arrearsdays,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getinterestbalance
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getinterestbalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getinterestbalance`(_accountid BIGINT,_date DATETIME) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
	DECLARE _interestcalculationmethod INT;
	DECLARE _recoverfutureinterest INT;
	DECLARE _productid INT;
	DECLARE _loanid BIGINT;
	DECLARE _interestrate DECIMAL(18,2);
	DECLARE _interestdays INT;
	DECLARE _interestbalance DECIMAL(18,2);
	DECLARE _principalbalance DECIMAL(18,2);
	DECLARE _interestpaid DECIMAL(18,2);
	DECLARE _disbursementdate DATETIME;
	
	SELECT productid,loanid,rate,disbursedon INTO _productid,_loanid,_interestrate,_disbursementdate FROM tb_loans WHERE accountid = _accountid;	
	SELECT COALESCE(IF(INSTR(settings,'interestcalculationmethod'), SUBSTRING(settings,INSTR(settings,'interestcalculationmethod')+29,1),0),0),COALESCE(IF(INSTR(settings,'futureinterest'), SUBSTRING(settings,INSTR(settings,'futureinterest')+18,1),0),0) INTO _interestcalculationmethod,_recoverfutureinterest FROM tb_productloan WHERE productid = _productid;
	SELECT DATEDIFF(_date, _disbursementdate) INTO _interestdays;
	
		
			
			
			
	
	IF (_interestcalculationmethod != 2) THEN
	    	    	    if (_recoverfutureinterest = "1") THEN
		SELECT
		((SELECT COALESCE(SUM(interest),0) FROM tb_loanschedule WHERE loanid = _loanid)-
		(SELECT COALESCE(SUM(amount),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 8 AND accountid = _accountid)) INTO _interestbalance;
	    ELSE
		SELECT fn_getinterestdue(_accountid,_date) INTO _interestbalance;
	    END IF;
	ELSE
	   	   		SELECT fn_getrecalculatedinterest(_accountid,_date,_disbursementdate,_interestrate) INTO _interestbalance;	
	END IF;
		
	RETURN COALESCE(_interestbalance,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getinterestdue
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getinterestdue`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getinterestdue`(_accountid BIGINT,_date DATETIME) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
	DECLARE _interestcalculationmethod INT;
	DECLARE _accrueinterest INT;
	DECLARE _productid INT;
	DECLARE _loanid BIGINT;
	DECLARE _interestrate DECIMAL(18,2);
	DECLARE _interestdays INT;
	DECLARE _interestdue DECIMAL(18,2);
	DECLARE _interestbalance DECIMAL(18,2);
	DECLARE _principalbalance DECIMAL(18,2);
	DECLARE _interestpaid DECIMAL(18,2);
	DECLARE _disbursementdate DATETIME;
	SELECT productid,loanid,rate,disbursedon INTO _productid,_loanid,_interestrate,_disbursementdate FROM tb_loans WHERE accountid = _accountid;	
	SELECT COALESCE(IF(INSTR(settings,'interestcalculationmethod'), SUBSTRING(settings,INSTR(settings,'interestcalculationmethod')+29,1),0),0) INTO _interestcalculationmethod FROM tb_productloan WHERE productid = _productid;
	SELECT COALESCE(IF(INSTR(settings,'accrueinterest'), SUBSTRING(settings,INSTR(settings,'accrueinterest')+18,1),0),0) INTO _accrueinterest FROM tb_productloan WHERE productid = _productid;
	IF (_accrueinterest = 1) THEN
	   SELECT
	   ((SELECT COALESCE(SUM(amount),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 7 AND tb_transactions.valuedate <= _date AND accountid = _accountid)-
	   (SELECT COALESCE(SUM(amount),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0  WHERE transactiontype = 8 AND tb_transactions.valuedate <= _date AND accountid = _accountid)) INTO _interestdue;
	ELSEIF (_interestcalculationmethod != 2) THEN
	   SELECT
	   ((SELECT COALESCE(SUM(interest),0) FROM tb_loanschedule WHERE loanid = _loanid AND repaymentdate <= _date)-
	   (SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 8 AND tb_transactions.valuedate <= _date AND accountid = _accountid)) INTO _interestdue;
	ELSE
	   SELECT fn_getrecalculatedinterest(_accountid,_date,_disbursementdate,_interestrate) INTO _interestdue; 	   
	END IF;
	
	if (_interestdue < 0) then
	    SET _interestdue = 0;
	END IF;
	
	RETURN COALESCE(_interestdue,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getprincipalbalance
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getprincipalbalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getprincipalbalance`(_accountid BIGINT,_date DATETIME) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
	DECLARE _productid INT;
	DECLARE _loanid BIGINT;
	DECLARE _principalbalance DECIMAL(18,2);
	
	SELECT productid,loanid INTO _productid,_loanid FROM tb_loans WHERE accountid = _accountid;	
	
	SELECT
	((SELECT COALESCE(SUM(principal),0) FROM tb_loanschedule WHERE loanid = _loanid)-
	(SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 6 AND tb_transactions.valuedate <= _date AND accountid = _accountid)) INTO _principalbalance;
	
	RETURN COALESCE(_principalbalance,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getprincipaldue
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getprincipaldue`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getprincipaldue`(_accountid BIGINT,_date DATETIME) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
	DECLARE _productid INT;
	DECLARE _loanid BIGINT;
	DECLARE _principaldue DECIMAL(18,2);
	
	SELECT productid,loanid INTO _productid,_loanid FROM tb_loans WHERE accountid = _accountid;	
	
	SELECT
	((SELECT COALESCE(SUM(principal),0) FROM tb_loanschedule WHERE loanid = _loanid AND repaymentdate <= _date)-
	(SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 6 AND tb_transactions.valuedate <= _date AND accountid = _accountid)) INTO _principaldue;
	SELECT CASE WHEN COALESCE(_principaldue,0) <= 0 THEN 0 ELSE COALESCE(_principaldue,0) END INTO _principaldue;
	
	RETURN _principaldue;	
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getrecalculatedinterest
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getrecalculatedinterest`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getrecalculatedinterest`(_accountid BIGINT,_date DATETIME,_disbursementdate DATETIME,_interestrate DECIMAL(18,2)) RETURNS decimal(18,2)
    READS SQL DATA
BEGIN
	DECLARE pb DECIMAL(18,2);
	DECLARE vd DATETIME;
	DECLARE receipt INT;
	DECLARE done INT DEFAULT FALSE;
	DECLARE prevrepaymentdate DATETIME DEFAULT _disbursementdate;
	DECLARE _interestdays INT DEFAULT 0;
	
	DECLARE _interestbalance FLOAT DEFAULT 0;
	DECLARE _interestpaid FLOAT DEFAULT 0;
	DECLARE _interestdue FLOAT DEFAULT 0;
		
        DECLARE actual_row_number INTEGER DEFAULT 0;
        DECLARE numRows INTEGER DEFAULT 0;	
	DECLARE csr CURSOR FOR 
	SELECT (fn_accountbalanceatdate(accountid,valuedate)+SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END)) AS amount,valuedate,receiptno
	FROM tb_transactions 	
	WHERE accountid = _accountid 
	AND valuedate <= _date
	AND transactiontype = 6 
	GROUP BY valuedate,receiptno
	HAVING SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END)<>0
	ORDER BY valuedate,receiptno;
						
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;	
           
	OPEN csr;
	read_loop: LOOP
	FETCH csr INTO pb,vd,receipt;	
	  IF done THEN
		LEAVE read_loop;
	  END IF;
	  
	  SELECT CASE WHEN DATEDIFF(vd,prevrepaymentdate) <=0 THEN 0 ELSE DATEDIFF(vd,prevrepaymentdate) END INTO _interestdays;
	  
	  SET _interestbalance = COALESCE(_interestbalance,0)+((pb*_interestrate) / ( 365 * 100 ))*_interestdays;
	  SET prevrepaymentdate = vd;
	  
	  		     
	END LOOP read_loop;
	CLOSE csr;
	
		SELECT CASE WHEN DATEDIFF(_date,prevrepaymentdate) <=0 THEN 0 ELSE DATEDIFF(_date,prevrepaymentdate) END INTO _interestdays;
	SELECT COALESCE(_interestbalance,0)+((COALESCE(fn_accountbalanceatdate(_accountid,_date),0)*_interestrate) / ( 365 * 100 ))*_interestdays INTO _interestbalance;
		SELECT COALESCE(SUM(CASE iscredit WHEN 1 THEN amount ELSE amount*-1 END),0) INTO _interestpaid FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE transactiontype = 8 AND tb_transactions.valuedate <= _date AND accountid = _accountid;
		SELECT CASE WHEN (_interestbalance - _interestpaid) <= 0 THEN 0 ELSE (_interestbalance - _interestpaid) END INTO _interestdue;
		
	RETURN ROUND(_interestdue,2); 
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_ledgerbalance
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_ledgerbalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_ledgerbalance`(_accountid BIGINT,_date DATETIME) RETURNS double(15,4)
    READS SQL DATA
BEGIN
	DECLARE balance DOUBLE(15,4);
	SELECT COALESCE(SUM(CASE WHEN categoryid IN (2,3) THEN debit-credit ELSE credit-debit END),0) AS amount INTO balance 
	FROM v_getledgertransactions	
	WHERE accountid = _accountid
	AND valuedate <= _date;
	
	
	RETURN COALESCE(balance,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_ledgerbalancebetweendates
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_ledgerbalancebetweendates`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_ledgerbalancebetweendates`(_accountid BIGINT,_fromdate DATETIME,_todate DATETIME) RETURNS double(15,4)
    READS SQL DATA
BEGIN
	DECLARE balance DOUBLE(15,4);
	SELECT COALESCE(SUM(CASE WHEN categoryid IN (2,3) THEN debit-credit ELSE credit-debit END),0) AS amount INTO balance 
	FROM v_getledgertransactions	
	WHERE accountid = _accountid
	AND valuedate BETWEEN _fromdate AND _todate;
	
	
	RETURN COALESCE(balance,0);
    END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for password_complexity
-- ----------------------------
DROP FUNCTION IF EXISTS `password_complexity`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `password_complexity`(password VARCHAR(64)) RETURNS varchar(256) CHARSET latin1
    READS SQL DATA
    SQL SECURITY INVOKER
BEGIN
  DECLARE message VARCHAR(256);
  DECLARE count INT;
  DECLARE username VARCHAR(64);
    IF LENGTH(password) < 8 THEN
     SET message = 'Password should be at least 8 characters';
  END IF;
      SELECT COUNT(*) INTO count FROM admin.dict WHERE word = password;
  IF count > 0 THEN
    SET message = CONCAT_WS(',',message,' Password too simple');
  END IF;
    IF password NOT RLIKE '[[:lower:]]' THEN
     SET message = CONCAT_WS(',',message,
                           ' Password should contain lower case character');
  END IF;
    IF password NOT RLIKE '[[:upper:]]' THEN
     SET message = CONCAT_WS(',',message,
                            ' Password should contain upper case character');
  END IF;
    IF password NOT RLIKE '[[:digit:]]' THEN
     SET message = CONCAT_WS(',',message,
                            ' Password should contain a digit');
  END IF;
    IF password NOT RLIKE '[[:punct:]]' THEN
     SET message = CONCAT_WS(',',message,
                             ' Password should contain punctuation');
  END IF;
      SET username = substring_index(CURRENT_USER(), '@', 1);
  IF username = password THEN
     SET message = 'Username and password are the same!';
  END IF;
    IF message IS NULL THEN
     SET message = 'Password is acceptible!';
  END IF;
  RETURN(message);
    END
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS=1;
