<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>...::: IMAB MIS :::...</title>
	<link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../css/isacco.css">
	
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../js/isacco.js"></script>
</head>
<body>
	<table>
		<tr>
			<td align="right"> Value Date :</td>  
			<td><input id="ldg_valuedate" class="easyui-datebox" required="true" name="ldg_valuedate"></td>
		</tr>
		<tr>
			<td align="right">Select Account :</td> 
			<td>
			<input id="ldg_accountid" class="easyui-combobox" name="ldg_accountid" style="width:300px"
				url="../data/get_data.php?action=64" valueField="accountid" textField="accountname">
			</td>
		</tr>
		<tr>
			<td align="right">Transaction Type :</td>
			<td>			
				<select id="ldg_trxtype" class="easyui-combobox" name="ldg_trxtype" style="width:80px;">  
					<option value="0">Debit</option>  
					<option value="1">Credit</option> 
				</select>
			<td/>
		</tr>	
		<tr>			
			<td align="right">Amount :</td>
			<td><input id="ldg_amount" name="ldg_amount" style="width:200px"></td>
		</tr>
		<tr>			
			<td align="right">Transaction note :</td>
			<td><input name="ldg_description" id="ldg_description" style="width:200px;"></td>
		</tr>
		<tr>
			<td align="center"><a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="addLedgerTransaction()">Add </a> </td>
			<td align="center"><a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="removeLedgerTransaction()">Remove </a> </td>
			<td><a href="#" id="btnpostledgertransaction" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#btnpostledgertransaction').linkbutton('disable');postLedgerTransaction();">Post</a> </td>
		</tr>
		<tr>	
		</tr>	
		</table> 		
		<table id="dgtrx" name="dgtrx[]" title="Ledger Transactions" class="easyui-datagrid"  style="width:700px;"  
				rownumbers="true" fitColumns="true" singleSelect="false">
			<thead>	
				<tr>  
					<th field="accountid" width="20" hidden="true">Account ID</th> 
					<th field="accountname" width="100">Account Name</th> 
					<th field="trxtypeid" width="20"  hidden="true">trxtypeid</th>	
					<th field="trxtype" width="20">Trx Type</th>										
					<th field="amount" width="20">Amount</th>  
				</tr>  
			</thead>  
		</table> 

</body>
</html>
