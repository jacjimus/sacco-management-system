<?php	
	date_default_timezone_set('Africa/Nairobi');
	include('../conf/config.inc');

	class ProcessImportedFiles {
		private $dbconnect;
		
	   //Database connect
		public function __construct()
		{
			$db = new DB_Class;
			$this->dbconnect=$db->Myconn();					
		}

		public function processmpesarepayments(){			
			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				if($_FILES['mpesarepaymentfile']['error'] > 0){
					echo json_encode(array('retcode'=>'003','retmsg'=>"Error occured when processing the file. Error no :" . $_FILES["mpesarepaymentfile"]["error"],'results'=>''));
				} else {
					$filename = $_FILES['mpesarepaymentfile']['name'];					
					$filesize = $_FILES['mpesarepaymentfile']['size'];
					$filetype = $_FILES['mpesarepaymentfile']['type'];
					$filetemp = $_FILES['mpesarepaymentfile']['tmp_name'];

					//basic check on the file
					$fileextension = explode('.',$filename);
					if(strtoupper($fileextension[1]) != 'CSV' || $filetype != 'application/vnd.ms-excel')
					{
						echo json_encode(array('retcode'=>'003','retmsg'=>"Error: Incorrect file type",'results'=>''));
						return;
					}
					
					//Insert into the database
					$duplicatedmpesarefnos = '';
					$noofprocessedtrxs = 0;
					
					try{
						$count = 1;
						$file = fopen($filetemp, 'r');
						while (($line = fgetcsv($file)) !== FALSE) {							
							$isduplicate = 0;
							
							//Start from row 7 to avoid headers
							if($count >= 7){
								$noofprocessedtrxs++;
								
								//Get sender and mobileno
								//REMEMBER TO TRIM 3 & 12 TO AVOID INCONSISTENT DATE
								$otherpartyinfo = explode('-',$line[10]);
								$mobileno = $otherpartyinfo[0];
								$sendername = $otherpartyinfo[1];
								
								//Get account inputed by the customer
								$details = explode('-',trim($line[3]));
								if(isset($details[1])){
									$details = explode('Acc.',$details[1]);
									if(isset($details[1])){
										$mpesa_acc = trim($details[1]);
									}else{
										$mpesa_acc = '';
									}
								}else{
									$mpesa_acc = '';
								}

								//Check if M-Pesa Reference No Exist
								$sql_checkmpesarefno = "SELECT * FROM tb_mpesarepayments WHERE mpesa_code = '".$line[0]."'";
								if($res=$this->dbconnect->query($sql_checkmpesarefno)){
									if($res->rowCount() > 0){
										$duplicatedmpesarefnos = $duplicatedmpesarefnos.','.$line[0];
										$isduplicate = 1;
									}
								} 
								
								if($isduplicate == 0){
									$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
									$this->dbconnect->beginTransaction();
									$date = str_replace('/', '-', $line[1]);
									//taking care of ddmmyy
									if(strrpos($date,' ') == 8){
										$date =  substr($date,0,6).'20'.substr($date,6,8);
									}elseif(strrpos($date,' ') == 10){
										$date =  substr($date,0,10);								
									}

									$res_loan = $this->dbconnect->prepare("INSERT INTO tb_mpesarepayments(mpesa_code,mpesa_trx_date,mpesa_amt,mpesa_sender,text,mpesa_msisdn,routemethod_name,filename,mpesa_acc,mpesa_trx_status) VALUES(?,?,?,?,?,?,?,?,?,?)");
									$res_loan->execute(array($line[0],date("Y-m-d H:i:s",strtotime($date)),$line[5],$sendername,$line[3],$mobileno,'UPLOAD',$filename,$mpesa_acc,$line[4]));
									
									$this->dbconnect->commit();
								}
							}					  
						  $count++;
						}
						fclose($file);																							
						
						if(strlen($duplicatedmpesarefnos) >0)
						{
							$duplicatedmpesarefnos = '. Duplicates M-Pesa Ref No(s) : '.substr($duplicatedmpesarefnos,0,100);
						}
						
						//Check for transactions where customer does not exist in the system
						$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments LEFT JOIN v_customerdetails  
						ON SUBSTR(mpesa_msisdn,4,LENGTH(mpesa_msisdn)) = RIGHT(TRIM(v_customerdetails.mobileno),9) 
						SET processed=2 WHERE v_customerdetails.mobileno IS NULL AND receiptno IS NULL AND mpesa_code IS NOT NULL"); 			
						$res_customerdoesnotexist->execute();	
						unset($res_customerdoesnotexist);
							
						//Post the repayments
						$mpesarepaymentresults = $this->postMpesaRepayment();
												
						echo json_encode(array('retcode'=>'000','retmsg'=>'File ('.$filename.') processed successfully. No of transactions processed : '.$noofprocessedtrxs.$duplicatedmpesarefnos,'results'=>''));			
					} catch (Exception $e){
						$this->dbconnect->rollBack();
						echo json_encode(array('retcode'=>'003','retmsg'=>'Error occured when processing the file','results'=>''));
					}					
				}		
			}else{
				echo json_encode(array('retcode'=>'003','retmsg'=>'Invalid Request','results'=>''));
			}
		}

		//Post M-Pesa Repayments
		public function postMpesaRepayment(){
			//Get System Settings first
			//Set default values
			$mpesacontraaccountid = 0;
			$systemuserid  = 0;
			$institutioncode = '';
			$mpesarepmandatoryfeeid = 0;
			$loanapplicationfeeid = 0;
			$excessrepaymentamountproductid = 0;			

			$sql_systemsettings = $this->dbconnect->prepare("SELECT settings FROM tb_syssettings");
			$sql_systemsettings->execute();		
			$systemsettings = $sql_systemsettings->fetchAll(PDO::FETCH_ASSOC); 
			
			if(!empty($systemsettings)){		
				$system_settings  = json_decode($systemsettings[0]["settings"], true);		
							 
				$mpesacontraaccountid = $system_settings['mpesacontraaccountid'];
				$systemuserid  = $system_settings['systemuserid'];
				$institutioncode = $system_settings['institutioncode'];
				$mpesarepmandatoryfeeid = $system_settings['mpesarepmandatoryfeeid'];
				$loanapplicationfeeid = $system_settings['loanapplicationfeeid'];
				$excessrepaymentamountproductid = $system_settings['excessrepaymentamountproductid'];					
			}else{
				return '001|Error Occured : System settings not available. Contact system administrator';;
			}		
			//Remember to validate parameters above
			
			//CEMES
			if($institutioncode == 'IMAB00002'){
				$sql_getmpesarepayments="SELECT rowid,mpesa_code AS trxdescription,mpesa_amt AS amount,v_customerdetails.customerid,DATE(mpesa_trx_date) AS valuedate,".$systemuserid." AS systemuserid,".$mpesacontraaccountid." AS mpesacontraaccountid,
					mpesa_sender,mpesa_msisdn		
					FROM tb_mpesarepayments
					INNER JOIN v_customerdetails  
					ON SUBSTR(mpesa_msisdn,4,LENGTH(mpesa_msisdn)) = RIGHT(TRIM(v_customerdetails.mobileno),9)  
					WHERE processed = 0
					ORDER BY mpesa_trx_date";
			}		
			
			//Sun Group
			if($institutioncode == 'IMAB00003'){
				$sql_getmpesarepayments="SELECT rowid,mpesa_code AS trxdescription,mpesa_amt AS amount,v_customerdetails.customerid,DATE(mpesa_trx_date) AS valuedate,".$systemuserid." AS systemuserid,".$mpesacontraaccountid." AS mpesacontraaccountid,
					mpesa_sender,mpesa_msisdn		
					FROM tb_mpesarepayments
					INNER JOIN v_customerdetails  
					ON TRIM(mpesa_acc) = TRIM(v_customerdetails.idno) 
					WHERE processed = 0
					ORDER BY mpesa_trx_date";
			}
			
			$items = array();
			
			if($res_getmpesarepayments=$this->dbconnect->query($sql_getmpesarepayments)){
				if($res_getmpesarepayments->rowCount()>0){			
					foreach($res_getmpesarepayments as $row) {
						$node=array();	
						$node['rowid']=$row['rowid'];
						$node['customerid']=$row['customerid'];
						$node['amount']=$row['amount'];			
						$node['systemuserid']=$row['systemuserid'];	
						$node['trxdescription']=$row['trxdescription'];
						$node['valuedate']=$row['valuedate'];					
						$node['mpesacontraaccountid']=$row['mpesacontraaccountid'];
						$node['mpesa_sender']=$row['mpesa_sender'];
						$node['mpesa_msisdn']=$row['mpesa_msisdn'];				
						array_push($items, $node);
					}
				}else{
					return '001|No repayments to post';
				}
			}else{
				return '001|Error Occured : Cannot get mpesa repayment to post';
			}
			
			//Validate repayment dates before allocation
			foreach($items as $item_validate){			
				//Check if period is active
				$sql_checkperiod="SELECT * FROM tb_period WHERE '".$item_validate['valuedate']."' BETWEEN startdate AND enddate";			
				if($res=$this->dbconnect->query($sql_checkperiod)){
					if($res->rowCount()<=0){					
						$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=3 WHERE receiptno = ?"); 			
						$res_customerdoesnotexist->execute(array($item_validate['trxdescription']));	
						unset($res_customerdoesnotexist);
						
						//Remove item from the array
						unset($items[$item_validate]);					
					}
				}else{					
					$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=3 WHERE receiptno = ?"); 			
					$res_customerdoesnotexist->execute(array($item_validate['trxdescription']));	
					unset($res_customerdoesnotexist);
					
					//Remove item from the array
					unset($items[$item_validate]);					
				} 	
			}
			
			foreach($items as $item){ 
				//Repayment amount to distribute
				$amount = $item['amount'];
				//Fee that must be paid atleast once
				$mandatoryfee = array();			
				//Loan application fee amount to be posted
				$loanapplicationfees = array();			
				//Loan lepayments allocation
				$postrepayments = array();
										
				$excessamount = 0;
				$excessamountaccount = 0;
				$excessamountcreditaccount	= 0;
			
				$sql_updateschedule = '';
				
				//Get this from the settings table
				$charge_registrationfee = 0;
				$charge_loanapplicationfee = 0;
				
				//Check if we need to charge man  fee
				if($mpesarepmandatoryfeeid != 0){
					if($amount > 0){
						//Get the following dynamically
						$mandatoryfeeamount = 0;
						$mandatoryfeeamountpaid = 0;
						$mandatoryfeeaccountid = 0;
						$feeamountallocated = 0;
						$mandatoryfeeincomeacc = 0;
						
						$sql_feeamountpaid = "SELECT COALESCE((SELECT amount FROM tb_fee AS mandatoryfee WHERE mandatoryfee.feeid = ".$mpesarepmandatoryfeeid."),0) AS feeamount,COALESCE(SUM(amount),0) AS feeamountpaid,COALESCE(tb_transactions.accountid,(SELECT accountid FROM tb_accounts AS savingsaccounts WHERE savingsaccounts.productid = 1 AND savingsaccounts.customerid = tb_accounts.customerid)) AS feeaccountid FROM tb_transactions INNER JOIN tb_accounts ON tb_transactions.accountid = tb_accounts.accountid INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0  AND feeid = ".$mpesarepmandatoryfeeid." AND tb_accounts.customerid = ".$item['customerid'];
						if($res_savingsaccount=$this->dbconnect->query($sql_feeamountpaid)){	
							foreach($res_feeamountpaid as $row_feeamountpaid) {
								$mandatoryfeeaccountid = $row_feeamountpaid['feeaccountid'];
								$mandatoryfeeamount = $row_feeamountpaid['feeamount'];
								$mandatoryfeeamountpaid = $row_feeamountpaid['feeamountpaid'];
							}
						}
						
						//Allocate				
						if($mandatoryfeeamountpaid < $mandatoryfeeamount){
							$feeamountallocated = $mandatoryfeeamount - $mandatoryfeeamountpaid;
							
							if($amount <= $feeamountallocated){
								$feeamountallocated = $amount;
								$amount = 0;					
							}else{
								$amount = $amount - $feeamountallocated;
							}						
						}
						
						//Using array to post mandatory fee
						if($feeamountallocated > 0){
							$sql_mandatoryfeeincome = "SELECT COALESCE(feeincome,0) AS feeincomeaccount FROM v_fee WHERE feeid = ".$mpesarepmandatoryfeeid;
							if($res_mandatoryfeeincome=$this->dbconnect->query($sql_mandatoryfeeincome)){	
								foreach($res_mandatoryfeeincome as $row_mandatoryfeeincome) {
									$mandatoryfeeincomeacc = $row_mandatoryfeeincome['feeincomeaccount'];
								}
							}							
														
							$node_madfee = array();
							$node_madfee['accountid'] = $mandatoryfeeaccountid;
							$node_madfee['feeamount'] = $feeamountallocated;
							$node_madfee['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
							$node_madfee['creditaccid'] = $mandatoryfeeincomeacc;
														
							array_push($mandatoryfee, $node_madfee);							
						}						
					}
				}

				//Allocate the rest of the amount 
				if($amount > 0){				
					//Get payoffamount of the oldest active loan for the customer based on disbursement date
					$sql_activeloans = "SELECT accountid,loanid,COALESCE((fn_getinterestbalance(accountid,NOW())+fn_getprincipalbalance(accountid,NOW())),0) AS payoffamount,COALESCE(fn_getprincipalbalance(accountid,NOW()),0) AS principalamount,COALESCE(fn_getinterestbalance(accountid,NOW()),0) AS interestamount FROM tb_loans WHERE customerid = ".$item['customerid']." AND loanstatus = 3 ORDER BY disbursedon ASC";

					$loans_arr = array();
					if($res_activeloans=$this->dbconnect->query($sql_activeloans)){						
						foreach($res_activeloans as $activeloan) {
							$node_activeloan = array();
							$node_activeloan['accountid'] = $activeloan['accountid'];
							$node_activeloan['loanid'] = $activeloan['loanid'];
							$node_activeloan['payoffamount'] = $activeloan['payoffamount'];
							$node_activeloan['principalamount'] = $activeloan['principalamount'];
							$node_activeloan['interestamount'] = $activeloan['interestamount'];
							
							array_push($loans_arr, $node_activeloan);
						}
					}

					//Loop through active loans assgining amount to be paid
					$noofloans = 0;
					$count = 0;
					
					if(!empty($loans_arr)){
						$noofloans = count($loans_arr);
					}

					foreach($loans_arr as $loan){
						$accrueinterest = 0;									
						$glcontrol = 0;
						$interestincome = 0;
						$interestreceivable = 0;
						
						/* Get GL control of the product */	
						$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
								INNER JOIN tb_ledgeraccounts
								ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
								WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = ".$loan['accountid'].")";	

						if($res=$this->dbconnect->query($sql_gllinks)){
							if($res->rowCount()<=0){
								return '001|Invalid GL Control Setup. Contact system administrator';
							}else{
								foreach($res as $row) {	
									if($row['gltype'] == 1){
										$glcontrol = $row['accountid'];	
									}
									if($row['gltype'] == 9){
										$interestincome = $row['accountid'];	
									}
									if($row['gltype'] == 10){
										$interestreceivable = $row['accountid'];	
									}
								}					
							}
						}else{
							return '001|Invalid GL Control Setup. Contact system administrator';		
						}
						
						//Get product settings for loan account
						$sql_product_settings = $this->dbconnect->prepare("SELECT productid,code,description,producttypeid,producttype,currencyid,currency,currencycode,maxaccounts,active,creationdate,activestate, 
							settings,effectivedate,productcontrol,productcontroldesc,interestincome,interestincomedesc,interestreceivable,interestreceivabledesc
							FROM v_getloanproducts WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = ?)");
						if($sql_product_settings->execute(array($loan['accountid']))){					
							$productsettings = $sql_product_settings->fetchAll(PDO::FETCH_ASSOC);
							
							if(!empty($productsettings)){		
								$product_settings  = json_decode($productsettings[0]["settings"], true);		
								
								$accrueinterest = $product_settings['accrueinterest'];						
							}
						}
						
						//Allocate loan application fee
						if($loanapplicationfeeid != 0){
							if($amount > 0){
								//Get the following dynamically
								$loanapplicationfeeamount = 0;
								$loanapplicationfeeamountpaid = 0;
								$loanapplicationfeeamountallocated = 0;
								$checkforfirstloan = 0;
								$loanapplicationfeeincomeacc = 0;
								
								//For sungroup
								if($institutioncode == 'IMAB00003'){
									$sql_checkforfirstloan= "SELECT COALESCE(COUNT(*),0) AS noofloans FROM tb_accounts WHERE customerid = ".$item['customerid']." AND productid <> ".$excessrepaymentamountproductid;
									if($res_checkforfirstloan=$this->dbconnect->query($sql_checkforfirstloan)){	
										foreach($res_checkforfirstloan as $row_checkforfirstloan) {
											$checkforfirstloan = $row_checkforfirstloan['noofloans'];
										}
									}
									
									if($checkforfirstloan == 1){
										$loanapplicationfeeid = 2;//Hard Coded Charge 500
									}									
								}
								
								$sql_loanapplicationfeeaccount = "SELECT COALESCE((SELECT amount FROM tb_fee AS mandatoryfee WHERE mandatoryfee.feeid = ".$loanapplicationfeeid."),0) AS feeamount,COALESCE((SELECT SUM(amount) FROM tb_transactions INNER JOIN tb_receipts ON tb_transactions.receiptno = tb_receipts.receiptno AND tb_receipts.isreversed=0 WHERE tb_transactions.accountid = tb_accounts.accountid AND feeid = ".$loanapplicationfeeid."),0) AS feeamountpaid FROM tb_accounts WHERE accountid = ".$loan['accountid'];
								if($res_account_la=$this->dbconnect->query($sql_loanapplicationfeeaccount)){	
									foreach($res_account_la as $row_la) {
										$loanapplicationfeeamount = $row_la['feeamount'];
										$loanapplicationfeeamountpaid = $row_la['feeamountpaid'];
									}
								}
								
								//Allocate
								if($loanapplicationfeeamountpaid < $loanapplicationfeeamount){
									$loanapplicationfeeamountallocated = $loanapplicationfeeamount - $loanapplicationfeeamountpaid;
									if($amount <= $loanapplicationfeeamountallocated){
										$loanapplicationfeeamountallocated = $amount;
										$amount = 0;					
									}else{
										$amount = $amount - $loanapplicationfeeamountallocated;
									}
									
									if($loanapplicationfeeamountallocated > 0){
										$sql_loanapplicationfeeincome = "SELECT COALESCE(feeincome,0) AS feeincomeaccount FROM v_fee WHERE feeid = ".$loanapplicationfeeid;
										if($res_loanapplicationfeeincome=$this->dbconnect->query($sql_loanapplicationfeeincome)){	
											foreach($res_loanapplicationfeeincome as $row_loanapplicationfeeincome) {
												$loanapplicationfeeincomeacc = $row_loanapplicationfeeincome['feeincomeaccount'];
											}
										}											
										$node_laf = array();
										$node_laf['accountid'] = $loan['accountid'];
										$node_laf['feeamount'] = $loanapplicationfeeamountallocated;
										$node_laf['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
										$node_laf['creditaccid'] = $loanapplicationfeeincomeacc;
										
										array_push($loanapplicationfees, $node_laf);							
									}
								}				
							}
						}
												
						//Allocate amount to loan
						if(($amount > 0) && ($loan['payoffamount'] > 0)){
							$principalamount = 0;
							$interestamount = 0;	
								
							if($amount < $loan['payoffamount']){																					
								if($accrueinterest == 1){
									//Allocate interest
									if($amount <= $loan['interestamount']){
										$interestamount = $amount;
										$amount = 0;
									}else{
										$interestamount = $amount - $loan['interestamount'];
										$amount = $amount - $interestamount;
									}
									
									if($amount > 0){
										if($amount <= $loan['principalamount']){
											$principalamount = $amount;
											$amount = 0;
										}else{
											$principalamount = $amount - $loan['principalamount'];
											$amount = $amount - $principalamount;
										}
									}

									$allocate_principalamount = $principalamount;
									$allocate_interestamount = $interestamount;
							
									//Update Schedule
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid']))){														
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
											
											if($allocate_interestamount > 0){
												if($allocate_interestamount <= $val_interest){
													$interestpaid = $allocate_interestamount;
													$allocate_interestamount = 0;							
												}else{
													$interestpaid = $val_interest;
													$allocate_interestamount = $allocate_interestamount + $interestpaid;							
												}
											}
											
											if($allocate_principalamount > 0){
												if($allocate_interestamount <= $val_principal){
													$interestpaid = $allocate_principalamount;
													$allocate_principalamount = 0;							
												}else{
													$principalpaid = $val_principal;
													$allocate_principalamount = $allocate_principalamount + $principalpaid;							
												}
											}
											
											if(($principalpaid > 0) && ($interestpaid > 0)){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($interestpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($principalpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}									
										}
									}									
								}else{								
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid']))){														
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
																				
											if($amount > 0){
												if($amount <= $val_interest){
													$principalpaid = 0;
													$interestpaid = $amount;
													
													$interestamount = $interestamount + $interestpaid;
													$principalamount = $principalamount + $principalpaid;
													$amount = $amount - $interestpaid;									
												}else{
													$interestpaid = $val_interest;
													$amount = $amount - $interestpaid;
													
													if($amount <= $val_principal){
														$principalpaid = $amount;
														$amount = $amount - $principalpaid;										
													}else{
														$principalpaid = $val_principal;
														$amount = $amount - $principalpaid;
													}									
													
													$principalamount = $principalamount + $principalpaid;
													$interestamount = $interestamount + $interestpaid;									
												}
												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}							
										}
									}
								}
								
								$node_repayment = array();
								$node_repayment['accountid'] = $loan['accountid'];
								$node_repayment['loanid'] = $loan['loanid'];
								$node_repayment['amount'] = $loan['payoffamount'];
								$node_repayment['systemuserid'] = $item['systemuserid'];
								$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
								$node_repayment['valuedate'] = $item['valuedate'];
								$node_repayment['mpesacontraaccountid'] = $item['mpesacontraaccountid'];								
								$node_repayment['principalamount'] = $principalamount; 
								$node_repayment['interestamount'] = $interestamount;
								$node_repayment['closeloan'] = 0;
								$node_repayment['updateschedule'] = $sql_updateschedule;
								$node_repayment['accrueinterest'] = $accrueinterest;								
								$node_repayment['glcontrol'] = $glcontrol;
								$node_repayment['interestincome'] = $interestincome;
								$node_repayment['interestreceivable'] = $interestreceivable;
						
								array_push($postrepayments, $node_repayment);								
							}else if($amount == $loan['payoffamount']){
								if($accrueinterest == 1){
									//Allocate interest
									if($amount <= $loan['interestamount']){
										$interestamount = $amount;
										$amount = 0;
									}else{
										$interestamount = $amount - $loan['interestamount'];
										$amount = $amount - $interestamount;
									}
									
									if($amount > 0){
										if($amount <= $loan['principalamount']){
											$principalamount = $amount;
											$amount = 0;
										}else{
											$principalamount = $amount - $loan['principalamount'];
											$amount = $amount - $principalamount;
										}
									}

									$allocate_principalamount = $principalamount;
									$allocate_interestamount = $interestamount;
							
									//Update Schedule
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid']))){														
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
											
											if($allocate_interestamount > 0){
												if($allocate_interestamount <= $val_interest){
													$interestpaid = $allocate_interestamount;
													$allocate_interestamount = 0;							
												}else{
													$interestpaid = $val_interest;
													$allocate_interestamount = $allocate_interestamount + $interestpaid;							
												}
											}
											
											if($allocate_principalamount > 0){
												if($allocate_interestamount <= $val_principal){
													$interestpaid = $allocate_principalamount;
													$allocate_principalamount = 0;							
												}else{
													$principalpaid = $val_principal;
													$allocate_principalamount = $allocate_principalamount + $principalpaid;							
												}
											}
											
											if(($principalpaid > 0) && ($interestpaid > 0)){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($interestpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($principalpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}									
										}
									}									
								}else{								
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid'])))
									{																
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
																				
											if($amount > 0)
											{
												if($amount <= $val_interest)
												{
													$principalpaid = 0;
													$interestpaid = $amount;
													
													$interestamount = $interestamount + $interestpaid;
													$principalamount = $principalamount + $principalpaid;
													$amount = $amount - $interestpaid;									
												}else{
													$interestpaid = $val_interest;
													$amount = $amount - $interestpaid;
													
													if($amount <= $val_principal){
														$principalpaid = $amount;
														$amount = $amount - $principalpaid;										
													}else{
														$principalpaid = $val_principal;
														$amount = $amount - $principalpaid;
													}									
													
													$principalamount = $principalamount + $principalpaid;
													$interestamount = $interestamount + $interestpaid;									
												}
												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}							
										}
									}
								}
								
								$node_repayment = array();
								$node_repayment['accountid'] = $loan['accountid'];
								$node_repayment['loanid'] = $loan['loanid'];
								$node_repayment['amount'] = $loan['payoffamount'];
								$node_repayment['systemuserid'] = $item['systemuserid'];
								$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
								$node_repayment['valuedate'] = $item['valuedate'];
								$node_repayment['mpesacontraaccountid'] = $item['mpesacontraaccountid'];								
								$node_repayment['principalamount'] = $principalamount; 
								$node_repayment['interestamount'] = $interestamount;
								$node_repayment['closeloan'] = 1;
								$node_repayment['updateschedule'] = $sql_updateschedule;
								$node_repayment['accrueinterest'] = $accrueinterest;
								$node_repayment['glcontrol'] = $glcontrol;
								$node_repayment['interestincome'] = $interestincome;
								$node_repayment['interestreceivable'] = $interestreceivable;								

								array_push($postrepayments, $node_repayment);																						
							}else{
								$amount_allocate = $loan['payoffamount'];
								$amount = $amount - $loan['payoffamount'];
								
								if($accrueinterest == 1){
									//Allocate interest
									if($amount_allocate <= $loan['interestamount']){
										$interestamount = $amount_allocate;
										$amount_allocate = 0;
									}else{
										$interestamount = $amount_allocate - $loan['interestamount'];
										$amount_allocate = $amount_allocate - $interestamount;
									}
									
									if($amount_allocate > 0){
										if($amount_allocate <= $loan['principalamount']){
											$principalamount = $amount_allocate;
											$amount_allocate = 0;
										}else{
											$principalamount = $amount_allocate - $loan['principalamount'];
											$amount_allocate = $amount_allocate - $principalamount;
										}
									}

									$allocate_principalamount = $principalamount;
									$allocate_interestamount = $interestamount;
							
									//Update Schedule
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid']))){														
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
											
											if($allocate_interestamount > 0){
												if($allocate_interestamount <= $val_interest){
													$interestpaid = $allocate_interestamount;
													$allocate_interestamount = 0;							
												}else{
													$interestpaid = $val_interest;
													$allocate_interestamount = $allocate_interestamount + $interestpaid;							
												}
											}
											
											if($allocate_principalamount > 0){
												if($allocate_interestamount <= $val_principal){
													$interestpaid = $allocate_principalamount;
													$allocate_principalamount = 0;							
												}else{
													$principalpaid = $val_principal;
													$allocate_principalamount = $allocate_principalamount + $principalpaid;							
												}
											}
											
											if(($principalpaid > 0) && ($interestpaid > 0)){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($interestpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}elseif($principalpaid > 0){												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}									
										}
									}									
								}else{												
									$sql_loan_schedule = $this->dbconnect->prepare("SELECT tb_loanschedule.* FROM tb_loanschedule INNER JOIN tb_loans ON tb_loans.loanid = tb_loanschedule.loanid WHERE accountid = ? AND (principal+interest)-(principalpaid+interestpaid) != 0 ORDER BY period");
									if($sql_loan_schedule->execute(array($loan['accountid'])))
									{																
										$loanschedule = $sql_loan_schedule->fetchAll(PDO::FETCH_ASSOC);
										unset($sql_loan_schedule);
										foreach($loanschedule as $row){
											$val_principal = $row['principal'];
											$val_interest = $row['interest'];
											$val_principalpaid = $row['principalpaid'];
											$val_interestpaid = $row['interestpaid'];							
											$val_loanscheduleid = $row['loanscheduleid'];
											$val_loanid = $row['loanid'];
											
											$principalpaid = 0;
											$interestpaid = 0;
											
											//Check for partial payments
											//Interest
											if($val_interestpaid > 0){
												$val_interest = $val_interest - $val_interestpaid;
											}
											//Principal
											if($val_principalpaid > 0)
											{
												$val_principal = $val_principal - $val_principalpaid;
											}
																				
											if($amount_allocate > 0)
											{
												if($amount_allocate <= $val_interest)
												{
													$principalpaid = 0;
													$interestpaid = $amount_allocate;
													
													$interestamount = $interestamount + $interestpaid;
													$principalamount = $principalamount + $principalpaid;
													$amount_allocate = $amount_allocate - $interestpaid;									
												}else{
													$interestpaid = $val_interest;
													$amount_allocate = $amount_allocate - $interestpaid;
													
													if($amount_allocate <= $val_principal){
														$principalpaid = $amount_allocate;
														$amount_allocate = $amount_allocate - $principalpaid;										
													}else{
														$principalpaid = $val_principal;
														$amount_allocate = $amount_allocate - $principalpaid;
													}									
													
													$principalamount = $principalamount + $principalpaid;
													$interestamount = $interestamount + $interestpaid;									
												}
												
												$sql_updateschedule .= "UPDATE tb_loanschedule SET principalpaid=principalpaid+".$principalpaid.",interestpaid=interestpaid+".$interestpaid." WHERE loanscheduleid=".$val_loanscheduleid." AND loanid=".$val_loanid." ; ";								
											}							
										}
									}
								}
															
								$node_repayment = array();
								$node_repayment['accountid'] = $loan['accountid'];
								$node_repayment['loanid'] = $loan['loanid'];
								$node_repayment['amount'] = $loan['payoffamount'];
								$node_repayment['systemuserid'] = $item['systemuserid'];
								$node_repayment['trxdescription'] = 'M-Pesa Transaction - '.$item['trxdescription'];
								$node_repayment['valuedate'] = $item['valuedate'];
								$node_repayment['mpesacontraaccountid'] = $item['mpesacontraaccountid'];								
								$node_repayment['principalamount'] = $principalamount; 
								$node_repayment['interestamount'] = $interestamount;
								$node_repayment['closeloan'] = 1;
								$node_repayment['updateschedule'] = $sql_updateschedule;
								$node_repayment['accrueinterest'] = $accrueinterest;
								$node_repayment['glcontrol'] = $glcontrol;
								$node_repayment['interestincome'] = $interestincome;
								$node_repayment['interestreceivable'] = $interestreceivable;								
								
								array_push($postrepayments, $node_repayment);
							}
						}
						
						$count = $count+1;
					}
									
					//Check for excess and post to savings after distributing to loans
					if(($amount > 0) && ($count == $noofloans)){
						$excessamount = $amount;
						$amount = 0;

						$sql_excessrepaymentaccount = "SELECT accountid FROM tb_accounts WHERE tb_accounts.productid = ".$excessrepaymentamountproductid." AND tb_accounts.customerid = ".$item['customerid']." LIMIT 1";
						if($res_excessrepaymentaccount=$this->dbconnect->query($sql_excessrepaymentaccount)){	
							foreach($res_excessrepaymentaccount as $row_excessrepaymentaccount){
								$excessamountaccount = $row_excessrepaymentaccount['accountid'];
							}
						}
						
						/* Get GL control of the product */	
						$sql_gllinks="SELECT tb_ledgerlinks.gltype,tb_ledgeraccounts.accountid FROM tb_ledgerlinks
								INNER JOIN tb_ledgeraccounts
								ON tb_ledgerlinks.accountid = tb_ledgeraccounts.accountid
								WHERE productid IN (SELECT productid FROM tb_accounts WHERE accountid = ".$excessamountaccount.")";	

						if($res=$this->dbconnect->query($sql_gllinks)){
							if($res->rowCount()<=0){
								return '001|Invalid GL Control Setup. Contact system administrator';
							}else{
								foreach($res as $row) {	
									if($row['gltype'] == 1){
										$excessamountcreditaccount = $row['accountid'];	
									}									
								}					
							}
						}else{
							return '001|Invalid GL Control Setup. Contact system administrator';		
						}						
					}
				}
				
				/*
				print_r($excessamount);
				echo '<br>';
				print_r($mandatoryfee);
				echo '<br>';
				print_r($loanapplicationfees);				
				echo '<br>';
				print_r($postrepayments);				
				return;											
				*/
				//Post transactions 
				try
				{			
					$this->dbconnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$this->dbconnect->beginTransaction();
					
					$serverdate = date('Y-m-d');
					$receiptno = 0;
					$ledgerid = 0;
					
					//post mandatory fee 
					if(!empty($mandatoryfee)){
						foreach($mandatoryfee as $mandatory_fee){					
							$iscredit = 1;
							$transactiontype = 3;
							$debitaccid = $mpesacontraaccountid;
							$creditaccid = $mandatory_fee['creditaccid'];
							
							if($receiptno == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
								$res_posttrx->execute( array($mandatory_fee['trxdescription'],date("Y-m-d",strtotime($item['valuedate'])),$systemuserid,0));
								unset($res_posttrx);
								
								$receiptno = $this->dbconnect->lastInsertId();	
							}
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array($receiptno,date("Y-m-d",strtotime($item['valuedate'])),$mandatory_fee['accountid'],$loanapplicationfeeid,$iscredit,$laf['feeamount'],$transactiontype,$debitaccid,$creditaccid));
							unset($res_posttrx);
							
							if($ledgerid == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
								$res_posttrx->execute( array(1,1,0,date("Y-m-d",strtotime($item['valuedate'])),$systemuserid,$receiptno,$mandatory_fee['trxdescription']));
								unset($res_posttrx);
								
								$ledgerid = $this->dbconnect->lastInsertId();
							}
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$debitaccid,0,$mandatory_fee['feeamount']));			
							unset($res_posttrx);
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$creditaccid,1,$mandatory_fee['feeamount']));	
							unset($res_posttrx);
						}					
					}
			
					//Post Loan Application Fee
					if(!empty($loanapplicationfees)){
						foreach($loanapplicationfees as $laf){					
							$iscredit = 1;
							$transactiontype = 3;
							$debitaccid = $mpesacontraaccountid;
							$creditaccid = $laf['creditaccid'];;
							
							if($receiptno == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
								$res_posttrx->execute( array($laf['trxdescription'],date("Y-m-d",strtotime($item['valuedate'])),$systemuserid,0));
								unset($res_posttrx);
								
								$receiptno = $this->dbconnect->lastInsertId();	
							}
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array($receiptno,date("Y-m-d",strtotime($item['valuedate'])),$laf['accountid'],$loanapplicationfeeid,$iscredit,$laf['feeamount'],$transactiontype,$debitaccid,$creditaccid));
							unset($res_posttrx);
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array(1,1,0,date("Y-m-d",strtotime($item['valuedate'])),$systemuserid,$receiptno,$laf['trxdescription']));
							unset($res_posttrx);
							
							$ledgerid = $this->dbconnect->lastInsertId();

							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$debitaccid,0,$laf['feeamount']));			
							unset($res_posttrx);
							
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
							$res_posttrx->execute( array($ledgerid,date("Y-m-d",strtotime($item['valuedate'])),$creditaccid,1,$laf['feeamount']));	
							unset($res_posttrx);
						}					
					}
					
					//loop through repayments and post. NB: In this case there is no overpayments
					if(!empty($postrepayments)){
						foreach($postrepayments as $postrepayment){
							$accountid   = $postrepayment['accountid'];
							$loanid   = $postrepayment['loanid'];
							$amount   = $postrepayment['amount'];
							$systemuserid   = $postrepayment['systemuserid'];
							$trxdescription   = $postrepayment['trxdescription'];
							$valuedate   = date("Y-m-d",strtotime($postrepayment['valuedate']));
							$mpesacontraaccountid   = $postrepayment['mpesacontraaccountid'];	
							$principalamount   = $postrepayment['principalamount'];	
							$interestamount   = $postrepayment['interestamount'];	
							$closeloan   = $postrepayment['closeloan'];
							$rep_accrueinterest    = $postrepayment['accrueinterest'];
							$rep_glcontrol = $node_repayment['glcontrol'];
							$rep_interestincome = $node_repayment['interestincome'];
							$rep_interestreceivable = $node_repayment['interestreceivable'];
								
							//Set receipt
							if($receiptno == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
								$res_posttrx->execute( array($trxdescription,$valuedate,$systemuserid,0));
								unset($res_posttrx);
								
								$receiptno = $this->dbconnect->lastInsertId();	
							}
							
							//Set ledger
							if($ledgerid == 0){
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
								$res_posttrx->execute( array(1,1,0,$valuedate,$systemuserid,$receiptno,$trxdescription));
								unset($res_posttrx);
								$ledgerid = $this->dbconnect->lastInsertId();
							}
							
							//Post Principal 
							if($principalamount > 0){
								$debitaccid = $mpesacontraaccountid;
								$creditaccid = $rep_glcontrol;
								
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
								$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,1,$principalamount,6,$debitaccid,$creditaccid));
								unset($res_posttrx);
								
								$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET crAmount=crAmount+? WHERE accountid=?"); 			
								$res_posttrx->execute( array($principalamount,$accountid));
								unset($res_posttrx);
							
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
								$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$principalamount));			
								unset($res_posttrx);
								
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
								$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$principalamount));	
								unset($res_posttrx);
							}			
										
							if($interestamount > 0){
								$interestincome = $rep_interestincome;
								$interestreceivable = $rep_interestreceivable;
									
								if($rep_accrueinterest == 0){
									//Post Interest Charged against loan account (but it should not affect loan balance) ---> DR - Interest receivable CR - Interest Income									
									$debitaccid = $interestreceivable;
									$creditaccid = $interestincome;	
								
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
									$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,0,$interestamount,7,$debitaccid,$creditaccid));
									unset($res_posttrx);
									
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
									$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
									unset($res_posttrx);
									$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
									$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));				
									unset($res_posttrx);
								}
								
								//Post Interest paid against loan account (but it should not affect loan balance) ---> DR - Source of payment e.g. bank\cash\mpesa CR - Interest receivable
								if($rep_accrueinterest == 1){
									$debitaccid = $mpesacontraaccountid;
									$creditaccid = $interestincome;	
								}else{
									$debitaccid = $mpesacontraaccountid;
									$creditaccid = $interestreceivable;
								}
								
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
								$res_posttrx->execute( array($receiptno,$valuedate,$accountid,0,1,$interestamount,8,$debitaccid,$creditaccid));
								unset($res_posttrx);
								
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
								$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$interestamount));			
								unset($res_posttrx);
								
								$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
								$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$interestamount));		
								unset($res_posttrx);
							}	

							if($closeloan == 1){
								//close loan
								$res_posttrx = $this->dbconnect->prepare("UPDATE tb_loans SET loanstatus=5 WHERE accountid = ?"); 			
								$res_posttrx->execute( array($accountid));	
								unset($res_posttrx);
								//close loan account
								$res_posttrx = $this->dbconnect->prepare("UPDATE tb_accounts SET accountstatus=7,active=0 WHERE accountid = ?"); 			
								$res_posttrx->execute( array($accountid));	
								unset($res_posttrx);						
							}
							
							$sql_updateschedule = $postrepayment['updateschedule'];
							if(strlen($sql_updateschedule) > 0){
								$res_update_schedule = $this->dbconnect->prepare($sql_updateschedule); 			
								$res_update_schedule->execute();
								unset($res_update_schedule);				
							}							
						}
					}
					
					//Post excess payment to savings account.
					if($excessamount > 0){
						$debitaccid = $mpesacontraaccountid;
						$creditaccid = $excessamountcreditaccount;	

						$trxdescription = 'M-Pesa Transaction - '.$item['trxdescription'];				
						$valuedate = date("Y-m-d",strtotime($item['valuedate']));
						$systemuserid = 1;
						
						//Set receipt
						if($receiptno == 0){
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_receipts(trxdescription,valuedate,createdby,isprinted) VALUES (?,?,?,?)"); 			
							$res_posttrx->execute( array($trxdescription,$valuedate,$systemuserid,0));
							unset($res_posttrx);
							
							$receiptno = $this->dbconnect->lastInsertId();	
						}
						
						//Set ledger
						if($ledgerid == 0){
							$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgers(ledgertypeid,periodid,yearend,valuedate,createdby,receiptno,transactionnote) VALUES (?,?,?,?,?,?,?);"); 			
							$res_posttrx->execute( array(1,1,0,$valuedate,$systemuserid,$receiptno,$trxdescription));
							unset($res_posttrx);
							$ledgerid = $this->dbconnect->lastInsertId();
						}
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_transactions(receiptno,valuedate,accountid,feeid,iscredit,amount,transactiontype,debitaccid,creditaccid) VALUES (?,?,?,?,?,?,?,?,?);"); 			
						$res_posttrx->execute( array($receiptno,$valuedate,$excessamountaccount,0,1,$excessamount,1,$debitaccid,$creditaccid));
						unset($res_posttrx);
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,$valuedate,$debitaccid,0,$excessamount));			
						unset($res_posttrx);
						
						$res_posttrx = $this->dbconnect->prepare("INSERT INTO tb_ledgertransactions(ledgerid,valuedate,accountid,iscredit,amount) VALUES (?,?,?,?,?);"); 			
						$res_posttrx->execute( array($ledgerid,$valuedate,$creditaccid,1,$excessamount));	
						unset($res_posttrx);				
					}			
					
					$res_update_loan = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=1,receiptno=? WHERE rowid = ?"); 			
					$res_update_loan->execute( array($receiptno,$item['rowid']));  
					unset($res_update_loan);	
					
					$message = 'Dear '.$item['mpesa_sender'].' your payment of ksh '.$item['amount'].' has been received. Thanks for choosing CEMES Ltd';
					$res_txt = $this->dbconnect->prepare("INSERT INTO tb_quesms(phoneno,message,sent,statusid,source) VALUES(?,?,0,0,1)"); 			
					$res_txt->execute(array($item['mpesa_msisdn'],$message));	
					unset($res_txt);			  
					
					$this->dbconnect->commit();	
					
					$retcode='000';
					$retmsg='Transaction Posted Successfully. Receipt No : '.$receiptno;
					$results='';	
					
				} catch (Exception $e) {
					  $this->dbconnect->rollBack();
					  $retcode = '003';
					  $retmsg = $e->getMessage();
					  $results = '';
				  
					$res_customerdoesnotexist = $this->dbconnect->prepare("UPDATE tb_mpesarepayments SET processed=4 WHERE receiptno = ?"); 			
					$res_customerdoesnotexist->execute(array($item['trxdescription']));	
					unset($res_customerdoesnotexist);			  
				}			
			}	
				
			return '000|Transaction Posted Successfully';
		}			
	}
	
	$processimportedfiles=new ProcessImportedFiles();
	
	//Import Mpesa File
	$processimportedfiles->processmpesarepayments();
	
	//Used to force m-pesa transaction posting
	//$processimportedfiles->postMpesaRepayment();
?>