<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>...::: IMAB MIS :::...</title>
	<link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../css/isacco.css">
	
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../js/isacco.js"></script>
</head>
<body>
<div id="toolbar_gl" style="padding:5px;height:auto"> 
    <div style="margin-bottom:5px"> 
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newLedgerAccount()">New Ledger Account</a>  
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editLedgerAccount()">Edit Ledger Account</a>  
    </div> 
    <div> 
        <input name="searchledgeraccounts" id="ss" class="easyui-combobox" style="width:350px"
        url="../data/get_data.php?action=48" valueField="accountid"  textField="accountname">
       
         <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="searchLedgeraccouts()">Search</a>
    </div> 
</div>  
<table id="dgLedgerAccount" class="easyui-datagrid"   style="width:1100px;height:380px"
        data-options="url:'../data/get_data.php?action=47&accountid=0',method:'get',toolbar:'#toolbar_gl',singleSelect:true,fitColumns:false,striped:true,autoRowHeight:false,remoteSort:true,sortName:'categoryname',pagination:true">
    <thead> 
        <tr> 
			<th data-options="field:'branchid',hidden:true">Branch ID</th>
			<th data-options="field:'branchname'">Branch</th>			
			<th data-options="field:'accountid'">Account ID</th>
			<th data-options="field:'number'">Number</th>
			<th data-options="field:'description'">Name</th>
			<th data-options="field:'dramount'">Dr Amount</th>
			<th data-options="field:'cramount'">CR Amount</th>
			<th data-options="field:'balancedate'">Balance Date</th>
			<th data-options="field:'categoryid',hidden:true">categoryid</th>
			<th data-options="field:'categoryname'">Category Name</th>			
			<th data-options="field:'financialcategoryid',hidden:true">financialcategoryid</th>
			<th data-options="field:'financialcategory'">Financial Category</th>
			<th data-options="field:'subfinancialcategoryid',hidden:true">financialcategoryid</th>
			<th data-options="field:'subfinancialcategory'">Sub Financial Category</th>			
			<th data-options="field:'active',hidden:true">active</th>
			<th data-options="field:'activestate'">Active</th>			
        </tr>  
    </thead>  
</table>  

<div id="dlgLedgerAccount" class="easyui-dialog" style="width:400px;height:350px;padding:10px 20px"  
        closed="true" buttons="#dlg-buttons-LedgerAccount">   
    <form id="fmLedgerAccount" method="post"> 
        <table>
		<tr>
            <td>Branch :</td>  
            <td><input name="branchid" id="branchid" class="easyui-combobox" style="width:200px"
            url="../data/get_data.php?action=7" valueField="branchid" textField="branchname" required="true"/> 
			</td>  
        </tr> 
        <tr>
            <td>Category :</td>  
            <td><input name="categoryid" id="categoryid" class="easyui-combobox" style="width:200px"
			data-options="  
			valueField: 'categoryid',  
			textField: 'description',  
			url: '../data/get_data.php?action=44',  
			onSelect: function(rec){  
				var url = '../data/get_data.php?action=45&categoryid='+rec.categoryid; 
				$('#financialcategoryid').combobox('clear');
				$('#financialcategoryid').combobox('reload', url);  
			}"  required="true"/>  
  
			</td>
        </tr> 	
        <tr>  
            <td>Sub Category 0:</td>  
			<td><input name="financialcategoryid" id="financialcategoryid" class="easyui-combobox" style="width:200px"
						data-options="  
			valueField: 'formatid',  
			textField: 'description',   
			onSelect: function(rec){  
				var url = '../data/get_data.php?action=46&parentid='+rec.formatid; 
				$('#subfinancialcategoryid').combobox('clear');
				$('#subfinancialcategoryid').combobox('reload', url);  
			}"  required="true"/> 
			</td>
        </tr> 	
        <tr>  
            <td>Sub Category 1:</td>  
            <td><input name="subfinancialcategoryid" id="subfinancialcategoryid" class="easyui-combobox" style="width:200px"
            valueField="formatid" textField="description"  required="true" />   
			</td>
        </tr>		
        <tr>  
            <td>Account Name :</td> 
            <td><input name="description" class="easyui-validatebox" required="true" style="width:200px"/>  
			</td>
        </tr> 	
        <tr>  
            <td>Active :</td> 
            <td>
			<select id="active" class="easyui-combobox"  required="true" name="active" style="width:200px;">  
				<option value="0">False</option>  
				<option value="1">True</option> 
			</select>			
			</td>
        </tr> 		
		</table>
    </form>  
</div>  
<div id="dlg-buttons-LedgerAccount">  
    <a href="#" id="btnsaveledgeraccount" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#btnsaveledgeraccount').linkbutton('disable');saveLedgerAccount();">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#fmLedgerAccount').form('clear');$('#dlgLedgerAccount').dialog('close');">Cancel</a>  
</div>  

</body>
</html>
