<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>...::: IMAB MIS :::...</title>
	<link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../css/isacco.css">
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../js/isacco.js"></script>
</head>

<body>
<div id="toolbar" style="padding:5px;height:auto"> 
    <div style="margin-bottom:5px"> 
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserprofile()">New User Profile</a>  
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserprofile()">Edit User Profile</a>   
    </div> 
    <div> 
        <input name="searchuserprofile" id="sup" class="easyui-combobox" style="width:200px"
        url="../data/get_data.php?action=3" valueField="profileid"  textField="profilename">
       
         <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="searchUser()">Search</a>
    </div> 
</div> 
 
<table id="dgUserProfile" class="easyui-datagrid" style="width:auto;height:400px"
   data-options="rownumbers:true,fitColumns:true,singleSelect:true,url:'../data/get_data.php?action=3',method:'get',toolbar:'#toolbar'">
    <thead> 
        <tr> 
        	<th data-options="field:'profileid'">Profile ID</th>
        	<th data-options="field:'profilename'">Profile Name</th>
        	<th data-options="field:'profileactive',hidden:'true'">Profile Name</th>
        	<th data-options="field:'activestate'">Is Active</th>	
        </tr>  
    </thead>  
</table>  

<div id="dlgUserProfile" class="easyui-dialog" style="width:450px;height:500px;padding:10px 20px"  
        closed="true" buttons="#dlg-buttons-UserProfile">   
    <form id="fmUserProfile" method="post">
		<table>
			<tr>
				<td>Profile Name :</td>
				<td><input id="profilename" name="profilename" class="easyui-validatebox" required="true"></td>
			</tr>	
			<tr>
				<td>Is Active :</td>
				<td> 
					<select id="profileactive" class="easyui-combobox" name="profileactive" style="width:200px;">  
						<option value="0">false</option>  
						<option value="1">true</option> 
					</select>
				</td> 
			</tr>
			<tr>
				<td colspan="2"> 
					<ul id="menuitems" class="easyui-tree" checkbox="true"></ul>
				</td> 
			</tr>			
		</table>
    </form>  
</div>  
<div id="dlg-buttons-UserProfile">  
    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserprofile()">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#fmUserProfile').form('clear');$('#dlgUserProfile').dialog('close');">Cancel</a>  
</div>  
</body>
</html>
